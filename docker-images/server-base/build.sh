#!/bin/bash
IMAGE="registry.gitlab.com/sambryant/rosy/server-base"
PARENT=$(dirname $(realpath "$0"))

set -e

echo "[1/1] Building docker image"
cd "$PARENT"
docker build . -t $IMAGE -f Dockerfile
echo "Build succeeded. To push image to container repository:"
echo "  docker push $IMAGE:latest"
