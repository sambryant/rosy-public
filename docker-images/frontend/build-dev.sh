#!/bin/bash
IMAGE="rosy-frontend-dev"
PARENT=$(dirname $(realpath "$0"))

set -e

echo "[1/1] Building docker image"
cd "$PARENT"
docker build . -t "$IMAGE" -f Dockerfile.dev
echo "Successfully built $IMAGE"
