#!/bin/bash
IMAGE="registry.gitlab.com/sambryant/rosy/frontend"
PARENT=$(dirname $(realpath "$0"))
ANGULAR_DIR="$PARENT/../../rosy-client"
NGINX_DIR="$PARENT/../../nginx"

set -e

CONTEXT="$PARENT/context"
# Prepare context directory which holds things which get copied to docker contaienr
if [ -e "$CONTEXT" ]; then
  rm -rf "$CONTEXT"
fi
mkdir "$CONTEXT"

echo "[1/3] Compiling frontend"
cd "$ANGULAR_DIR"
npm i
ng build
cp -rf "dist/d" "$CONTEXT"/

echo "[2/3] Copying over needed files"
cp "$NGINX_DIR/nginx.conf" "$CONTEXT"/
cp "$NGINX_DIR/mime.types" "$CONTEXT"/

echo "[3/3] Building docker image"
cd "$PARENT"
docker build . -t $IMAGE -f Dockerfile

echo "Build succeeded. To push image to container repository:"
echo "  docker push $IMAGE:latest"
