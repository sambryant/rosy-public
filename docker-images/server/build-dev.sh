#!/bin/bash
IMAGE="rosy-server-dev"
PARENT=$(dirname $(realpath "$0"))
SERVER_DIR="$PARENT/../../rosy-server"
PY_DIR="$PARENT/../../rosy-server/py"

set -e

CONTEXT="$PARENT/context-dev"
# Prepare context directory which holds things which get copied to docker contaienr
if [ -e "$CONTEXT" ]; then
  rm -rf "$CONTEXT"
fi
mkdir "$CONTEXT"

echo "[1/2] Copying over needed files"
cp -r "$SERVER_DIR/src" "$CONTEXT"/
cp "$SERVER_DIR/Cargo.toml" "$CONTEXT"/

echo "[2/2] Building docker image"
cd "$PARENT"
docker build . -t $IMAGE -f Dockerfile.dev

echo "Successfully built $IMAGE"