const configFn = require('./karma.conf.js');

// Took 16.078 seconds from cold start

module.exports = function (config) {
  config.set(configFn(config));
};
