#!/usr/bin/python
import argparse
import os
import re
import sys
from yachalk import chalk
from typing import List, Tuple

def go(dir):
  unit_tests = []
  scan_for_unit_tests(dir, unit_tests)

  flattened = []
  for ut in unit_tests:
    for (_, name, testbeds) in analyze_spec_file(ut):
      for imports in testbeds:
        flattened.append((ut, name, imports or []))

  missing_noop = []
  strange_name = []
  imports_self = []

  for (file, block_name, imports) in flattened:
    m = re.search('^(.*)(Component|Module|Pipe|Service|Directive)', block_name)
    if m:
      base_name = m.group(1)
      test_type = m.group(2)
      base_module = base_name + 'Module'

      if any(imp == base_module for imp in imports):
        imports_self.append((file, block_name, imports))

      if test_type not in ['Service', 'Pipe'] and not any(imp == 'NoopAnimationsModule' for imp in imports):
        missing_noop.append((file, block_name, imports))

    else:
      strange_name.append((file, block_name, imports))

  report_results(missing_noop, imports_self, strange_name)


def report_results(missing_noop, imports_self, strange_name):
  if missing_noop:
    print(chalk.yellow('Descibe blocks missing NoopAnimationsModule:'))
    for (file, block_name, imports) in missing_noop:
      print('    %s (%s)' % (block_name, file))

  if imports_self:
    print(chalk.yellow('Descibe blocks importing their own Module:'))
    for (file, block_name, imports) in imports_self:
      print('    %s (%s)' % (block_name, file))

  if strange_name:
    print(chalk.red('Descibe blocks with odd names:'))
    for (file, block_name, imports) in strange_name:
      print('    %s (%s)' % (block_name, file))

  if not missing_noop and not imports_self and not strange_name:
    print(chalk.green('All specs passed checks'))


def parse_quoted(s):
  s = s.strip()
  if s.startswith("'"):
    s = s[1:]
  if s.endswith("'"):
    s = s[:-1]
  s1 = s
  return s

def scan_for_unit_tests(cdir, unit_tests):
  subfiles = os.listdir(cdir)
  files = []
  dirs = []
  for f in subfiles:
    path = os.path.join(cdir, f)
    if os.path.isfile(path) and path.endswith('.spec.ts'):
      files.append(path)
    elif os.path.isdir(path):
      dirs.append(path)
  files.sort()
  unit_tests.extend(files)

  dirs.sort()
  for d in dirs:
    scan_for_unit_tests(d, unit_tests)

class Reader():

  def __init__(self, file):
    self.lines =open(file).readlines()
    self.pos = 0

  def next(self):
    if self.pos < len(self.lines):
      self.pos += 1
      return self.lines[self.pos - 1]

  def has_next(self):
    return self.pos < len(self.lines)

  def put_back_after(self, position):
    last = self.lines[self.pos - 1]
    if position + 1 < len(last):
      self.lines[self.pos - 1] = self.lines[self.pos - 1][position:]
      # print('put back: %s' % self.lines[self.pos - 1], end='')
    else:
      self.lines[self.pos - 1] = ''
      # print('put back nothing')
    self.pos -= 1

  def get_pos(self):
    return self.pos

class BadBlockException(Exception):

  def __init__(self, msg):
    super().__init__(self, msg)

def analyze_imports_block(r) -> List[str]:
  contents = []

  def parse(import_block):
    imports = [parse_quoted(l) for l in import_block.split(',') if l.strip()]
    return imports

  bracket_count = 1
  while r.has_next():
    line = r.next()
    for i, c in enumerate(line):
      if c == '[':
        bracket_count += 1
      elif c == ']':
        bracket_count -= 1
      if bracket_count == 0:
        r.put_back_after(i + 1)
        return parse(''.join(contents))
      else:
        contents.append(c)
  raise BadBlockException('Import end never found')

def analyze_testbed_block(r) -> List[str]:
  imports = None
  paren_count = 1
  BLOCK = 'imports: ['
  while r.has_next():
    l = r.next()
    # print('  testbed search line  : %s' % l, end='')
    if l.find(BLOCK) != -1:
      end = l.find(BLOCK) + len(BLOCK)
      r.put_back_after(end)
      imports = analyze_imports_block(r)
    else:
      for i, c in enumerate(l):
        if c == '(':
          paren_count += 1
        elif c == ')':
          paren_count -= 1
        if paren_count == 0:
          r.put_back_after(i + 1)
          return imports
  raise BadBlockException('Block end never found')

def analyze_describe_block(r) -> List[List[str]]:
  testbeds = []
  paren_count = 1
  pattern = '^.*TestBed.configureTestingModule\\s*\\('
  while r.has_next():
    line = r.next()
    if m := re.search(pattern, line):
      r.put_back_after(len(m.group(0)))
      testbeds.append(analyze_testbed_block(r))
    else:
      for i, c in enumerate(line):
        if c == '(':
          paren_count += 1
        elif c == ')':
          paren_count -= 1
        if paren_count == 0:
          r.put_back_after(i + 1)
          return testbeds
  raise BadBlockException('Describe end never found')


def analyze_spec_file(file) -> List[Tuple[int, str, List[List[str]]]]:
  r = Reader(file)
  describe_blocks = []
  pattern = '^describe\\s*\\(\\s*\'([^\']+)\','
  while r.has_next():
    line = r.next()
    if m := re.search(pattern, line):
      line_num = r.get_pos()
      r.put_back_after(len(m.group(0)))
      name = parse_quoted(m.group(1))
      try:
        testbeds = analyze_describe_block(r)
        describe_blocks.append((line_num, name, testbeds))
      except BadBlockException as ex:
        print('Error in file: "%s"' % file)
        print('  bad describe block "%s" starting on line number %d' % (name, line_num))
        print('  cause: %s' % str(ex))
        return
  return describe_blocks

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--dir', type=str, default='src', help='directory to scan for karma unit tests to analyze')

  args = parser.parse_args()


  go(args.dir)
