const configFn = require('./karma.conf.js');

// Took 5.7 seconds from cold start

module.exports = function (config) {
  const configs = configFn(config);
  configs.frameworks.splice(0, 0, 'parallel'); // has to be loaded first
  configs.plugins.splice(1, 0, require('karma-parallel'));
  config.set(configs);
};
