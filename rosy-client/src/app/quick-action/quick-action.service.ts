import {Injectable} from '@angular/core';

import {Action, QUICK_ACTION_ACTIONS, } from '@app/models';
import {ActionService} from '@app/services';

import {
  DEFAULT_QUICK_ACTION_MODE,
  DEFAULT_QUICK_ACTION_TYPE,
  QuickAction,
} from './quick-action.model';


@Injectable({
  providedIn: 'root'
})
export class QuickActionService {

  protected quickActions: Map<Action, QuickAction>;

  constructor(protected actionService: ActionService) {
    this.quickActions = new Map();
    QUICK_ACTION_ACTIONS.forEach(action => {
      const entry = actionService.getRegisteredAction(action);
      this.quickActions.set(action, {
        binding: entry.binding,
        mode: DEFAULT_QUICK_ACTION_MODE,
        target: {
          type: DEFAULT_QUICK_ACTION_TYPE,
          item: null
        }
      });
      // Update quick action binding field whenever the action's key binding changes.
      entry.bindingChanged$.subscribe(() => {
        this.getQuickAction(action).binding = entry.binding;
      });
    });
  }

  getQuickAction(action: Action): QuickAction {
    const qa = this.quickActions.get(action);
    if (qa) {
      return qa;
    } else {
      throw new Error('this should never happen');
    }
  }

}
