import {Injectable} from '@angular/core';

import {Action} from '@app/models';
import {ActionService} from '@app/services';
import {DialogService} from '@app/shared/dialog';

import {QuickActionDialogComponent} from './quick-action-dialog.component';


@Injectable({
  providedIn: 'root'
})
export class QuickActionDialogService {

  constructor(
    dialog: DialogService,
    actionService: ActionService
  ){
    actionService.captureAction(Action.ShowQuickActions).subscribe(() => {
      dialog.open(QuickActionDialogComponent, {});
    });
  }

}
