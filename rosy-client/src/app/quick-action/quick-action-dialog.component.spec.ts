import {CdkTableModule} from '@angular/cdk/table';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Subject} from 'rxjs';

import {ApiService, LibraryService, NotificationService, } from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {PipesModule} from '@app/shared/pipes';

import {QuickActionDialogComponent, QuickActionModule, } from '.';


describe('QuickActionDialogComponent', () => {
  let component: QuickActionDialogComponent;
  let fixture: ComponentFixture<QuickActionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        QuickActionDialogComponent,
      ],
      imports: [
        CdkTableModule,
        MatDialogModule,
        MatSortModule,
        MatTableModule,
        NoopAnimationsModule,
        PipesModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
   })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickActionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
