import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';

import {AutoInputModule} from '@app/shared/auto-input';
import {DialogModule} from '@app/shared/dialog';
import {EditableModule} from '@app/shared/editable';
import {PipesModule} from '@app/shared/pipes';

import {QuickActionDialogComponent, QuickActionModesPipe, } from './quick-action-dialog.component';


@NgModule({
  declarations: [
    QuickActionDialogComponent,
    QuickActionModesPipe,
  ],
  exports: [
    QuickActionDialogComponent,
  ],
  imports: [
    AutoInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    DialogModule,
    EditableModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatTableModule,
    PipesModule,
  ]
})
export class QuickActionModule { }
