import {M} from '@angular/cdk/keycodes';
import {TestBed} from '@angular/core/testing';

import {Action, ACTION_DATA, KeyBinding, QUICK_ACTION_ACTIONS, } from '@app/models';
import {ActionService} from '@app/services';

import {QuickActionService} from '.';


describe('QuickActionService', () => {
  let actionService: ActionService;
  let service: QuickActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuickActionService);
    actionService = TestBed.inject(ActionService);
  });

  it('should define a quick action for each of the application-level quick actions', () => {
    QUICK_ACTION_ACTIONS.forEach(action => {
      const qa = service.getQuickAction(action);
      expect(qa).toBeTruthy();
      expect(qa.binding).toEqual(actionService.getRegisteredAction(action).binding);
    });
  });

  it('should update the local key binding whenever a quick action is rebound', () => {
    const action = QUICK_ACTION_ACTIONS[1];
    const newBinding: KeyBinding = new KeyBinding(M, {altKey: true, ctrlKey: true});
    const oldBinding = actionService.getRegisteredAction(action).binding;

    expect(service.getQuickAction(action).binding).toEqual(oldBinding);
    actionService.setActionKeyBinding(action, newBinding);
    expect(service.getQuickAction(action).binding).toEqual(newBinding);
  });

});
