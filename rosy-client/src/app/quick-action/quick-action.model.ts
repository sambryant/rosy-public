import {ContentMeta, ContentType, KeyBinding, OperationMode, } from '@app/models';

export const DEFAULT_QUICK_ACTION_TYPE = 'tag';

export const DEFAULT_QUICK_ACTION_MODE = 'toggle';

export interface QuickAction {
  target: ContentMeta | ({type: ContentType} & {item: null});
  binding: KeyBinding | null;
  mode: OperationMode;
}
