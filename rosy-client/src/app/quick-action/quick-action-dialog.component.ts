import {takeUntil} from 'rxjs/operators';
import {
  Component,
  Pipe,
  PipeTransform,
} from '@angular/core';

import {Action, ContentType, CONTENT_TYPES, QUICK_ACTION_ACTIONS, } from '@app/models';
import {ActionService} from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

import {OperationMode, OPERATION_MODES, } from '@app/models';
import {QuickAction} from './quick-action.model';
import {QuickActionService} from './quick-action.service';


@Component({
  selector: 'rx-quick-action-dialog',
  templateUrl: './quick-action-dialog.component.html',
  styleUrls: ['./quick-action-dialog.component.scss']
})
export class QuickActionDialogComponent
extends UnsubscribeOnDestroyedDirective {

  readonly DISPLAYED_COLUMNS = ['label', 'mode', 'type', 'value'];

  readonly CONTENT_TYPES = CONTENT_TYPES;

  data: QuickAction[];

  constructor(
    private actionService: ActionService,
    private dialogService: DialogService,
    public quickActionService: QuickActionService) {
    super();
    this.data = QUICK_ACTION_ACTIONS.map(action => quickActionService.getQuickAction(action));
  }

  onTypeChanged(action: QuickAction) {
    action.target.item = null;

    // Feature quick-actions only support 'adding'.
    if (action.target.type === 'feature') {
      action.mode = 'add';
    }
  }

}

/**
 * Pipe which gives availiable modes (e.g. 'add', 'remove') for each content type.
 *
 * Currently all types (e.g. 'tag', 'name') support all modes, except for 'feature' which only
 * supports 'add'.
 */
@Pipe({
  name: 'quickActionModes'
})
export class QuickActionModesPipe implements PipeTransform {

  transform(type: ContentType): OperationMode[] {
    if (type === 'feature') {
      return ['add'];
    } else {
      return OPERATION_MODES;
    }
  }

}
