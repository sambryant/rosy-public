import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule} from '@angular/material/core';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {AppHeaderButtonsModule} from './app-header-buttons';
import {ActionButtonModule} from '@app/shared/action-button';
import {AdminModule} from './admin';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AuthInterceptor} from './services';
import {ImageDetailsModule} from './image-details';
import {LoginModule} from './login';
import {PagesModule} from './pages';
import {QuickActionModule} from './quick-action';
import {ServicesModule} from './services';
import {SidenavModule} from './sidenav';
import {UploadModule} from './upload';
import {ViewModeSelectorModule} from '@app/shared/view-mode-selector';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    ActionButtonModule,
    AdminModule,
    AppHeaderButtonsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatToolbarModule,
    PagesModule,
    QuickActionModule,
    ReactiveFormsModule,
    RouterModule,
    ServicesModule,
    SidenavModule,
    UploadModule,
    ViewModeSelectorModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
