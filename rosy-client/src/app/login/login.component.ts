import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router, } from '@angular/router';
import {Component, OnDestroy, OnInit, } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators, } from '@angular/forms';
import {Subject} from 'rxjs';

import {AuthService, ConfigService, } from '@app/services';
import {HOME_PAGE_URL} from '@app/app-page.model';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  _forms: { username: FormControl, password: FormControl, schema: FormControl } = {
    username: new FormControl('', [Validators.required, Validators.maxLength(24)]),
    password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(24)]),
    schema: new FormControl('', [Validators.minLength(1)]),
  };

  _form: FormGroup;

  protected returnUrl: string;

  constructor(
    protected authService: AuthService,
    protected configService: ConfigService,
    protected router: Router,
    protected route: ActivatedRoute,
  ) {
    super();
    this._form = new FormGroup(this._forms);
  }

  ngOnInit() {
    if (this.route.snapshot.queryParams.hasOwnProperty('returnUrl')) {
      this.returnUrl = decodeURI(this.route.snapshot.queryParams['returnUrl']);
    } else {
      this.returnUrl = HOME_PAGE_URL;
    }
    if (this.authService.checkLoggedIn()) {
      this.router.navigateByUrl(this.returnUrl);
    }
  }

  _doLogin() {
    if (this._form.valid) {
      this.authService.login(this._form.value.username, this._form.value.password, this._form.value.schema || null)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.router.navigateByUrl(this.returnUrl);
        }, err => {
          this._form.get('password')!.setValue('');
          if (err && err.error && err.error.message) {
            this._form.setErrors({server: err.error.message});
          } else {
            this._form.setErrors({server: 'Unknown server error'});
          }
        });
    }
  }
}
