import {
  fakeAsync,
  tick,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';
import {ActivatedRoute, Router} from '@angular/router';
import {Component, CUSTOM_ELEMENTS_SCHEMA, } from '@angular/core';
import {Location} from '@angular/common';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {Subject} from 'rxjs';

import {ApiService, AuthService, } from '@app/services';
import {HOME_PAGE, HOME_PAGE_URL, } from '@app/app-page.model';
import {LoginModule} from './login.module';
import {LoginComponent} from './login.component';


@Component({
  template: '',
})
class TestRedirectComponent {}


describe('LoginComponent', () => {
  const TEST_RETURN_URL_PATH = 'test/route';

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let location: Location;
  let authLoginSpy: jasmine.Spy;
  let authCheckLoggedInSpy: jasmine.Spy;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        LoginModule,
        NoopAnimationsModule,
        RouterTestingModule.withRoutes([ // [5]
          {path: HOME_PAGE.link, component: TestRedirectComponent},
          {path: TEST_RETURN_URL_PATH, component: TestRedirectComponent},
        ]),
      ],
      declarations: [LoginComponent],
      providers: [{provide: ApiService, useValue: {}}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ] // Ignores component not in imports, declarations, or providers
    })
    .compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    const authService = TestBed.inject(AuthService);
    authLoginSpy = spyOn(authService, 'login');
    authCheckLoggedInSpy = spyOn(authService, 'checkLoggedIn');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
  });

  it('should automatically redirect if user is already logged in', fakeAsync(() => {
    fixture.ngZone!.run(() => {
      authCheckLoggedInSpy.and.returnValue(true);
      fixture.detectChanges();
      tick();
      expect(location.path()).toBe(HOME_PAGE_URL);
    });
  }));

  it('should automatically redirect to redirect URL if user is already logged in', fakeAsync(() => {
    fixture.ngZone!.run(() => {
      router.navigate([], {
        queryParams: {
          returnUrl: TEST_RETURN_URL_PATH
        }
      });
      authCheckLoggedInSpy.and.returnValue(true);
      tick();
      fixture.detectChanges();
      tick();
      expect(location.path()).toBe('/' + TEST_RETURN_URL_PATH);
    });
  }));

  describe('login behavior', () => {
    let mockLoginResponse: Subject<null>;

    beforeEach(() => {
      authCheckLoggedInSpy.and.returnValue(false);
      fixture.detectChanges();

      mockLoginResponse = new Subject();
      authLoginSpy.and.returnValue(mockLoginResponse);
    });

    it('should redirect after a successful login', fakeAsync(() => {
      component._forms.username.setValue('ValidUsername');
      component._forms.password.setValue('ValidPassword');

      fixture.ngZone!.run(() => {
        component._doLogin();
        tick();

        expect(authLoginSpy).toHaveBeenCalledWith('ValidUsername', 'ValidPassword', null);
        expect(location.path()).toBe('');

        mockLoginResponse.next(null);
        tick();

        expect(location.path()).toBe(HOME_PAGE_URL);
      });
    }));

    it('should not redirect and display message after a failed login', fakeAsync(() => {
      const errorResponse = {
        error: {
          message: 'Message from server'
        }
      };
      const expectedError = 'Message from server';
      component._forms.username.setValue('ValidUsername');
      component._forms.password.setValue('InvalidPassword');

      fixture.ngZone!.run(() => {
        component._doLogin();
        mockLoginResponse.error(errorResponse);
        tick();

        expect(location.path()).toBe('');
        expect(authLoginSpy).toHaveBeenCalledWith('ValidUsername', 'InvalidPassword', null);
        expect(component._form.getError('server')).toEqual(expectedError);
      });
    }));

    it('should show generic error message on unknown login error', fakeAsync(() => {
      const errorResponse = {};
      const expectedError = 'Unknown server error';
      component._forms.username.setValue('ValidUsername');
      component._forms.password.setValue('InvalidPassword');

      fixture.ngZone!.run(() => {
        component._doLogin();
        mockLoginResponse.error(errorResponse);
        tick();

        expect(location.path()).toBe('');
        expect(authLoginSpy).toHaveBeenCalledWith('ValidUsername', 'InvalidPassword', null);
        expect(component._form.getError('server')).toEqual(expectedError);
      });
    }));

    it('should login with schema if provided', fakeAsync(() => {
      component._forms.username.setValue('ValidUsername');
      component._forms.password.setValue('ValidPassword');
      component._forms.schema.setValue('schema');

      fixture.ngZone!.run(() => {
        component._doLogin();
        expect(authLoginSpy).toHaveBeenCalledWith('ValidUsername', 'ValidPassword', 'schema');
      });
    }));
  });
});
