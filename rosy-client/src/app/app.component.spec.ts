import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MockComponent, MockProvider, } from 'ng-mocks';
import {MatMenu, MatMenuTrigger, } from '@angular/material/menu';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';

import {DialogService} from '@app/shared/dialog';
import {LibraryService, NotificationService, } from '@app/services';

import {AppModule} from './app.module';
import {AppComponent} from './app.component';



describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockComponent(MatMenu),
        MockComponent(MatMenuTrigger),
      ],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        MatRadioModule,
        MatSlideToggleModule,
        NoopAnimationsModule,
        RouterTestingModule,
      ],
      providers: [
        MockProvider(DialogService),
        MockProvider(LibraryService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

});
