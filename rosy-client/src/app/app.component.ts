import {
  Component,
  HostListener,
  Input,
  OnInit,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';

import {
  ActionService,
  AuthService,
  ConfigService,
  LoggerService,
} from '@app/services';
import {Action} from '@app/models';
import {APP_PAGES, HOME_PAGE, LOGIN_PAGE, } from '@app/app-page.model';
import {DialogService} from '@app/shared/dialog';
import {QuickActionDialogService} from '@app/quick-action';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  readonly ActionLogIn = Action.LogIn;

  readonly ActionLogOut = Action.LogOut;

  readonly HOME_PAGE = HOME_PAGE;

  readonly INBOX_PAGE = APP_PAGES.inbox;

  loggedIn: boolean;

  menuActions: Action[] = [
    Action.ShowKeyBindings,
    Action.ShowQuickActions,
    Action.SetDatabaseSchema,
  ];

  @HostListener('touchstart')
  _onTouch(): void {
    if (!this.configService.mobileMode$.value) {
      this.logger.warn('This is a bad method of detecting mobile');
      this.configService.mobileMode$.next(true);
    }
  }

  constructor(
    quickActionDialogService: QuickActionDialogService, // need to inject otherwise its never constructed
    protected actionService: ActionService,
    protected authService: AuthService,
    public configService: ConfigService,
    protected dialogService: DialogService,
    protected logger: LoggerService,
    protected router: Router,
  ) {
    super();
    this.authService.login$.subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });

  }

  ngOnInit(): void {
    this.actionService.captureAction(Action.SetDatabaseSchema)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.dialogService.showInput('Choose database schema', '')
          .pipe(takeUntil(this.destroyed$))
          .subscribe(response => {
            if (response) {
              this.configService.setDatabaseSchema(response);
            }
          });
      });
    this.actionService.captureAction(Action.LogOut)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.authService.logout()
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            LOGIN_PAGE.navigate(this.router);
          });
      });
    this.actionService.captureAction(Action.LogIn)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        LOGIN_PAGE.navigate(this.router);
      });
  }

}
