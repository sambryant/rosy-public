import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnInit,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';

import {ConfigService} from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

@Component({
  selector: 'rx-show-image-features-button',
  templateUrl: './show-image-features-button.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShowImageFeaturesButtonComponent extends UnsubscribeOnDestroyedDirective implements OnInit {

  showImageFeatures: boolean = false;

  constructor(
    protected cdr: ChangeDetectorRef,
    protected configService: ConfigService,
  ) {
    super();
  }

  toggle() {
    this.configService.showImageFeatures$.next(!this.configService.showImageFeatures$.value);
  }

  ngOnInit(): void {
    this.configService.showImageFeatures$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((state) => {
        this.showImageFeatures = state;
        this.cdr.detectChanges();
      });
  }

}
