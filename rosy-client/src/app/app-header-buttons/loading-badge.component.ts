import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnInit,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';

import {LibraryService} from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-loading-badge',
  templateUrl: './loading-badge.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingBadgeComponent extends UnsubscribeOnDestroyedDirective implements OnInit {

  showLoadingBadge: boolean = false;

  constructor(
    protected cdr: ChangeDetectorRef,
    protected libraryService: LibraryService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.libraryService.requestInProgress$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((requestInProgress) => {
        this.showLoadingBadge = requestInProgress;
        this.cdr.detectChanges();
      });
  }

}
