import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgModule} from '@angular/core';

import {ServicesModule} from '@app/services';

import {LoadingBadgeComponent} from './loading-badge.component';
import {ShowImageFeaturesButtonComponent} from './show-image-features-button.component';


@NgModule({
  declarations: [
    LoadingBadgeComponent,
    ShowImageFeaturesButtonComponent,
  ],
  exports: [
    LoadingBadgeComponent,
    ShowImageFeaturesButtonComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    ServicesModule,
  ]
})
export class AppHeaderButtonsModule { }
