import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {
  ArtistPageComponent,
  EigendataPageComponent,
  GroupPageComponent,
  ImagePageComponent,
  NamePageComponent,
  TagPageComponent,
} from './pages';
import {AdminComponent} from './admin';
import {AppPage, APP_PAGES, HOME_PAGE_URL} from '@app/app-page.model';
import {AuthGuardService} from './services';
import {DupeCandidatesBrowserComponent} from './pages/browsers';
import {ImageDetailsComponent} from './image-details';
import {LoginComponent} from './login';
import {UploadComponent} from './upload';


const routes: Routes = [
  {
    path: '',
    redirectTo: HOME_PAGE_URL,
    pathMatch: 'full'
  },
  {
    path: APP_PAGES.dupes.link,
    component: DupeCandidatesBrowserComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.images.link,
    component: ImagePageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.groups.link,
    component: GroupPageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.artists.link,
    component: ArtistPageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.names.link,
    component: NamePageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.tags.link,
    component: TagPageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.metadata.link,
    component: EigendataPageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.login.link,
    component: LoginComponent
  },
  {
    path: APP_PAGES.image.link + '/:imageId',
    component: ImageDetailsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.upload.link,
    component: UploadComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: APP_PAGES.admin.link,
    component: AdminComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: HOME_PAGE_URL,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
