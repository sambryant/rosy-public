import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTabsModule} from '@angular/material/tabs';
import {NgModule} from '@angular/core';

import {ActionButtonModule} from '@app/shared/action-button';
import {PagesModule} from '@app/pages';
import {ServicesModule} from '@app/services';

import {AdminComponent} from './admin.component';
import {WorkerModule} from './worker';


@NgModule({
  declarations: [
    AdminComponent,
  ],
  exports: [
    AdminComponent
  ],
  imports: [
    ActionButtonModule,
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    PagesModule,
    ServicesModule,
    WorkerModule,
  ]
})
export class AdminModule { }
