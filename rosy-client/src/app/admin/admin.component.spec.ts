import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ApiService} from '@app/services';

import {AdminComponent, AdminModule, } from '.';


describe('AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AdminComponent
      ],
      imports: [
        NoopAnimationsModule,
      ],
      providers: [
        MockProvider(ApiService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have more unit tests', () => {
    expect(component).toBeTruthy();
    console.warn('Write more unit tests');
  });
});
