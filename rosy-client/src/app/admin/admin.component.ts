import {Component} from '@angular/core';

import {ApiService, ThreadApiService, } from '@app/services';
import {Action} from '@app/models';
import {DialogService} from '@app/shared/dialog';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

import {
  generateDatasetWorker,
  generateFeatureDatasetWorker,
  generateTagDatasetWorker,
  OptionDialogThreadedWorker,
  ThreadedWorker,
  Worker,
} from './worker';


@Component({
  selector: 'rx-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent
extends UnsubscribeOnDestroyedDirective {

  actionWorkers: Worker<any>[] = [];

  generateWorkers: OptionDialogThreadedWorker<any>[] = [];

  constructor(
    apiService: ApiService,
    dialogService: DialogService,
    threadApiService: ThreadApiService
  ) {
    super();
    this.actionWorkers = [
      new Worker(Action.AdminFixSequences, apiService.fixSequences()),
      new Worker(Action.AdminComputeMetadata, apiService.computeMetadata()),
      new Worker(Action.AdminGenerateDupeReport, apiService.generateDupeReport()),
      new ThreadedWorker(Action.AdminMergeDuplicates, apiService.mergeDuplicates(), threadApiService),
      new ThreadedWorker(Action.AdminRecomputeFilesizes, apiService.recomputeFilesizes(), threadApiService),
      new ThreadedWorker(Action.AdminRegenerateThumbnails, apiService.regenerateThumbnails(), threadApiService),
      new ThreadedWorker(Action.AdminReindexImages, apiService.reindexImages(true), threadApiService),
      new ThreadedWorker(Action.AdminResetFiles, apiService.resetFiles(), threadApiService),
    ];
    this.generateWorkers = [
      generateDatasetWorker(apiService, threadApiService, dialogService),
      generateFeatureDatasetWorker(apiService, threadApiService, dialogService),
      generateTagDatasetWorker(apiService, threadApiService, dialogService),
    ];
  }

}
