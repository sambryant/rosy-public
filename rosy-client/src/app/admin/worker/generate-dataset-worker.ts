import {flatMap} from 'rxjs/operators';
import {NEVER, Observable, } from 'rxjs';

import {Action} from '@app/models';
import {ApiService, ThreadApiService, ThreadStatusResponse, } from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {GenerateFeatureDatasetArgs, GenerateTagDatasetArgs} from '@app/models';

import {GenerateDatasetDialogComponent, GenerateFeatureDatasetDialogComponent} from './worker-dialogs';
import {OptionDialogThreadedWorker} from './worker';

export function generateDatasetWorker(
  apiService: ApiService,
  threadApiService: ThreadApiService,
  dialogService: DialogService): OptionDialogThreadedWorker<GenerateTagDatasetArgs> {
  return new OptionDialogThreadedWorker(
    apiService,
    dialogService,
    threadApiService,
    apiService.generateDataset.bind(apiService),
    GenerateDatasetDialogComponent,
    Action.AdminGenerateDataset
  );
}

export function generateFeatureDatasetWorker(
  apiService: ApiService,
  threadApiService: ThreadApiService,
  dialogService: DialogService): OptionDialogThreadedWorker<GenerateTagDatasetArgs> {
  return new OptionDialogThreadedWorker(
    apiService,
    dialogService,
    threadApiService,
    apiService.generateFeatureDataset.bind(apiService),
    GenerateFeatureDatasetDialogComponent,
    Action.AdminGenerateFeatureDataset
  );
}

export function generateTagDatasetWorker(
  apiService: ApiService,
  threadApiService: ThreadApiService,
  dialogService: DialogService): OptionDialogThreadedWorker<GenerateTagDatasetArgs> {
  return new OptionDialogThreadedWorker(
    apiService,
    dialogService,
    threadApiService,
    apiService.generateTagDataset.bind(apiService),
    GenerateDatasetDialogComponent,
    Action.AdminGenerateTagDataset
  );
}
