import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {NgModule} from '@angular/core';

import {ActionButtonModule} from '@app/shared/action-button';
import {ServicesModule} from '@app/services';

import {WorkerDialogsModule} from './worker-dialogs';
import {WorkerStatusComponent} from './worker-status.component';


@NgModule({
  declarations: [
    WorkerStatusComponent
  ],
  exports: [
    WorkerStatusComponent
  ],
  imports: [
    ActionButtonModule,
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    ServicesModule,
    WorkerDialogsModule,
  ]
})
export class WorkerModule { }
