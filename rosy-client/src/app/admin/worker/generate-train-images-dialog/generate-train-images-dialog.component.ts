import {Component, Inject, OnInit, } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

import {GenerateTrainImagesArgs} from '@app/models';


@Component({
  selector: 'rx-generate-train-images-dialog',
  templateUrl: './generate-train-images-dialog.component.html',
  styleUrls: ['./generate-train-images-dialog.component.scss']
})
export class GenerateTrainImagesDialogComponent {

  readonly trainSizes = [16, 32, 64, 128];

  public data: GenerateTrainImagesArgs = {
    datasetName: null,
    useExistingImages: false,
    includeHidden: false,
    includeUnprocessed: false,
    trainSize: 32,
  };

  constructor(
    public dialogRef: MatDialogRef<GenerateTrainImagesDialogComponent>,
  ) {}

}
