import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {NgModule} from '@angular/core';

import {ServicesModule} from '@app/services';

import {GenerateTrainImagesDialogComponent} from './generate-train-images-dialog.component';


@NgModule({
  declarations: [
    GenerateTrainImagesDialogComponent,
  ],
  exports: [
    GenerateTrainImagesDialogComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    ServicesModule,
  ]
})
export class GenerateTrainImagesDialogModule { }