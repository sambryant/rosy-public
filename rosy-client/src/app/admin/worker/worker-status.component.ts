import {catchError, takeUntil, } from 'rxjs/operators';
import {of, Subject, } from 'rxjs';
import {Component, Input, OnDestroy, OnInit, } from '@angular/core';

import {ActionService, NotificationService, } from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

import {Worker} from './worker';


@Component({
  selector: 'rx-worker-status',
  templateUrl: './worker-status.component.html',
  styleUrls: ['./worker-status.component.scss']
})
export class WorkerStatusComponent
extends UnsubscribeOnDestroyedDirective
implements OnDestroy, OnInit {

  @Input() worker: Worker<any>;

  constructor(private actionService: ActionService, private notificationService: NotificationService)
  {
    super();
  }

  ngOnInit() {
    this.actionService.captureAction(this.worker.action, this.worker.ready)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.worker.start()
          .pipe(takeUntil(this.destroyed$))
          .pipe(catchError((msg: string) => {
            msg = msg || 'Unknown error';
            this.notificationService.error('Job failed: ' + msg);
            return of(msg);
          })).subscribe();
      });
  }

}
