import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {NgModule} from '@angular/core';

import {AutoInputModule} from '@app/shared/auto-input';
import {ChipModule} from '@app/shared/chip';
import {EditableModule} from '@app/shared/editable';
import {PipesModule} from '@app/shared/pipes';
import {ServicesModule} from '@app/services';

import {
  GenerateDatasetDialogComponent,
  GenerateFeatureDatasetDialogComponent,
} from './generate-dataset-dialog.component';


@NgModule({
  declarations: [
    GenerateDatasetDialogComponent,
    GenerateFeatureDatasetDialogComponent,
  ],
  exports: [
    GenerateDatasetDialogComponent,
    GenerateFeatureDatasetDialogComponent,
  ],
  imports: [
    AutoInputModule,
    BrowserModule,
    ChipModule,
    CommonModule,
    EditableModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    PipesModule,
    ServicesModule,
  ]
})
export class WorkerDialogsModule { }