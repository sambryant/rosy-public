import {Component, Inject, OnInit, } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

import {GenerateDatasetArgs, GenerateFeatureDatasetArgs, GenerateTagDatasetArgs} from '@app/models';


@Component({
  templateUrl: './generate-tag-dataset-dialog.component.html',
  styleUrls: ['./generate-dataset-dialog.component.scss']
})
export class GenerateDatasetDialogComponent {

  readonly trainSizes = [16, 32, 64, 128, 256];

  public data: GenerateDatasetArgs = {
    datasetName: null,
    useExistingImages: false,
    includeHidden: false,
    includeUnprocessed: false,
    trainSize: 32,
  };

  constructor(
    public dialogRef: MatDialogRef<GenerateDatasetDialogComponent>,
  ) {}

}

@Component({
  templateUrl: './generate-feature-dataset-dialog.component.html',
  styleUrls: ['./generate-dataset-dialog.component.scss']
})
export class GenerateFeatureDatasetDialogComponent {

  public data: GenerateFeatureDatasetArgs = {
    datasetName: null,
    useExistingImages: false,
    includeHidden: false,
    includeUnprocessed: false,
    features: [],
    excludeTags: [],
    includeTags: [],
    generateNullData: false,
    nullFeatures: [],
  };

  constructor(
    public dialogRef: MatDialogRef<GenerateFeatureDatasetDialogComponent>,
  ) {}

}
