import {flatMap, takeUntil, tap, } from 'rxjs/operators';
import {BehaviorSubject, NEVER, Observable, } from 'rxjs';

import {Action} from '@app/models';
import {ApiService, ThreadApiService, ThreadState, ThreadStatusResponse, } from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {GenerateFeatureDatasetArgs, GenerateTagDatasetArgs} from '@app/models';


export const PROGRESS_INTERVAL = 1000; // (ms) interval to update progress bar with query

export class Worker<T> {

  public progress: number | undefined = undefined; // percent job that is complete

  public incrementalUpdates = false; // true if job can give fractional progress updates

  public started = false;

  public state: ThreadState | null = null;

  public ready = new BehaviorSubject(true);

  constructor(public action: Action, protected request: Observable<T> | null) {}

  start(): Observable<T> {
    if (this.request) {
      if (this.state === 'active') {
        throw new Error('Job is already in progress');
      }
      this.started = true;
      this.state = 'active';
      this.ready.next(false);

      return this.request.pipe(tap({
        complete: () => {
          this.state = 'success';
          this.progress = 100.0;
          this.ready.next(true);
        },
        error: err => {
          this.state = 'failure';
          this.progress = 0.0;
          this.ready.next(true);
          const msg = err && err.error && err.error.message || 'Unknown error';
          throw msg;
        }
      }));
    } else {
      throw new Error('Cannot start worker: no request set');
    }
  }
}


export class ThreadedWorker extends Worker<ThreadStatusResponse> {
  constructor(
    action: Action,
    protected underlyingRequest: Observable<ThreadStatusResponse> | null,
    protected threadApiService: ThreadApiService,
  ) {
    super(action, null);
    this.progress = 0.0;
    this.incrementalUpdates = true;
  }

  override start(): Observable<ThreadStatusResponse> {
    if (this.underlyingRequest) {
      this.request = this.threadApiService.startThread(this.underlyingRequest, PROGRESS_INTERVAL);
      return super.start().pipe(tap(
        (response: ThreadStatusResponse) => {
          this.state = response.status.state;
          this.progress = 100.0 * (response.status.current / response.status.total);
          if (response.status.state === 'failure') {
            this.ready.next(true);
            throw response.status.message;
          }
        }
      ));
    } else {
      throw new Error('Cannot start worker: no request set');
    }
  }

}

export class OptionDialogThreadedWorker<T> extends ThreadedWorker {

  constructor(
    protected apiService: ApiService,
    protected dialogService,
    threadApiService: ThreadApiService,
    protected apiMethod: any,
    protected component: any,
    action: Action,
  ) {
    super(action, null, threadApiService);
  }

  protected openDialog(): Observable<T | null> {
    const ref = this.dialogService.open(this.component);
    return ref.afterClosed();
  }

  override start(): Observable<ThreadStatusResponse> {
    return this.openDialog()
      .pipe(flatMap((result) => {
        if (result) {
          this.underlyingRequest = this.apiMethod(result);
          return super.start();
        } else {
          return NEVER;
        }
      }));
  }

}

