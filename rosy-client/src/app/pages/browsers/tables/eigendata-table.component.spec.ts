import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ContentEigendata, EigendataSelection, } from '@app/models';
import {GraphModule} from '@app/shared/graph';

import {EigendataTableComponent, SelectionManager, } from '.';


describe('EigendataTableComponent', () => {
  let component: EigendataTableComponent;
  let fixture: ComponentFixture<EigendataTableComponent>;
  let selectionManager: SelectionManager<ContentEigendata, EigendataSelection>;
  let items: ContentEigendata[];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        EigendataTableComponent
      ],
      imports: [
        GraphModule,
        NoopAnimationsModule,
      ]
    })
    .compileComponents();
    selectionManager = new SelectionManager(EigendataSelection);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EigendataTableComponent);
    component = fixture.componentInstance;
    items = [
      {eigenvalue: 1.0, weights: [1.0], items: [{type: 'tag', id: 1, value: 'tag 1'}]},
      {eigenvalue: 0.5, weights: [1.0], items: [{type: 'name', id: 1, value: 'name 1'}]},
      {eigenvalue: 0.2, weights: [1.0], items: [{type: 'tag', id: 3, value: 'tag 3'}]},
    ];
    component.selectionManager = selectionManager;
    fixture.detectChanges();
  });

  it('should load items from selectionManager', () => {
    expect(component.items).toEqual([]);
    selectionManager.setItems(items);
    expect(component.items).toEqual(items);
  });

  it('should mark all rows as expanded initially', () => {
    expect(component.rowExpanded).toEqual([]);
    selectionManager.setItems(items);
    expect(component.rowExpanded).toEqual([true, true, true]);
  });

  it('should toggle a row', () => {
    selectionManager.setItems(items);
    expect(component.rowExpanded).toEqual([true, true, true]);
    component.toggleRow(1);
    expect(component.rowExpanded).toEqual([true, false, true]);
    component.toggleRow(1);
    expect(component.rowExpanded).toEqual([true, true, true]);
  });

  it('should toggle all rows', () => {
    selectionManager.setItems(items);
    component.toggleRow(1);
    component.toggleRow(2);
    expect(component.rowExpanded).toEqual([true, false, false]);
    component.toggleAll(true);
    expect(component.rowExpanded).toEqual([true, true, true]);
    component.toggleAll(false);
    expect(component.rowExpanded).toEqual([false, false, false]);
  });
});
