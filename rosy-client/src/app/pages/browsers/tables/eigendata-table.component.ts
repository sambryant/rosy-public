import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
} from '@angular/core';

import {ContentEigendata} from '@app/models';

import {BaseTableDirective} from './base-table.directive';


@Component({
  selector: 'rx-eigendata-table',
  templateUrl: './eigendata-table.component.html',
  styleUrls: ['./eigendata-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EigendataTableComponent
extends BaseTableDirective<ContentEigendata> {

  rowExpanded: boolean[] = [];

  constructor(
    cdr: ChangeDetectorRef,
  ) {
    super(cdr);
  }

  protected override onItemsChanged() {
    super.onItemsChanged();
    this.rowExpanded = this.items.map(_ => true);
  }

  toggleRow(itemIndex: number) {
    this.rowExpanded[itemIndex] = !this.rowExpanded[itemIndex];
    this.cdr.markForCheck();
  }

  toggleAll(expanded: boolean) {
    this.items.forEach((_, i) => {
      this.rowExpanded[i] = expanded;
    });
    this.cdr.markForCheck();
  }

}
