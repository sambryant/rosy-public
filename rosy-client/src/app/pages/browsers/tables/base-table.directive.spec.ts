import {fakeAsync, tick, ComponentFixture, TestBed, } from '@angular/core/testing';
import {ChangeDetectionStrategy, Component, } from '@angular/core';
import {By} from '@angular/platform-browser';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Subject} from 'rxjs';

import {NavigationRequest, Selection, } from '@app/models';

import {BaseTableDirective, SelectionManager, } from '.';


export class Item {
  id: number;
  text: string;
  constructor(id: number) {
    this.id = id;
    this.text = `item ${id}`;
  }
}


export class ItemSelection extends Selection<Item> {}


@Component({
  selector: 'rx-test-table',
  template: `
    <div *ngFor="let item of items; let i = index"
         class="item"
         [ngClass]="{'selected': rowSelected[i]}">
      {{item.id}}
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush, // important! otherwise many of these tests are trivial!
})
export class TestTableComponent extends BaseTableDirective<Item> {}


describe('BaseTableDirective', () => {
  let component: TestTableComponent;
  let fixture: ComponentFixture<TestTableComponent>;

  const items = [new Item(1), new Item(2), new Item(3)];
  let selectionManager: SelectionManager<Item, ItemSelection>;
  let selectionChangeSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TestTableComponent
      ],
      imports: [
        NoopAnimationsModule,
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTableComponent);
    selectionManager = new SelectionManager(ItemSelection);
    selectionChangeSpy = spyOn(selectionManager.selectionChange$, 'emit').and.callThrough();
    component = fixture.componentInstance;
    component.selectionManager = selectionManager;
    fixture.detectChanges();
  });

  function expectSelection(fromItems: Item[], selected: number[], cursor: number | null = null) {
    expect(selectionManager.getCursorIndex()).toBe(cursor);
    expect(selectionManager.getSelectedItems()).toEqual(selected.map(i => fromItems[i]));
    expect(selectionManager.hasSelection()).toBe(selected.length !== 0);
    const expectedSelected = fromItems.map(_ => false);
    selected.forEach(i => expectedSelected[i] = true);
    expect(component.rowSelected).toEqual(expectedSelected);
  }

  describe('view features', () => {

    function getSelectedViewElements(): string[] {
      return fixture.debugElement
        .queryAll(By.css('.item'))
        .filter(el => el.classes.hasOwnProperty('selected'))
        .map(el => el.nativeElement.innerText);
    }

    function getViewElements(): string[] {
      return fixture.debugElement
        .queryAll(By.css('.item'))
        .map(el => el.nativeElement.innerText);
    }

    it('should update after changing items', () => {
      expect(getViewElements().length).toBe(0);

      selectionManager.setItems(items);
      fixture.detectChanges();
      expect(getViewElements().length).toBe(3);

      selectionManager.setItems([]);
      fixture.detectChanges();
      expect(getViewElements().length).toBe(0);
    });

    it('should update selection after changing items', () => {
      selectionManager.setItems(items);
      selectionManager.setSelection(1, false);
      fixture.detectChanges();
      expect(getSelectedViewElements()).toEqual(['2']);

      selectionManager.setItems([new Item(4), new Item(5)]);
      fixture.detectChanges();
      expect(getSelectedViewElements()).toEqual([]);

      selectionManager.setSelection(0, true);
      fixture.detectChanges();
      expect(getSelectedViewElements()).toEqual(['4']);
    });

    it('should update after selecting an item', () => {
      selectionManager.setItems(items);
      selectionManager.setSelection(0, false);
      fixture.detectChanges();

      expect(getSelectedViewElements()).toEqual(['1']);
      selectionManager.setSelection(1, false);
      fixture.detectChanges();

      expect(getSelectedViewElements()).toEqual(['2']);
    });

    it('should update after extend-selecting an item', () => {
      selectionManager.setItems(items);
      selectionManager.setSelection(0, false);
      fixture.detectChanges();

      expect(getSelectedViewElements()).toEqual(['1']);
      selectionManager.setSelection(1, true);
      fixture.detectChanges();

      expect(getSelectedViewElements()).toEqual(['1', '2']);
    });

    it('should update after navigating', () => {
      selectionManager.setItems(items);
      selectionManager.setSelection(0, false);
      fixture.detectChanges();
      expect(getSelectedViewElements()).toEqual(['1']);

      const requests: NavigationRequest[] = [
        {direction: 'next', extend: false},
        {direction: 'next', extend: false},
        {direction: 'next', extend: false},
        {direction: 'prev', extend: false},
        {direction: 'prev', extend: false},
        {direction: 'prev', extend: false},
      ];
      const expectedSelected = [ ['2'], ['3'], ['3'], ['2'], ['1'], ['1'] ];
      requests.forEach((req, i) => {
        selectionManager.navigate(req);
        fixture.detectChanges();
        expect(getSelectedViewElements()).toEqual(expectedSelected[i]);
      });
    });

    it('should update after extend-navigating', () => {
      selectionManager.setItems(items);
      selectionManager.setSelection(1, false);
      fixture.detectChanges();
      expect(getSelectedViewElements()).toEqual(['2']);

      const tests: [NavigationRequest, string[]][] = [
        [{direction: 'next', extend: true}, ['2', '3']],
        [{direction: 'next', extend: true}, ['2', '3']],
        [{direction: 'prev', extend: true}, ['2']],
        [{direction: 'prev', extend: true}, ['1', '2']],
        [{direction: 'prev', extend: true}, ['1', '2']],
      ];
      tests.forEach(([request, expected]) => {
        selectionManager.navigate(request);
        fixture.detectChanges();
        expect(getSelectedViewElements()).toEqual(expected);
      });
    });

    it('should update after deselecting all', () => {
      selectionManager.setItems(items);
      selectionManager.setSelection(0, false);
      selectionManager.setSelection(1, true);
      fixture.detectChanges();
      expect(getSelectedViewElements()).toEqual(['1', '2']);

      selectionManager.deselectAll();
      fixture.detectChanges();
      expect(getSelectedViewElements()).toEqual([]);
    });
  });

});
