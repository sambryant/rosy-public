import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CdkTableModule} from '@angular/cdk/table';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatSortModule} from '@angular/material/sort';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgModule} from '@angular/core';

import {ChipModule} from '@app/shared/chip';
import {ContextMenuModule} from '@app/shared/context-menu';
import {GraphModule} from '@app/shared/graph';
import {ImageModule} from '@app/shared/image';
import {PipesModule} from '@app/shared/pipes';
import {ServicesModule} from '@app/services';

import {
  ArtistTableComponent,
  ImageTableComponent,
  NameTableComponent,
  TagTableComponent,
} from './tables.component';
import {EigendataTableComponent} from './eigendata-table.component';
import {GroupTableComponent, GroupTableHeaderComponent, } from './group-table.component';


@NgModule({
  declarations: [
    ArtistTableComponent,
    EigendataTableComponent,
    GroupTableComponent,
    GroupTableHeaderComponent,
    ImageTableComponent,
    NameTableComponent,
    TagTableComponent,
  ],
  exports: [
    ArtistTableComponent,
    EigendataTableComponent,
    GroupTableComponent,
    GroupTableHeaderComponent,
    ImageTableComponent,
    NameTableComponent,
    TagTableComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    CdkTableModule,
    ChipModule,
    CommonModule,
    ContextMenuModule,
    DragDropModule,
    FormsModule,
    GraphModule,
    MatButtonModule,
    MatGridListModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTooltipModule,
    ImageModule,
    PipesModule,
    ServicesModule,
  ]
})
export class TablesModule { }
