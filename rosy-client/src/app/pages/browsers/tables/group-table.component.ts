import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Sort} from '@angular/material/sort';

import {
  Group,
  Image,
  ImageSelection,
} from '@app/models';
import {LibraryService} from '@app/services';

import {BaseTableDirective} from './base-table.directive';
import {ImageColumnType} from './tables.component';
import {SelectionManager} from './selection-manager';


@Component({
  selector: 'rx-group-table',
  templateUrl: './group-table.component.html',
  styleUrls: ['./base-table.scss', './group-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupTableComponent
extends BaseTableDirective<Group>
implements OnInit
{

  @Input() mode: 'list' | 'thumbnails';

  @Input() imageSelectionManagers: SelectionManager<Image, ImageSelection>[];

  rowExpanded: boolean[] = [];

  readonly DEFAULT_COLUMNS: ImageColumnType[] = [ 'id', 'favorite', 'processed', 'tags', 'names' ];

  constructor(
    cdr: ChangeDetectorRef,
    protected libraryService: LibraryService,
  ) {
    super(cdr);
  }

  override ngOnInit() {
    super.ngOnInit();

    this.libraryService.contentUpdated$.group
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.cdr.detectChanges();
      });
  }

  protected override onItemsChanged() {
    super.onItemsChanged();
    this.rowExpanded = this.items.map(_ => true);
  }

  isRowExpanded(groupIndex: number): boolean {
    return this.rowExpanded[groupIndex];
  }

  toggleGroup(groupIndex: number) {
    this.rowExpanded[groupIndex] = !this.rowExpanded[groupIndex];
  }

}

@Component({
  selector: 'rx-group-table-header',
  templateUrl: './group-table-header.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupTableHeaderComponent {

  @Input() sort: Sort | null = null;

  @Output() sortChange = new EventEmitter<Sort>();

  constructor(protected cdr: ChangeDetectorRef) {}

}
