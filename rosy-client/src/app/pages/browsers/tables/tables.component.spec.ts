import {CdkTableModule} from '@angular/cdk/table';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Subject} from 'rxjs';

import {
  ArtistDetails,
  ArtistId,
  ArtistSelection,
  Image,
  ImageId,
  ImageSelection,
  NameDetails,
  NameId,
  NameSelection,
  TagDetails,
  TagId,
  TagSelection,
} from '@app/models';
import {ApiService, LibraryService, NotificationService, } from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {MockArtistD, MockImage, MockNameD, MockTagD, } from '@app/models/testing';
import {PipesModule} from '@app/shared/pipes';

import {
  ArtistColumnType,
  ArtistTableComponent,
  ImageColumnType,
  ImageTableComponent,
  NameColumnType,
  NameTableComponent,
  SelectionManager,
  TagColumnType,
  TagTableComponent,
} from '.';


describe('ContentTableComponents', () => {
  let libraryService: LibraryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ArtistTableComponent,
        ImageTableComponent,
        NameTableComponent,
        TagTableComponent,
      ],
      imports: [
        CdkTableModule,
        MatSortModule,
        MatTooltipModule,
        NoopAnimationsModule,
        PipesModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
    libraryService = TestBed.inject(LibraryService);
    spyOn(libraryService, 'getContentValueById').and.returnValue(new Subject());
  });

  describe('ArtistTableComponent', () => {
    let component: ArtistTableComponent;
    let fixture: ComponentFixture<ArtistTableComponent>;
    let selectionManager: SelectionManager<ArtistDetails, ArtistSelection>;
    let selectionChangeSpy: jasmine.Spy;

    let items: ArtistDetails[];

    beforeEach(() => {
      fixture = TestBed.createComponent(ArtistTableComponent);
      component = fixture.componentInstance;
    });

    beforeEach(() => {
      items  = [MockArtistD.new(1), MockArtistD.new(2), MockArtistD.new(3)];
      selectionManager = new SelectionManager(ArtistSelection);
      selectionChangeSpy = spyOn(selectionManager.selectionChange$, 'emit').and.callThrough();
      component.selectionManager = selectionManager;
    });

    it('should have the expected columns', () => {
      const expAllCols: ArtistColumnType[] = ['id', 'value', 'imageCount', 'score', 'createdTime'];
      const expVisCols: ArtistColumnType[] = ['id', 'value', 'imageCount', 'score', 'createdTime'];
      const expVis = {} as any;
      expAllCols.forEach(col => { expVis[col] = false; });
      expVisCols.forEach(col => { expVis[col] = true; });

      fixture.detectChanges();
      expect(component.ALL_COLUMNS).toEqual(expAllCols);
      expect(component.columns).toEqual(expVisCols);
      expect(component.columnVisibility).toEqual(expVis);
    });

    it('should render in list view', () => {
      const getViewRows = () => fixture.nativeElement.querySelectorAll('tr.cdk-row');

      selectionManager.setItems([]);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(0);

      selectionManager.setItems(items);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(3);
    });

    it('should update view when library notifies a artist was updated', () => {
      items[0].value = 'abc';
      items[1].value = 'def';
      items[2].value = 'ghi';
      selectionManager.setItems(items);
      fixture.detectChanges();
      const getViewValues = () => (Array.from(fixture.nativeElement
        .querySelectorAll('td.cell-value')) as any)
        .map(el => el.innerText);

      expect(getViewValues()).toEqual(['abc', 'def', 'ghi']);

      // Here we change value, but dont fire imageUpdated, so expect no result
      component.items[0].value = 'xyz';
      fixture.detectChanges();
      expect(getViewValues()).toEqual(['abc', 'def', 'ghi']);

      libraryService.contentUpdated$.artist.next([1]);
      expect(getViewValues()).toEqual(['xyz', 'def', 'ghi']);
    });

  });


  describe('ImageTableComponent', () => {
    let component: ImageTableComponent;
    let fixture: ComponentFixture<ImageTableComponent>;
    let selectionManager: SelectionManager<Image, ImageSelection>;
    let selectionChangeSpy: jasmine.Spy;

    let items: Image[];

    beforeEach(() => {
      fixture = TestBed.createComponent(ImageTableComponent);
      component = fixture.componentInstance;
    });

    beforeEach(() => {
      items  = [MockImage.new(1), MockImage.new(2), MockImage.new(3)];
      selectionManager = new SelectionManager(ImageSelection);
      selectionChangeSpy = spyOn(selectionManager.selectionChange$, 'emit').and.callThrough();
      component.selectionManager = selectionManager;
      component.mode = 'thumbnails';
    });

    it('should have the expected columns', () => {
      const expAllCols: ImageColumnType[] = ['id', 'favorite', 'processed', 'tags', 'names', 'artistId', 'groupId', 'rating', 'ratingCount', 'createdTime', 'processedTime', 'filesize', 'length'];
      const expVisCols: ImageColumnType[] = ['id', 'favorite', 'processed', 'tags', 'names', 'rating', 'createdTime', 'filesize'];
      const expVis = {} as any;
      expAllCols.forEach(col => { expVis[col] = false; });
      expVisCols.forEach(col => { expVis[col] = true; });

      fixture.detectChanges();
      expect(component.ALL_COLUMNS).toEqual(expAllCols);
      expect(component.columns).toEqual(expVisCols);
      expect(component.columnVisibility).toEqual(expVis);
    });

    it('should recompute displayed columns', () => {
      const expCols: ImageColumnType[] = ['id', 'processed', 'names', 'rating', 'createdTime', 'filesize'];
      fixture.detectChanges();
      component.columnVisibility['favorite'] = false;
      component.columnVisibility['tags'] = false;
      component.recomputeDisplayedColumns();

      expect(component.columns).toEqual(expCols);
    });

    it('should render in thumbnail view', () => {
      component.mode = 'thumbnails';

      const getViewImages = () => fixture.nativeElement.querySelectorAll('rx-thumbnail');

      selectionManager.setItems([]);
      fixture.detectChanges();
      expect(getViewImages().length).toBe(0);

      selectionManager.setItems(items);
      fixture.detectChanges();
      expect(getViewImages().length).toBe(3);
    });

    it('should render in list view', () => {
      component.mode = 'list';

      const getViewRows = () => fixture.nativeElement.querySelectorAll('tr.cdk-row');

      selectionManager.setItems([]);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(0);

      selectionManager.setItems(items);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(3);
    });

    it('should update view when library notifies an image was updated', () => {
      items[0].groupId = 10;
      component.mode = 'list';
      component.columnVisibility['groupId'] = true;
      component.recomputeDisplayedColumns();
      selectionManager.setItems(items);
      fixture.detectChanges();
      const getViewGroupIds = () => (Array.from(fixture.nativeElement
        .querySelectorAll('td.cell-group')) as any)
        .map(el => el.innerText);

      expect(getViewGroupIds()).toEqual(['10', '-', '-']);

      // Here we change value, but dont fire imageUpdated, so expect no result
      component.items[0].groupId = 11;
      fixture.detectChanges();
      expect(getViewGroupIds()).toEqual(['10', '-', '-']);

      libraryService.imageUpdated$.next([1]);
      expect(getViewGroupIds()).toEqual(['11', '-', '-']);
    });

  });


  describe('NameTableComponent', () => {
    let component: NameTableComponent;
    let fixture: ComponentFixture<NameTableComponent>;
    let selectionManager: SelectionManager<NameDetails, NameSelection>;
    let selectionChangeSpy: jasmine.Spy;

    let items: NameDetails[];

    beforeEach(() => {
      fixture = TestBed.createComponent(NameTableComponent);
      component = fixture.componentInstance;
    });

    beforeEach(() => {
      items  = [MockNameD.new(1), MockNameD.new(2), MockNameD.new(3)];
      selectionManager = new SelectionManager(NameSelection);
      selectionChangeSpy = spyOn(selectionManager.selectionChange$, 'emit').and.callThrough();
      component.selectionManager = selectionManager;
    });

    it('should have the expected columns', () => {
      const expAllCols: NameColumnType[] = ['id', 'value', 'groupCount', 'imageCount', 'score', 'createdTime'];
      const expVisCols: NameColumnType[] = ['id', 'value', 'imageCount', 'score', 'createdTime'];
      const expVis = {} as any;
      expAllCols.forEach(col => { expVis[col] = false; });
      expVisCols.forEach(col => { expVis[col] = true; });

      fixture.detectChanges();
      expect(component.ALL_COLUMNS).toEqual(expAllCols);
      expect(component.columns).toEqual(expVisCols);
      expect(component.columnVisibility).toEqual(expVis);
    });

    it('should render in list view', () => {
      const getViewRows = () => fixture.nativeElement.querySelectorAll('tr.cdk-row');

      selectionManager.setItems([]);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(0);

      selectionManager.setItems(items);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(3);
    });

    it('should update view when library notifies a name was updated', () => {
      items[0].value = 'abc';
      items[1].value = 'def';
      items[2].value = 'ghi';
      selectionManager.setItems(items);
      fixture.detectChanges();
      const getViewValues = () => (Array.from(fixture.nativeElement
        .querySelectorAll('td.cell-value')) as any)
        .map(el => el.innerText);

      expect(getViewValues()).toEqual(['abc', 'def', 'ghi']);

      // Here we change value, but dont fire imageUpdated, so expect no result
      component.items[0].value = 'xyz';
      fixture.detectChanges();
      expect(getViewValues()).toEqual(['abc', 'def', 'ghi']);

      libraryService.contentUpdated$.name.next([1]);
      expect(getViewValues()).toEqual(['xyz', 'def', 'ghi']);
    });

  });


  describe('TagTableComponent', () => {
    let component: TagTableComponent;
    let fixture: ComponentFixture<TagTableComponent>;
    let selectionManager: SelectionManager<TagDetails, TagSelection>;
    let selectionChangeSpy: jasmine.Spy;

    let items: TagDetails[];

    beforeEach(() => {
      fixture = TestBed.createComponent(TagTableComponent);
      component = fixture.componentInstance;
    });

    beforeEach(() => {
      items  = [MockTagD.new(1), MockTagD.new(2), MockTagD.new(3)];
      selectionManager = new SelectionManager(TagSelection);
      selectionChangeSpy = spyOn(selectionManager.selectionChange$, 'emit').and.callThrough();
      component.selectionManager = selectionManager;
    });

    it('should have the expected columns', () => {
      const expAllCols: TagColumnType[] = ['id', 'value', 'groupCount', 'imageCount', 'score', 'createdTime'];
      const expVisCols: TagColumnType[] = ['id', 'value', 'imageCount', 'score', 'createdTime'];
      const expVis = {} as any;
      expAllCols.forEach(col => { expVis[col] = false; });
      expVisCols.forEach(col => { expVis[col] = true; });

      fixture.detectChanges();
      expect(component.ALL_COLUMNS).toEqual(expAllCols);
      expect(component.columns).toEqual(expVisCols);
      expect(component.columnVisibility).toEqual(expVis);
    });

    it('should render in list view', () => {
      const getViewRows = () => fixture.nativeElement.querySelectorAll('tr.cdk-row');

      selectionManager.setItems([]);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(0);

      selectionManager.setItems(items);
      fixture.detectChanges();
      expect(getViewRows().length).toBe(3);
    });

    it('should update view when library notifies a tag was updated', () => {
      items[0].value = 'abc';
      items[1].value = 'def';
      items[2].value = 'ghi';
      selectionManager.setItems(items);
      fixture.detectChanges();
      const getViewValues = () => (Array.from(fixture.nativeElement
        .querySelectorAll('td.cell-value')) as any)
        .map(el => el.innerText);

      expect(getViewValues()).toEqual(['abc', 'def', 'ghi']);

      // Here we change value, but dont fire imageUpdated, so expect no result
      component.items[0].value = 'xyz';
      fixture.detectChanges();
      expect(getViewValues()).toEqual(['abc', 'def', 'ghi']);

      libraryService.contentUpdated$.tag.next([1]);
      expect(getViewValues()).toEqual(['xyz', 'def', 'ghi']);
    });

  });

});