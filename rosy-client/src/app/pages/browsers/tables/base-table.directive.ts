import {
  ChangeDetectorRef,
  Directive,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {moveItemInArray, CdkDragDrop, } from '@angular/cdk/drag-drop';
import {takeUntil, } from 'rxjs/operators';
import {Sort} from '@angular/material/sort';

import {Selection} from '@app/models';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

import {SelectionManager} from './selection-manager';


@Directive()
export class BaseTableDirective<ItemType>
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  @Input() sort: Sort | null = null;

  @Input() selectionManager: SelectionManager<ItemType, Selection<ItemType>>;

  @Input() showHeader = true;

  @Output() sortChange = new EventEmitter<Sort>();

  items: ItemType[] = [];

  rowSelected: boolean[] = [];

  constructor(protected cdr: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.onItemsChanged();
    this.selectionManager.selectionChange$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.cdr.markForCheck());
    this.selectionManager.itemsChanged$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.onItemsChanged());
  }

  protected onItemsChanged() {
    this.items = this.selectionManager.getItems();
    this.rowSelected = this.selectionManager.getSelectedRows();
    this.cdr.markForCheck();
  }

}


@Directive()
export class BaseColumnTableDirective<ItemType, ColumnType extends string>
extends BaseTableDirective<ItemType>
implements OnInit {

  ALL_COLUMNS: ColumnType[] = [];

  @Input() columns: ColumnType[];

  columnVisibility: { [key in ColumnType]: boolean };

  constructor(
    cdr: ChangeDetectorRef,
  ) {
    super(cdr);
    this.columnVisibility = {} as any;
  }

  override ngOnInit() {
    super.ngOnInit();
    // Warning: Don't put the below line in the constructor. Within the constructor it uses the
    // parent definition of `ALL_COLUMNS`` for some reason
    this.ALL_COLUMNS.forEach((col: ColumnType) => {
      this.columnVisibility[col] = false;
    });
    this.columns.forEach((col: ColumnType) => {
      this.columnVisibility[col] = true;
    });
    this.recomputeDisplayedColumns();
  }

  moveColumnEvent(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  recomputeDisplayedColumns() {
    this.columns = this.ALL_COLUMNS.filter((col: ColumnType) => this.columnVisibility[col]);
  }

}
