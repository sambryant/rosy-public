import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';

import {
  ArtistDetails,
  Image,
  NameDetails,
  TagDetails,
} from '@app/models';
import {LibraryService} from '@app/services';

import {BaseColumnTableDirective} from './base-table.directive';


export type ImageColumnType = 'id' | 'favorite' | 'processed' | 'tags' | 'names' | 'artistId' | 'groupId' | 'rating' | 'ratingCount' | 'createdTime' | 'processedTime' | 'filesize' | 'length';


@Component({
  selector: 'rx-image-table',
  templateUrl: './image-table.component.html',
  styleUrls: ['./base-table.scss', './image-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageTableComponent
extends BaseColumnTableDirective<Image, ImageColumnType>
implements OnInit {

  override readonly ALL_COLUMNS: ImageColumnType[] = ['id', 'favorite', 'processed', 'tags', 'names', 'artistId', 'groupId', 'rating', 'ratingCount', 'createdTime', 'processedTime', 'filesize', 'length'];

  @Input() mode: 'list' | 'thumbnails';

  constructor(
    cdr: ChangeDetectorRef,
    protected libraryService: LibraryService,
  ) {
    super(cdr);
    this.columns = ['id', 'favorite', 'processed', 'tags', 'names', 'rating', 'createdTime', 'filesize'];
  }

  override ngOnInit() {
    super.ngOnInit();

    this.libraryService.imageUpdated$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.cdr.detectChanges();
      });
  }

}


export type ArtistColumnType = 'id' | 'value' | 'imageCount' | 'score' | 'createdTime';


@Component({
  selector: 'rx-artist-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./base-table.scss', './basic-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArtistTableComponent
extends BaseColumnTableDirective<ArtistDetails, ArtistColumnType>
implements OnInit {

  override readonly ALL_COLUMNS: ArtistColumnType[] = ['id', 'value', 'imageCount', 'score', 'createdTime'];

  constructor(
    cdr: ChangeDetectorRef,
    protected libraryService: LibraryService,
  ) {
    super(cdr);
    this.columns = ['id', 'value', 'imageCount', 'score', 'createdTime'];
  }

  override ngOnInit() {
    super.ngOnInit();

    this.libraryService.contentUpdated$.artist
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.cdr.detectChanges();
      });
  }

}


export type NameColumnType = 'id' | 'value' | 'groupCount' | 'imageCount' | 'score' | 'createdTime';


@Component({
  selector: 'rx-name-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./base-table.scss', './basic-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NameTableComponent
extends BaseColumnTableDirective<NameDetails, NameColumnType>
implements OnInit {

  override readonly ALL_COLUMNS: NameColumnType[] = ['id', 'value', 'groupCount', 'imageCount', 'score', 'createdTime'];

  constructor(
    cdr: ChangeDetectorRef,
    protected libraryService: LibraryService,
  ) {
    super(cdr);
    this.columns = ['id', 'value', 'imageCount', 'score', 'createdTime'];
  }

  override ngOnInit() {
    super.ngOnInit();

    this.libraryService.contentUpdated$.name
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.cdr.detectChanges();
      });
  }

}


export type TagColumnType = NameColumnType;


@Component({
  selector: 'rx-tag-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./base-table.scss', './basic-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagTableComponent
extends BaseColumnTableDirective<TagDetails, TagColumnType>
implements OnInit {

  override readonly ALL_COLUMNS: TagColumnType[] = ['id', 'value', 'groupCount', 'imageCount', 'score', 'createdTime'];

  constructor(
    cdr: ChangeDetectorRef,
    protected libraryService: LibraryService,
  ) {
    super(cdr);
    this.columns = ['id', 'value', 'imageCount', 'score', 'createdTime'];
  }

  override ngOnInit() {
    super.ngOnInit();

    this.libraryService.contentUpdated$.tag
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.cdr.detectChanges();
      });
  }

}
