import {EventEmitter} from '@angular/core';
import {Observable, Subject, } from 'rxjs';

import {Direction, NavigationRequest, Selection, } from '@app/models';


export type SelectionConstructor<T, S> = new (selected: T[], item: T | null) => S;


export class SelectionManager<T, S extends Selection<T>> {

  selectionChange$: EventEmitter<S>;

  itemsChanged$: EventEmitter<void>;

  boundaryEvent$: EventEmitter<Direction>;

  protected items: T[] = [];

  protected markHasChanged = false;

  protected rowSelected: boolean[] = [];

  protected selectionCursor: number | null = null;

  protected selectionAnchor: number | null = null;

  protected selection: S;

  protected selectedItem: T | null = null;

  protected selectedItems: T[] = [];

  constructor(protected SelectionClass: SelectionConstructor<T, S>) {
    this.boundaryEvent$ = new EventEmitter();
    this.itemsChanged$ = new EventEmitter();
    this.selectionChange$ = new EventEmitter();
    this.selection = new SelectionClass([], null);
  }

  deselectAll(): void {
    if (this.hasSelection()) {
      this.selectionAnchor = null;
      this.selectionCursor = null;
      this.selectedItem = null;
      this.selectedItems = [];
      this.items.forEach((_, i) => {
        this.rowSelected[i] = false;
      });
      this.markHasChanged = true;
      this.notifyChanged();
    }
  }

  hasSelection(): boolean {
    return this.selectionCursor !== null;
  }

  getCursorIndex(): number | null {
    return this.selectionCursor;
  }

  getItems(): T[] {
    return this.items;
  }

  getSelectedRows(): boolean[] { return this.rowSelected; }

  getSelectedItems(): T[] {
    return this.selectedItems;
  }

  getSelection(): S {
    return this.selection;
  }

  navigate({direction, extend}: NavigationRequest) {
    const index = this.selectionCursor !== null ?
      this.selectionCursor : direction === 'next' ?
        -1 : this.items.length;
    const newIndex = direction === 'next' ? index + 1 : index - 1;

    if (newIndex >= 0 && newIndex < this.items.length) {
      this.setSelection(newIndex, extend || false);
    } else if (!extend) {
      this.boundaryEvent$.emit(direction);
    }
  }

  setItems(items: T[]) {
    this.items = items;
    this.selectionAnchor = null;
    this.selectionCursor = null;
    this.selectedItem = null;
    this.selectedItems = [];
    this.rowSelected = this.items.map(_ => false);
    this.markHasChanged = true;
    this.notifyChanged();
    this.itemsChanged$.emit();
  }

  setSelection(cursor: number | null, extend: boolean) {
    if (cursor === null) {
      if (this.selectionAnchor !== null || this.selectionCursor !== null) {
        this.selectionCursor = null;
        this.selectionAnchor = null;
        this.markHasChanged = true;
      }
    } else {
      this.setSelectionBounded(cursor, extend);
    }
    if (this.markHasChanged) {
      this.computeSelectedItems();
      this.notifyChanged();
    }
  }

  protected setSelectionBounded(index: number, extend: boolean) {
    const cursor = this.items[index] ? index : null;
    let anchor = cursor;
    if (extend) {
      anchor = (this.selectionAnchor !== null && this.items[this.selectionAnchor]) ? this.selectionAnchor : cursor;
    }
    if (cursor !== this.selectionCursor || anchor !== this.selectionAnchor || cursor !== index) {
      this.markHasChanged = true;
    }
    this.selectionCursor = cursor;
    this.selectionAnchor = anchor;
  }

  protected computeSelectedItems() {
    this.selectedItem = this.selectionCursor !== null && this.items[this.selectionCursor] || null;
    this.selectedItems = [];
    if (this.selectionCursor === null || this.selectionAnchor === null) {
      this.items.forEach((_, i) => {
        this.rowSelected[i] = false;
      });
    } else {
      this.items.forEach((_, i) => {
        const selected =
          (i >= this.selectionAnchor! && i <= this.selectionCursor!) ||
          (i <= this.selectionAnchor! && i >= this.selectionCursor!);
        if (selected) {
          this.selectedItems.push(this.items[i]);
        }
        this.rowSelected[i] = selected;
      });
    }
  }

  protected notifyChanged() {
    if (this.markHasChanged) {
      this.markHasChanged = false;
      this.selection = new this.SelectionClass(this.selectedItems, this.selectedItem);
      this.selectionChange$.emit(this.selection);
    }
  }
}
