import {Selection} from '@app/models';

import {SelectionManager} from '.';


class Item {
  id: number;
  text: string;
  constructor(id: number) {
    this.id = id;
    this.text = `item ${id}`;
  }
}


class ItemSelection extends Selection<Item> {
  static none(): ItemSelection {
    return new ItemSelection([], null);
  }
  static single(item: Item): ItemSelection {
    return new ItemSelection([item], item);
  }
}


describe('SelectionManager', () => {
  let manager: SelectionManager<Item, ItemSelection>;
  let selectionChangeSpy: jasmine.Spy;
  let itemsChangedSpy: jasmine.Spy;
  let boundaryEventSpy: jasmine.Spy;

  let mockItems: Item[];

  beforeEach(() => {
    manager = new SelectionManager(ItemSelection);
    selectionChangeSpy = spyOn(manager.selectionChange$, 'emit').and.callThrough();
    itemsChangedSpy = spyOn(manager.itemsChanged$, 'emit').and.callThrough();
    boundaryEventSpy = spyOn(manager.boundaryEvent$, 'emit').and.callThrough();

    mockItems = [0, 1, 2, 3, 4].map(id => new Item(id));
  });

  it('should not have anything selected by default', () => {
    // No items have been set yet, expect nothing selected
    expect(manager.hasSelection()).toBeFalse();
    expect(manager.getCursorIndex()).toBe(null);
    expect(manager.getItems()).toEqual([]);
    expect(manager.getSelectedRows()).toEqual([]);
    expect(manager.getSelectedItems()).toEqual([]);

    // Items have been set, but still expect nothing selected
    manager.setItems(mockItems);
    expect(manager.hasSelection()).toBeFalse();
    expect(manager.getCursorIndex()).toBe(null);
    expect(manager.getItems()).toEqual(mockItems);
    expect(manager.getSelectedRows()).toEqual([false, false, false, false, false]);
    expect(manager.getSelectedItems()).toEqual([]);
  });

  describe('getSelectedRows', () => {

    it('should update existing array instance if selection changes', () => {
      // This test might seem a bit weird, but it's actually important to the design of the
      // components which rely on this. They only call 'getSelectedRows()' when the list of items
      // change and then expect the instance they received to be updated thereafter whenever the
      // selection changes.

      manager.setItems(mockItems);
      const rowSelected = manager.getSelectedRows();

      expect(rowSelected).toEqual([false, false, false, false, false]);

      manager.setSelection(4, false);
      expect(rowSelected).toEqual([false, false, false, false, true]);
    });

    it('should use new existing array instance once set of items are reset', () => {
      manager.setItems(mockItems);
      const rowSelected = manager.getSelectedRows();

      manager.setSelection(4, false);
      expect(rowSelected).toEqual([false, false, false, false, true]);

      manager.setItems(mockItems.slice());
      manager.setSelection(1, false);
      expect(rowSelected).toEqual([false, false, false, false, true]);
      expect(manager.getSelectedRows()).toEqual([false, true, false, false, false]);
    });

  });

  describe('setSelection', () => {

    beforeEach(() => {
      manager.setItems(mockItems);
      itemsChangedSpy.calls.reset();
      selectionChangeSpy.calls.reset();
    });

    it('should fire selectionChangeSpy when cursor moves', () => {
      manager.setSelection(2, false);
      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[2]));
      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);

      manager.setSelection(3, false);
      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[3]));
      expect(selectionChangeSpy).toHaveBeenCalledTimes(2);
    });

    it('should fire selectionChangeSpy when cursor moves with extend', () => {
      manager.setSelection(2, true);
      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[2]));

      manager.setSelection(3, true);
      expect(selectionChangeSpy).toHaveBeenCalledWith(
        new ItemSelection([mockItems[2], mockItems[3]], mockItems[3]));

      expect(selectionChangeSpy).toHaveBeenCalledTimes(2);
    });

    it('should fire selectionChangeSpy when item is deselected', () => {
      manager.setSelection(2, true);
      selectionChangeSpy.calls.reset();

      manager.setSelection(null, false);
      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.none());
      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
    });

    it('should not fire selectionChangeSpy if selection does not change', () => {
      manager.setSelection(2, true);
      selectionChangeSpy.calls.reset();

      manager.setSelection(2, true);
      expect(selectionChangeSpy).not.toHaveBeenCalled();
    });

    it('should fire selectionChangeSpy if selection doesnt change because it is out of bounds', () => {
      manager.setSelection(null, false);
      selectionChangeSpy.calls.reset();

      manager.setSelection(null, false);
      expect(selectionChangeSpy).not.toHaveBeenCalled();

      manager.setSelection(118, false);
      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.none());
      expect(manager.hasSelection()).toBeFalse();
      expect(manager.getCursorIndex()).toBeNull();
      expect(manager.getSelectedItems()).toEqual([]);
      expect(manager.getSelectedRows()).toEqual([false, false, false, false, false]);
      selectionChangeSpy.calls.reset();

      manager.setSelection(-1, false);
      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.none());
      expect(manager.hasSelection()).toBeFalse();
      expect(manager.getCursorIndex()).toBeNull();
      expect(manager.getSelectedItems()).toEqual([]);
      expect(manager.getSelectedRows()).toEqual([false, false, false, false, false]);
    });

    it('should update state functions', () => {
      expect(manager.hasSelection()).toBeFalse();
      expect(manager.getCursorIndex()).toBeNull();
      expect(manager.getSelectedRows()).toEqual([false, false, false, false, false]);
      expect(manager.getSelectedItems()).toEqual([]);

      manager.setSelection(2, true);
      expect(manager.hasSelection()).toBeTrue();
      expect(manager.getCursorIndex()).toBe(2);
      expect(manager.getSelectedRows()).toEqual([false, false, true, false, false]);
      expect(manager.getSelectedItems()).toEqual([mockItems[2]]);

      manager.setSelection(4, true);
      expect(manager.hasSelection()).toBeTrue();
      expect(manager.getCursorIndex()).toBe(4);
      expect(manager.getSelectedRows()).toEqual([false, false, true, true, true]);
      expect(manager.getSelectedItems()).toEqual([mockItems[2], mockItems[3], mockItems[4]]);

      manager.setSelection(null, false);
      expect(manager.hasSelection()).toBeFalse();
      expect(manager.getCursorIndex()).toBeNull();
      expect(manager.getSelectedRows()).toEqual([false, false, false, false, false]);
      expect(manager.getSelectedItems()).toEqual([]);
    });
  });

  describe('setItems', () => {

    it('should fire itemsChanged', () => {
      expect(itemsChangedSpy).not.toHaveBeenCalled();
      manager.setItems(mockItems);
      expect(itemsChangedSpy).toHaveBeenCalledTimes(1);
      manager.setItems(mockItems);
      expect(itemsChangedSpy).toHaveBeenCalledTimes(2);
    });

    it('should remove selection state', () => {
      manager.setItems(mockItems);

      manager.setSelection(2, false);
      manager.setSelection(3, true);

      selectionChangeSpy.calls.reset();

      manager.setItems(mockItems);

      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.none());
      expect(manager.hasSelection()).toBeFalse();
      expect(manager.getCursorIndex()).toBeNull();
      expect(manager.getSelectedRows()).toEqual([false, false, false, false, false]);
      expect(manager.getSelectedItems()).toEqual([]);
    });

    it('should fire selection change even if already had nothing selected', () => {
      manager.setItems(mockItems);
      manager.setSelection(null, false);
      selectionChangeSpy.calls.reset();
      manager.setItems(mockItems);
      expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.none());
    });

  });

  describe('navigate', () => {

    beforeEach(() => {
      manager.setItems(mockItems);
      itemsChangedSpy.calls.reset();
      selectionChangeSpy.calls.reset();
    });

    describe('next', () => {

      it('should navigate to first item if nothing selected', () => {
        expect(manager.getCursorIndex()).toBeNull();

        manager.navigate({direction: 'next', extend: false});
        expect(manager.getCursorIndex()).toBe(0);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[0]));
      });

      it('should extend-navigate to first item if nothing selected', () => {
        expect(manager.getCursorIndex()).toBeNull();

        manager.navigate({direction: 'next', extend: true});
        expect(manager.getCursorIndex()).toBe(0);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[0]));
      });

      it('should navigate to next item', () => {
        manager.setSelection(2, false);
        selectionChangeSpy.calls.reset();

        manager.navigate({direction: 'next', extend: false});
        expect(manager.getCursorIndex()).toBe(3);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[3]));
        expect(boundaryEventSpy).not.toHaveBeenCalled();
      });

      it('should extend-navigate to next item', () => {
        manager.setSelection(2, false);
        selectionChangeSpy.calls.reset();

        manager.navigate({direction: 'next', extend: true});
        expect(manager.getCursorIndex()).toBe(3);
        expect(manager.getSelectedItems()).toEqual([mockItems[2], mockItems[3]]);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(new ItemSelection([mockItems[2], mockItems[3]], mockItems[3]));
        expect(boundaryEventSpy).not.toHaveBeenCalled();
      });

      it('should fire boundary event when navigating at end of items', () => {
        manager.setSelection(4, false);
        boundaryEventSpy.calls.reset();
        selectionChangeSpy.calls.reset();

        expect(manager.getCursorIndex()).toBe(4);
        manager.navigate({direction: 'next', extend: false});
        expect(manager.getCursorIndex()).toBe(4);

        expect(boundaryEventSpy).toHaveBeenCalledTimes(1);
        expect(boundaryEventSpy).toHaveBeenCalledWith('next');
        expect(selectionChangeSpy).not.toHaveBeenCalled();
      });

      it('should not fire boundary event when extend-navigating at end of items', () => {
        manager.setSelection(4, false);
        boundaryEventSpy.calls.reset();
        selectionChangeSpy.calls.reset();

        expect(manager.getCursorIndex()).toBe(4);
        manager.navigate({direction: 'next', extend: true});
        expect(manager.getCursorIndex()).toBe(4);

        expect(boundaryEventSpy).not.toHaveBeenCalled();
        expect(selectionChangeSpy).not.toHaveBeenCalled();
      });
    });

    describe('prev', () => {

      it('should navigate to last item if nothing selected', () => {
        expect(manager.getCursorIndex()).toBeNull();

        manager.navigate({direction: 'prev', extend: false});
        expect(manager.getCursorIndex()).toBe(4);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[4]));
      });

      it('should extend-navigate to last item if nothing selected', () => {
        expect(manager.getCursorIndex()).toBeNull();

        manager.navigate({direction: 'prev', extend: true});
        expect(manager.getCursorIndex()).toBe(4);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[4]));
      });

      it('should navigate to prev item', () => {
        manager.setSelection(2, false);
        selectionChangeSpy.calls.reset();

        manager.navigate({direction: 'prev', extend: false});
        expect(manager.getCursorIndex()).toBe(1);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(ItemSelection.single(mockItems[1]));
        expect(boundaryEventSpy).not.toHaveBeenCalled();
      });

      it('should extend-navigate to next item', () => {
        manager.setSelection(2, false);
        selectionChangeSpy.calls.reset();

        manager.navigate({direction: 'prev', extend: true});
        expect(manager.getCursorIndex()).toBe(1);
        expect(manager.getSelectedItems()).toEqual([mockItems[1], mockItems[2]]);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(new ItemSelection([mockItems[1], mockItems[2]], mockItems[1]));
        expect(boundaryEventSpy).not.toHaveBeenCalled();
      });

      it('should fire boundary event when navigating at start of items', () => {
        manager.setSelection(0, false);
        boundaryEventSpy.calls.reset();
        selectionChangeSpy.calls.reset();

        expect(manager.getCursorIndex()).toBe(0);
        manager.navigate({direction: 'prev', extend: false});
        expect(manager.getCursorIndex()).toBe(0);

        expect(boundaryEventSpy).toHaveBeenCalledTimes(1);
        expect(boundaryEventSpy).toHaveBeenCalledWith('prev');
        expect(selectionChangeSpy).not.toHaveBeenCalled();
      });

      it('should not fire boundary event when extend-navigating at start of items', () => {
        manager.setSelection(0, false);
        boundaryEventSpy.calls.reset();
        selectionChangeSpy.calls.reset();

        expect(manager.getCursorIndex()).toBe(0);
        manager.navigate({direction: 'prev', extend: true});
        expect(manager.getCursorIndex()).toBe(0);

        expect(boundaryEventSpy).not.toHaveBeenCalled();
        expect(selectionChangeSpy).not.toHaveBeenCalled();
      });
    });
  });
});
