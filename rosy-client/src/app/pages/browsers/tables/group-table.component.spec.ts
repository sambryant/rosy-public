import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CdkTableModule} from '@angular/cdk/table';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Subject} from 'rxjs';

import {
  Group,
  GroupId,
  GroupSelection,
  Image,
  ImageId,
  ImageSelection,
} from '@app/models';
import {ApiService, LibraryService, NotificationService, } from '@app/services';
import {MockGroup, MockImage, } from '@app/models/testing';
import {PipesModule} from '@app/shared/pipes';

import {GroupTableComponent, ImageTableComponent, SelectionManager, } from '.';


describe('GroupTableComponent', () => {
  let component: GroupTableComponent;
  let fixture: ComponentFixture<GroupTableComponent>;
  let selectionManager: SelectionManager<Group, GroupSelection>;
  let selectionChangeSpy: jasmine.Spy;

  let libraryService: LibraryService;
  let items: Group[];

  function imageSelection(image: Image): ImageSelection {
    return new ImageSelection([image], image);
  }

  function groupSelection(group: Group): GroupSelection {
    return new GroupSelection([group], group);
  }

  beforeEach(async () => {
    const mockNotificationService = { error: jasmine.createSpy() } as any as NotificationService;
    await TestBed.configureTestingModule({
      declarations: [
        GroupTableComponent,
        ImageTableComponent,
      ],
      imports: [
        CdkTableModule,
        MatSortModule,
        MatTooltipModule,
        NoopAnimationsModule,
        PipesModule,
      ],
      providers: [
        {provide: ApiService, useValue: {}},
        {provide: NotificationService, useValue: mockNotificationService},
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ] // Ignores component not in imports, declarations, or providers
    })
    .compileComponents();

    libraryService = TestBed.inject(LibraryService);
    spyOn(libraryService, 'getContentValueById').and.returnValue(new Subject());
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupTableComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    items = [
      MockGroup.new(1).withImages([1, 2, 3]),
      MockGroup.new(2).withImages([4, 5, 6]),
      MockGroup.new(3).withImages([7, 8, 9]),
    ];
    selectionManager = new SelectionManager(GroupSelection);
    selectionChangeSpy = spyOn(selectionManager.selectionChange$, 'emit').and.callThrough();
    component.selectionManager = selectionManager;
    component.imageSelectionManagers = [];
    component.mode = 'thumbnails';
  });

  function setLoadedItems(groups: Group[]) {
    selectionManager.setItems(groups);
    component.imageSelectionManagers = groups.map(group => {
      return new SelectionManager(ImageSelection);
    });
    component.imageSelectionManagers.forEach((manager, groupIndex) => {
      manager.setItems(groups[groupIndex].images);
    });
    fixture.detectChanges();
  }

  it('should return the row expanded array', () => {
    setLoadedItems(items);

    expect(component.rowExpanded).toEqual([true, true, true]);
  });

  it('should toggle expanded rows', () => {
    setLoadedItems(items);

    component.toggleGroup(1);

    expect(component.rowExpanded).toEqual([true, false, true]);
  });

  it('should reset row toggles after resetting images', () => {
    setLoadedItems(items);

    component.toggleGroup(1);

    expect(component.rowExpanded).toEqual([true, false, true]);

    items = [items[0], items[1], items[2], MockGroup.new(3)];
    setLoadedItems(items);

    expect(component.rowExpanded).toEqual([true, true, true, true]);
  });

  it('should render in thumbnail view', () => {
    component.mode = 'thumbnails';

    const getViewImages = () => fixture.nativeElement.querySelectorAll('rx-thumbnail');

    setLoadedItems([]);
    expect(getViewImages().length).toBe(0);

    setLoadedItems(items);
    expect(getViewImages().length).toBe(9);
  });

  it('should render in list view', () => {
    component.mode = 'list';

    const getViewRows = () => fixture.nativeElement.querySelectorAll('tr.cdk-row');

    setLoadedItems([]);
    expect(getViewRows().length).toBe(0);

    setLoadedItems(items);
    expect(getViewRows().length).toBe(9);
  });

  it('should update view when library notifies a group was updated', () => {
    component.mode = 'list';
    setLoadedItems(items);
    const getViewGroupTitles = () => (Array.from(fixture.nativeElement
      .querySelectorAll('div.cell-title')) as any)
      .map(el => el.innerText);

    expect(getViewGroupTitles()).toEqual(['Group 1', 'Group 2', 'Group 3']);

    // Here we change value, but dont fire imageUpdated, so expect no result
    component.items[0].value = 'new title';
    fixture.detectChanges();
    expect(getViewGroupTitles()).toEqual(['Group 1', 'Group 2', 'Group 3']);

    libraryService.contentUpdated$.group.next([1]);
    expect(getViewGroupTitles()).toEqual(['new title', 'Group 2', 'Group 3']);
  });

  it('should update image view when library notifies an image was updated', () => {
    component.mode = 'list';
    setLoadedItems(items);
    const getViewImageIds = () => (Array.from(fixture.nativeElement
      .querySelectorAll('td.cell-id')) as any)
      .map(el => el.innerText);

    expect(getViewImageIds()).toEqual(['1', '2', '3', '4', '5', '6', '7', '8', '9']);

    // Here we change value, but dont fire imageUpdated, so expect no result
    component.items[0].images[0].id = 11;
    fixture.detectChanges();
    expect(getViewImageIds()).toEqual(['1', '2', '3', '4', '5', '6', '7', '8', '9']);

    libraryService.imageUpdated$.next([1]);
    expect(getViewImageIds()).toEqual(['11', '2', '3', '4', '5', '6', '7', '8', '9']);
  });

});
