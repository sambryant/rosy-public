import {tick, fakeAsync, ComponentFixture, TestBed, } from '@angular/core/testing';
import {ActivatedRoute, Router, Params} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Output, } from '@angular/core';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';

import {
  ActionService,
  ApiService,
  AuthGuardService,
  ConfigService,
  LibraryService,
  NotificationService,
} from '@app/services';
import {ItemsResponse} from '@app/services';
import {MockQueryItems} from '@app/services/testing';
import {Action, GroupQuery, Selection, } from '@app/models';

import {mixinPagedBrowser, mixinSortedBrowser, SimpleSelectionBrowserDirective, } from '.';
import {SelectionManager} from './tables';


export class Item {
  id: number;
  text: string;
  constructor(id: number) {
    this.id = id;
    this.text = `item ${id}`;
  }
}


abstract class _TestBrowserBase extends SimpleSelectionBrowserDirective<Item, GroupQuery> {}
const _TestBrowser = mixinSortedBrowser(mixinPagedBrowser(_TestBrowserBase));

@Component({
  template: '<div></div>',
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
class TestBrowserComponent extends _TestBrowser {
  mockResponseItems: Item[];
  mockQueryItems: MockQueryItems<Item, GroupQuery>;

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,GroupQuery);
    this.mockResponseItems = [];
    for (let i = 0; i < 10; i++) {
      this.mockResponseItems.push(new Item(i));
    }
    this.mockQueryItems = new MockQueryItems(this.mockResponseItems.slice(), true);
  }

  protected queryLibrary() {
    return this.mockQueryItems.queryItems(this.query);
  }
}


function singleSelection(item: Item): Selection<Item> {
  return new Selection([item], item);
}


describe('BaseBrowserDirective', () => {
  let component: TestBrowserComponent;
  let fixture: ComponentFixture<TestBrowserComponent>;
  let selectionManager: SelectionManager<Item, Selection<Item>>;

  // @Output spies
  let selectionSpy: jasmine.Spy;

  // This simulates querying the server with a query that contains 10 total results.
  let mockQueryObject: MockQueryItems<Item, GroupQuery>;

  // Misc
  let querySpy: jasmine.Spy;
  let mockItems: Item[];
  let router: Router;
  let route: ActivatedRoute;
  let configService: ConfigService;
  let notificationService: NotificationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TestBrowserComponent
      ],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(NotificationService),
        {provide: AuthGuardService, useValue: {canActivate: () => true}},
      ],
    })
    .compileComponents();
    configService = TestBed.inject(ConfigService);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
  });

  beforeEach(() => {
    notificationService = TestBed.inject(NotificationService);
    spyOn(notificationService, 'error');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestBrowserComponent);
    component = fixture.componentInstance;
    selectionManager = component.selectionManager;
    selectionSpy = spyOn(component.selectionChange, 'next').and.callThrough();
    querySpy = spyOn(component.mockQueryItems, 'queryItems').and.callThrough();
    mockQueryObject = component.mockQueryItems;
    mockItems = component.mockResponseItems;
  });

  /**
   * Function to simulation normal post-initialization state of library browser where the browser
   * displays the given query and has selected the row with the given index relative to that query.
   */
  function startAt(query: GroupQuery, selectedIndex: number) {
    const pageSize = query.size;
    const pageNumber = query.page;
    const pageIndex = selectedIndex;

    router.navigate([], {
      queryParams: query.toParams(),
      fragment: pageIndex.toString()
    });
    tick(); // navigate is an async promise, let it resolve
    fixture.detectChanges();
    tick();
    mockQueryObject.resolve();
    tick();

    expect(querySpy).toHaveBeenCalledWith(query);
    const image = mockItems[pageSize * pageNumber + pageIndex];
    expect(selectionSpy).toHaveBeenCalledWith(singleSelection(image));
    expect(route.snapshot.queryParams).toEqual(query.toParams());
    expect(route.snapshot.fragment).toEqual(selectedIndex.toString());
    selectionSpy.calls.reset();
    querySpy.calls.reset();
  }

  it('should start a query request on start up', () => {
    fixture.ngZone!.run(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
      expect(mockQueryObject.hasPendingResponse()).toBeTrue();
      mockQueryObject.resolve();
    });
  });

  it('should refresh query when database schema changes', () => {
    fixture.ngZone!.run(() => {
      fixture.detectChanges();
      mockQueryObject.resolve();

      expect(mockQueryObject.hasPendingResponse()).toBeFalse();
      configService.setDatabaseSchema('new-schema');
      expect(mockQueryObject.hasPendingResponse()).toBeTrue();
      mockQueryObject.resolve();
    });
  });

  it('should mark status as loading while query is in progress', fakeAsync(() => {
    fixture.ngZone!.run(() => {
      fixture.detectChanges();

      expect(component.status).toEqual('loading');
      expect(component.statusMessage).toEqual('Querying server');

      mockQueryObject.resolve();

      expect(component.status).toEqual('normal');
      expect(component.statusMessage).toBeNull();

      tick();
      component.refreshQuery();
      expect(component.status).toEqual('loading');
      mockQueryObject.resolve();
      expect(component.status).toEqual('normal');
    });
  }));

  it('should mark status as error when query returns empty', fakeAsync(() => {
    const navigateOptions = {
      queryParams: {size: 50, page: 10}, // triggers empty response
    };
    const expStatus = 'error';
    const expStatusMessage = 'No results found';

    fixture.ngZone!.run(() => {
      router.navigate([], navigateOptions);
      tick(); // navigate is an async promise, let it resolve
      fixture.detectChanges();

      mockQueryObject.resolve();
      expect(component.status).toEqual(expStatus);
      expect(component.statusMessage).toEqual(expStatusMessage);
    });
  }));

  it('should mark status as error and empty items when query emits error', fakeAsync(() => {
    const mockError = {error: { message: 'test error message' }};
    const expStatus = 'error';
    const expStatusMessage = 'test error message';

    fixture.ngZone!.run(() => {
      fixture.detectChanges();

      expect(component.status).toEqual('loading');

      mockQueryObject.resolve();

      expect(component.status).toEqual('normal');
      expect(component.statusMessage).toBeNull();
      expect(component.items.length).not.toBe(0);

      component.refreshQuery();
      mockQueryObject.resolveWithError(mockError);

      expect(component.status).toEqual('error');
      expect(component.statusMessage).toEqual(expStatusMessage);
      expect(component.items.length).toBe(0);
    });
  }));

  describe('navigation methods', () => {
    let actionService: ActionService;

    beforeEach(() => {
      spyOn(component, 'navigate');
      actionService = TestBed.inject(ActionService);
      fixture.detectChanges();
    });

    it('should navigate forward', () => {
      expect(component.navigate).not.toHaveBeenCalled();
      actionService.doAction(Action.NavForward);
      expect(component.navigate).toHaveBeenCalledOnceWith({direction: 'next', extend: false});
    });

    it('should navigate backward', () => {
      expect(component.navigate).not.toHaveBeenCalled();
      actionService.doAction(Action.NavBackward);
      expect(component.navigate).toHaveBeenCalledOnceWith({direction: 'prev', extend: false});
    });

  });

  describe('initial URL response', () => {

    it('should emit image select change once initial query resolves', fakeAsync(() => {
      const navigateOptions = {
        queryParams: {size: 4, page: 1}, // images: 4,5,6,7
        fragment: '2' // image: 6
      };
      const expectedSelect = singleSelection(mockItems[6]);

      fixture.ngZone!.run(() => {
        router.navigate([], navigateOptions);
        tick(); // navigate is an async promise, let it resolve
        fixture.detectChanges();

        expect(selectionSpy).not.toHaveBeenCalled();
        mockQueryObject.resolve();
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should initialize with default query and first index if no fragment and no url params', fakeAsync(() => {
      const expectedQuery = new GroupQuery();
      const expectedImages = mockItems.slice();
      const expectedSelect = singleSelection(mockItems[0]);

      fixture.ngZone!.run(() => {
        fixture.detectChanges();
        tick();
        mockQueryObject.resolve();
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(component.items).toEqual(expectedImages);
        expect(component.query).toEqual(expectedQuery);
        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(querySpy).toHaveBeenCalledTimes(1);
      });
    }));

    it('should set canonical URL for default query', fakeAsync(() => {
      const expectedQuery = new GroupQuery();
      const expectedParams = expectedQuery.toParams();
      const expectedFragment = '0';

      fixture.ngZone!.run(() => {
        fixture.detectChanges();
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(route.snapshot.fragment).toEqual(expectedFragment);
      });
    }));

    it('should initialize with query and selection from URL', fakeAsync(() => {
      const navigateOptions = {
        queryParams: {size: 4, page: 1}, // image ids: 4,5,6,7
        fragment: '2' // image: 6
      };
      const expectedQuery = new GroupQuery(4, 1);
      const expectedImages = mockItems.slice(4, 8);
      const expectedSelect = singleSelection(mockItems[6]);

      fixture.ngZone!.run(() => {
        router.navigate([], navigateOptions);
        tick(); // navigate is an async promise, let it resolve
        fixture.detectChanges();

        expect(querySpy).toHaveBeenCalledWith(expectedQuery);

        mockQueryObject.resolve();
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(component.items).toEqual(expectedImages);
        expect(component.query).toEqual(expectedQuery);
        expect(querySpy).toHaveBeenCalledTimes(1);
      });
    }));

    it('should set selection to first if no fragment given', fakeAsync(() => {
      const query = new GroupQuery(4, 1); // images: 4,5,6,7
      const navigateOptions = {
        queryParams: query.toParams()
      };
      const expectedSelect = singleSelection(mockItems[4]);
      const expectedFragment = '0';
      const expectedParams = query.toParams();

      fixture.ngZone!.run(() => {
        router.navigate([], navigateOptions);
        tick();
        fixture.detectChanges();
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });

    }));

    it('should deselect and clear fragment if fragment is past end of page', fakeAsync(() => {
      const navigateOptions = {
        queryParams: {size: 4, page: 1}, // images: 4,5,6,7
        fragment: '5' // bigger than page size
      };
      const expectedSelect = new Selection([], null);

      fixture.ngZone!.run(() => {
        router.navigate([], navigateOptions);
        tick(); // navigate is an async promise, let it resolve
        fixture.detectChanges();

        mockQueryObject.resolve();
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(route.snapshot.fragment).toBeNull();
        expect(querySpy).toHaveBeenCalledTimes(1);
      });
    }));

    it('should remove fragment when no image is selected', fakeAsync(() => {
      const expectedFragment = null;
      const expectedSelect = new Selection([], null);

      fixture.ngZone!.run(() => {
        fixture.detectChanges();
        tick();
        mockQueryObject.resolve();
        tick();

        selectionManager.setSelection(null, false);
        tick();
        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));
  });

  describe('external navigate changes', () => {

    it('should respond to changes in the URL query params and fragment', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 0); // images: 0,1,2
      const startSelection = 2; // image: 2
      const newQuery = new GroupQuery(4, 1); // images: 4,5,6,7
      const newSelection = 1; // image: 5
      const expectedQuery = new GroupQuery(4, 1);
      const expectedParams = expectedQuery.toParams();
      const expectedFragment = '1';
      const expectedSelect = singleSelection(mockItems[5]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);
        router.navigate([], {queryParams: newQuery.toParams(), fragment: newSelection.toString()});
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(component.query).toEqual(expectedQuery);
        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should ignore and remove unknown query parameters', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 0); // images: 0,1,2
      const startSelection = 2; // image: 2
      const newUrlParams = {
        queryParams: {invalidParam: 'cow', size: 3, page: 1},
        fragment: '1'
      };
      const expectedQuery = new GroupQuery(3, 1);
      const expectedParams = expectedQuery.toParams();
      const expectedFragment = '1';
      const expectedSelect = singleSelection(mockItems[4]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);
        router.navigate([], newUrlParams);
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(component.query).toEqual(expectedQuery);
        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should set the canonical URL when routed to identical non-default query', fakeAsync(() => {
      // Starts with non-default query that uses default page
      // Navigate to page with same query size but no page parameter
      // Expect resulting URL to be the canonical URL
      const query = new GroupQuery(3, 0); // images: 0,1,2
      const selected = 2; // image: 2
      const newUrlParams = {
        queryParams: {size: query.size}, fragment: '2'
      }; // no page given
      const expectedQuery = new GroupQuery(3, 0);
      const expectedFragment = '2';
      const expectedParams = expectedQuery.toParams();

      fixture.ngZone!.run(() => {
        startAt(query, selected);
        router.navigate([], newUrlParams);
        tick();

        expect(querySpy).not.toHaveBeenCalled();
        expect(selectionSpy).not.toHaveBeenCalled();
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(route.snapshot.fragment).toEqual(expectedFragment);
        expect(querySpy).not.toHaveBeenCalled();
      });
    }));

    it('should set URL to canonical URL when routed to identical default query', fakeAsync(() => {
      // Starts with default query
      // Navigate to page with no query params or fragment (which maps to same query and selection).
      // Expect resulting URL to be the canonical URL.
      // Expect no re-querying or select notifications
      const startQuery = new GroupQuery(); // default
      const startSelection = 0; // default
      const newUrlParams = {}; // should map to default.
      const expectedParams = new GroupQuery().toParams();
      const expectedFragment = '0';

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);
        router.navigate([], newUrlParams);
        tick();

        expect(querySpy).not.toHaveBeenCalled();
        expect(selectionSpy).not.toHaveBeenCalled();
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(route.snapshot.fragment).toEqual(expectedFragment);
        expect(querySpy).not.toHaveBeenCalled();
      });
    }));

    it('should deselect and clear fragment if routed with fragment exceeding page size', fakeAsync(() => {
      const newQuery = new GroupQuery(4, 1); // images 4,5,6,7
      const newSelection = 5; // exceeds page size by 1
      const expectedParams = newQuery.toParams();
      const expectedSelect = new Selection([], null);

      fixture.ngZone!.run(() => {
        startAt(new GroupQuery(), 0);
        router.navigate([], {queryParams: newQuery.toParams(), fragment: newSelection.toString()});
        tick();
        mockQueryObject.resolve();
        tick();

        expect(querySpy).toHaveBeenCalledWith(newQuery);
        expect(component.query).toEqual(newQuery);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(route.snapshot.fragment).toBeNull();
      });
    }));

    it('should deselect and clear fragment if routed with fragment exceeding query size', fakeAsync(() => {
      const newQuery = new GroupQuery(4, 2); // images 8,9
      const newSelection = 2; // would be image 10, but last image is 9
      const expectedParams = newQuery.toParams();
      const expectedSelect = new Selection([], null);

      fixture.ngZone!.run(() => {
        startAt(new GroupQuery(), 0);
        router.navigate([], {queryParams: newQuery.toParams(), fragment: newSelection.toString()});
        tick();
        mockQueryObject.resolve();
        tick();

        expect(querySpy).toHaveBeenCalledWith(newQuery);
        expect(component.query).toEqual(newQuery);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(route.snapshot.fragment).toBeNull();
      });
    }));

    it('should wait for query to load before trying to set fragment', fakeAsync(() => {
      // Idea here:
      // 1. start with query with small page size (3)
      // 2. Simultaneously: change query url to have larger page size (5) and change url fragment to
      //    a larger index (4).
      // If component tries to set the index (4) before resolving the query, they will get an
      // undefined image.
      const startQuery = new GroupQuery(3, 0); // images: 0,1,2
      const startSelection = 2; // image 2
      const newQuery = new GroupQuery(5, 0); // 0,1,2,3,4
      const newSelection = 4; // 4
      const expectedSelect = singleSelection(mockItems[4]);
      const expectedParams = newQuery.toParams();
      const expectedFragment = '4';

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);
        router.navigate([], {queryParams: newQuery.toParams(), fragment: newSelection.toString()});
        tick();
        mockQueryObject.resolve();
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
        expect(component.query).toEqual(newQuery);
        expect(querySpy).toHaveBeenCalledTimes(1);
        expect(selectionSpy).toHaveBeenCalledTimes(1);
      });
    }));
  });

  describe('onSelectionChange', () => {

    it('should select an image by index', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 1); // images: 3,4,5
      const startSelection = 1; // image: 4
      const newSelection = 2; // image: 5
      const expectedSelect = singleSelection(mockItems[5]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);
        selectionManager.setSelection(newSelection, false);
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should set the canonical URL', fakeAsync(() => {
      const query = new GroupQuery(4, 1); // images: 4,5,6,7
      const startSelection = 1; // image: 5
      const newSelection = 3; // image: 7
      const expectedFragment = '3'; // image: 7
      const expectedParams = query.toParams();

      fixture.ngZone!.run(() => {
        startAt(query, startSelection);
        selectionManager.setSelection(newSelection, false);
        tick();

        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
      });
    }));

    it('should set image to null when selection is less than zero', fakeAsync(() => {
      const query = new GroupQuery(3, 1); // images: 3,4,5
      const startSelection = 2; // image: 5
      const newSelection = -1; // invalid because below zero
      const expectedSelect = new Selection([], null);
      const expectedFragment = null;

      fixture.ngZone!.run(() => {
        startAt(query, startSelection);
        selectionManager.setSelection(newSelection, false);
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(route.snapshot.fragment).toBe(expectedFragment);
      });
    }));

    it('should set image to null when selection is beyond images length', fakeAsync(() => {
      const query = new GroupQuery(4, 2); // images: 8,9
      const startSelection = 1; // image: 9
      const newSelection = 2; // would be 10, but out of range of total images
      const expectedSelect = new Selection([], null);
      const expectedFragment = null;

      fixture.ngZone!.run(() => {
        startAt(query, startSelection);
        selectionManager.setSelection(newSelection, false);
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(route.snapshot.fragment).toBe(expectedFragment);
      });
    }));

    it('should do nothing if loading', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 0);
      const startSelection = 1; // image: 1
      const newQuery = new GroupQuery(4, 0);
      const newSelection = 2; // would be 2
      const expectedSelect = singleSelection(mockItems[1]); // unchanged.
      const expectedFragment = '1'; // unchangd.

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);
        component.refreshQuery();
        tick();
        selectionManager.setSelection(newSelection, false);
        tick();

        expect(selectionSpy).not.toHaveBeenCalled();
        expect(route.snapshot.fragment).toBe(expectedFragment);

        mockQueryObject.resolve(); // to avoid ngZone errors.
      });
    }));

    it('should set URL fragment when selecting prev image on page', fakeAsync(() => {
      const query = new GroupQuery(3, 0); // images: 0,1,2
      const startSelection = 1; // image: 1
      const expectedFragment = '0'; // next image
      const expectedParams = query.toParams(); // unchanged

      fixture.ngZone!.run(() => {
        startAt(query, startSelection);
        expect(route.snapshot.fragment).not.toBe(expectedFragment);

        selectionManager.setSelection(0, true);
        tick();

        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
      });
    }));

    it('should not notify selection listeners if selection unchanged', fakeAsync(() => {
      const query = new GroupQuery(3, 1); // images: 3,4,5
      const selection = 2; // image: 5
      const expectedSelect = singleSelection(mockItems[5]);

      fixture.ngZone!.run(() => {
        startAt(query, selection);
        tick();

        let counter = 0;

        // We dont expect this to ever execute because subject unchanged
        component.selectionChange.subscribe(() => {
          expect(counter).toBe(0);
          counter += 1;
        });

        selectionManager.setSelection(selection, false);
        tick();
        selectionManager.setSelection(selection, false);
        tick();
        selectionManager.setSelection(selection, false);
        tick();
      });
    }));
  });

  describe('onBoundaryEvent', () => {

    it('should increment page and select first image when at end of current page', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 0); // images: 0,1,2
      const startSelection = 2; // selected 2 (end of page)
      const expectedQuery = new GroupQuery(3, 1);
      const expectedSelect = singleSelection(mockItems[3]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(component.query).not.toEqual(expectedQuery);
        expect(querySpy).not.toHaveBeenCalled();
        expect(selectionSpy).not.toHaveBeenCalled();

        selectionManager.boundaryEvent$.next('next');
        tick();
        mockQueryObject.resolve();
        tick();

        expect(component.query).toEqual(expectedQuery);
        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should set canonical URL when at end of current page', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 0); // images: 0,1,2
      const startSelection = 2; // selected 2 (end of page)
      const expectedParams = new GroupQuery(3, 1).toParams();
      const expectedFragment = '0';

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(route.snapshot.fragment).not.toBe(expectedFragment);
        expect(route.snapshot.queryParams).not.toEqual(expectedParams);

        selectionManager.boundaryEvent$.next('next');
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.fragment).toBe(expectedFragment);
        expect(route.snapshot.queryParams).toEqual(expectedParams);
      });
    }));

    it('should decrement page and select last image when at start of current page', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 2); // images: 6,7,8
      const startSelection = 0; // selected 6 (start of page)
      const expectedQuery = new GroupQuery(3, 1);
      const expectedSelect = singleSelection(mockItems[5]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(component.query).not.toEqual(expectedQuery);
        expect(querySpy).not.toHaveBeenCalled();
        expect(selectionSpy).not.toHaveBeenCalled();

        selectionManager.boundaryEvent$.next('prev');
        tick();
        mockQueryObject.resolve();
        tick();

        expect(component.query).toEqual(expectedQuery);
        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should do nothing if loading', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 0); // images 0, 1, 2
      const startSelection = 0; // image: 0
      const newQuery = new GroupQuery(4, 0);
      const newSelection = 2; // would be 2
      const expectedSelect = singleSelection(mockItems[1]); // unchanged.
      const expectedFragment = '0'; // unchangd.
      const boundarySpy = spyOn(selectionManager.boundaryEvent$, 'emit').and.callThrough();

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);
        component.onPageEvent({pageSize: 3, pageIndex: 1}); // trigger load by changing page
        tick();

        expect(boundarySpy).not.toHaveBeenCalled();
        selectionManager.navigate({direction: 'prev', extend: false});
        expect(boundarySpy).toHaveBeenCalledWith('prev');
        tick();

        expect(selectionSpy).not.toHaveBeenCalled();
        expect(route.snapshot.fragment).toBe(expectedFragment);

        mockQueryObject.resolve(); // to avoid ngZone errors.
      });
    }));

  });

  describe('onPageEvent', () => {

    it('should re-query when changing page', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 0); // images: 0,1,2,3
      const startSelection = 2; // image: 2
      const onPageEvent = {pageSize: 4, pageIndex: 1};
      const expectedQuery = new GroupQuery(4, 1);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(querySpy).not.toHaveBeenCalled();

        component.onPageEvent(onPageEvent);
        tick();

        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(component.query).toEqual(expectedQuery);
      });
    }));

    it('should select first index when changing page', fakeAsync(() => {
      const startQuery = new GroupQuery(3, 1); // images: 3,4,5
      const startSelection = 1; // image: 4
      const onPageEvent = {pageSize: 3, pageIndex: 0}; // back a page
      const expectedSelect = singleSelection(mockItems[0]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(selectionSpy).not.toHaveBeenCalled();

        component.onPageEvent(onPageEvent);
        tick();
        mockQueryObject.resolve();
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should set canonical URL when changing page', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 0); // images: 0,1,2,3
      const startSelection = 2; // image: 2
      const onPageEvent = {pageSize: 4, pageIndex: 1};
      const expectedQueryParams = new GroupQuery(4, 1).toParams();
      const expectedFragment = '0';

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(route.snapshot.queryParams).not.toEqual(expectedQueryParams);
        expect(route.snapshot.fragment).not.toEqual(expectedFragment);

        component.onPageEvent(onPageEvent);
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.queryParams).toEqual(expectedQueryParams);
        expect(route.snapshot.fragment).toEqual(expectedFragment);
      });
    }));

    it('should preserve selected image when changing page size - simple', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 0); // images: 0,1,2,3
      const startSelection = 1; // image: 1
      const onPageEvent = {pageSize: 6, pageIndex: 0}; // images: 0,1,2,3,4,5
      const expectedSelect = singleSelection(mockItems[1]); // unchanged

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        component.onPageEvent(onPageEvent);
        tick();
        mockQueryObject.resolve();
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should re-query when changing page size - simple', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 0); // images: 0,1,2,3
      const startSelection = 1; // image: 1
      const onPageEvent = {pageSize: 6, pageIndex: 0}; // ids=0,..,5
      const expectedQuery = new GroupQuery(6, 0);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        component.onPageEvent(onPageEvent);
        tick();
        mockQueryObject.resolve();
        tick();

        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(component.query).toEqual(expectedQuery);
      });
    }));

    it('should set fragment and query params correctly when changing page size - simple', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 0); // images: 0,1,2,3
      const startSelection = 1; // image: 1
      const onPageEvent = {pageSize: 6, pageIndex: 0}; // ids=0,..,5
      const expectedQueryParams = new GroupQuery(6, 0).toParams();
      const expectedFragment = '1'; // should preserve since index in view

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(route.snapshot.queryParams).not.toEqual(expectedQueryParams);
        expect(route.snapshot.fragment).toEqual(expectedFragment);

        component.onPageEvent(onPageEvent);
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.queryParams).toEqual(expectedQueryParams);
        expect(route.snapshot.fragment).toEqual(expectedFragment);
      });
    }));

    it('should preserve selected image when changing page size - complex', fakeAsync(() => {
      // Here the page offset is non-zero so when the page size changes, the system must compute how
      // to alter the page offset to keep the selected image in view.

      // Note: this is a very important test. A bug was caught because of this test. It's very
      // important that we test that the new 'fragment' is correct because this indicates the
      // internal selectedIndex used by the object is correct (it wasnt before).
      const startQuery = new GroupQuery(3, 2); // images: 6,7,8
      const startSelection = 1; // image: 7
      const onPageEvent = {pageSize: 5, pageIndex: 973}; // should ignore page index
      const expectedQuery = new GroupQuery(5, 1);
      const expectedFragment = '2';
      const expectedQueryParams = expectedQuery.toParams();
      const expectedSelect = singleSelection(mockItems[7]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        component.onPageEvent(onPageEvent);
        tick();
        mockQueryObject.resolve();
        tick();

        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(route.snapshot.queryParams).toEqual(expectedQueryParams);
        expect(route.snapshot.fragment).toEqual(expectedFragment);
      });
    }));
  });

  describe('refreshQueryAndViewImages', () => {

    it('should switch to view mode when query resolves', fakeAsync(() => {
      fixture.ngZone!.run(() => {
        startAt(new GroupQuery(3, 0), 2);

        expect(configService.viewMode$.value).not.toEqual('image');

        component.refreshQueryAndViewImage();
        tick();

        expect(configService.viewMode$.value).not.toEqual('image');

        mockQueryObject.resolve();
        tick();

        expect(configService.viewMode$.value).toEqual('image');
        expect(notificationService.error).toHaveBeenCalledTimes(0);
      });
    }));

    it('should not switch to view mode when query resolves but is empty', fakeAsync(() => {
      fixture.ngZone!.run(() => {
        startAt(new GroupQuery(3, 0), 2);

        component.mockQueryItems.setItems([]);

        expect(configService.viewMode$.value).not.toEqual('image');

        component.refreshQueryAndViewImage();
        tick();
        mockQueryObject.resolve();
        tick();

        expect(configService.viewMode$.value).not.toEqual('image');
        expect(notificationService.error).toHaveBeenCalledTimes(1);
      });

    }));

  });

  describe('onSortChange', () => {

    it('should sort by id when id header clicked', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 1); // images: 4,5,6,7
      const startSelection = 2; // image: 5
      const sortCol = 'id';
      const sortDir = 'asc';
      const expectedQuery = new GroupQuery(4, 0);
      expectedQuery.sort = {active: sortCol, direction: sortDir};

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(querySpy).not.toHaveBeenCalled();

        component.onSortChange({active: sortCol, direction: sortDir});
        tick();

        expect(querySpy).toHaveBeenCalledWith(expectedQuery);
        expect(component.query).toEqual(expectedQuery);
      });
    }));

    it('should select first index when changing sort', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 1); // images: 4,5,6,7
      const startSelection = 2; // image: 5
      const sortCol = 'id';
      const sortDir = 'asc';
      const expectedIndex = 0;
      const expectedSelect = singleSelection(mockItems[0]);

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(querySpy).not.toHaveBeenCalled();

        component.onSortChange({active: sortCol, direction: sortDir});
        tick();
        mockQueryObject.resolve();
        tick();

        expect(selectionManager.getSelectedItems()).toEqual([mockItems[expectedIndex]]);
        expect(selectionSpy).toHaveBeenCalledWith(expectedSelect);
      });
    }));

    it('should set canonical URL when changing sort', fakeAsync(() => {
      const startQuery = new GroupQuery(4, 0); // images: 0,1,2,3
      const startSelection = 2; // image: 2
      const sortCol = 'id';
      const sortDir = 'asc';
      const expectedQuery = new GroupQuery(4, 0);
      expectedQuery.sort = {active: sortCol, direction: 'asc'};
      const expectedFragment = '0';

      fixture.ngZone!.run(() => {
        startAt(startQuery, startSelection);

        expect(route.snapshot.queryParams).not.toEqual(expectedQuery.toParams());
        expect(route.snapshot.fragment).not.toEqual(expectedFragment);

        component.onSortChange({active: sortCol, direction: sortDir});
        tick();
        mockQueryObject.resolve();
        tick();

        expect(route.snapshot.queryParams).toEqual(expectedQuery.toParams());
        expect(route.snapshot.fragment).toEqual(expectedFragment);
      });
    }));
  });
});
