import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router, } from '@angular/router';
import {BehaviorSubject, Observable, } from 'rxjs';

import {
  Action,
  ArtistDetails,
  ArtistQuery,
  ContentEigendata,
  DeletedQuery,
  Direction,
  DupeCandidate,
  DupeCandidatesQuery,
  DupeType,
  DUPE_TYPES,
  EigendataQuery,
  Image,
  ImageQuery,
  ImageSelection,
  NameDetails,
  NameQuery,
  Selection,
  TagDetails,
  TagQuery,
} from '@app/models';
import {
  ActionService,
  ConfigService,
  ImageFetcherService,
  LibraryService,
  NotificationService,
  QueryArtistsResponse,
  QueryDupeCandidatesResponse,
  QueryEigendataResponse,
  QueryImagesResponse,
  QueryNamesResponse,
  QueryTagsResponse,
} from '@app/services';
import {DialogService} from '@app/shared/dialog';

import {
  mixinPagedBrowser,
  mixinReloadOnDelete,
  mixinSortedBrowser,
  BaseBrowserDirective,
  PagedBrowser,
  SelectionBrowser,
  SimpleSelectionBrowserDirective,
  SortedBrowser,
} from './base-browser.directive';
import {ImageTableComponent, SelectionManager, } from './tables';


abstract class _ArtistBrowserBase extends SimpleSelectionBrowserDirective<ArtistDetails, ArtistQuery> {}
const _ArtistBrowser = mixinSortedBrowser(mixinReloadOnDelete(_ArtistBrowserBase, 'artist'));

@Component({
  selector: 'rx-artist-browser',
  templateUrl: './artist-browser.component.html',
  styleUrls: ['./browsers.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArtistBrowserComponent extends _ArtistBrowser implements SortedBrowser {

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,ArtistQuery);
  }

  protected queryLibrary(): Observable<QueryArtistsResponse> {
    return this.libraryService.queryArtists(this.query);
  }

}

abstract class _DeletedBrowserBase extends SimpleSelectionBrowserDirective<Image, DeletedQuery> {}
const _DeletedBrowser = mixinPagedBrowser(mixinReloadOnDelete(_DeletedBrowserBase, 'image'));

@Component({
  selector: 'rx-deleted-browser',
  templateUrl: './deleted-browser.component.html',
  styleUrls: ['./browsers.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeletedBrowserComponent extends _DeletedBrowser implements PagedBrowser
{

  ActionRestoreSelected = Action.RestoreSelected;
  ActionPermadeleteSelected = Action.PermadeleteSelected;

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
    protected dialogService: DialogService,
    protected imageFetcherService: ImageFetcherService,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,DeletedQuery);
  }

  protected queryLibrary(): Observable<QueryImagesResponse> {
    return this.libraryService.queryDeleted(this.query);
  }

}

abstract class _DupeCandidatesBrowserBase extends SimpleSelectionBrowserDirective<DupeCandidate, DupeCandidatesQuery> {}
const _DupeCandidatesBrowser = mixinPagedBrowser(_DupeCandidatesBrowserBase);

@Component({
  selector: 'rx-dupe-candidates-browser',
  templateUrl: './dupe-candidates-browser.component.html',
  styleUrls: ['./dupe-candidates-browser.component.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DupeCandidatesBrowserComponent extends _DupeCandidatesBrowser implements PagedBrowser
{

  DUPE_TYPES = DUPE_TYPES;

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router, DupeCandidatesQuery);
    this.selectionManager.selectionChange$.subscribe(() => {
      this.changeRef.markForCheck();
    });
  }

  markAsDupes(candidate: DupeCandidate, dupeType: DupeType) {
    this.libraryService.markDupes([candidate.item1, candidate.item2], dupeType).subscribe(() => {
      candidate.mark = dupeType;
      this.changeRef.markForCheck();
    });
  }

  unmarkAsDupes(candidate: DupeCandidate) {
    this.libraryService.unmarkDupes([candidate.item1, candidate.item2]).subscribe(() => {
      candidate.mark = null;
      this.changeRef.markForCheck();
    });
  }

  protected queryLibrary(): Observable<QueryDupeCandidatesResponse> {
    return this.libraryService.queryDupeCandidates(this.query);
  }

}


@Component({
  selector: 'rx-eigendata-browser',
  templateUrl: './eigendata-browser.component.html',
  styleUrls: ['./browsers.scss', './eigendata-browser.component.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EigendataBrowserComponent
extends SimpleSelectionBrowserDirective<ContentEigendata, EigendataQuery> {

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,EigendataQuery);
  }

  protected queryLibrary(): Observable<QueryEigendataResponse> {
    return this.libraryService.queryEigendata(this.query);
  }

}


abstract class _ImageBrowserBase extends BaseBrowserDirective<Image, ImageQuery> {}
const _ImageBrowser = mixinPagedBrowser(mixinSortedBrowser(mixinReloadOnDelete(_ImageBrowserBase, 'image')));

@Component({
  selector: 'rx-image-browser',
  templateUrl: './image-browser.component.html',
  styleUrls: ['./browsers.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageBrowserComponent extends _ImageBrowser implements PagedBrowser, SelectionBrowser<ImageSelection>, SortedBrowser
{

  @Input() displayMode: 'list' | 'thumbnails' = 'list';

  @Output()
  selectionChange: BehaviorSubject<ImageSelection | null> = new BehaviorSubject(null);

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
    protected dialogService: DialogService,
    protected imageFetcherService: ImageFetcherService,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,ImageQuery, ImageSelection);
  }

  protected emitSelectionChanged(selection: ImageSelection) {
    if (selection.item) {
      this.imageFetcherService.prefetchFromImage(selection.item);
    }
    this.selectionChange.next(selection);
  }

  protected queryLibrary(): Observable<QueryImagesResponse> {
    return this.libraryService.queryImages(this.query);
  }

  protected override handleQueryResponse(response: QueryImagesResponse) {
    this.imageFetcherService.setExpectedImages(response.items);
    super.handleQueryResponse(response);
  }

}


abstract class _NameBrowserBase extends SimpleSelectionBrowserDirective<NameDetails, NameQuery> {}
const _NameBrowser = mixinSortedBrowser(mixinReloadOnDelete(_NameBrowserBase, 'name'));

@Component({
  selector: 'rx-name-browser',
  templateUrl: './name-browser.component.html',
  styleUrls: ['./browsers.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NameBrowserComponent extends _NameBrowser implements SortedBrowser {

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,NameQuery);
  }

  protected queryLibrary(): Observable<QueryNamesResponse> {
    return this.libraryService.queryNames(this.query);
  }

}


abstract class _TagBrowserBase extends SimpleSelectionBrowserDirective<TagDetails, TagQuery> {}
const _TagBrowser = mixinSortedBrowser(mixinReloadOnDelete(_TagBrowserBase, 'tag'));

@Component({
  selector: 'rx-tag-browser',
  templateUrl: './tag-browser.component.html',
  styleUrls: ['./browsers.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagBrowserComponent extends _TagBrowser implements SortedBrowser {

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,TagQuery);
  }

  protected queryLibrary(): Observable<QueryTagsResponse> {
    return this.libraryService.queryTags(this.query);
  }

}
