import {Component, Directive, EventEmitter, Input, Output, ViewChild, } from '@angular/core';

import {MatMenuTrigger} from '@angular/material/menu';

import {ContentMeta, FlagFilter, GroupQuery, ImageQuery, } from '@app/models';


@Directive()
class BaseQueryFilterMenu<QueryType> {

  readonly FLAG_FILTER_ALL = FlagFilter.All;
  readonly FLAG_FILTER_TRUE = FlagFilter.ForceTrue;
  readonly FLAG_FILTER_FALSE = FlagFilter.ForceFalse;
  readonly PAGE_SIZE_OPTIONS: number[] = [5, 10, 25, 100];

  @Input() query: QueryType;

  @Output() changed: EventEmitter<void> = new EventEmitter();

  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;

  constructor() { }

  open(event) {
    event.stopPropagation();
    this.menuTrigger.openMenu();
  }

}

@Component({
  selector: 'rx-image-filter-menu',
  templateUrl: './image-filter-menu.component.html',
  styleUrls: ['./query-filter-menu.component.scss']
})
export class ImageFilterMenuComponent extends BaseQueryFilterMenu<ImageQuery> {

  addNotContentFilter(content: ContentMeta) {
    if (content.type === 'tag') {
      this.query.notTags.push(content.item.id);
    } else if (content.type === 'name') {
      this.query.notNames.push(content.item.id);
    } else if (content.type === 'feature') {
      this.query.notFeatures.push(content.item.id);
    } else {
      throw new Error('Invalid content type for not filter');
    }
    this.changed.emit();
  }

}


@Component({
  selector: 'rx-group-filter-menu',
  templateUrl: './group-filter-menu.component.html',
  styleUrls: ['./query-filter-menu.component.scss']
})
export class GroupFilterMenuComponent extends BaseQueryFilterMenu<GroupQuery> {}
