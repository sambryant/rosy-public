import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatRadioModule} from '@angular/material/radio';

import {AutoInputModule} from '@app/shared/auto-input';
import {EditableModule} from '@app/shared/editable';

import {GroupFilterMenuComponent, ImageFilterMenuComponent, } from './query-filter-menu.component';


@NgModule({
  declarations: [
    GroupFilterMenuComponent,
    ImageFilterMenuComponent,
  ],
  exports: [
    GroupFilterMenuComponent,
    ImageFilterMenuComponent,
  ],
  imports: [
    AutoInputModule,
    CommonModule,
    EditableModule,
    FormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatRadioModule,
  ]
})
export class QueryFilterMenuModule { }
