import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule} from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {GroupQuery, ImageQuery, } from '@app/models';

import {GroupFilterMenuComponent, ImageFilterMenuComponent, } from '.';


describe('GroupFilterMenuComponent', () => {
  let component: GroupFilterMenuComponent;
  let fixture: ComponentFixture<GroupFilterMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GroupFilterMenuComponent
      ],
      imports: [
        FormsModule,
        MatDatepickerModule,
        MatInputModule,
        MatMenuModule,
        MatRadioModule,
        MatNativeDateModule,
        NoopAnimationsModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupFilterMenuComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.query = new GroupQuery();
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});


describe('ImageFilterMenuComponent', () => {
  let component: ImageFilterMenuComponent;
  let fixture: ComponentFixture<ImageFilterMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ImageFilterMenuComponent
      ],
      imports: [
        FormsModule,
        MatDatepickerModule,
        MatInputModule,
        MatMenuModule,
        MatRadioModule,
        MatNativeDateModule,
        NoopAnimationsModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageFilterMenuComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.query = new ImageQuery();
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
