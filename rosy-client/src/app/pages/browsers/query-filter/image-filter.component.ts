import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

import {ContentMeta, ContentType, ImageQuery, Range, } from '@app/models';

import {ContentQueryFilterDirective} from './query-filter.directive';


@Component({
  selector: 'rx-image-filter',
  templateUrl: './image-filter.component.html',
  styleUrls: ['./query-filter.directive.scss']
})
export class ImageFilterComponent extends ContentQueryFilterDirective<ImageQuery> {

  readonly ADD_CONTENT_TYPES: ContentType[] = ['tag', 'name', 'artist', 'group', 'feature'];

  constructor() { super(); }

  override addContentFilter(c: ContentMeta) {
    if (c.type === 'artist') {
      this.query.artists.push(c.item.id);
      this.changed();
    } else if (c.type === 'group') {
      this.query.groups.push(c.item.id);
      this.changed();
    } else if (c.type === 'feature') {
      this.query.features.push(c.item.id);
      this.changed();
    } else {
      super.addContentFilter(c);
    }
  }

}
