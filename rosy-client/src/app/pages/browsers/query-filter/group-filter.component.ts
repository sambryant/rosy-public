import {Component} from '@angular/core';

import {GroupQuery} from '@app/models';

import {ContentQueryFilterDirective} from './query-filter.directive';


@Component({
  selector: 'rx-group-filter',
  templateUrl: './group-filter.component.html',
  styleUrls: ['./query-filter.directive.scss']
})
export class GroupFilterComponent extends ContentQueryFilterDirective<GroupQuery> {

  constructor() { super(); }

}
