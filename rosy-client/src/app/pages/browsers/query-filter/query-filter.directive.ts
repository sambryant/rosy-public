import {
  Directive,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

import {ContentMeta, FlagFilter, Query, Range, TaggedQuery, } from '@app/models';


@Directive()
export abstract class QueryFilterDirective<T extends Query> {

  readonly PAGE_SIZE_OPTIONS: number[] = [5, 10, 25, 100];
  readonly FLAG_FILTER_ALL = FlagFilter.All;
  readonly FLAG_FILTER_TRUE = FlagFilter.ForceTrue;
  readonly FLAG_FILTER_FALSE = FlagFilter.ForceFalse;

  /**
   * The query object to model.
   */
  @Input() query: T;

  /**
   * Fires whenever the query changes.
   */
  @Output() queryChange = new EventEmitter<T>();

  /**
   * When true, renders suitably for mobile view
   */
  @Input() compact = false;

  constructor() {}

  changed() {
    this.queryChange.emit(this.query);
  }

}

@Directive()
export abstract class ContentQueryFilterDirective<T extends Query & TaggedQuery>
extends QueryFilterDirective<T> {

  addContentFilter({type, item}: ContentMeta) {
    if (type === 'name') {
      this.query.names.push(item.id);
      this.changed();
    } else if (type === 'tag') {
      this.query.tags.push(item.id);
      this.changed();
    }
  }

  removeRange(rangeField: Range<any>) {
    rangeField.lower = null;
    rangeField.upper = null;
    this.changed();
  }

}
