import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {NgModule} from '@angular/core';

import {AutoInputModule} from '@app/shared/auto-input';
import {ChipModule} from '@app/shared/chip';
import {EditableModule} from '@app/shared/editable';
import {PipesModule} from '@app/shared/pipes';

import {DupeCandidatesFilterComponent} from './dupe-candidates-filter.component';
import {EigendataFilterComponent} from './eigendata-filter.component';
import {GroupFilterComponent} from './group-filter.component';
import {ImageFilterComponent} from './image-filter.component';
import {QueryFilterDirective} from './query-filter.directive';
import {QueryFilterMenuModule} from './query-filter-menu';


@NgModule({
  declarations: [
    DupeCandidatesFilterComponent,
    EigendataFilterComponent,
    GroupFilterComponent,
    ImageFilterComponent,
  ],
  exports: [
    DupeCandidatesFilterComponent,
    EigendataFilterComponent,
    GroupFilterComponent,
    ImageFilterComponent,
  ],
  imports: [
    AutoInputModule,
    ChipModule,
    CommonModule,
    FormsModule,
    EditableModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    PipesModule,
    QueryFilterMenuModule,
  ]
})
export class QueryFilterModule { }
