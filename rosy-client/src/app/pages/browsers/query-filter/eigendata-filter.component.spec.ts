import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {EigendataQuery} from '@app/models';

import {EigendataFilterComponent} from '.';

describe('EigendataFilterComponent', () => {
  let component: EigendataFilterComponent;
  let fixture: ComponentFixture<EigendataFilterComponent>;

  let query: EigendataQuery;
  let queryChangeSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        EigendataFilterComponent
      ],
       imports: [
        NoopAnimationsModule
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EigendataFilterComponent);
    component = fixture.componentInstance;
    queryChangeSpy = spyOn(component.queryChange, 'emit').and.callThrough();
    query = new EigendataQuery();
    component.query = query;
    fixture.detectChanges();
  });

  it('should fire query change when numVecs changes in input to valid value', () => {
    const inputVal = '158';
    const expValue = 158;
    let input = fixture.nativeElement.querySelector('#numVecsInput');

    input.value = inputVal;
    input.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    expect(query.numVecs).toBe(expValue);
    expect(queryChangeSpy).toHaveBeenCalledTimes(1);
  });

  it('should do nothing when numVecs changes in input to invalid value', () => {
    const inputVal = '0';
    const expValue = query.numVecs;
    let input = fixture.nativeElement.querySelector('#numVecsInput');

    input.value = inputVal;
    input.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    expect(query.numVecs).toBe(expValue);
    expect(queryChangeSpy).not.toHaveBeenCalled();
  });

  it('should fire query change when numWeights changes in input to valid value', () => {
    const inputVal = '158';
    const expValue = 158;
    let input = fixture.nativeElement.querySelector('#numWeightsInput');

    input.value = inputVal;
    input.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    expect(query.numWeights).toBe(expValue);
    expect(queryChangeSpy).toHaveBeenCalledTimes(1);
  });

  it('should do nothing when numWeights changes in input to invalid value', () => {
    const inputVal = '0';
    const expValue = query.numWeights;
    let input = fixture.nativeElement.querySelector('#numWeightsInput');

    input.value = inputVal;
    input.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    expect(query.numWeights).toBe(expValue);
    expect(queryChangeSpy).not.toHaveBeenCalled();
  });
});
