import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import {DupeCandidatesQuery} from '@app/models';

import {QueryFilterDirective} from './query-filter.directive';


@Component({
  selector: 'rx-dupe-candidates-filter',
  templateUrl: './dupe-candidates-filter.component.html',
  styleUrls: ['./dupe-candidates-filter.component.scss']
})
export class DupeCandidatesFilterComponent extends QueryFilterDirective<DupeCandidatesQuery>{

  constructor() { super(); }

}
