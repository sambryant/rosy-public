import {Component} from '@angular/core';

import {EigendataQuery} from '@app/models';

import {QueryFilterDirective} from './query-filter.directive';


@Component({
  selector: 'rx-eigendata-filter',
  templateUrl: './eigendata-filter.component.html',
  styleUrls: ['./eigendata-filter.component.scss']
})
export class EigendataFilterComponent extends QueryFilterDirective<EigendataQuery> {

  constructor() { super(); }

  updateNumVecs(target: HTMLInputElement) {
    if (target.validity.valid) {
      this.query.numVecs = parseInt(target.value, 10);
      this.changed();
    }
  }

  updateNumWeights(target: HTMLInputElement) {
    if (target.validity.valid) {
      this.query.numWeights = parseInt(target.value, 10);
      this.changed();
    }
  }

}
