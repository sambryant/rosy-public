import {ComponentFixture, TestBed, } from '@angular/core/testing';

import {DupeCandidatesQuery} from '@app/models';

import {DupeCandidatesFilterComponent} from './dupe-candidates-filter.component';

describe('DupeCandidatesFilterComponent', () => {
  let component: DupeCandidatesFilterComponent;
  let fixture: ComponentFixture<DupeCandidatesFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DupeCandidatesFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DupeCandidatesFilterComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.query = new DupeCandidatesQuery();
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
