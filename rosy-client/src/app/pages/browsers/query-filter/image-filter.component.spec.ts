import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ImageQuery} from '@app/models';
import {MockArtist, MockGroup, MockName, MockTag, } from '@app/models/testing';

import {ImageFilterComponent} from '.';


describe('ImageFilterComponent', () => {
  let component: ImageFilterComponent;
  let fixture: ComponentFixture<ImageFilterComponent>;

  let query: ImageQuery;
  let queryChangeSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ImageFilterComponent,
      ],
      imports: [
        NoopAnimationsModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();

    query = new ImageQuery();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageFilterComponent);
    component = fixture.componentInstance;
    component.query = query;
    queryChangeSpy = spyOn(component.queryChange, 'emit').and.callThrough();
    fixture.detectChanges();

    expect(queryChangeSpy).not.toHaveBeenCalled();
  });

  it('should add an artist filter', () => {
    expect(query.artists).toEqual([]);
    component.addContentFilter({type: 'artist', item: MockArtist.new(1)});
    expect(queryChangeSpy).toHaveBeenCalledOnceWith(query);
    expect(query.artists).toEqual([1]);
    component.addContentFilter({type: 'artist', item: MockArtist.new(2)});
    expect(query.artists).toEqual([1, 2]);
  });

  it('should add a group filter', () => {
    expect(query.groups).toEqual([]);
    component.addContentFilter({type: 'group', item: MockGroup.new(1)});
    expect(queryChangeSpy).toHaveBeenCalledOnceWith(query);
    expect(query.groups).toEqual([1]);
    component.addContentFilter({type: 'group', item: MockGroup.new(2)});
    expect(query.groups).toEqual([1, 2]);
  });

  it('should add a name filter', () => {
    expect(query.names).toEqual([]);
    component.addContentFilter({type: 'name', item: MockName.new(1)});
    expect(queryChangeSpy).toHaveBeenCalledOnceWith(query);
    expect(query.names).toEqual([1]);
    component.addContentFilter({type: 'name', item: MockName.new(2)});
    expect(query.names).toEqual([1, 2]);
  });

  it('should add a tag filter', () => {
    expect(query.tags).toEqual([]);
    component.addContentFilter({type: 'tag', item: MockTag.new(1)});
    expect(queryChangeSpy).toHaveBeenCalledOnceWith(query);
    expect(query.tags).toEqual([1]);
    component.addContentFilter({type: 'tag', item: MockTag.new(2)});
    expect(query.tags).toEqual([1, 2]);
  });

  it('should remove range fields', () => {
    query.createdTime.upper = {} as Date;
    component.removeRange(query.createdTime);
    expect(query.createdTime.upper).toBeNull();

    query.processedTime.lower = {} as Date;
    component.removeRange(query.processedTime);
    expect(query.processedTime.lower).toBeNull();

    query.rating.upper = 3;
    component.removeRange(query.rating);
    expect(query.rating.upper).toBeNull();

    query.ratingCount.upper = 3;
    component.removeRange(query.ratingCount);
    expect(query.ratingCount.upper).toBeNull();
  });

});
