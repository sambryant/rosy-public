import {Component, ViewChild, } from '@angular/core';

import {Action} from '@app/models';

import {ContextMenuComponent} from '@app/shared/context-menu';


@Component({
  selector: 'rx-selection-context-menu',
  templateUrl: './selection-context-menu.component.html',
})
export class SelectionContextMenuComponent {

  AddArtistToSelected = Action.AddArtistToSelected;
  AddGroupToSelected = Action.AddGroupToSelected;
  AddNameToSelected = Action.AddNameToSelected;
  AddTagToSelected = Action.AddTagToSelected;
  CreateNewGroup = Action.CreateNewGroup;
  DeleteSelected = Action.DeleteSelected;
  MarkDupesExactFromSelected = Action.MarkDupesExactFromSelected;
  MarkDupesMostFromSelected = Action.MarkDupesMostFromSelected;
  MarkDupesPartialFromSelected = Action.MarkDupesPartialFromSelected;
  MarkDupesNoneFromSelected = Action.MarkDupesNoneFromSelected;
  RemoveArtistFromSelected = Action.RemoveArtistFromSelected;
  RemoveGroupFromSelected = Action.RemoveGroupFromSelected;
  RemoveNameFromSelected = Action.RemoveNameFromSelected;
  RemoveTagFromSelected = Action.RemoveTagFromSelected;
  ShowMarkedDupes = Action.ShowMarkedDupes;
  ShowFaceMatches = Action.ShowFaceMatches;
  UnmarkDupesFromSelected = Action.UnmarkDupesFromSelected;

  @ViewChild(ContextMenuComponent, {static: true}) protected menuWrapper: ContextMenuComponent;

  constructor() {}

  open(event: MouseEvent) {
    this.menuWrapper.open(event);
  }
}
