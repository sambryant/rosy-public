import {Component, ViewChild, } from '@angular/core';

import {Action} from '@app/models';

import {ContextMenuComponent} from '@app/shared/context-menu';


@Component({
  selector: 'rx-delete-selection-context-menu',
  templateUrl: './delete-selection-context-menu.component.html',
})
export class DeleteSelectionContextMenuComponent {

  PermadeleteSelected = Action.PermadeleteSelected;

  RestoreSelected = Action.RestoreSelected;

  @ViewChild(ContextMenuComponent, {static: true}) protected menuWrapper: ContextMenuComponent;

  constructor() {}

  open(event: MouseEvent) {
    this.menuWrapper.open(event);
  }

}
