import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatMenuModule} from '@angular/material/menu';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ContextMenuComponent} from '@app/shared/context-menu';

import {SelectionContextMenuComponent} from '.';

describe('SelectionContextMenuComponent', () => {
  let component: SelectionContextMenuComponent;
  let fixture: ComponentFixture<SelectionContextMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SelectionContextMenuComponent,
        ContextMenuComponent,
      ],
      imports: [
        MatMenuModule,
        NoopAnimationsModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should open on click', () => {
    component.open(new MouseEvent('click', { bubbles: true }));
    fixture.detectChanges();
  });
});
