import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
  OnInit,
  ViewChild,
} from '@angular/core';
import {merge, BehaviorSubject, Observable, } from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router, } from '@angular/router';

import {
  Action,
  Direction,
  Group,
  GroupQuery,
  GroupSelection,
  Image,
  ImageSelection,
  NavigationRequest,
} from '@app/models';
import {
  ActionService,
  ConfigService,
  LibraryService,
  NotificationService,
  QueryGroupsResponse,
} from '@app/services';
import {DialogService} from '@app/shared/dialog';

import {GroupTableComponent, SelectionManager, } from './tables';
import {
  mixinPagedBrowser,
  mixinReloadOnDelete,
  mixinSortedBrowser,
  BaseBrowserDirective,
  PagedBrowser,
  SelectionBrowser,
  SortedBrowser,
} from './base-browser.directive';


export type GroupBrowserSelection = GroupSelection | ImageSelection | null;


abstract class _GroupBrowserBase extends BaseBrowserDirective<Group, GroupQuery> {}
const _GroupBrowser = mixinPagedBrowser(
  mixinSortedBrowser(
    mixinReloadOnDelete(mixinReloadOnDelete(_GroupBrowserBase, 'group'), 'image')));

@Component({
  selector: 'rx-group-browser',
  templateUrl: './group-browser.component.html',
  styleUrls: ['./browsers.scss'],
  outputs: ['selectionChange'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroupBrowserComponent extends _GroupBrowser
implements OnInit, PagedBrowser, SelectionBrowser<GroupBrowserSelection>, SortedBrowser
{

  @Input() displayMode: 'list' | 'thumbnails' = 'list';

  @Output() selectionChange = new BehaviorSubject<GroupBrowserSelection | null>(null);

  /**
   * These manage the selection for the `ImageTableComponent` used for each `Group.images`.
   */
  imageSelectionManagers: SelectionManager<Image, ImageSelection>[] = [];

  /**
   * Either `selectionManager` or one of `imageSelectionManagers`.
   */
  currentSelectionManager: SelectionManager<Image, ImageSelection> | SelectionManager<Group, GroupSelection>;

  /**
   * Index of currently active group regardless of whether `selectionManager` or one of
   * `imageSelectionManagers` is the current active selection manager.
   *
   * This is only used to set the fragment correctly.
   */
  protected currentSelectionGroupIndex: number | null = null;

  /**
   * Component responsible for rendering the view of the groups
   */
  @ViewChild(GroupTableComponent) groupTable: GroupTableComponent;

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
    protected dialogService: DialogService,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router,GroupQuery, GroupSelection);
    this.currentSelectionManager = this.selectionManager;
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.libraryService.groupImagesUpdated$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((ids) => {
        this.reloadQuery(this.currentSelectionGroupIndex || 0);
      });
  }

  /**
   * Performs navigation on this tree-like object, overrides parent implementation.
   *
   * Basically, if a group row is selected, navigate forward to the first of its child images or
   * backward to the last of the previous group's images. If an image is selected, just navigate
   * within that `ImageTableComponent` as usual.
   */
  override navigate({direction, extend}: NavigationRequest) {
    if (this.currentSelectionManager === null || !this.currentSelectionManager.hasSelection()) {
      super.navigate({direction, extend});
    } else if (this.currentSelectionManager === this.selectionManager) {
      const nextIndex = this.selectionManager.getCursorIndex()! + (direction === 'prev' && -1 || 0);
      if (this.configService.mobileMode$.value) {
        super.navigate({direction, extend});
      } else if (this.groupTable.isRowExpanded(nextIndex) === false) {
        // If target is collapsed, we ignore images and just navigate through groups
        super.navigate({direction, extend});
      } else if (!extend) {
        // Otherwise we use the image table for target to navigate
        if (nextIndex >= 0 && nextIndex < this.items.length) {
          this.imageSelectionManagers[nextIndex].navigate({direction, extend});
        } else {
          this.onBoundaryEvent(direction);
        }
      }
    } else {
      this.currentSelectionManager.navigate({direction, extend});
    }
  }

  protected emitSelectionChanged(selection: GroupSelection) {
    this.doEmitSelectionChanged(this.selectionManager.getCursorIndex(), this.selectionManager, selection);
  }

  protected onImageSelectionChanged(
    groupIndex: number, mngr: SelectionManager<Image, ImageSelection>, selection: ImageSelection) {
    if (this.status !== 'loading' && this.ignoreSelectionChange.length === 0) {
      this.doEmitSelectionChanged(groupIndex, mngr, selection);
      this.writeStateToUrl();
    }
  }

  private doEmitSelectionChanged(
    groupIndex: number | null, mngr: SelectionManager<any, any>, selection: GroupBrowserSelection) {
    if (this.currentSelectionManager !== mngr && this.currentSelectionManager !== null) {
      // Deselect any leftover selection from previous manager and ignore subsequent change signal.
      this.ignoreSelectionChange.push(null);
      this.currentSelectionManager.deselectAll();
      this.ignoreSelectionChange.pop();
    }
    this.currentSelectionGroupIndex = groupIndex;
    this.currentSelectionManager = mngr;
    this.selectionChange.next(selection);
  }

  protected queryLibrary(): Observable<QueryGroupsResponse> {
    return this.libraryService.queryGroups(this.query);
  }

  /**
   * Takes the response of a query and updates up the corresponding `SelectionManager` instances.
   */
  protected override handleQueryResponse(response: QueryGroupsResponse) {
    super.handleQueryResponse(response);
    const items = response.items;

    // For each group, we need to create a `SelectionManager` instance to manage its child images.
    // These are then passed to an `ImageTableComponent` element.
    this.imageSelectionManagers = items.map(_ => new SelectionManager(ImageSelection));
    this.imageSelectionManagers.forEach((manager, groupIndex) => {
      manager.setItems(items[groupIndex].images);
      manager.selectionChange$
        .pipe(takeUntil(this.loadingStarted))
        .pipe(takeUntil(this.destroyed$))
        .subscribe((selection) => this.onImageSelectionChanged(groupIndex, manager, selection));
      manager.boundaryEvent$
        .pipe(takeUntil(this.loadingStarted))
        .pipe(takeUntil(this.destroyed$))
        .subscribe((direction) => this.onImageBoundaryEvent(groupIndex, direction));
    });
  }

  protected override getFragmentForSelection(): string | undefined {
    if (this.currentSelectionGroupIndex !== null) {
      return this.currentSelectionGroupIndex.toString();
    } else {
      return undefined;
    }
  }

  protected onImageBoundaryEvent(groupIndex: number, direction: Direction) {
    const newIndex = groupIndex + (direction === 'next' ? 1 : 0);
    if (newIndex >= 0 && newIndex < this.items.length) {
      this.selectionManager.setSelection(newIndex, false);
    } else {
      this.onBoundaryEvent(direction);
    }
  }

}
