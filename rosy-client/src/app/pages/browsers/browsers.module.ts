import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ContextMenuModule} from '@app/shared/context-menu';
import {GraphModule} from '@app/shared/graph';
import {PipesModule} from '@app/shared/pipes';
import {ServicesModule} from '@app/services';
import {ViewModeSelectorModule} from '@app/shared/view-mode-selector';

import {
  ArtistBrowserComponent,
  DeletedBrowserComponent,
  DupeCandidatesBrowserComponent,
  EigendataBrowserComponent,
  ImageBrowserComponent,
  NameBrowserComponent,
  TagBrowserComponent,
} from './browsers.component';
import {ActionButtonModule} from '@app/shared/action-button';
import {GroupBrowserComponent} from './group-browser.component';
import {ImageModule} from '@app/shared/image';
import {StatusOverlayModule} from '@app/shared/status-overlay';

import {QueryFilterModule} from './query-filter';
import {SelectionActionManagerModule} from './selection-action-manager';
import {SelectionContextMenuComponent, DeleteSelectionContextMenuComponent, } from './context-menu';
import {TablesModule} from './tables';


@NgModule({
  declarations: [
    ArtistBrowserComponent,
    DeletedBrowserComponent,
    DeleteSelectionContextMenuComponent,
    DupeCandidatesBrowserComponent,
    EigendataBrowserComponent,
    GroupBrowserComponent,
    ImageBrowserComponent,
    NameBrowserComponent,
    SelectionContextMenuComponent,
    TagBrowserComponent,
  ],
  exports: [
    ArtistBrowserComponent,
    DeletedBrowserComponent,
    DupeCandidatesBrowserComponent,
    EigendataBrowserComponent,
    GroupBrowserComponent,
    ImageBrowserComponent,
    NameBrowserComponent,
    TagBrowserComponent,
  ],
  imports: [
    ActionButtonModule,
    BrowserAnimationsModule,
    CommonModule,
    ContextMenuModule,
    FormsModule,
    GraphModule,
    ImageModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSortModule,
    MatTooltipModule,
    QueryFilterModule,
    RouterModule,
    PipesModule,
    SelectionActionManagerModule,
    ServicesModule,
    StatusOverlayModule,
    TablesModule,
    ViewModeSelectorModule,
  ]
})
export class BrowsersModule { }
