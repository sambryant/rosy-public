import {merge, BehaviorSubject, } from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Component, Input, OnInit, } from '@angular/core';

import {Action, ImageSelection, } from '@app/models';
import {ActionService, LibraryService, } from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

@Component({
  selector: 'rx-delete-selection-action-manager',
  template: '<div></div>',
})
export class DeleteSelectionActionManagerComponent
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  @Input() selection: BehaviorSubject<(ImageSelection | null)>;

  protected hasTargets$: BehaviorSubject<boolean>;

  constructor(
    private actionService: ActionService,
    private dialogService: DialogService,
    private libraryService: LibraryService) {
    super();
    this.hasTargets$ = new BehaviorSubject(false);
  }

  ngOnInit() {
    this.selection
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.updateActionStates(); });

    // Register actions interpretted by this component
    this.actionService.captureAction(Action.PermadeleteSelected, this.hasTargets$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.permadelete());
    this.actionService.captureAction(Action.RestoreSelected, this.hasTargets$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.restore());
  }

  protected permadelete() {
    const targets = this.selection.value!.selected.map(target => target.id);

    let message;
    if (targets.length === 1) {
      message = 'Are you sure you want to delete 1 image? (Cannot be undone)';
    } else {
      message = `Are you sure you want to delete ${targets.length} images? (Cannot be undone)`;
    }

    this.dialogService
      .showConfirm('Confirm permadelete', message)
      .subscribe((confirm) => {
        if (confirm) {
          this.libraryService.permadeleteImages(targets);
        }
      });
  }

  protected restore() {
    const targets = this.selection.value!.selected.map(target => target.id);
    this.libraryService.restoreImages(targets);
  }

  /**
   * Determines whether each action is enabled/disabled based on current value of selection.
   *
   * Also regenerates lists used in autocompletes.
   */
  protected updateActionStates() {
    if (this.selection.value === null) {
      this.hasTargets$.next(false);
      return;
    }
    this.hasTargets$.next(this.selection.value.selected.length !== 0);
  }
}
