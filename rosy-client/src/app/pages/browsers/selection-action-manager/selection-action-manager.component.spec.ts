import {take} from 'rxjs/operators';
import {BehaviorSubject, Subject, } from 'rxjs';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {
  Action,
  ContentMeta,
  ContentValue,
  Group,
  GroupSelection,
  GroupSimple,
  Image,
  ImageSelection,
} from '@app/models';
import {
  MockArtist,
  MockGroup,
  MockImage,
  MockName,
  MockTag,
} from '@app/models/testing';
import {ActionService, ApiService, LibraryService, NotificationService, } from '@app/services';
import {ContentInputDialogResponse, DialogService, } from '@app/shared/dialog';
import {ContentCreateRequest} from '@app/shared/auto-input';

import {SelectionActionManagerComponent, SelectionActionManagerModule, } from '.';


describe('SelectionActionManagerComponent', () => {
  let actionService: ActionService;
  let dialogService: DialogService;
  let libraryService: LibraryService;
  let selection: BehaviorSubject<(GroupSelection | ImageSelection | null)>;
  let component: SelectionActionManagerComponent;
  let fixture: ComponentFixture<SelectionActionManagerComponent>;

  let imageData: Image[];
  let groupData: Group[];

  // Library service spys
  let libraryCreateContentSpy: jasmine.Spy;
  let libraryUpdateGroupsSpy: jasmine.Spy;
  let libraryUpdateImagesSpy: jasmine.Spy;
  let createContentResponse: Subject<ContentValue>;

  // Dialog service spies
  let showContentInputSpy: jasmine.Spy;
  let showContentInputSelected: Subject<ContentMeta>;
  let showContentInputCreated: Subject<ContentCreateRequest>;

  let showInputSpy: jasmine.Spy;
  let showInputResponse: Subject<string>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    actionService = TestBed.inject(ActionService);
    dialogService = TestBed.inject(DialogService);
    libraryService = TestBed.inject(LibraryService);

    createContentResponse = new Subject();

    spyOn(libraryService, 'deleteGroup');
    spyOn(libraryService, 'deleteImages');
    spyOn(libraryService, 'markDupes');
    spyOn(libraryService, 'unmarkDupes');
    libraryCreateContentSpy = spyOn(libraryService, 'createContent').and.returnValue(createContentResponse);
    libraryUpdateGroupsSpy = spyOn(libraryService, 'updateGroups');
    libraryUpdateImagesSpy = spyOn(libraryService, 'updateImages').and.returnValue(new Subject());

    showInputResponse = new Subject();
    showInputSpy = spyOn(dialogService, 'showInput').and.returnValue(showInputResponse);

    showContentInputSelected = new Subject();
    showContentInputCreated = new Subject();
    showContentInputSpy = spyOn(dialogService, 'showContentInputDialog').and.returnValue({
      created: showContentInputCreated,
      selected: showContentInputSelected,
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionActionManagerComponent);
    component = fixture.componentInstance;
    selection = new BehaviorSubject(null);
    component.selection = selection;
    fixture.detectChanges();
  });

  beforeEach(() => {
    imageData = [1, 2, 3].map(id => MockImage.new(id));
    groupData = [1, 2, 3].map(id => MockGroup.new(id));
  });

  describe('added related content actions', () => {

    it('should open dialog with correct arguments', () => {
      selection.next(new ImageSelection([imageData[0]], null));
      const actions = [
        Action.AddArtistToSelected,
        Action.AddGroupToSelected,
        Action.AddNameToSelected,
        Action.AddTagToSelected,
      ];
      const expDialogArgs = [
        {type: 'artist', title: 'Set artist for selected', placeholder: 'Choose artist', allowCreate: true, persistent: false},
        {type: 'group', title: 'Set group for selected', placeholder: 'Choose group', allowCreate: true, persistent: false},
        {type: 'name', title: 'Add name to selected', placeholder: 'Choose name', allowCreate: true, persistent: true},
        {type: 'tag', title: 'Add tag to selected', placeholder: 'Choose tag', allowCreate: true, persistent: true},
      ];

      actions.forEach((action, index) => {
        actionService.doAction(action);
        expect(showContentInputSpy).toHaveBeenCalledOnceWith(expDialogArgs[index]);
        showContentInputSpy.calls.reset();
      });
    });

    it('should set the artist for images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const oldArtists = [ 1, null, null ];
      const inputResp: ContentMeta = {type: 'artist', item: MockArtist.new(3)};
      const expArtists = [ 3, 3, null ];
      const expImageGroupsUpdated = false;

      MockImage.setData(imageData, {artistId: oldArtists});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.AddArtistToSelected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();

      showContentInputSelected.next(inputResp);
      expect(libraryService.updateImages).toHaveBeenCalledWith(selected, expImageGroupsUpdated);
      expect(imageData.map(img => img.artistId)).toEqual(expArtists);
    });

    it('should attach a new artist for images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const oldArtists = [ 1, null, null ];
      const createRequest: ContentCreateRequest = {type: 'artist', value: 'new artist name'};
      const fakeArtist = new MockArtist(3, 'new artist name');
      const expArtists = [ 3, 3, null ];
      const expImageGroupsUpdated = false;

      MockImage.setData(imageData, {artistId: oldArtists});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.AddArtistToSelected);
      showContentInputCreated.next(createRequest);
      createContentResponse.next(fakeArtist);

      expect(libraryService.createContent).toHaveBeenCalledOnceWith('artist', 'new artist name', selected);
    });

    it('should set the group for images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const oldGroups = [ 1, null, null ];
      const inputResp: ContentMeta = {type: 'group', item: MockGroup.new(3)};
      const expGroups = [ 3, 3, null ];
      const expImageGroupsUpdated = true;

      MockImage.setData(imageData, {groupId: oldGroups});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.AddGroupToSelected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();

      showContentInputSelected.next(inputResp);
      expect(libraryService.updateImages).toHaveBeenCalledWith(selected, expImageGroupsUpdated);
      expect(imageData.map(img => img.groupId)).toEqual(expGroups);
    });

    it('should add a single name to a single target', () => {
      const selected = [0].map(ind => imageData[ind]);
      const oldNames = [[MockName.new(1)], [], []];
      const newName: ContentMeta = {type: 'name', item: MockName.new(2)};
      const expNames = [[MockName.new(1), MockName.new(2)], [], []];
      const expImageGroupsUpdated = false;

      MockImage.setData(imageData, {names: oldNames});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.AddNameToSelected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();

      showContentInputSelected.next(newName);
      expect(libraryService.updateImages).toHaveBeenCalledWith(selected, expImageGroupsUpdated);
      expect(imageData.map(img => img.names)).toEqual(expNames);
    });

    it('should add multiple names to multiple images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const oldNames = [[MockName.new(1)], [MockName.new(2)], []];
      const newNames: ContentMeta[] = [
        {type: 'name', item: MockName.new(3)},
        {type: 'name', item: MockName.new(4)}
      ];
      const expNames = [
        [MockName.new(1), MockName.new(3), MockName.new(4)],
        [MockName.new(2), MockName.new(3), MockName.new(4)],
        []];
      const expImageGroupsUpdated = false;

      MockImage.setData(imageData, {names: oldNames});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.AddNameToSelected);
      newNames.forEach(name => {
        showContentInputSelected.next(name);
      });
      expect(libraryService.updateImages).toHaveBeenCalledTimes(2);
      expect(libraryService.updateImages).toHaveBeenCalledWith(selected, expImageGroupsUpdated);
      expect(imageData.map(img => img.names)).toEqual(expNames);
    });

    it('should add multiple names to multiple groups', () => {
      const selected = [0, 1].map(ind => groupData[ind]);
      const oldNames = [[MockName.new(1)], [MockName.new(2)], []];
      const newNames: ContentMeta[] = [
        {type: 'name', item: MockName.new(3)},
        {type: 'name', item: MockName.new(4)}
      ];
      const expNames = [
        [MockName.new(1), MockName.new(3), MockName.new(4)],
        [MockName.new(2), MockName.new(3), MockName.new(4)],
        []];
      const expImageGroupsUpdated = false;

      MockGroup.setData(groupData, {names: oldNames});
      selection.next(new GroupSelection(selected, null));

      actionService.doAction(Action.AddNameToSelected);
      newNames.forEach(name => {
        showContentInputSelected.next(name);
      });
      expect(libraryService.updateGroups).toHaveBeenCalledTimes(2);
      expect(libraryService.updateGroups).toHaveBeenCalledWith(selected);
      expect(groupData.map(img => img.names)).toEqual(expNames);
    });

    it('should add a single tag to a single target', () => {
      const selected = [0].map(ind => imageData[ind]);
      const oldTags = [[MockTag.new(1)], [], []];
      const newTag: ContentMeta = {type: 'tag', item: MockTag.new(2)};
      const expTags = [[MockTag.new(1), MockTag.new(2)], [], []];
      const expImageGroupsUpdated = false;

      MockImage.setData(imageData, {tags: oldTags});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.AddTagToSelected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();

      showContentInputSelected.next(newTag);
      expect(libraryService.updateImages).toHaveBeenCalledWith(selected, expImageGroupsUpdated);
      expect(imageData.map(img => img.tags)).toEqual(expTags);
    });

    it('should add multiple tags to multiple images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const oldTags = [[MockTag.new(1)], [MockTag.new(2)], []];
      const newTags: ContentMeta[] = [
        {type: 'tag', item: MockTag.new(3)},
        {type: 'tag', item: MockTag.new(4)}
      ];
      const expTags = [
        [MockTag.new(1), MockTag.new(3), MockTag.new(4)],
        [MockTag.new(2), MockTag.new(3), MockTag.new(4)],
        []];
      const expImageGroupsUpdated = false;

      MockImage.setData(imageData, {tags: oldTags});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.AddTagToSelected);
      newTags.forEach(tag => {
        showContentInputSelected.next(tag);
      });
      expect(libraryService.updateImages).toHaveBeenCalledTimes(2);
      expect(libraryService.updateImages).toHaveBeenCalledWith(selected, expImageGroupsUpdated);
      expect(imageData.map(img => img.tags)).toEqual(expTags);
    });

    it('should add multiple tags to multiple groups', () => {
      const selected = [0, 1].map(ind => groupData[ind]);
      const oldTags = [[MockTag.new(1)], [MockTag.new(2)], []];
      const newTags: ContentMeta[] = [
        {type: 'tag', item: MockTag.new(3)},
        {type: 'tag', item: MockTag.new(4)}
      ];
      const expTags = [
        [MockTag.new(1), MockTag.new(3), MockTag.new(4)],
        [MockTag.new(2), MockTag.new(3), MockTag.new(4)],
        []];

      MockGroup.setData(groupData, {tags: oldTags});
      selection.next(new GroupSelection(selected, null));

      actionService.doAction(Action.AddTagToSelected);
      newTags.forEach(tag => {
        showContentInputSelected.next(tag);
      });
      expect(libraryService.updateGroups).toHaveBeenCalledTimes(2);
      expect(libraryService.updateGroups).toHaveBeenCalledWith(selected);
      expect(groupData.map(img => img.tags)).toEqual(expTags);
    });

  });

  describe('create related content actions', () => {

    it('should open a dialog corresponding to the content type', () => {
      const selected = [0].map(ind => imageData[ind]);
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewArtist);
      expect(showInputSpy).toHaveBeenCalledWith('Create artist', 'New artist name?');
      showInputSpy.calls.reset();

      actionService.doAction(Action.CreateNewGroup);
      expect(showInputSpy).toHaveBeenCalledWith('Create group', 'New group name?');
      showInputSpy.calls.reset();

      actionService.doAction(Action.CreateNewName);
      expect(showInputSpy).toHaveBeenCalledWith('Create name', 'New name name?');
      showInputSpy.calls.reset();

      actionService.doAction(Action.CreateNewTag);
      expect(showInputSpy).toHaveBeenCalledWith('Create tag', 'New tag name?');
      showInputSpy.calls.reset();
    });

    it('should create a new artist for a selected image', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValue = 'new object value';
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewArtist);
      showInputResponse.next(newValue);
      expect(libraryService.createContent).toHaveBeenCalledOnceWith('artist', newValue, selected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should not create a new artist when value is null or empty', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValues = [ undefined, null as any, '' ];
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewArtist);
      showInputResponse.next(newValues[0]);
      expect(libraryService.createContent).not.toHaveBeenCalled();
      expect(libraryService.updateImages).not.toHaveBeenCalled();

      actionService.doAction(Action.CreateNewArtist);
      showInputResponse.next(newValues[1]);
      expect(libraryService.createContent).not.toHaveBeenCalled();
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should create a new group for a selected image', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValue = 'new object value';
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewGroup);
      showInputResponse.next(newValue);
      expect(libraryService.createContent).toHaveBeenCalledOnceWith('group', newValue, selected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should not create a new group when value is null or empty', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValues = [ undefined, null as any, '' ];
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewGroup);
      showInputResponse.next(newValues[0]);
      actionService.doAction(Action.CreateNewGroup);
      showInputResponse.next(newValues[1]);

      expect(libraryService.createContent).not.toHaveBeenCalled();
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should create a new name for a selected image', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValue = 'new object value';
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewName);
      showInputResponse.next(newValue);
      expect(libraryService.createContent).toHaveBeenCalledOnceWith('name', newValue, selected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should not create a new name when value is null or empty', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValues = [ undefined, null as any, '' ];
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewName);
      showInputResponse.next(newValues[0]);
      actionService.doAction(Action.CreateNewName);
      showInputResponse.next(newValues[1]);

      expect(libraryService.createContent).not.toHaveBeenCalled();
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should create a new tag for a selected image', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValue = 'new object value';
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewTag);
      showInputResponse.next(newValue);
      expect(libraryService.createContent).toHaveBeenCalledOnceWith('tag', newValue, selected);
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should not create a new tag when value is null or empty', () => {
      const selected = [0].map(ind => imageData[ind]);
      const newValues = [ undefined, null as any, '' ];
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.CreateNewTag);
      showInputResponse.next(newValues[0]);
      actionService.doAction(Action.CreateNewTag);
      showInputResponse.next(newValues[1]);

      expect(libraryService.createContent).not.toHaveBeenCalled();
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

    it('should create a new name and bind it to selected groups', () => {
      const selected = [0].map(ind => groupData[ind]);
      const newValue = 'new object value';
      const newName = new MockName(13, 'new object value');
      const expNames = [[newName], [], []];
      selection.next(new GroupSelection(selected, null));

      actionService.doAction(Action.CreateNewName);
      showInputResponse.next(newValue);
      expect(libraryService.createContent).toHaveBeenCalledOnceWith('name', newValue, []);
      expect(libraryService.updateGroups).not.toHaveBeenCalled();

      createContentResponse.next(newName);
      expect(groupData.map(t => t.names)).toEqual(expNames);
      expect(libraryService.updateGroups).toHaveBeenCalledOnceWith(selected);
    });

    it('should create a new tag and bind it to selected groups', () => {
      const selected = [0].map(ind => groupData[ind]);
      const newValue = 'new object value';
      const newTag = new MockTag(13, 'new object value');
      const expTags = [[newTag], [], []];
      selection.next(new GroupSelection(selected, null));

      actionService.doAction(Action.CreateNewTag);
      showInputResponse.next(newValue);
      expect(libraryService.createContent).toHaveBeenCalledOnceWith('tag', newValue, []);
      expect(libraryService.updateGroups).not.toHaveBeenCalled();

      createContentResponse.next(newTag);
      expect(groupData.map(t => t.tags)).toEqual(expTags);
      expect(libraryService.updateGroups).toHaveBeenCalledOnceWith(selected);
    });

  });

  describe('removing related content actions', () => {

    it('should remove artist from targeted images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const oldArtistIds = [null, 3, 5];
      const expArtistIds = [null, null, 5];
      const expImageGroupsUpdated = false;

      MockImage.setData(imageData, {artistId: oldArtistIds});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.RemoveArtistFromSelected);
      expect(imageData.map(t => t.artistId)).toEqual((expArtistIds));
      expect(libraryService.updateImages).toHaveBeenCalledOnceWith(selected, expImageGroupsUpdated);
    });

    it('should remove the group for multiple images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const oldGroupIds = [null, 3, 5];
      const expGroupIds = [null, null, 5];
      const expImageGroupsUpdated = true;

      MockImage.setData(imageData, {groupId: oldGroupIds});
      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.RemoveGroupFromSelected);
      expect(libraryService.updateImages).toHaveBeenCalledOnceWith(selected, expImageGroupsUpdated);
      expect(imageData.map(t => t.groupId)).toEqual((expGroupIds));
    });

    it('should pass values to autocomplete when removing names', () => {
      const names = [new MockName(1, 'c'), new MockName(2, 'b'), new MockName(3, 'a')]

      const selected = [0, 1, 2].map(ind => groupData[ind]);
      const oldNames = [[names[0], names[1]], [names[1], names[2]], [names[2]]];
      const expAutocompleteValues = [names[2], names[1], names[0]]; // alphabetical order

      MockGroup.setData(groupData, {names: oldNames});
      selection.next(new GroupSelection(selected, null));
      actionService.doAction(Action.RemoveNameFromSelected);

      showContentInputSpy.and.callFake(({type, title, allowCreate, persistent, values}) => {
        expect(type).toEqual('name');
        expect(title).toEqual('Remove name from selected');
        expect(allowCreate).toBeFalse();
        expect(persistent).toBeTrue();
        expect(values.pipe(take(1)).subscribe(vals => {
          expect(vals).toEqual(expAutocompleteValues);
        }).closed).toBeTrue();
      });
      expect(showContentInputSpy).toHaveBeenCalledTimes(1);
    });

    it('should pass values to autocomplete when removing tags', () => {
      const tags = [new MockTag(1, 'c'), new MockTag(2, 'b'), new MockTag(3, 'a')]

      const selected = [0, 1, 2].map(ind => imageData[ind]);
      const oldTags = [[tags[0], tags[1]], [tags[1], tags[2]], [tags[2]]];
      const expAutocompleteValues = [tags[2], tags[1], tags[0]]; // alphabetical order

      MockImage.setData(imageData, {tags: oldTags});
      selection.next(new ImageSelection(selected, null));
      actionService.doAction(Action.RemoveTagFromSelected);

      showContentInputSpy.and.callFake(({type, title, allowCreate, persistent, values}) => {
        expect(type).toEqual('tag');
        expect(title).toEqual('Remove tag from selected');
        expect(allowCreate).toBeFalse();
        expect(persistent).toBeTrue();
        expect(values.pipe(take(1)).subscribe(vals => {
          expect(vals).toEqual(expAutocompleteValues);
        }).closed).toBeTrue();
      });
      expect(showContentInputSpy).toHaveBeenCalledTimes(1);
    });

    it('should remove a name from targeted groups', () => {
      const names = [new MockName(1, 'c'), new MockName(2, 'b'), new MockName(3, 'a')]

      const selected = [0, 1, 2].map(ind => groupData[ind]);
      const oldNames = [[names[0], names[1]], [names[1], names[2]], [names[2]]];
      const removedName: ContentMeta = {type: 'name', item: names[1]};
      const expNames = [[names[0]], [names[2]], [names[2]]];

      MockGroup.setData(groupData, {names: oldNames});
      selection.next(new GroupSelection(selected, null));
      actionService.doAction(Action.RemoveNameFromSelected);
      showContentInputSelected.next(removedName);

      expect(groupData.map(grp => grp.names)).toEqual(expNames);
      expect(libraryService.updateGroups).toHaveBeenCalledOnceWith(selected);
    });

    it('should remove a tag from targeted images', () => {
      const tags = [new MockTag(1, 'c'), new MockTag(2, 'b'), new MockTag(3, 'a')]

      const selected = [0, 1, 2].map(ind => imageData[ind]);
      const oldTags = [[tags[0], tags[1]], [tags[1], tags[2]], [tags[2]]];
      const removedTag: ContentMeta = {type: 'tag', item: tags[1]};
      const expTags = [[tags[0]], [tags[2]], [tags[2]]];

      MockImage.setData(imageData, {tags: oldTags});
      selection.next(new ImageSelection(selected, null));
      actionService.doAction(Action.RemoveTagFromSelected);
      showContentInputSelected.next(removedTag);

      expect(imageData.map(img => img.tags)).toEqual(expTags);
      expect(libraryService.updateImages).toHaveBeenCalledOnceWith(selected, false);
    });

    it('should ignore when content has no id', () => {
      const tags = [new MockTag(1, 'c'), new MockTag(2, 'b'), new MockTag(3, 'a')]

      const selected = [0, 1, 2].map(ind => imageData[ind]);
      const oldTags = [[tags[0], tags[1]], [tags[1], tags[2]], [tags[2]]];
      const removed: ContentMeta = {type: 'tag', item: new MockTag(null as any, 'b')};
      const expTags = oldTags;

      MockImage.setData(imageData, {tags: oldTags});
      selection.next(new ImageSelection(selected, null));
      actionService.doAction(Action.RemoveTagFromSelected);
      showContentInputSelected.next(removed);

      expect(imageData.map(img => img.tags)).toEqual(expTags);
      expect(libraryService.updateImages).not.toHaveBeenCalled();
    });

  });

  describe('delete actions', () => {
    let showConfirmSpy: jasmine.Spy;
    let showConfirmResponse: Subject<boolean>;

    beforeEach(() => {
      showConfirmResponse = new Subject();
      showConfirmSpy = spyOn(dialogService, 'showConfirm').and.returnValue(showConfirmResponse);
    });

    it('should delete an image', () => {
      const selected = [0].map(ind => imageData[ind]);
      const expMessage = 'Are you sure you want to delete 1 image?';

      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.DeleteSelected);
      expect(libraryService.deleteImages).not.toHaveBeenCalled();

      showConfirmResponse.next(true);
      expect(libraryService.deleteImages).toHaveBeenCalledWith([1]);
      expect(libraryService.deleteGroup).not.toHaveBeenCalled();
      expect(showConfirmSpy).toHaveBeenCalledWith('Confirm Delete', expMessage);
    });

    it('should delete multiple images', () => {
      const selected = [0, 1].map(ind => imageData[ind]);
      const expMessage = 'Are you sure you want to delete 2 images?';

      selection.next(new ImageSelection(selected, null));

      actionService.doAction(Action.DeleteSelected);
      expect(libraryService.deleteImages).not.toHaveBeenCalled();

      showConfirmResponse.next(true);
      expect(libraryService.deleteImages).toHaveBeenCalledWith([1, 2]);
      expect(libraryService.deleteGroup).not.toHaveBeenCalled();
      expect(showConfirmSpy).toHaveBeenCalledWith('Confirm Delete', expMessage);
    });

    it('should delete a group', () => {
      const selected = [0].map(ind => groupData[ind]);
      const groupTitle = 'Test group';
      const groupId = 13;
      const expId = 13;
      const expMessage = 'Are you sure you want to delete group "Test group" (id #13)? (the group\'s images will not be deleted)';

      selected[0].value = groupTitle;
      selected[0].id = groupId;
      selection.next(new GroupSelection(selected, null));

      actionService.doAction(Action.DeleteSelected);
      expect(libraryService.deleteGroup).not.toHaveBeenCalled();

      showConfirmResponse.next(true);
      expect(libraryService.deleteGroup).toHaveBeenCalledWith(expId);
      expect(libraryService.deleteImages).not.toHaveBeenCalled();
      expect(showConfirmSpy).toHaveBeenCalledWith('Confirm Delete', expMessage);
    });

    it('should not delete an image when user cancels', () => {
      selection.next(new ImageSelection([imageData[0]], imageData[0]));

      expect(libraryService.deleteImages).not.toHaveBeenCalled();
      actionService.doAction(Action.DeleteSelected);
      expect(libraryService.deleteImages).not.toHaveBeenCalled();
      showConfirmResponse.next(false);
      expect(libraryService.deleteImages).not.toHaveBeenCalled();
    });

    it('should do nothing if nothing is selected', () => {
      const entry = actionService.getRegisteredAction(Action.DeleteSelected);

      // Control: select image and fire delete action directly.
      selection.next(new ImageSelection([imageData[0]], imageData[0]));
      entry.activated$.next(null);
      expect(showConfirmSpy).toHaveBeenCalledTimes(1);
      showConfirmResponse.next(false);

      showConfirmSpy.calls.reset();

      // Experiment: select nothing and fire delete action directly.
      selection.next(null);
      entry.activated$.next(null);
      expect(showConfirmSpy).not.toHaveBeenCalled();
    });
  });

  describe('dupe marking actions', () => {

    it('should mark targets as exact duplicates', () => {
      const selected = [0, 1].map(ind => imageData[ind]);

      selection.next(new ImageSelection(selected, null));
      actionService.doAction(Action.MarkDupesExactFromSelected);
      expect(libraryService.markDupes).toHaveBeenCalledOnceWith(selected, 'Exact');
    });

    it('should mark targets as partial duplicates', () => {
      const selected = [0, 1, 2].map(ind => imageData[ind]);

      selection.next(new ImageSelection(selected, null));
      actionService.doAction(Action.MarkDupesPartialFromSelected);
      expect(libraryService.markDupes).toHaveBeenCalledOnceWith(selected, 'Partial');
    });

    it('should unmark targets as duplicates', () => {
      const selected = [0, 1, 2].map(ind => imageData[ind]);

      selection.next(new ImageSelection(selected, null));
      actionService.doAction(Action.UnmarkDupesFromSelected);
      expect(libraryService.unmarkDupes).toHaveBeenCalledOnceWith(selected);
    });
  });

  describe('enabled conditions', () => {

    it('should only allow adding/creating artist when there are selected images', () => {
      selection.next(new GroupSelection([], null));
      expect(actionService.getRegisteredAction(Action.AddArtistToSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.CreateNewArtist).enabled$.value).toBeFalse();
      selection.next(new ImageSelection([], null));
      expect(actionService.getRegisteredAction(Action.AddArtistToSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.CreateNewArtist).enabled$.value).toBeFalse();
      selection.next(new ImageSelection([imageData[0]], null));
      expect(actionService.getRegisteredAction(Action.AddArtistToSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.CreateNewArtist).enabled$.value).toBeTrue();
      selection.next(new ImageSelection([imageData[0], imageData[1]], imageData[0]));
      expect(actionService.getRegisteredAction(Action.AddArtistToSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.CreateNewArtist).enabled$.value).toBeTrue();
    });

    it('should only allow adding names/tags when there are targets', () => {
      selection.next(new GroupSelection([], null));
      expect(actionService.getRegisteredAction(Action.AddNameToSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.AddTagToSelected).enabled$.value).toBeFalse();
      selection.next(new GroupSelection([groupData[0], groupData[1]], groupData[0]));
      expect(actionService.getRegisteredAction(Action.AddNameToSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.AddTagToSelected).enabled$.value).toBeTrue();
      selection.next(new ImageSelection([], null));
      expect(actionService.getRegisteredAction(Action.AddNameToSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.AddTagToSelected).enabled$.value).toBeFalse();
      selection.next(new ImageSelection([imageData[0], imageData[1]], imageData[0]));
      expect(actionService.getRegisteredAction(Action.AddNameToSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.AddTagToSelected).enabled$.value).toBeTrue();
    });

    it('should only allow removing names/tags when there are targets with non-empty data', () => {
      selection.next(new GroupSelection([], null));
      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeFalse();
      selection.next(new GroupSelection([groupData[0], groupData[1]], groupData[0]));
      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeFalse();

      groupData[0].names = [MockName.new(1)];
      selection.next(new GroupSelection([groupData[0], groupData[1]], groupData[0]));
      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeFalse();

      groupData[0].tags = [MockTag.new(13)];
      selection.next(new GroupSelection([groupData[0], groupData[1]], groupData[0]));
      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeTrue();
    });

    it('should only allow setting group when target is non-zero list of images', () => {
      selection.next(new ImageSelection([], null));
      expect(actionService.getRegisteredAction(Action.AddGroupToSelected).enabled$.value).toBeFalse();
      selection.next(new GroupSelection([groupData[0], groupData[1]], groupData[0]));
      expect(actionService.getRegisteredAction(Action.AddGroupToSelected).enabled$.value).toBeFalse();
      selection.next(new ImageSelection([imageData[0], imageData[1]], imageData[0]));
      expect(actionService.getRegisteredAction(Action.AddGroupToSelected).enabled$.value).toBeTrue();
    });

    it('should allow deleting multiple images but only a single group', () => {
      selection.next(new ImageSelection([], null));
      expect(actionService.getRegisteredAction(Action.DeleteSelected).enabled$.value).toBeFalse();
      selection.next(new GroupSelection([], null));
      expect(actionService.getRegisteredAction(Action.DeleteSelected).enabled$.value).toBeFalse();
      selection.next(new ImageSelection([imageData[0]], null));
      expect(actionService.getRegisteredAction(Action.DeleteSelected).enabled$.value).toBeTrue();
      selection.next(new GroupSelection([groupData[0]], null));
      expect(actionService.getRegisteredAction(Action.DeleteSelected).enabled$.value).toBeTrue();
      selection.next(new ImageSelection([imageData[0], imageData[1]], null));
      expect(actionService.getRegisteredAction(Action.DeleteSelected).enabled$.value).toBeTrue();
      selection.next(new GroupSelection([groupData[0], groupData[1]], null));
      expect(actionService.getRegisteredAction(Action.DeleteSelected).enabled$.value).toBeFalse();
    });

    it('should allow removing group only if one or more target images has a group attached', () => {
      selection.next(new GroupSelection([groupData[0]], null));
      expect(actionService.getRegisteredAction(Action.RemoveGroupFromSelected).enabled$.value).toBeFalse();

      selection.next(new ImageSelection([], null));
      expect(actionService.getRegisteredAction(Action.RemoveGroupFromSelected).enabled$.value).toBeFalse();
      selection.next(new ImageSelection([imageData[0]], null));
      expect(actionService.getRegisteredAction(Action.RemoveGroupFromSelected).enabled$.value).toBeFalse();

      imageData[0].groupId = groupData[0].id;
      selection.next(new ImageSelection([imageData[0]], null));
      expect(actionService.getRegisteredAction(Action.RemoveGroupFromSelected).enabled$.value).toBeTrue();
      selection.next(new ImageSelection([imageData[0], imageData[1]], null));
      expect(actionService.getRegisteredAction(Action.RemoveGroupFromSelected).enabled$.value).toBeTrue();
    });

    it('should allow creating group only if no target images have groups already', () => {
      selection.next(new GroupSelection([groupData[0]], null));
      expect(actionService.getRegisteredAction(Action.CreateNewGroup).enabled$.value).toBeFalse();

      selection.next(new ImageSelection([], null));
      expect(actionService.getRegisteredAction(Action.CreateNewGroup).enabled$.value).toBeFalse();

      selection.next(new ImageSelection([imageData[0]], null));
      expect(actionService.getRegisteredAction(Action.CreateNewGroup).enabled$.value).toBeTrue();

      selection.next(new ImageSelection([imageData[0], imageData[1]], null));
      expect(actionService.getRegisteredAction(Action.CreateNewGroup).enabled$.value).toBeTrue();

      imageData[0].groupId = groupData[0].id;
      selection.next(new ImageSelection([imageData[0], imageData[1]], null));
      expect(actionService.getRegisteredAction(Action.CreateNewGroup).enabled$.value).toBeFalse();
    });

    it('should update action enabled status as images are updated', () => {
      imageData[0].tags = [MockTag.new(1)];
      imageData[0].groupId = groupData[0].id;

      selection.next(new ImageSelection(imageData, null));
      expect(actionService.getRegisteredAction(Action.RemoveGroupFromSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeTrue();
      expect(actionService.getRegisteredAction(Action.CreateNewGroup).enabled$.value).toBeFalse();

      imageData[0].tags = [];
      imageData[0].groupId = null;
      libraryService.imageUpdated$.next([imageData[0].id]);

      expect(actionService.getRegisteredAction(Action.RemoveGroupFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.CreateNewGroup).enabled$.value).toBeTrue();
    });

    it('should update action enabled status as groups are updated', () => {
      groupData[0].tags = [MockTag.new(1)];

      selection.next(new GroupSelection(groupData, null));
      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeTrue();

      groupData[0].tags = [];
      libraryService.contentUpdated$.group.next([groupData[0].id]);

      expect(actionService.getRegisteredAction(Action.RemoveNameFromSelected).enabled$.value).toBeFalse();
      expect(actionService.getRegisteredAction(Action.RemoveTagFromSelected).enabled$.value).toBeFalse();
    });

    it('should only marking/unmarking duplicates when multiple images are selected', () => {
      function checkActionState(enabled) {
        [Action.MarkDupesExactFromSelected, Action.MarkDupesMostFromSelected, Action.MarkDupesPartialFromSelected, Action.MarkDupesNoneFromSelected, Action.UnmarkDupesFromSelected].forEach((action, i) => {
          expect(actionService.getRegisteredAction(action).enabled$.value).toBe(enabled[i]);
        });
      }

      let selectedg: Group[] = [];
      selection.next(new GroupSelection(selectedg, null));
      checkActionState([false, false, false, false, false]);

      selectedg = [0, 1].map(i => groupData[i]);
      selection.next(new GroupSelection(selectedg, null));
      checkActionState([false, false, false, false, false]);

      let selectedi = [0].map(i => imageData[i]);
      selection.next(new ImageSelection(selectedi, null));
      checkActionState([false, false, false, false, false]);

      selectedi = [0, 1].map(i => imageData[i]);
      selection.next(new ImageSelection(selectedi, null));
      checkActionState([true, true, true, true, true]);
    });
  });
});
