import {BehaviorSubject, Subject, } from 'rxjs';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {Action, Image, ImageSelection, } from '@app/models';
import {ActionService, LibraryService, NotificationService, } from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {MockImage} from '@app/models/testing';

import {DeleteSelectionActionManagerComponent} from '.';


describe('DeleteSelectionActionManagerComponent', () => {
  let actionService: ActionService;
  let component: DeleteSelectionActionManagerComponent;
  let fixture: ComponentFixture<DeleteSelectionActionManagerComponent>;

  let images: Image[];

  // Library service spys
  let permadeleteSpy: jasmine.Spy;
  let restoreSpy: jasmine.Spy;

  let showConfirmSpy: jasmine.Spy;
  let showConfirmResponse: Subject<boolean>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
      ],
      providers: [
        MockProvider(DialogService),
        MockProvider(LibraryService),
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    actionService = TestBed.inject(ActionService);
    const dialogService = TestBed.inject(DialogService);
    const libraryService = TestBed.inject(LibraryService);

    permadeleteSpy = spyOn(libraryService, 'permadeleteImages');
    restoreSpy = spyOn(libraryService, 'restoreImages');

    showConfirmResponse = new Subject();
    showConfirmSpy = spyOn(dialogService, 'showConfirm').and.returnValue(showConfirmResponse);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSelectionActionManagerComponent);
    component = fixture.componentInstance;
    component.selection = new BehaviorSubject(null);
    fixture.detectChanges();
  });

  beforeEach(() => {
    images = [1, 2, 3].map(id => MockImage.new(id));
  });

  describe('permadelete', () => {

    it('should permadelete a single image', () => {
      const selection = new ImageSelection([images[0]], images[0])
      const expTargets = [1];

      component.selection.next(selection);
      fixture.detectChanges();

      actionService.doAction(Action.PermadeleteSelected);
      expect(showConfirmSpy).toHaveBeenCalledOnceWith('Confirm permadelete', 'Are you sure you want to delete 1 image? (Cannot be undone)')

      showConfirmResponse.next(true);
      expect(permadeleteSpy).toHaveBeenCalledOnceWith(expTargets);
    });

    it('should permadelete multiple images', () => {
      const selection = new ImageSelection([images[0], images[2]], images[0])
      const expTargets = [1, 3];

      component.selection.next(selection);
      fixture.detectChanges();

      actionService.doAction(Action.PermadeleteSelected);
      expect(showConfirmSpy).toHaveBeenCalledOnceWith('Confirm permadelete', 'Are you sure you want to delete 2 images? (Cannot be undone)')

      showConfirmResponse.next(true);
      expect(permadeleteSpy).toHaveBeenCalledOnceWith(expTargets);
    });

    it('should not permadelete when canceled', () => {
      const selection = new ImageSelection([images[0]], images[0])

      component.selection.next(selection);
      fixture.detectChanges();

      actionService.doAction(Action.PermadeleteSelected);

      showConfirmResponse.next(false);
      expect(permadeleteSpy).not.toHaveBeenCalled();
    });

    it('should not permadelete when there are no targets', () => {
      component.selection.next(null);
      fixture.detectChanges();

      actionService.doAction(Action.PermadeleteSelected);

      expect(showConfirmSpy).not.toHaveBeenCalled();
      expect(permadeleteSpy).not.toHaveBeenCalled();
    });
  });

  describe('restore', () => {

    it('should restore a single image', () => {
      const selection = new ImageSelection([images[0]], images[0])
      const expTargets = [1];

      component.selection.next(selection);
      fixture.detectChanges();

      actionService.doAction(Action.RestoreSelected);
      expect(restoreSpy).toHaveBeenCalledOnceWith(expTargets);
    });

    it('should restore multiple images', () => {
      const selection = new ImageSelection([images[0], images[2]], images[0])
      const expTargets = [1, 3];

      component.selection.next(selection);
      fixture.detectChanges();

      actionService.doAction(Action.RestoreSelected);
      expect(restoreSpy).toHaveBeenCalledOnceWith(expTargets);
    });

    it('should not permadelete when there are no targets', () => {
      component.selection.next(null);
      fixture.detectChanges();

      actionService.doAction(Action.RestoreSelected);
      expect(showConfirmSpy).not.toHaveBeenCalled();
      expect(restoreSpy).not.toHaveBeenCalled();
    });
  });

});
