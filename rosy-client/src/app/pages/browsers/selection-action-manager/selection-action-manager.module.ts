import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {DialogModule} from '@app/shared/dialog';
import {ServicesModule} from '@app/services';

import {DeleteSelectionActionManagerComponent} from './delete-selection-action-manager.component';
import {SelectionActionManagerComponent} from './selection-action-manager.component';


@NgModule({
  declarations: [
    DeleteSelectionActionManagerComponent,
    SelectionActionManagerComponent,
  ],
  exports: [
    DeleteSelectionActionManagerComponent,
    SelectionActionManagerComponent
  ],
  imports: [
    CommonModule,
    DialogModule,
    ServicesModule,
  ]
})
export class SelectionActionManagerModule { }
