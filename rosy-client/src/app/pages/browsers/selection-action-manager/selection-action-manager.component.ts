import {
  merge,
  BehaviorSubject,
  Observable,
  ReplaySubject,
  Subscription,
  Subject,
} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Component, Input, OnInit, } from '@angular/core';

import {
  Action,
  Artist,
  ContentMeta,
  ContentType,
  ContentValue,
  DupeType,
  DUPE_TYPES,
  GroupSelection,
  GroupSimple,
  Image,
  ImageSelection,
  MARK_DUPES_FROM_SELECTION_ACTIONS,
  Name,
  Tag,
} from '@app/models';
import {ActionService, LibraryService, } from '@app/services';
import {ContentInputDialogData, DialogService, } from '@app/shared/dialog';
import {ContentCreateRequest} from '@app/shared/auto-input';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

// Important that this doesn't contain 'feature'
const CONTENT_TYPES: ContentType[] = ['artist', 'group', 'name', 'tag'];

interface ContentMap<T> {
  artist: T,
  group: T,
  name: T,
  tag: T,
}

type ContentActionMap = ContentMap<Action>;

type ContentEnabledMap = ContentMap<BehaviorSubject<boolean>>;

const ADD_TO_ACTIONS: ContentActionMap = {
  artist: Action.AddArtistToSelected,
  group: Action.AddGroupToSelected,
  name: Action.AddNameToSelected,
  tag: Action.AddTagToSelected,
};

const REMOVE_FROM_ACTIONS: ContentActionMap = {
  artist: Action.RemoveArtistFromSelected,
  group: Action.RemoveGroupFromSelected,
  name: Action.RemoveNameFromSelected,
  tag: Action.RemoveTagFromSelected,
};

const CREATE_ACTIONS: ContentActionMap = {
  artist: Action.CreateNewArtist,
  group: Action.CreateNewGroup,
  name: Action.CreateNewName,
  tag: Action.CreateNewTag,
}



@Component({
  selector: 'rx-selection-action-manager',
  template: '<div></div>',
})
export class SelectionActionManagerComponent
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  @Input() selection: BehaviorSubject<(GroupSelection | ImageSelection | null)>;

  /**
   * Union of all target image names/tags for use in autocompletion in remove tag action.
   *
   * These are regenerated whenever the menu is opened or `LibraryService.contentUpdated$` or
   * `LibraryService.imageUpdated$` is fired.
   */
  protected removableTags: ReplaySubject<Tag[]>;
  protected removableNames: ReplaySubject<Name[]>;

  /**
   * Data used to determine when and how certain actions can be performed.
   *
   * These are all updated whenever the selection changes or `LibraryService` indicates that data
   * has been updated.
   */
  protected hasTargets$: BehaviorSubject<boolean>;
  protected hasImageTargets$: BehaviorSubject<boolean>;
  protected enabledCreateGroup$: BehaviorSubject<boolean>;
  protected enabledDelete$: BehaviorSubject<boolean>;
  protected enabledRemoveArtist$: BehaviorSubject<boolean>;
  protected enabledRemoveGroup$: BehaviorSubject<boolean>;
  protected enabledRemoveNames$: BehaviorSubject<boolean>;
  protected enabledRemoveTags$: BehaviorSubject<boolean>;
  protected enabledMarkDupes$: BehaviorSubject<boolean>;
  protected enabledUnmarkDupes$: BehaviorSubject<boolean>;

  protected enabledAddRelated: ContentEnabledMap;
  protected enabledCreateRelated: ContentEnabledMap;
  protected enabledRemoveRelated: ContentEnabledMap;

  constructor(
    private actionService: ActionService,
    private dialogService: DialogService,
    private libraryService: LibraryService) {
    super();

    this.hasTargets$ = new BehaviorSubject(false);
    this.hasImageTargets$ = new BehaviorSubject(false);
    this.enabledCreateGroup$ = new BehaviorSubject(false);
    this.enabledDelete$ = new BehaviorSubject(false);
    this.enabledRemoveArtist$ = new BehaviorSubject(false);
    this.enabledRemoveGroup$ = new BehaviorSubject(false);
    this.enabledRemoveNames$ = new BehaviorSubject(false);
    this.enabledRemoveTags$ = new BehaviorSubject(false);
    this.enabledMarkDupes$ = new BehaviorSubject(false);
    this.enabledUnmarkDupes$ = new BehaviorSubject(false);
    this.removableNames = new ReplaySubject();
    this.removableTags = new ReplaySubject();
    this.enabledAddRelated = {
      artist: this.hasImageTargets$,
      group: this.hasImageTargets$,
      name:  this.hasTargets$,
      tag: this.hasTargets$,
    };
    this.enabledCreateRelated = {
      artist: this.hasImageTargets$,
      group: this.enabledCreateGroup$,
      name:  this.hasTargets$,
      tag: this.hasTargets$,
    };
    this.enabledRemoveRelated = {
      artist: this.enabledRemoveArtist$,
      group: this.enabledRemoveGroup$,
      name:  this.enabledRemoveNames$,
      tag: this.enabledRemoveTags$
    };
  }

  ngOnInit() {
    merge(this.selection, this.libraryService.contentUpdated$.group, this.libraryService.imageUpdated$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.updateActionStates(); });

    // Register actions interpretted by this component
    this.actionService.captureAction(Action.DeleteSelected, this.enabledDelete$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.deleteTargets());

    DUPE_TYPES.forEach((type: DupeType) => {
      this.actionService.captureAction(MARK_DUPES_FROM_SELECTION_ACTIONS[type], this.enabledMarkDupes$)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => this.markSelectedAsDupes(type));
    });
    this.actionService.captureAction(Action.UnmarkDupesFromSelected, this.enabledUnmarkDupes$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.unmarkSelectedAsDupes());

    CONTENT_TYPES.forEach((type: ContentType) => {
      this.actionService.captureAction(CREATE_ACTIONS[type], this.enabledCreateRelated[type])
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => this.openCreateContentDialog(type));
      this.actionService.captureAction(ADD_TO_ACTIONS[type], this.enabledAddRelated[type])
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => this.openAddRelatedDialog(type));
      this.actionService.captureAction(REMOVE_FROM_ACTIONS[type], this.enabledRemoveRelated[type])
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => this.removeRelated(type));
    });
  }

  private static readonly _ADD_RELATED_DIALOG_TITLE = {
    artist: 'Set artist for selected',
    group: 'Set group for selected',
    name: 'Add name to selected',
    tag: 'Add tag to selected',
  };

  protected openAddRelatedDialog(type: ContentType) {
    const data: ContentInputDialogData = {
      type,
      title: SelectionActionManagerComponent._ADD_RELATED_DIALOG_TITLE[type],
      placeholder: `Choose ${type}`,
      allowCreate: true,
      persistent: type === 'name' || type === 'tag'
    };
    const {selected, created} = this.dialogService.showContentInputDialog(data);
    selected
      .pipe(takeUntil(this.destroyed$))
      .subscribe((selected: ContentMeta) => {
        this.addRelated(selected);
      });
    created
      .pipe(takeUntil(this.destroyed$))
      .subscribe((request: ContentCreateRequest) => {
        this.createRelated(request.type, request.value);
      });
  }

  protected openCreateContentDialog(type: ContentType) {
    this.dialogService.showInput(`Create ${type}`, `New ${type} name?`)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((value: string | null) => {
        if (value !== null) {
          this.createRelated(type, value);
        }
      });
  }

  protected openRemoveRelatedDialog(type: 'name' | 'tag') {
    const data: ContentInputDialogData = {
      type,
      title: `Remove ${type} from selected`,
      placeholder: `Choose ${type}`,
      allowCreate: false,
      persistent: true,
      values: type === 'tag' ? this.removableTags : this.removableNames
    };
    this.dialogService.showContentInputDialog(data).selected
      .pipe(takeUntil(this.destroyed$))
      .subscribe((result: ContentMeta) => {
        this.removeTaglike(type, result.item.id);
      });
  }

  protected addRelated(content: ContentMeta) {
    if (content.type === 'artist') {
      this.selection.value!.selected.forEach(img => {
        img.artistId = content.item.id;
      });
    } else if (content.type === 'group') {
      this.selection.value!.selected.forEach(img => {
        img.groupId = content.item.id;
      });
    } else if (content.type === 'name') {
      this.selection.value!.selected.forEach(t => {
        t.names.push(content.item);
      });
    } else if (content.type === 'tag') {
      this.selection.value!.selected.forEach(t => {
        t.tags.push(content.item);
      });
    }
    this.updateTargets(content.type === 'group');
  }

  protected createRelated(type: ContentType, value: string) {
    if (value && this.enabledCreateRelated[type].value) {
      // We have to be careful - we should only pass selected IDs to create method if selection
      // is a set of images. Otherwise we are sending group ids as image ids.
      if (this.selection.value instanceof ImageSelection) {
        this.libraryService.createContent(type, value, this.selection.value.selected)
          .pipe(takeUntil(this.destroyed$))
          .subscribe();
      } else {
        // TODO: If selection switches while library is saving, this will cause issues.
        this.libraryService.createContent(type, value, []).subscribe((item: ContentValue) => {
          this.addRelated({type, item} as ContentMeta);
        });
      }
    }
  }

  protected removeRelated(type: ContentType) {
    if (type === 'name' || type === 'tag') {
      this.openRemoveRelatedDialog(type);
    } else if (type === 'artist') {
      // Need to check class so typescript doesnt complain about artistId
      if (this.selection.value instanceof ImageSelection) {
        this.selection.value.selected.forEach((img: Image) => {
          img.artistId = null;
        });
      }
      this.updateTargets(false);
    } else if (type === 'group') {
      // Need to check class so typescript doesnt complain about groupId
      if (this.selection.value instanceof ImageSelection) {
        this.selection.value.selected.forEach((img: Image) => {
          img.groupId = null;
        });
      }
      this.updateTargets(true);
    }
  }

  /**
   * Removes given tag/name value from all target images.
   */
  protected removeTaglike(type: 'name' | 'tag', id: number) {
    if (id && this.enabledRemoveRelated[type].value) {
      this.selection.value!.selected.forEach(item => {
        const list = type === 'name' ? item.names : item.tags;
        const index = list.findIndex(tag => tag.id === id);
        if (index !== -1) {
          list.splice(index, 1);
        }
      });
      this.updateTargets(false);
    }
  }

  protected updateTargets(groupImagesChange: boolean) {
    if (this.selection.value instanceof ImageSelection) {
      this.libraryService.updateImages(this.selection.value.selected, groupImagesChange)
        .pipe(takeUntil(this.destroyed$))
        .subscribe();
    } else if (this.selection.value instanceof GroupSelection) {
      this.libraryService.updateGroups(this.selection.value.selected);
    }
  }

  protected deleteTargets() {
    // The below is really stupid but necessary because of how typescript works. In order for this
    // to work without the typecheck, either Group would have to be a subclass of Image or vice
    // versa. It doesn't even matter what is passed to `map`, you just cant call map on this type
    // of mututally exclusive union type.
    // let targets;
    let targets;
    let isImage;
    if (this.selection.value instanceof ImageSelection) {
      isImage = true;
      targets = this.selection.value.selected.map(target => target.id);
    } else if (this.selection.value instanceof GroupSelection) {
      isImage = false;
      targets = this.selection.value.selected.map(target => target.id);
    } else {
      return;
    }

    let message;
    if (isImage && targets.length === 1) {
      message = 'Are you sure you want to delete 1 image?';
    } else if (isImage && targets.length > 1) {
      message = `Are you sure you want to delete ${targets.length} images?`;
    } else if (!isImage && targets.length === 1) {
      const target = this.selection.value.selected[0] as GroupSimple;
      const label = `"${target.value}" (id #${target.id})`;
      message = `Are you sure you want to delete group ${label}? (the group's images will not be deleted)`;
    }

    this.dialogService
      .showConfirm('Confirm Delete', message)
      .subscribe((confirm) => {
        if (confirm && isImage) {
          this.libraryService.deleteImages(targets);
        } else if (confirm) {
          this.libraryService.deleteGroup(targets[0]);
        }
      });
  }

  protected markSelectedAsDupes(dupeType: DupeType) {
    if (this.selection.value instanceof ImageSelection) {
      this.libraryService.markDupes(this.selection.value.selected, dupeType);
    }
  }

  protected unmarkSelectedAsDupes() {
    if (this.selection.value instanceof ImageSelection) {
      this.libraryService.unmarkDupes(this.selection.value.selected);
    }
  }

  /**
   * Determines whether each action is enabled/disabled based on current value of selection.
   *
   * Also regenerates lists used in autocompletes.
   */
  protected updateActionStates() {
    if (this.selection.value === null) {
      this.hasTargets$.next(false);
      this.hasImageTargets$.next(false);
      this.enabledDelete$.next(false);
      this.enabledCreateGroup$.next(false);
      this.enabledRemoveArtist$.next(false);
      this.enabledRemoveGroup$.next(false);
      this.enabledRemoveNames$.next(false);
      this.enabledRemoveTags$.next(false);
      this.enabledMarkDupes$.next(false);
      this.enabledUnmarkDupes$.next(false);
      this.removableNames.next([]);
      this.removableTags.next([]);
      return;
    }
    const selected = this.selection.value.selected;
    const numSelected = selected.length;
    this.hasTargets$.next(numSelected !== 0);
    this.enabledRemoveNames$.next(numSelected !== 0 && selected.some(t => t.names.length !== 0));
    this.enabledRemoveTags$.next(numSelected !== 0 && selected.some(t => t.tags.length !== 0));
    if (this.selection.value instanceof ImageSelection) {
      this.hasImageTargets$.next(numSelected > 0);
      this.enabledDelete$.next(numSelected > 0);
      this.enabledCreateGroup$.next(numSelected !== 0 && this.selection.value.selected.every(t => t.groupId === null));
      this.enabledRemoveArtist$.next(selected.some(t => t.artistId !== null));
      this.enabledRemoveGroup$.next(selected.some(t => t.groupId !== null));
      this.enabledMarkDupes$.next(numSelected > 1);
      this.enabledUnmarkDupes$.next(numSelected > 1);
    } else {
      this.hasImageTargets$.next(false);
      this.enabledDelete$.next(numSelected === 1);
      this.enabledCreateGroup$.next(false);
      this.enabledRemoveArtist$.next(false);
      this.enabledRemoveGroup$.next(false);
      this.enabledMarkDupes$.next(false);
      this.enabledUnmarkDupes$.next(false);
    }

    // Update name/tag lists
    const names: { [key: number]: Name } = {};
    const tags: {  [key: number]: Tag } = {};
    selected.forEach(item => {
      item.names.forEach(t => {
        names[t.id] = t;
      });
      item.tags.forEach(t => {
        tags[t.id] = t;
      });
    });
    function abcSort(a, b) {
      if (a.value < b.value) { return -1; }
      else if (a.value > b.value) { return +1; }
      else { return 0; }
    }
    this.removableNames.next(Object.values(names).sort(abcSort));
    this.removableTags.next(Object.values(tags).sort(abcSort));
  }
}
