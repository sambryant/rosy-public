import {tick, fakeAsync, ComponentFixture, TestBed, } from '@angular/core/testing';
import {ActivatedRoute, Router, } from '@angular/router';
import {CdkTableModule} from '@angular/cdk/table';
import {CUSTOM_ELEMENTS_SCHEMA, EventEmitter, } from '@angular/core';
import {MatSortModule} from '@angular/material/sort';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {Subject} from 'rxjs';

import {
  ApiService,
  AuthGuardService,
  ConfigService,
  ItemsResponse,
  LibraryService,
  NotificationService,
} from '@app/services';
import {
  Group,
  GroupId,
  GroupQuery,
  GroupSelection,
  Image,
  ImageId,
  ImageSelection,
  Selection,
} from '@app/models';
import {DialogService} from '@app/shared/dialog';
import {MockGroup, MockImage, } from '@app/models/testing';
import {PipesModule} from '@app/shared/pipes';

import {GroupBrowserComponent} from '.';
import {GroupTableComponent, ImageTableComponent, SelectionManager, } from './tables';


describe('GroupBrowserComponent', () => {
  let component: GroupBrowserComponent;
  let fixture: ComponentFixture<GroupBrowserComponent>;
  let selectionManager: SelectionManager<Group, GroupSelection>;

  // @Output spies
  let selectionChangeSpy: jasmine.Spy;

  // Misc
  let items: Group[];
  let queryResponse: Subject<ItemsResponse<Group>>;
  let querySpy: jasmine.Spy;
  let libraryService: LibraryService;
  let route: ActivatedRoute;

  function imageSelection(image: Image): ImageSelection {
    return new ImageSelection([image], image);
  }

  function groupSelection(group: Group): GroupSelection {
    return new GroupSelection([group], group);
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GroupBrowserComponent,
        GroupTableComponent,
        ImageTableComponent,
      ],
      imports: [
        CdkTableModule,
        MatSortModule,
        NoopAnimationsModule,
        RouterTestingModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
        {provide: AuthGuardService, useValue: {canActivate: () => true}},
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();

    route = TestBed.inject(ActivatedRoute);
  });

  beforeEach(() => {
    queryResponse = new Subject();
    libraryService = TestBed.inject(LibraryService);
    spyOn(libraryService, 'getContentValueById').and.returnValue(new Subject());
    querySpy = spyOn(libraryService, 'queryGroups').and.returnValue(queryResponse);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupBrowserComponent);
    component = fixture.componentInstance;
    selectionManager = component.selectionManager;
    selectionChangeSpy = spyOn(component.selectionChange, 'next').and.callThrough();

    items = [
      MockGroup.new(1).withImages([1, 2, 3]),
      MockGroup.new(2).withImages([4, 5, 6]),
      MockGroup.new(3).withImages([7, 8, 9]),
    ];
  });

  function startWith(groups: Group[], totalCount: number) {
    fixture.ngZone!.run(() => {
      const router = TestBed.inject(Router);
      router.navigate([], {
        queryParams: new GroupQuery(groups.length).toParams()
      });
      tick();
      fixture.detectChanges();
      tick();
      queryResponse.next({items: groups, count: totalCount});
      tick();
      fixture.detectChanges();
    });
  }

  it('should fire a single selection event when switching from group to image', fakeAsync(() => {
    startWith(items, 6);
    selectionChangeSpy.calls.reset();

    fixture.ngZone!.run(() => {
      component.imageSelectionManagers[1].setSelection(1, false);
      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[1]));

      component.selectionManager.setSelection(1, false);
      expect(selectionChangeSpy).toHaveBeenCalledTimes(2);
      expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[1]));
    });
  }));

  it('should reload query in response to library changes', fakeAsync(() => {
    startWith(items, 6);
    querySpy.calls.reset();

    fixture.ngZone!.run(() => {
      libraryService.contentDeleted$.group.next([1]);
      items = [items[1], items[2]];
      expect(querySpy).toHaveBeenCalledTimes(1);
      queryResponse.next({items, count: 5});

      items[0].images.pop()!.groupId = null;
      libraryService.groupImagesUpdated$.next(null);
      expect(querySpy).toHaveBeenCalledTimes(2);
      queryResponse.next({items, count: 5});

      items[0].images.pop();
      libraryService.contentDeleted$.image.next([2]);
      expect(querySpy).toHaveBeenCalledTimes(3);
      queryResponse.next({items, count: 5});
    });
  }));

  it('should maintain current selection after refreshing after an image is deleted', fakeAsync(() => {
    startWith(items, items.length);
    selectionManager.setSelection(1, false);
    selectionChangeSpy.calls.reset();
    querySpy.calls.reset();

    fixture.ngZone!.run(() => {
      libraryService.contentDeleted$.image.next([1]);
      expect(querySpy).toHaveBeenCalledTimes(1);
      queryResponse.next({items, count: items.length});
      tick();
      expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[1]));
    });
  }));

  it('should set the fragment to its group when selecting an image', fakeAsync(() => {
    startWith(items, 6);

    fixture.ngZone!.run(() => {
      expect(route.snapshot.fragment).toEqual('0');
      component.imageSelectionManagers[2].setSelection(1, false);
      tick();
      expect(route.snapshot.fragment).toEqual('2');
    });
  }));

  it('should set the fragment to group index when selecting a group', fakeAsync(() => {
    startWith(items, 6);

    fixture.ngZone!.run(() => {
      expect(route.snapshot.fragment).toEqual('0');
      selectionManager.setSelection(2, false);
      tick();
      expect(route.snapshot.fragment).toEqual('2');
    });
  }));

  describe('navigation features', () => {

    beforeEach(fakeAsync(() => {
      component.displayMode = 'thumbnails';
      startWith(items, 6);
      fixture.ngZone!.run(() => {
        selectionManager.setSelection(null, false);
      });
      selectionChangeSpy.calls.reset();
    }));

    describe('forwards', () => {

      it('should navigate forward to first item when nothing selected', () => {
        fixture.ngZone!.run(() => {
          component.navigate({direction: 'next', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[0]));
        });
      });

      it('should navigate from a group to its first image', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[0]));
        });
      });

      it('should not expand-navigate from a group to its first image', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: true});

          expect(selectionChangeSpy).not.toHaveBeenCalled();
        });
      });

      it('should navigate from a group to next group when collapsed', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();
          component.groupTable.toggleGroup(1);

          component.navigate({direction: 'next', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[2]));
        });
      });

      it('should navigate from an image to another image', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: false});
          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[0]));
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: false});
          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[1]));
        });
      });

      it('should navigate from last image to next group', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          component.navigate({direction: 'next', extend: false}); // to image 4
          component.navigate({direction: 'next', extend: false}); // to image 5
          component.navigate({direction: 'next', extend: false}); // to image 6
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: false});
          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[2]));
        });
      });

      it('should not expand-navigate from last image to next group', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);

          component.navigate({direction: 'next', extend: false}); // to image 4
          component.navigate({direction: 'next', extend: false}); // to image 5
          component.navigate({direction: 'next', extend: false}); // to image 6
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: true});
          expect(selectionChangeSpy).not.toHaveBeenCalled();
        });
      });

      it('should navigate from group to next group when current group has no images', fakeAsync(() => {
        fixture.ngZone!.run(() => {
          items[0].images = [];
          component.refreshQuery();
          queryResponse.next({items, count: 6});
          tick();

          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: false});
          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[1]));
        });
      }));

      it('should automatically page forward when navigating past last element', fakeAsync(() => {
        fixture.ngZone!.run(() => {
          querySpy.calls.reset();
          component.imageSelectionManagers[2].setSelection(2, false);

          expect(querySpy).not.toHaveBeenCalled();
          component.navigate({direction: 'next', extend: false});
          expect(querySpy).toHaveBeenCalledTimes(1);
          expect(querySpy).toHaveBeenCalledWith(new GroupQuery(items.length, 1));

          // Need to fulfill request otherwise get intermitant errors
          queryResponse.next({items: [], count: 0});
          tick();
          fixture.detectChanges();
        });
      }));

      it('should navigate forwards when mobile mode is true', () => {
        const configService = TestBed.inject(ConfigService);
        configService.mobileMode$.next(true);
        fixture.detectChanges();

        fixture.ngZone!.run(() => {
          expect(component.groupTable).toBeFalsy();
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'next', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledOnceWith(groupSelection(items[2]));
        });
      });

    });

    describe('backwards', () => {

      it('should navigate backward to last item when nothing selected', () => {
        fixture.ngZone!.run(() => {
          component.navigate({direction: 'prev', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[2]));
        });
      });

      it('should navigate from a group to last groups last image', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'prev', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[0].images[2]));
        });
      });

      it('should not expand-navigate from a group to previous groups last image', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'prev', extend: true});

          expect(selectionChangeSpy).not.toHaveBeenCalled();
        });
      });

      it('should navigate from a group to prev group when previous group is collapsed', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();
          component.groupTable.toggleGroup(0);

          component.navigate({direction: 'prev', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[0]));
        });
      });

      it('should navigate from an image to another image', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);

          component.navigate({direction: 'next', extend: false}); // to image 4
          component.navigate({direction: 'next', extend: false}); // to image 5
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'prev', extend: false});
          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[0]));
        });
      });

      it('should navigate from first image to current group', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);
          component.navigate({direction: 'next', extend: false}); // to image 4
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'prev', extend: false});
          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[1]));
        });
      });

      it('should not expand-navigate from first image to current group', () => {
        fixture.ngZone!.run(() => {
          selectionManager.setSelection(1, false);

          component.navigate({direction: 'next', extend: false}); // to image 4
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'prev', extend: true});
          expect(selectionChangeSpy).not.toHaveBeenCalled();
        });
      });

      it('should navigate from group to previous group when group has no images', fakeAsync(() => {
        fixture.ngZone!.run(() => {
          items[0].images = [];
          component.refreshQuery();
          queryResponse.next({items, count: 6});
          tick();

          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'prev', extend: false});
          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[0]));
        });
      }));

      it('should automatically page backward when navigating before first element', fakeAsync(() => {
        fixture.ngZone!.run(() => {
          querySpy.calls.reset();
          component.query.page = 1;
          selectionManager.setSelection(0, false);

          expect(querySpy).not.toHaveBeenCalled();
          component.navigate({direction: 'prev', extend: false});
          expect(querySpy).toHaveBeenCalledTimes(1);
          expect(querySpy).toHaveBeenCalledWith(new GroupQuery(items.length, 0));

          // Need to fulfill request otherwise get intermitant errors
          queryResponse.next({items: [], count: 0});
          tick();
          fixture.detectChanges();
        });
      }));

      it('should navigate backwards when mobile mode is true', () => {
        const configService = TestBed.inject(ConfigService);
        configService.mobileMode$.next(true);
        fixture.detectChanges();

        fixture.ngZone!.run(() => {
          expect(component.groupTable).toBeFalsy();
          selectionManager.setSelection(1, false);
          selectionChangeSpy.calls.reset();

          component.navigate({direction: 'prev', extend: false});

          expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
          expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[0]));
        });
      });
    });

  });

  describe('selection features', () => {

    function getGroupBlock(group: number): Element {
      return fixture.nativeElement.querySelectorAll('div.group')[group];
    }

    function getGroupRow(group: number): Element | null {
      return getGroupBlock(group).querySelector('div.group-row');
    }

    function getSelectedGroups(): string[] {
      const rows = Array.from(fixture.nativeElement.querySelectorAll('div.group-row.rx-active')) as Element[];
      return rows.map(el => (el.querySelector('div.cell-id') as any).innerText.trim());
    }

    function getGroupImageRow(group: number, image: number): Element {
      return getGroupBlock(group).querySelectorAll('tr.cdk-row')[image];
    }

    function getSelectedGroupImages(): string[] {
      const rows = Array.from(fixture.nativeElement.querySelectorAll('tr.rx-active')) as Element[];
      return rows.map(el => (el.querySelector('td.cell-id') as any).innerText.trim());
    }

    function clickOn(element: Element) {
      const event = document.createEvent('Events');
      event.initEvent('click', true, false);
      element.dispatchEvent(event);
    }

    beforeEach(fakeAsync(() => {
      component.displayMode = 'list';
      fixture.detectChanges();
      startWith(items, 6);
      selectionChangeSpy.calls.reset();
    }));

    it('should select a group', () => {
      fixture.ngZone!.run(() => {
        expect(selectionChangeSpy).not.toHaveBeenCalled();
        selectionManager.setSelection(1, false);
        expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[1]));
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      });
    });

    it('should select a group on click', () => {
      fixture.ngZone!.run(() => {
        expect(selectionChangeSpy).not.toHaveBeenCalled();
        clickOn(getGroupRow(1)!);
        expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[1]));
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      });
    });

    it('should select an image on click', () => {
      fixture.ngZone!.run(() => {
        expect(selectionChangeSpy).not.toHaveBeenCalled();
        clickOn(getGroupImageRow(1, 1));
        expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[1]));
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      });
    });

    it('should deselect groups after image is selected', () => {
      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
        fixture.detectChanges();

        expect(getSelectedGroups()).toEqual(['2']);
        expect(getSelectedGroupImages()).toEqual([]);
        expect(selectionManager.getSelectedItems().map(grp => grp.id)).toEqual([2]);
        expect(selectionManager.getSelectedRows()).toEqual([false, true, false]);

        clickOn(getGroupImageRow(1, 1));
        fixture.detectChanges();

        expect(getSelectedGroups()).toEqual([]);
        expect(getSelectedGroupImages()).toEqual(['5']);
        expect(selectionManager.getSelectedItems().map(grp => grp.id)).toEqual([]);
        expect(selectionManager.getSelectedRows()).toEqual([false, false, false]);
      });
    });

    it('should deselect images after group is selected', () => {
      fixture.ngZone!.run(() => {
        clickOn(getGroupImageRow(1, 1));
        fixture.detectChanges();

        expect(getSelectedGroups()).toEqual([]);
        expect(getSelectedGroupImages()).toEqual(['5']);
        expect(component.imageSelectionManagers
          .map(mngr => mngr.getSelectedItems()
            .map(img => img.id))).toEqual([[], [5], []]);
        expect(component.imageSelectionManagers
          .map(mngr => mngr.getSelectedRows())).toEqual([
            [false, false, false],
            [false, true, false],
            [false, false, false]]);
        expect(selectionManager.getSelectedItems().map(grp => grp.id)).toEqual([]);
        expect(selectionManager.getSelectedRows()).toEqual([false, false, false]);
        selectionChangeSpy.calls.reset();

        clickOn(getGroupRow(0)!);
        fixture.detectChanges();
        expect(getSelectedGroups()).toEqual(['1']);
        expect(getSelectedGroupImages()).toEqual([]);
        expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[0]));
      });
    });

    it('should deselect other image tables after image is selected', () => {
      fixture.ngZone!.run(() => {
        // Select image 5 from group 2
        clickOn(getGroupImageRow(1, 1));
        fixture.detectChanges();

        expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[1]));
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(getSelectedGroupImages()).toEqual(['5']);
        expect(getSelectedGroups()).toEqual([]);

        // Select image 9 from group 3
        selectionChangeSpy.calls.reset();
        clickOn(getGroupImageRow(2, 2));
        fixture.detectChanges();

        expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[2].images[2]));
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(getSelectedGroupImages()).toEqual(['9']);
        expect(getSelectedGroups()).toEqual([]);
      });
    });

    it('should do nothing after selecting a group if loading', fakeAsync(() => {
      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);

        expect(component.status).toEqual('normal');
        expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[1]));
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        selectionChangeSpy.calls.reset();

        component.refreshQuery();

        expect(component.status).toEqual('loading');
        selectionManager.setSelection(2, false);
        expect(selectionChangeSpy).not.toHaveBeenCalled();

        queryResponse.next({items, count: items.length});
        tick();
      });
    }));

    it('should do nothing after selecting an image if loading', fakeAsync(() => {
      fixture.ngZone!.run(() => {
        const imageSelectionManagers = component.imageSelectionManagers;
        imageSelectionManagers[1].setSelection(1, false);

        expect(component.status).toEqual('normal');
        expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1].images[1]));
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        selectionChangeSpy.calls.reset();

        component.refreshQuery();

        expect(component.status).toEqual('loading');
        imageSelectionManagers[1].setSelection(2, false);
        expect(selectionChangeSpy).not.toHaveBeenCalled();

        queryResponse.next({items, count: items.length});
        tick();
      });
    }));

    it('should not subscribe to selection events from old image selection managers', fakeAsync(() => {
      fixture.ngZone!.run(() => {
        selectionChangeSpy.calls.reset();

        const imageSelectionManagers = component.imageSelectionManagers;
        imageSelectionManagers[1].setSelection(1, false);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);

        component.refreshQuery();
        queryResponse.next({items, count: items.length});
        tick();
        selectionChangeSpy.calls.reset();

        // Try changing selection of old image selection manager, nothing happens
        imageSelectionManagers[1].setSelection(0, false);
        expect(selectionChangeSpy).not.toHaveBeenCalled();

        // However, changing selection of new image selection manager does do something
        component.imageSelectionManagers[1].setSelection(0, false);
        expect(selectionChangeSpy).toHaveBeenCalled();
      });
    }));

    it('should not subscribe to boundary events from old image selection managers', fakeAsync(() => {
      fixture.ngZone!.run(() => {
        const imageSelectionManager = component.imageSelectionManagers[1];
        const boundarySpy = spyOn(imageSelectionManager.boundaryEvent$, 'emit').and.callThrough();

        // Set selection to last image in selection manager to prep for boundary event
        imageSelectionManager.setSelection(2, false);

        // Now navigating to next image should trigger boundary event
        boundarySpy.calls.reset();
        selectionChangeSpy.calls.reset();
        imageSelectionManager.navigate({direction: 'next', extend: false});
        expect(boundarySpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).toHaveBeenCalledWith(groupSelection(items[2]));

        // Repeat the same process but try to do it after a new query has been loaded.
        // Call refresh before trying to navigate
        component.refreshQuery();
        queryResponse.next({items, count: items.length});
        tick();
        imageSelectionManager.setSelection(2, false);

        // Now navigating to next image should not do anything since image manager has expired.
        boundarySpy.calls.reset();
        selectionChangeSpy.calls.reset();
        imageSelectionManager.navigate({direction: 'next', extend: false});
        expect(boundarySpy).toHaveBeenCalledTimes(1);
        expect(selectionChangeSpy).not.toHaveBeenCalled();
      });
    }));
  });
});
