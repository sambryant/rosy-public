import {
  ChangeDetectorRef,
  Directive,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Params, Router, } from '@angular/router';
import {BehaviorSubject, Observable, Subject, } from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {Sort} from '@angular/material/sort';

import {
  AbstractConstructor,
  Action,
  Constructor,
  ContentTypeFull,
  Direction,
  NavigationRequest,
  Query,
  QueryFromParams,
  Selection,
  SortedQuery,
  PagedQuery,
} from '@app/models';
import {
  ActionService,
  ConfigService,
  ItemsResponse,
  LibraryService,
  NotificationService,
} from '@app/services';
import {Status} from '@app/shared/status-overlay';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';

import {SelectionConstructor, SelectionManager, } from './tables';


interface BaseBrowser<ItemType, QueryType> {
  status: Status;
  statusMessage: string | null;
  items: ItemType[];
  totalCount: number;
  query: QueryType;
  selectionManager: SelectionManager<ItemType, Selection<ItemType>>;

  navigate(request: NavigationRequest): void;
  refreshQuery(): void;
  refreshQueryAndViewImage();
}

export interface SelectionBrowser<SelectionType> {
  selectionChange: BehaviorSubject<SelectionType | null>
}

export interface PagedBrowser {
  onPageEvent(event: {pageSize: number, pageIndex: number}): void
  onBoundaryEvent(direction: Direction): void;
}

export interface SortedBrowser {
  onSortChange(sort: Sort): void;
}

@Directive()
export abstract class BaseBrowserDirective<ItemType, QueryType extends Query>
extends UnsubscribeOnDestroyedDirective
implements OnInit, BaseBrowser<ItemType, QueryType> {

  /**
   * Indicates whether browser object is loading a query, received an error, or in a normal state.
   */
  status: Status = 'normal';

  /**
   * A status-dependent message to give display to user (usually an error message).
   */
  statusMessage: string | null = null;

  /**
   * List of items in page queried from server.
   */
  items: ItemType[] = [];

  /**
   * Total number of items as reported by server.
   */
  totalCount = 0;

  /**
   * Query object used to retrieve items from server.
   */
  query: QueryType;

  /**
   * Object responsible for controlling selection logic for the set of items loaded from the server.
   */
  selectionManager: SelectionManager<ItemType, Selection<ItemType>>;

  /**
   * Event fired whenever a new query is sent to the server, this is used to cancel subscriptions.
   */
  protected loadingStarted = new Subject<null>();

  /**
   * This is a blocking stack. Changes in `selectionManager` are only observed when this is empty.
   */
  protected ignoreSelectionChange: null[] = [];

  /**
   * Constructor used to construct new query objects.
   */
  protected QueryClass: Constructor<QueryType>;

  constructor(
    protected actionService: ActionService,
    protected changeRef: ChangeDetectorRef,
    public configService: ConfigService,
    protected libraryService: LibraryService,
    protected notificationService: NotificationService,
    protected route: ActivatedRoute,
    protected router: Router,
    QueryClass: Constructor<QueryType>,
    SelectionClass: SelectionConstructor<ItemType, Selection<ItemType>>,
  ) {
    super();
    this.QueryClass = QueryClass;
    this.selectionManager = new SelectionManager(SelectionClass);
  }

  ngOnInit(): void {
    this.configService.mobileMode$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((_) => {
        this.changeRef.markForCheck();
      });

    // Whenever URL query parameters change, reset query object and reload images.
    // Warning: Angular may depreciate 'queryParams' in future in favor of 'queryParamMap'.
    // Difference is mostly superficial but 'queryParamMap' is more annoying in tests cause it's not
    // just a raw object.
    this.route.queryParams
      .pipe(takeUntil(this.destroyed$))
      .subscribe((queryParams: Params) => {
        this.handleQueryParamChange(queryParams);
      });

    this.configService.databaseSchema$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((_) => {
        this.refreshQuery();
      });

    this.selectionManager.selectionChange$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((selection) => {
        this.onSelectionChanged(selection);
      });

    // Register actions interpretted by this component
    this.actionService.captureAction(Action.NavForward)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.navigate({direction: 'next', extend: false}) );
    this.actionService.captureAction(Action.NavBackward)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.navigate({direction: 'prev', extend: false}) );
  }

  protected abstract queryLibrary(): Observable<ItemsResponse<ItemType>>;

  navigate(request: NavigationRequest) {
    this.selectionManager.navigate(request);
  }

  refreshQuery(): void {
    this.query.refresh();
    this.reloadQuery(0);
  }

  refreshQueryAndViewImage(): void {
    this.query.refresh();
    this.reloadQuery(0).subscribe(() => {
      if (this.items.length > 0) {
        // Set view mode to image where only images are displayed
        this.configService.viewMode$.next('image');
      } else {
        this.notificationService.error('No results found');
      }
    });
  }

  protected abstract emitSelectionChanged(selection: Selection<ItemType>);

  protected onSelectionChanged(selection: Selection<ItemType>) {
    if (this.status !== 'loading' && this.ignoreSelectionChange.length === 0) {
      this.emitSelectionChanged(selection);
      this.writeStateToUrl();
    }
  }

  protected handleQueryParamChange(queryParams: Params) {
    // Use URL fragment to set selected image. If not defined or improperly formatted, default to 0.
    const selectedIndex = this.getSelectedIndexFromFragment(this.route.snapshot.fragment);

    // Use URL query params to set query.
    const query = QueryFromParams(this.QueryClass, queryParams);

    if (JSON.stringify(query) !== JSON.stringify(this.query)) {
      // If the query has changed, reload the query and select the given index when finished.
      this.query = query;
      this.reloadQuery(selectedIndex);
    } else {
      // If the query has not changed, write state to URL to ensure URL is canonical.
     this.writeStateToUrl(true);
    }
  }

  protected getSelectedIndexFromFragment(fragment: string | null | undefined): number {
    if (fragment) {
      return parseInt(fragment, 10) || 0;
    } else {
      return 0;
    }
  }

  protected getFragmentForSelection(): string | undefined {
    const groupIndex = this.selectionManager.getCursorIndex();
    if (groupIndex !== null) {
      return groupIndex.toString();
    } else {
      return undefined;
    }
  }

  /**
   * Re-queries library using current query and sets the selection to given selection when finished.
   *
   * This also sets the URL to match the resulting query/selection state.
   */
  protected reloadQuery(newSelectedIndex: number): Observable<null> {
    const loadingObserver = new Subject<null>();
    this.loadingStarted.next(null);
    this.status = 'loading';
    this.statusMessage = 'Querying server';
    this.queryLibrary()
      .pipe(takeUntil(this.loadingStarted))
      .pipe(takeUntil(this.destroyed$))
      .subscribe((response) => {
        this.handleQueryResponse(response);
        this.selectionManager.setSelection(newSelectedIndex, false);
        loadingObserver.next(null);
      }, err => this.handleQueryError(err));
    return loadingObserver
      .pipe(takeUntil(this.loadingStarted))
      .pipe(takeUntil(this.destroyed$));
  }

  /**
   * Takes the response of a query and updates up the corresponding `SelectionManager` instance.
   */
  protected handleQueryResponse(response: ItemsResponse<ItemType>) {
    this.totalCount = response.count;
    this.items = response.items;
    this.ignoreSelectionChange.push();
    this.selectionManager.setItems(response.items); // will fire a `null` selection change event
    this.ignoreSelectionChange.pop();
    if (this.items.length === 0) {
      this.status = 'error';
      this.statusMessage = 'No results found';
    } else {
      this.status = 'normal';
      this.statusMessage = null;
    }
  }

  protected handleQueryError(err: any) {
    this.handleQueryResponse({ count: 0, items: [] });
    this.status = 'error';
    this.statusMessage = err.error && err.error.message || 'Something went wrong';
  }

  /**
   * Sets the canonical URL based on current query and selection.
   */
  protected writeStateToUrl(skipLocationChange: boolean = false) {
    if (this.status === 'loading') {
      throw new Error('This should never happen');
    }
    const opts = {queryParams: this.query.toParams(), skipLocationChange} as any;
    opts.fragment = this.getFragmentForSelection();
    this.router.navigate([], opts);
  }

}

@Directive()
export abstract class SimpleSelectionBrowserDirective<ItemType, QueryType extends Query>
extends BaseBrowserDirective<ItemType, QueryType>
implements SelectionBrowser<Selection<ItemType>> {

  @Output()
  selectionChange: BehaviorSubject<Selection<ItemType> | null> = new BehaviorSubject(null);

  constructor(
    actionService: ActionService,
    changeRef: ChangeDetectorRef,
    configService: ConfigService,
    libraryService: LibraryService,
    notificationService: NotificationService,
    route: ActivatedRoute,
    router: Router,
    QueryClass: Constructor<QueryType>,
  ) {
    super(actionService, changeRef, configService, libraryService, notificationService, route, router, QueryClass, Selection);
  }

  protected emitSelectionChanged(selection: Selection<ItemType>) {
    this.selectionChange.next(selection);
  }

}

export function mixinReloadOnDelete<
  ItemType,
  QueryType extends Query,
  TBase extends AbstractConstructor<BaseBrowserDirective<ItemType, QueryType>>
>(Base: TBase, type: ContentTypeFull): TBase {

  @Directive()
  abstract class Inner extends Base implements OnInit {

    constructor(...args: any[]) { super(...args); }

    override ngOnInit() {
      super.ngOnInit();
      this.libraryService.contentDeleted$[type]
        .pipe(takeUntil(this.destroyed$))
        .subscribe((ids) => {
          this.reloadQuery(this.selectionManager.getCursorIndex() || 0);
        });
    }
  }

  return Inner;

}

export function mixinPagedBrowser<
  ItemType,
  QueryType extends Query & PagedQuery,
  TBase extends AbstractConstructor<BaseBrowserDirective<ItemType, QueryType>>
>(Base: TBase): AbstractConstructor<PagedBrowser> & TBase {

  @Directive()
  abstract class Derived extends Base implements OnInit {

    constructor(...args: any[]) { super(...args); }

    override ngOnInit(): void {
      super.ngOnInit();
      this.selectionManager.boundaryEvent$
        .pipe(takeUntil(this.destroyed$))
        .subscribe((direction) => {
          if (this.status !== 'loading') {
            this.onBoundaryEvent(direction);
          }
        });
    }

    /**
     * Responds to navigation at edges of current list by incrementing/decrementing the query page.
     */
    onBoundaryEvent(direction: Direction) {
      const indexAbs = this.query.size * this.query.page;
      if (direction === 'next') {
        if (this.query.size * (this.query.page + 1) + 1 < this.totalCount) {
          this.query.page += 1;
          this.reloadQuery(0);
        }
      } else if (direction === 'prev') {
        if (this.query.size * this.query.page - 1 >= 0) {
          this.query.page -= 1;
          this.reloadQuery(this.query.size - 1);
        }
      }
    }

    /**
     * Handles changes in the pagination.
     *
     * If user pages forward/backward, the first image in new page is automatically selected.
     * If user changes the page size, the page index is chosen to keep current selection in view.
     */
    onPageEvent(event: {pageSize: number, pageIndex: number}): void {
      let newIndex = 0; // index of new selection after pagination

      // When changing page size, choose page index so that current selection can remain selected.
      if (this.query.size !== event.pageSize) {
        const absIndex = this.query.size * this.query.page + (this.selectionManager.getCursorIndex() || 0);
        const page = Math.floor(absIndex / event.pageSize);
        newIndex = this.selectionManager.getCursorIndex() !== null ? absIndex % event.pageSize : 0;
        this.query.page = page;
        this.query.size = event.pageSize;
      } else {
        this.query.size = event.pageSize;
        this.query.page = event.pageIndex;
      }
      this.reloadQuery(newIndex);
    }
  }
  return Derived;

}

export function mixinSortedBrowser<
  ItemType,
  QueryType extends Query & SortedQuery,
  TBase extends AbstractConstructor<BaseBrowserDirective<ItemType, QueryType>>
>(Base: TBase): AbstractConstructor<SortedBrowser> & TBase {

  abstract class Inner extends Base {

    constructor(...args: any[]) { super(...args); }

    /**
     * Handles changes in query's sort as result of clicking headers in view.
     */
    onSortChange(sort: Sort) {
      this.query.sort = sort;
      this.query.refresh();
      this.reloadQuery(0);
    }
  }

  return Inner;

}
