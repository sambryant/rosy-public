import {take} from 'rxjs/operators';
import {tick, fakeAsync, ComponentFixture, TestBed, } from '@angular/core/testing';
import {CdkTableModule} from '@angular/cdk/table';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatSelectModule} from '@angular/material/select';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {Subject} from 'rxjs';

import {
  ApiService,
  AuthGuardService,
  ConfigService,
  ImageFetcherService,
  ItemsResponse,
  LibraryService,
  NotificationService,
} from '@app/services';
import {
  ArtistDetails,
  ArtistId,
  ArtistQuery,
  ContentEigendata,
  DeletedQuery,
  DupeCandidate,
  DupeCandidatesQuery,
  EigendataQuery,
  Image,
  ImageId,
  ImageQuery,
  ImageSelection,
  NameDetails,
  NameId,
  NameQuery,
  Selection,
  TagDetails,
  TagId,
  TagQuery,
} from '@app/models';
import {
  MockArtistD,
  MockGroup,
  MockImage,
  MockNameD,
  MockTagD,
} from '@app/models/testing';
import {ActionButtonDirective} from '@app/shared/action-button';
import {DialogService} from '@app/shared/dialog';
import {MockQueryItems} from '@app/services/testing';
import {PipesModule} from '@app/shared/pipes';

import {
  ArtistTableComponent,
  ImageTableComponent,
  NameTableComponent,
  SelectionManager,
  TagTableComponent,
} from './tables';
import {
  ArtistBrowserComponent,
  DeletedBrowserComponent,
  DupeCandidatesBrowserComponent,
  EigendataBrowserComponent,
  ImageBrowserComponent,
  NameBrowserComponent,
  TagBrowserComponent,
} from '.';


describe('BrowsersComponent', () => {
  let libraryService: LibraryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ActionButtonDirective,
        ArtistBrowserComponent, ArtistTableComponent,
        DeletedBrowserComponent,
        EigendataBrowserComponent,
        ImageBrowserComponent, ImageTableComponent,
        NameBrowserComponent, NameTableComponent,
        TagBrowserComponent, TagTableComponent,
      ],
      imports: [
        CdkTableModule,
        MatSelectModule,
        MatSortModule,
        MatTooltipModule,
        NoopAnimationsModule,
        RouterTestingModule,
        PipesModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
        {provide: AuthGuardService, useValue: {canActivate: () => true}},
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    libraryService = TestBed.inject(LibraryService);
    spyOn(libraryService, 'getImageBlobUrl').and.returnValue(new Subject());
    spyOn(libraryService, 'getContentValueById').and.returnValue(new Subject());
  });


  describe('ImageBrowserComponent', () => {
    let component: ImageBrowserComponent;
    let fixture: ComponentFixture<ImageBrowserComponent>;
    let selectionManager: SelectionManager<Image, Selection<Image>>;
    let selectionChangeSpy: jasmine.Spy;

    let items: Image[];
    let queryResponse: Subject<ItemsResponse<Image>>;
    let querySpy: jasmine.Spy;
    let imageFetcher: ImageFetcherService;

    function imageSelection(image: Image): ImageSelection {
      return new ImageSelection([image], image);
    }

    beforeEach(() => {
      queryResponse = new Subject();
      querySpy = spyOn(libraryService, 'queryImages').and.returnValue(queryResponse);
      fixture = TestBed.createComponent(ImageBrowserComponent);
      component = fixture.componentInstance;
      selectionManager = component.selectionManager;
      selectionChangeSpy = spyOn(component.selectionChange, 'next').and.callThrough();

      imageFetcher = TestBed.inject(ImageFetcherService);

      items = [
        MockImage.new(1),
        MockImage.new(2),
        MockImage.new(3),
      ];
    });

    function startWith(items: Image[], totalCount: number) {
      fixture.ngZone!.run(() => {
        const router = TestBed.inject(Router);
        router.navigate([], {
          queryParams: new ImageQuery(items.length).toParams()
        });
        tick();
        fixture.detectChanges();
        tick();
        queryResponse.next({items, count: totalCount});
        tick();
        fixture.detectChanges();
      });
    }

    it('should emit selection change when the selection changes', fakeAsync(() => {
      startWith(items, items.length);
      selectionChangeSpy.calls.reset();

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
      });

      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1]));
    }));

    it('should set the image fetcher pool with the items returned from query', fakeAsync(() => {
      const setExpectedImagesSpy = spyOn(imageFetcher, 'setExpectedImages').and.callThrough();
      startWith(items, items.length);
      expect(setExpectedImagesSpy).toHaveBeenCalledTimes(1);
      expect(setExpectedImagesSpy).toHaveBeenCalledWith(items);
    }));

    it('should tell image fetcher pool to fetch from currently selected image', fakeAsync(() => {
      startWith(items, items.length);

      const prefetchFromImageSpy = spyOn(imageFetcher, 'prefetchFromImage');
      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
      });

      expect(prefetchFromImageSpy).toHaveBeenCalledTimes(1);
      expect(prefetchFromImageSpy).toHaveBeenCalledWith(items[1]);
    }));

    it('should reset the query when an image is deleted', fakeAsync(() => {
      startWith(items, items.length);
      querySpy.calls.reset();

      libraryService.contentDeleted$.image.next([1]);
      expect(querySpy).toHaveBeenCalledTimes(1);
    }));

    it('should maintain current selection after refreshing after an image is deleted', fakeAsync(() => {
      startWith(items, items.length);

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
        selectionChangeSpy.calls.reset();
        querySpy.calls.reset();

        libraryService.contentDeleted$.image.next([1]);
        expect(querySpy).toHaveBeenCalledTimes(1);
        queryResponse.next({items: items, count: items.length});
        tick();
        expect(selectionChangeSpy).toHaveBeenCalledWith(imageSelection(items[1]));
      });
    }));

  });

  describe('ArtistBrowserComponent', () => {
    let component: ArtistBrowserComponent;
    let fixture: ComponentFixture<ArtistBrowserComponent>;
    let selectionManager: SelectionManager<ArtistDetails, Selection<ArtistDetails>>;
    let selectionChangeSpy: jasmine.Spy;

    let items: ArtistDetails[];
    let queryResponse: Subject<ItemsResponse<ArtistDetails>>;
    let querySpy: jasmine.Spy;

    beforeEach(async () => {
      queryResponse = new Subject();
      querySpy = spyOn(libraryService, 'queryArtists').and.returnValue(queryResponse);
      fixture = TestBed.createComponent(ArtistBrowserComponent);
      component = fixture.componentInstance;
      selectionManager = component.selectionManager;
      selectionChangeSpy = spyOn(component.selectionChange, 'next').and.callThrough();

      items = [
        MockArtistD.new(1),
        MockArtistD.new(2),
        MockArtistD.new(3),
      ];
    });

    function startWith(items: ArtistDetails[], totalCount: number) {
      fixture.ngZone!.run(() => {
        const router = TestBed.inject(Router);
        router.navigate([], {
          queryParams: new ArtistQuery().toParams()
        });
        tick();
        fixture.detectChanges();
        tick();
        queryResponse.next({items, count: totalCount});
        tick();
        fixture.detectChanges();
      });
    }

    it('should emit selection change when the selection changes', fakeAsync(() => {
      startWith(items, items.length);
      selectionChangeSpy.calls.reset();

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
      });

      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
    }));

    it('should reset the query when a artist is deleted', fakeAsync(() => {
      startWith(items, items.length);
      querySpy.calls.reset();

      libraryService.contentDeleted$.artist.next([1]);
      expect(querySpy).toHaveBeenCalledTimes(1);
    }));

    it('should maintain current selection after refreshing after a artist is deleted', fakeAsync(() => {
      startWith(items, items.length);

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
        selectionChangeSpy.calls.reset();
        querySpy.calls.reset();

        libraryService.contentDeleted$.artist.next([1]);
        expect(querySpy).toHaveBeenCalledTimes(1);
        queryResponse.next({items, count: items.length});
        tick();
        expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
      });
    }));
  });

  describe('DeletedBrowserComponent', () => {
    let component: DeletedBrowserComponent;
    let fixture: ComponentFixture<DeletedBrowserComponent>;
    let selectionManager: SelectionManager<Image, Selection<Image>>;
    let selectionChangeSpy: jasmine.Spy;

    let items: Image[];
    let queryResponse: Subject<ItemsResponse<Image>>;
    let querySpy: jasmine.Spy;

    beforeEach(async () => {
      queryResponse = new Subject();
      querySpy = spyOn(libraryService, 'queryDeleted').and.returnValue(queryResponse);
      fixture = TestBed.createComponent(DeletedBrowserComponent);
      component = fixture.componentInstance;
      selectionManager = component.selectionManager;
      selectionChangeSpy = spyOn(component.selectionChange, 'next').and.callThrough();

      items = [
        MockImage.new(1),
        MockImage.new(2),
        MockImage.new(3),
      ];
    });

    function startWith(items: Image[], totalCount: number) {
      fixture.ngZone!.run(() => {
        const router = TestBed.inject(Router);
        router.navigate([], {
          queryParams: new DeletedQuery().toParams()
        });
        tick();
        fixture.detectChanges();
        tick();
        queryResponse.next({items, count: totalCount});
        tick();
        fixture.detectChanges();
      });
    }

    it('should emit selection change when the selection changes', fakeAsync(() => {
      startWith(items, items.length);
      selectionChangeSpy.calls.reset();

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
      });

      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
    }));

    it('should reset the query when an image is deleted', fakeAsync(() => {
      startWith(items, items.length);
      querySpy.calls.reset();

      libraryService.contentDeleted$.image.next([1]);
      expect(querySpy).toHaveBeenCalledTimes(1);
    }));

    it('should maintain current selection after refreshing after an image is deleted', fakeAsync(() => {
      startWith(items, items.length);

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
        selectionChangeSpy.calls.reset();
        querySpy.calls.reset();

        libraryService.contentDeleted$.image.next([1]);
        expect(querySpy).toHaveBeenCalledTimes(1);
        queryResponse.next({items, count: items.length});
        tick();
        expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
      });
    }));
  });

  describe('DupeCandidatesBrowserComponent', () => {
    let component: DupeCandidatesBrowserComponent;
    let fixture: ComponentFixture<DupeCandidatesBrowserComponent>;

    let images: MockImage[];
    let items: DupeCandidate[];
    let queryResponse: Subject<ItemsResponse<DupeCandidate>>;
    let querySpy: jasmine.Spy;

    beforeEach(async () => {
      queryResponse = new Subject();
      querySpy = spyOn(libraryService, 'queryDupeCandidates').and.returnValue(queryResponse);
      fixture = TestBed.createComponent(DupeCandidatesBrowserComponent);
      component = fixture.componentInstance;

      images = [0, 1, 2, 3, 4, 5].map(i => MockImage.new(1));
      images[1].dupes = { 2: 'Exact', 3: 'Partial' };
      images[2].dupes = { 1: 'Exact', 3: 'Partial' };
      images[3].dupes = { 1: 'Partial', 2: 'Partial' };
      items = [
        {item1: images[1], item2: images[2], score: 0.01, mark: 'Exact'},
        {item1: images[1], item2: images[5], score: 0.02, mark: null},
        {item1: images[1], item2: images[3], score: 0.10, mark: 'Partial'},
        {item1: images[2], item2: images[3], score: 0.11, mark: 'Partial'},
      ];
    });

    function startWith(items: DupeCandidate[], totalCount: number) {
      fixture.ngZone!.run(() => {
        const router = TestBed.inject(Router);
        router.navigate([], {
          queryParams: new DupeCandidatesQuery().toParams()
        });
        tick();
        fixture.detectChanges();
        tick();
        queryResponse.next({items, count: totalCount});
        tick();
        fixture.detectChanges();
      });
    }

    it('should mark candidates as dupes', fakeAsync(() => {
      startWith(items, items.length);

      const mockSubject = new Subject();
      const markSpy = spyOn(libraryService, 'markDupes').and.returnValue(mockSubject.pipe(take(1)));

      expect(items[0].mark).toEqual('Exact');
      component.markAsDupes(items[0], 'Partial');
      expect(markSpy).toHaveBeenCalledOnceWith([images[1], images[2]], 'Partial');
      expect(items[0].mark).toEqual('Exact');
      mockSubject.next(null);
      expect(items[0].mark).toEqual('Partial');
    }));

    it('should unmark candidates as dupes', fakeAsync(() => {
      startWith(items, items.length);

      const mockSubject = new Subject();
      const markSpy = spyOn(libraryService, 'unmarkDupes').and.returnValue(mockSubject.pipe(take(1)));

      expect(items[0].mark).toEqual('Exact');
      component.unmarkAsDupes(items[0]);
      expect(markSpy).toHaveBeenCalledOnceWith([images[1], images[2]]);
      expect(items[0].mark).toEqual('Exact');
      mockSubject.next(null);
      expect(items[0].mark).toEqual(null);
    }));

  });

  describe('EigendataBrowserComponent', () => {
    let component: EigendataBrowserComponent;
    let fixture: ComponentFixture<EigendataBrowserComponent>;
    let queryResponse: Subject<ItemsResponse<ContentEigendata>>;
    let querySpy: jasmine.Spy;
    let items: ContentEigendata[];

    beforeEach(async () => {
      queryResponse = new Subject();
      querySpy = spyOn(libraryService, 'queryEigendata').and.returnValue(queryResponse);
      fixture = TestBed.createComponent(EigendataBrowserComponent);
      component = fixture.componentInstance;
      items = [];
    });

    it('should mark as loading while query loads', fakeAsync(() => {
      fixture.ngZone!.run(() => {
        const router = TestBed.inject(Router);
        router.navigate([], { queryParams: new EigendataQuery().toParams() });
        tick();
        fixture.detectChanges();
        tick();

        expect(component.status).toEqual('loading');

        queryResponse.next({items: [{} as any], count: 1});
        tick();
        fixture.detectChanges();
        expect(component.status).toEqual('normal');

        queryResponse.error({});
        tick();
        fixture.detectChanges();
        expect(component.status).toEqual('error');
      });
    }));
  });

  describe('NameBrowserComponent', () => {
    let component: NameBrowserComponent;
    let fixture: ComponentFixture<NameBrowserComponent>;
    let selectionManager: SelectionManager<NameDetails, Selection<NameDetails>>;
    let selectionChangeSpy: jasmine.Spy;

    let items: NameDetails[];
    let queryResponse: Subject<ItemsResponse<NameDetails>>;
    let querySpy: jasmine.Spy;

    beforeEach(async () => {
      queryResponse = new Subject();
      querySpy = spyOn(libraryService, 'queryNames').and.returnValue(queryResponse);

      fixture = TestBed.createComponent(NameBrowserComponent);
      component = fixture.componentInstance;
      selectionManager = component.selectionManager;
      selectionChangeSpy = spyOn(component.selectionChange, 'next').and.callThrough();

      items = [
        MockNameD.new(1),
        MockNameD.new(2),
        MockNameD.new(3),
      ];
    });

    function startWith(items: NameDetails[], totalCount: number) {
      fixture.ngZone!.run(() => {
        const router = TestBed.inject(Router);
        router.navigate([], {
          queryParams: new NameQuery().toParams()
        });
        tick();
        fixture.detectChanges();
        tick();
        queryResponse.next({items, count: totalCount});
        tick();
        fixture.detectChanges();
      });
    }

    it('should emit selection change when the selection changes', fakeAsync(() => {
      startWith(items, items.length);
      selectionChangeSpy.calls.reset();

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
      });

      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
    }));

    it('should reset the query when a name is deleted', fakeAsync(() => {
      startWith(items, items.length);
      querySpy.calls.reset();

      libraryService.contentDeleted$.name.next([1]);
      expect(querySpy).toHaveBeenCalledTimes(1);
    }));

    it('should maintain current selection after refreshing after a name is deleted', fakeAsync(() => {
      startWith(items, items.length);

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
        selectionChangeSpy.calls.reset();
        querySpy.calls.reset();

        libraryService.contentDeleted$.name.next([1]);
        expect(querySpy).toHaveBeenCalledTimes(1);
        queryResponse.next({items, count: items.length});
        tick();
        expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
      });
    }));
  });

  describe('TagBrowserComponent', () => {
    let component: TagBrowserComponent;
    let fixture: ComponentFixture<TagBrowserComponent>;
    let selectionManager: SelectionManager<TagDetails, Selection<TagDetails>>;
    let selectionChangeSpy: jasmine.Spy;

    let items: TagDetails[];
    let queryResponse: Subject<ItemsResponse<TagDetails>>;
    let querySpy: jasmine.Spy;

    beforeEach(async () => {
      queryResponse = new Subject();
      querySpy = spyOn(libraryService, 'queryTags').and.returnValue(queryResponse);

      fixture = TestBed.createComponent(TagBrowserComponent);
      component = fixture.componentInstance;
      selectionManager = component.selectionManager;
      selectionChangeSpy = spyOn(component.selectionChange, 'next').and.callThrough();

      items = [
        MockTagD.new(1),
        MockTagD.new(2),
        MockTagD.new(3),
      ];
    });

    function startWith(items: TagDetails[], totalCount: number) {
      fixture.ngZone!.run(() => {
        const router = TestBed.inject(Router);
        router.navigate([], {
          queryParams: new TagQuery().toParams()
        });
        tick();
        fixture.detectChanges();
        tick();
        queryResponse.next({items, count: totalCount});
        tick();
        fixture.detectChanges();
      });
    }

    it('should emit selection change when the selection changes', fakeAsync(() => {
      startWith(items, items.length);
      selectionChangeSpy.calls.reset();

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
      });

      expect(selectionChangeSpy).toHaveBeenCalledTimes(1);
      expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
    }));

    it('should reset the query when a tag is deleted', fakeAsync(() => {
      startWith(items, items.length);
      querySpy.calls.reset();

      libraryService.contentDeleted$.tag.next([1]);
      expect(querySpy).toHaveBeenCalledTimes(1);
    }));

    it('should maintain current selection after refreshing after a tag is deleted', fakeAsync(() => {
      startWith(items, items.length);

      fixture.ngZone!.run(() => {
        selectionManager.setSelection(1, false);
        selectionChangeSpy.calls.reset();
        querySpy.calls.reset();

        libraryService.contentDeleted$.tag.next([1]);
        expect(querySpy).toHaveBeenCalledTimes(1);
        queryResponse.next({items, count: items.length});
        tick();
        expect(selectionChangeSpy).toHaveBeenCalledWith(new Selection([items[1]], items[1]));
      });
    }));
  });
});
