import {Component, Input, OnInit, ViewChild, } from '@angular/core';

import {
  Group,
  GroupSelection,
  Image,
  ImageSelection,
} from '@app/models';
import {ConfigService} from '@app/services';
import {GroupEditorComponent, ImageViewerComponent, } from '@app/shared/content-editor';

import {GroupBrowserSelection} from './browsers';


@Component({
  selector: 'rx-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./pages.scss']
})
export class GroupPageComponent {
  selectionType: 'group' | 'image';

  selectedGroup: Group | null = null;

  selectedImage: Image | null = null;

  @ViewChild('imageViewer') imageViewer: ImageViewerComponent;

  @ViewChild('groupEditor') groupEditor: GroupEditorComponent;

  constructor(public configService: ConfigService) {}

  onSelectionChange(selection: GroupBrowserSelection) {
    if (this.selectedImage !== null) {
      if (this.imageViewer && this.configService.saveOnNavigate) {
        this.imageViewer.save();
      }
    } else if (this.selectedGroup !== null) {
      if (this.groupEditor && this.configService.saveOnNavigate) {
        this.groupEditor.save();
      }
    }
    this.selectedImage = null;
    this.selectedGroup = null;
    if (selection instanceof ImageSelection) {
      this.selectedImage = selection.item as Image;
    } else if (selection instanceof GroupSelection) {
      this.selectedGroup = selection.item as Group;
    }
  }
}
