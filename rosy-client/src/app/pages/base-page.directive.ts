import {Directive, ViewChild, } from '@angular/core';

import {ConfigService} from '@app/services';
import {Content} from '@app/models';
import {BaseEditorDirective} from '@app/shared/content-editor';

import {BaseBrowserDirective} from './browsers';


@Directive()
export class BasePageDirective<
  ItemType extends Content,
  BrowserComponent extends BaseBrowserDirective<ItemType, any>,
  EditorComponent extends BaseEditorDirective<ItemType>> {

  selected: ItemType | null = null;

  @ViewChild('browser') browser: BrowserComponent;

  @ViewChild('editor') editor: EditorComponent;

  constructor(public configService: ConfigService) {}

  onSelectionChange(selected: ItemType | null) {
    if (this.editor && this.configService.saveOnNavigate) {
      this.editor.save();
    }
    this.selected = selected;
  }

}
