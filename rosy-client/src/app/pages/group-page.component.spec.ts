import {
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  DebugElement,
  Input,
} from '@angular/core';
import {of} from 'rxjs';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ConfigService} from '@app/services';
import {Group, GroupSelection, Image, ImageSelection, } from '@app/models';
import {MockGroup, MockImage, } from '@app/models/testing';

import {GroupPageComponent} from '.';


@Component({
  selector: 'rx-group-editor',
  template: '<div></div>',
})
class MockGroupEditorComponent {
  @Input() target: Group;
  save() {}
}


@Component({
  selector: 'rx-image-viewer',
  template: '<div></div>',
})
class MockImageViewerComponent {
  @Input() target: Image;
  save() {}
}


describe('GroupPageComponent', () => {
  let configService: ConfigService;
  let component: GroupPageComponent;
  let fixture: ComponentFixture<GroupPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GroupPageComponent,
        MockGroupEditorComponent,
        MockImageViewerComponent,
      ],
      imports: [
        NoopAnimationsModule,
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ] // Ignores component not in imports, declarations, or providers
    })
    .compileComponents();
    configService = TestBed.inject(ConfigService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change displayed image', () => {
    const startImage = MockImage.new(1);
    const nextImage = MockImage.new(2);

    component.selectedGroup = null;
    component.selectedImage = startImage;
    fixture.detectChanges();

    expect(component.imageViewer.target).toEqual(startImage);
    expect(component.groupEditor).toBeFalsy();

    component.onSelectionChange(new ImageSelection([nextImage], nextImage));
    fixture.detectChanges();

    expect(component.imageViewer.target).toEqual(nextImage);
    expect(component.groupEditor).toBeFalsy();
  });

  it('should save before changing displayed image when saveOnNavigate is true', () => {
    const startImage = MockImage.new(1);
    const nextImage = MockImage.new(2);
    configService.saveOnNavigate = true;

    component.selectedImage = startImage;
    component.selectionType = 'image';
    fixture.detectChanges();
    const saveSpy = spyOn(component.imageViewer, 'save');

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.imageViewer.target).toEqual(startImage);

    component.onSelectionChange(new ImageSelection([nextImage], nextImage));
    fixture.detectChanges();

    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(component.imageViewer.target).toEqual(nextImage);
  });

  it('should not save before changing displayed image when saveOnNavigate is false', () => {
    const startImage = MockImage.new(1);
    const nextImage = MockImage.new(2);
    configService.saveOnNavigate = false;

    component.selectedImage = startImage;
    fixture.detectChanges();
    const saveSpy = spyOn(component.imageViewer, 'save');

    expect(component.imageViewer.target).toEqual(startImage);

    component.onSelectionChange(new ImageSelection([nextImage], nextImage));
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.imageViewer.target).toEqual(nextImage);
  });

  it('should not render group editor when an image is displayed', () => {
    const selectedImage = MockImage.new(1);
    component.selectedGroup = null;
    component.selectedImage = selectedImage;
    fixture.detectChanges();

    expect(component.imageViewer.target).toEqual(selectedImage);
    expect(component.groupEditor).toBeFalsy();
  });

  it('should not render image viewer when a group is displayed', () => {
    const selectedGroup = MockGroup.new(1);
    component.selectedGroup = selectedGroup;
    component.selectedImage = null;
    fixture.detectChanges();

    expect(component.imageViewer).toBeFalsy();
    expect(component.groupEditor.target).toEqual(selectedGroup);
  });

  it('should save before changing group when saveOnNavigate is true', () => {
    const startGroup = MockGroup.new(1);
    const nextGroup = null;
    configService.saveOnNavigate = true;

    component.selectedGroup = startGroup;
    fixture.detectChanges();
    const saveSpy = spyOn(component.groupEditor, 'save');

    expect(saveSpy).not.toHaveBeenCalled();
    component.onSelectionChange(new GroupSelection([], nextGroup));
    fixture.detectChanges();
    expect(saveSpy).toHaveBeenCalledTimes(1);
  });
});
