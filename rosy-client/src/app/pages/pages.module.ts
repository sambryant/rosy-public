import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {ContentEditorModule} from '@app/shared/content-editor';
import {ServicesModule} from '@app/services';

import {
  ArtistPageComponent,
  DeletePageComponent,
  EigendataPageComponent,
  ImagePageComponent,
  NamePageComponent,
  TagPageComponent,
} from './pages.component';
import {BrowsersModule} from './browsers';
import {GroupPageComponent} from './group-page.component';

@NgModule({
  declarations: [
    ArtistPageComponent,
    DeletePageComponent,
    EigendataPageComponent,
    GroupPageComponent,
    ImagePageComponent,
    NamePageComponent,
    TagPageComponent,
  ],
  exports: [
    ArtistPageComponent,
    DeletePageComponent,
    EigendataPageComponent,
    GroupPageComponent,
    ImagePageComponent,
    NamePageComponent,
    TagPageComponent,
  ],
  imports: [
    BrowsersModule,
    CommonModule,
    ContentEditorModule,
    ServicesModule,
  ]
})
export class PagesModule { }
