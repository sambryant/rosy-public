import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MockComponent} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ConfigService} from '@app/services';
import {ImageViewerComponent, TagEditorComponent, } from '@app/shared/content-editor';
import {MockImage, MockTagD, } from '@app/models/testing';

import {ImagePageComponent, TagPageComponent, } from '.';


describe('ImagePageComponent', () => {
  let configService: ConfigService;
  let component: ImagePageComponent;
  let fixture: ComponentFixture<ImagePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ImagePageComponent,
        MockComponent(ImageViewerComponent),
      ],
      imports: [
        NoopAnimationsModule,
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    })
    .compileComponents();
    configService = TestBed.inject(ConfigService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should change displayed image', () => {
    const startItem = MockImage.new(1);
    const nextItem = MockImage.new(2);

    component.selected = startItem;
    fixture.detectChanges();

    expect(component.editor.target).toEqual(startItem);

    component.onSelectionChange(nextItem);
    fixture.detectChanges();

    expect(component.editor.target).toEqual(nextItem);
  });

  it('should save before changing displayed image when saveOnNavigate is true', () => {
    const saveSpy = spyOn(component.editor, 'save');
    const startItem = MockImage.new(1);
    const nextItem = MockImage.new(2);
    configService.saveOnNavigate = true;

    component.selected = startItem;
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.editor.target).toEqual(startItem);

    component.onSelectionChange(nextItem);
    fixture.detectChanges();

    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(component.editor.target).toEqual(nextItem);
  });

  it('should save before changing displayed image to null when saveOnNavigate is true', () => {
    const saveSpy = spyOn(component.editor, 'save');
    const startItem = MockImage.new(1);
    const nextItem = null;
    configService.saveOnNavigate = true;

    component.selected = startItem;
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.editor.target).toEqual(startItem);

    component.onSelectionChange(nextItem);
    fixture.detectChanges();

    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(component.editor.target).toEqual(nextItem);
  });

  it('should not save before changing displayed image when saveOnNavigate is false', () => {
    const saveSpy = spyOn(component.editor, 'save');
    const startItem = MockImage.new(1);
    const nextItem = MockImage.new(2);
    configService.saveOnNavigate = false;

    component.selected = startItem;
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.editor.target).toEqual(startItem);

    component.onSelectionChange(nextItem);
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.editor.target).toEqual(nextItem);
  });
});


describe('TagPageComponent', () => {
  let configService: ConfigService;
  let component: TagPageComponent;
  let fixture: ComponentFixture<TagPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        MockComponent(TagEditorComponent),
        TagPageComponent,
      ],
      imports: [
        NoopAnimationsModule,
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    })
    .compileComponents();
    configService = TestBed.inject(ConfigService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TagPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should change displayed image', () => {
    const startItem = MockTagD.new(1);
    const nextItem = MockTagD.new(2);

    component.selected = startItem;
    fixture.detectChanges();

    expect(component.editor.target).toEqual(startItem);

    component.onSelectionChange(nextItem);
    fixture.detectChanges();

    expect(component.editor.target).toEqual(nextItem);
  });

  it('should save before changing displayed image when saveOnNavigate is true', () => {
    const saveSpy = spyOn(component.editor, 'save');
    const startItem = MockTagD.new(1);
    const nextItem = MockTagD.new(2);
    configService.saveOnNavigate = true;

    component.selected = startItem;
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.editor.target).toEqual(startItem);

    component.onSelectionChange(nextItem);
    fixture.detectChanges();

    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(component.editor.target).toEqual(nextItem);
  });

  it('should not save before changing displayed image when saveOnNavigate is false', () => {
    const saveSpy = spyOn(component.editor, 'save');
    const startItem = MockTagD.new(1);
    const nextItem = MockTagD.new(2);
    configService.saveOnNavigate = false;

    component.selected = startItem;
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.editor.target).toEqual(startItem);

    component.onSelectionChange(nextItem);
    fixture.detectChanges();

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.editor.target).toEqual(nextItem);
  });
});
