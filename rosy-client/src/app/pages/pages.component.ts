import {Component, Input, ViewChild, } from '@angular/core';
import {Subscription} from 'rxjs';

import {
  ArtistEditorComponent,
  ImageViewerComponent,
  NameEditorComponent,
  TagEditorComponent,
} from '@app/shared/content-editor';
import {ArtistDetails, Image, NameDetails, TagDetails, } from '@app/models';
import {ConfigService} from '@app/services';

import {
  ArtistBrowserComponent,
  DeletedBrowserComponent,
  ImageBrowserComponent,
  NameBrowserComponent,
  TagBrowserComponent,
} from './browsers';
import {BasePageDirective} from './base-page.directive';


@Component({
  selector: 'rx-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./pages.scss']
})
export class ArtistPageComponent
extends BasePageDirective<ArtistDetails, ArtistBrowserComponent, ArtistEditorComponent> {}


@Component({
  selector: 'rx-eigendata-page',
  template: '<rx-eigendata-browser style="width: 100%"></rx-eigendata-browser>',
  styleUrls: ['./pages.scss']
})
export class EigendataPageComponent {}


@Component({
  selector: 'rx-image-page',
  templateUrl: './image-page.component.html',
  styleUrls: ['./pages.scss']
})
export class ImagePageComponent
extends BasePageDirective<Image, ImageBrowserComponent, ImageViewerComponent> {}


@Component({
  selector: 'rx-delete-page',
  templateUrl: './delete-page.component.html',
  styleUrls: ['./pages.scss']
})
export class DeletePageComponent
extends BasePageDirective<Image, DeletedBrowserComponent, ImageViewerComponent> {}


@Component({
  selector: 'rx-name-page',
  templateUrl: './name-page.component.html',
  styleUrls: ['./pages.scss']
})
export class NamePageComponent
extends BasePageDirective<NameDetails, NameBrowserComponent, NameEditorComponent> {}


@Component({
  selector: 'rx-tag-page',
  templateUrl: './tag-page.component.html',
  styleUrls: ['./pages.scss']
})
export class TagPageComponent
extends BasePageDirective<TagDetails, TagBrowserComponent, TagEditorComponent> {}
