import {HttpParams} from '@angular/common/http';
import {NavigationExtras, Params, Router, } from '@angular/router';


import {
  DUPE_CANDIDATES_DEFAULT_PARAMS,
  GROUP_DEFAULT_PARAMS,
  IMAGE_DEFAULT_PARAMS,
  IMAGE_INBOX_PARAMS,
} from '@app/models';

/**
 * Represents a unique page provided by the app.
 */
export class AppPage {
  /**
   * The path portion (without `/` prefix) of the URL for this page.
   */
  link: string;

  /**
   * A default set of URL query params to set when navigating to this page.
   */
  params: Params;

  /**
   * A label associated with this page for use in the navigation pane.
   */
  label: string;

  /**
   * An icon associated with this page for use in navigation options.
   */
  icon: string;

  constructor(label: string, link: string, icon: string = 'lock', params: Params = {}) {
    this.label = label;
    this.link = link;
    this.params = params;
    this.icon = icon;
  }

  /**
   * Converts link and query params into full URL with query params included.
   */
  getFullUrl(): string {
    return '/' + this.link + '?' + new HttpParams({ fromObject: this.params }).toString();
  }

  /**
   * Returns the link portion of URL with a `/` at the front.
   */
  getBaseUrl(): string {
    return '/' + this.link;
  }

  /**
   * Navigates to the application page defined by this instance.
   */
  navigate(router: Router, extraParams: Params | null = null) {
    const extras: NavigationExtras = {
      queryParams: Object.assign(extraParams || {}, this.params)
    };
    router.navigate([this.link], extras);
  }

}

export const APP_PAGES: {
  admin: AppPage,
  artists: AppPage,
  dupes: AppPage,
  groups: AppPage,
  image: AppPage,
  images: AppPage,
  inbox: AppPage,
  login: AppPage,
  metadata: AppPage,
  names: AppPage,
  tags: AppPage,
  upload: AppPage,
} = {
    admin: new AppPage('Admin', 'admin', 'lock'),
    images: new AppPage('Images', 'library/images', 'photo_library', IMAGE_DEFAULT_PARAMS),
    groups: new AppPage('Groups', 'library/groups', 'folder', GROUP_DEFAULT_PARAMS),
    artists: new AppPage('Artists', 'metadata/artists', 'person'),
    names: new AppPage('Names', 'metadata/names', 'face'),
    tags: new AppPage('Tags', 'metadata/tags', 'tag'),
    metadata: new AppPage('Metadata analysis', 'metadata/analysis', 'functions'),
    dupes: new AppPage('Dupe candidates', 'dupe_candidates', 'people_outline', DUPE_CANDIDATES_DEFAULT_PARAMS),
    upload: new AppPage('Upload files', 'upload', 'upload'),
    inbox: new AppPage('Inbox', 'library/images', 'inbox', IMAGE_INBOX_PARAMS),
    login: new AppPage('Login', 'login', 'login'),
    image: new AppPage('Image', 'image', 'image'),
};

/**
 * Defines how pages appear in the navigation side pane. Not all application pages are exposed here.
 */
export const SORTED_NAV_ROUTES: AppPage[][] = [
  [ APP_PAGES.images, APP_PAGES.groups ],
  [ APP_PAGES.upload ],
  [ APP_PAGES.artists, APP_PAGES.names, APP_PAGES.tags ],
  [ APP_PAGES.admin, APP_PAGES.metadata, APP_PAGES.dupes,  ],
];

export const HOME_PAGE = APP_PAGES.images;

export const HOME_PAGE_URL = HOME_PAGE.getFullUrl();

export const LOGIN_PAGE = APP_PAGES.login;
