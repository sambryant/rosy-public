import {
  catchError,
  delay,
  finalize,
  last,
  map,
  mapTo,
  shareReplay,
  skip,
  take,
  tap,
} from 'rxjs/operators';
import {
  concat,
  forkJoin,
  of,
  throwError,
  Subject,
  BehaviorSubject,
  Observable,
  OperatorFunction,
  ReplaySubject,
  Subscription,
} from 'rxjs';
import {DomSanitizer, SafeUrl, } from '@angular/platform-browser';
import {Injectable} from '@angular/core';

import {
  ApiService,
  FaceSuggestResponse,
  GetImageResponse,
  GetMetadataResponse,
  ItemResponse,
  QueryArtistsResponse,
  QueryDupeCandidatesResponse,
  QueryEigendataResponse,
  QueryFaceMatchesResponse,
  QueryGroupsResponse,
  QueryImagesResponse,
  QueryMarkedDupesResponse,
  QueryNamesResponse,
  QueryTagsResponse,
  RateImageResponse,
  UpdateGroupResponse,
  UpdateImageResponse,
} from './api.service';
import {
  Artist,
  ArtistId,
  ArtistQuery,
  Content,
  ContentMeta,
  ContentType,
  ContentTypeMap,
  ContentValue,
  CONTENT_TYPES,
  DeletedQuery,
  DupeCandidatesQuery,
  DupeType,
  EigendataQuery,
  Feature,
  GroupId,
  Group,
  GroupQuery,
  GroupSimple,
  Id,
  Image,
  ImageId,
  ImageQuery,
  Name,
  NameId,
  NameQuery,
  Point,
  Tag,
  TagId,
  TagQuery,
} from '@app/models';
import {ConfigService} from './config.service';
import {LoggerService} from './logger.service';
import {NotificationService} from './notification.service';


function doBefore<T>(observable: Observable<T>, startFn: () => void): Observable<T> {
  return concat(
    of(null).pipe(tap(startFn)),
    observable
  ).pipe(skip(1)) as Observable<T>;
}


function writebackResponseData<T extends Content>(item: T): OperatorFunction<ItemResponse<T>, Id> {
  return map((response: ItemResponse<T>) => {
    Object.assign(item, response && response.item || {});
    return item.id;
  });
}


@Injectable({
  providedIn: 'root'
})
export class LibraryService {
  readonly MISSING_IMAGE = '/assets/missing.svg';
  readonly MOVIE_THUMBNAIL_IMAGE = '/assets/movie.svg';
  readonly IMAGE_THUMBNAIL_IMAGE = '/assets/image.svg';

  public groupImagesUpdated$ = new Subject<null>();

  public imageUpdated$ = new Subject<ImageId[]>();

  public requestInProgress$ = new BehaviorSubject<boolean>(false);

  public contentDeleted$ = {
    artist: new Subject<Id[]>(),
    feature: new Subject<Id[]>(),
    group: new Subject<Id[]>(),
    image: new Subject<Id[]>(),
    imageFeature: new Subject<Id[]>(),
    name: new Subject<Id[]>(),
    tag: new Subject<Id[]>(),
  };

  public contentUpdated$: ContentTypeMap<Subject<Id[]>> = {
    artist: new Subject(),
    feature: new Subject(),
    group: new Subject(),
    name: new Subject(),
    tag: new Subject(),
  };

  /**
   * Counter used to keep track of the number of outstanding save requests.
   */
  private requestCount = 0;

  private metadataRequested = false;
  private metadataRequest: Subscription | null = null;

  /**
   * Map from the Id of objects to the index in the corresponding metadata list.
   */
  protected contentIdLookup: ContentTypeMap<{ [key: number]: number }> = {
    artist: {},
    feature: {},
    group: {},
    name: {},
    tag: {},
  };
  protected contentListUpdated: ContentTypeMap<ReplaySubject<null>> = {
    artist: new ReplaySubject(),
    feature: new ReplaySubject(),
    group: new ReplaySubject(),
    name: new ReplaySubject(),
    tag: new ReplaySubject(),
  };
  protected contentListValues: ContentTypeMap<ContentValue[]> = {
    artist: [],
    feature: [],
    group: [],
    name: [],
    tag: []
  };

  constructor(
    protected configService: ConfigService,
    protected logger: LoggerService,
    protected notificationService: NotificationService,
    protected apiService: ApiService,
    protected sanitizer: DomSanitizer) {
    // Empty cached metadata when user changes schema
    this.configService.databaseSchema$.subscribe(_ => {
      this.refreshMetadata();
    });
  }

  queryArtists(query: ArtistQuery): Observable<QueryArtistsResponse> {
    return this.wrapRequest(this.apiService.queryArtists(query), false);
  }

  queryDeleted(query: DeletedQuery): Observable<QueryImagesResponse> {
    return this.wrapRequest(this.apiService.queryDeleted(query), false);
  }

  queryDupeCandidates(query: DupeCandidatesQuery): Observable<QueryDupeCandidatesResponse> {
    return this.wrapRequest(this.apiService.queryDupeCandidates(query), false);
  }

  queryEigendata(query: EigendataQuery): Observable<QueryEigendataResponse> {
    return this.wrapRequest(this.apiService.queryEigendata(query), false);
  }

  queryGroups(query: GroupQuery): Observable<QueryGroupsResponse> {
    return this.wrapRequest(this.apiService.queryGroups(query), false);
  }

  queryImages(query: ImageQuery): Observable<QueryImagesResponse> {
    return this.wrapRequest(this.apiService.queryImages(query), false);
  }

  queryMarkedDupes(image: Image): Observable<QueryMarkedDupesResponse> {
    return this.wrapRequest(this.apiService.queryMarkedDupes(image), false);
  }

  queryFaceMatches(image: Image): Observable<QueryFaceMatchesResponse> {
    return this.wrapRequest(this.apiService.queryFaceMatches(image), false);;
  }

  queryNames(query: NameQuery): Observable<QueryNamesResponse> {
    return this.wrapRequest(this.apiService.queryNames(query), false);;
  }

  queryTags(query: TagQuery): Observable<QueryTagsResponse> {
    return this.wrapRequest(this.apiService.queryTags(query), false);;
  }

  private refreshMetadata() {
    if (this.metadataRequested) {
      if (this.metadataRequest) {
        this.metadataRequest.unsubscribe();
      }
      this.metadataRequested = false;
      this.startMetadataRequest();
    }
  }

  private startMetadataRequest() {
    if (this.metadataRequested) {
      return;
    }
    this.metadataRequested = true;
    this.metadataRequest = this.apiService.getMetadata().subscribe((metadata: GetMetadataResponse) => {
      CONTENT_TYPES.forEach((type: ContentType) => {
        this.contentListValues[type] = metadata[type][0];
        this.contentIdLookup[type] = metadata[type][1];
        this.contentListUpdated[type].next(null);
      });
    });
  }

  getContentValueById(type: ContentType, id: number): Observable<string | undefined> {
    this.startMetadataRequest();
    return this.contentListUpdated[type].pipe(map(() => {
      const index = this.contentIdLookup[type][id];
      if (index !== undefined) {
        return this.contentListValues[type][index].value;
      } else {
        return undefined;
      }
    }));
  }

  getContentList(type: ContentType): Observable<ContentValue[]> {
    this.startMetadataRequest();
    return this.contentListUpdated[type].pipe(map(() => this.contentListValues[type]));
  }

  handleErrorResponse(err) {
    this.logger.error('Request returned error: ', err);
    this.notificationService.error(err.error && err.error.message || 'Unknown error');
  }

  protected updateRequestCount(increment) {
    this.requestCount += increment;
    const inProgress = this.requestCount !== 0;
    if (inProgress !== this.requestInProgress$.value) {
      this.requestInProgress$.next(inProgress);
    }
  }

  private wrapRequest<T>(obs: Observable<T>, catchStandard: boolean = true): Observable<T> {
    return doBefore(obs, () => { this.updateRequestCount(+1); })
      .pipe(finalize(() => { this.updateRequestCount(-1); }))
      .pipe(catchError(err => {
        this.handleErrorResponse(err);
        if (catchStandard) {
          return of(err && err.error || {});
        } else {
          return throwError(err);
        }
      }));
  }

  createContent(type: ContentType, value: string, images: Image[]=[]): Observable<ContentValue> {
    if (type === 'artist') {
      return this.createArtist(value, images);
    } else if (type === 'feature') {
      return this.createFeature(value)
    } else if (type === 'group') {
      return this.createGroup(value, images);
    } else if (type === 'name') {
      return this.createName(value, images);
    } else if (type === 'tag') {
      return this.createTag(value, images);
    } else {
      throw new Error('Unreachable');
    }
  }

  protected createArtist(name: string, images: Image[]): Observable<Artist> {
    return this.wrapRequest(this.apiService.createArtist(name, images), false)
      .pipe(map(response => response.item))
      .pipe(tap((artist) => {
        images.forEach(image => {
          image.artistId = artist.id;
        });
        this.contentUpdated$.artist.next([artist.id]);
      }))
      .pipe(finalize(() => {
        this.refreshMetadata();
        if (images.length > 0) {
          this.imageUpdated$.next(images.map(image => image.id));
        }
      }));
  }

  protected createFeature(value: string): Observable<Feature> {
    return this.wrapRequest(this.apiService.createFeature(value), false)
      .pipe(map(response => response.item))
      .pipe(tap((feature) => {
        this.contentUpdated$.feature.next([feature.id]);
      }))
      .pipe(finalize(() => {
        this.refreshMetadata();
      }));
  }

  createImageFeature(image: Image, feature: Feature, p1: Point, p2: Point) {
    this.wrapRequest(this.apiService.createImageFeature(image, feature, p1, p2))
      .pipe(writebackResponseData(image))
      .subscribe((id) => {
        this.imageUpdated$.next([id]);
      });
  }

  protected createGroup(title: string, images: Image[]): Observable<Group> {
    return this.wrapRequest(this.apiService.createGroup(title, images), false)
      .pipe(map(response => response.item))
      .pipe(tap((group) => {
        images.forEach(image => {
          image.groupId = group.id;
        });
        this.contentUpdated$.group.next([group.id]);
      }))
      .pipe(finalize(() => {
        this.refreshMetadata();
        if (images.length > 0) {
          this.imageUpdated$.next(images.map(image => image.id));
          this.groupImagesUpdated$.next(null);
        }
      }));
  }

  protected createName(value: string, images: Image[]): Observable<Name> {
    return this.wrapRequest(this.apiService.createName(value, images), false)
      .pipe(map(response => response.item))
      .pipe(tap((name) => {
        images.forEach(image => {
          image.names.push(name);
        });
      }))
      .pipe(finalize(() => {
        this.refreshMetadata();
        if (images.length > 0) {
          this.imageUpdated$.next(images.map(image => image.id));
        }
      }));
  }

  protected createTag(value: string, images: Image[]): Observable<Tag> {
    return this.wrapRequest(this.apiService.createTag(value, images), false)
      .pipe(map(response => response.item))
      .pipe(tap((tag) => {
        images.forEach(image => {
          image.tags.push(tag);
        });
      }))
      .pipe(finalize(() => {
        this.refreshMetadata();
        if (images.length > 0) {
          this.imageUpdated$.next(images.map(image => image.id));
        }
      }));
  }

  deleteArtist(id: ArtistId) {
    this.wrapRequest(this.apiService.deleteArtist(id))
      .subscribe(() => this.contentDeleted$.artist.next([id]));
  }

  deleteFeature(id: Id) {
    this.wrapRequest(this.apiService.deleteFeature(id))
      .subscribe(() => this.contentDeleted$.feature.next([id]));
  }

  deleteGroup(id: GroupId) {
    this.wrapRequest(this.apiService.deleteGroup(id))
      .subscribe(() => this.contentDeleted$.group.next([id]));
  }

  deleteImages(ids: ImageId[]) {
    forkJoin(ids.map(id => this.wrapRequest(this.apiService.deleteImage(id))))
      .subscribe(() => this.contentDeleted$.image.next(ids));
  }

  deleteImage(id: ImageId) {
    this.wrapRequest(this.apiService.deleteImage(id))
      .subscribe(() => this.contentDeleted$.image.next([id]));
  }

  deleteImageFeature(image: Image, imageFeatureId: Id) {
    this.wrapRequest(this.apiService.deleteImageFeature(imageFeatureId))
      .pipe(tap(() => {
        // Writeback won't work here since it doesn't return an image copy.
        // However, we can manually apply it assuming no errors have occurred. This is a bit risky.
        if (image.features) {
          const ind = image.features.findIndex(f => f.id === imageFeatureId);
          if (ind >= 0) {
            image.features.splice(ind, 1);
          }
          const newIds = image.features!.map((f) => f.id);
        }
      }))
      .subscribe(() => {
        this.imageUpdated$.next([image.id]);
        this.contentDeleted$.imageFeature.next([imageFeatureId])
      });
  }

  deleteName(id: NameId) {
    this.wrapRequest(this.apiService.deleteName(id))
      .subscribe(() => this.contentDeleted$.name.next([id]));
  }

  deleteTag(id: TagId) {
    this.wrapRequest(this.apiService.deleteTag(id))
      .subscribe(() => this.contentDeleted$.tag.next([id]));
  }

  faceSuggest(id: ImageId): Observable<Name[]> {
    // Don't wrap request - this causes notification on error but we want it to be invisible.
    return this.apiService.faceSuggest(id)
      .pipe(map((response) => response.items));
  }

  getImage(id: ImageId): Observable<GetImageResponse> {
    return this.wrapRequest(this.apiService.getImage(id), false);
  }

  markDupes(images: Image[], dupeType: DupeType): Observable<any> {
    const ids = images.map(target => target.id);

    const obs = this.wrapRequest(this.apiService.markDupes(ids, dupeType))
      .pipe(tap(() => {
        images.forEach(image => {
          ids.forEach(id => {
            if (image.id !== id) {
              image.dupes[id] = dupeType;
            }
          });
        });}))
      .pipe(shareReplay(1));
    obs.subscribe();
    return obs;
  }

  permadeleteImages(ids: ImageId[]) {
    forkJoin(ids.map(id => this.wrapRequest(this.apiService.permadeleteImage(id))))
      .subscribe(() => this.contentDeleted$.image.next(ids));
  }

  restoreImages(ids: ImageId[]) {
    forkJoin(ids.map(id => this.wrapRequest(this.apiService.restoreImage(id))))
      .subscribe(() => this.contentDeleted$.image.next(ids));
  }

  unmarkDupes(images: Image[]): Observable<any> {
    const ids = images.map(target => target.id);

    const obs = this.wrapRequest(this.apiService.unmarkDupes(ids))
      .pipe(tap(() => {
        images.forEach(image => {
          ids.forEach(id => {
            delete image.dupes[id];
          });
        });}))
      .pipe(shareReplay(1));
    obs.subscribe();
    return obs;
  }

  updateArtist(item: Artist) {
    this.wrapRequest(this.apiService.updateArtist(item))
      .pipe(writebackResponseData(item))
      .pipe(tap(() => this.updateContentList({type: 'artist', item})))
      .subscribe((id) => this.contentUpdated$.artist.next([id]));
  }

  updateFeature(item: Feature) {
    this.wrapRequest(this.apiService.updateFeature(item))
      .pipe(writebackResponseData(item))
      .pipe(tap(() => this.updateContentList({type: 'feature', item})))
      .subscribe((id) => this.contentUpdated$.feature.next([id]));
  }

  updateGroups(groups: Group[]) {
    forkJoin(groups.map(group => this.wrapRequest(this.apiService.updateGroup(group))
      .pipe(writebackResponseData(group))
      .pipe(tap(() => this.updateContentList({type: 'group', item: group})))
    )).subscribe((ids) => this.contentUpdated$.group.next(ids));
  }

  updateGroup(group: Group) {
    this.wrapRequest(this.apiService.updateGroup(group))
      .pipe(writebackResponseData(group))
      .pipe(tap(() => this.updateContentList({type: 'group', item: group})))
      .subscribe((id) => this.contentUpdated$.group.next([id]));
  }

  updateImages(images: Image[], groupModified: boolean = false): Observable<any> {
    const subject = new Subject();
    forkJoin(images.map(image => this.wrapRequest(this.apiService.updateImage(image))
      .pipe(writebackResponseData(image))
    )).pipe(map((ids) => {
      if (groupModified) {
        this.groupImagesUpdated$.next(null);
      }
      this.imageUpdated$.next(ids);
    })).subscribe(subject);
    return new Observable((observer) => subject.subscribe(observer));
  }

  updateImage(image: Image, groupModified: boolean = false) {
    this.wrapRequest(this.apiService.updateImage(image))
      .pipe(writebackResponseData(image))
      .subscribe((id) => {
        this.imageUpdated$.next([id]);
        if (groupModified) {
          this.groupImagesUpdated$.next(null);
        }
      });
  }

  updateName(item: Name) {
    this.wrapRequest(this.apiService.updateName(item))
      .pipe(writebackResponseData(item))
      .pipe(tap(() => this.updateContentList({type: 'name', item})))
      .subscribe((id) => this.contentUpdated$.name.next([id]));
  }

  updateTag(item: Tag) {
    this.wrapRequest(this.apiService.updateTag(item))
      .pipe(writebackResponseData(item))
      .pipe(tap(() => this.updateContentList({type: 'tag', item})))
      .subscribe((id) => this.contentUpdated$.tag.next([id]));
  }

  protected updateContentList({type, item}: ContentMeta) {
    const objectIndex = this.contentIdLookup[type][item.id];
    if (objectIndex === undefined) {
      this.contentListValues[type].push(item);
      this.contentIdLookup[type][item.id] = this.contentListValues[type].length - 1;
    } else {
      this.contentListValues[type][objectIndex] = item;
    }
    this.contentListUpdated[type].next(null);
  }

  rateImage(image: Image, rating: number, clearExisting: boolean): Observable<any> {
    const obs = this.wrapRequest(this.apiService.rateImage(image.id, rating, clearExisting))
      .pipe(tap((response) => {
        image.ratingAverage = response.average || image.ratingAverage;
        image.ratingCount = response.count || image.ratingCount;
      }))
      .pipe(tap(() => this.imageUpdated$.next([image.id])))
      .pipe(shareReplay(1));
    obs.subscribe();
    return obs;
  }

  /**
   * Generates url which points towards file blob stored in memory, used by getImageBlobUrl.
   *
   * This method has been split out to make unit testing cleaner.
   */
  getUrlFromBlob(blob: Blob): SafeUrl {
    const createdUrl = window.URL.createObjectURL(blob);
    return this.sanitizer.bypassSecurityTrustUrl(createdUrl);
  }

  /**
   * Creates cold observable for image file blob and returns generated URL to that blob.
   *
   * This observable is NOT multicast and thus should not be called multiple times.
   */
  getImageBlobUrl(image: Image): Observable<SafeUrl> {
    return this.apiService.getImageBlob(image).pipe(
      take(1),
      map((blob: Blob) => this.getUrlFromBlob(blob)),
      catchError(err => of(this.MISSING_IMAGE as SafeUrl))
    );
  }

  /**
   * Creates cold observable for image file blob and returns generated URL to that blob.
   *
   * This observable is NOT multicast and thus should not be called multiple times.
   */
  getThumbnailBlobUrl(image: Image): Observable<SafeUrl> {
    return this.apiService.getThumbnailBlob(image).pipe(
      take(1),
      map((blob: Blob) => this.getUrlFromBlob(blob)),
      catchError(err => {
        if (image.isVideo) {
          return of(this.MOVIE_THUMBNAIL_IMAGE as SafeUrl);
        } else {
          return of(this.IMAGE_THUMBNAIL_IMAGE as SafeUrl);
        }
      })
    );
  }
}
