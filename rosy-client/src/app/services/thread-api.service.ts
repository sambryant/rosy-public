import {take} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Observable, Subscription} from 'rxjs';

import {ApiService, ThreadStatusResponse, } from './api.service';
import {LoggerService} from './logger.service';

/**
 * This service is used to interact with threaded requests to the server.
 *
 * Generally these are API requests which spawn server threads which take a long
 * time to complete. The server responds to the initial request with an ID that
 * uniquely identifies the corresponding thread.
 *
 * Afterwards, the client may request a progress update from the server using
 * the thread's ID. The server then responds with the progress (fraction of job
 * completed) as well as flags indicating that it has finished or failed.
 *
 * It is up to the client to pool the server to determine the thread's status.
 */
@Injectable({
  providedIn: 'root'
})
export class ThreadApiService {

  constructor(protected apiService: ApiService, protected logger: LoggerService) { }

  startThread(request: Observable<ThreadStatusResponse>, delay: number): Observable<ThreadStatusResponse> {

    return new Observable(subscriber => {
      let startSubscription = Subscription.EMPTY; // subscription to request thread start
      let updateSubscription = Subscription.EMPTY; // subscription to request thread updates

      startSubscription = request.pipe(take(1)).subscribe({
        next: (response: ThreadStatusResponse) => {
          subscriber.next(response);
          updateSubscription = this.getThreadUpdates(response.thread_id, delay).subscribe(subscriber);
        },
        error: err => subscriber.error(err)
      });
      return {
        unsubscribe() {
          startSubscription.unsubscribe();
          updateSubscription.unsubscribe();
        }
      };
    });
  }

  private getThreadUpdates(threadId: number, delay: number): Observable<ThreadStatusResponse> {
    return new Observable(subscriber => {
      let updateSubscription = Subscription.EMPTY;
      let timeoutId: number | null = null;
      const requestUpdate = () => {
        updateSubscription = this.apiService.getThreadStatus(threadId)
          .pipe(take(1))
          .subscribe({
            next: (progress: ThreadStatusResponse) => {
              subscriber.next(progress);
              if (progress.status.state !== 'active') {
                subscriber.complete();
              } else {
                timeoutId = setTimeout(requestUpdate, delay) as any;
              }
            },
            error: err => subscriber.error(err)
          });
      };
      setTimeout(requestUpdate, delay);

      return {
        unsubscribe() {
          updateSubscription.unsubscribe();
          if (timeoutId) {
            clearTimeout(timeoutId);
          }
        }
      };
    });
  }

}
