import {ActivatedRouteSnapshot, Router, CanActivate, RouterStateSnapshot, } from '@angular/router';
import {Injectable} from '@angular/core';

import {AuthService} from './auth.service';
import {LOGIN_PAGE} from '@app/app-page.model';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(_: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.authService.checkLoggedIn()) {
      LOGIN_PAGE.navigate(this.router, {returnUrl: encodeURI(state.url)});
      return false;
    }
    return true;
  }
}
