import {
  concat,
  of,
  Observable,
  Subject,
  Subscription,
} from 'rxjs';
import {
  delay,
  tap,
  take,
  takeUntil,
  map,
  shareReplay,
} from 'rxjs/operators';
import {EventEmitter, Injectable, } from '@angular/core';
import {SafeUrl} from '@angular/platform-browser';

import {ConfigService} from './config.service';
import {Image, ImageId, } from '@app/models';
import {LibraryService} from './library.service';
import {LoggerService} from './logger.service';


/**
 * This is the number of open blob requests the fetcher will make to the server before pausing.
 */
export const MAX_CONCURRENT_BLOB_REQUESTS = 20;


/**
 * This is the order that the adjacent images are loaded when a given image is loaded.
 * This has a healthy balance of favoring forward movement, while not neglecting backward movement.
 *
 * Implementation note: when put into instance, the order shown here is reversed. This is because
 * the highest priority target is put at the end of the queue.
 */
export const PRE_FETCH_LOAD_PATTERN = [0, +1, +2, +3, -1];


/**
 * Class representing a page from a query response used to pre-fetch image blobs.
 */
class ImagePool {

  // list of images in order expected to load from
  private images: Image[];

  // map from image id to index within `images`
  private idToIndexMap: Map<ImageId, number>;

  constructor() {
    this.setImages([]);
  }

  setImages(images: Image[]) {
    this.images = images;
    this.idToIndexMap = new Map();
    this.images.forEach((image: Image, index: number) => {
      this.idToIndexMap.set(image.id, index);
    });
  }

  getImage(index: number): Image {
    return this.images[index];
  }

  getIndex(image: Image): number {
    const index = this.idToIndexMap.get(image.id);
    if (index === undefined) {
      throw new Error(`Image ${image.id} is not in pool`);
    }
    return index;
  }
}


class BlobCache {

  // Map from image id to url giving stored image blob
  // TODO: Implement gradual cache clearing
  private blobCache: Map<ImageId, Observable<SafeUrl>> = new Map();

  private maxNumberConcurrentRequests = MAX_CONCURRENT_BLOB_REQUESTS;

  private cancelSubscriptions = new EventEmitter<void>();

  private pendingBlobRequests: Image[] = [];

  private currentBlobRequests: ImageId[] = [];

  constructor(protected libraryService: LibraryService, protected logger: LoggerService) {}

  clearCache() {
    this.cancelAll();
    this.blobCache.clear();
  }

  setMaxNumberConcurrentRequests(num: number): void {
    this.maxNumberConcurrentRequests = num;
  }

  cancelAll(): void {
    this.pendingBlobRequests = [];
    this.cancelSubscriptions.emit();
    this.currentBlobRequests.forEach((id) => {
      this.blobCache.delete(id);
    });
    this.currentBlobRequests = [];
  }

  hasBlob(image: Image): boolean {
    return this.blobCache.has(image.id);
  }

  private isAcceptingRequests(): boolean {
    return this.currentBlobRequests.length < this.maxNumberConcurrentRequests;
  }

  getBlob(image: Image): Observable<SafeUrl> {
    let observable = this.blobCache.get(image.id);
    if (observable === undefined) {
      observable = this.createNewBlobRequest(image);
    }
    return observable;
  }

  setPending(images: Image[]) {
    this.pendingBlobRequests = images;
    this.processPendingQueue();
  }

  private processPendingQueue() {
    while (this.pendingBlobRequests.length > 0 && this.isAcceptingRequests()) {
      const image = this.pendingBlobRequests.pop()!;
      if (!this.hasBlob(image)) {
        this.createNewBlobRequest(image);
      } else {
        this.logger.warn('Warning: scheduled prefetch already loaded');
      }
    }
  }

  private markAsFinished(id: ImageId): void {
    const ind = this.currentBlobRequests.indexOf(id);
    this.currentBlobRequests.splice(ind, 1);
    this.processPendingQueue();
  }

  private createNewBlobRequest(image: Image): Observable<SafeUrl> {
    const id = image.id;

    // Create cold observable for API request for image blob data.
    // we need to make sure it only fires once, and that it stops if cancel is called.
    const observable = this.libraryService.getImageBlobUrl(image).pipe(
      take(1),
      takeUntil(this.cancelSubscriptions),
      tap(() => { this.markAsFinished(id); }),
      shareReplay(1));

    // Activate observable
    this.currentBlobRequests.push(id);
    this.blobCache.set(id, observable);
    observable.subscribe();

    return observable;
  }
}


/**
 * Service that retrieves image blobs from server using an eager model, improving app performance.
 *
 * **Warning**: this is a singleton service, but it is stateful in a non-trivial way. If multiple
 * sets of components are using this simultaneously, it will cause many errors.
 *
 */
@Injectable({
  providedIn: 'root'
})
export class ImageFetcherService {

  private blobCache: BlobCache;

  private imagePool: ImagePool = new ImagePool();

  /**
   * List of pending images to prefetch with the highest priority at *end* of the list.
   */
  private preFetchLoadPattern: number[];

  constructor(configService: ConfigService, libraryService: LibraryService, logger: LoggerService) {
    this.setPrefetchStrategy(PRE_FETCH_LOAD_PATTERN);
    this.blobCache = new BlobCache(libraryService, logger);
    configService.databaseSchema$.subscribe(() => {
      this.clearCache();
    });
  }

  clearCache() {
    this.imagePool.setImages([]);
    this.blobCache.clearCache();
  }

  setMaxNumberConcurrentRequests(num: number): void {
    this.blobCache.setMaxNumberConcurrentRequests(num);
  }

  /**
   * Sets order that images will be prefetched in terms of indices relative to last requested image.
   *
   * Example:
   *
   * Suppose `preFetchLoadPattern` is `[+1, -1, +2]`. If the user requests image 10, the system will
   * automatically prefetch images 11, 9, 12 (in that order). Here the image numbers refer to their
   * indices with respect to the expected image array, not their IDs.
   */
  setPrefetchStrategy(preFetchLoadPattern: number[]) {
    // reverse given pattern because prefetch queue is stored with low-to-high priority.
    this.preFetchLoadPattern = preFetchLoadPattern.slice().reverse();
  }

  /**
   * Begins prefetching images neighboring the given image with respect to the expected images.
   *
   * The set of images which are prefetched is determined by the prefetching strategy.
   */
  prefetchFromImage(image: Image) {
    // Refresh prefetching queue based on current position in the expected images.
    const baseIndex = this.imagePool.getIndex(image); // may throw errow
    this.blobCache.setPending(this.preFetchLoadPattern
      .map(offset => (this.imagePool.getImage(baseIndex + offset)))
      .filter(img => (!!img && !this.blobCache.hasBlob(img))));  // Only add unloaded images.
  }

  getImage(image: Image): Observable<SafeUrl> {
    // Do this before refreshing pending queue so the asynchronous request starts immediately.
    return this.blobCache.getBlob(image);
  }

  setExpectedImages(images: Image[], hintIndex: number | null = null) {
    this.blobCache.cancelAll();
    this.imagePool.setImages(images);
    if (hintIndex !== null) {
      const start = this.imagePool.getImage(hintIndex);
      if (start) {
        this.prefetchFromImage(start);
      }
    }
  }
}
