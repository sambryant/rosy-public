import {map, tap, take, } from 'rxjs/operators';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Params} from '@angular/router';
import {Observable} from 'rxjs';

import {
  Artist,
  ArtistDetails,
  ArtistId,
  ArtistQuery,
  ContentEigendata,
  DeletedQuery,
  DupeCandidate,
  DupeCandidatesQuery,
  DupeType,
  EigendataQuery,
  FaceMatch,
  Feature,
  GenerateDatasetArgs,
  GenerateFeatureDatasetArgs,
  GenerateTagDatasetArgs,
  Group,
  GroupId,
  GroupQuery,
  GroupSimple,
  Id,
  Image,
  ImageQuery,
  ImageId,
  Name,
  NameDetails,
  NameId,
  NameQuery,
  Point,
  Tag,
  TagDetails,
  TagId,
  TagQuery,
} from '@app/models';


export type EmptyResponse = {};

export interface ItemResponse<T> {
  item: T;
}

export interface ItemsResponse<T> {
  count: number;
  items: T[];
}

export type CreateArtistResponse = ItemResponse<Artist>;

export type CreateFeatureResponse = ItemResponse<Feature>;

export type CreateGroupResponse = ItemResponse<Group>;

export type CreateImageFeatureResponse = ItemResponse<Image>;

export type CreateNameResponse = ItemResponse<Name>;

export type CreateTagResponse = ItemResponse<Tag>;

export type FaceSuggestResponse = ItemsResponse<Name>;

export interface LoginResponse {
  access_token: string; // base 64 encoded JWT
}

/**
 * For each content type provides a list of objects and a map from the IDs to the indexes in list.
 */
export interface GetMetadataResponse {
  artist: [Artist[], { [key: number]: number }];
  feature: [Feature[], { [key: number]: number }];
  group: [GroupSimple[], { [key: number]: number }];
  name: [Name[], { [key: number]: number }];
  tag: [Tag[], { [key: number]: number }];
}

export type GetImageResponse = ItemResponse<Image>;

export type QueryArtistsResponse = ItemsResponse<ArtistDetails>;

export type QueryDupeCandidatesResponse = ItemsResponse<DupeCandidate>;

export type QueryEigendataResponse = ItemsResponse<ContentEigendata>;

export type QueryFaceMatchesResponse = ItemsResponse<FaceMatch>;

export type QueryGroupsResponse = ItemsResponse<Group>;

export type QueryImagesResponse = ItemsResponse<Image>;

export type QueryMarkedDupesResponse = ItemsResponse<[Image, DupeType]>;

export type QueryNamesResponse = ItemsResponse<NameDetails>;

export type QueryTagsResponse = ItemsResponse<TagDetails>;

export type UpdateArtistResponse = ItemResponse<Artist>;

export type UpdateFeatureResponse = ItemResponse<Feature>;

export type UpdateGroupResponse = ItemResponse<Group>;

export type UpdateImageResponse = ItemResponse<Image>;

export type UpdateNameResponse = ItemResponse<Name>;

export type UpdateTagResponse = ItemResponse<Tag>;

export interface RateImageResponse {
  average: number;
  count: number;
}

export interface ReindexImagesResponse {
  added: number;
  updated: number;
  failed: number;
}

export type ThreadState = 'active' | 'success' | 'failure';

export interface ThreadStatus {
  current: number;
  total: number;
  state: ThreadState;
  message?: string;
}

export interface ThreadStatusResponse {
  thread_id: number;
  status: ThreadStatus;
}

// Content links
const API_IMAGE_FILE = 'images';
const API_THUMBNAIL_FILE = 'thumbnails';

// Auth endpoints
const API_AUTH_LOGIN = 'auth/login';

// Api endpoints
const API_ADMIN = 'api/admin';
const API_COMPUTE_METADATA = API_ADMIN + '/compute_metadata';
const API_FIX_SEQUENCES = API_ADMIN + '/fix_sequences';
const API_GENERATE_DUPE_REPORT = API_ADMIN + '/generate_dupe_report';
const API_GENERATE_DATASET = API_ADMIN + '/generate_dataset';
const API_GENERATE_FEATURE_DATASET = API_ADMIN + '/generate_feature_dataset';
const API_GENERATE_TAG_DATASET = API_ADMIN + '/generate_tag_dataset';
const API_MERGE_DUPLICATES = API_ADMIN + '/merge_duplicates';
const API_RECOMPUTE_FILESIZES = API_ADMIN + '/recompute_filesizes';
const API_REGENERATE_THUMBNAILS = API_ADMIN + '/regenerate_thumbnails';
const API_REINDEX_IMAGES = API_ADMIN + '/reindex_images';
const API_RESET_FILES = API_ADMIN + '/reset_files';
const API_THREAD_STATUS = API_ADMIN + '/thread_status';

const API_ARTIST = 'api/artist';
const API_FEATURE = 'api/feature';
const API_GROUP = 'api/group';
const API_IMAGE = 'api/image';
const API_IMAGE_FEATURE = 'api/image_feature';
const API_IMAGE_RATE = 'api/image/rate';
const API_IMAGE_MARK_DUPES = 'api/image/mark_dupes';
const API_IMAGE_FACE_SUGGEST = 'api/image/face_suggest';
const API_IMAGE_UNMARK_DUPES = 'api/image/unmark_dupes';
const API_IMAGE_PERMADELETE = 'api/image/permadelete';
const API_IMAGE_RESTORE = 'api/image/restore';
const API_NAME = 'api/name';
const API_TAG = 'api/tag';
const API_METADATA = 'api/metadata';
const API_QUERY = 'api/query';
const API_QUERY_ARTISTS  = API_QUERY + '/artists';
const API_QUERY_EIGENDATA  = API_QUERY + '/eigendata';
const API_QUERY_GROUPS = API_QUERY + '/groups';
const API_QUERY_IMAGES = API_QUERY + '/images';
const API_QUERY_DELETED = API_QUERY + '/deleted';
const API_QUERY_DUPE_CANDIDATES = API_QUERY + '/dupe_candidates';
const API_QUERY_MARKED_DUPES = API_QUERY + '/marked_dupes';
const API_QUERY_FACE_MATCHES = API_QUERY + '/face_matches';
const API_QUERY_NAMES = API_QUERY + '/names';
const API_QUERY_TAGS = API_QUERY + '/tags';

// Misc

/**
 * Service providing raw access to backend API routes.
 *
 * This performs no caching or smart multi-subscriptions. It should not be used directly. Instead
 * its calls should be used through `LibraryService`.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {}

  //////////////////////////////////////////////////////////////////////////////
  // Content Links
  //////////////////////////////////////////////////////////////////////////////
  getImageBlob(image: Image): Observable<Blob> {
    const url = this.getImageUrl(image);
    return this.http.get(url, {responseType: 'blob'});
  }

  getThumbnailBlob(image: Image): Observable<Blob> {
    const url = this.getThumbnailUrl(image);
    return this.http.get(url, {responseType: 'blob'});
  }

  private getImageUrl(image: Image): string {
    return API_IMAGE_FILE + '/' + image.filename;
  }

  private getThumbnailUrl(image: Image): string {
    return API_THUMBNAIL_FILE + '/' + image.thumbnail;
  }

  //////////////////////////////////////////////////////////////////////////////
  // Authentication
  //////////////////////////////////////////////////////////////////////////////
  postLogin(username: string, password: string): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(API_AUTH_LOGIN, {username, password});
  }

  //////////////////////////////////////////////////////////////////////////////
  // Admin
  //////////////////////////////////////////////////////////////////////////////
  computeMetadata(): Observable<EmptyResponse> {
    return this.http.put<EmptyResponse>(API_COMPUTE_METADATA, {});
  }

  fixSequences(): Observable<EmptyResponse> {
    return this.http.put<EmptyResponse>(API_FIX_SEQUENCES, {});
  }

  generateDupeReport(): Observable<EmptyResponse> {
    return this.http.post<EmptyResponse>(API_GENERATE_DUPE_REPORT, {});
  }

  generateDataset(args: GenerateDatasetArgs): Observable<ThreadStatusResponse> {
    return this.http.post<ThreadStatusResponse>(API_GENERATE_DATASET, args);
  }

  generateFeatureDataset(args: GenerateFeatureDatasetArgs): Observable<ThreadStatusResponse> {
    const params: any = args;
    params.features = args.features || null;
    params.includeTags = args.includeTags || null;
    params.excludeTags = args.excludeTags || null;
    return this.http.post<ThreadStatusResponse>(API_GENERATE_FEATURE_DATASET, params);
  }

  generateTagDataset(args: GenerateTagDatasetArgs): Observable<ThreadStatusResponse> {
    return this.http.post<ThreadStatusResponse>(API_GENERATE_TAG_DATASET, args);
  }

  getThreadStatus(threadId: number): Observable<ThreadStatusResponse> {
    const url = API_THREAD_STATUS + '/' + threadId;
    return this.http.get<ThreadStatusResponse>(url);
  }

  mergeDuplicates(): Observable<ThreadStatusResponse> {
    return this.http.put<ThreadStatusResponse>(API_MERGE_DUPLICATES, {});
  }

  recomputeFilesizes(): Observable<ThreadStatusResponse> {
    return this.http.put<ThreadStatusResponse>(API_RECOMPUTE_FILESIZES, {});
  }

  regenerateThumbnails(): Observable<ThreadStatusResponse> {
    return this.http.put<ThreadStatusResponse>(API_REGENERATE_THUMBNAILS, {});
  }

  reindexImages(excludeExisting: boolean): Observable<ThreadStatusResponse> {
    const data = {excludeExisting};
    return this.http.put<ThreadStatusResponse>(API_REINDEX_IMAGES, data);
  }

  resetFiles(): Observable<ThreadStatusResponse> {
    return this.http.put<ThreadStatusResponse>(API_RESET_FILES, {});
  }

  //////////////////////////////////////////////////////////////////////////////
  // Artist
  //////////////////////////////////////////////////////////////////////////////
  createArtist(name: string, images: Image[]): Observable<CreateArtistResponse> {
    const url = API_ARTIST;
    return this.http.post<CreateArtistResponse>(url, {
      value: name,
      imageIds: images.map(i => i.id)
    });
  }

  deleteArtist(id: ArtistId): Observable<EmptyResponse> {
    const url = API_ARTIST + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  updateArtist(artist: Artist): Observable<UpdateArtistResponse> {
    const url = API_ARTIST;
    return this.http.put<UpdateArtistResponse>(url, artist);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Feature
  //////////////////////////////////////////////////////////////////////////////
  createFeature(value: string): Observable<CreateFeatureResponse> {
    const url = API_FEATURE;
    return this.http.post<CreateFeatureResponse>(url, { value });
  }

  deleteFeature(id: Id): Observable<EmptyResponse> {
    const url = API_FEATURE + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  updateFeature(target: Feature): Observable<UpdateFeatureResponse> {
    const url = API_FEATURE;
    return this.http.put<UpdateFeatureResponse>(url, target);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Group
  //////////////////////////////////////////////////////////////////////////////
  createGroup(title: string, images: Image[]): Observable<CreateGroupResponse> {
    const url = API_GROUP;
    return this.http.post<CreateGroupResponse>(url, {
      value: title,
      imageIds: images.map(i => i.id)
    });
  }

  deleteGroup(id: GroupId): Observable<EmptyResponse> {
    const url = API_GROUP + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  updateGroup(group: Group): Observable<UpdateGroupResponse> {
    const url = API_GROUP;
    return this.http.put<UpdateGroupResponse>(url, group);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Image
  //////////////////////////////////////////////////////////////////////////////
  deleteImage(id: ImageId): Observable<EmptyResponse> {
    const url = API_IMAGE + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  faceSuggest(id: ImageId): Observable<FaceSuggestResponse> {
    const url = API_IMAGE_FACE_SUGGEST + '/' + id;
    return this.http.get<FaceSuggestResponse>(url);
  }

  getImage(id: ImageId): Observable<GetImageResponse> {
    const url = API_IMAGE + '/' + id;
    return this.http.get<GetImageResponse>(url);
  }

  markDupes(imageIds: ImageId[], dupeType: DupeType): Observable<EmptyResponse> {
    const url = API_IMAGE_MARK_DUPES;
    const data = { imageIds, dupeType };
    return this.http.put<EmptyResponse>(url, data);
  }

  permadeleteImage(id: ImageId): Observable<EmptyResponse> {
    const url = API_IMAGE_PERMADELETE + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  rateImage(imageId: ImageId, rating: number, clearExisting: boolean): Observable<RateImageResponse> {
    const url = API_IMAGE_RATE;
    const data = { imageId, rating, clearExisting };
    return this.http.post<RateImageResponse>(url, data);
  }

  restoreImage(id: ImageId): Observable<EmptyResponse> {
    const url = API_IMAGE_RESTORE + '/' + id;
    return this.http.put<EmptyResponse>(url, {});
  }

  unmarkDupes(imageIds: ImageId[]): Observable<EmptyResponse> {
    const url = API_IMAGE_UNMARK_DUPES;
    const data = { imageIds };
    return this.http.put<EmptyResponse>(url, data);
  }

  updateImage(image: Image): Observable<UpdateImageResponse> {
    const url = API_IMAGE;
    return this.http.put<UpdateImageResponse>(url, image);
  }

  uploadImage(file: File): Observable<HttpEvent<any>> {
    const url = API_IMAGE;
    return this.http.post<any>(url, file, {
      reportProgress: true,
      observe: 'events'
    });
  }

  //////////////////////////////////////////////////////////////////////////////
  // Image Feature
  //////////////////////////////////////////////////////////////////////////////
  createImageFeature(image: Image, feature: Feature, p1: Point, p2: Point): Observable<CreateImageFeatureResponse> {
    const url = API_IMAGE_FEATURE;
    const data = {
      imageId: image.id,
      featureId: feature.id,
      x1: p1.x,
      x2: p2.x,
      y1: p1.y,
      y2: p2.y,
    }
    return this.http.post<CreateImageFeatureResponse>(url, data);
  }

  deleteImageFeature(id: Id): Observable<EmptyResponse> {
    const url = API_IMAGE_FEATURE + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Misc
  //////////////////////////////////////////////////////////////////////////////
  getMetadata(): Observable<GetMetadataResponse> {
    const url = API_METADATA;
    return this.http.get<GetMetadataResponse>(url);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Name
  //////////////////////////////////////////////////////////////////////////////
  createName(value: string, images: Image[]): Observable<CreateNameResponse> {
    const url = API_NAME;
    return this.http.post<CreateNameResponse>(url, {
      value,
      imageIds: images.map(i => i.id)
    });
  }

  deleteName(id: NameId): Observable<EmptyResponse> {
    const url = API_NAME + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  updateName(name: Name): Observable<UpdateNameResponse> {
    const url = API_NAME;
    return this.http.put<UpdateNameResponse>(url, name);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Tag
  //////////////////////////////////////////////////////////////////////////////
  createTag(value: string, images: Image[]): Observable<CreateTagResponse> {
    const url = API_TAG;
    return this.http.post<CreateTagResponse>(url, {
      value,
      imageIds: images.map(i => i.id)
    });
  }

  deleteTag(id: TagId): Observable<EmptyResponse> {
    const url = API_TAG + '/' + id;
    return this.http.delete<EmptyResponse>(url);
  }

  updateTag(tag: Tag): Observable<UpdateTagResponse> {
    const url = API_TAG;
    return this.http.put<UpdateTagResponse>(url, tag);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Query
  //////////////////////////////////////////////////////////////////////////////
  queryArtists(query: ArtistQuery): Observable<QueryArtistsResponse> {
    const url = API_QUERY_ARTISTS;
    const options = {params: query.toParams()};
    return this.http.get<QueryArtistsResponse>(url, options);
  }

  queryDeleted(query: DeletedQuery): Observable<QueryImagesResponse> {
    const url = API_QUERY_DELETED;
    const options = {params: query.toParams()};
    return this.http.get<QueryImagesResponse>(url, options);
  }

  queryDupeCandidates(query: DupeCandidatesQuery): Observable<QueryDupeCandidatesResponse> {
    const url = API_QUERY_DUPE_CANDIDATES;
    const options = {params: query.toParams()};
    return this.http.get<QueryDupeCandidatesResponse>(url, options);
  }

  queryEigendata(query: EigendataQuery): Observable<QueryEigendataResponse> {
    const url = API_QUERY_EIGENDATA;
    const options = {params: query.toParams()};
    return this.http.get<QueryEigendataResponse>(url, options);
  }

  queryFaceMatches(image: Image): Observable<QueryFaceMatchesResponse> {
    const url = API_QUERY_FACE_MATCHES + '/' + image.id;
    return this.http.get<QueryFaceMatchesResponse>(url);
  }

  queryGroups(query: GroupQuery): Observable<QueryGroupsResponse> {
    const url = API_QUERY_GROUPS;
    const options = {params: query.toParams()};
    return this.http.get<QueryGroupsResponse>(url, options);
  }

  queryImages(query: ImageQuery): Observable<QueryImagesResponse> {
    const url = API_QUERY_IMAGES;
    const options = {params: query.toParams()};
    return this.http.get<QueryImagesResponse>(url, options);
  }

  queryMarkedDupes(image: Image): Observable<QueryMarkedDupesResponse> {
    const url = API_QUERY_MARKED_DUPES + '/' + image.id;
    return this.http.get<QueryMarkedDupesResponse>(url);
  }

  queryNames(query: NameQuery): Observable<QueryNamesResponse> {
    const url = API_QUERY_NAMES;
    const options = {params: query.toParams()};
    return this.http.get<QueryNamesResponse>(url, options);
  }

  queryTags(query: TagQuery): Observable<QueryTagsResponse> {
    const url = API_QUERY_TAGS;
    const options = {params: query.toParams()};
    return this.http.get<QueryTagsResponse>(url, options);
  }
}
