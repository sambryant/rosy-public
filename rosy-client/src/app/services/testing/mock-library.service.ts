import {of, Observable, Subject, } from 'rxjs';

import {GroupQuery} from '@app/models';
import {ItemsResponse, LibraryService, } from '@app/services';


/**
 * Class used to mock the response of LibraryService.queryImages/LibraryService.queryGroups.
 *
 * This takes a list of `Item` objects that it will use to construct realistic responses from
 * `queryItems` that take into account `GroupQuery.size` and `GroupQuery.page`, but
 * no other query parameters.
 *
 * This can function in either `async` mode or not. When `async=true`, `queryItems` will return an
 * `Observable` that will not emit anything until `resolve` is called. Also, `queryItems` will
 * throw an Error if multiple calls to `queryItems` are made without being resolved first.
 *
 * When `async=false`, `queryItems` returns a synchronous promise (i.e. `of(...)`).
 *
 * Usage example:
 *
 *     mockLibrary = jasmine.createSpyObj('LibraryService', [
 *         'queryImages', 'getTagMap'
 *     ]);
 *     let mockItems = [new MockImage(1, '1.jpg'), ...];
 *     let mockQueryObject = new MockQueryItems<Image>(mockItems, true);
 *     mockLibrary.queryImages.and.callFake(mockQueryObject.queryImages);
 *
 */
export class MockQueryItems<Item, Query extends GroupQuery> {

  mockItems: Item[];
  responseSubject: Subject<ItemsResponse<Item>> | null = null;
  responseData: ItemsResponse<Item> | null = null;
  async: boolean;

  constructor(mockItems: Item[], async = true) {
    this.mockItems = mockItems;
    this.responseSubject = null;
    this.responseData = null;
    this.async = async;
  }

  setItems(mockItems: Item[]) {
    this.mockItems = mockItems;
  }

  resolve() {
    if (!this.async) {
      throw new Error('This mock object was not marked async so "resolve" is not available');
    }
    if (this.responseSubject == null) {
      throw new Error('No outstanding request to resolve!');
    } else {
      this.responseSubject.next(this.responseData as any);
      this.responseSubject.complete();
      this.responseSubject = null;
      this.responseData = null;
    }
  }

  resolveWithError(error: any) {
    if (!this.async) {
      throw new Error('This mock object was not marked async so "resolve" is not available');
    }
    if (this.responseSubject == null) {
      throw new Error('No outstanding request to resolve!');
    } else {
      this.responseSubject.error(error);
      this.responseSubject = null;
      this.responseData = null;
    }
  }

  hasPendingResponse(): boolean {
    if (!this.async) {
      throw new Error('This mock object was not marked async so it cannot have a pending response');
    }
    return this.responseSubject != null;
  }

  queryItems(query: Query): Observable<ItemsResponse<Item>> {
    if (this.async && this.hasPendingResponse()) {
      throw new Error('Multiple outstanding requests!');
    }
    const start = query.page * query.size;
    const stop = Math.min(this.mockItems.length, start + query.size);
    this.responseData = {
      count: this.mockItems.length,
      items: this.mockItems.slice(start, stop)
    };
    if (this.async) {
      this.responseSubject = new Subject<ItemsResponse<Item>>();
      return this.responseSubject;
    } else {
      return of(this.responseData);
    }
  }
}
