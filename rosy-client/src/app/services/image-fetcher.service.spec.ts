import {
  of,
  AsyncSubject,
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
} from 'rxjs';
import {delay, take, } from 'rxjs/operators';
import {fakeAsync, tick, TestBed, } from '@angular/core/testing';
import {DomSanitizer, SafeUrl, } from '@angular/platform-browser';
import {MockProvider} from 'ng-mocks';
import {TestScheduler} from 'rxjs/testing';

import {ImageFetcherService, LibraryService, LoggerService, } from '.';
import {Image, ImageId, } from '@app/models';
import {MockImage} from '@app/models/testing';


function subscribeAndExpect<T>(observable: Observable<T>, expected: T): Subscription {
  return observable.subscribe((result: T) => {
    expect(result).toEqual(expected);
  });
}


/**
 * Deterministic mock url for blob corresponding to an image.
 */
function mockUrlForId(id: ImageId): SafeUrl {
  return `fake/url/for/image/${id}`;
}

function mockUrlFor(image: Image): SafeUrl {
  return mockUrlForId(image.id);
}


class GetImageBlobUrlMocker {

  /**
   * Map from image id to subjects for observable.
   */
  private requests: Map<ImageId, Subject<SafeUrl>> = new Map();

  constructor() {}

  dropAll() {
    this.requests = new Map();
  }

  flush(image: Image, response: SafeUrl | null = null) {
    const req = this.requests.get(image.id);
    if (!req) {
      throw new Error('No request with id: ${image.id}');
    }
    req.next(response || mockUrlFor(image));
    this.requests.delete(image.id);
  }

  flushAll() {
    this.requests.forEach((sub: Subject<SafeUrl>, id: ImageId) => {
      sub.next(mockUrlForId(id));
    });
  }

  getImageBlobUrl(image: Image): Observable<SafeUrl> {
    const obs: Subject<SafeUrl> = new Subject();

    if (this.requests.has(image.id)) {
      throw new Error('Multiple outstanding requests with same id: ' + image.id);
    }
    this.requests.set(image.id, obs);
    return obs.pipe(take(1)); // mimics LibraryService behavior.
  }

}


describe('ImageFetcherService', () => {
  let service: ImageFetcherService;
  let getBlobSpy;
  let images: MockImage[];
  let libraryCtrl: GetImageBlobUrlMocker;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
      ],
      providers: [
        MockProvider(LoggerService),
        MockProvider(LibraryService)
      ]
    });
    service = TestBed.inject(ImageFetcherService);
    const library = TestBed.inject(LibraryService);
    getBlobSpy = spyOn(library, 'getImageBlobUrl');
  });

  beforeEach(() => {
    libraryCtrl = new GetImageBlobUrlMocker();
    getBlobSpy.and.callFake((image: Image) => {
      return libraryCtrl.getImageBlobUrl(image);
    });

    images = [
      new MockImage(1, 'image 1'),
      new MockImage(2, 'image 2'),
      new MockImage(3, 'image 3'),
      new MockImage(4, 'image 4'),
      new MockImage(5, 'image 5'),
      new MockImage(6, 'image 6'),
      new MockImage(7, 'image 7'),
      new MockImage(8, 'image 8'),
      new MockImage(9, 'image 9'),
      new MockImage(0, 'image 10'),
    ];
    service.setExpectedImages(images);
  });

  it('should fetch an image blob', fakeAsync(() => {
    const fakeImage = new MockImage(1, 'file 1.jpg');
    const fakeUrl = 'fake/url.com' as SafeUrl;
    getBlobSpy.and.returnValue(of(fakeUrl));

    service.setExpectedImages([fakeImage]);

    expect(getBlobSpy).not.toHaveBeenCalled();

    expect(subscribeAndExpect(
      service.getImage(fakeImage), 'fake/url.com' as SafeUrl
    ).closed).toBeTrue();

    expect(getBlobSpy).toHaveBeenCalledWith(fakeImage);
    expect(getBlobSpy).toHaveBeenCalledTimes(1);
  }));

  it('should use hot observables when requesting image blobs', () => {
    // Request image 1 but don't subscribe and expect there to be API calls anyway
    const image = images[0];
    const fakeUrl = 'fake/url.com' as SafeUrl;
    getBlobSpy.and.returnValue(of(fakeUrl));

    expect(getBlobSpy).not.toHaveBeenCalled();
    const obs = service.getImage(image); // dont subscribe
    expect(getBlobSpy).toHaveBeenCalledWith(image);
    getBlobSpy.calls.reset();

    // If we subscribe to original request it should resolve immediately without requests.
    const sub = subscribeAndExpect(obs, fakeUrl);
    expect(getBlobSpy).not.toHaveBeenCalledWith(image);
    expect(sub.closed).toBeTrue();
  });

  it('should not raise an error when requested image is not in given expectedImages', fakeAsync(() => {
    const image = new MockImage(123, 'high id');

    // Okay if image id is within given expected ids
    expect(() => { service.getImage(images[0]); }).not.toThrow();
    expect(() => { service.getImage(new MockImage(1, 'blah')); }).not.toThrow();
    expect(() => { service.getImage(image); }).not.toThrow();
  }));

  it('should raise an error when requested prefetch image is not in given expectedImages', fakeAsync(() => {
    const image = new MockImage(123, 'high id');
    // Okay if image id is within given expected ids
    expect(() => { service.prefetchFromImage(images[0]); }).not.toThrow();
    expect(() => { service.prefetchFromImage(new MockImage(1, 'blah')); }).not.toThrow();
    expect(() => { service.prefetchFromImage(image); }).toThrow();
  }));

  it('should prefetch with the given strategy', () => {
    service.setPrefetchStrategy([0, +1]); // indicates should always fetch next image in list

    expect(getBlobSpy).not.toHaveBeenCalled();

    service.prefetchFromImage(images[1]);

    expect(getBlobSpy.calls.allArgs()).toEqual([
      [images[1]],
      [images[2]]
    ]);
  });

  it('should not requery server for already prefetched images', () => {
    // We request prefetching from image 1, which triggers prefetching of image 2
    // We then resolve prefetch of image 2
    // Then we request image 2 and expect no API call to follow
    service.setPrefetchStrategy([0, +1]); // indicates should always fetch next image in list
    const fakeUrl = 'fake/url/for/image2' as SafeUrl;

    service.prefetchFromImage(images[1]);
    libraryCtrl.flush(images[2], fakeUrl); // resolves prefetch request for image 2

    expect(getBlobSpy).toHaveBeenCalledWith(images[2]);
    getBlobSpy.calls.reset();

    const sub1 = service.getImage(images[1]).subscribe();
    const sub2 = subscribeAndExpect(service.getImage(images[2]), fakeUrl);
    expect(getBlobSpy).not.toHaveBeenCalledWith(images[2]);

    expect(sub1.closed).toBeFalse();
    expect(sub2.closed).toBeTrue();
  });

  it('should not requery server when fetch is already is in progress', () => {
    service.setPrefetchStrategy([]); // no prefetching
    const image = images[1];
    const fakeUrl = 'fake/response/url' as SafeUrl;

    // Make two direct requests before resolve
    const subs = [
      subscribeAndExpect(service.getImage(image), fakeUrl),
      subscribeAndExpect(service.getImage(image), fakeUrl)
    ];
    subs.forEach((sub) => { expect(sub.closed).toBeFalse(); });

    libraryCtrl.flush(image, fakeUrl); // resolve api request

    // Make two direct requests after resolve
    subs.push(subscribeAndExpect(service.getImage(image), fakeUrl));
    subs.push(subscribeAndExpect(service.getImage(image), fakeUrl));

    // All of them should now be closed with same value
    subs.forEach((sub) => { expect(sub.closed).toBeTrue(); });

    // Underlying API should only have been hit once
    expect(getBlobSpy).toHaveBeenCalledTimes(1);
    expect(getBlobSpy).toHaveBeenCalledWith(image);
  });

  it('should prefetch in the order given by prefetch pattern', () => {
    const maxRequests = 3;
    const image = images[4];
    const prefetchPattern = [0, -3, -18, 1, -2, -1, 2, 18, 3, 4, 5, 6];
    const expectedApiCalls = [
      [4, 1, 5],
      [2, 3],
      [6, 7],
      [8],
      [9]
    ].map((indList) => (indList.map((ind) => ([images[ind]]))));
    const resolveEvents = [
      [1, 5],
      [4, 2],
      [6],
      [8],
      []
    ];
    service.setPrefetchStrategy(prefetchPattern);
    service.setMaxNumberConcurrentRequests(maxRequests);
    service.prefetchFromImage(image);

    for (let i = 0; i < expectedApiCalls.length; i++) {
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[i]);
      getBlobSpy.calls.reset();
      resolveEvents[i].forEach((ind) => {
        libraryCtrl.flush(images[ind]);
      });
    }
  });

  it('should start prefetching images until max concurrency reached', () => {
    const maxRequests = 4;
    const prefetchPattern = [+0, +1, +2, +3, +4, +5, +6];
    const prefetchImage = images[3];
    const expectedApiCalls = [
      [images[3]], [images[4]], [images[5]], [images[6]]
    ];

    service.setMaxNumberConcurrentRequests(maxRequests);
    service.setPrefetchStrategy(prefetchPattern);

    expect(getBlobSpy.calls.allArgs()).toEqual([]);
    service.prefetchFromImage(prefetchImage);
    expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls);
  });

  it('should handle case where the prefetch pattern is outside of image pool', () => {
    const maxRequests = 5;
    const prefetchImage = images[7]; // third to last
    const prefetchPattern =  [0, +1, +4, +2, +3, -1, -8, -2];
    const expectedImages = [7, 8, 9, 6, 5];
    const expectedApiCalls = expectedImages.map((ind) => ([images[ind]]));

    service.setMaxNumberConcurrentRequests(maxRequests);
    service.setPrefetchStrategy(prefetchPattern);

    expect(getBlobSpy.calls.allArgs()).toEqual([]);
    service.prefetchFromImage(prefetchImage);
    expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls);
  });

  it('should continue prefetch queue when outstanding request resolves', () => {
    // Have prefetch pattern constrained by number of concurrent requests
    // When any one of those requests resolves, expect the next pending prefetch to take its place
    const maxRequests = 3;
    const prefetchImage = images[0];
    const prefetchPattern = [0, 1, 2, 3, 4, 5, 6, 7, 8]; // all images
    const resolveEvents = [
      [1, 2], // resolve api requests for image 1 and image 2
      [0],
      [] // resolve api request for image 0
    ];
    const expectedApiCalls = [
      [0, 1, 2],
      [3, 4], // triggered after resolving 1, 2
      [5] // triggered after resolving 0
    ].map((indList) => (indList.map((ind) => ([images[ind]]))));

    service.setMaxNumberConcurrentRequests(maxRequests);
    service.setPrefetchStrategy(prefetchPattern);
    service.prefetchFromImage(prefetchImage);

    for (let i = 0; i < expectedApiCalls.length; i++) {
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[i]);

      getBlobSpy.calls.reset();
      resolveEvents[i].forEach((ind) => {
        libraryCtrl.flush(images[ind]);
      });
    }
  });

  it('should not prefetch already prefetched images', () => {
    // Get image which triggers prefetch of adjacent images and resolve all requests
    // Get an image outside of prefetch window that has some overlapping prefetch images
    // Expect the resulting prefetch load to skip the overlapped images
    const maxRequests = 5;
    const prefetchPattern = [0, 1, 2, 3, 7]; // strange pattern
    const prefetchImages = [0, 5].map((i) => (images[i]));
    const expectedApiCalls = [
      [0, 1, 2, 3, 7],
      [5, 6, 8] // 7 is skipped because included in first prefetch set
    ].map((indList) => (indList.map((ind) => ([images[ind]]))));

    service.setMaxNumberConcurrentRequests(maxRequests);
    service.setPrefetchStrategy(prefetchPattern);

    service.prefetchFromImage(prefetchImages[0]);
    expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[0]);

    libraryCtrl.flushAll();
    getBlobSpy.calls.reset();

    service.prefetchFromImage(prefetchImages[1]);
    expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[1]);
  });

  it('should immediately fetch requested image even if at maximum concurrent', () => {
    const prefetchPattern = [0, 1, 2, 3, 4, 5, 6];
    const maxRequests = 3;
    const prefetchImage = images[0]; // triggers fetch of [0, 1, 2],
    const requestedImage = images[5]; // triggers fetch of 5 even though at capacity

    service.setMaxNumberConcurrentRequests(maxRequests);
    service.setPrefetchStrategy(prefetchPattern);
    service.prefetchFromImage(prefetchImage);

    expect(getBlobSpy).not.toHaveBeenCalledWith(requestedImage);
    service.getImage(requestedImage);
    expect(getBlobSpy).toHaveBeenCalledWith(requestedImage);
  });

  it('should handle request for image which is also a pending pre-fetch', () => {
    const prefetchPattern = [0, 1, 2, 3, 4, 5, 6];
    const maxRequests = 3;
    const prefetchImage = images[0]; // triggers fetch of [0, 1, 2],
    const requestedImage = images[1];

    service.setMaxNumberConcurrentRequests(maxRequests);
    service.setPrefetchStrategy(prefetchPattern);
    service.prefetchFromImage(prefetchImage);
    getBlobSpy.calls.reset();

    const obs = service.getImage(requestedImage);
    expect(getBlobSpy).not.toHaveBeenCalledWith(requestedImage);

    libraryCtrl.flush(requestedImage, 'result' as SafeUrl);
    expect(subscribeAndExpect(obs, 'result' as SafeUrl).closed).toBeTrue();
  });

  it('should still cache response of a resolve from an outdated prefetch request', () => {
    // Request prefetching from image which triggers a prefetch set
    // Request prefetching from different image which triggers a different prefetch set
    // Resolve one of the prefetch requests from the original event
    // Check that subsequent requests use cached value from that resolve
    const prefetchPattern = [0, 1, 2];
    const maxRequests = 3;
    const prefetchImages = [
      images[0], // triggers fetch of [0, 1, 2],
      images[5], // triggers completely different window
      images[2], // expect immediate resolve because of prefetch
    ];
    service.setMaxNumberConcurrentRequests(maxRequests);
    service.setPrefetchStrategy(prefetchPattern);

    service.prefetchFromImage(prefetchImages[0]); // triggers: 0, 1, 2
    service.prefetchFromImage(prefetchImages[1]); // triggers: 5, 6, 7
    libraryCtrl.flush(images[2], 'test resolve' as SafeUrl); // resolves 2
    getBlobSpy.calls.reset();

    // now if we request images[2], it should use resolved value
    const sub = subscribeAndExpect(
      service.getImage(prefetchImages[2]), 'test resolve' as SafeUrl);
    expect(sub.closed).toBeTrue();
    expect(getBlobSpy).not.toHaveBeenCalledWith(prefetchImages[2]);
  });

  describe('Prefetch discard behavior A - cautious', () => {
    /*
    The following three tests describe one possible behavior for what happens when a new image is
    requested before prefetching finishes on the previous image.

    Behavior A - cautious

    When a new image is requested, the target fetch is performed immeidately even if at capacity.
    However, no additional prefetching is done until the number of requests falls below capacity.
    All unfinished active prefetching requests from previous events count towards the total number
    of requests. However, no pending prefetch requests from previous events are considered when
    room is available for additional requests.

    To use an analogy: to move on to the next course, you must finish what is currently on your
    plate, but you dont have to finish everything in the pot.

    This implementation is nice primarily because it ensures that resources are not excessively
    clogged and it is very easy to implement. It is actually the natural result of a system where
    the BlobCache is agnostic to the pending prefetch requests.
    */
    it('should fetch a requested image even when fetching is at capacity', () => {
      // Request an image which triggers prefetching at saturation
      // Request a different image
      // Expect fetch to begin for new image, but no other fetching to be issued
      const prefetchPattern = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]; // all images
      const maxRequests = 3;
      const prefetchImage = images[0];
      const requestedImage = images[4];
      const expectedApiCalls = [
        [0, 1, 2],
        [4]
      ].map((indList) => (indList.map((ind) => ([images[ind]]))));
      service.setMaxNumberConcurrentRequests(maxRequests);
      service.setPrefetchStrategy(prefetchPattern);

      service.prefetchFromImage(prefetchImage); // triggers: 0, 1, 2
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[0]);
      getBlobSpy.calls.reset();

      service.getImage(requestedImage); // only triggers: 4
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[1]);
      getBlobSpy.calls.reset();
    });

    it('should use new fetching queue when under capacity', () => {
      // Request prefetch image which triggers prefetching at saturation
      // Request a different prefetch image
      // When old or new prefetch request resolves, re-populate with new prefetch request
      const prefetchPattern = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]; // all images
      const maxRequests = 3;
      const prefetchImages = [
        images[0],
        images[4]
      ];
      const resolveEvents = [
        [images[1], images[2]], // only frees 1 position since over capacity
        [images[0], images[5]], // frees 2 positions
      ];
      const expectedApiCalls = [
        [0, 1, 2],
        [],
        [4, 5],
        [6, 7]
      ].map((indList) => (indList.map((ind) => ([images[ind]]))));
      service.setMaxNumberConcurrentRequests(maxRequests);
      service.setPrefetchStrategy(prefetchPattern);

      service.prefetchFromImage(prefetchImages[0]); // triggers: 0, 1, 2
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[0]);
      getBlobSpy.calls.reset();

      service.prefetchFromImage(prefetchImages[1]); // triggers nothing since at capacity
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[1]);
      getBlobSpy.calls.reset();

      resolveEvents[0].forEach((img) => { libraryCtrl.flush(img); });
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[2]);
      getBlobSpy.calls.reset();

      resolveEvents[1].forEach((img) => { libraryCtrl.flush(img); });
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[3]);
      getBlobSpy.calls.reset();
    });
  });

  xdescribe('Prefetch discard behavior B - greedy', () => {
    /*
    The following three tests describe one possible behavior for what happens when a new image is
    requested before prefetching finishes on the previous image.

    Behavior B - greedy

    When a new image is requested, any oustanding prefetching is ignored with respect to counting
    the number of active requests. This means that prefetching for the new image automatically
    begins at full capacity even though the number of active requests may already be saturated.

    There are two considerations that make this implementation more difficult:

    1. Since we dont count existing prefetch requests towards the total number of active requests,
       we cannot decrement this number when any of those existing requests finish. Otherwise, by
       rapidly requesting different images, we would effectively be raising the concurrent request
       limit.
    2. When an old existing request resolves, we still want to cache its result. Otherwise,
       rapidly browsing back and forth through a list of images would trigger many costly
       identical image requests which are ignored.

    These two constraints are difficult to fulfill simultaneously. This implementation also has
    the drawback of potentially overloading the server.
    */

    it('should not count old prefetch requests towards capacity when new image is requested', () => {
      // Request an image which triggers prefetching at saturation
      // Request a different image
      // Expect prefetching to begin for new image even though we are at maximum capacity)
      const prefetchPattern = [1, 2, 3, 4, 5, 6, 7, 8, 9]; // all images
      const maxRequests = 3;
      const prefetchImages = [
        images[0],
        images[4]
      ];
      const expectedApiCalls = [
        [0, 1, 2],
        [4, 5, 6],
        [7]
      ].map((indList) => (indList.map((ind) => ([images[ind]]))));
      service.setMaxNumberConcurrentRequests(maxRequests);
      service.setPrefetchStrategy(prefetchPattern);

      service.prefetchFromImage(prefetchImages[0]); // triggers: 0, 1, 2
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[0]);
      getBlobSpy.calls.reset();

      service.prefetchFromImage(prefetchImages[1]); // triggers: 5, 6, 7
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[1]);
      getBlobSpy.calls.reset();

      libraryCtrl.flush(images[5]);
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[2]);
    });

    it('should not subtract old prefetch requests from number of active requests when they resolve', () => {
      // Request an image which triggers prefetching at saturation
      // Request a different image which triggers a different saturated prefetch
      // Resolve a prefetch request from original set, expect no new requests to be issued
      const prefetchPattern = [1, 2];
      const maxRequests = 3;
      const prefetchImages = [
        images[0], // triggers fetch of [0, 1, 2],
        images[5], // triggers completely different window
      ];
      const expectedApiCalls = [
        [0, 1, 2],
        [4, 5, 6],
        []
      ].map((indList) => (indList.map((ind) => ([images[ind]]))));
      service.setMaxNumberConcurrentRequests(maxRequests);
      service.setPrefetchStrategy(prefetchPattern);

      service.prefetchFromImage(prefetchImages[0]); // triggers: 0, 1, 2
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[0]);
      getBlobSpy.calls.reset();

      service.prefetchFromImage(prefetchImages[1]); // triggers: 5, 6, 7
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[1]);
      getBlobSpy.calls.reset();

      libraryCtrl.flush(images[2]);
      expect(getBlobSpy.calls.allArgs()).toEqual(expectedApiCalls[2]);
    });
  });

  describe('setExpectedImages', () => {

    it('should clear the pending prefetch queue', () => {
      service.setPrefetchStrategy([1, 2, 3, 4, 5, 6, 7, 8, 9]);
      service.setMaxNumberConcurrentRequests(3);

      service.getImage(images[0]);
      getBlobSpy.calls.reset();

      service.setExpectedImages(images);
      libraryCtrl.flushAll(); // normally triggers additional prefetching since below capcity.

      expect(getBlobSpy).not.toHaveBeenCalled();
    });

    it('should preserve cached image blobs', () => {
      // Do we really want this behavior??? - we should clean it out *eventually*
      const image = images[0];

      // Request image blob and resolve triggering caching
      service.setExpectedImages(images);
      service.getImage(image);
      libraryCtrl.flush(image, '1st value' as SafeUrl);
      libraryCtrl.dropAll();
      getBlobSpy.calls.reset();

      // Request same image blob after "changing" expected image list, expect no api call
      service.setExpectedImages(images);
      const sub = subscribeAndExpect(service.getImage(image), '1st value' as SafeUrl);
      expect(getBlobSpy).not.toHaveBeenCalledWith(image);
      expect(sub.closed).toBeTrue();
    });

    it('should not preserve outstanding api requests', () => {
      const image = images[0];

      // Request image blob, dont resolve request
      service.setExpectedImages(images);
      service.getImage(image);
      expect(getBlobSpy).toHaveBeenCalledWith(image);

      libraryCtrl.dropAll(); // prevents resolve in either direction;
      getBlobSpy.calls.reset();

      // Request same image blob after "changing" expected image list, expect an api call
      service.setExpectedImages(images);
      service.getImage(image);
      expect(getBlobSpy).toHaveBeenCalledWith(image);
    });

    it('should ignore when old outstanding requests resolve', () => {
      const image = images[0];

      // Request image blob, dont resolve request
      service.setExpectedImages(images);
      service.getImage(image);
      expect(getBlobSpy).toHaveBeenCalledWith(image);

      getBlobSpy.calls.reset();

      // Request same image blob after "changing" expected image list, expect an api call
      service.setExpectedImages(images);
      libraryCtrl.flush(image);
      libraryCtrl.dropAll(); // to clear other prefetch requests.
      service.getImage(image);
      expect(getBlobSpy).toHaveBeenCalledWith(image);
    });
  });

});
