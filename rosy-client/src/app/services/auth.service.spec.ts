import {HttpParams} from '@angular/common/http';
import {MockProvider} from 'ng-mocks';
import {Subject} from 'rxjs';
import {TestBed} from '@angular/core/testing';

import {mockLocalStorage} from '@app/testing';
import {ApiService, AuthService, ConfigService, LoginResponse, } from '.';


/**
 * Creates realistic mock version of token response from server.
 * exp, iat, nbf are all timestamps in seconds since epoch in local time zone
 * identity is id of user in database (i think).
 */
function createMockAuthToken(exp: number) {
  const headers = btoa(JSON.stringify({typ: 'JWT', alg: 'HS256'}));
  const payload = btoa(JSON.stringify({exp}));
  const signature = btoa('1234');
  return `${headers}.${payload}.${signature}`;
}


describe('AuthService', () => {
  let postLoginSpy: jasmine.Spy;
  let fakeResponse: Subject<LoginResponse>;
  let service: AuthService;
  let mockDateToday: Date;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        MockProvider(ApiService)
      ],
    });
    service = TestBed.inject(AuthService);
    const apiService = TestBed.inject(ApiService);
    fakeResponse = new Subject();
    postLoginSpy = spyOn(apiService, 'postLogin').and.returnValue(fakeResponse);
  });

  beforeEach(() => {
    mockDateToday = new Date('2020-04-01');
    jasmine.clock().install();
    jasmine.clock().mockDate(mockDateToday);
    mockLocalStorage({});
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  describe('login', () => {

    it('should send correct login request', () => {
      const fakeUsername = 'user';
      const fakePassword = 'hog';
      service.login(fakeUsername, fakePassword).subscribe();
      expect(postLoginSpy).toHaveBeenCalledWith(fakeUsername, fakePassword);
    });

    it('should set database schema if provided', () => {
      const dbSpy = spyOn(TestBed.inject(ConfigService), 'setDatabaseSchema');
      const sub = service.login('user', 'password', 'schema').subscribe();

      expect(dbSpy).toHaveBeenCalledBefore(postLoginSpy);
      expect(dbSpy).toHaveBeenCalledWith('schema');
    });

    it('should not repeat hits to server', () => {
      const sub = service.login('user', 'password');

      sub.subscribe();
      sub.subscribe();
      sub.subscribe();
      expect(postLoginSpy).toHaveBeenCalledTimes(1);
    });

    it('should never resolve first login request if it makes a second one', () => {
      const login1 = new Subject();
      const login2 = new Subject();

      postLoginSpy.and.returnValue(login1);
      const sub1 = service.login('user', 'password').subscribe(() => {
        throw new Error('this should not happen');
      });

      postLoginSpy.and.returnValue(login2);
      const sub2 = service.login('user', 'password').subscribe();

      login1.next(null);
      expect(sub1.closed).toBeFalse();
      expect(sub2.closed).toBeFalse();

      login2.next(null);
      expect(sub1.closed).toBeFalse();
      expect(sub2.closed).toBeTrue();
    });

    it('should set token and expires_at to local storage', () => {
      const fakeExpireTimestamp = 1585785600; // (UTC) 2020/4/2 0:0:0
      const expectedExpiresAt = (fakeExpireTimestamp * 1000).toString();
      const mockToken = createMockAuthToken(fakeExpireTimestamp);

      expect(localStorage.getItem('id_token')).toBeFalsy();
      expect(localStorage.getItem('expires_at')).toBeFalsy();

      const sub = service.login('user', 'password').subscribe();

      expect(localStorage.getItem('id_token')).toBeFalsy();
      expect(localStorage.getItem('expires_at')).toBeFalsy();
      expect(sub.closed).toBeFalse();

      fakeResponse.next({access_token: mockToken});

      expect(localStorage.getItem('id_token')).toEqual(mockToken);
      expect(localStorage.getItem('expires_at')).toEqual(expectedExpiresAt);
      expect(sub.closed).toBeTrue();
    });

    it('should notify login$ observers true', () => {
      const fakeExpireTimestamp = 1585785600; // (UTC) 2020/4/2 0:0:0
      const mockToken = createMockAuthToken(fakeExpireTimestamp);

      expect(service.login$.getValue()).toBeFalse();
      service.login('user', 'password').subscribe();
      fakeResponse.next({access_token: mockToken});
      expect(service.login$.getValue()).toBeTrue();
    });

  });

  describe('checkLoggedIn', () => {

    function loginWithExpireTime(expire: Date) {
      const expireTimestamp = expire.getTime().toString();
      const fakeToken = 'fake token';
      localStorage.setItem('id_token', fakeToken);
      localStorage.setItem('expires_at', expireTimestamp);
    }

    it('should return false if no token is set', () => {
      expect(service.checkLoggedIn()).toBeFalse();
    });

    it('should return false if token is expired', () => {
      const mockExpDate = new Date('2020-04-01T01:00:00Z');
      const mockNowDate = new Date('2020-04-01T02:00:00Z');
      loginWithExpireTime(mockExpDate);
      jasmine.clock().mockDate(mockNowDate);
      expect(service.checkLoggedIn()).toBeFalse();
    });

    it('should return true if token is not expired', () => {
      const mockExpDate = new Date('2020-04-01T02:00:00Z');
      const mockNowDate = new Date('2020-04-01T01:00:00Z');
      loginWithExpireTime(mockExpDate);
      jasmine.clock().mockDate(mockNowDate);
      expect(service.checkLoggedIn()).toBeTrue();
    });

  });

  describe('logout', () => {

    function loginWithExpireTime(expire: Date) {
      const expireTimestamp = expire.getTime().toString();
      const fakeToken = 'fake token';
      localStorage.setItem('id_token', fakeToken);
      localStorage.setItem('expires_at', expireTimestamp);
    }

    it('should log a user out', () => {
      const mockExpDate = new Date('2020-04-01T02:00:00Z');
      const mockNowDate = new Date('2020-04-01T01:00:00Z');
      loginWithExpireTime(mockExpDate);
      jasmine.clock().mockDate(mockNowDate);

      expect(service.checkLoggedIn()).toBeTrue();
      service.logout();
      expect(service.checkLoggedIn()).toBeFalse();
    });

    it('should notify observers', () => {
      const mockExpDate = new Date('2020-04-01T02:00:00Z');
      const mockNowDate = new Date('2020-04-01T01:00:00Z');
      loginWithExpireTime(mockExpDate);
      jasmine.clock().mockDate(mockNowDate);

      expect(service.login$.getValue()).toBeFalse();
      service.checkLoggedIn();
      expect(service.login$.getValue()).toBeTrue();
      service.logout();
      expect(service.login$.getValue()).toBeFalse();
    });

  });
});
