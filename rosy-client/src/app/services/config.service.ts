import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject, } from 'rxjs';

export type ViewMode = 'selector' | 'image' | 'split';


@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  public showImageFeatures$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  mobileMode$: BehaviorSubject<boolean>;

  viewMode$: BehaviorSubject<ViewMode>;

  databaseSchema$: Subject<string | null>;

  databaseSchema: string | null;

  disableImageInfoOverlay = false;

  navigateOnRate = true;

  saveAfterChange = true;

  saveOnNavigate = true;

  searchPrefixOnly = true;

  get mobileMode() {
    return this.mobileMode$.value;
  }

  set mobileMode(value) {
    this.mobileMode$.next(value);
  }

  get viewMode() {
    return this.viewMode$.value;
  }

  set viewMode(value) {
    this.viewMode$.next(value);
  }

  constructor() {
    const databaseSchema = localStorage.getItem('databaseSchema') || null;
    this.databaseSchema = databaseSchema;
    this.databaseSchema$ = new Subject();
    this.databaseSchema$.next(databaseSchema);
    this.mobileMode$ = new BehaviorSubject(false);
    this.viewMode$ = new BehaviorSubject('split');

    this.mobileMode$.subscribe((mobile) => {
      this.navigateOnRate = !mobile;
      if (mobile && this.viewMode$.value === 'split') {
        this.viewMode$.next('selector');
      }
    });

  }

  setDatabaseSchema(databaseSchema: string): void {
    localStorage.setItem('databaseSchema', databaseSchema);
    this.databaseSchema = databaseSchema;
    this.databaseSchema$.next(databaseSchema);
  }

}
