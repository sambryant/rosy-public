import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private snackBar: MatSnackBar) {}

  info(message: string) {
    this.snackBar.open(message, undefined, {
      duration: 3000,
      panelClass: 'rx-notification-info',
      verticalPosition: 'top',
    });
  }

  error(message: string) {
    this.snackBar.open('Error: ' + message, undefined, {
      duration: 3000,
      panelClass: 'rx-notification-error',
      verticalPosition: 'top',
    });
  }

}
