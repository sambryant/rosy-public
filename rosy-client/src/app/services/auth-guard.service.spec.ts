import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, } from '@angular/router';
import {TestBed} from '@angular/core/testing';

import {APP_PAGES} from '@app/app-page.model';
import {AuthGuardService, AuthService, } from '.';


describe('AuthGuardService', () => {
  let service: AuthGuardService;
  let mockAuthService;
  let mockRouter;

  beforeEach(() => {
    mockRouter = jasmine.createSpyObj('Router', ['navigate']);
    mockAuthService = jasmine.createSpyObj('AuthService', ['checkLoggedIn']);
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {provide: AuthService, useValue: mockAuthService},
        {provide: Router, useValue: mockRouter}
      ],
    });
    service = TestBed.inject(AuthGuardService);
  });

  it('should block routing when not logged in', () => {
    mockAuthService.checkLoggedIn.and.returnValue(false);
    expect(service.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot)).toBeFalse();
    expect(mockAuthService.checkLoggedIn).toHaveBeenCalledTimes(1);
  });

  it('should redirect to login when not logged in', () => {
    mockAuthService.checkLoggedIn.and.returnValue(false);
    service.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot);
    expect(mockRouter.navigate).toHaveBeenCalledTimes(1);
  });

  it('should not block routing when logged in', () => {
    mockAuthService.checkLoggedIn.and.returnValue(true);
    expect(service.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot)).toBeTrue();
    expect(mockAuthService.checkLoggedIn).toHaveBeenCalledTimes(1);
    expect(mockRouter.navigate).not.toHaveBeenCalled();
  });

  it('should set redirect URL to query params when not logged in', () => {
    mockAuthService.checkLoggedIn.and.returnValue(false);
    const fakeUrl = '/library?query=thing&other=other';
    const fakeRouterSnapshot = {url: fakeUrl} as RouterStateSnapshot;
    const expectedOptions = {
      queryParams: {returnUrl: encodeURI(fakeUrl)}
    };
    service.canActivate({} as ActivatedRouteSnapshot, fakeRouterSnapshot);
    expect(mockRouter.navigate).toHaveBeenCalledTimes(1);
    expect(mockRouter.navigate).toHaveBeenCalledWith([APP_PAGES.login.link], expectedOptions);
  });
});
