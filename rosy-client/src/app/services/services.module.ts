import {CommonModule} from '@angular/common';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NgModule} from '@angular/core';

@NgModule({
  declarations: [],
  exports: [],
  imports: [
    CommonModule,
    MatSnackBarModule,
  ]
})
export class ServicesModule { }
