import {map, take, tap, shareReplay, } from 'rxjs/operators';
import {of, BehaviorSubject, Observable, Subject, Subscription, } from 'rxjs';
import {HttpClient, HttpHeaders, } from '@angular/common/http';
import {Injectable} from '@angular/core';

import {ApiService, LoginResponse, } from './api.service';
import {ConfigService} from './config.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public login$: BehaviorSubject<boolean>;

  private loginSub: Subscription | null = null;

  constructor(
    protected apiService: ApiService,
    protected configService: ConfigService,
  ) {
    this.login$ = new BehaviorSubject(this.isLoggedIn());
  }

  login(username: string, password: string, schema: string | null = null): Observable<null> {
    if (this.login$.value) {
      throw new Error('Already logged in');
    }
    if (schema) {
      this.configService.setDatabaseSchema(schema);
    }
    if (this.loginSub) {
      this.loginSub.unsubscribe();
    }
    const returned = new Subject<null>();
    this.loginSub = this.apiService.postLogin(username, password).pipe(
      take(1),
      tap((response: LoginResponse) => { this.setSession(response); }),
      map(_ => null),
      shareReplay(1)
    ).subscribe(returned);
    return returned.asObservable();
  }

  logout(): Observable<null> {
    if (this.loginSub) {
      this.loginSub.unsubscribe();
    }
    // TODO: Clear all schema associated values as well
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    this.checkLoggedIn(); // update logged in observers
    return of(null);
  }

  checkLoggedIn(): boolean {
    const loggedIn = this.isLoggedIn();
    if (loggedIn !== this.login$.getValue()) {
      this.login$.next(loggedIn);
    }
    return loggedIn;
  }

  private isLoggedIn(): boolean {
    return Date.now() <= this.getExpiration();
  }

  private setSession(loginResult: LoginResponse) {
    const expireData = JSON.parse(atob(loginResult.access_token.split('.')[1]));
    const expiresAt = expireData.exp * 1000; // in ms since UTC epoch
    localStorage.setItem('id_token', loginResult.access_token);
    localStorage.setItem('expires_at', expiresAt.toString());
    this.checkLoggedIn(); // update logged in observers
  }

  private getExpiration(): number {
    return parseInt(localStorage.getItem('expires_at') || '0', 10); // ms time stamp
  }
}
