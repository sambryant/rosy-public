import {MockProvider} from 'ng-mocks';
import {TestBed} from '@angular/core/testing';

import {ApiService, LoggerService, ServicesModule, ThreadApiService, } from '.';


describe('ThreadApiService', () => {
  let service: ThreadApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        MockProvider(ApiService),
        MockProvider(LoggerService)
      ]
    });
    service = TestBed.inject(ThreadApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have more unit tests', () => {});

});
