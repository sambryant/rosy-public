import {
  of,
  throwError,
  AsyncSubject,
  BehaviorSubject,
  Subject,
} from 'rxjs';
import {fakeAsync, TestBed, tick, } from '@angular/core/testing';
import {take} from 'rxjs/operators';
import {MockProvider} from 'ng-mocks';
import {SafeUrl} from '@angular/platform-browser';

import {
  ApiService,
  ConfigService,
  CreateArtistResponse,
  CreateGroupResponse,
  CreateNameResponse,
  CreateTagResponse,
  EmptyResponse,
  GetMetadataResponse,
  LibraryService,
  LoggerService,
  NotificationService,
  QueryImagesResponse,
  RateImageResponse,
  UpdateArtistResponse,
  UpdateGroupResponse,
  UpdateImageResponse,
  UpdateNameResponse,
  UpdateTagResponse,
} from '.';
import {
  Artist,
  ArtistQuery,
  ContentType,
  ContentValue,
  CONTENT_TYPES,
  DeletedQuery,
  DupeCandidatesQuery,
  DupeType,
  EigendataQuery,
  GroupSimple,
  GroupQuery,
  Image,
  ImageId,
  ImageQuery,
  Name,
  NameQuery,
  Tag,
  TagQuery,
} from '@app/models';
import {ExpectObserver} from '../testing/expect-observer';
import {MockArtist, MockGroup, MockImage, MockName, MockTag, } from '@app/models/testing';


describe('LibraryService', () => {
  let apiService: ApiService;
  let configService: ConfigService;
  let noteService: NotificationService;
  let service: LibraryService;

  let getMetadataSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MockProvider(ApiService),
        MockProvider(LoggerService),
        MockProvider(NotificationService),
      ]
    });
    service = TestBed.inject(LibraryService);
    apiService = TestBed.inject(ApiService);
    configService = TestBed.inject(ConfigService);
    noteService = TestBed.inject(NotificationService);
    getMetadataSpy = spyOn(apiService, 'getMetadata');
  });

  function mockMetadataResponse(objs: {artist?: Artist[], group?: GroupSimple[], name?: Name[], tag?: Tag[]}): GetMetadataResponse {
    const resp = {} as GetMetadataResponse;
    function buildMap(list: ContentValue[]) {
      const map = {};
      list.forEach((o, i) => {
        map[o.id] = i;
      });
      return map;
    }
    return {
      artist: [objs.artist || [], buildMap(objs.artist || [])],
      group: [objs.group || [], buildMap(objs.group || [])],
      name: [objs.name || [], buildMap(objs.name || [])],
      tag: [objs.tag || [], buildMap(objs.tag || [])],
    };
  }

  describe('query methods', () => {

    it('should query artists', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new ArtistQuery();
      spyOn(apiService, 'queryArtists').and.returnValue(of(fakeResponse));

      expect(service.queryArtists(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryArtists).toHaveBeenCalledOnceWith(fakeQuery);
    });

    it('should query deleted', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new DeletedQuery();
      spyOn(apiService, 'queryDeleted').and.returnValue(of(fakeResponse));

      expect(service.queryDeleted(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryDeleted).toHaveBeenCalledOnceWith(fakeQuery);
    });

    it('should query dupe candidates', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new DupeCandidatesQuery();
      spyOn(apiService, 'queryDupeCandidates').and.returnValue(of(fakeResponse));

      expect(service.queryDupeCandidates(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryDupeCandidates).toHaveBeenCalledOnceWith(fakeQuery);
    });

    it('should query eigendata', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new EigendataQuery();
      spyOn(apiService, 'queryEigendata').and.returnValue(of(fakeResponse));

      expect(service.queryEigendata(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryEigendata).toHaveBeenCalledOnceWith(fakeQuery);
    });

    it('should query groups', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new GroupQuery();
      spyOn(apiService, 'queryGroups').and.returnValue(of(fakeResponse));

      expect(service.queryGroups(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryGroups).toHaveBeenCalledOnceWith(fakeQuery);
    });

    it('should query images', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new ImageQuery();
      spyOn(apiService, 'queryImages').and.returnValue(of(fakeResponse));

      expect(service.queryImages(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryImages).toHaveBeenCalledOnceWith(fakeQuery);
    });

    it('should query names', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new NameQuery();
      spyOn(apiService, 'queryNames').and.returnValue(of(fakeResponse));

      expect(service.queryNames(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryNames).toHaveBeenCalledOnceWith(fakeQuery);
    });

    it('should query tags', () => {
      const fakeResponse = {
        count: 10,
        items: []
      };
      const fakeQuery = new TagQuery();
      spyOn(apiService, 'queryTags').and.returnValue(of(fakeResponse));

      expect(service.queryTags(fakeQuery).subscribe((response) => {
        expect(response).toEqual(fakeResponse);
      }).closed).toBeTrue();
      expect(apiService.queryTags).toHaveBeenCalledOnceWith(fakeQuery);
    });

  });

  describe('create methods', () => {
    let errorNoteSpy: jasmine.Spy;

    beforeEach(() => {
      errorNoteSpy = spyOn(noteService, 'error');
    });

    describe('createArtist', () => {
      let apiCreateSpy: jasmine.Spy;

      beforeEach(() => {
        apiCreateSpy = spyOn(apiService, 'createArtist');
      });

      it('should call correct api service method', () => {
        apiCreateSpy.and.returnValue(new Subject());

        expect(apiCreateSpy).not.toHaveBeenCalled();
        service.createContent('artist', 'new artist', []);
        expect(apiCreateSpy).toHaveBeenCalledWith('new artist', []);
      });

      it('should return the artist as an observable', () => {
        const mockArtist = new MockArtist(1, 'new artist');
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));

        const sub = service.createContent('artist', 'new artist', []).subscribe(artist => {
          expect(artist).toEqual(mockArtist);
        });
        expect(sub.closed).toBeFalse();
        mockSubject.next({item: mockArtist});
        expect(sub.closed).toBeTrue();
      });

      it('should mark as saving during creation', () => {
        const mockArtist = new MockArtist(39, 'new artist');
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(service.requestInProgress$.value).toBeFalse();
        const sub = service.createContent('artist', 'new artist', []).subscribe();
        expect(service.requestInProgress$.value).toBeTrue();
        mockSubject.next({item: mockArtist});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should set the image artist ids on success', () => {
        const mockArtist = MockArtist.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));

        const sub = service.createContent('artist', 'new artist', mockImages).subscribe();
        expect(mockImages.map(img => img.artistId)).toEqual([null, null]);
        mockSubject.next({item: mockArtist});
        expect(mockImages.map(img => img.artistId)).toEqual([39, 39]);
      });

      it('should not set the image artist ids on failure', () => {
        const mockArtist = MockArtist.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));

        const sub = service.createContent('artist', 'new artist', mockImages).subscribe({error: () => {}});
        expect(mockImages.map(img => img.artistId)).toEqual([null, null]);
        mockSubject.error({});
        expect(mockImages.map(img => img.artistId)).toEqual([null, null]);
      });

      it('should fire artistUpdated with the new artists id', () => {
        const mockArtist = new MockArtist(39, 'new artist');
        const mockImages = [MockImage.new(1), MockImage.new(2)];
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));

        const sub1 = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2]);
        });
        const sub2 = service.contentUpdated$.artist.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([39]);
        });
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeFalse();
        service.createContent('artist', 'new artist', mockImages).subscribe();
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeFalse();
        mockSubject.next({item: mockArtist});
        expect(sub1.closed).toBeTrue();
        expect(sub2.closed).toBeTrue();
      });

      it('should trigger a notification on error', () => {
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));
        const errorMessage = 'Could not create!';

        service.createContent('artist', 'new artist', []).subscribe({error: () => {}});
        expect(errorNoteSpy).not.toHaveBeenCalled();
        mockSubject.error({
          error: {
            message: errorMessage
          }
        });
        expect(errorNoteSpy).toHaveBeenCalledWith(errorMessage);
      });

      it('should only fire image updated on error but not artist updated', () => {
        const mockArtist = new MockArtist(39, 'new artist');
        const mockImages = [MockImage.new(1), MockImage.new(2)];
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));

        const sub1 = service.contentUpdated$.artist.pipe(take(1)).subscribe();
        const sub2 = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2]);
        });
        service.createContent('artist', 'new artist', mockImages).subscribe({error: () => {}});
        mockSubject.error({});
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeTrue();
      });

      it('should return error observable on error', () => {
        const mockArtist = new MockArtist(39, 'new artist');
        const mockSubject = new Subject<CreateArtistResponse>();
        apiCreateSpy.and.returnValue(mockSubject.pipe(take(1)));

        let errorHit = false;
        const sub = service.createContent('artist', 'new artist', []).subscribe({
          error: () => { errorHit = true; }
        });
        mockSubject.error({});
        expect(sub.closed).toBeTrue();
        expect(errorHit).toBeTrue();
      });

    });

    describe('create group', () => {
      let apiCreateSpy: jasmine.Spy;
      let mockResponse: Subject<CreateGroupResponse>;

      beforeEach(() => {
        apiCreateSpy = spyOn(apiService, 'createGroup');
        mockResponse = new Subject<CreateGroupResponse>();
        apiCreateSpy.and.returnValue(mockResponse.pipe(take(1)));
      });

      it('should call correct api service method', () => {
        apiCreateSpy.and.returnValue(new Subject());

        expect(apiCreateSpy).not.toHaveBeenCalled();
        service.createContent('group', 'new group', []);
        expect(apiCreateSpy).toHaveBeenCalledWith('new group', []);
      });

      it('should return the group as an observable', () => {
        const mockGroup = new MockGroup(1, 'new group');

        const sub = service.createContent('group', 'new group', []).subscribe(group => {
          expect(group).toEqual(mockGroup);
        });
        expect(sub.closed).toBeFalse();
        mockResponse.next({item: mockGroup});
        expect(sub.closed).toBeTrue();
      });

      it('should mark as saving during creation', () => {
        const mockGroup = new MockGroup(39, 'new group');

        expect(service.requestInProgress$.value).toBeFalse();
        const sub = service.createContent('group', 'new group', []).subscribe();
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponse.next({item: mockGroup});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should set the image group ids on success', () => {
        const mockGroup = MockGroup.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub = service.createContent('group', 'new group', mockImages).subscribe();
        expect(mockImages.map(img => img.groupId)).toEqual([null, null]);
        mockResponse.next({item: mockGroup});
        expect(mockImages.map(img => img.groupId)).toEqual([39, 39]);
      });

      it('should not set the image group ids on failure', () => {
        const mockGroup = MockGroup.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub = service.createContent('group', 'new group', mockImages).subscribe({error: () => {}});
        expect(mockImages.map(img => img.groupId)).toEqual([null, null]);
        mockResponse.error({});
        expect(mockImages.map(img => img.groupId)).toEqual([null, null]);
      });

      it('should fire groupUpdated with the new groups id', () => {
        const mockGroup = new MockGroup(39, 'new group');
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub1 = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2]);
        });
        const sub2 = service.contentUpdated$.group.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([39]);
        });
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeFalse();
        service.createContent('group', 'new group', mockImages).subscribe();
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeFalse();
        mockResponse.next({item: mockGroup});
        expect(sub1.closed).toBeTrue();
        expect(sub2.closed).toBeTrue();
      });

      it('should trigger a notification on error', () => {
        const errorMessage = 'Could not create!';

        service.createContent('group', 'new group', []).subscribe({error: () => {}});
        expect(errorNoteSpy).not.toHaveBeenCalled();
        mockResponse.error({
          error: {
            message: errorMessage
          }
        });
        expect(errorNoteSpy).toHaveBeenCalledWith(errorMessage);
      });

      it('should fire groupImagesUpdated updated', () => {
        const mockGroup = new MockGroup(39, 'new group');
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub = service.groupImagesUpdated$.pipe(take(1)).subscribe();
        service.createContent('group', 'new group', mockImages).subscribe();
        expect(sub.closed).toBeFalse();
        mockResponse.next({item: mockGroup});
        expect(sub.closed).toBeTrue();
      });

      it('should fire groupImagesUpdated$ and imageUpdated$ on error but not contentUpdated$.group', () => {
        const mockGroup = new MockGroup(39, 'new group');
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub1 = service.contentUpdated$.group.pipe(take(1)).subscribe();
        const sub2 = service.groupImagesUpdated$.pipe(take(1)).subscribe();
        const sub3 = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2]);
        });
        service.createContent('group', 'new group', mockImages).subscribe({error: () => {}});
        mockResponse.error({});
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeTrue();
        expect(sub3.closed).toBeTrue();
      });

      it('should return error observable on error', () => {
        const mockGroup = new MockGroup(39, 'new group');

        let errorHit = false;
        const sub = service.createContent('group', 'new group', []).subscribe({
          error: () => { errorHit = true; }
        });
        mockResponse.error({});
        expect(sub.closed).toBeTrue();
        expect(errorHit).toBeTrue();
      });
    });

    describe('create name', () => {
      let apiCreateSpy: jasmine.Spy;
      let mockResponse: Subject<CreateNameResponse>;

      beforeEach(() => {
        apiCreateSpy = spyOn(apiService, 'createName');
        mockResponse = new Subject<CreateNameResponse>();
        apiCreateSpy.and.returnValue(mockResponse.pipe(take(1)));
      });

      it('should call correct api service method', () => {
        apiCreateSpy.and.returnValue(new Subject());

        expect(apiCreateSpy).not.toHaveBeenCalled();
        service.createContent('name', 'new name', []);
        expect(apiCreateSpy).toHaveBeenCalledWith('new name', []);
      });

      it('should return the name as an observable', () => {
        const mockName = new MockName(1, 'new name');
        const sub = service.createContent('name', 'new name', []).subscribe(name => {
          expect(name).toEqual(mockName);
        });
        expect(sub.closed).toBeFalse();
        mockResponse.next({item: mockName});
        expect(sub.closed).toBeTrue();
      });

      it('should mark as saving during creation', () => {
        const mockName = new MockName(39, 'new name');

        expect(service.requestInProgress$.value).toBeFalse();
        const sub = service.createContent('name', 'new name', []).subscribe();
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponse.next({item: mockName});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should set the image names on success', () => {
        const mockName = MockName.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub1 = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2]);
        });
        const sub2 = service.createContent('name', 'new name', mockImages).subscribe();
        expect(mockImages.map(img => img.names)).toEqual([[], []]);
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeFalse();
        mockResponse.next({item: mockName});
        expect(mockImages.map(img => img.names)).toEqual([[mockName], [mockName]]);
        expect(sub1.closed).toBeTrue();
        expect(sub2.closed).toBeTrue();
      });

      it('should not set the image name ids on failure', () => {
        const mockName = MockName.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub = service.createContent('name', 'new name', mockImages)
          .subscribe({error: () => {}});
        expect(mockImages.map(img => img.names)).toEqual([[], []]);
        mockResponse.error({});
        expect(mockImages.map(img => img.names)).toEqual([[], []]);
      });

      it('should trigger a notification on error', () => {
        const errorMessage = 'Could not create!';

        service.createContent('name', 'new name', []).subscribe({error: () => {}});
        expect(errorNoteSpy).not.toHaveBeenCalled();
        mockResponse.error({
          error: {
            message: errorMessage
          }
        });
        expect(errorNoteSpy).toHaveBeenCalledWith(errorMessage);
      });

      it('should return error observable on error', () => {
        const mockName = new MockName(39, 'new name');

        let errorHit = false;
        const sub = service.createContent('name', 'new name', []).subscribe({
          error: () => { errorHit = true; }
        });
        mockResponse.error({});
        expect(sub.closed).toBeTrue();
        expect(errorHit).toBeTrue();
      });
    });

    describe('create tag', () => {
      let apiCreateSpy: jasmine.Spy;
      let mockResponse: Subject<CreateTagResponse>;

      beforeEach(() => {
        apiCreateSpy = spyOn(apiService, 'createTag');
        mockResponse = new Subject<CreateTagResponse>();
        apiCreateSpy.and.returnValue(mockResponse.pipe(take(1)));
      });

      it('should call correct api service method', () => {
        apiCreateSpy.and.returnValue(new Subject());

        expect(apiCreateSpy).not.toHaveBeenCalled();
        service.createContent('tag', 'new tag', []);
        expect(apiCreateSpy).toHaveBeenCalledWith('new tag', []);
      });

      it('should return the tag as an observable', () => {
        const mockTag = new MockTag(1, 'new tag');
        const sub = service.createContent('tag', 'new tag', []).subscribe(tag => {
          expect(tag).toEqual(mockTag);
        });
        expect(sub.closed).toBeFalse();
        mockResponse.next({item: mockTag});
        expect(sub.closed).toBeTrue();
      });

      it('should mark as saving during creation', () => {
        const mockTag = new MockTag(39, 'new tag');

        expect(service.requestInProgress$.value).toBeFalse();
        const sub = service.createContent('tag', 'new tag', []).subscribe();
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponse.next({item: mockTag});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should set the image tags on success', () => {
        const mockTag = MockTag.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub1 = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2]);
        });
        const sub2 = service.createContent('tag', 'new tag', mockImages).subscribe();
        expect(mockImages.map(img => img.tags)).toEqual([[], []]);
        expect(sub1.closed).toBeFalse();
        expect(sub2.closed).toBeFalse();
        mockResponse.next({item: mockTag});
        expect(mockImages.map(img => img.tags)).toEqual([[mockTag], [mockTag]]);
        expect(sub1.closed).toBeTrue();
        expect(sub2.closed).toBeTrue();
      });

      it('should not set the image tag ids on failure', () => {
        const mockTag = MockTag.new(39);
        const mockImages = [MockImage.new(1), MockImage.new(2)];

        const sub = service.createContent('tag', 'new tag', mockImages)
          .subscribe({error: () => {}});
        expect(mockImages.map(img => img.tags)).toEqual([[], []]);
        mockResponse.error({});
        expect(mockImages.map(img => img.tags)).toEqual([[], []]);
      });

      it('should trigger a notification on error', () => {
        const errorMessage = 'Could not create!';

        service.createContent('tag', 'new tag', []).subscribe({error: () => {}});
        expect(errorNoteSpy).not.toHaveBeenCalled();
        mockResponse.error({
          error: {
            message: errorMessage
          }
        });
        expect(errorNoteSpy).toHaveBeenCalledWith(errorMessage);
      });

      it('should return error observable on error', () => {
        const mockTag = new MockTag(39, 'new tag');

        let errorHit = false;
        const sub = service.createContent('tag', 'new tag', []).subscribe({
          error: () => { errorHit = true; }
        });
        mockResponse.error({});
        expect(sub.closed).toBeTrue();
        expect(errorHit).toBeTrue();
      });
    });
  });

  describe('deleteArtist', () => {
    let apiDeleteSpy: jasmine.Spy;

    beforeEach(() => {
      apiDeleteSpy = spyOn(apiService, 'deleteArtist');
    });

    it('should call correct api service method', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(apiDeleteSpy).not.toHaveBeenCalled();
      service.deleteArtist(1);
      expect(apiDeleteSpy).toHaveBeenCalledWith(1);
    });

    it('should fire artist deleted', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;

      const sub = service.contentDeleted$.artist.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([id]);
      });

      service.deleteArtist(id);
      expect(sub.closed).toBeFalse();
      mockSubject.next({});
      expect(sub.closed).toBeTrue();
    });

  });

  describe('deleteGroup', () => {
    let apiDeleteSpy: jasmine.Spy;
    let errorNoteSpy: jasmine.Spy;

    beforeEach(() => {
      apiDeleteSpy = spyOn(apiService, 'deleteGroup');
      errorNoteSpy = spyOn(noteService, 'error');
    });

    it('should call correct api service method', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(apiDeleteSpy).not.toHaveBeenCalled();
      service.deleteGroup(1);
      expect(apiDeleteSpy).toHaveBeenCalledWith(1);
    });

    it('should fire group deleted when completed successfully', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;

      const sub = service.contentDeleted$.group.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([id]);
      });

      service.deleteGroup(id);
      expect(sub.closed).toBeFalse();
      mockSubject.next({});
      expect(sub.closed).toBeTrue();
    });

    it('should trigger a notification on error', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;
      const errorMessage = 'Could not delete!';

      service.deleteGroup(id);
      expect(errorNoteSpy).not.toHaveBeenCalled();
      mockSubject.error({
        error: {
          message: errorMessage
        }
      });
      expect(errorNoteSpy).toHaveBeenCalledWith(errorMessage);
    });

    it('should fire group deleted even on error', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;

      const sub = service.contentDeleted$.group.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([id]);
      });

      service.deleteGroup(id);
      expect(sub.closed).toBeFalse();
      mockSubject.error({
        error: {message: 'Some error message'}
      });
      expect(sub.closed).toBeTrue();
    });

    it('should mark as saving while open', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(service.requestInProgress$.value).toBeFalse();
      service.deleteGroup(1);
      expect(service.requestInProgress$.value).toBeTrue();
    });

    it('should unmark as saving after success', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(service.requestInProgress$.value).toBeFalse();
      service.deleteGroup(1);
      expect(service.requestInProgress$.value).toBeTrue();
      mockSubject.next({});
      expect(service.requestInProgress$.value).toBeFalse();
    });

    it('should unmark as saving after error', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(service.requestInProgress$.value).toBeFalse();
      service.deleteGroup(1);
      expect(service.requestInProgress$.value).toBeTrue();
      mockSubject.error({error: {message: 'blah'}});
      expect(service.requestInProgress$.value).toBeFalse();
    });

  });

  describe('deleteImage', () => {
    let apiDeleteSpy: jasmine.Spy;
    let errorNoteSpy: jasmine.Spy;

    beforeEach(() => {
      apiDeleteSpy = spyOn(apiService, 'deleteImage');
      errorNoteSpy = spyOn(noteService, 'error');
    });

    it('should call correct api service method', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(apiDeleteSpy).not.toHaveBeenCalled();
      service.deleteImage(1);
      expect(apiDeleteSpy).toHaveBeenCalledWith(1);
    });

    it('should fire image deleted when completed successfully', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;

      const sub = service.contentDeleted$.image.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([id]);
      });

      service.deleteImage(id);
      expect(sub.closed).toBeFalse();
      mockSubject.next({});
      expect(sub.closed).toBeTrue();
    });

    it('should trigger a notification on error', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;
      const errorMessage = 'Could not delete!';

      service.deleteImage(id);
      expect(errorNoteSpy).not.toHaveBeenCalled();
      mockSubject.error({
        error: {
          message: errorMessage
        }
      });
      expect(errorNoteSpy).toHaveBeenCalledWith(errorMessage);
    });

    it('should fire image deleted even on error', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;

      const sub = service.contentDeleted$.image.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([id]);
      });

      service.deleteImage(id);
      expect(sub.closed).toBeFalse();
      mockSubject.error({
        error: {message: 'Some error message'}
      });
      expect(sub.closed).toBeTrue();
    });

    it('should mark as saving while open', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(service.requestInProgress$.value).toBeFalse();
      service.deleteImage(1);
      expect(service.requestInProgress$.value).toBeTrue();
    });

    it('should unmark as saving after success', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(service.requestInProgress$.value).toBeFalse();
      service.deleteImage(1);
      expect(service.requestInProgress$.value).toBeTrue();
      mockSubject.next({});
      expect(service.requestInProgress$.value).toBeFalse();
    });

    it('should unmark as saving after error', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(service.requestInProgress$.value).toBeFalse();
      service.deleteImage(1);
      expect(service.requestInProgress$.value).toBeTrue();
      mockSubject.error({error: {message: 'blah'}});
      expect(service.requestInProgress$.value).toBeFalse();
    });

  });

  describe('deleteName', () => {
    let apiDeleteSpy: jasmine.Spy;

    beforeEach(() => {
      apiDeleteSpy = spyOn(apiService, 'deleteName');
    });

    it('should call correct api service method', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(apiDeleteSpy).not.toHaveBeenCalled();
      service.deleteName(1);
      expect(apiDeleteSpy).toHaveBeenCalledWith(1);
    });

    it('should fire name deleted', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;

      const sub = service.contentDeleted$.name.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([id]);
      });

      service.deleteName(id);
      expect(sub.closed).toBeFalse();
      mockSubject.next({});
      expect(sub.closed).toBeTrue();
    });

  });

  describe('deleteTag', () => {
    let apiDeleteSpy: jasmine.Spy;

    beforeEach(() => {
      apiDeleteSpy = spyOn(apiService, 'deleteTag');
    });

    it('should call correct api service method', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));

      expect(apiDeleteSpy).not.toHaveBeenCalled();
      service.deleteTag(1);
      expect(apiDeleteSpy).toHaveBeenCalledWith(1);
    });

    it('should fire tag deleted', () => {
      const mockSubject = new Subject<EmptyResponse>();
      apiDeleteSpy.and.returnValue(mockSubject.pipe(take(1)));
      const id = 11;

      const sub = service.contentDeleted$.tag.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([id]);
      });

      service.deleteTag(id);
      expect(sub.closed).toBeFalse();
      mockSubject.next({});
      expect(sub.closed).toBeTrue();
    });

  });

  describe('rateImage', () => {
    let apiRateSpy: jasmine.Spy;
    let mockRateResponse: Subject<RateImageResponse>;

    beforeEach(() => {
      mockRateResponse = new Subject<RateImageResponse>();
      apiRateSpy = spyOn(apiService, 'rateImage').and.returnValue(mockRateResponse.pipe(take(1)));
    });

    it('should call correct api service method', () => {
      const image = MockImage.new(13);
      const rating = 3.5;

      expect(apiRateSpy).not.toHaveBeenCalled();
      service.rateImage(image, rating, true);
      expect(apiRateSpy).toHaveBeenCalledWith(image.id, rating, true);
    });

    it('should overwrite original image rating data with data from server on success', () => {
      const image = MockImage.new(13);
      const response = { average: 13.0, count: 151, };
      const expectedAverage = 13.0;
      const expectedCount = 151;

      service.rateImage(image, 3.5, true);
      expect(image.ratingAverage).not.toEqual(expectedAverage);
      expect(image.ratingCount).not.toEqual(expectedCount);
      mockRateResponse.next(response);
      expect(image.ratingAverage).toEqual(expectedAverage);
      expect(image.ratingCount).toEqual(expectedCount);
    });

    it('should fire image updated when completed successfully', () => {
      const image = MockImage.new(13);
      const response = { average: 13.0, count: 151, };

      const sub = service.imageUpdated$.pipe(take(1)).subscribe((ids) => {
        expect(ids).toEqual([13]);
      });

      service.rateImage(image, 3.5, false);
      expect(sub.closed).toBeFalse();
      mockRateResponse.next(response);
      expect(sub.closed).toBeTrue();
    });

  });

  describe('mark/unmark dupe methods', () => {
    let markSpy: jasmine.Spy;
    let unmarkSpy: jasmine.Spy;
    let imageData: Image[];
    let mockSubject: Subject<any>;

    beforeEach(() => {
      mockSubject = new Subject();
      let mockResponse = mockSubject.pipe(take(1));
      markSpy = spyOn(apiService, 'markDupes').and.returnValue(mockResponse);
      unmarkSpy = spyOn(apiService, 'unmarkDupes').and.returnValue(mockResponse);
      imageData = [0, 1, 2, 3, 4, 5].map(id => MockImage.new(10 + id));
    });

    it('should make correct call when marking duplicates', () => {
      const fakeImages = [0, 3, 4].map(i => imageData[i]);
      const expIds = [10, 13, 14];
      service.markDupes(fakeImages, 'Exact');
      expect(markSpy).toHaveBeenCalledOnceWith(expIds, 'Exact');
    });

    it('should make correct call when unmarking duplicates', () => {
      const fakeImages = [0, 3, 4].map(i => imageData[i]);
      const expIds = [10, 13, 14];
      service.unmarkDupes(fakeImages);
      expect(unmarkSpy).toHaveBeenCalledOnceWith(expIds);
    });

    it('should mark as saving while marking duplicates', () => {
      const fakeImages = [0, 3, 4].map(i => imageData[i]);

      expect(service.requestInProgress$.value).toBeFalse();
      service.markDupes(fakeImages, 'Exact');
      expect(service.requestInProgress$.value).toBeTrue();
      mockSubject.next(null);
      expect(service.requestInProgress$.value).toBeFalse();
    });

    it('should mark as saving while unmarking duplicates', () => {
      const fakeImages = [0, 3, 4].map(i => imageData[i]);

      expect(service.requestInProgress$.value).toBeFalse();
      service.unmarkDupes(fakeImages);
      expect(service.requestInProgress$.value).toBeTrue();
      mockSubject.next(null);
      expect(service.requestInProgress$.value).toBeFalse();
    });

    it('should update image dupe data when marking duplicates', () => {
      const fakeImages = [0, 1, 2].map(i => imageData[i]);
      const fakeDupeType = 'Partial';
      const startDupeData: {[key: number]: DupeType}[] = [
        {11: 'Exact', 12: 'Partial', 14: 'None'},
        {10: 'Exact', 13: 'None'},
        {10: 'Partial'},
        {11: 'None'},
        {10: 'None'},
        {},
      ];
      const expDupeData: {[key: number]: DupeType}[] = [
        {11: 'Partial', 12: 'Partial', 14: 'None'},
        {10: 'Partial', 12: 'Partial', 13: 'None'},
        {10: 'Partial', 11: 'Partial'},
        {11: 'None'},
        {10: 'None'},
        {},
      ];
      MockImage.setData(imageData, {dupes: startDupeData});
      expect(imageData.map(img => img.dupes)).toEqual(startDupeData);
      service.markDupes(fakeImages, fakeDupeType);
      expect(imageData.map(img => img.dupes)).toEqual(startDupeData);
      mockSubject.next(null);
      expect(imageData.map(img => img.dupes)).toEqual(expDupeData);
    });

    it('should update image dupe data when unmarking duplicates', () => {
      const fakeImages = [0, 1, 2].map(i => imageData[i]);
      const startDupeData: {[key: number]: DupeType}[] = [
        {11: 'Exact', 12: 'Partial', 14: 'None'},
        {10: 'Exact', 13: 'None'},
        {10: 'Partial'},
        {11: 'None'},
        {10: 'None'},
        {},
      ];
      const expDupeData: {[key: number]: DupeType}[] = [
        {14: 'None'},
        {13: 'None'},
        {},
        {11: 'None'},
        {10: 'None'},
        {},
      ];
      MockImage.setData(imageData, {dupes: startDupeData});
      expect(imageData.map(img => img.dupes)).toEqual(startDupeData);
      service.unmarkDupes(fakeImages);
      expect(imageData.map(img => img.dupes)).toEqual(startDupeData);
      mockSubject.next({});
      expect(imageData.map(img => img.dupes)).toEqual(expDupeData);
    });

  })

  describe('update methods', () => {

    describe('updateImage', () => {
      let apiUpdateSpy: jasmine.Spy;
      let errorNoteSpy: jasmine.Spy;
      let mockResponse: Subject<UpdateImageResponse>;

      beforeEach(() => {
        apiUpdateSpy = spyOn(apiService, 'updateImage');
        errorNoteSpy = spyOn(noteService, 'error');
        mockResponse = new Subject<UpdateImageResponse>();
        apiUpdateSpy.and.returnValue(mockResponse.pipe(take(1)));
      });

      it('should call correct api service method', () => {
        const image = MockImage.new(1);

        expect(apiUpdateSpy).not.toHaveBeenCalled();
        service.updateImage(image);
        expect(apiUpdateSpy).toHaveBeenCalledWith(image);
      });

      it('should overwrite original image with data from server on success', () => {
        const image = new MockImage(1, 'old name');
        const returnedImage = new MockImage(1, 'new name');
        apiUpdateSpy.and.returnValue(of({item: returnedImage}));

        expect(image.filename).toEqual('old name');
        service.updateImage(image);
        expect(image.filename).toEqual('new name');
      });

      it('should overwrite original image with data from server even on failure', () => {
        const image = new MockImage(1, 'old name');
        const returnedImage = new MockImage(1, 'new name');
        apiUpdateSpy.and.returnValue(throwError({
          error: {
            message: 'some message',
            item: returnedImage
          }
        }));

        expect(image.filename).toEqual('old name');
        service.updateImage(image);
        expect(image.filename).toEqual('new name');
      });

      it('should not overwrite original image with data from server when not given', () => {
        const image = new MockImage(1, 'old name');
        apiUpdateSpy.and.returnValue(throwError({
          error: {
            message: 'some message',
          }
        }));

        expect(image.filename).toEqual('old name');
        service.updateImage(image);
        expect(image.filename).toEqual('old name');
      });

      it('should fire image updated when completed successfully', () => {
        const image = MockImage.new(11);

        const sub = service.imageUpdated$.pipe(take(1)).subscribe((ids) => {
          expect(ids).toEqual([11]);
        });

        service.updateImage(image);
        expect(sub.closed).toBeFalse();
        mockResponse.next({item: image});
        expect(sub.closed).toBeTrue();
      });

      it('should trigger a notification on error', () => {
        const image = MockImage.new(11);
        const message = 'Could not delete!';

        service.updateImage(image);
        expect(errorNoteSpy).not.toHaveBeenCalled();
        mockResponse.error({
          error: {message, item: image}
        });
        expect(errorNoteSpy).toHaveBeenCalledWith(message);
      });

      it('should fire image updated even on error', () => {
        const image = MockImage.new(11);

        const sub = service.imageUpdated$.pipe(take(1)).subscribe((ids) => {
          expect(ids).toEqual([11]);
        });

        service.updateImage(image);
        expect(sub.closed).toBeFalse();
        mockResponse.error({
          error: {message: 'Some error message', item: image}
        });
        expect(sub.closed).toBeTrue();
      });

      it('should mark as saving while open', () => {
        const image = MockImage.new(11);

        expect(service.requestInProgress$.value).toBeFalse();
        service.updateImage(image);
        expect(service.requestInProgress$.value).toBeTrue();
      });

      it('should unmark as saving after success', () => {
        const image = MockImage.new(11);

        expect(service.requestInProgress$.value).toBeFalse();
        service.updateImage(image);
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponse.next({item: image});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should unmark as saving after error', () => {
        const image = MockImage.new(11);

        expect(service.requestInProgress$.value).toBeFalse();
        service.updateImage(image);
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponse.error({error: {message: 'blah'}});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should fire groupImagesUpdated$ when groupModified is true', () => {
        const image = MockImage.new(11);
        const sub = service.groupImagesUpdated$.pipe(take(1)).subscribe();

        expect(sub.closed).toBeFalse();
        service.updateImage(image, false);
        mockResponse.next({item: image});
        expect(sub.closed).toBeFalse();
        service.updateImage(image, true);
        mockResponse.next({item: image});
        expect(sub.closed).toBeTrue();
      });

    });

    describe('updateArtist', () => {
      let apiUpdateSpy: jasmine.Spy;
      let errorNoteSpy: jasmine.Spy;

      beforeEach(() => {
        apiUpdateSpy = spyOn(apiService, 'updateArtist');
        errorNoteSpy = spyOn(noteService, 'error');
      });

      it('should call correct api service method', () => {
        const artist = MockArtist.new(11);
        const mockSubject = new Subject<UpdateArtistResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(apiUpdateSpy).not.toHaveBeenCalled();
        service.updateArtist(artist);
        expect(apiUpdateSpy).toHaveBeenCalledWith(artist);
      });

      it('should overwrite original artist with data from server on success', () => {
        const artist = new MockArtist(1, 'old title');
        const returnedArtist = new MockArtist(1, 'new title');
        const mockSubject = new Subject<UpdateArtistResponse>();
        apiUpdateSpy.and.returnValue(of({item: returnedArtist}));

        expect(artist.value).toEqual('old title');
        service.updateArtist(artist);
        expect(artist.value).toEqual('new title');
      });

      it('should update the list of items in metadata', () => {
        const oldItem = new MockArtist(1, 'old name');
        const newItem = new MockArtist(1, 'new name');
        const expectedValues = ['old name', 'new name'];
        apiUpdateSpy.and.returnValue(newItem);
        getMetadataSpy.and.returnValue(of(mockMetadataResponse({artist: [oldItem]})));

        let counter1 = 0;
        let counter2 = 0;
        service.getContentList('artist').subscribe((items) => {
          expect(items.map(i => i.value)).toEqual([expectedValues[counter1]]);
          counter1 += 1;
        });
        service.getContentValueById('artist', 1).subscribe((value)=> {
          expect(value).toEqual(expectedValues[counter2]);
          counter2 += 1;
        });

        expect(counter1).toBe(1);
        expect(counter2).toBe(1);

        service.updateArtist(newItem);

        expect(counter1).toBe(2);
        expect(counter2).toBe(2);
      });

    });

    describe('updateGroup', () => {
      let apiUpdateSpy: jasmine.Spy;
      let errorNoteSpy: jasmine.Spy;

      beforeEach(() => {
        apiUpdateSpy = spyOn(apiService, 'updateGroup');
        errorNoteSpy = spyOn(noteService, 'error');
      });

      it('should call correct api service method', () => {
        const group = MockGroup.new(11);
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(apiUpdateSpy).not.toHaveBeenCalled();
        service.updateGroup(group);
        expect(apiUpdateSpy).toHaveBeenCalledWith(group);
      });

      it('should overwrite original group with data from server on success', () => {
        const group = new MockGroup(1, 'old title');
        const returnedGroup = new MockGroup(1, 'new title');
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(of({item: returnedGroup}));

        expect(group.value).toEqual('old title');
        service.updateGroup(group);
        expect(group.value).toEqual('new title');
      });

      it('should overwrite original group with data from server even on failure', () => {
        const group = new MockGroup(1, 'old title');
        const returnedGroup = new MockGroup(1, 'new title');
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(throwError({
          error: {
            message: 'some message',
            item: returnedGroup
          }
        }));

        expect(group.value).toEqual('old title');
        service.updateGroup(group);
        expect(group.value).toEqual('new title');
      });

      it('should not overwrite original group with data from server when not given', () => {
        const group = new MockGroup(1, 'old title');
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(throwError({
          error: {
            message: 'some message',
          }
        }));

        expect(group.value).toEqual('old title');
        service.updateGroup(group);
        expect(group.value).toEqual('old title');
      });

      it('should fire group updated when completed successfully', () => {
        const group = MockGroup.new(11);
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        const sub = service.contentUpdated$.group.pipe(take(1)).subscribe((ids) => {
          expect(ids).toEqual([11]);
        });

        service.updateGroup(group);
        expect(sub.closed).toBeFalse();
        mockSubject.next({item: group});
        expect(sub.closed).toBeTrue();
      });

      it('should trigger a notification on error', () => {
        const group = MockGroup.new(11);
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));
        const message = 'Could not delete!';

        service.updateGroup(group);
        expect(errorNoteSpy).not.toHaveBeenCalled();
        mockSubject.error({
          error: {message, item: group}
        });
        expect(errorNoteSpy).toHaveBeenCalledWith(message);
      });

      it('should fire group updated even on error', () => {
        const group = MockGroup.new(11);
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        const sub = service.contentUpdated$.group.pipe(take(1)).subscribe((ids) => {
          expect(ids).toEqual([11]);
        });

        service.updateGroup(group);
        expect(sub.closed).toBeFalse();
        mockSubject.error({
          error: {message: 'Some error message', item: group}
        });
        expect(sub.closed).toBeTrue();
      });

      it('should mark as saving while open', () => {
        const group = MockGroup.new(11);
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(service.requestInProgress$.value).toBeFalse();
        service.updateGroup(group);
        expect(service.requestInProgress$.value).toBeTrue();
      });

      it('should unmark as saving after success', () => {
        const group = MockGroup.new(11);
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(service.requestInProgress$.value).toBeFalse();
        service.updateGroup(group);
        expect(service.requestInProgress$.value).toBeTrue();
        mockSubject.next({item: group});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should unmark as saving after error', () => {
        const group = MockGroup.new(11);
        const mockSubject = new Subject<UpdateGroupResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(service.requestInProgress$.value).toBeFalse();
        service.updateGroup(group);
        expect(service.requestInProgress$.value).toBeTrue();
        mockSubject.error({error: {message: 'blah'}});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should update the list of items in metadata', () => {
        const oldItem = new MockGroup(1, 'old name');
        const newItem = new MockGroup(1, 'new name');
        const expectedValues = ['old name', 'new name'];
        apiUpdateSpy.and.returnValue(newItem);
        getMetadataSpy.and.returnValue(of(mockMetadataResponse({group: [oldItem]})));

        let counter1 = 0;
        let counter2 = 0;
        service.getContentList('group').subscribe((items) => {
          expect(items.map(i => i.value)).toEqual([expectedValues[counter1]]);
          counter1 += 1;
        });
        service.getContentValueById('group', 1).subscribe((value)=> {
          expect(value).toEqual(expectedValues[counter2]);
          counter2 += 1;
        });

        expect(counter1).toBe(1);
        expect(counter2).toBe(1);

        service.updateGroup(newItem);

        expect(counter1).toBe(2);
        expect(counter2).toBe(2);
      });

    });

    describe('updateImages', () => {
      let mockImages: Image[];
      let mockResponses: Map<ImageId, Subject<UpdateImageResponse>>; // fake responses from ApiService
      let errorNoteSpy: jasmine.Spy;

      beforeEach(() => {
        mockImages = [
          new MockImage(9, '99.jpg'), // so indices and IDs line up for clarity
          new MockImage(1, '1.jpg'),
          new MockImage(2, '2.jpg'),
          new MockImage(3, '3.jpg'),
          new MockImage(4, '4.jpg'),
          new MockImage(5, '5.jpg'),
        ];
        mockResponses = new Map();
        function fakeUpdate(image: Image) {
          const fakeResponse = new Subject<UpdateImageResponse>();
          mockResponses.set(image.id, fakeResponse);
          return fakeResponse.pipe(take(1));
        }
        spyOn(apiService, 'updateImage').and.callFake((image) => {
          return fakeUpdate(image);
        });
        errorNoteSpy = spyOn(noteService, 'error');
      });

      it('should do nothing when list of images is empty', () => {
        expect(service.requestInProgress$.value).toBeFalse();
        service.updateImages([]);
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should be saving until all saves are finished', () => {
        const images = [
          mockImages[1],
          mockImages[2]
        ];

        expect(service.requestInProgress$.value).toBeFalse();
        service.updateImages(images);
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponses.get(1)!.next({item: mockImages[1]});
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponses.get(2)!.next({item: mockImages[2]});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should emit from imageUpdated$ once at the end', () => {
        const images = [
          mockImages[1],
          mockImages[2]
        ];
        const sub = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2]);
        });

        service.updateImages(images);
        expect(sub.closed).toBeFalse();
        mockResponses.get(1)!.next({item: mockImages[1]});
        expect(sub.closed).toBeFalse();
        mockResponses.get(2)!.next({item: mockImages[2]});
        expect(sub.closed).toBeTrue();
      });

      it('should overwrite passed objects with data from server', () => {
        const originalImages = [
          new MockImage(1, 'old filename 1.jpg'),
          new MockImage(2, 'old filename 2.jpg')
        ];
        const responseImages = [
          new MockImage(1, 'new filename 1.jpg'),
          new MockImage(2, 'new filename 2.jpg')
        ];

        service.updateImages(originalImages);
        mockResponses.get(1)!.next({item: responseImages[0]});
        expect(originalImages[0].filename).toEqual('new filename 1.jpg');
        mockResponses.get(2)!.next({item: responseImages[1]});
        expect(originalImages[1].filename).toEqual('new filename 2.jpg');
      });

      it('should wait for all jobs to finish after failure before emitting', () => {
        const images = [
          mockImages[1],
          mockImages[2],
          mockImages[3],
        ];
        const sub = service.imageUpdated$.pipe(take(1)).subscribe(ids => {
          expect(ids).toEqual([1, 2, 3]);
        });

        service.updateImages(images);
        expect(sub.closed).toBeFalse();
        mockResponses.get(1)!.next({item: mockImages[1]});
        expect(sub.closed).toBeFalse();
        mockResponses.get(2)!.error({error: {item: mockImages[2]}});
        expect(sub.closed).toBeFalse();
        mockResponses.get(3)!.next({item: mockImages[3]});
        expect(sub.closed).toBeTrue();
      });

      it('should wait for all jobs to finish after failure before setting is saved to false', () => {
        const images = [
          mockImages[1],
          mockImages[2],
          mockImages[3],
        ];

        service.updateImages(images);
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponses.get(1)!.next({item: mockImages[1]});
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponses.get(2)!.error({error: {item: mockImages[2]}});
        expect(service.requestInProgress$.value).toBeTrue();
        mockResponses.get(3)!.next({item: mockImages[3]});
        expect(service.requestInProgress$.value).toBeFalse();
      });

      it('should trigger a notification for each update that fails', () => {
        const images = [
          mockImages[1],
          mockImages[2],
          mockImages[3],
        ];
        const errorMessages = [
          'image 1 failed',
          'image 2 failed',
          'image 3 failed'
        ];

        service.updateImages(images);
        mockResponses.get(1)!.error({error: {message: errorMessages[0]}});
        mockResponses.get(2)!.error({error: {message: errorMessages[1]}});
        mockResponses.get(3)!.error({error: {message: errorMessages[2]}});
        expect(errorNoteSpy).toHaveBeenCalledTimes(3);
        expect(errorNoteSpy).toHaveBeenCalledWith('image 1 failed');
        expect(errorNoteSpy).toHaveBeenCalledWith('image 2 failed');
        expect(errorNoteSpy).toHaveBeenCalledWith('image 3 failed');
      });

      it('should fire groupImagesUpdated when groupModified is true', () => {
        const images = [ mockImages[1], mockImages[2] ];

        const sub1 = service.groupImagesUpdated$.pipe(take(1)).subscribe();
        service.updateImages(images, true);
        mockResponses.get(1)!.next({item: mockImages[1]});
        mockResponses.get(2)!.next({item: mockImages[2]});
        expect(sub1.closed).toBeTrue();

        const sub2 = service.groupImagesUpdated$.pipe(take(1)).subscribe();
        service.updateImages(images, false);
        mockResponses.get(1)!.next({item: mockImages[1]});
        mockResponses.get(2)!.next({item: mockImages[2]});
        expect(sub2.closed).toBeFalse();
      });
    });

    describe('updateName', () => {
      let apiUpdateSpy: jasmine.Spy;
      let errorNoteSpy: jasmine.Spy;

      beforeEach(() => {
        apiUpdateSpy = spyOn(apiService, 'updateName');
        errorNoteSpy = spyOn(noteService, 'error');
      });

      it('should call correct api service method', () => {
        const name = MockName.new(11);
        const mockSubject = new Subject<UpdateNameResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(apiUpdateSpy).not.toHaveBeenCalled();
        service.updateName(name);
        expect(apiUpdateSpy).toHaveBeenCalledWith(name);
      });

      it('should overwrite original name with data from server on success', () => {
        const name = new MockName(1, 'old title');
        const returnedName = new MockName(1, 'new title');
        const mockSubject = new Subject<UpdateNameResponse>();
        apiUpdateSpy.and.returnValue(of({item: returnedName}));

        expect(name.value).toEqual('old title');
        service.updateName(name);
        expect(name.value).toEqual('new title');
      });

      it('should update the list of items in metadata', () => {
        const oldItem = new MockName(1, 'old name');
        const newItem = new MockName(1, 'new name');
        const expectedValues = ['old name', 'new name'];
        apiUpdateSpy.and.returnValue(newItem);
        getMetadataSpy.and.returnValue(of(mockMetadataResponse({name: [oldItem]})));

        let counter1 = 0;
        let counter2 = 0;
        service.getContentList('name').subscribe((items) => {
          expect(items.map(i => i.value)).toEqual([expectedValues[counter1]]);
          counter1 += 1;
        });
        service.getContentValueById('name', 1).subscribe((value)=> {
          expect(value).toEqual(expectedValues[counter2]);
          counter2 += 1;
        });

        expect(counter1).toBe(1);
        expect(counter2).toBe(1);

        service.updateName(newItem);

        expect(counter1).toBe(2);
        expect(counter2).toBe(2);
      });

    });

    describe('updateTag', () => {
      let apiUpdateSpy: jasmine.Spy;
      let errorNoteSpy: jasmine.Spy;

      beforeEach(() => {
        apiUpdateSpy = spyOn(apiService, 'updateTag');
        errorNoteSpy = spyOn(noteService, 'error');
      });

      it('should call correct api service method', () => {
        const tag = MockTag.new(11);
        const mockSubject = new Subject<UpdateTagResponse>();
        apiUpdateSpy.and.returnValue(mockSubject.pipe(take(1)));

        expect(apiUpdateSpy).not.toHaveBeenCalled();
        service.updateTag(tag);
        expect(apiUpdateSpy).toHaveBeenCalledWith(tag);
      });

      it('should overwrite original tag with data from server on success', () => {
        const tag = new MockTag(1, 'old title');
        const returnedTag = new MockTag(1, 'new title');
        const mockSubject = new Subject<UpdateTagResponse>();
        apiUpdateSpy.and.returnValue(of({item: returnedTag}));

        expect(tag.value).toEqual('old title');
        service.updateTag(tag);
        expect(tag.value).toEqual('new title');
      });

      it('should update the list of items in metadata', () => {
        const oldItem = new MockTag(1, 'old name');
        const newItem = new MockTag(1, 'new name');
        const expectedValues = ['old name', 'new name'];
        apiUpdateSpy.and.returnValue(newItem);
        getMetadataSpy.and.returnValue(of(mockMetadataResponse({tag: [oldItem]})));

        let counter1 = 0;
        let counter2 = 0;
        service.getContentList('tag').subscribe((items) => {
          expect(items.map(i => i.value)).toEqual([expectedValues[counter1]]);
          counter1 += 1;
        });
        service.getContentValueById('tag', 1).subscribe((value)=> {
          expect(value).toEqual(expectedValues[counter2]);
          counter2 += 1;
        });

        expect(counter1).toBe(1);
        expect(counter2).toBe(1);

        service.updateTag(newItem);

        expect(counter1).toBe(2);
        expect(counter2).toBe(2);
      });

    });

  });

  describe('metadataFeatures', () => {

    it('should get a list of artists', () => {
      const fakeContent = {
        artist: [MockArtist.new(1), MockArtist.new(42), MockArtist.new(13)],
        group: [MockGroup.new(2), MockGroup.new(3)],
        name: [MockName.new(11)],
        tag: [MockTag.new(1), MockTag.new(2), MockTag.new(3)],
      };
      getMetadataSpy.and.returnValue(of(mockMetadataResponse(fakeContent)));

      const sub = service.getContentList('artist').pipe(take(1)).subscribe((items) => {
        expect(items).toEqual(fakeContent.artist);
      });
      expect(sub.closed).toBeTrue();
    });

    it('should get a list for each content type', () => {
      const fakeContent = {
        artist: [MockArtist.new(1), MockArtist.new(42), MockArtist.new(13)],
        group: [MockGroup.new(2), MockGroup.new(3)],
        name: [MockName.new(11)],
        tag: [MockTag.new(1), MockTag.new(2), MockTag.new(3)],
      };
      const fakeMetadataResponse = new Subject();
      getMetadataSpy.and.returnValue(fakeMetadataResponse.pipe(take(1)));

      // Request the content list for each content type.
      const subs = {};
      CONTENT_TYPES.forEach((type: ContentType) => {
        subs[type] = service.getContentList(type).pipe(take(1)).subscribe((items) => {
          expect(items).toEqual(fakeContent[type]);
        });
      });

      // Expect that only a single API call was made.
      expect(apiService.getMetadata).toHaveBeenCalledTimes(1);
      CONTENT_TYPES.forEach(type => {
        expect(subs[type].closed).toBeFalse();
      });

      // Fulfill that API call.
      fakeMetadataResponse.next(mockMetadataResponse(fakeContent));

      CONTENT_TYPES.forEach(type => {
        expect(subs[type].closed).toBeTrue();
      });
    });

    it('should get a list of artists without subsequent requests', () => {
      const fakeContent = {
        artist: [MockArtist.new(1), MockArtist.new(42), MockArtist.new(13)],
        group: [MockGroup.new(2), MockGroup.new(3)],
        name: [MockName.new(11)],
        tag: [MockTag.new(1), MockTag.new(2), MockTag.new(3)],
      };
      getMetadataSpy.and.returnValue(of(mockMetadataResponse(fakeContent)));

      service.getContentList('artist').subscribe();
      expect(apiService.getMetadata).toHaveBeenCalledTimes(1);
      service.getContentList('artist').subscribe();
      service.getContentList('artist').subscribe();
      service.getContentList('artist').subscribe();
      service.getContentList('artist').subscribe();
      service.getContentList('artist').subscribe();
      expect(apiService.getMetadata).toHaveBeenCalledTimes(1);
    });

    it('should regenerate metadata after schema change', fakeAsync(() => {
      const mockNames1 = [MockName.new(1), MockName.new(2)];
      const mockNames2 = [MockName.new(3), MockName.new(4)];
      const expectedNames = [[1, 2], [3, 4]];

      getMetadataSpy.and.returnValue(of(mockMetadataResponse({name: mockNames1})));
      let counter = 0;
      const sub = service.getContentList('name').subscribe((names) => {
        expect(names.map(n => n.id)).toEqual(expectedNames[counter]);
        counter += 1;
      });

      getMetadataSpy.and.returnValue(of(mockMetadataResponse({name: mockNames2})));
      configService.setDatabaseSchema('new-schema');

      expect(getMetadataSpy).toHaveBeenCalledTimes(2);
      expect(counter).toBe(2);
    }));

    it('should handle when schema changes before request resolves', () => {
      const mockResponse1Ctrl = new Subject();
      const mockResponse2Ctrl = new Subject();
      const mockResponse1 = {name: [MockName.new(1), MockName.new(2)], tag: []};
      const mockResponse2 = {name: [MockName.new(3), MockName.new(4)], tag: []};
      const subExpected = (names) => expect(names.map(n => n.id)).toEqual([3, 4]);

      // We change schema before first metadata request resolves, then we resolve the requests in
      // received order. However, we expect all of the metadata requests to resolve with the data
      // matching the new schema.

      getMetadataSpy.and.returnValue(mockResponse1Ctrl);
      const sub1 = service.getContentList('name').pipe(take(1)).subscribe(subExpected);

      getMetadataSpy.and.returnValue(mockResponse2Ctrl);
      configService.setDatabaseSchema('schema-2');

      const sub2 = service.getContentList('name').pipe(take(1)).subscribe(subExpected);

      mockResponse1Ctrl.next(mockMetadataResponse(mockResponse1));

      expect(sub1.closed).toBeFalse();
      expect(sub2.closed).toBeFalse();

      mockResponse2Ctrl.next(mockMetadataResponse(mockResponse2));
      expect(sub1.closed).toBeTrue();
      expect(sub2.closed).toBeTrue();
    });

    it('should handle when schema changes before request resolves (reverse order)', () => {
      const mockResponse1Ctrl = new Subject();
      const mockResponse2Ctrl = new Subject();
      const mockResponse1 = {name: [MockName.new(1), MockName.new(2)], tag: []};
      const mockResponse2 = {name: [MockName.new(3), MockName.new(4)], tag: []};
      const subExpected = (names) => expect(names.map(n => n.id)).toEqual([3, 4]);

      // We change schema before first metadata request resolves, then we resolve the requests in
      // reverse order. However, we expect all of the metadata requests to resolve with the data
      // matching the new schema.

      getMetadataSpy.and.returnValue(mockResponse1Ctrl);
      const sub1 = service.getContentList('name').pipe(take(2)).subscribe(subExpected);

      getMetadataSpy.and.returnValue(mockResponse2Ctrl);
      configService.setDatabaseSchema('schema-2');

      const sub2 = service.getContentList('name').pipe(take(2)).subscribe(subExpected);

      mockResponse2Ctrl.next(mockMetadataResponse(mockResponse2));
      expect(sub1.closed).toBeFalse();
      expect(sub2.closed).toBeFalse();

      mockResponse1Ctrl.next(mockMetadataResponse(mockResponse2));
      expect(sub1.closed).toBeFalse();
      expect(sub2.closed).toBeFalse();

      mockResponse2Ctrl.next(mockMetadataResponse(mockResponse2));
      expect(sub1.closed).toBeTrue();
      expect(sub2.closed).toBeTrue();
    });

    it('should get the value of content by looking up the ID in a map', () => {
      const fakeContent = {
        artist: [MockArtist.new(1), MockArtist.new(42), MockArtist.new(13)],
        group: [MockGroup.new(2), MockGroup.new(3)],
        name: [MockName.new(11)],
        tag: [MockTag.new(1), MockTag.new(2), MockTag.new(3)],
      };
      getMetadataSpy.and.returnValue(of(mockMetadataResponse(fakeContent)));

      service.getContentList('artist').subscribe();

      expect(service.getContentValueById('artist', 1).pipe(take(1)).subscribe(value => {
        expect(value).toEqual('Artist 1');
      }).closed).toBeTrue();
      expect(service.getContentValueById('artist', 42).pipe(take(1)).subscribe(value => {
        expect(value).toEqual('Artist 42');
      }).closed).toBeTrue();
      expect(service.getContentValueById('group', 3).pipe(take(1)).subscribe(value => {
        expect(value).toEqual('Group 3');
      }).closed).toBeTrue();
      expect(service.getContentValueById('name', 11).pipe(take(1)).subscribe(value => {
        expect(value).toEqual('Name 11');
      }).closed).toBeTrue();
      expect(service.getContentValueById('tag', 2).pipe(take(1)).subscribe(value => {
        expect(value).toEqual('Tag 2');
      }).closed).toBeTrue();
    });

    it('should return undefined from getContentValueById for unknown IDs', () => {
      const fakeContent = {
        artist: [MockArtist.new(1), MockArtist.new(42), MockArtist.new(13)],
        group: [MockGroup.new(2), MockGroup.new(3)],
        name: [MockName.new(11)],
        tag: [MockTag.new(1), MockTag.new(2), MockTag.new(3)],
      };
      getMetadataSpy.and.returnValue(of(mockMetadataResponse(fakeContent)));

      service.getContentList('artist').subscribe();

      expect(service.getContentValueById('artist', 1).pipe(take(1)).subscribe(value => {
        expect(value).toEqual('Artist 1');
      }).closed).toBeTrue();
      expect(service.getContentValueById('artist', 142).pipe(take(1)).subscribe(value => {
        expect(value).toBe(undefined);
      }).closed).toBeTrue();
      expect(service.getContentValueById('group', 13).pipe(take(1)).subscribe(value => {
        expect(value).toBe(undefined);
      }).closed).toBeTrue();
      expect(service.getContentValueById('name', 111).pipe(take(1)).subscribe(value => {
        expect(value).toBe(undefined);
      }).closed).toBeTrue();
      expect(service.getContentValueById('tag', 12).pipe(take(1)).subscribe(value => {
        expect(value).toBe(undefined);
      }).closed).toBeTrue();
    });

    it('should emit new values from getContentValueById when schema changes', fakeAsync(() => {
      const mockNames1 = [new MockName(1, 'old name')];
      const mockNames2 = [new MockName(1, 'new name')];
      const expectedValues = ['old name', 'new name'];

      getMetadataSpy.and.returnValue(of(mockMetadataResponse({name: mockNames1})));
      let counter = 0;
      const sub = service.getContentValueById('name', 1).subscribe((value) => {
        expect(value).toEqual(expectedValues[counter]);
        counter += 1;
      });

      getMetadataSpy.and.returnValue(of(mockMetadataResponse({name: mockNames2})));
      configService.setDatabaseSchema('new-schema');
      expect(counter).toBe(2);
    }));

  });

  describe('getImageBlobUrl', () => {
    let getImageBlobSpy: jasmine.Spy;
    let getUrlFromBlobSpy: jasmine.Spy;

    beforeEach(() => {
      getImageBlobSpy = spyOn(apiService, 'getImageBlob');
      getUrlFromBlobSpy = spyOn(service, 'getUrlFromBlob');
    });

    it('should call correct api service method', () => {
      const image = MockImage.new(1);
      const mockBlob = {test: 1} as any as Blob;
      const mockSafeUrl = {test: 2} as any as SafeUrl;
      getImageBlobSpy.and.returnValue(of(mockBlob));
      getUrlFromBlobSpy.and.returnValue(mockSafeUrl);

      const sub = service.getImageBlobUrl(image)
        .pipe(take(1))
        .subscribe((result) => { expect(result).toEqual(mockSafeUrl); });

      expect(sub.closed).toBeTrue();
      expect(getImageBlobSpy).toHaveBeenCalledOnceWith(image);
      expect(getUrlFromBlobSpy).toHaveBeenCalledOnceWith(mockBlob);
    });

    it('should return the stock missing image when blob load fails', () => {
      const image = MockImage.new(1);
      const mockBlob = {test: 1} as any as Blob;
      const expSafeUrl = service.MISSING_IMAGE;
      getImageBlobSpy.and.returnValue(throwError('Fake error'));

      const sub = service.getImageBlobUrl(image)
        .pipe(take(1))
        .subscribe((result) => { expect(result).toEqual(expSafeUrl); });

      expect(sub.closed).toBeTrue();
      expect(getImageBlobSpy).toHaveBeenCalledOnceWith(image);
      expect(getUrlFromBlobSpy).not.toHaveBeenCalled();
    });

  });

  describe('getThumbnailBlobUrl', () => {
    let getThumbnailBlobSpy: jasmine.Spy;
    let getUrlFromBlobSpy: jasmine.Spy;

    beforeEach(() => {
      getThumbnailBlobSpy = spyOn(apiService, 'getThumbnailBlob');
      getUrlFromBlobSpy = spyOn(service, 'getUrlFromBlob');
    });

    it('should call correct api service method', () => {
      const image = MockImage.new(1);
      const mockBlob = {test: 1} as any as Blob;
      const mockSafeUrl = {test: 2} as any as SafeUrl;
      getThumbnailBlobSpy.and.returnValue(of(mockBlob));
      getUrlFromBlobSpy.and.returnValue(mockSafeUrl);

      const sub = service.getThumbnailBlobUrl(image)
        .pipe(take(1))
        .subscribe((result) => { expect(result).toEqual(mockSafeUrl); });

      expect(sub.closed).toBeTrue();
      expect(getThumbnailBlobSpy).toHaveBeenCalledOnceWith(image);
      expect(getUrlFromBlobSpy).toHaveBeenCalledOnceWith(mockBlob);
    });

    it('should return a stock image thumbnail for non-video image when blob load fails', () => {
      const image = MockImage.new(1);
      image.isVideo = false;
      const mockBlob = {test: 1} as any as Blob;
      const expSafeUrl = service.IMAGE_THUMBNAIL_IMAGE;
      getThumbnailBlobSpy.and.returnValue(throwError('Fake error'));

      expect(service.getThumbnailBlobUrl(image)
        .pipe(take(1))
        .subscribe((result) => { expect(result).toEqual(expSafeUrl); })
        .closed
      ).toBeTrue();

      expect(getThumbnailBlobSpy).toHaveBeenCalledOnceWith(image);
      expect(getUrlFromBlobSpy).not.toHaveBeenCalled();
    });

    it('should return a stock video thumbnail for video when blob load fails', () => {
      const image = MockImage.new(1);
      image.isVideo = true;
      const mockBlob = {test: 1} as any as Blob;
      const expSafeUrl = service.MOVIE_THUMBNAIL_IMAGE;
      getThumbnailBlobSpy.and.returnValue(throwError('Fake error'));

      expect(service.getThumbnailBlobUrl(image)
        .pipe(take(1))
        .subscribe((result) => { expect(result).toEqual(expSafeUrl); })
        .closed
      ).toBeTrue();

      expect(getThumbnailBlobSpy).toHaveBeenCalledOnceWith(image);
      expect(getUrlFromBlobSpy).not.toHaveBeenCalled();
    });

  });

});
