import {take} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {M, P, } from '@angular/cdk/keycodes';
import {TestBed} from '@angular/core/testing';

import {Action, ACTION_DATA, KeyBinding, KeyEventLike, } from '@app/models';

import {ActionService} from '.';

describe('ActionService', () => {
  let service: ActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActionService);
  });

  afterEach(() => {
    service.testRemoveEventListener();
  });

  it('should register all actions', () => {
    ACTION_DATA.forEach((_, action) => {
      const entry = service.getRegisteredAction(action);
      expect(entry).toBeTruthy();
    });
  });

  it('should disable all actions by default', () => {
    ACTION_DATA.forEach((_, action) => {
      const entry = service.getRegisteredAction(action);
      expect(entry.enabled$.value).toBeFalse();
    });
  });

  describe('captureAction', () => {

    it('should mark action as captured', () => {
      const action = Action.ShowFaceMatches;

      const entry = service.getRegisteredAction(action);

      expect(entry.captured).toBeFalse();
      const sub = service.captureAction(action).subscribe();
      expect(entry.captured).toBeTrue();
      sub.unsubscribe();
      expect(entry.captured).toBeFalse();
    });

    it('should throw an error when multiple sources try to capture an action', () => {
      const action = Action.ShowFaceMatches;
      const description = ACTION_DATA.get(action)!.description;
      const expectedMessage = `Action "${description}" is already captured`;

      service.captureAction(action).subscribe();
      const errorSub = service.captureAction(action).subscribe({
        next: () => expect(true).toBeFalse(),
        error: (msg) => expect(msg).toEqual(expectedMessage)
      });

      expect(errorSub.closed).toBeTrue();
    });

    it('should capture action invocations when enabled', () => {
      const action = Action.ShowFaceMatches;
      const mockEvent = {test: 'hi'} as any;
      const sub = service.captureAction(action)
        .pipe(take(2))
        .subscribe((ev) => {
          expect(ev).toEqual(mockEvent);
        });

      expect(sub.closed).toBeFalse();
      service.doAction(action, mockEvent);
      expect(sub.closed).toBeFalse();
      service.doAction(action, mockEvent);
      expect(sub.closed).toBeTrue();
    });

    it('should not capture action invocations when disabled', () => {
      const action = Action.ShowFaceMatches;
      const sub = service.captureAction(action)
        .pipe(take(1))
        .subscribe(() => expect(true).toBeFalse());

      service.addActionCondition(action, new BehaviorSubject(false)).subscribe();

      expect(service.isActionEnabled(action)).toBeFalse();
      service.doAction(action);
      expect(sub.closed).toBeFalse();
    });

    it('should enable the action by default', () => {
      const action = Action.ShowFaceMatches;

      expect(service.isActionEnabled(action)).toBeFalse();
      const obs = service.captureAction(action);
      expect(service.isActionEnabled(action)).toBeFalse();
      const sub = obs.subscribe();
      expect(service.isActionEnabled(action)).toBeTrue();
    });

    it('should use enabled observable to determine enable', () => {
      const action = Action.ShowFaceMatches;
      const enabled = new BehaviorSubject(false);

      expect(service.isActionEnabled(action)).toBeFalse();
      const sub = service.captureAction(action, enabled).subscribe();
      expect(service.isActionEnabled(action)).toBeFalse();
      enabled.next(true);
      expect(service.isActionEnabled(action)).toBeTrue();
      enabled.next(false);
      expect(service.isActionEnabled(action)).toBeFalse();
    });

    it('should disable the action after unsubscribing without an enabled observable', () => {
      const action = Action.ShowFaceMatches;

      const sub = service.captureAction(action).subscribe();
      expect(service.isActionEnabled(action)).toBeTrue();

      sub.unsubscribe();
      expect(service.isActionEnabled(action)).toBeFalse();

      service.captureAction(action).subscribe();
      expect(service.isActionEnabled(action)).toBeTrue();
    });

    it('should disable the action after unsubscribing with an enabled observable', () => {
      const action = Action.ShowFaceMatches;
      const enabled = new BehaviorSubject(true);

      const sub = service.captureAction(action, enabled).subscribe();
      expect(service.isActionEnabled(action)).toBeTrue();

      sub.unsubscribe();
      expect(service.isActionEnabled(action)).toBeFalse();

      enabled.next(true);
      expect(service.isActionEnabled(action)).toBeFalse();
    });

    it('should disable after unsubscribing even with other action conditions', () => {
      const action = Action.ShowFaceMatches;
      const externalCondition = new BehaviorSubject(true);
      const internalCondition = new BehaviorSubject(true);

      const sub = service.captureAction(action, internalCondition).subscribe();
      service.addActionCondition(action, externalCondition).subscribe();
      expect(service.isActionEnabled(action)).toBeTrue();

      sub.unsubscribe();
      expect(service.isActionEnabled(action)).toBeFalse();

      externalCondition.next(true);
      expect(service.isActionEnabled(action)).toBeFalse();
    });
  });

  describe('setActionKeyBinding', () => {
    const action = Action.ShowFaceMatches;
    const binding1 = new KeyBinding(M, {ctrlKey: true, altKey: true, shiftKey: true});
    const binding2 = new KeyBinding(P, {ctrlKey: true, altKey: true, shiftKey: true});
    let bindingChangedSpy: jasmine.Spy;

    beforeEach(() => {
      bindingChangedSpy = spyOn(service.getRegisteredAction(action).bindingChanged$, 'next').and.callThrough();
    });

    it('should change the key binding', () => {
      expect(service.getActionBinding(action)).not.toEqual(binding1);
      service.setActionKeyBinding(action, binding1);
      expect(service.getActionBinding(action)).toEqual(binding1);
      service.setActionKeyBinding(action, binding2);
      expect(service.getActionBinding(action)).toEqual(binding2);
    });

    it('should mark existing key binding as free', () => {
      service.setActionKeyBinding(action, binding1);

      expect(service.getBoundAction(binding1)).toEqual(action);
      expect(service.getBoundAction(binding2)).toBeNull();

      service.setActionKeyBinding(action, binding2);
      expect(service.getBoundAction(binding1)).toBeNull();
      expect(service.getBoundAction(binding2)).toEqual(action);
    });

    it('should fire bindingChanged$ after setting an action key binding', () => {
      expect(bindingChangedSpy).not.toHaveBeenCalled();

      service.setActionKeyBinding(action, binding1);

      expect(bindingChangedSpy).toHaveBeenCalledTimes(1);
    });

    it('should fire keyBindingChanged$ even when setting to null', () => {
      service.setActionKeyBinding(action, binding1);
      bindingChangedSpy.calls.reset();

      service.setActionKeyBinding(action, null);
      expect(bindingChangedSpy).toHaveBeenCalledTimes(1);
    });

    it('should throw an error if the action is marked as unbindable', () => {
      const hiddenAction = Action.AdminFixSequences;
      expect(ACTION_DATA.get(hiddenAction)!.allowBinding).toBeFalse();

      expect(service.getActionBinding(hiddenAction)).toBeNull();
      expect(() => service.setActionKeyBinding(hiddenAction, binding1)).toThrow(new Error('This action is not permitted to be bound!'));
      expect(service.getActionBinding(hiddenAction)).toBeNull();
    });

    it('should throw an error if there is a conflicting key binding', () => {
      const action2 = Action.AddTag;
      const originalBinding = service.getActionBinding(action2);
      const expectedMessage = `key "${binding1.toLabel()}" is already bound`;

      service.setActionKeyBinding(action, binding1);

      expect(() => service.setActionKeyBinding(action2, binding1)).toThrow(new Error(expectedMessage));
      expect(service.getActionBinding(action2)).toEqual(originalBinding);
    });

    it('should not throw an error if action is already bound to given key binding', () => {
      service.setActionKeyBinding(action, binding1);
      service.setActionKeyBinding(action, binding1);
      expect(service.getActionBinding(action)).toEqual(binding1);
    });

    it('should set all key bindings to their default value', () => {
      const actions = [
        Action.AddTag, // has a default
        Action.AddName, // has a default
        Action.CreateNewGroup, // has no default
        Action.AddGroupToSelected, // has no default
        Action.AdminFixSequences, // cannot be bound
      ];
      const defaults = actions.map(a => ACTION_DATA.get(a)!.defaultBinding);
      const newBindings = [binding1, null, defaults[2], binding2, null];
      const expectedDefaults = [jasmine.any(KeyBinding), jasmine.any(KeyBinding), null, null, null];
      const expectedToFire = [true, true, true, true, false];
      expect(defaults).toEqual(expectedDefaults);

      actions.forEach((a, i) => service.setActionKeyBinding(a, newBindings[i]));

      // Make sure binding changes fire for all keys
      const bindingChangedSubs = actions.map(a =>
        service.getRegisteredAction(a).bindingChanged$.pipe(take(1)).subscribe()
      );

      expect(actions.map(a => service.getActionBinding(a))).toEqual(newBindings);
      expect(bindingChangedSubs.map(s => s.closed)).toEqual([false, false, false, false, false]);

      service.resetAllBindings();

      expect(actions.map(a => service.getActionBinding(a))).toEqual(defaults);
      expect(bindingChangedSubs.map(s => s.closed)).toEqual(expectedToFire);
    });

  });

  describe('event handling', () => {
    const action = Action.ShowFaceMatches;
    const keyData = {keyCode: M, ctrlKey: true, altKey: false, shiftKey: false};
    const keyBinding = KeyBinding.fromEvent(keyData);

    function makeKeydownEvent(data: KeyEventLike): KeyboardEvent {
      const event = new KeyboardEvent('keydown', {...data, bubbles: true});
      spyOn(event, 'stopPropagation').and.callThrough();
      spyOn(event, 'preventDefault').and.callThrough();
      return event;
    }

    it('should respond to a key binding event on the document', () => {
      service.setActionKeyBinding(action, KeyBinding.fromEvent(keyData));
      const sub = service.captureAction(action).pipe(take(1)).subscribe();

      expect(sub.closed).toBeFalse();
      document.dispatchEvent(makeKeydownEvent(keyData));
      expect(sub.closed).toBeTrue();
    });

    it('should not respond to a key binding event when action is disabled', () => {
      service.setActionKeyBinding(action, keyBinding);
      const sub = service.captureAction(action, new BehaviorSubject(false)).pipe(take(1)).subscribe();

      expect(sub.closed).toBeFalse();
      expect(service.isActionEnabled(action)).toBeFalse();
      document.dispatchEvent(makeKeydownEvent(keyData));
      expect(sub.closed).toBeFalse();
    });

    it('should not respond to a contextual key binding event when an input has focus', () => {
      keyBinding.enableDuringInput = false;
      const sub = service.captureAction(action).pipe(take(1)).subscribe();

      service.setActionKeyBinding(action, keyBinding);

      const mockNodeName = spyOnProperty(document.activeElement, 'nodeName' as never);

      mockNodeName.and.returnValue('INPUT');
      document.dispatchEvent(makeKeydownEvent(keyData));
      expect(sub.closed).toBeFalse();

      mockNodeName.and.returnValue('TEXTAREA');
      document.dispatchEvent(makeKeydownEvent(keyData));
      expect(sub.closed).toBeFalse();

      mockNodeName.and.returnValue('DIV');
      document.dispatchEvent(makeKeydownEvent(keyData));
      expect(sub.closed).toBeTrue();
    });

    it('should respond to a non-contextual key binding event when an input has focus', () => {
      keyBinding.enableDuringInput = true;
      const sub = service.captureAction(action).pipe(take(1)).subscribe();

      service.setActionKeyBinding(action, keyBinding);

      spyOnProperty(document.activeElement, 'nodeName' as never).and.returnValue('INPUT');
      document.dispatchEvent(makeKeydownEvent(keyData));
      expect(sub.closed).toBeTrue();
    });

    it('should prevent event propagation when a key binding is pressed', () => {
      service.setActionKeyBinding(action, keyBinding);
      const sub = service.captureAction(action).pipe(take(1)).subscribe();

      const event = makeKeydownEvent(keyData);

      // This is a valid key event and therefore its propagation should be prevented
      document.dispatchEvent(event);
      expect(event.stopPropagation).toHaveBeenCalledTimes(1);
      expect(event.preventDefault).toHaveBeenCalledTimes(1);
    });

    it('should prevent event propagation when a disabled key binding is pressed', () => {
      service.setActionKeyBinding(action, keyBinding);
      const sub = service.captureAction(action, new BehaviorSubject(false)).pipe(take(1)).subscribe();

      const event = makeKeydownEvent(keyData);

      // This is a valid key event and therefore its propagation should be prevented
      document.dispatchEvent(event);
      expect(service.isActionEnabled(action)).toBeFalse();
      expect(event.stopPropagation).toHaveBeenCalledTimes(1);
      expect(event.preventDefault).toHaveBeenCalledTimes(1);
    });

    it('should not prevent event propagation when a non-hotkey is pressed', () => {
      const event = makeKeydownEvent(keyData);
      // This is a valid key event and therefore its propagation should be prevented
      document.dispatchEvent(event);
      expect(event.stopPropagation).not.toHaveBeenCalled();
      expect(event.preventDefault).not.toHaveBeenCalled();
    });

    it('should not prevent event propagation when event is blocked due to input focus', () => {
      keyBinding.enableDuringInput = false;

      const sub = service.captureAction(action).pipe(take(1)).subscribe();
      service.setActionKeyBinding(action, keyBinding);

      const event = makeKeydownEvent(keyData);
      const mockNodeName = spyOnProperty(document.activeElement, 'nodeName' as never);

      mockNodeName.and.returnValue('INPUT');
      document.dispatchEvent(event);
      expect(event.stopPropagation).not.toHaveBeenCalled();
      expect(event.preventDefault).not.toHaveBeenCalled();

      mockNodeName.and.returnValue('DIV');
      document.dispatchEvent(event);
      expect(event.stopPropagation).toHaveBeenCalledTimes(1);
      expect(event.preventDefault).toHaveBeenCalledTimes(1);
    });

    it('should not respond to a key binding when the input is blocked', () => {
      const sub = service.captureAction(action).pipe(take(1)).subscribe();
      service.setActionKeyBinding(action, keyBinding);

      const event = makeKeydownEvent(keyData);

      service.blockEventHandling();
      document.dispatchEvent(event);
      expect(sub.closed).toBeFalse();
      expect(event.stopPropagation).not.toHaveBeenCalled();
      expect(event.preventDefault).not.toHaveBeenCalled();

      service.unblockEventHandling();
      document.dispatchEvent(event);
      expect(sub.closed).toBeTrue();
      expect(event.stopPropagation).toHaveBeenCalledTimes(1);
      expect(event.preventDefault).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when input blocking is done in the wrong state', () => {
      service.blockEventHandling();

      expect(() => service.blockEventHandling()).toThrow(new Error('Already blocking event handling!'));

      service.unblockEventHandling();

      expect(() => service.unblockEventHandling()).toThrow(new Error('Event handling not blocked!'));
    });

  });

});
