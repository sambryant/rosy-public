import {take} from 'rxjs/operators';
import {HttpClientTestingModule, HttpTestingController, TestRequest, } from '@angular/common/http/testing';
import {HttpParams, HttpRequest, } from '@angular/common/http';
import {TestBed} from '@angular/core/testing';

import {
  ApiService,
  GetMetadataResponse,
  QueryImagesResponse,
  ServicesModule,
  UpdateImageResponse,
} from '.';
import {
  Artist,
  ArtistQuery,
  DeletedQuery,
  EigendataQuery,
  Group,
  GroupQuery,
  Image,
  ImageQuery,
  NameQuery,
  TagQuery,
} from '@app/models';
import {MockArtist, MockGroup, MockImage, MockName, MockTag, } from '@app/models/testing';


describe('ApiService', () => {
  let httpMock: HttpTestingController;
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ServicesModule
      ],
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  function expectOneRequest(expectedUrl: string, expectedMethod: string, expectedBody: any): TestRequest {
    const req = httpMock.expectOne(expectedUrl);
    expect(req.request.method).toBe(expectedMethod);
    expect(req.request.body).toEqual(expectedBody);
    return req;
  }

  function checkReqSimple(observable, expectedUrl: string, expectedMethod: string, expectedBody: any): HttpRequest<any> {
    const fakeResponse = {generic: 'response'};
    const sub = observable.pipe(take(1)).subscribe(response => {
      expect(response).toEqual({generic: 'response'});
    });
    const req = expectOneRequest(expectedUrl, expectedMethod, expectedBody);
    expect(sub.closed).toBeFalse();
    req.flush(fakeResponse);
    expect(sub.closed).toBeTrue();
    return req.request;
  }

  describe('content links', () => {

    it('should call getThumbnailBlock correctly', () => {
      const fakeImage = new MockImage(1, 'file1.jpg');
      fakeImage.thumbnail = 'thumb1.jpg';
      const expUrl = 'thumbnails/thumb1.jpg';
      const expMethod = 'GET';
      const expBody = null;

      service.getThumbnailBlob(fakeImage).subscribe();
      expectOneRequest(expUrl, expMethod, expBody);
    });

    it('should return an observable from the server', () => {
      const fakeImage = new MockImage(1, 'file1.jpg');
      fakeImage.filename = 'image1.jpg';
      const expUrl = 'images/image1.jpg';
      const expMethod = 'GET';
      const expBody = null;

      service.getImageBlob(fakeImage).subscribe();
      expectOneRequest(expUrl, expMethod, expBody);
    });
  });

  describe('authentication', () => {

    it('should call postLogin correctly', () => {
      const expUrl = 'auth/login';
      const expMethod = 'POST';
      const expBody = {
        username: 'test-user',
        password: 'hog-password'
      };

      checkReqSimple(service.postLogin('test-user', 'hog-password'), expUrl, expMethod, expBody);
    });

  });

  describe('admin endpoints', () => {
    it('should call fixSequences correctly', () => {
      const expUrl = 'api/admin/fix_sequences';
      const expMethod = 'PUT';
      const expBody = {};

      checkReqSimple(service.fixSequences(), expUrl, expMethod, expBody);
    });

    it('should call generateDupeReport correctly', () => {
      const expUrl = 'api/admin/generate_dupe_report';
      const expMethod = 'POST';
      const expBody = {};

      checkReqSimple(service.generateDupeReport(), expUrl, expMethod, expBody);
    });

    it('should call computeMetadata correctly', () => {
      const expUrl = 'api/admin/compute_metadata';
      const expMethod = 'PUT';
      const expBody = {};

      checkReqSimple(service.computeMetadata(), expUrl, expMethod, expBody);
    });

    it('should call mergeDuplicates correctly', () => {
      const expUrl = 'api/admin/merge_duplicates';
      const expMethod = 'PUT';
      const expBody = {};

      checkReqSimple(service.mergeDuplicates(), expUrl, expMethod, expBody);
    });

    it('should call regenerateThumbnails correctly', () => {
      const expUrl = 'api/admin/regenerate_thumbnails';
      const expMethod = 'PUT';
      const expBody = {};

      checkReqSimple(service.regenerateThumbnails(), expUrl, expMethod, expBody);
    });

    it('should call reindexImages with exlude true', () => {
      const expUrl = 'api/admin/reindex_images';
      const expMethod = 'PUT';
      const expBody = {excludeExisting: true};
      checkReqSimple(service.reindexImages(true), expUrl, expMethod, expBody);
    });

    it('should call reindexImages with exlude false', () => {
      const expUrl = 'api/admin/reindex_images';
      const expMethod = 'PUT';
      const expBody = {excludeExisting: false};
      checkReqSimple(service.reindexImages(false), expUrl, expMethod, expBody);
    });

    it('should call resetFiles correctly', () => {
      const expUrl = 'api/admin/reset_files';
      const expMethod = 'PUT';
      const expBody = {};

      checkReqSimple(service.resetFiles(), expUrl, expMethod, expBody);
    });

    it('should call getThreadStatus correctly', () => {
      const threadId = 1234;
      const expUrl = 'api/admin/thread_status/1234';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.getThreadStatus(threadId), expUrl, expMethod, expBody);
    });

  });

  describe('artist endpoints', () => {

    it('should call createArtist correctly', () => {
      const fakeValue = 'test value';
      const fakeImages = [ MockImage.new(1), MockImage.new(3) ];
      const expUrl = 'api/artist';
      const expMethod = 'POST';
      const expBody = {
        value: 'test value',
        imageIds: [1, 3]
      };

      checkReqSimple(service.createArtist(fakeValue, fakeImages), expUrl, expMethod, expBody);
    });

    it('should call deleteArtist correctly', () => {
      const fakeId = 42;
      const expUrl = 'api/artist/42';
      const expMethod = 'DELETE';
      const expBody = null;

      checkReqSimple(service.deleteArtist(fakeId), expUrl, expMethod, expBody);
    });

    it('should call updateArtist correctly', () => {
      const fakeObj = MockArtist.new(42);
      const expUrl = 'api/artist';
      const expMethod = 'PUT';
      const expBody = MockArtist.new(42);

      checkReqSimple(service.updateArtist(fakeObj), expUrl, expMethod, expBody);
    });

  });

  describe('group endpoints', () => {

    it('should call createGroup correctly', () => {
      const fakeTitle = 'test value';
      const fakeImages = [ MockImage.new(1), MockImage.new(3) ];
      const expUrl = 'api/group';
      const expMethod = 'POST';
      const expBody = {
        value: 'test value',
        imageIds: [1, 3]
      };

      checkReqSimple(service.createGroup(fakeTitle, fakeImages), expUrl, expMethod, expBody);
    });

    it('should call deleteGroup correctly', () => {
      const fakeId = 42;
      const expUrl = 'api/group/42';
      const expMethod = 'DELETE';
      const expBody = null;

      checkReqSimple(service.deleteGroup(fakeId), expUrl, expMethod, expBody);
    });

    it('should call updateGroup correctly', () => {
      const fakeObj = MockGroup.new(42);
      const expUrl = 'api/group';
      const expMethod = 'PUT';
      const expBody = MockGroup.new(42);

      checkReqSimple(service.updateGroup(fakeObj), expUrl, expMethod, expBody);
    });

  });

  describe('image endpoints', () => {

    it('should call deleteImage correctly', () => {
      const fakeId = 42;
      const expUrl = 'api/image/42';
      const expMethod = 'DELETE';
      const expBody = null;

      checkReqSimple(service.deleteImage(fakeId), expUrl, expMethod, expBody);
    });

    it('should call getImage correctly', () => {
      const fakeId = 42;
      const expUrl = 'api/image/42';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.getImage(fakeId), expUrl, expMethod, expBody);
    });

    it('should call markDupes correctly', () => {
      const fakeIds = [1, 2, 42];
      const fakeDupeType = 'Exact';
      const expUrl = 'api/image/mark_dupes';
      const expMethod = 'PUT';
      const expBody = { imageIds: fakeIds, dupeType: fakeDupeType };

      checkReqSimple(service.markDupes(fakeIds, fakeDupeType), expUrl, expMethod, expBody);
    });

    it('should call permadelete correctly', () => {
      const fakeId = 1;
      const expUrl = 'api/image/permadelete/1';
      const expMethod = 'DELETE';

      checkReqSimple(service.permadeleteImage(fakeId), expUrl, expMethod, null);
    });

    it('should call rateImage with clearExisting true', () => {
      const fakeId = 42;
      const fakeRating = 2.0;
      const expUrl = 'api/image/rate';
      const expMethod = 'POST';
      const expBody = { imageId: fakeId, rating: fakeRating, clearExisting: true }

      checkReqSimple(service.rateImage(fakeId, fakeRating, true), expUrl, expMethod, expBody);
    });

    it('should call rateImage with clearExisting false', () => {
      const fakeId = 42;
      const fakeRating = 2.0;
      const expUrl = 'api/image/rate';
      const expMethod = 'POST';
      const expBody = { imageId: fakeId, rating: fakeRating, clearExisting: false }

      checkReqSimple(service.rateImage(fakeId, fakeRating, false), expUrl, expMethod, expBody);
    });

    it('should call restore image correctly', () => {
      const fakeId = 1;
      const expUrl = 'api/image/restore/1';
      const expMethod = 'PUT';

      checkReqSimple(service.restoreImage(fakeId), expUrl, expMethod, {});
    });

    it('should call unmarkDupes correctly', () => {
      const fakeIds = [1, 2, 42];
      const expUrl = 'api/image/unmark_dupes';
      const expMethod = 'PUT';
      const expBody = { imageIds: fakeIds };

      checkReqSimple(service.unmarkDupes(fakeIds), expUrl, expMethod, expBody);
    });

    it('should call updateImage correctly', () => {
      const fakeObj = MockImage.new(42);
      const expUrl = 'api/image';
      const expMethod = 'PUT';
      const expBody = MockImage.new(42);

      checkReqSimple(service.updateImage(fakeObj), expUrl, expMethod, expBody);
    });

    it('should call uploadImage correctly', () => {
      const fakeFile = {fake: 'File'} as any as File;
      const expUrl = 'api/image';
      const expMethod = 'POST';
      const expBody = { fake: 'File' };

      service.uploadImage(fakeFile).subscribe();
      expectOneRequest(expUrl, expMethod, expBody);
    });

  });

  describe('metadata endpoints', () => {

    it('should call getMetadata correctly', () => {
      const expUrl = 'api/metadata';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.getMetadata(), expUrl, expMethod, expBody);
    });

  });

  describe('name endpoints', () => {

    it('should call createName correctly', () => {
      const fakeValue = 'test name';
      const fakeImages = [ MockImage.new(1), MockImage.new(3) ];
      const expUrl = 'api/name';
      const expMethod = 'POST';
      const expBody = {
        value: 'test name',
        imageIds: [1, 3]
      };

      checkReqSimple(service.createName(fakeValue, fakeImages), expUrl, expMethod, expBody);
    });

    it('should call deleteName correctly', () => {
      const fakeId = 42;
      const expUrl = 'api/name/42';
      const expMethod = 'DELETE';
      const expBody = null;

      checkReqSimple(service.deleteName(fakeId), expUrl, expMethod, expBody);
    });

    it('should call updateName correctly', () => {
      const fakeObj = MockName.new(42);
      const expUrl = 'api/name';
      const expMethod = 'PUT';
      const expBody = MockName.new(42);

      checkReqSimple(service.updateName(fakeObj), expUrl, expMethod, expBody);
    });

  });

  describe('query endpoints', () => {

    it('should call queryArtists correctly', () => {
      const fakeQuery = new ArtistQuery();
      spyOn(fakeQuery, 'toParams').and.returnValue({param1: 'hi', param2: 'cool'});

      const expUrl = 'api/query/artists?param1=hi&param2=cool';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryArtists(fakeQuery), expUrl, expMethod, expBody);
    });

    it('should call queryDeleted correctly', () => {
      const fakeQuery = new DeletedQuery();
      spyOn(fakeQuery, 'toParams').and.returnValue({param1: 'hi', param2: 'cool'});

      const expUrl = 'api/query/deleted?param1=hi&param2=cool';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryDeleted(fakeQuery), expUrl, expMethod, expBody);
    });

    it('should call queryEigendata correctly', () => {
      const fakeQuery = new EigendataQuery();
      spyOn(fakeQuery, 'toParams').and.returnValue({param1: 'hi', param2: 'cool'});

      const expUrl = 'api/query/eigendata?param1=hi&param2=cool';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryEigendata(fakeQuery), expUrl, expMethod, expBody);
    });

    it('should call queryFaceMatches correctly', () => {
      const fakeImage = MockImage.new(42);
      const expUrl = 'api/query/face_matches/42';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryFaceMatches(fakeImage), expUrl, expMethod, expBody);
    });

    it('should call queryGroups correctly', () => {
      const fakeQuery = new GroupQuery(14, 3);
      spyOn(fakeQuery, 'toParams').and.returnValue({param1: 'hi', param2: 'cool'});

      const expUrl = 'api/query/groups?param1=hi&param2=cool';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryGroups(fakeQuery), expUrl, expMethod, expBody);
    });

    it('should call queryImages correctly', () => {
      const fakeQuery = new ImageQuery(14, 3);
      spyOn(fakeQuery, 'toParams').and.returnValue({param1: 'hi', param2: 'cool'});

      const expUrl = 'api/query/images?param1=hi&param2=cool';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryImages(fakeQuery), expUrl, expMethod, expBody);
    });

    it('should call queryMarkedDupes correctly', () => {
      const fakeImage = MockImage.new(42);
      const expUrl = 'api/query/marked_dupes/42';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryMarkedDupes(fakeImage), expUrl, expMethod, expBody);
    });

    it('should call queryNames correctly', () => {
      const fakeQuery = new NameQuery();
      spyOn(fakeQuery, 'toParams').and.returnValue({param1: 'hi', param2: 'cool'});

      const expUrl = 'api/query/names?param1=hi&param2=cool';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryNames(fakeQuery), expUrl, expMethod, expBody);
    });

    it('should call queryTags correctly', () => {
      const fakeQuery = new TagQuery();
      spyOn(fakeQuery, 'toParams').and.returnValue({param1: 'hi', param2: 'cool'});

      const expUrl = 'api/query/tags?param1=hi&param2=cool';
      const expMethod = 'GET';
      const expBody = null;

      checkReqSimple(service.queryTags(fakeQuery), expUrl, expMethod, expBody);
    });

  });

  describe('tag endpoints', () => {

    it('should call createTag correctly', () => {
      const fakeValue = 'test tag';
      const fakeImages = [ MockImage.new(1), MockImage.new(3) ];
      const expUrl = 'api/tag';
      const expMethod = 'POST';
      const expBody = {
        value: 'test tag',
        imageIds: [1, 3]
      };

      checkReqSimple(service.createTag(fakeValue, fakeImages), expUrl, expMethod, expBody);
    });

    it('should call deleteTag correctly', () => {
      const fakeId = 42;
      const expUrl = 'api/tag/42';
      const expMethod = 'DELETE';
      const expBody = null;

      checkReqSimple(service.deleteTag(fakeId), expUrl, expMethod, expBody);
    });

    it('should call updateTag correctly', () => {
      const fakeObj = MockTag.new(42);
      const expUrl = 'api/tag';
      const expMethod = 'PUT';
      const expBody = MockTag.new(42);

      checkReqSimple(service.updateTag(fakeObj), expUrl, expMethod, expBody);
    });

  });
});
