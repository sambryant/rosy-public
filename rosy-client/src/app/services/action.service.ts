import {BehaviorSubject, Observable, Subject, } from 'rxjs';
import {DOCUMENT} from '@angular/common';
import {EventManager} from '@angular/platform-browser';
import {Injectable, Inject, } from '@angular/core';

import {
  Action,
  ActionData,
  ACTION_DATA,
  KeyBinding,
  KeyBindingHash,
  KeyEventLike,
} from '@app/models';
import {safeUnsubscribeAll} from '@app/shared/utility';


export class RegisteredAction {
  action: Action;
  data: ActionData;
  captured = false;
  binding: KeyBinding | null = null;
  readonly bindingChanged$: Subject<RegisteredAction> = new Subject();
  readonly enabled$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  readonly activated$: Subject<Event | null> = new Subject();
  readonly enabledPredicates: BehaviorSubject<boolean>[] = [];

  constructor(action: Action, data: ActionData) {
    this.action = action;
    this.data = data;
  }
}


@Injectable({
  providedIn: 'root'
})
export class ActionService {

  private registeredActions: Map<Action, RegisteredAction> = new Map();
  private registeredBindings: Map<KeyBindingHash, [KeyBinding, Action]> = new Map();
  private handlingBlocked = false;
  private unregisterListener: Function | null = null;

  constructor(
    private eventManager: EventManager,
    @Inject(DOCUMENT) private document: Document)
  {
    // Register all the actions and initialize their key bindings to the defaults
    ACTION_DATA.forEach((data, action) => {
      const entry = new RegisteredAction(action, data);
      this.registeredActions.set(action, entry);
      if (entry.data.allowBinding) {
        this.setActionKeyBinding(action, entry.data.defaultBinding);
      }
    });

    // Main keybinding listener that routes to proper action if registered.
    this.unregisterListener = this.eventManager.addGlobalEventListener('document', 'keydown', (event) => {
      if (!this.handlingBlocked) {
        const hash = KeyBinding.keyToHash(event);

        const bind = this.registeredBindings.get(hash);
        if (bind) {
          const [binding, action] = bind;

          // Ignore event if an input has focus and the key binding is marked as disabled for inputs.
          if (!binding.enableDuringInput && this.inputElementHasFocus()) {
            return;
          }

          this.doAction(action);
          event.stopPropagation();
          event.preventDefault();
        }
      }
    });
  }

  private inputElementHasFocus(): boolean {
    if (this.document.activeElement && this.document.activeElement.nodeName) {
      return this.document.activeElement.nodeName === 'INPUT' || this.document.activeElement.nodeName === 'TEXTAREA';
    }
    return false;
  }

  public getRegisteredAction(action: Action): RegisteredAction {
    const a = this.registeredActions.get(action);
    if (a === undefined) {
      throw new Error(`Unregistered action ${action}`);
    } else {
      return a;
    }
  }

  public isActionEnabled(action: Action): boolean {
    return this.getRegisteredAction(action).enabled$.value;
  }

  public captureAction(action: Action, enabled: BehaviorSubject<boolean> | null = null): Observable<Event | null> {
    return new Observable(observer => {
      const entry = this.getRegisteredAction(action);
      if (entry.captured) {
        observer.error(`Action "${entry.data.description}" is already captured`);
        return () => {};
      }

      entry.captured = true;
      const listeningSub = entry.activated$.subscribe(observer);
      const conditionSub = this.addActionCondition(action, enabled).subscribe();

      return () => {
        entry.captured = false;
        safeUnsubscribeAll([listeningSub, conditionSub]);
      };
    });
  }

  protected recomputeEnabled(entry: RegisteredAction) {
    const predicates = entry.enabledPredicates;
    const enabledValue = entry.captured && entry.enabledPredicates.every(pred => pred.value);

    if (enabledValue !== entry.enabled$.value) {
      entry.enabled$.next(enabledValue);
    }
  }

  public addActionCondition(action: Action, condition: BehaviorSubject<boolean> | null): Observable<any> {
    return new Observable(observer => {
      const entry = this.getRegisteredAction(action);
      // When passed null, it will defer to other conditions (default is true)
      if (condition === null) {
        // Need to recompute the enabled state now and also when unsubscribed.
        this.recomputeEnabled(entry);
        return () => {
          this.recomputeEnabled(entry);
        };
      } else {
        entry.enabledPredicates.push(condition);

        // Every time any condition updates, we set `enabled` to be conjunction of all conditions.
        const conditionSub = condition.subscribe(_ => this.recomputeEnabled(entry));

        return () => {
          const ind = entry.enabledPredicates.indexOf(condition);
          entry.enabledPredicates.splice(ind, 1);
          conditionSub.unsubscribe();

          // By default, without any conditions, an action is disabled
          this.recomputeEnabled(entry);
        };
      }
    });
  }

  public getBoundAction(keyEvent: KeyEventLike): Action | null {
    const hash = KeyBinding.keyToHash(keyEvent);
    const bind = this.registeredBindings.get(hash);
    if (bind) {
      return bind[1];
    } else {
      return null;
    }
  }

  public getActionBinding(action: Action): KeyBinding | null {
    return this.getRegisteredAction(action).binding;
  }

  /**
   * Removes key binding for the given action without firing the action's `bindingChanged$`.
   *
   * Returns true if the action previously had a non-null binding.
   */
  private unsetActionKeyBinding(entry: RegisteredAction): boolean {
    if (entry.binding !== null) {
      this.registeredBindings.delete(entry.binding.toHash());
      entry.binding = null;
      return true;
    } else {
      return false;
    }
  }

  /**
   * Sets key binding for the given action without firing the action's `bindingChanged$`.
   *
   * Returns true if the original binding changed.
   */
  private setActionKeyBindingInner(entry: RegisteredAction, keyBinding: KeyBinding | null) {
    if (keyBinding === null) {
      return this.unsetActionKeyBinding(entry);
    } else if (!entry.data.allowBinding) {
      throw new Error('This action is not permitted to be bound!');
    } else {
      const hash = keyBinding.toHash();

      const existing = this.registeredBindings.get(hash);
      if (existing && existing[1] === entry.action) {
        return false;
      } else if (existing) {
        throw new Error(`key "${keyBinding.toLabel()}" is already bound`);
      }
      this.unsetActionKeyBinding(entry);
      this.registeredBindings.set(hash, [keyBinding, entry.action]);
      entry.binding = keyBinding;

      return true;
    }
  }

  /**
   * Sets key binding for the given action and fires the action's `bindingChanged$`.
   */
  public setActionKeyBinding(action: Action, keyBinding: KeyBinding | null) {
    const entry = this.getRegisteredAction(action);
    if (this.setActionKeyBindingInner(entry, keyBinding)) {
      entry.bindingChanged$.next(entry);
    }
  }

  /**
   * Sets the key binding for all actions to their default.
   */
  public resetAllBindings() {
    this.registeredBindings.clear();
    this.registeredActions.forEach((entry) => {
      entry.binding = null;
    });
    this.registeredActions.forEach((entry, action) => {
      this.setActionKeyBindingInner(entry, entry.data.defaultBinding);
      if (entry.data.allowBinding) {
        entry.bindingChanged$.next(entry);
      }
    });
  }

  public doAction(action: Action, event: Event | null = null) {
    const entry = this.getRegisteredAction(action);
    if (entry.enabled$.value) {
      entry.activated$.next(event);
    }
  }

  public blockEventHandling() {
    if (this.handlingBlocked) {
      throw new Error('Already blocking event handling!');
    }
    this.handlingBlocked = true;
  }

  public unblockEventHandling() {
    if (!this.handlingBlocked) {
      throw new Error('Event handling not blocked!');
    }
    this.handlingBlocked = false;
  }

  /**
   * For use in tests, the event listener added to the `EventManager` persists between unit tests.
   */
  public testRemoveEventListener() {
    if (this.unregisterListener) {
      this.unregisterListener();
    }
  }

}
