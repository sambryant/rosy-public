import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  error(...args) {
    console.error(args);
  }

  warn(...args) {
    console.warn(args);
  }

}
