import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {ConfigService} from './config.service';
import {SCHEMA_HEADER_KEY} from '@app/app.constants';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private configService: ConfigService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const idToken = localStorage.getItem('id_token');
    const dbSchema = this.configService.databaseSchema;

    if (idToken || dbSchema) {
      const headers: any = {};
      if (idToken) {
        headers.Authorization = 'JWT ' + idToken;
      }
      if (dbSchema) {
        headers[SCHEMA_HEADER_KEY] = dbSchema;
      }
      const modified = req.clone({
        setHeaders: headers
      });
      return next.handle(modified);
    } else {
      return next.handle(req);
    }
  }
}
