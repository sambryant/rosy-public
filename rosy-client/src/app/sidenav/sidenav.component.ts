import {Component} from '@angular/core';

import {AppPage, APP_PAGES, SORTED_NAV_ROUTES, } from '@app/app-page.model';


@Component({
  selector: 'rx-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {

  _routes: AppPage[][] = SORTED_NAV_ROUTES;

}
