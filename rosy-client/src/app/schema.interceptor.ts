import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {SCHEMA_HEADER_KEY} from '@app/app.constants';
import {ConfigService} from '@app/services/config.service';

@Injectable()
export class SchemaInterceptor implements HttpInterceptor {

  constructor(public configService: ConfigService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.configService.databaseSchema) {
      const cloned = req.clone({
        headers: req.headers.set(SCHEMA_HEADER_KEY, this.configService.databaseSchema)
      });
      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}
