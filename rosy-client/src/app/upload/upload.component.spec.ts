import {
  HttpEvent,
  HttpProgressEvent,
  HttpEventType,
  HttpResponse,
} from '@angular/common/http';
import {of, Subject} from 'rxjs';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ApiService, NotificationService, } from '@app/services';

import {MAX_CONCURRENT_JOBS, UploadComponent, } from '.';


function updateDone(): HttpResponse<any> {
  return {
    type: HttpEventType.Response,
    ok: true
  } as unknown as HttpResponse<any>;
}

function updateProgress(loaded: number, total: number): HttpResponse<any> {
  return {
    type: HttpEventType.UploadProgress,
    loaded,
    total
  } as unknown as HttpResponse<any>;
}


describe('UploadComponent', () => {
  let fixture: ComponentFixture<UploadComponent>;
  let c: UploadComponent;
  let uploadSpy;
  let notifyErrorSpy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        UploadComponent
      ],
      imports: [
        NoopAnimationsModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
    const api = TestBed.inject(ApiService);
    uploadSpy = spyOn(api, 'uploadImage');
    notifyErrorSpy = spyOn(TestBed.inject(NotificationService), 'error');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadComponent);
    c = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should upload a single file', () => {
    const mockFiles: any = [{name: 'file 1.jpg'}];
    const mockResponse = {
      type: HttpEventType.Response,
      ok: true
    };
    uploadSpy.and.returnValue(of(mockResponse));

    c.onUploadChange({files: mockFiles} as any);

    expect(uploadSpy).toHaveBeenCalledTimes(1);
    expect(uploadSpy).toHaveBeenCalledWith(mockFiles[0]);
  });

  it('should update job list when finished', () => {
    const mockFiles: any = [{name: 'file 1.jpg'}];
    const ctrl = new Subject<any>();
    const mockResponse = {
      type: HttpEventType.Response,
      ok: true
    };
    const expectedJobs: any = [
      {file: mockFiles[0], state: 'current', message: 'okay', progress: 0.0},
      {file: mockFiles[0], state: 'success', message: 'okay', progress: 1.0},
    ];
    uploadSpy.and.returnValue(ctrl);

    c.onUploadChange({files: mockFiles} as any);
    expect(c.uploadJobs.length).toBe(1);
    expect(c.uploadJobs[0]).toEqual(expectedJobs[0]);

    ctrl.next(mockResponse);

    expect(c.uploadJobs.length).toBe(1);
    expect(c.uploadJobs[0]).toEqual(expectedJobs[1]);
  });

  it('should update progress of a job as it updates', () => {
    const mockFiles: any = [{name: 'file 1.jpg'}];
    const mockResponses = [
      updateProgress(33, 100),
      updateProgress(66, 100),
      updateDone()
    ];
    const expectedJobs: any = [
      {file: mockFiles[0], state: 'current', message: 'okay', progress: 0.0},
      {file: mockFiles[0], state: 'current', message: 'okay', progress: (33 / 100) * 0.8},
      {file: mockFiles[0], state: 'current', message: 'okay', progress: (66 / 100) * 0.8},
      {file: mockFiles[0], state: 'success', message: 'okay', progress: 1.0},
    ];
    const ctrl = new Subject<HttpEvent<any>>();
    uploadSpy.and.returnValue(ctrl);

    c.onUploadChange({files: mockFiles} as any);

    expect(c.uploadJobs).toEqual([expectedJobs[0]]);

    for (let i = 0; i < 3; i++) {
      ctrl.next(mockResponses[i]);
      expect(c.uploadJobs).toEqual([expectedJobs[i + 1]]);
    }
  });

  it('should limit number of concurrent jobs', () => {
    const mockFiles: any = [
      {name: '1.jpg'},
      {name: '2.jpg'},
      {name: '3.jpg'},
      {name: '4.jpg'},
      {name: '5.jpg'},
      {name: '6.jpg'}
    ];
    expect(MAX_CONCURRENT_JOBS).toBeLessThan(mockFiles.length);

    // Setup separate controller for each file upload
    const ctrlMap = {};
    mockFiles.forEach(f => {
      ctrlMap[f.name] = new Subject<HttpResponse<any>>();
    });
    uploadSpy.and.callFake(f => {
      return ctrlMap[f.name];
    });

    c.onUploadChange({files: mockFiles} as any);

    let running = c.uploadJobs.filter(j => j.state === 'current').map(j => j.file.name);
    expect(running).toEqual(['1.jpg', '2.jpg', '3.jpg']);

    ctrlMap['2.jpg'].next(updateDone());

    running = c.uploadJobs.filter(j => j.state === 'current').map(j => j.file.name);
    expect(running).toEqual(['1.jpg', '3.jpg', '4.jpg']);

    ctrlMap['4.jpg'].error({});

    running = c.uploadJobs.filter(j => j.state === 'current').map(j => j.file.name);
    expect(running).toEqual(['1.jpg', '3.jpg', '5.jpg']);
  });

  it('should handle a failed job', () => {
    const mockFiles: any = [{name: '1.jpg'}];
    const mockError = {
      error: {
        message: 'test error message'
      }
    };
    const expectedJob: any = {
      message: 'Upload failed - test error message',
      progress: 0.0,
      state: 'failure',
      file: mockFiles[0]
    };
    const ctrl = new Subject<any>();
    uploadSpy.and.returnValue(ctrl);

    c.onUploadChange({files: mockFiles} as any);
    ctrl.next(updateProgress(33, 100));
    ctrl.error(mockError);
    expect(c.uploadJobs).toEqual([expectedJob]);
    expect(notifyErrorSpy).toHaveBeenCalledTimes(1);
    expect(notifyErrorSpy).toHaveBeenCalledWith(expectedJob.message);
  });
});
