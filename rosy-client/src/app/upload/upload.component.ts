import {
  EventEmitter,
  Component,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  HttpEvent,
  HttpProgressEvent,
  HttpEventType,
  HttpResponse,
} from '@angular/common/http';
import {catchError, takeUntil, } from 'rxjs/operators';
import {FormGroup, FormControl, FormBuilder, Validators, } from '@angular/forms';
import {Subject} from 'rxjs';

import {ApiService, NotificationService, } from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


export type JobState = 'pending' | 'current' | 'success' | 'failure';

export interface Job {
  file: File;
  state: JobState;
  progress: number;
  message: string;
}

export const MAX_CONCURRENT_JOBS = 3;


@Component({
  selector: 'rx-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent
extends UnsubscribeOnDestroyedDirective {

  uploadJobs: Job[] = [];

  private nextPendingIndex = 0;

  private currentJobCount = 0;

  constructor(private apiService: ApiService, private notificationService: NotificationService)
  {
    super();
  }

  onUploadChange(target: HTMLInputElement): void {
    const files = target.files || [];
    for (let index = 0; index < files.length; index++) {
      const job: Job = {
        file: files[index],
        state: 'pending',
        progress: 0.0,
        message: 'okay'
      };
      this.uploadJobs.push(job);
    }
    this.processJobs();
  }

  private hasPendingJobs(): boolean {
    return this.nextPendingIndex < this.uploadJobs.length;
  }

  private processJobs(): void {
    while (this.hasPendingJobs() && this.currentJobCount < MAX_CONCURRENT_JOBS) {
      const nextJob = this.uploadJobs[this.nextPendingIndex];
      this.nextPendingIndex += 1;
      this.startJob(nextJob);
    }
  }

  private startJob(job: Job): void {
    job.state = 'current';
    this.currentJobCount += 1;
    this.apiService.uploadImage(job.file)
      .pipe(takeUntil(this.destroyed$))
      .subscribe({
        next: event => this.onHttpEvent(job, event),
        error: err => this.onHttpError(job, err)
      });
  }

  private onHttpEvent(job: Job, event: HttpEvent<any>): void {
    if (event.type === HttpEventType.UploadProgress) {
      // Dont fill up progress bar even when file finishes uploading because we
      // still need to wait for the server to process the upload before the
      // import is complete.
      const progress = !!event.total ? (event.loaded / event.total) : 0.0;
      job.progress = 0.8 * progress;
    }
    else if (event.type === HttpEventType.Response) {
      if (event.ok) {
        job.progress = 1.0;
        job.state = 'success';
        this.currentJobCount -= 1;
        this.processJobs();
      } else {
        // Note: I dont think this can ever happen, but just in case
        this.onHttpError(job, event);
      }
    }
  }

  private onHttpError(job: Job, err: any): void {
    const msg = 'Upload failed - ' + (err.error && err.error.message || 'unknown error');
    job.progress = 0.0;
    job.state = 'failure';
    job.message = msg;
    this.currentJobCount -= 1;
    this.notificationService.error(msg);
    this.processJobs();
  }

}
