import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgModule} from '@angular/core';

import {ServicesModule} from '@app/services';

import {UploadComponent} from './upload.component';


@NgModule({
  declarations: [
    UploadComponent
  ],
  exports: [
    UploadComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    ServicesModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatTooltipModule
  ]
})
export class UploadModule { }
