import {Observer} from 'rxjs';

/**
 * Utility testing class typically used to check output of `BehaviorSubject` objects.
 *
 * Usage example:
 *
 *     let subject = new BehaviorSubject<int>(1);
 *     let observer = new ExpectObserver([1,2,3]);
 *     let sub = subject.subscribe(observer);
 *     subject.next(2);
 *     subject.next(3);
 *     subject.complete();
 *     expect(sub.closed).toBeTrue();
 */
export class ExpectObserver<T> implements Observer<T>{

  /**
   * NOTE: you cannot check this number directly in tests. When you begin a new subscription, a
   * seperate instance of this is created so you will always find `notifyCount` to be 0. As far as I
   * can tell, there is no obvious way to access these seperate instances. I haven't seen this
   * documented anywhere, I've only deduced this from tests.
   *
   * Instead, you should `complete` the observable that this is subscribed to which will
   * automatically verify that the correct calls were made.
   *
   * Bad Example (DONT DO THIS):
   *
   *     let subject = new BehaviorSubject<int>(1);
   *     let observer = new ExpectObserver([1,2,3]);
   *     subject.subscribe(observer);
   *     subject.next(2);
   *     subject.next(3);
   *     expect(observer.notifyCount).toBe(3); // THIS WILL ALWAYS FAIL
   *
   * Good Example:
   *
   *     let subject = new BehaviorSubject<int>(1);
   *     let obs = new ExpectObserver([1,2,3]);
   *     let sub = subject.subscribe(obs);
   *     subject.next(2);
   *     subject.next(3);
   *     subject.complete();
   *     expect(sub.closed).toBeTrue();
   */
  private notifyCount: number;

  constructor(private expected: T[]) {
    this.notifyCount = 0;
  }

  next(object: T) {
    expect(object).toEqual(this.expected[this.notifyCount]);
    this.notifyCount = this.notifyCount + 1;
  }

  error(a) {
    throw Error();
  }

  complete() {
    if (this.notifyCount !== this.expected.length) {
      throw new Error('Observer expected ' + this.expected.length + ' values but only received ' + this.notifyCount);
    }
  }
}
