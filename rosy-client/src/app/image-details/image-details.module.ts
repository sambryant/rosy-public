import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ContentEditorModule} from '@app/shared/content-editor';
import {ImageDetailsComponent} from './image-details.component';
import {ServicesModule} from '@app/services';


@NgModule({
  declarations: [
    ImageDetailsComponent
  ],
  exports: [
    ImageDetailsComponent
  ],
  imports: [
    CommonModule,
    ContentEditorModule,
    RouterModule,
    ServicesModule,
  ]
})
export class ImageDetailsModule { }
