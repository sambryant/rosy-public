import {takeUntil} from 'rxjs/operators';
import {timer, Subject, Subscription, } from 'rxjs';
import {ActivatedRoute, Router, } from '@angular/router';
import {Component, OnDestroy, OnInit, } from '@angular/core';

import {HOME_PAGE} from '@app/app-page.model';
import {Image} from '@app/models';
import {LibraryService} from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-image-details',
  templateUrl: './image-details.component.html',
  styleUrls: ['./image-details.component.scss']
})
export class ImageDetailsComponent
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  image: Image | null = null;

  protected loadingStarted$ = new Subject<null>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private libraryService: LibraryService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroyed$))
      .subscribe(params => {
        let id = null;
        if (params.hasOwnProperty('imageId')) {
          id = params['imageId'];
        }

        if (this.image && this.image.id !== id) {
          this.image = null;
        }
        this.loadingStarted$.next(null);
        if (id !== null) {
          this.libraryService.getImage(id)
            .pipe(takeUntil(this.loadingStarted$))
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
              this.image = response.item;
            }, _ => {
              // Redirect to home page
              timer(500).subscribe(() => {
                HOME_PAGE.navigate(this.router);
              });
            });
        }
    });
  }

}
