import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';

import {ApiService, NotificationService, } from '@app/services';

import {ImageDetailsComponent, ImageDetailsModule, } from '.';


describe('ImageDetailsComponent', () => {
  let component: ImageDetailsComponent;
  let fixture: ComponentFixture<ImageDetailsComponent>;
  let getImageSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ImageDetailsComponent
      ],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(NotificationService),
      ],
    })
    .compileComponents();

    const apiService = TestBed.inject(ApiService);
    getImageSpy = spyOn(apiService, 'getImage');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
