import {map} from 'rxjs/operators';
import {of, Observable, } from 'rxjs';
import {Pipe, PipeTransform, } from '@angular/core';

import {
  ArtistId,
  ContentType,
  GroupId,
  NameId,
  Id,
  TagId,
} from '@app/models';
import {LibraryService} from '@app/services';


@Pipe({
  name: 'contentValue'
})
export class ContentValuePipe implements PipeTransform {

  constructor(private libraryService: LibraryService) {}

  transform(id: Id | null, type: ContentType): Observable<string> {
    if (id === null) {
      return of(`no ${type}`);
    } else {
      return this.libraryService.getContentValueById(type, id)
        .pipe(map(m => m ? m : `Unknown ${type}`));
    }
  }

}
