import {take} from 'rxjs/operators';
import {Observable, Subject, } from 'rxjs';

import {ContentValue} from '@app/models';
import {LibraryService} from '@app/services';
import {MockArtist, MockGroup, MockName, MockTag, } from '@app/models/testing';

import {ContentValuePipe} from '.';


describe('ContentValuePipe', () => {
  let contentValueSubj: Subject<string | undefined>;
  let getContentSpy: jasmine.Spy;
  let libraryService: Partial<LibraryService>;
  let pipe: ContentValuePipe;

  beforeEach(() => {
    contentValueSubj = new Subject();
    libraryService = {
      getContentValueById: () => {
        return 1 as any as Observable<string | undefined>;
      }
    };
    getContentSpy = spyOn(libraryService, 'getContentValueById' as never);
    getContentSpy.and.returnValue(contentValueSubj.pipe(take(1)));
    pipe = new ContentValuePipe(libraryService as LibraryService);
  });

  it('should handle null correctly', () => {
    expect(pipe.transform(null, 'artist').subscribe((value) => {
      expect(value).toEqual('no artist');
    }).closed).toBeTrue();
    expect(pipe.transform(null, 'group').subscribe((value) => {
      expect(value).toEqual('no group');
    }).closed).toBeTrue();
    expect(pipe.transform(null, 'name').subscribe((value) => {
      expect(value).toEqual('no name');
    }).closed).toBeTrue();
    expect(pipe.transform(null, 'tag').subscribe((value) => {
      expect(value).toEqual('no tag');
    }).closed).toBeTrue();
    expect(getContentSpy).not.toHaveBeenCalled();
  });

  it('should label an artist correctly', () => {
    const type = 'artist';
    const contentValue = 'my name';
    const fakeId = 1;
    const expectedResult = 'my name';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('artist', fakeId);
    expect(sub.closed).toBeTrue();
  });

  it('should label a group correctly', () => {
    const type = 'group';
    const contentValue = 'my name';
    const fakeId = 1;
    const expectedResult = 'my name';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('group', fakeId);
    expect(sub.closed).toBeTrue();
  });

  it('should label a name correctly', () => {
    const type = 'name';
    const contentValue = 'my name';
    const fakeId = 1;
    const expectedResult = 'my name';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('name', fakeId);
    expect(sub.closed).toBeTrue();
  });

  it('should label a tag correctly', () => {
    const type = 'tag';
    const contentValue = 'my name';
    const fakeId = 1;
    const expectedResult = 'my name';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('tag', fakeId);
    expect(sub.closed).toBeTrue();
  });

  it('should label an unknown artist correctly', () => {
    const type = 'artist';
    const contentValue = undefined;
    const fakeId = 2;
    const expectedResult = 'Unknown artist';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('artist', fakeId);
    expect(sub.closed).toBeTrue();
  });

  it('should label an unknown group correctly', () => {
    const type = 'group';
    const contentValue = undefined;
    const fakeId = 2;
    const expectedResult = 'Unknown group';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('group', fakeId);
    expect(sub.closed).toBeTrue();
  });

  it('should label an unknown name correctly', () => {
    const type = 'name';
    const contentValue = undefined;
    const fakeId = 2;
    const expectedResult = 'Unknown name';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('name', fakeId);
    expect(sub.closed).toBeTrue();
  });

  it('should label an unknown tag correctly', () => {
    const type = 'tag';
    const contentValue = undefined;
    const fakeId = 2;
    const expectedResult = 'Unknown tag';

    const sub = pipe.transform(fakeId, type).subscribe((value) => {
      expect(value).toEqual(expectedResult);
    });
    contentValueSubj.next(contentValue);
    expect(getContentSpy).toHaveBeenCalledOnceWith('tag', fakeId);
    expect(sub.closed).toBeTrue();
  });

});
