import {Pipe, PipeTransform, } from '@angular/core';

import {Range} from '@app/models';


@Pipe({
  name: 'range',
})
export class RangePipe implements PipeTransform {

  transform(label: string, lower: number | null, upper: number | null): string {
    if (lower !== null && upper !== null) {
      return `${lower} < ${label} < ${upper}`;
    } else if (lower !== null) {
      return `${label} > ${lower}`;
    } else if (upper !== null) {
      return `${label} < ${upper}`;
    } else {
      throw new Error('This should not be called on an empty range');
    }
  }

}
