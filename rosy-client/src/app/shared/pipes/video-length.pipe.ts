import {Pipe, PipeTransform, } from '@angular/core';

const MINUTE = 60;
const HOUR = 60 * 60;

@Pipe({
  name: 'videoLength'
})
export class VideoLengthPipe implements PipeTransform {

  transform(size: number): string {
    if (size < MINUTE) {
      return Math.round(size) + ' s';
    } else if (size < HOUR) {
      return Math.round(size / MINUTE) + ' m';
    } else {
      return Math.round(size / HOUR) + ' h';
    }
  }
}
