import {Range} from '@app/models';

import {RangePipe} from '.';


describe('RangePipe', () => {
  let pipe: RangePipe;

  beforeEach(() => {
    pipe = new RangePipe();
  });

  it('should handle case where lower and upper range are defined', () => {
    const label = 'rating';
    const range: Range<number> = {lower: 1.4, upper: 2.9};
    expect(pipe.transform(label, range.lower, range.upper)).toEqual('1.4 < rating < 2.9');
  });

  it('should handle case where only lower range is defined', () => {
    const label = 'rating';
    const range: Range<number> = {lower: 1.4, upper: null};
    expect(pipe.transform(label, range.lower, range.upper)).toEqual('rating > 1.4');
  });

  it('should handle case where only upper range', () => {
    const label = 'rating';
    const range: Range<number> = {lower: null, upper: 2.9};
    expect(pipe.transform(label, range.lower, range.upper)).toEqual('rating < 2.9');
  });

  it('should throw an error when called on an empty range', () => {
    const label = 'rating';
    const range: Range<number> = {lower: null, upper: null};
    expect(() => {pipe.transform(label, range.lower, range.upper);}).toThrowError();
  });

});
