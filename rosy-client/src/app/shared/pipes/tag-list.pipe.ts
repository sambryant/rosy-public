import {Pipe, PipeTransform, } from '@angular/core';

import {Tag} from '@app/models';


@Pipe({
  name: 'tagList'
})
export class TagListPipe implements PipeTransform {

  transform(tags: Tag[]): string {
    return tags.map(t => t.value).join(', ');
  }
}
