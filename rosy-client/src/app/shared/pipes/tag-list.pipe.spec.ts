import {MockName, MockTag, } from '@app/models/testing';

import {TagListPipe} from '.';


describe('TagListPipe', () => {
  it('should work correctly', () => {
    const pipe = new TagListPipe();

    expect(pipe.transform([])).toEqual('');
    expect(pipe.transform([
      new MockName(1, 'hi there'),
      new MockName(2, 'bye there'),
    ])).toEqual('hi there, bye there');
    expect(pipe.transform([
      new MockTag(1, 'hi there'),
      new MockTag(2, 'bye there'),
    ])).toEqual('hi there, bye there');
  });
});
