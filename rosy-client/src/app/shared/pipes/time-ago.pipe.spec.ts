import {TimeAgoPipe} from '.';


describe('TimeAgoPipe', () => {
  const second = 1000;
  const minute = 60 * second;
  const hour = 60 * minute;
  const day =  24 * hour;
  const week = 7 * day;
  const year = 365 * day;
  const mockNow = 100000000;
  let pipe: TimeAgoPipe;

  beforeEach(() => {
    spyOn(Date, 'now').and.returnValue(mockNow);
    pipe = new TimeAgoPipe();
  });

  it('should show a dash when timestamp is null', () => {
    const timestamp = null;
    const expectedResult = '-';

    expect(pipe.transform(timestamp)).toBe(expectedResult);
  });

  it('should report now when timestamp is less than a second ago', () => {
    const timestamp = mockNow - 999;
    const expectedResult = 'just now';

    expect(pipe.transform(timestamp)).toBe(expectedResult);
  });

  it('should report seconds ago if less than a minute ago', () => {
    const timestamps = [
      mockNow - 1  * second - 1,
      mockNow - 2  * second - 1,
      mockNow - 60 * second + 1,
    ];
    const expectedResults = [
      '1 sec ago',
      '2 secs ago',
      '59 secs ago'
    ];

    for (let i = 0; i < timestamps.length; i++) {
      expect(pipe.transform(timestamps[i])).toBe(expectedResults[i]);
    }
  });

  it('should report minutes ago if less than an hour ago', () => {
    const timestamps = [
      mockNow - 1  * minute - 1,
      mockNow - 2  * minute - 1,
      mockNow - 60 * minute + 1,
    ];
    const expectedResults = [
      '1 min ago',
      '2 mins ago',
      '59 mins ago'
    ];

    for (let i = 0; i < timestamps.length; i++) {
      expect(pipe.transform(timestamps[i])).toBe(expectedResults[i]);
    }
  });

  it('should report hours ago if less than a day ago', () => {
    const timestamps = [
      mockNow - 1  * hour - 1,
      mockNow - 2  * hour - 1,
      mockNow - 24 * hour + 1,
    ];
    const expectedResults = [
      '1 hour ago',
      '2 hours ago',
      '23 hours ago'
    ];

    for (let i = 0; i < timestamps.length; i++) {
      expect(pipe.transform(timestamps[i])).toBe(expectedResults[i]);
    }
  });

  it('should report days ago if less than a week ago', () => {
    const timestamps = [
      mockNow - 1 * day - 1,
      mockNow - 2 * day - 1,
      mockNow - 7 * day + 1,
    ];
    const expectedResults = [
      '1 day ago',
      '2 days ago',
      '6 days ago'
    ];

    for (let i = 0; i < timestamps.length; i++) {
      expect(pipe.transform(timestamps[i])).toBe(expectedResults[i]);
    }
  });

  it('should report weeks ago if less than a year ago', () => {
    const timestamps = [
      mockNow - 1 * week - 1,
      mockNow - 2 * week - 1,
      mockNow - 1 * year + 1,
    ];
    const expectedResults = [
      '1 week ago',
      '2 weeks ago',
      '52 weeks ago'
    ];

    for (let i = 0; i < timestamps.length; i++) {
      expect(pipe.transform(timestamps[i])).toBe(expectedResults[i]);
    }
  });

  it('should otherwise report years ago', () => {
    const timestamps = [
      mockNow - 1 * year - 1,
      mockNow - 2 * year - 1,
      mockNow - 25 * year + 1,
      mockNow - 25 * year - 1,
    ];
    const expectedResults = [
      '1 year ago',
      '2 years ago',
      '24 years ago',
      '25 years ago'
    ];

    for (let i = 0; i < timestamps.length; i++) {
      expect(pipe.transform(timestamps[i])).toBe(expectedResults[i]);
    }
  });
});
