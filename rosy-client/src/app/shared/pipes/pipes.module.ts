import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {ContentValuePipe} from './content-value.pipe';
import {DateRangePipe} from './date-range.pipe';
import {DecimalPipe} from './decimal.pipe';
import {FilesizePipe} from './filesize.pipe';
import {KeyBindingLabelPipe} from './key-binding-label.pipe';
import {RangePipe} from './range.pipe';
import {TagListPipe} from './tag-list.pipe';
import {TimeAgoPipe} from './time-ago.pipe';
import {VideoLengthPipe} from './video-length.pipe';


@NgModule({
  declarations: [
    ContentValuePipe,
    DateRangePipe,
    DecimalPipe,
    FilesizePipe,
    KeyBindingLabelPipe,
    RangePipe,
    TagListPipe,
    TimeAgoPipe,
    VideoLengthPipe,
  ],
  exports: [
    ContentValuePipe,
    DateRangePipe,
    DecimalPipe,
    FilesizePipe,
    KeyBindingLabelPipe,
    RangePipe,
    TagListPipe,
    TimeAgoPipe,
    VideoLengthPipe,
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
