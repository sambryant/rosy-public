import {formatDate} from '@angular/common';
import {Inject, LOCALE_ID, Pipe, PipeTransform, } from '@angular/core';

import {Range} from '@app/models';


@Pipe({
  name: 'dateRange',
  pure: false,
})
export class DateRangePipe implements PipeTransform {

  constructor(@Inject(LOCALE_ID) protected localeId: string) {}

  protected format(date: Date): string {
    return formatDate(date, 'shortDate', this.localeId);
  }

  transform(range: Range<Date>): string {
    if (range.lower !== null && range.upper !== null) {
      return `after ${this.format(range.lower)} and before ${this.format(range.upper)}`;
    } else if (range.lower !== null) {
      return `after ${this.format(range.lower)}`;
    } else if (range.upper !== null) {
      return `before ${this.format(range.upper)}`;
    } else {
      throw new Error('This should not be called on an empty range');
    }
  }

}
