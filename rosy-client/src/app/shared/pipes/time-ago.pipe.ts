import {Pipe, PipeTransform, } from '@angular/core';


const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;
const WEEK = 7 * DAY;
const YEAR = 365 * DAY;


@Pipe({
  name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {

  transform(timestamp: number | null): string {
    if (timestamp === null) {
      return '-';
    }
    const now = Date.now();
    let unit: string;
    let value = (now - timestamp);
    if (value < SECOND) {
      return 'just now';
    } else if (value < MINUTE) {
      value = Math.floor(value / SECOND);
      unit = 'sec';
    } else if (value < HOUR) {
      value = Math.floor(value / MINUTE);
      unit = 'min';
    } else if (value < DAY) {
      value = Math.floor(value / HOUR);
      unit = 'hour';
    } else if (value < WEEK) {
      value = Math.floor(value / DAY);
      unit = 'day';
    } else if (value < YEAR) {
      value = Math.floor(value / WEEK);
      unit = 'week';
    } else {
      value = Math.floor(value / YEAR);
      unit = 'year';
    }
    if (value < 2) {
      return `${value} ${unit} ago`;
    } else {
      return `${value} ${unit}s ago`;
    }
  }
}
