import {Pipe, PipeTransform, } from '@angular/core';

import {KeyBinding} from '@app/models';


@Pipe({
  name: 'keyBindingLabel'
})
export class KeyBindingLabelPipe implements PipeTransform {

  transform(value: KeyBinding | null, omitUnbound: boolean = false): string {
    return (value && value.toLabel()) || (omitUnbound ? '' : 'not bound');
  }

}
