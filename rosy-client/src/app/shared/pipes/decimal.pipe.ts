import {Pipe, PipeTransform, } from '@angular/core';

@Pipe({
  name: 'decimal'
})
export class DecimalPipe implements PipeTransform {

  transform(value: number | null, places: 1 | 2 | 3 = 1): string {
    if (value === null) {
      return '-';
    } else {
      if (places === 1) {
        return (Math.round(value * 10) / 10).toString();
      } else if (places === 2) {
        return (Math.round(value * 100) / 100).toString();
      } else if (places === 3) {
        return (Math.round(value * 1000) / 1000).toString();
      } else {
        throw new Error('Unreachable');
      }
    }
  }

}
