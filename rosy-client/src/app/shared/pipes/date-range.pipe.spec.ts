import {Range} from '@app/models';

import {DateRangePipe} from '.';


describe('DateRangePipe', () => {
  let pipe: DateRangePipe;

  beforeEach(() => {
    pipe = new DateRangePipe('en-US');
  });

  it('should handle case where lower and upper range are defined', () => {
    const range: Range<Date> = {
      lower: Date.parse('14 Oct 2011 00:00:00 EST') as any as Date,
      upper: Date.parse('14 Oct 2014 00:00:00 EST') as any as Date,
    };
    expect(pipe.transform(range)).toEqual('after 10/14/11 and before 10/14/14');
  });

  it('should handle case where only lower range is defined', () => {
    const range: Range<Date> = {
      lower: Date.parse('14 Oct 2011 00:00:00 EST') as any as Date,
      upper: null,
    };
    expect(pipe.transform(range)).toEqual('after 10/14/11');
  });

  it('should handle case where only upper range', () => {
    const range: Range<Date> = {
      lower: null,
      upper: Date.parse('14 Oct 2014 00:00:00 EST') as any as Date,
    };
    expect(pipe.transform(range)).toEqual('before 10/14/14');
  });

  it('should throw an error when called on an empty range', () => {
    const range: Range<Date> = {
      lower: null,
      upper: null,
    };
    expect(() => {pipe.transform(range);}).toThrowError();
  });

});
