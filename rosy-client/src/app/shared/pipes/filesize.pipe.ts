import {Pipe, PipeTransform, } from '@angular/core';

const MB = 1024 * 1024;
const KB = 1024;

@Pipe({
  name: 'filesize'
})
export class FilesizePipe implements PipeTransform {

  transform(size: number, digits: number = 2): string {
    if (size < MB) {
      return (size / KB).toFixed(digits) + 'kB';
    } else {
      return (size / MB).toFixed(digits) + 'MB';
    }
  }
}
