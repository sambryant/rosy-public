import {DecimalPipe} from '.';

describe('DecimalPipe', () => {

  it('should cutoff after one decimal point by default', () => {
    const pipe = new DecimalPipe();
    expect(pipe.transform(null)).toEqual('-');
    expect(pipe.transform(1.333)).toEqual('1.3');
    expect(pipe.transform(1.36)).toEqual('1.4');
  });

  it('should cutoff after two decimal points', () => {
    const pipe = new DecimalPipe();
    expect(pipe.transform(null, 2)).toEqual('-');
    expect(pipe.transform(1.333, 2)).toEqual('1.33');
    expect(pipe.transform(1.366, 2)).toEqual('1.37');
  });

  it('should cutoff after three decimal points', () => {
    const pipe = new DecimalPipe();
    expect(pipe.transform(null, 3)).toEqual('-');
    expect(pipe.transform(1.3333, 3)).toEqual('1.333');
    expect(pipe.transform(1.3666, 3)).toEqual('1.367');
  });

});
