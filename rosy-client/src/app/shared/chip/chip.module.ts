import {CommonModule} from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {NgModule} from '@angular/core';

import {ChipComponent, ChipListComponent, } from './chip.component';


@NgModule({
  declarations: [
    ChipComponent,
    ChipListComponent,
  ],
  exports: [
    ChipComponent,
    ChipListComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
  ]
})
export class ChipModule { }
