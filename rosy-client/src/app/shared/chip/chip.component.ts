import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'rx-chip',
  template: `
<ng-content></ng-content>
<mat-icon *ngIf="removable" (click)="removed.emit()">cancel</mat-icon>
`,
  styleUrls: ['./chip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipComponent {

  @Input() removable: boolean = false;

  @Output() removed: EventEmitter<void> = new EventEmitter();

  constructor() { }

}

@Component({
  selector: 'rx-chip-list',
  template: `<ng-content></ng-content>`,
  styleUrls: ['./chip-list.component.scss'],
  host: {
    '[class.nowrap]': `nowrap`,
    '[class.compact]': `compact`,
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChipListComponent {

  @Input() compact = false;

  @Input() nowrap = false;

  constructor() { }

}
