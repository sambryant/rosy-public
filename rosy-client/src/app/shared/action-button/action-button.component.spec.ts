import {take, takeUntil, } from 'rxjs/operators';
import {BehaviorSubject, Subject, } from 'rxjs';
import {Component, CUSTOM_ELEMENTS_SCHEMA, ViewChild, } from '@angular/core';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {J} from '@angular/cdk/keycodes';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {Action, KeyBinding, } from '@app/models';
import {ActionService, } from '@app/services';
import {PipesModule} from '@app/shared/pipes';

import {
  ActionButtonDirective,
  ActionMenuButtonComponent,
  ActionButtonModule,
} from '.';


describe('ActionButtonDirective', () => {
  let afterEach$: Subject<null>;
  let hostComponent: TestActionButtonDirectiveHostComponent;
  let component: ActionButtonDirective;
  let fixture: ComponentFixture<TestActionButtonDirectiveHostComponent>;
  let actionService: ActionService;

  let doActionSpy: jasmine.Spy;
  let button: HTMLElement;
  const action = Action.ShowFaceMatches;

  beforeEach(async () => {
    afterEach$ = new Subject();
    await TestBed.configureTestingModule({
      declarations: [
        ActionButtonDirective,
        TestActionButtonDirectiveHostComponent,
      ],
      imports: [
        MatButtonModule,
        NoopAnimationsModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    actionService = TestBed.inject(ActionService);
    doActionSpy = spyOn(actionService, 'doAction');

    fixture = TestBed.createComponent(TestActionButtonDirectiveHostComponent);
    hostComponent = fixture.componentInstance;
    hostComponent.action = action;
    fixture.detectChanges();
    component = hostComponent.component;
    button = fixture.nativeElement.querySelector('button');
  });

  afterEach(() => { afterEach$.next(null); });

  it('should execute action once when clicked if action is captured', () => {
    const event = new MouseEvent('click', { bubbles: true });
    actionService.captureAction(action).pipe(takeUntil(afterEach$)).subscribe();
    fixture.detectChanges();

    button.dispatchEvent(event);

    expect(doActionSpy).toHaveBeenCalledOnceWith(action, event);
  });

  it('should not execute action if action is disabled', () => {
    const event = new MouseEvent('click', { bubbles: true });
    const enabled = new BehaviorSubject(false);
    actionService.captureAction(action, enabled).pipe(takeUntil(afterEach$)).subscribe();
    fixture.detectChanges();

    button.dispatchEvent(event);
    expect(doActionSpy).not.toHaveBeenCalled();

    enabled.next(true);
    fixture.detectChanges();

    button.dispatchEvent(event);
    expect(doActionSpy).toHaveBeenCalledOnceWith(action, event);
  });

  it('should display button as disabled when action is disabled', () => {
    const enabled = new BehaviorSubject(true);
    actionService.captureAction(action, enabled).pipe(takeUntil(afterEach$)).subscribe();
    fixture.detectChanges();

    enabled.next(true);
    fixture.detectChanges();
    expect(button.classList.contains('mat-button-disabled')).toBeFalse();

    enabled.next(false);
    fixture.detectChanges();
    expect(button.classList.contains('mat-button-disabled')).toBeTrue();
  });

  it('should updated disabled value when action changes', () => {
    const actions = [ Action.AddName, Action.AddTag ];
    const enabled = [ new BehaviorSubject(true), new BehaviorSubject(false) ];
    actions.forEach((action, i) => {
      actionService.captureAction(action, enabled[i]).pipe(takeUntil(afterEach$)).subscribe();
    });

    hostComponent.action = actions[0];
    fixture.detectChanges();
    expect(component.disabled).toBe(false);

    hostComponent.action = actions[1];
    fixture.detectChanges();
    expect(component.disabled).toBe(true, 'should update disabled when action switches');
  });

});


describe('ActionMenuButtonComponent', () => {
  let afterEach$: Subject<null>;
  let hostComponent: TestMenuButtonHostComponent;
  let fixture: ComponentFixture<TestMenuButtonHostComponent>;
  let component: ActionMenuButtonComponent;

  let actionService: ActionService;
  let doActionSpy: jasmine.Spy;
  const action = Action.ShowFaceMatches;

  // DOM element from Component template to test things on
  let button: HTMLElement;

  beforeEach(async () => {
    afterEach$ = new Subject();
    await TestBed.configureTestingModule({
      declarations: [
        ActionMenuButtonComponent,
        ActionButtonDirective,
        TestMenuButtonHostComponent,
      ],
      imports: [
        MatButtonModule,
        MatMenuModule,
        NoopAnimationsModule,
        PipesModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    actionService = TestBed.inject(ActionService);
    doActionSpy = spyOn(actionService, 'doAction');

    fixture = TestBed.createComponent(TestMenuButtonHostComponent);
    hostComponent = fixture.componentInstance;
    hostComponent.action = action;
    hostComponent.label = null;
    fixture.detectChanges();

    button = fixture.nativeElement.querySelector('div.menu-button-container');
  });

  afterEach(() => { afterEach$.next(null); });

  it('should set the key binding label for a bound action', () => {
    const keyBinding = new KeyBinding(J, {ctrlKey: true});
    const expKeyLabel = 'Ctrl + j';

    actionService.setActionKeyBinding(action, keyBinding);
    fixture.detectChanges();

    const actKeyLabel = fixture.nativeElement.querySelector('.key').innerText;
    expect(actKeyLabel).toEqual(expKeyLabel);
  });

  it('should not set key binding label for an unbound action', () => {
    const keyBinding = null;
    const expKeyLabel = '';

    actionService.setActionKeyBinding(action, keyBinding);
    fixture.detectChanges();

    const actKeyLabel = fixture.nativeElement.querySelector('.key').innerText;
    expect(actKeyLabel).toEqual(expKeyLabel);
  });

  it('should use the action description as a label by default', () => {
    const label = null;
    const expLabel = actionService.getRegisteredAction(action).data.description;

    hostComponent.label = label;
    fixture.detectChanges();

    const actLabel = fixture.nativeElement.querySelector('.label').innerText;
    expect(actLabel).toEqual(expLabel);
  });

  it('should use provided name as a label', () => {
    const label = 'test test label';
    const expLabel = 'test test label';

    hostComponent.label = label;
    fixture.detectChanges();

    const actLabel = fixture.nativeElement.querySelector('.label').innerText;
    expect(actLabel).toEqual(expLabel);
  });

  it('should perform action once when clicked if action is enabled', () => {
    const event = new MouseEvent('click', { bubbles: true });
    const enabled = new BehaviorSubject(false);
    actionService.captureAction(action, enabled).pipe(takeUntil(afterEach$)).subscribe();

    enabled.next(false);
    fixture.detectChanges();

    button.dispatchEvent(event);
    expect(doActionSpy).not.toHaveBeenCalled();

    enabled.next(true);
    fixture.detectChanges();

    button.dispatchEvent(event);
    expect(doActionSpy).toHaveBeenCalledOnceWith(action, event);
  });

  it('should apply the disabled class when the action is disabled', () => {
    const enabled = new BehaviorSubject(true);
    actionService.captureAction(action, enabled).pipe(takeUntil(afterEach$)).subscribe();

    fixture.detectChanges();
    expect(button.classList.contains('disabled')).toBeFalse();

    enabled.next(false);
    fixture.detectChanges();
    expect(button.classList.contains('disabled')).toBeTrue();

    enabled.next(true);
    fixture.detectChanges();
    expect(button.classList.contains('disabled')).toBeFalse();
  });

  it('should update label when key binding changes', () => {
    const tests: [KeyBinding, string][] = [
      [new KeyBinding(J, {}), 'j'],
      [new KeyBinding(J, {ctrlKey: true}), 'Ctrl + j'],
      [new KeyBinding(J, {ctrlKey: true, shiftKey: true}), 'Ctrl + J'],
    ];
    actionService.captureAction(action).pipe(takeUntil(afterEach$)).subscribe();
    fixture.detectChanges();

    tests.forEach(([keyBinding, expKeyLabel]) => {
      actionService.setActionKeyBinding(action, keyBinding);
      fixture.detectChanges();
      const actKeyLabel = fixture.nativeElement.querySelector('.key').innerText;
      expect(actKeyLabel).toEqual(expKeyLabel);
    });
  });

});

@Component({
  template: `
<button [rxActionable]="action"
        #actionable=actionable
        mat-raised-button
        disableRipple
        [disabled]="actionable.disabled">
  {{actionable.description}}
</button>
`
})
class TestActionButtonDirectiveHostComponent {
  action: Action;
  @ViewChild('actionable') component: ActionButtonDirective;
}

@Component({
  template: `
    <div mat-menu-item disableRipple>
      <rx-action-menu-button [action]="action" [label]="label">
      </rx-action-menu-button>
    </div>
  `
})
class TestMenuButtonHostComponent {
  action: Action;
  label: string | null;
}
