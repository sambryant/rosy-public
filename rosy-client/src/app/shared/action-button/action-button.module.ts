import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {NgModule} from '@angular/core';

import {PipesModule} from '@app/shared/pipes';
import {ServicesModule} from '@app/services';

import {ActionButtonDirective, ActionMenuButtonComponent, } from './action-button.component';


@NgModule({
  declarations: [
    ActionButtonDirective,
    ActionMenuButtonComponent,
  ],
  exports: [
    ActionButtonDirective,
    ActionMenuButtonComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    PipesModule,
    ServicesModule,
  ]
})
export class ActionButtonModule { }
