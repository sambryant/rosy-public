import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  Directive,
  HostListener,
  Input,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

import {Action, KeyBinding, } from '@app/models';
import {ActionService} from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Directive({
  selector: '[rxActionable]',
  exportAs: 'actionable',
})
export class ActionButtonDirective
extends UnsubscribeOnDestroyedDirective {

  @Input('rxActionable')
  get action() { return this._action; }
  set action(action: Action | null) {
    this._action = action;
    this.onActionChange();
  }

  description: string | null = null;

  keyBinding: KeyBinding | null = null;

  disabled: boolean = true;

  protected actionChanged$ = new Subject<null>();

  protected _action: Action | null = null;

  constructor(protected actionService: ActionService, protected cdr: ChangeDetectorRef) {
    super();
  }

  @HostListener('click', ['$event']) onClick(event) {
    if (this._action !== null && !this.disabled) {
      this.actionService.doAction(this._action, event);
    }
  }

  protected onActionChange() {
    this.actionChanged$.next(null);
    if (this._action === null) {
      this.keyBinding = null;
      this.disabled = true;
    } else {
      const entry = this.actionService.getRegisteredAction(this._action);
      this.description = entry.data.description;
      this.keyBinding = entry.binding;
      this.disabled = !entry.enabled$.value;

      entry.enabled$
        .pipe(takeUntil(this.actionChanged$))
        .pipe(takeUntil(this.destroyed$))
        .subscribe(enabled => {
          this.disabled = !enabled;
          this.cdr.markForCheck();
        });

      entry.bindingChanged$
        .pipe(takeUntil(this.actionChanged$))
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.keyBinding = entry.binding;
          this.cdr.markForCheck();
        });
    }
  }

}


@Component({
  selector: 'rx-action-menu-button',
  template: `
    <div [rxActionable]="action" #actionable=actionable
         class="menu-button-container" [ngClass]="{'disabled': actionable.disabled}">
      <div class="label">
        {{label || actionable.description}}
      </div>
      <div class="key">
        {{actionable.keyBinding | keyBindingLabel:true}}
      </div>
    </div>
  `,
  styleUrls: ['./action-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionMenuButtonComponent {

  @Input() action: Action | null = null;

  @Input() label: string | null = null;

  constructor() {}

}
