import {Directive, OnDestroy, } from '@angular/core';
import {Subject} from 'rxjs';

@Directive()
export abstract class UnsubscribeOnDestroyedDirective implements OnDestroy {

  protected readonly destroyed$: Subject<null> = new Subject();

  constructor() { }

  ngOnDestroy(): void {
    this.destroyed$.next(null);
    this.destroyed$.complete();
  }
}
