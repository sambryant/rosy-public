import {Unsubscribable} from 'rxjs';

export function safeUnsubscribe(sub: Unsubscribable | null | undefined) {
  if (sub) {
    sub.unsubscribe();
  }
}

export function safeUnsubscribeAll(subs: (Unsubscribable | null | undefined)[] | null) {
  if (subs) {
    subs.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }
}
