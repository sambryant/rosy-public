import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {ClickIgnoreDirective} from './click-ignore.directive';


@NgModule({
  declarations: [
    ClickIgnoreDirective
  ],
  exports: [
    ClickIgnoreDirective
  ],
  imports: [
    CommonModule
  ]
})
export class UtilityModule { }
