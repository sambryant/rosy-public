import {fromEvent} from 'rxjs';
import {Directive, ElementRef, OnInit, } from '@angular/core';


@Directive({selector: '[rxClickIgnore]'})
export class ClickIgnoreDirective implements OnInit {

  constructor(private host: ElementRef) {}

  ngOnInit() {
    fromEvent(this.host.nativeElement, 'click')
      .subscribe((e: MouseEvent) => {
        e.stopPropagation();
      });
  }
}
