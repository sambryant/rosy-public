import {take} from 'rxjs/operators';
import {ComponentType} from '@angular/cdk/portal';
import {Inject, Injectable, } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, } from '@angular/material/dialog';
import {Observable} from 'rxjs';

import {
  Action,
  Artist,
  ContentMeta,
  ContentType,
  GroupSimple,
  Tag,
} from '@app/models';
import {ActionService} from '@app/services';
import {ContentCreateRequest} from '@app/shared/auto-input';

import {
  GenericDialogData,
  GenericConfirmDialogComponent,
  GenericInputDialogComponent,
  GenericMessageDialogComponent,
} from './generic-dialog.component';
import {ActionDialogComponent} from './action-dialog';
import {ContentInputDialogComponent, ContentInputDialogData, } from './content-input-dialog.component';


export interface ContentInputDialogResponse {
  selected: Observable<ContentMeta>;
  created: Observable<ContentCreateRequest>;
};


@Injectable({providedIn: 'root'})
export class DialogService {

  constructor(
    private actionService: ActionService,
    private dialog: MatDialog,
  ) {
    this.actionService.captureAction(Action.ShowKeyBindings).subscribe(() => {
      this.showActionDialog();
    });
  }

  open<T>(component: ComponentType<T>, args: MatDialogConfig<any>): MatDialogRef<T, any> {
    this.closeAll();
    const ref = this.dialog.open(component, args);

    ref.afterOpened().pipe(take(1)).subscribe(() => {
      this.actionService.blockEventHandling();
    });
    ref.afterClosed().pipe(take(1)).subscribe(() => {
      this.actionService.unblockEventHandling();
    });
    return ref;
  }

  closeAll() {
    this.dialog.closeAll();
  }

  showActionDialog() {
    this.open(ActionDialogComponent, {
      width: '600px',
    });
  }

  showContentInputDialog(data: ContentInputDialogData): ContentInputDialogResponse {
    const dialogRef = this.open(ContentInputDialogComponent, { data });
    return {
      selected: dialogRef.componentInstance.selected$,
      created: dialogRef.componentInstance.created$
    };
  }

  showConfirm(title: string, message: string): Observable<boolean> {
    // TODO: make unsubscribe close the dialog!
    const data: GenericDialogData = {title, message};
    const dialogRef = this.open(GenericConfirmDialogComponent, { data });
    return dialogRef.afterClosed() as Observable<boolean>;
  }

  showInput(title: string, message: string): Observable<string | null> {
    const data: GenericDialogData = {title, message};
    const dialogRef = this.open(GenericInputDialogComponent, { data });
    return dialogRef.afterClosed() as Observable<string>;
  }

  showMessage(title: string, message: string): Observable<void> {
    const data: GenericDialogData = {title, message};
    const dialogRef = this.open(GenericMessageDialogComponent, { data });
    return dialogRef.afterClosed() as Observable<void>;
  }

}
