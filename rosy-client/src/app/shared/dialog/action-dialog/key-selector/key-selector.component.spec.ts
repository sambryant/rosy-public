import {
  A,
  J,
  T,
  CONTROL,
  BACKSPACE,
} from '@angular/cdk/keycodes';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MockProvider, } from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {Action, KeyBinding, } from '@app/models';
import {ActionService, NotificationService, } from '@app/services';
import {PipesModule} from '@app/shared/pipes';

import {ActionDialogModule} from '..';
import {KeySelectorComponent} from '.';


describe('KeySelectorComponent', () => {
  let actionService: ActionService;
  let component: KeySelectorComponent;
  let fixture: ComponentFixture<KeySelectorComponent>;
  let notificationService: NotificationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        KeySelectorComponent,
      ],
      imports: [
        NoopAnimationsModule,
        PipesModule,
      ],
      providers: [
        MockProvider(NotificationService),
      ]
    })
    .compileComponents();
    notificationService = TestBed.inject(NotificationService);
    actionService = TestBed.inject(ActionService);

    spyOn(notificationService, 'error');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeySelectorComponent);
    component = fixture.componentInstance;
  });

  it('should show an overlay once capture mode starts', () => {
    component.action = Action.ShowFaceMatches;
    component.value = new KeyBinding(A, {});
    fixture.detectChanges();

    expect(component.captureMode).toBeFalse();
    expect(fixture.nativeElement.querySelector('.capture-overlay')).toBeFalsy();

    const click = document.createEvent('Events');
    click.initEvent('click', true, false);
    component.startCaptureMode(click);
    fixture.detectChanges();

    expect(component.captureMode).toBeTrue();
    expect(fixture.nativeElement.querySelector('.capture-overlay')).toBeTruthy();
  });

  it('should stop click propagation when starting capture mode', () => {
    component.action = Action.ShowFaceMatches;
    component.value = new KeyBinding(A, {});
    fixture.detectChanges();

    const click = document.createEvent('Events');
    click.initEvent('click', true, false);
    const spy = spyOn(click, 'stopPropagation');
    component.startCaptureMode(click);

    expect(spy).toHaveBeenCalled();
  });

  describe('captureMode', () => {
    const action = Action.ShowFaceMatches;

    beforeEach(() => {
      component.action = action;
      fixture.detectChanges();

      const click = document.createEvent('Events');
      click.initEvent('click', true, false);
      component.startCaptureMode(click);
      fixture.detectChanges();
    });

    it('should switch to view mode when clicked', () => {
      expect(component.captureMode).toBeTrue();

      const click = new MouseEvent('click', { bubbles: true });

      document.dispatchEvent(click);
      fixture.detectChanges();

      expect(component.captureMode).toBeFalse();
    });

    it('should not respond to successive clicks', () => {
      const click = new MouseEvent('click', { bubbles: true });
      const spy = spyOn(click, 'stopPropagation');

      document.dispatchEvent(click);
      fixture.detectChanges();

      expect(spy).toHaveBeenCalledTimes(1);

      document.dispatchEvent(click);
      fixture.detectChanges();

      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should change key binding when new key binding pressed', () => {
      const keyEvent = new KeyboardEvent('keydown', {
        bubbles: true,
        ctrlKey: true,
        keyCode: J,
      } as any);
      const spy = spyOn(keyEvent, 'stopPropagation');
      const expectedKeyBinding = new KeyBinding(J, {ctrlKey: true});

      expect(actionService.getActionBinding(action)).not.toEqual(expectedKeyBinding);
      document.dispatchEvent(keyEvent);
      expect(actionService.getActionBinding(action)).toEqual(expectedKeyBinding);
      expect(component.captureMode).toBeFalse();
    });

    it('should notify user when they change key to already bound key', () => {
      const keyEvent = new KeyboardEvent('keydown', {
        bubbles: true,
        ctrlKey: true,
        keyCode: J,
      } as any);
      const conflictingAction = Action.ShowKeyBindings;
      const description = actionService.getRegisteredAction(conflictingAction).data.description;
      const expectedError = `Already bound to '${description}'`;
      const keyBinding = KeyBinding.fromEvent(keyEvent);

      actionService.setActionKeyBinding(conflictingAction, keyBinding);
      expect(actionService.getActionBinding(action)).not.toEqual(keyBinding);
      expect(notificationService.error).not.toHaveBeenCalled();
      document.dispatchEvent(keyEvent);
      expect(actionService.getActionBinding(action)).not.toEqual(keyBinding);
      expect(notificationService.error).toHaveBeenCalledWith(expectedError);
      expect(component.captureMode).toBeFalse();
    });

    it('should notify user when they try to use a forbidden key binding', () => {
      const keyEvent = new KeyboardEvent('keydown', {
        bubbles: true,
        ctrlKey: true,
        keyCode: T,
      } as any);
      const expectedError = 'Cannot bind to that hotkey';

      expect(notificationService.error).not.toHaveBeenCalled();
      document.dispatchEvent(keyEvent);
      expect(notificationService.error).toHaveBeenCalledWith(expectedError);
      expect(component.captureMode).toBeFalse();
    });

    it('should unbind the action when backspace is pressed', () => {
      const originalBinding = new KeyBinding(J, {ctrlKey: true});
      const keyEvent = new KeyboardEvent('keydown', { bubbles: true, keyCode: BACKSPACE, } as any);
      const expectedBinding = null;

      actionService.setActionKeyBinding(action, originalBinding);
      expect(actionService.getActionBinding(action)).toEqual(originalBinding);
      document.dispatchEvent(keyEvent);
      expect(actionService.getActionBinding(action)).toBeNull();
      expect(component.captureMode).toBeFalse();
    });

    it('should ignore modifier key presses', () => {
      const originalBinding = new KeyBinding(J, {ctrlKey: true});
      const keyEvent = new KeyboardEvent('keydown', { bubbles: true, keyCode: CONTROL, } as any);

      actionService.setActionKeyBinding(action, originalBinding);
      expect(actionService.getActionBinding(action)).toEqual(originalBinding);
      expect(component.captureMode).toBeTrue();
      document.dispatchEvent(keyEvent);
      expect(actionService.getActionBinding(action)).toEqual(originalBinding);
      expect(component.captureMode).toBeTrue();
    });
  });

});
