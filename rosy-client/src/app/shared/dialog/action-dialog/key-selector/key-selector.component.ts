import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {fromEvent, Subject, } from 'rxjs';
import {takeUntil, take, } from 'rxjs/operators';
import {CONTROL, SHIFT, ENTER, ALT, BACKSPACE, META, } from '@angular/cdk/keycodes';

import {Action, KeyBinding, } from '@app/models';
import {ActionService, NotificationService, } from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-key-selector',
  templateUrl: './key-selector.component.html',
  styleUrls: ['./key-selector.component.scss']
})
export class KeySelectorComponent
extends UnsubscribeOnDestroyedDirective {

  @Input() action: Action;

  @Input() value: KeyBinding | null;

  @Output() changed = new EventEmitter<KeyBinding>();

  captureMode = false;

  protected viewModeStart: Subject<null> = new Subject();

  constructor(
    private actionService: ActionService,
    private notificationService: NotificationService) {
    super();
  }

  protected startViewMode() {
    this.captureMode = false;
    this.viewModeStart.next(null);
  }

  startCaptureMode(event) {
    if (this.captureMode) {
      return;
    }

    this.captureMode = true;
    event.stopPropagation();
    event.preventDefault();

    fromEvent(document, 'click')
      .pipe(takeUntil(this.viewModeStart))
      .pipe(takeUntil(this.destroyed$))
      .pipe(take(1))
      .subscribe(ev => {
        ev.stopPropagation();
        ev.preventDefault();
        this.startViewMode();
      });
    fromEvent(document, 'keydown')
      .pipe(takeUntil(this.viewModeStart))
      .pipe(takeUntil(this.destroyed$))
      .subscribe((ev: any) => {
        ev.stopPropagation();
        ev.preventDefault();
        this.capturePress(ev);
      });
  }

  private capturePress(event: KeyboardEvent) {
    const c = event.keyCode;
    console.log('key event: ', event);
    if (c === CONTROL || c === SHIFT || c === ALT || c === META) {
      return;
    } else if (c === ENTER && (!event.ctrlKey && !event.altKey && !event.shiftKey)) {
      this.startViewMode();
      return;
    } else if (c === BACKSPACE) {
      this.actionService.setActionKeyBinding(this.action, null);
      this.startViewMode();
      return;
    } else if (!KeyBinding.isValid(event)) {
      this.notificationService.error(`Cannot bind to that hotkey`);
      this.startViewMode();
      return;
    } else {
      const keyBinding = KeyBinding.fromEvent(event);
      const existing = this.actionService.getBoundAction(keyBinding);
      if (existing) {
        if (existing !== this.action) {
          const description = this.actionService.getRegisteredAction(existing).data.description;
          this.notificationService.error(`Already bound to '${description}'`);
        }
        this.startViewMode();
      } else {
        this.actionService.setActionKeyBinding(this.action, keyBinding);
        this.value = keyBinding;
        this.changed.emit(keyBinding);
        this.startViewMode();
      }
    }
  }

}
