import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgModule} from '@angular/core';

import {EditableModule} from '@app/shared/editable';
import {PipesModule} from '@app/shared/pipes';
import {ServicesModule} from '@app/services';

import {ActionDialogComponent} from './action-dialog.component';
import {KeySelectorComponent} from './key-selector/key-selector.component';


@NgModule({
  declarations: [
    ActionDialogComponent,
    KeySelectorComponent,
  ],
  exports: [
    ActionDialogComponent,
    KeySelectorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    EditableModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatTooltipModule,
    PipesModule,
    ReactiveFormsModule,
    ServicesModule,
  ]
})
export class ActionDialogModule { }
