import {Component, Inject, OnInit, } from '@angular/core';

import {ActionService, RegisteredAction, } from '@app/services';
import {Category, ACTION_DATA, CATEGORIES, } from '@app/models';


@Component({
  selector: 'rx-action-dialog',
  templateUrl: './action-dialog.component.html',
  styleUrls: ['./action-dialog.component.scss']
})
export class ActionDialogComponent {

  categories: Category[] = CATEGORIES;

  sortedActions: RegisteredAction[][];

  constructor(protected actionService: ActionService) {
    const categoryToIndex: Map<Category, number> = new Map(CATEGORIES.map((cat, i) => [cat, i]));
    this.categories = CATEGORIES;
    this.sortedActions = CATEGORIES.map(_ => []);

    ACTION_DATA.forEach((data, name) => {
      if (data.allowBinding) {
        const action = this.actionService.getRegisteredAction(name);
        this.sortedActions[categoryToIndex.get(data.category)!].push(action);
      }
    });
  }

  resetAllToDefault() {
    this.actionService.resetAllBindings();
  }

}
