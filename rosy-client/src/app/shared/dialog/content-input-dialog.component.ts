import {
  Component,
  Inject,
  Input,
  OnInit,
  Output,
 } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, } from '@angular/material/dialog';
import {Observable, Subject, } from 'rxjs';

import {ContentCreateRequest, ContentInputComponent, } from '@app/shared/auto-input';
import {ContentMeta, ContentType, ContentValue, } from '@app/models';


export interface ContentInputDialogData {
  type: ContentType;
  title: string;
  placeholder: string;
  allowCreate: boolean;
  persistent: boolean;
  values?: Observable<ContentValue[]>;
}


@Component({
  template: `
    <h2 mat-dialog-title>{{title}}</h2>
    <mat-dialog-content>
      <rx-content-input
        [allowCreate]="allowCreate"
        [optionsList]="values"
        [multiple]="persistent"
        [placeholder]="placeholder"
        [type]="type"
        (create)="onCreate($event)"
        (selected)="onSelected($event)">
      </rx-content-input>
    </mat-dialog-content>
  `,
})
export class ContentInputDialogComponent implements ContentInputDialogData {

  allowCreate: boolean;

  persistent: boolean;

  placeholder: string;

  type: ContentType;

  title: string;

  values?: Observable<ContentValue[]>;

  selected$: Subject<ContentMeta> = new Subject();

  created$: Subject<ContentCreateRequest> = new Subject();

  constructor(
    private dialogRef: MatDialogRef<ContentInputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ContentInputDialogData) {
    this.allowCreate = data.allowCreate;
    this.persistent = data.persistent;
    this.placeholder = data.placeholder;
    this.title = data.title;
    this.type = data.type;
    this.values = data.values;
    this.dialogRef.afterClosed().subscribe(() => {
      this.created$.complete();
      this.selected$.complete();
    });
  }

  onCreate(request: ContentCreateRequest) {
    if (request.value) {
      this.created$.next(request);
    }
    if (!this.persistent) {
      this.dialogRef.close();
    }
  }

  onSelected(selection: ContentMeta) {
    if (selection.item && selection.item.id) {
      this.selected$.next(selection);
    }
    if (!this.persistent) {
      this.dialogRef.close();
    }
  }

}
