import {TestBed} from '@angular/core/testing';

import {DialogModule, DialogService, } from '.';

describe('DialogService', () => {
  let service: DialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        DialogModule
      ]
    });
    service = TestBed.inject(DialogService);
  });

  it('should have more unit tests', () => {});

});
