import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {NgModule} from '@angular/core';

import {AutoInputModule} from '@app/shared/auto-input';

import {
  GenericConfirmDialogComponent,
  GenericInputDialogComponent,
  GenericMessageDialogComponent,
} from './generic-dialog.component';
import {ContentInputDialogComponent} from './content-input-dialog.component';


@NgModule({
  declarations: [
    ContentInputDialogComponent,
    GenericConfirmDialogComponent,
    GenericInputDialogComponent,
    GenericMessageDialogComponent,
  ],
  exports: [
    ContentInputDialogComponent,
    GenericConfirmDialogComponent,
    GenericInputDialogComponent,
    GenericMessageDialogComponent,
  ],
  imports: [
    AutoInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    MatDialogModule,
  ]
})
export class DialogModule { }
