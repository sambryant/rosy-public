import {
  Component,
  Directive,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable} from 'rxjs';


export interface GenericDialogData {
  message: string;
  title: string;
}


@Directive({})
export abstract class GenericDialogDirective {

  title: string;

  message: string;

  constructor(
    public dialogRef: MatDialogRef<GenericDialogDirective>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData) {
    this.title = data.title;
    this.message = data.message;
  }
}


@Component({
  template: `
    <h2 mat-dialog-title>{{title}}</h2>
    <div mat-dialog-content>
      <p>{{message}}</p>
    </div>
    <div mat-dialog-actions>
      <button mat-raised-button (click)="dialogRef.close(false)">
        Cancel
      </button>
      <button mat-raised-button (click)="dialogRef.close(true)" cdkFocusInitial>
        Ok
       </button>
    </div>
  `,
})
export class GenericConfirmDialogComponent extends GenericDialogDirective {}


@Component({
  template: `
    <h2 mat-dialog-title>{{title}}</h2>
    <mat-dialog-content>
      <div>
        <p *ngIf="message">{{message}}</p>
        <input #input class="rx-input mat-body-1">
      </div>
    </mat-dialog-content>
    <div mat-dialog-actions>
      <button mat-raised-button (click)="dialogRef.close(null)">
        Cancel
      </button>
      <button mat-raised-button (click)="dialogRef.close(input.value)" cdkFocusInitial>
        Ok
      </button>
    </div>
  `,
  styles: [`
    mat-dialog-content {
      display: flex;
    }
    mat-dialog-content > * {
      margin: auto;
    }
  `]
})
export class GenericInputDialogComponent extends GenericDialogDirective {}


@Component({
  template: `
    <h2 mat-dialog-title>{{title}}</h2>
    <div mat-dialog-content>
      <p>{{message}}</p>
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="dialogRef.close()" cdkFocusInitial>
        Ok
       </button>
    </div>
  `,
})
export class GenericMessageDialogComponent extends GenericDialogDirective {}
