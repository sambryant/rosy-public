import {of, Observable, Subject, } from 'rxjs';
import {take} from 'rxjs/operators';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MAT_DIALOG_DATA, MatDialogRef, } from '@angular/material/dialog';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ContentCreateRequest, ContentInputComponent, } from '@app/shared/auto-input';
import {ContentMeta} from '@app/models';
import {MockTag} from '@app/models/testing';
import {Name} from '@app/models';

import {ContentInputDialogComponent, ContentInputDialogData, } from '.';


class MockDialogRef {
  afterClosed$ = new Subject<any>();
  closed = false;

  afterClosed(): Observable<null> {
    return this.afterClosed$.pipe(take(1));
  }

  close() {
    this.closed = true;
    this.afterClosed$.next(null);
  }
}


describe('ContentInputDialogComponent', () => {
  let component: ContentInputDialogComponent;
  let fixture: ComponentFixture<ContentInputDialogComponent>;
  let dialogData: ContentInputDialogData;
  let mockDialogRef: MockDialogRef;

  beforeEach(async () => {
    mockDialogRef = new MockDialogRef();
    dialogData = {
      type: 'tag',
      title: 'test tag title',
      placeholder: 'test tag placeholder',
      persistent: false,
      allowCreate: false
    };

    await TestBed.configureTestingModule({
      declarations: [
        ContentInputDialogComponent,
      ],
      imports: [
        NoopAnimationsModule,
      ],
      providers: [
        {provide: MAT_DIALOG_DATA, useValue: dialogData},
        {provide: MatDialogRef, useValue: mockDialogRef}
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ] // Ignores component not in imports, declarations, or providers
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentInputDialogComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should emit selected tag and close when persistent is false', () => {
    component.persistent = false;
    fixture.detectChanges();
    const selection: ContentMeta = {
      type: 'tag',
      item: MockTag.new(1)
    };
    const sub = component.selected$.subscribe(actualSelection => {
      expect(actualSelection).toEqual(selection);
    });
    component.onSelected(selection);
    expect(sub.closed).toBe(true);
    expect(mockDialogRef.closed).toBe(true);
  });

  it('should emit created content', () => {
    component.allowCreate = true;
    component.persistent = false;
    fixture.detectChanges();
    const request: ContentCreateRequest = {
      type: 'artist',
      value: 'new artist name'
    };
    const sub = component.created$.subscribe(created => {
      expect(created).toEqual(request);
    });
    component.onCreate(request);
    expect(sub.closed).toBe(true);
  });

  it('should not emit created content when value is empty', () => {
    component.allowCreate = true;
    component.persistent = true;
    fixture.detectChanges();
    const request: ContentCreateRequest = {
      type: 'artist',
      value: ''
    };
    const sub = component.created$.subscribe(() => {throw new Error()});
    component.onCreate(request);
    expect(sub.closed).toBe(false);
  });

  it('should emit stream of selected tags when persistent is true', () => {
    component.persistent = true;
    fixture.detectChanges();
    const selections: ContentMeta[] = [
      {type: 'tag', item: MockTag.new(1)},
      {type: 'tag', item: MockTag.new(2)},
      {type: 'tag', item: MockTag.new(3)},
    ];
    let counter = 0;
    const sub = component.selected$.subscribe(actualSelection => {
      expect(actualSelection).toEqual(selections[counter]);
      counter += 1;
    });
    selections.forEach(selection => {
      component.onSelected(selection);
    });
    expect(sub.closed).toBe(false);
    mockDialogRef.close();
    expect(sub.closed).toBe(true);
  });

  it('should ignore empty values', () => {
    component.persistent = true;
    fixture.detectChanges();
    const selections: ContentMeta[] = [
      {type: 'tag', item: MockTag.new(1)},
      {type: 'name', item: {value: 'hello'} as any as Name},
      {type: 'tag', item: null as any as Name},
    ];
    let counter = 0;
    const sub = component.selected$.subscribe(actualSelection => {
      expect(actualSelection).toEqual(selections[0]);
      counter += 1;
    });
    selections.forEach(selection => {
      component.onSelected(selection);
    });
    mockDialogRef.close();
    expect(sub.closed).toBe(true);
    expect(counter).toBe(1);
  });
});
