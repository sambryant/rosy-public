import {Component} from '@angular/core';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {Action, OperationMode, Name, NameId, TaggedContent, Tag, TagId, } from '@app/models';
import {ActionService, ConfigService, } from '@app/services';
import {MockArtist, MockGroup, MockName, MockTag, } from '@app/models/testing';
import {QuickActionService} from '@app/quick-action';

import {BaseTaggedEditorDirective, ContentEditorModule, } from '.';


describe('BaseTaggedEditorDirective', () => {
  let component: TestContentEditorComponent;
  let fixture: ComponentFixture<TestContentEditorComponent>;

  let configService: ConfigService;
  let target: TaggedContent;
  let saveSpy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TestContentEditorComponent
      ],
      imports: [
        NoopAnimationsModule,
      ]
    })
    .compileComponents();

    configService = TestBed.inject(ConfigService);
    configService.saveAfterChange = false;
    target = { id: 1, names: [], tags: [] };

    fixture = TestBed.createComponent(TestContentEditorComponent);
    component = fixture.componentInstance;
    component.target = target;
    saveSpy = spyOn(component, 'doSaveWatcher');
    fixture.detectChanges();
  });

  it('should save after modifying when saveOnChange is true', () => {
    // When saveAfterChange is true, any modification triggers a save so isModified is always false.
    configService.saveAfterChange = true;

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.isModified()).toBeFalse();
    component.markModified();
    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(component.isModified()).toBeFalse();
  });

  it('should not save after modifying when saveOnChange is false', () => {
    configService.saveAfterChange = false;

    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.isModified()).toBeFalse();
    component.markModified();
    expect(saveSpy).not.toHaveBeenCalled();
    expect(component.isModified()).toBeTrue();
  });

  it('should not save the target when not modified', () => {
    configService.saveAfterChange = false;
    component.save();
    expect(saveSpy).not.toHaveBeenCalled();
  });

  it('should mark target as unmodified after a save', () => {
    component.markModified();

    expect(component.isModified()).toBeTrue();
    component.save();
    expect(component.isModified()).toBeFalse();
  });

  it('should mark target as unmodified after it changes', () => {
    component.target = { id: 1, names: [], tags: [] };
    fixture.detectChanges();
    component.markModified();

    expect(component.target.id).toBe(1);
    expect(component.isModified()).toBeTrue();
    component.target =  { id: 2, names: [], tags: [] };
    expect(component.target.id).toBe(2);
    expect(component.isModified()).toBeFalse();
  });

  describe('captured actions', () => {
    let actionService: ActionService;
    let quickActionService: QuickActionService;

    beforeEach(() => {
      actionService = TestBed.inject(ActionService);
      quickActionService = TestBed.inject(QuickActionService);
    });

    it('should disable actions when readonly is true', () => {
      component.target = { id: 2, names: [], tags: [] };
      component.readonly = false;
      fixture.detectChanges();
      expect(actionService.isActionEnabled(Action.QuickAction1)).toBeTrue();

      component.target = { id: 2, names: [], tags: [] };
      component.readonly = true;
      fixture.detectChanges();
      expect(actionService.isActionEnabled(Action.QuickAction1)).toBeFalse();

      component.target = null;
      component.readonly = false;
      fixture.detectChanges();
      expect(actionService.isActionEnabled(Action.QuickAction1)).toBeFalse();
    });

    describe('name quick actions', () => {
      function runTest(mode: OperationMode, value: NameId | null, args: {old: NameId[], exp: NameId[], modified: boolean}) {
        const qa = quickActionService.getQuickAction(Action.QuickAction3);
        qa.mode = mode;
        qa.target = value === null ? {type: 'name', item: null} : {type: 'name', item: MockName.new(value)};

        target.names = args.old.map((id) => MockName.new(id));

        actionService.doAction(Action.QuickAction3);

        expect(target.names.map((n) => n.id)).toEqual(args.exp);
        expect(component.isModified()).toBe(args.modified);

        component.save(); // so that modified is reset
      }

      it('should add names', () => {
        runTest('add', 42, { old: [1, 2], exp: [1, 2, 42], modified: true });
      });

      it('should not add duplicate names', () => {
        runTest('add', 1, { old: [1, 2], exp: [1, 2], modified: false });
      });

      it('should not add empty names', () => {
        runTest('add', null, { old: [1, 2], exp: [1, 2], modified: false });
      });

      it('should toggle names', () => {
        runTest('toggle', 42, { old: [1, 2, 42], exp: [1, 2], modified: true });
        runTest('toggle', 42, { old: [1, 2], exp: [1, 2, 42], modified: true });
      });

      it('should not toggle empty names', () => {
        runTest('toggle', null, { old: [1, 2], exp: [1, 2], modified: false });
      });

      it('should remove names', () => {
        runTest('remove', 2, { old: [1, 2, 3], exp: [1, 3], modified: true });
        runTest('remove', 42, { old: [1, 2, 3], exp: [1, 2, 3], modified: false });
      });
    });

    describe('tag quick actions', () => {
      function runTest(mode: OperationMode, value: TagId | null, args: {old: TagId[], exp: TagId[], modified: boolean}) {
        const qa = quickActionService.getQuickAction(Action.QuickAction3);
        qa.mode = mode;
        qa.target = value === null ? {type: 'tag', item: null} : {type: 'tag', item: MockTag.new(value)};

        target.tags = args.old.map((id) => MockTag.new(id));

        actionService.doAction(Action.QuickAction3);

        expect(target.tags.map((n) => n.id)).toEqual(args.exp);
        expect(component.isModified()).toBe(args.modified);

        component.save(); // so that modified is reset
      }

      it('should add tags', () => {
        runTest('add', 42, { old: [1, 2], exp: [1, 2, 42], modified: true });
      });

      it('should not add duplicate tags', () => {
        runTest('add', 1, { old: [1, 2], exp: [1, 2], modified: false });
      });

      it('should not add empty tags', () => {
        runTest('add', null, { old: [1, 2], exp: [1, 2], modified: false });
      });

      it('should toggle tags', () => {
        runTest('toggle', 42, { old: [1, 2, 42], exp: [1, 2], modified: true });
        runTest('toggle', 42, { old: [1, 2], exp: [1, 2, 42], modified: true });
      });

      it('should not toggle empty tags', () => {
        runTest('toggle', null, { old: [1, 2], exp: [1, 2], modified: false });
      });

      it('should remove tags', () => {
        runTest('remove', 2, { old: [1, 2, 3], exp: [1, 3], modified: true });
        runTest('remove', 42, { old: [1, 2, 3], exp: [1, 2, 3], modified: false });
      });
    });

  });

  describe('addRelated', () => {

    it('should add a new tag', () => {
      expect(component.isModified()).toBeFalse();
      expect(target.tags).toEqual([]);
      component.addRelated({type: 'tag', item: MockTag.new(1)});
      expect(component.isModified()).toBeTrue();
      expect(target.tags).toEqual([MockTag.new(1)]);
    });

    it('should add a new name', () => {
      expect(component.isModified()).toBeFalse();
      expect(target.names).toEqual([]);
      component.addRelated({type: 'name', item: MockName.new(1)});
      expect(component.isModified()).toBeTrue();
      expect(target.names).toEqual([MockName.new(1)]);
    });

    it('should do nothing for artist and group', () => {
      expect(component.isModified()).toBeFalse();
      component.addRelated({type: 'artist', item: MockArtist.new(1)});
      component.addRelated({type: 'group', item: MockGroup.new(1)});
      expect(component.isModified()).toBeFalse();
    });

  });

});


@Component({
  template: '<div></div>'
})
class TestContentEditorComponent extends BaseTaggedEditorDirective<TaggedContent> {

  constructor(
    configService: ConfigService,
    actionService: ActionService,
    quickActionService: QuickActionService) {
    super(configService, actionService, quickActionService);
  }

  protected updateFilterLink() {}

  protected doSave() {
    this.doSaveWatcher();
  }

  createRelated() {}

  doSaveWatcher() {}
}
