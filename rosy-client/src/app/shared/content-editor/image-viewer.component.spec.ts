import {
  fakeAsync,
  flush,
  tick,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';
import {of, NEVER, Subject, } from 'rxjs';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatExpansionModule, MatExpansionPanel, } from '@angular/material/expansion';
import {MatMenuModule} from '@angular/material/menu';
import {MockComponent, MockProvider, } from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {
  Action,
  Artist,
  ArtistId,
  ContentValue,
  Group,
  GroupId,
  GroupSimple,
  Image,
  OperationMode,
  RATE_ACTIONS,
} from '@app/models';
import {
  ActionService,
  ApiService,
  ConfigService,
  LibraryService,
  NotificationService,
} from '@app/services';
import {
  MockArtist,
  MockGroup,
  MockImage,
  MockName,
  MockTag,
} from '@app/models/testing';
import {ContentInputComponent} from '@app/shared/auto-input';
import {DialogService} from '@app/shared/dialog';
import {EditableModule} from '@app/shared/editable';
import {PipesModule} from '@app/shared/pipes';
import {QuickActionService} from '@app/quick-action';

import {ImageViewerComponent, } from '.';


describe('ImageViewerComponent', () => {
  let libraryService: LibraryService;
  let navigateRequestSpy;
  let configService: ConfigService;
  let component: ImageViewerComponent;
  let fixture: ComponentFixture<ImageViewerComponent>;
  let image: Image;
  let saveSpy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ImageViewerComponent,
        MockComponent(ContentInputComponent),
      ],
      imports: [
        EditableModule,
        MatExpansionModule, // we need this because otherwise changes in info panel are ignored.
        MatMenuModule,
        NoopAnimationsModule,
        PipesModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
    libraryService = TestBed.inject(LibraryService);
    spyOn(libraryService, 'getContentList').and.returnValue(of([]));
    spyOn(libraryService, 'rateImage').and.returnValue(of([]));
    saveSpy = spyOn(libraryService, 'updateImage');
    spyOn(libraryService, 'getImageBlobUrl').and.returnValue(NEVER);

    configService = TestBed.inject(ConfigService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageViewerComponent);
    configService.saveAfterChange = false;
    component = fixture.componentInstance;
    image = MockImage.new(1);
    component.target = image;
    component.allowNavigation = true;
    fixture.detectChanges();
    navigateRequestSpy = spyOn(component.navigationRequest, 'emit');
  });

  it('should save target', () => {
    component.markModified();
    component.save();
    expect(libraryService.updateImage).toHaveBeenCalledOnceWith(image, false);
  });

  it('should reset givenRating after target changes', () => {
    component.target = MockImage.new(1);
    fixture.detectChanges();
    component.rateImage(3, false);

    expect(component.givenRating).toBe(3);
    component.target = MockImage.new(2);
    expect(component.givenRating).toBe(0);
  });

  it('should do nothing', () => {
    expect(true).toBeTrue();
  })

  describe('captured actions', () => {
    let actionService: ActionService;
    let quickActionService: QuickActionService;

    beforeEach(() => {
      actionService = TestBed.inject(ActionService);
      quickActionService = TestBed.inject(QuickActionService);

      spyOn(component.editable, 'edit');
    });

    it('should start a new name', () => {
      component.input.type = 'tag';
      actionService.doAction(Action.AddName);
      expect(component.input.type).toEqual('name');
      expect(component.editable.edit).toHaveBeenCalledTimes(1);
    });

    it('should start a new name, even when info panel is collapsed', fakeAsync(() => {
      component.infoPanel.close();
      fixture.detectChanges();
      flush();

      expect(component.infoPanel.expanded).toBeFalse();

      actionService.doAction(Action.AddName);
      expect(component.editable.edit).not.toHaveBeenCalled();

      fixture.detectChanges();
      flush();

      expect(component.editable.edit).toHaveBeenCalledTimes(1);
    }));

    it('should start a new tag', () => {
      component.input.type = 'name';
      actionService.doAction(Action.AddTag);
      expect(component.input.type).toEqual('tag');
      expect(component.editable.edit).toHaveBeenCalledTimes(1);
    });

    it('should start editing the artist', () => {
      component.input.type = 'name';
      actionService.doAction(Action.AddArtist);
      expect(component.input.type).toEqual('artist');
      expect(component.editable.edit).toHaveBeenCalledTimes(1);
    });

    it('should start editing the group', () => {
      component.input.type = 'name';
      actionService.doAction(Action.AddGroup);
      expect(component.input.type).toEqual('group');
      expect(component.editable.edit).toHaveBeenCalledTimes(1);
    });

    it('should show marked dupes', () => {
      const dialogService = TestBed.inject(DialogService);
      spyOn(dialogService, 'open');

      component.target = new MockImage(1, 'img1');
      expect(actionService.getRegisteredAction(Action.ShowMarkedDupes).enabled$.value).toBeFalse();
      actionService.doAction(Action.ShowMarkedDupes);
      expect(dialogService.open).not.toHaveBeenCalled();

      let fakeTarget = new MockImage(1, 'img1');
      fakeTarget.dupes = {1234: 'Exact'};
      component.target = fakeTarget;
      expect(actionService.getRegisteredAction(Action.ShowMarkedDupes).enabled$.value).toBeTrue();
      actionService.doAction(Action.ShowMarkedDupes);
      expect(dialogService.open).toHaveBeenCalledTimes(1);
    });

    it('should show matches', () => {
      const dialogService = TestBed.inject(DialogService);
      spyOn(dialogService, 'open');
      actionService.doAction(Action.ShowFaceMatches);
      expect(dialogService.open).toHaveBeenCalledTimes(1);
    });

    it('should rate images', () => {
      const rateSpy = spyOn(component, 'rateImage');
      [1, 2, 3, 4, 5].forEach((i) => {
        actionService.doAction(RATE_ACTIONS[i - 1]);
        expect(rateSpy).toHaveBeenCalledOnceWith(i, false);
        rateSpy.calls.reset();
      });
    });

    function runTest(action: Action, args: {oldImg: Partial<Image>, expImg: Partial<Image>, modified: boolean}) {
      // Set image starting state to specified values.
      for (let prop in args.oldImg) {
        image[prop] = args.oldImg[prop];
      }

      actionService.doAction(action);

      // Check that image values are as expected.
      for (let prop in args.expImg) {
        expect(image[prop]).toEqual(args.expImg[prop]);
      }
      expect(component.isModified()).toBe(args.modified);

      component.save(); // so that modified is reset
    }

    it('should toggle favorite', () => {
      const action = Action.ToggleFavorite;
      runTest(action, { oldImg: {favorite: false}, expImg: {favorite: true}, modified: true });
      runTest(action, { oldImg: {favorite: true}, expImg: {favorite: false}, modified: true });
    });

    it('should toggle hidden', () => {
      const action = Action.ToggleHidden;
      runTest(action, { oldImg: {hidden: false}, expImg: {hidden: true}, modified: true });
      runTest(action, { oldImg: {hidden: true}, expImg: {hidden: false}, modified: true });
    });

    it('should toggle processed', () => {
      const action = Action.ToggleProcessed;
      runTest(action, { oldImg: {processed: false}, expImg: {processed: true}, modified: true });
      runTest(action, { oldImg: {processed: true}, expImg: {processed: false}, modified: true });
    });

    describe('artist quick actions', () => {

      function runQATest(mode: OperationMode, target: ArtistId, args: {old: ArtistId | null, exp: ArtistId | null, modified: boolean}) {
        const qa = quickActionService.getQuickAction(Action.QuickAction1);
        qa.mode = mode;
        qa.target.type = 'artist';
        qa.target.item = MockArtist.new(target);
        runTest(Action.QuickAction1, {
          oldImg: {artistId: args.old},
          expImg: {artistId: args.exp},
          modified: args.modified
        });
      }

      it('should add an artist', () => {
        // If starting image has no artist, it should be added and modified.
        runQATest('add', 42, {old: null, exp: 42, modified: true});

        // If starting image has a different artist, it should be added and modified.
        runQATest('add', 42, {old: 13, exp: 42, modified: true});

        // If starting image has the same artist, it should not be modified.
        runQATest('add', 42, {old: 42, exp: 42, modified: false});
      });

      it('should remove an artist', () => {
        // If starting image has no artist, nothing should change.
        runQATest('remove', 42, {old: null, exp: null, modified: false});

        // If starting image has a different artist, nothing should change.
        runQATest('remove', 42, {old: 13, exp: 13, modified: false});

        // If starting image has target artist, it should be removed and modified.
        runQATest('remove', 42, {old: 42, exp: null, modified: true});
      });

      it('should toggle an artist', () => {
        // If starting image has no artist, it should add new artist.
        runQATest('toggle', 42, {old: null, exp: 42, modified: true});

        // If starting image has different artist, it should add new artist
        runQATest('toggle', 42, {old: 13, exp: 42, modified: true});

        // If starting image has target artist, it should be removed and modified.
        runQATest('toggle', 42, {old: 42, exp: null, modified: true});
      });
    });

    describe('group quick actions', () => {

      function runQATest(mode: OperationMode, target: GroupId, args: {old: GroupId | null, exp: GroupId | null, modified: boolean}) {
        const qa = quickActionService.getQuickAction(Action.QuickAction1);
        qa.mode = mode;
        qa.target.type = 'group';
        qa.target.item = MockGroup.new(target);
        runTest(Action.QuickAction1, {
          oldImg: {groupId: args.old},
          expImg: {groupId: args.exp},
          modified: args.modified
        });
      }

      it('should add a group', () => {
        // If starting image has no group, it should be added and modified.
        runQATest('add', 42, {old: null, exp: 42, modified: true});

        // If starting image has a different group, it should be added and modified.
        runQATest('add', 42, {old: 13, exp: 42, modified: true});

        // If starting image has the same group, it should not be modified.
        runQATest('add', 42, {old: 42, exp: 42, modified: false});
      });

      it('should remove a group', () => {
        // If starting image has no group, nothing should change.
        runQATest('remove', 42, {old: null, exp: null, modified: false});

        // If starting image has a different group, nothing should change.
        runQATest('remove', 42, {old: 13, exp: 13, modified: false});

        // If starting image has target group, it should be removed and modified.
        runQATest('remove', 42, {old: 42, exp: null, modified: true});
      });

      it('should toggle a group', () => {
        // If starting image has no group, it should add new group.
        runQATest('toggle', 42, {old: null, exp: 42, modified: true});

        // If starting image has different group, it should add new group
        runQATest('toggle', 42, {old: 13, exp: 42, modified: true});

        // If starting image has target group, it should be removed and modified.
        runQATest('toggle', 42, {old: 42, exp: null, modified: true});
      });

      it('should tell library that group has updated when it saves', () => {
        configService.saveAfterChange = true;
        runQATest('toggle', 42, {old: null, exp: 42, modified: false});
        expect(libraryService.updateImage).toHaveBeenCalledOnceWith(image, true);

        saveSpy.calls.reset();

        configService.saveAfterChange = true;
        runQATest('add', 42, {old: 42, exp: 42, modified: false});
        expect(libraryService.updateImage).not.toHaveBeenCalled();
      });
    });

  });

  describe('template methods', () => {
    let modifySpy: jasmine.Spy;

    beforeEach(() => {
      configService.saveAfterChange = false;
      modifySpy = spyOn(component, 'markModified').and.callThrough();
    });

    it('should toggle processed state', () => {
      image.processed = false;

      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();

      expect(image.processed).toBeFalse();
      component.toggleProcessed();
      expect(image.processed).toBeTrue();
      component.toggleProcessed();
      expect(image.processed).toBeFalse();

      expect(modifySpy).toHaveBeenCalled();
      expect(component.isModified()).toBeTrue();
    });

    it('should not toggle processed state without a target', () => {
      component.target = null;
      component.toggleProcessed();
      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();
    });

    it('should toggle hidden state', () => {
      image.hidden = false;

      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();

      expect(image.hidden).toBeFalse();
      component.toggleHidden();
      expect(image.hidden).toBeTrue();
      component.toggleHidden();
      expect(image.hidden).toBeFalse();

      expect(modifySpy).toHaveBeenCalled();
      expect(component.isModified()).toBeTrue();
    });

    it('should not toggle hidden state without a target', () => {
      component.target = null;
      component.toggleHidden();
      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();
    });

    it('should toggle favorite state', () => {
      image.favorite = false;

      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();

      expect(image.favorite).toBeFalse();
      component.toggleFavorite();
      expect(image.favorite).toBeTrue();
      component.toggleFavorite();
      expect(image.favorite).toBeFalse();

      expect(modifySpy).toHaveBeenCalled();
      expect(component.isModified()).toBeTrue();
    });

    it('should not toggle favorite state without a target', () => {
      component.target = null;
      component.toggleFavorite();
      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();
    });

    it('should rate an image', () => {
      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();
      expect(libraryService.rateImage).not.toHaveBeenCalled();

      component.rateImage(1, false);
      expect(libraryService.rateImage).toHaveBeenCalledWith(image, 1, false);
      component.rateImage(3, true);
      expect(libraryService.rateImage).toHaveBeenCalledWith(image, 3, true);

      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();
    });

    it('should not rate an image without a target', () => {
      component.target = null;
      component.rateImage(1, true);
      expect(modifySpy).not.toHaveBeenCalled();
      expect(component.isModified()).toBeFalse();
    });


    it('should navigate after rating an image when nagivateOnRate is true', () => {
      configService.navigateOnRate = true;

      expect(navigateRequestSpy).not.toHaveBeenCalled();
      component.rateImage(1, false);
      expect(navigateRequestSpy).toHaveBeenCalledWith({direction: 'next'});
      expect(navigateRequestSpy).toHaveBeenCalledTimes(1);
    });

    it('should not navigate after rating an image when nagivateOnRate is false', () => {
      configService.navigateOnRate = false;

      expect(navigateRequestSpy).not.toHaveBeenCalled();
      component.rateImage(1, false);
      expect(navigateRequestSpy).not.toHaveBeenCalled();
    });

    it('should set givenRating after rating image', () => {
      expect(component.givenRating).toBe(0);
      component.rateImage(1, false);
      expect(component.givenRating).toBe(1);
      component.rateImage(3, false);
      expect(component.givenRating).toBe(3);
    });

  });

  describe('addRelated', () => {

    it('should handle name/tag stuff', () => {
      image.tags = [MockTag.new(1), MockTag.new(2)];
      component.addRelated({type: 'tag', item: MockTag.new(3)});
      expect(image.tags).toEqual([MockTag.new(1), MockTag.new(2), MockTag.new(3)]);
      image.names = [MockName.new(1), MockName.new(2)];
      component.addRelated({type: 'name', item: MockName.new(3)});
      expect(image.names).toEqual([MockName.new(1), MockName.new(2), MockName.new(3)]);
    });

    it('should add artist', () => {
      image.artistId = null;
      component.addRelated({type: 'artist', item: MockArtist.new(5)});
      expect(image.artistId as any as number).toBe(5);
      expect(component.isModified()).toBeTrue();
    });

    it('should change artist', () => {
      image.artistId = 1;
      component.addRelated({type: 'artist', item: MockArtist.new(5)});
      expect(image.artistId as any as number).toBe(5);
      expect(component.isModified()).toBeTrue();
    });

    it('should add group', () => {
      image.groupId = null;
      component.addRelated({type: 'group', item: MockGroup.new(5)});
      expect(image.groupId as any as number).toEqual(5);
      expect(component.isModified()).toBeTrue();
    });

    it('should change group', () => {
      image.groupId = 1;
      component.addRelated({type: 'group', item: MockGroup.new(5)});
      expect(image.groupId as any as number).toBe(5);
      expect(component.isModified()).toBeTrue();
    });
  });

  describe('createRelated', () => {
    let createResponseSubscribeSpy;
    let createSpy;

    beforeEach(() => {
      const response: Subject<ContentValue> = new Subject();
      createSpy = spyOn(libraryService, 'createContent').and.returnValue(response);
      createResponseSubscribeSpy = spyOn(response, 'subscribe');
    });

    it('should created related objects and bind them to image', () => {
      component.createRelated({type: 'artist', value: 'new artist'});
      expect(createResponseSubscribeSpy).toHaveBeenCalledTimes(1);
      expect(createSpy).toHaveBeenCalledOnceWith('artist', 'new artist', [image]);
      createResponseSubscribeSpy.calls.reset();
      createSpy.calls.reset();
      component.createRelated({type: 'group', value: 'new g'});
      expect(createResponseSubscribeSpy).toHaveBeenCalledTimes(1);
      expect(createSpy).toHaveBeenCalledOnceWith('group', 'new g', [image]);
      createResponseSubscribeSpy.calls.reset();
      createSpy.calls.reset();
      component.createRelated({type: 'name', value: 'new n'});
      expect(createResponseSubscribeSpy).toHaveBeenCalledTimes(1);
      expect(createSpy).toHaveBeenCalledOnceWith('name', 'new n', [image]);
      createResponseSubscribeSpy.calls.reset();
      createSpy.calls.reset();
      component.createRelated({type: 'tag', value: 'new t'});
      expect(createResponseSubscribeSpy).toHaveBeenCalledTimes(1);
      expect(createSpy).toHaveBeenCalledOnceWith('tag', 'new t', [image]);
    });

    it('should not created related objects without a target', () => {
      component.target = null;
      component.createRelated({type: 'artist', value: 'new artist'});
      expect(createResponseSubscribeSpy).not.toHaveBeenCalled();
      expect(createSpy).not.toHaveBeenCalled();
    });

  });

  describe('mouse events', () => {

    it('should turn on the overlay when the mouse enters', fakeAsync(() => {
      configService.disableImageInfoOverlay = true;

      const el = fixture.debugElement;

      expect(component.infoOverlayHidden).toBeTrue();

      el.triggerEventHandler('mouseenter', {});
      tick();
      fixture.detectChanges();

      configService.disableImageInfoOverlay = false;

      expect(component.infoOverlayHidden).toBeTrue();

      el.triggerEventHandler('mouseenter', {});
      tick();
      fixture.detectChanges();

      expect(component.infoOverlayHidden).toBeFalse();

      el.triggerEventHandler('mouseleave', {});
      tick();
      fixture.detectChanges();

      expect(component.infoOverlayHidden).toBeTrue();
    }));

  });



});
