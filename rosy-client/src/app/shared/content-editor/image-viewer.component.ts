import {
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {BehaviorSubject, ReplaySubject, } from 'rxjs';
import {take, takeUntil, } from 'rxjs/operators';
import {MatExpansionPanel} from '@angular/material/expansion';

import {
  Action,
  Artist,
  ContentMeta,
  ContentType,
  ContentTypeMap,
  CONTENT_TYPES,
  Feature,
  GroupSimple,
  Image,
  Name,
  NavigationRequest,
  OperationMode,
  RATE_ACTIONS,
  Tag,
} from '@app/models';
import {
  ActionService,
  ConfigService,
  LibraryService,
  NotificationService,
} from '@app/services';
import {ContentInputDialogData, DialogService, } from '@app/shared/dialog';
import {EditableComponent} from '@app/shared/editable';
import {MatchResultsDialogComponent} from '@app/shared/match-results-dialog';
import {QuickAction, QuickActionService, } from '@app/quick-action';
import {ContentCreateRequest, ContentInputComponent, } from '@app/shared/auto-input';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';
import {ImageFeatureOverlayComponent} from '@app/shared/image/image-feature-overlay/image-feature-overlay.component';

import {BaseTaggedEditorDirective} from './base-editor.directive';


export const CONTENT_ADD_ACTIONS: ContentTypeMap<Action> = {
  'artist': Action.AddArtist,
  'feature': Action.AddFeature,
  'group': Action.AddGroup,
  'name': Action.AddName,
  'tag': Action.AddTag,
};


@Component({
  selector: 'rx-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.scss'],
})
export class ImageViewerComponent
extends BaseTaggedEditorDirective<Image>
implements OnInit {

  readonly RemoveFeatureAction = Action.RemoveFeature;

  @Input() allowNavigation: boolean;

  /**
   * When true, allows displayed image to toggle into fullscreen mode.
   */
  @Input() allowFullscreen: boolean = true;

  /**
   * Component does not actually control selection, instead it emit requests to change images which
   * should be handled elsewhere. This allows flexibility in how this component is embedded in other
   * views.
   */
  @Output() navigationRequest = new EventEmitter<NavigationRequest>();

  /**
   * Mobile only: When true, the overlay showing brief information about image is hidden.
   */
  infoOverlayHidden = true;

  /**
   * Rating given to currently displayed image during this view session (used for mobile ratings display).
   */
  givenRating = 0;

  /**
   * Observable that emits suggested names for the image based on server call. When the target
   * changes, this will fire null and then request the suggestions from the server and fire the
   * results.
   */
  nameSuggestion: BehaviorSubject<Name[] | null> = new BehaviorSubject(null);

  /**
   * Indicates that the group for this image has been changed.
   */
  protected groupModified = false;

  /**
   * Indicates that the 'Show dupes' action should be enabled.
   */
  protected enableShowMarkedDupes$: BehaviorSubject<boolean>;

  /**
   * Indicates that the 'Femove features' action should be enabled.
   */
  protected enableRemoveFeatures$: BehaviorSubject<boolean>;

  /**
   * List of features that can be removed from the target image.
   */
  protected removableFeatures$: ReplaySubject<Feature[]>;

  contextMenuActions = [
    Action.AddTag,
    Action.AddName,
    Action.AddArtist,
    Action.AddGroup,
    Action.RemoveFeature,
    Action.ShowFaceMatches,
    Action.ShowMarkedDupes,
    Action.ToggleFavorite,
    Action.ToggleHidden,
    Action.ToggleProcessed
  ];
  contextMenuRatingActions = RATE_ACTIONS;

  @HostBinding('attr.tabindex') '-1';

  /**
   * Input used to add/change artist, group, names, or tags.
   */
  @ViewChild(ContentInputComponent) input: ContentInputComponent;

  /**
   * Editable wrapper around form used to add/change artist, group, names, or tags.
   */
  @ViewChild(EditableComponent) editable: EditableComponent;

  /**
   * Panel that hides/shows image details (non-mobile only).
   */
  @ViewChild(MatExpansionPanel) infoPanel: MatExpansionPanel;

  /**
   * Component which displays image features over image and is responsible for logic of adding new
   * features to an image. It's not displayed unless configService.showImageFeatures is on.
   */
  @ViewChild(ImageFeatureOverlayComponent) featureOverlay: ImageFeatureOverlayComponent;

  constructor(
    actionService: ActionService,
    configService: ConfigService,
    quickActionService: QuickActionService,
    protected dialogService: DialogService,
    protected libraryService: LibraryService,
    protected notificationService: NotificationService,
  ) {
    super(configService, actionService, quickActionService);
    this.enableShowMarkedDupes$ = new BehaviorSubject(false);
    this.enableRemoveFeatures$ = new BehaviorSubject(false);
    this.removableFeatures$ = new ReplaySubject();
    this.targetChanged.subscribe(() => {
      this.givenRating = 0;
      this.groupModified = false;
      this.recomputeActionStates();
      this.recomputeNameSuggestion();
    });
  }

  protected recomputeNameSuggestion() {
    // Fire null to "clear" the name suggestion
    if (this.nameSuggestion.value !== null) {
      this.nameSuggestion.next(null);
    }
    // Start request for name suggestions for current target only if its not a video and not already
    // tagged with a name.
    if (this._target && this._target.names.length === 0 && !this._target.isVideo) {
      this.libraryService.faceSuggest(this._target.id)
        .pipe(takeUntil(this.destroyed$))
        .pipe(takeUntil(this.targetChanged))
        .subscribe((suggested: Name[]) => {
          if (suggested) {
            this.nameSuggestion.next(suggested);
          }
        });
    }
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.libraryService.imageUpdated$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.recomputeActionStates();
      });
    this.actionService.captureAction(Action.ToggleFavorite, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.toggleFavorite(); });
    this.actionService.captureAction(Action.ToggleHidden, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.toggleHidden(); });
    this.actionService.captureAction(Action.ToggleMarked, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.toggleMarked(); });
    this.actionService.captureAction(Action.ToggleProcessed, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.toggleProcessed(); });
    this.actionService.captureAction(Action.ShowMarkedDupes, this.enableShowMarkedDupes$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.dialogService.open(MatchResultsDialogComponent, {data: {
          type: 'dupes',
          target: this._target
        }});
      });
    this.actionService.captureAction(Action.ShowFaceMatches, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.dialogService.open(MatchResultsDialogComponent, {data: {
          type: 'faces',
          target: this._target
        }});
      });
    CONTENT_TYPES.forEach((type: ContentType) => {
      this.actionService.captureAction(CONTENT_ADD_ACTIONS[type], this.hasEditableTarget$)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => { this.startEditingRelated(type); });
    });
    [1, 2, 3, 4, 5].forEach((i) => {
      this.actionService.captureAction(RATE_ACTIONS[i - 1], this.hasEditableTarget$)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => { this.rateImage(i, false); });
    });
    this.actionService.captureAction(Action.RemoveFeature, this.enableRemoveFeatures$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.openRemoveFeatureDialog());
  }

  protected recomputeActionStates() {
    this.enableShowMarkedDupes$.next((this._target && Object.keys(this._target.dupes).length !== 0) || false);
    this.enableRemoveFeatures$.next(!!this._target && !!(this._target.features.length));
    if (!!this._target && !!this._target.features.length) {
      this.removableFeatures$.next(this._target.features.map((f) => {
        return {id: f.id, value: f.value, createdTime: 0};
      }));
    } else {
      this.removableFeatures$.next([]);
    }
  }

  protected updateFilterLink() {}

  protected doSave(target: Image) {
    this.libraryService.updateImage(target, this.groupModified);
  }

  createRelated(request: ContentCreateRequest) {
    if (this._target) {
      this.libraryService.createContent(request.type, request.value, [this._target]).subscribe();
    }
  }

  protected openRemoveFeatureDialog() {
    // If the features are hidden, show them automatically.
    if (!this.configService.showImageFeatures$.value) {
      this.configService.showImageFeatures$.next(!this.configService.showImageFeatures$.value);
    }
    // Open a dialog allowing user to remove the features added to this target.
    const target = this._target;
    if (target) {
      const data: ContentInputDialogData = {
        type: 'feature',
        title: `Remove feature from selected`,
        placeholder: `Choose feature`,
        allowCreate: false,
        persistent: true,
        values: this.removableFeatures$,
      };
      this.dialogService.showContentInputDialog(data).selected
        .pipe(takeUntil(this.destroyed$))
        .subscribe((result: ContentMeta) => {
          if (result.type === 'feature') {
            this.libraryService.deleteImageFeature(target, result.item.id);
          }
        });
    }
  }

  protected override doQuickAction(quickAction: Action) {
    let {mode, target} = this.quickActionService.getQuickAction(quickAction);
    if (target && target.item) {
      if (target.type === 'tag' || target.type === 'name') {
        this.modifyTaglike(mode, target.type, target.item);
      } else if (target.type === 'artist') {
        this.modifyArtist(mode, target.item);
      } else if (target.type === 'group') {
        this.modifyGroup(mode, target.item);
      } else if (target.type === 'feature' && mode === 'add') {
        this.featureOverlay.startNewFeature(target.item);
      }
    }
  }

  @HostListener('mouseenter')
  _onMouseEnter(): void {
    if (!this.configService.disableImageInfoOverlay) {
      this.infoOverlayHidden = false;
    }
  }

  @HostListener('mouseleave')
  _onMouseLeave(): void {
    this.infoOverlayHidden = true;
  }

  toggleFavorite(): void {
    if (this._target) {
      this._target.favorite = !this._target.favorite;
      this.markModified();
    }
  }

  toggleHidden(): void {
    if (this._target) {
      this._target.hidden = !this._target.hidden;
      this.markModified();
    }
  }

  toggleMarked(): void {
    if (this._target) {
      this._target.marked = !this._target.marked;
      this.markModified();
    }
  }

  toggleProcessed(): void {
    if (this._target) {
      this._target.processed = !this._target.processed;
      this.markModified();
    }
  }

  rateImage(value: number, clearExisting: boolean): void {
    if (this._target) {
      const target = this._target;
      this.givenRating = value;
      this.libraryService.rateImage(this._target, value, clearExisting)
        .subscribe(() => {
          if (target.ratingCount > 1) {
            const avg = (Math.round(target.ratingAverage! * 10) / 10).toString();
            this.notificationService.info(`score: ${avg} (${target.ratingCount})`);
          }
        })
      if (this.configService.navigateOnRate && this.allowNavigation) {
        this.navigationRequest.emit({direction: 'next'});
      }
    }
  }

  override addRelated(selection: ContentMeta) {
    if (selection.type === 'feature') {
      this.featureOverlay.startNewFeature(selection.item);
    } else if (selection.type === 'tag' || selection.type === 'name') {
      this.modifyTaglike('add', selection.type, selection.item)
    } else if (selection.type === 'artist') {
      this.modifyArtist('add', selection.item);
    } else if (selection.type === 'group') {
      this.modifyGroup('add', selection.item);
    }
  }

  protected startEditingRelated(type: ContentType) {
    // We need to set the editable component containing the related input to be in edit mode.
    // If the editable component is within an expansion panel which is collapsed, then it's not in
    // the DOM yet. In this case, we have to request the expansion panel to open, allow the change
    // to propagate to the DOM, and THEN put it in edit mode.
    if (this.infoPanel && !this.infoPanel.expanded) {
      this.infoPanel.afterExpand
        .pipe(take(1))
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => this.startEditingRelated(type));
      this.infoPanel.open();
    } else {
      this.input.type = type;
      this.editable.edit();
    }
  }

  protected modifyArtist(mode: OperationMode, item: Artist) {
    if (this._target!.artistId === item.id) {
      // If given ID matches existing ID, we set it to null for Remove, Toggle, and ignore for Add.
      if (mode !== 'add') {
        this._target!.artistId = null;
        this.markModified();
      }
    } else {
      // If given ID doesn't match existing ID, we change it to new ID for Add and Toggle.
      if (mode !== 'remove') {
        this._target!.artistId = item.id;
        this.markModified();
      }
    }
  }

  protected modifyGroup(mode: OperationMode, item: GroupSimple) {
    if (this._target!.groupId === item.id) {
      // If given ID matches existing ID, we set it to null for Remove, Toggle, and ignore for Add.
      if (mode !== 'add') {
        this._target!.groupId = null;
        this.groupModified = true;
        this.markModified();
      }
    } else {
      // If given ID doesn't match existing ID, we change it to new ID for Add and Toggle.
      if (mode !== 'remove') {
        this._target!.groupId = item.id;
        this.groupModified = true;
        this.markModified();
      }
    }
  }

}
