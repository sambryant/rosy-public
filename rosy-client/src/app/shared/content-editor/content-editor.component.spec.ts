import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MockComponent, MockProvider, } from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {Subject} from 'rxjs';

import {
  Action,
  ArtistDetails,
  ContentValue,
  Group,
  Name,
  NameDetails,
  Tag,
  TagDetails,
} from '@app/models';
import {
  ActionService,
  ApiService,
  ConfigService,
  LibraryService,
  NotificationService,
} from '@app/services';
import {
  MockArtistD,
  MockGroup,
  MockName,
  MockNameD,
  MockTag,
  MockTagD,
} from '@app/models/testing';
import {ContentInputComponent, } from '@app/shared/auto-input';
import {DialogService} from '@app/shared/dialog';

import {
  ArtistEditorComponent,
  ContentEditorModule,
  GroupEditorComponent,
  NameEditorComponent,
  TagEditorComponent,
} from '.';



describe('ArtistEditorComponent', () => {
  let component: ArtistEditorComponent;
  let fixture: ComponentFixture<ArtistEditorComponent>;
  let libraryService: LibraryService;
  let target: MockArtistD;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ArtistEditorComponent,
      ],
      imports: [
        FormsModule,
        NoopAnimationsModule,
        RouterTestingModule, // actually is needed due for routerLink directives
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();

    libraryService = TestBed.inject(LibraryService);

    target = MockArtistD.new(1);

    fixture = TestBed.createComponent(ArtistEditorComponent);
    component = fixture.componentInstance;
    component.target = target;
    fixture.detectChanges();
  });

  it('should have correct title', () => {
    expect(component.titleLabel).toEqual('Artist');
  });

  it('should save target', () => {
    spyOn(libraryService, 'updateArtist');
    component.markModified();
    component.save();
    expect(libraryService.updateArtist).toHaveBeenCalledOnceWith(target);
  });

  it('should update query params when target changes', () => {
    const fakeItems = [
      MockArtistD.new(1),
      MockArtistD.new(2),
    ];
    const expParams = [
      {artists: '1'},
      {artists: '2'},
    ]
    component.target = fakeItems[0];
    expect(component._filterLinkParams).toEqual(expParams[0]);
    component.target = fakeItems[1];
    expect(component._filterLinkParams).toEqual(expParams[1]);
  });

  it('should delete a artist when confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteArtist');

    component.delete();

    expect(dialogService.showConfirm).toHaveBeenCalledTimes(1);
    expect(libraryService.deleteArtist).not.toHaveBeenCalled();

    dialogResponse.next(true);

    expect(libraryService.deleteArtist).toHaveBeenCalledOnceWith(42);
  });

  it('should not delete a artist when not confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteArtist');

    component.delete();
    dialogResponse.next(false);

    expect(libraryService.deleteArtist).not.toHaveBeenCalled();
  });

});


describe('GroupEditorComponent', () => {
  let component: GroupEditorComponent;
  let fixture: ComponentFixture<GroupEditorComponent>;
  let libraryService: LibraryService;
  let target: MockGroup;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GroupEditorComponent,
        MockComponent(ContentInputComponent),
      ],
      imports: [
        FormsModule,
        NoopAnimationsModule,
        RouterTestingModule, // actually is needed due for routerLink directives
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();

    libraryService = TestBed.inject(LibraryService);

    target = MockGroup.new(1);

    fixture = TestBed.createComponent(GroupEditorComponent);
    component = fixture.componentInstance;
    component.target = target;
    fixture.detectChanges();
  });

  it('should update query params when target changes', () => {
    const fakeItems = [
      MockGroup.new(1),
      MockGroup.new(2),
    ];
    const expParams = [
      {groups: '1'},
      {groups: '2'},
    ]
    component.target = fakeItems[0];
    expect(component._filterLinkParams).toEqual(expParams[0]);
    component.target = fakeItems[1];
    expect(component._filterLinkParams).toEqual(expParams[1]);
  });

  it('should save target', () => {
    spyOn(libraryService, 'updateGroup');
    component.markModified();
    component.save();
    expect(libraryService.updateGroup).toHaveBeenCalledOnceWith(target);
  });

  it('should delete a group when confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteGroup');

    component.delete();

    expect(dialogService.showConfirm).toHaveBeenCalledTimes(1);
    expect(libraryService.deleteGroup).not.toHaveBeenCalled();

    dialogResponse.next(true);

    expect(libraryService.deleteGroup).toHaveBeenCalledOnceWith(42);
  });

  it('should not delete a group when not confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteGroup');

    component.delete();
    dialogResponse.next(false);

    expect(libraryService.deleteGroup).not.toHaveBeenCalled();
  });

  describe('captured actions', () => {
    let actionService: ActionService;

    beforeEach(() => {
      actionService = TestBed.inject(ActionService);
      spyOn(component.nameInput, 'open');
      spyOn(component.tagInput, 'open');
      (TestBed.inject(ConfigService) as ConfigService).saveAfterChange = false;
    });

    it('should start a new name', () => {
      actionService.doAction(Action.AddName);
      expect(component.tagInput.open).not.toHaveBeenCalled();
      expect(component.nameInput.open).toHaveBeenCalledTimes(1);
    });

    it('should start a new tag', () => {
      actionService.doAction(Action.AddTag);
      expect(component.tagInput.open).toHaveBeenCalledTimes(1);
      expect(component.nameInput.open).not.toHaveBeenCalled();
    });

    it('should toggle hidden', () => {
      target.hidden = true;
      actionService.doAction(Action.ToggleHidden);
      expect(target.hidden).toBe(false);
      expect(component.isModified()).toBeTrue();

      actionService.doAction(Action.ToggleHidden);
      expect(target.hidden).toBe(true);
      expect(component.isModified()).toBeTrue();
    });

  });

  describe('createRelated', () => {
    let createSpy: jasmine.Spy;
    let createResponse: Subject<ContentValue>;

    beforeEach(() => {
      (TestBed.inject(ConfigService) as ConfigService).saveAfterChange = false;
      createResponse = new Subject();
      createSpy = spyOn(libraryService, 'createContent').and.returnValue(createResponse);;
    });

    it('should create a new name and then add it to the group', () => {
      const mockNewName = MockName.new(4);

      expect(component.isModified()).toBeFalse();
      expect(target.names).toEqual([]);

      component.createRelated({type: 'name', value: 'a new name'});

      expect(component.isModified()).toBeFalse();
      expect(target.names).toEqual([]);
      expect(createSpy).toHaveBeenCalledOnceWith('name', 'a new name', []);

      createResponse.next(mockNewName);

      expect(component.isModified()).toBeTrue();
      expect(target.names).toEqual([MockName.new(4)]);
    });

    it('should create a new tag and then add it to the group', () => {
      const mockNewTag = MockTag.new(4);

      expect(component.isModified()).toBeFalse();
      expect(target.tags).toEqual([]);

      component.createRelated({type: 'tag', value: 'a new tag'});

      expect(component.isModified()).toBeFalse();
      expect(target.tags).toEqual([]);
      expect(createSpy).toHaveBeenCalledOnceWith('tag', 'a new tag', []);

      createResponse.next(mockNewTag);

      expect(component.isModified()).toBeTrue();
      expect(target.tags).toEqual([MockTag.new(4)]);
    });

    it('should ignore other content types', () => {
      component.createRelated({type: 'artist', value: 'a new tag'});
      expect(createSpy).not.toHaveBeenCalled();
      component.createRelated({type: 'group', value: 'a new tag'});
      expect(createSpy).not.toHaveBeenCalled();
    });

  });

});


describe('NameEditorComponent', () => {
  let component: NameEditorComponent;
  let fixture: ComponentFixture<NameEditorComponent>;
  let libraryService: LibraryService;
  let target: MockNameD;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        NameEditorComponent,
      ],
      imports: [
        FormsModule,
        NoopAnimationsModule,
        RouterTestingModule, // actually is needed due for routerLink directives
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();

    libraryService = TestBed.inject(LibraryService);

    target = MockNameD.new(1);

    fixture = TestBed.createComponent(NameEditorComponent);
    component = fixture.componentInstance;
    component.target = target;
    fixture.detectChanges();
  });

  it('should have correct title', () => {
    expect(component.titleLabel).toEqual('Name');
  });

  it('should update query params when target changes', () => {
    const fakeItems = [
      MockNameD.new(1),
      MockNameD.new(2),
    ];
    const expParams = [
      {names: '1'},
      {names: '2'},
    ]
    component.target = fakeItems[0];
    expect(component._filterLinkParams).toEqual(expParams[0]);
    component.target = fakeItems[1];
    expect(component._filterLinkParams).toEqual(expParams[1]);
  });

  it('should save target', () => {
    spyOn(libraryService, 'updateName');
    component.markModified();
    component.save();
    expect(libraryService.updateName).toHaveBeenCalledOnceWith(target);
  });

  it('should delete a name when confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteName');

    component.delete();

    expect(dialogService.showConfirm).toHaveBeenCalledTimes(1);
    expect(libraryService.deleteName).not.toHaveBeenCalled();

    dialogResponse.next(true);

    expect(libraryService.deleteName).toHaveBeenCalledOnceWith(42);
  });

  it('should not delete a name when not confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteName');

    component.delete();
    dialogResponse.next(false);

    expect(libraryService.deleteName).not.toHaveBeenCalled();
  });


});

describe('TagEditorComponent', () => {
  let component: TagEditorComponent;
  let fixture: ComponentFixture<TagEditorComponent>;
  let libraryService: LibraryService;
  let target: MockTagD;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TagEditorComponent,
      ],
      imports: [
        FormsModule,
        NoopAnimationsModule,
        RouterTestingModule, // actually is needed due for routerLink directives
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(DialogService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();

    libraryService = TestBed.inject(LibraryService);

    target = MockTagD.new(1);

    fixture = TestBed.createComponent(TagEditorComponent);
    component = fixture.componentInstance;
    component.target = target;
    fixture.detectChanges();
  });

  it('should have correct title', () => {
    expect(component.titleLabel).toEqual('Tag');
  });

  it('should update query params when target changes', () => {
    const fakeItems = [
      MockTagD.new(1),
      MockTagD.new(2),
    ];
    const expParams = [
      {tags: '1'},
      {tags: '2'},
    ]
    component.target = fakeItems[0];
    expect(component._filterLinkParams).toEqual(expParams[0]);
    component.target = fakeItems[1];
    expect(component._filterLinkParams).toEqual(expParams[1]);
  });

  it('should save target', () => {
    spyOn(libraryService, 'updateTag');
    component.markModified();
    component.save();
    expect(libraryService.updateTag).toHaveBeenCalledOnceWith(target);
  });

  it('should delete a tag when confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteTag');

    component.delete();

    expect(dialogService.showConfirm).toHaveBeenCalledTimes(1);
    expect(libraryService.deleteTag).not.toHaveBeenCalled();

    dialogResponse.next(true);

    expect(libraryService.deleteTag).toHaveBeenCalledOnceWith(42);
  });

  it('should not delete a tag when not confirmed to do so', () => {
    target.id = 42;
    const dialogService = TestBed.inject(DialogService);
    const dialogResponse: Subject<boolean> = new Subject();
    spyOn(dialogService, 'showConfirm').and.returnValue(dialogResponse);
    spyOn(libraryService, 'deleteTag');

    component.delete();
    dialogResponse.next(false);

    expect(libraryService.deleteTag).not.toHaveBeenCalled();
  });


});
