import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from '@angular/core';

import {ContentType, Id, } from '@app/models';

@Component({
  selector: 'rx-related-content-list',
  templateUrl: './related-content-list.component.html',
  styleUrls: ['./related-content-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RelatedContentListComponent {

  @Input() types: ContentType[];

  @Input() content: ([Id, number][] | null)[];

  @Input() labels: string[];

  constructor() { }

}
