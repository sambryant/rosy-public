import {of} from 'rxjs';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {MockPipe} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ChipModule} from '@app/shared/chip';
import {ContentValuePipe, DecimalPipe, } from '@app/shared/pipes';

import {ContentEditorModule} from '..';
import {RelatedContentListComponent} from './related-content-list.component';


describe('RelatedContentListComponent', () => {
  let component: RelatedContentListComponent;
  let fixture: ComponentFixture<RelatedContentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        MockPipe(ContentValuePipe, (id, type) => of(`${type} ${id}`)),
        MockPipe(DecimalPipe, value => value!.toString()),
        RelatedContentListComponent,
      ],
      imports: [
        ChipModule,
        NoopAnimationsModule,
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedContentListComponent);
    component = fixture.componentInstance;
  });

  it('should display correctly', () => {
    component.labels = ['label one', 'label two'];
    component.types = ['tag', 'name'];
    component.content = [ [[1, 1.4], [2, 1.3]], [[1, 1.5], [7, 1.1]] ];
    const expChips = [
      ['tag 1 (1.4)', 'tag 2 (1.3)'],
      ['name 1 (1.5)', 'name 7 (1.1)']
    ];
    fixture.detectChanges();

    const cards: HTMLElement[] = Array.from(fixture.nativeElement.querySelectorAll('.rx-card'));
    const actLabels = cards.map(card => card.querySelector('.rx-header-2')!['innerText']);
    const actChips = cards.map(card =>
      Array.from(card.querySelectorAll('rx-chip')).map(chip =>
        chip['innerText'].trim()
      )
    );
    expect(actLabels).toEqual(component.labels);
    expect(actChips).toEqual(expChips);
  });
});
