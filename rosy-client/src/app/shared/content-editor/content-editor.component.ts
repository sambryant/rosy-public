import {takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Subject, } from 'rxjs';
import {Component, Input, OnInit, ViewChild,} from '@angular/core';
import {Params} from '@angular/router';

import {
  Action,
  ArtistDetails,
  ContentMeta,
  Group,
  Name,
  NameDetails,
  ImageQuery,
  Tag,
  TagDetails,
} from '@app/models';
import {ActionService, ConfigService, LibraryService, } from '@app/services';
import {DialogService} from '@app/shared/dialog';
import {QuickActionService} from '@app/quick-action';
import {ContentCreateRequest, ContentInputComponent, } from '@app/shared/auto-input';

import {BaseEditorDirective, BaseTaggedEditorDirective, } from './base-editor.directive';


@Component({
  selector: 'rx-artist-editor',
  templateUrl: './content-editor.component.html',
  styleUrls: ['./content-editor.component.scss']
})
export class ArtistEditorComponent
extends BaseEditorDirective<ArtistDetails> {

  readonly titleLabel = 'Artist';

  constructor(
    configService: ConfigService,
    protected dialogService: DialogService,
    protected libraryService: LibraryService,
  ) {
    super(configService);
  }

  protected updateFilterLink() {
    if (this._target) {
      this._filterLinkDisabled = false;
      this._filterLinkParams = ImageQuery.getParamsFor({type: 'artist', item: this._target});
    } else {
      this._filterLinkDisabled = true;
      this._filterLinkParams = {};
    }
  }

  protected doSave(target: ArtistDetails) {
    this.libraryService.updateArtist(target);
  }

  delete() {
    if (this._target) {
      const id = this._target.id;
      this.dialogService
        .showConfirm('Confirm delete', 'Are you sure you want to delete this artist?')
        .pipe(takeUntil(this.destroyed$))
        .subscribe((confirm) => {
          if (confirm) {
            this.libraryService.deleteArtist(id);
          }
        });
    }
  }

}


@Component({
  selector: 'rx-group-editor',
  templateUrl: './group-editor.component.html',
  styleUrls: ['./content-editor.component.scss']
})
export class GroupEditorComponent
extends BaseTaggedEditorDirective<Group>
implements OnInit {

  @ViewChild('tagInput') tagInput: ContentInputComponent;

  @ViewChild('nameInput') nameInput: ContentInputComponent;

  constructor(
    actionService: ActionService,
    configService: ConfigService,
    quickActionService: QuickActionService,
    protected libraryService: LibraryService,
    protected dialogService: DialogService,
  ) {
    super(configService, actionService, quickActionService);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    // Define hotkeys for this component.
    this.actionService.captureAction(Action.AddTag, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.tagInput.open(); });
    this.actionService.captureAction(Action.AddName, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.nameInput.open(); });
    this.actionService.captureAction(Action.ToggleHidden, this.hasEditableTarget$)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => { this.toggleHidden(); });
  }

  createRelated({type, value}: ContentCreateRequest) {
    if (type === 'name' || type === 'tag') {
      this.libraryService.createContent(type, value, [])
        .pipe(takeUntil(this.destroyed$))
        .subscribe((item: Name | Tag) => {
          this.addRelated({type, item});
        });
    }
  }

  protected updateFilterLink() {
    if (this._target) {
      this._filterLinkDisabled = false;
      this._filterLinkParams = ImageQuery.getParamsFor({type: 'group', item: this._target});
    } else {
      this._filterLinkDisabled = true;
      this._filterLinkParams = {};
    }
  }

  protected doSave(group: Group) {
    this.libraryService.updateGroup(group);
  }

  toggleHidden(): void {
    if (this._target) {
      this._target.hidden = !this._target.hidden;
      this.markModified();
    }
  }

  delete() {
    if (this._target) {
      const id = this._target.id;
      this.dialogService
        .showConfirm('Confirm delete', 'Are you sure you want to delete this group?')
        .pipe(takeUntil(this.destroyed$))
        .subscribe((confirm) => {
          if (confirm) {
            this.libraryService.deleteGroup(id);
          }
        });
    }
  }
}


@Component({
  selector: 'rx-name-editor',
  templateUrl: './content-editor.component.html',
  styleUrls: ['./content-editor.component.scss']
})
export class NameEditorComponent
extends BaseEditorDirective<NameDetails> {

  readonly titleLabel = 'Name';

  constructor(
    configService: ConfigService,
    protected dialogService: DialogService,
    protected libraryService: LibraryService,
  ) {
    super(configService);
  }

  protected updateFilterLink() {
    if (this._target) {
      this._filterLinkDisabled = false;
      this._filterLinkParams = ImageQuery.getParamsFor({type: 'name', item: this._target});
    } else {
      this._filterLinkDisabled = true;
      this._filterLinkParams = {};
    }
  }

  protected doSave(target: NameDetails) {
    this.libraryService.updateName(target);
  }

  delete() {
    if (this._target) {
      const id = this._target.id;
      this.dialogService
        .showConfirm('Confirm delete', 'Are you sure you want to delete this name?')
        .pipe(takeUntil(this.destroyed$))
        .subscribe((confirm) => {
          if (confirm) {
            this.libraryService.deleteName(id);
          }
        });
    }
  }

}


@Component({
  selector: 'rx-tag-editor',
  templateUrl: './content-editor.component.html',
  styleUrls: ['./content-editor.component.scss']
})
export class TagEditorComponent
extends BaseEditorDirective<TagDetails> {

  public readonly titleLabel = 'Tag';

  constructor(
    configService: ConfigService,
    protected dialogService: DialogService,
    protected libraryService: LibraryService,
  ) {
    super(configService);
  }

  protected updateFilterLink() {
    if (this._target) {
      this._filterLinkDisabled = false;
      this._filterLinkParams = ImageQuery.getParamsFor({type: 'tag', item: this._target});
    } else {
      this._filterLinkDisabled = true;
      this._filterLinkParams = {};
    }
  }

  protected doSave(target: TagDetails) {
    this.libraryService.updateTag(target);
  }

  delete() {
    if (this._target) {
      const id = this._target.id;
      this.dialogService
        .showConfirm('Confirm delete', 'Are you sure you want to delete this tag?')
        .pipe(takeUntil(this.destroyed$))
        .subscribe((confirm) => {
          if (confirm) {
            this.libraryService.deleteTag(id);
          }
        });
    }
  }

}
