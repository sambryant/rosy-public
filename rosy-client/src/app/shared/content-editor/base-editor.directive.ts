import {takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Subject, } from 'rxjs';
import {Directive, Input, OnInit, } from '@angular/core';
import {Params} from '@angular/router';

import {
  Action,
  Content,
  ContentMeta,
  Name,
  OperationMode,
  QUICK_ACTION_ACTIONS,
  Tag,
  TaggedContent,
} from '@app/models';
import {APP_PAGES} from '@app/app-page.model';
import {ActionService, ConfigService, } from '@app/services';
import {ContentCreateRequest} from '@app/shared/auto-input';
import {QuickActionService} from '@app/quick-action';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Directive()
export abstract class BaseEditorDirective<T extends Content>
extends UnsubscribeOnDestroyedDirective {

  // Base link to search results page filtered by current target.
  readonly _FILTER_LINK_URL = APP_PAGES.images.getBaseUrl();

  // Set of URL query params that will trigger an image search filtered by target of current view.
  _filterLinkParams: Params = {};

  _filterLinkDisabled: boolean = true;

  /**
   * When true, all modification elements are disabled.
   */
  @Input() get readonly() { return this._readonly; }
  set readonly(readonly: boolean) {
    this._readonly = readonly;
    this.hasEditableTarget$.next(!this._readonly && !!this._target);
  }

  @Input() get target() { return this._target; }
  set target(target: T | null) {
    this._target = target;
    this.targetModified = false;
    this.targetChanged.next(null);
    this.updateFilterLink();
    this.hasTarget.next(!!this._target);
    this.hasEditableTarget$.next(!this._readonly && !!this._target);
  }

  protected _readonly: boolean = false;

  protected _target: T | null = null;

  protected targetModified = false;

  protected targetChanged = new Subject<null>();

  protected hasTarget: BehaviorSubject<boolean>;

  protected hasEditableTarget$: BehaviorSubject<boolean>;

  constructor(public configService: ConfigService) {
    super();
    this.hasTarget = new BehaviorSubject(!!this._target);
    this.hasEditableTarget$ = new BehaviorSubject(!!this._target && !this._readonly);
  }

  isModified(): boolean {
    return this.targetModified;
  }

  markModified(): void {
    this.targetModified = true;
    if (this.configService.saveAfterChange) {
      this.save();
    }
  }

  save(): void {
    if (this._target && this.isModified()) {
      this.doSave(this._target);
      this.targetModified = false;
    }
  }

  protected abstract updateFilterLink();

  protected abstract doSave(target: T);

}



@Directive()
export abstract class BaseTaggedEditorDirective<T extends TaggedContent>
extends BaseEditorDirective<T>
implements OnInit {

  constructor(
    configService: ConfigService,
    protected actionService: ActionService,
    protected quickActionService: QuickActionService,
  ) {
    super(configService);
  }

  ngOnInit(): void {
    QUICK_ACTION_ACTIONS.forEach((action) => {
      this.actionService.captureAction(action, this.hasEditableTarget$)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.doQuickAction(action);
        });
    });
  }

  addRelated(content: ContentMeta) {
    if (content.type === 'name' || content.type === 'tag') {
      this.modifyTaglike('add', content.type, content.item);
    }
  }

  abstract createRelated(request: ContentCreateRequest);

  protected doQuickAction(quickAction: Action) {
    let {mode, target} = this.quickActionService.getQuickAction(quickAction);
    if (target && target.item) {
      if (target.type === 'tag' || target.type === 'name') {
        this.modifyTaglike(mode, target.type, target.item);
      }
    }
  }

  protected modifyTaglike(mode: OperationMode, type: 'name' | 'tag', item: Name | Tag) {
    if (this._target) {
      const list = type === 'name' && this._target.names || this._target.tags;
      const index = list.findIndex(it => it.id === item.id);
      if (index === -1) {
        if (mode !== 'remove') {
          list.push(item);
          this.markModified();
        }
      } else {
        if (mode !== 'add') {
          list.splice(index, 1);
          this.markModified();
        }
      }
    }
  }

}
