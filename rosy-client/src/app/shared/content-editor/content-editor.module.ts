import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule, } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ActionButtonModule} from '@app/shared/action-button';
import {AutoInputModule} from '@app/shared/auto-input';
import {ChipModule} from '@app/shared/chip';
import {ContextMenuModule} from '@app/shared/context-menu';
import {EditableModule} from '@app/shared/editable';
import {GraphModule} from '@app/shared/graph';
import {ImageModule} from '@app/shared/image';
import {PipesModule} from '@app/shared/pipes';
import {ServicesModule} from '@app/services';
import {UtilityModule} from '@app/shared/utility';
import {ViewModeSelectorModule} from '@app/shared/view-mode-selector';

import {
  ArtistEditorComponent,
  GroupEditorComponent,
  NameEditorComponent,
  TagEditorComponent,
} from './content-editor.component';
import {ImageViewerComponent} from './image-viewer.component';
import {RelatedContentListComponent} from './related-content-list';


@NgModule({
  declarations: [
    ArtistEditorComponent,
    GroupEditorComponent,
    ImageViewerComponent,
    NameEditorComponent,
    RelatedContentListComponent,
    TagEditorComponent,
  ],
  exports: [
    ArtistEditorComponent,
    GroupEditorComponent,
    ImageViewerComponent,
    NameEditorComponent,
    TagEditorComponent,
  ],
  imports: [
    ActionButtonModule,
    AutoInputModule,
    ChipModule,
    CommonModule,
    ContextMenuModule,
    EditableModule,
    FormsModule,
    GraphModule,
    ImageModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    PipesModule,
    ReactiveFormsModule,
    RouterModule,
    ServicesModule,
    UtilityModule,
    ViewModeSelectorModule,
  ]
})
export class ContentEditorModule { }
