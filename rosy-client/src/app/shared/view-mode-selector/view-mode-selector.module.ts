import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule, } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {NgModule} from '@angular/core';

import {ServicesModule} from '@app/services';

import {ViewModeSelectorComponent} from './view-mode-selector.component';


@NgModule({
  declarations: [
    ViewModeSelectorComponent,
  ],
  exports: [
    ViewModeSelectorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    ServicesModule,
  ]
})
export class ViewModeSelectorModule { }
