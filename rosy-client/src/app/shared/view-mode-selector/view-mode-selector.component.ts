import {Component, Input, } from '@angular/core';

import {ConfigService} from '@app/services';

@Component({
  selector: 'rx-view-mode-selector',
  template: `
    <mat-button-toggle-group [(ngModel)]="configService.viewMode">
      <mat-button-toggle value="selector">
        <mat-icon>search</mat-icon>
      </mat-button-toggle>
    <mat-button-toggle value="split" *ngIf="showSplit">
      <mat-icon>vertical_split</mat-icon>
    </mat-button-toggle>
    <mat-button-toggle value="image">
        <mat-icon>image</mat-icon>
      </mat-button-toggle>
    </mat-button-toggle-group>
  `,
})
export class ViewModeSelectorComponent {

  @Input() showSplit: boolean = false;

  constructor(public configService: ConfigService) { }

}
