import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject, } from 'rxjs';

import {AutoInputDirective} from './auto-input.directive';
import {ConfigService, LibraryService, } from '@app/services';
import {ContentMeta, ContentType, ContentValue, } from '@app/models';


export interface ContentCreateRequest {
  type: ContentType;
  value: string;
}


@Component({
  selector: 'rx-content-input',
  templateUrl: './content-input.component.html',
  styleUrls: ['./auto-input.component.scss']
})
export class ContentInputComponent extends AutoInputDirective<ContentValue> {

  /**
   * Indicates that this input should display an option to create new objects.
   */
  @Input() allowCreate = false;

  @Input() optionsList?: Observable<ContentValue[]> | null = null; // dont use subject

  /**
   * An observable that may be passed whose values are populated at the top of the autocomplete.
   */
  @Input() set suggestion(suggestion: Observable<ContentValue[] | null> | null) {
    this.suggestionChanged$.next(null);
    if (suggestion) {
      suggestion
        .pipe(takeUntil(this.destroyed$))
        .pipe(takeUntil(this.suggestionChanged$))
        .subscribe((suggestion: ContentValue[] | null) => {
          this._suggested = suggestion;
          this.recomputeAllOptions();
        });
    } else if (this._suggested !== null) {
      this._suggested = null;
      this.recomputeAllOptions();
    }
  }

  @Input() set type(type: ContentType | null) {
    if (type === this._type) {
      return;
    }
    this._type = type;
    this.typeChanged.next(null);
    if (!this._type) {
      return;
    }
    // Change the source of the autocomplete values.
    let source = this.optionsList || this.libraryService.getContentList(this._type);

    // Every time library service emits new set of tags/names, updates options list.
    this.isLoading = true;
    source
      .pipe(takeUntil(this.destroyed$))
      .pipe(takeUntil(this.typeChanged)) // cancel when type changes
      .subscribe((options: ContentValue[]) => {
        this.isLoading = false;
        this._options = options;
        this.recomputeAllOptions();
      });
  }
  get type() { return this._type; }

  @Output() create: EventEmitter<ContentCreateRequest> = new EventEmitter();

  @Output() selected: EventEmitter<ContentMeta> = new EventEmitter();

  protected _type: ContentType | null = null;

  protected typeChanged = new Subject<null>();

  protected _options: ContentValue[] = [];

  protected _suggested: ContentValue[] | null = null;

  protected suggestionChanged$ = new Subject<null>();

  constructor(
    configService: ConfigService,
    element: ElementRef,
    protected libraryService: LibraryService) {
    super(configService, element);
  }

  protected recomputeAllOptions() {
    if (this._suggested) {
      this.allOptions.next(this._suggested.concat(this._options));
    } else {
      this.allOptions.next(this._options);
    }
  }

  protected doSelection(item: ContentValue | { value: string }) {
    if (this._type) {
      if (item && item.hasOwnProperty('id')) {
        this.selected.emit({ type: this._type, item } as ContentMeta);
      } else if (item.value && this.allowCreate) {
        this.create.emit({ type: this._type, value: item.value });
      }
    }
  }

  protected matchKey({value}: ContentValue): string {  return value; }

}
