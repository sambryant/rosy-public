import {
  combineLatest,
  fromEvent,
  timer,
  BehaviorSubject,
  Observable,
  Subscription,
} from 'rxjs';
import {
  filter,
  map,
  startWith,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';
import {
  AfterViewInit,
  Component,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  Injectable,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocomplete, MatAutocompleteTrigger, } from '@angular/material/autocomplete';

import {ConfigService} from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Directive()
export abstract class AutoInputDirective<T>
extends UnsubscribeOnDestroyedDirective
implements AfterViewInit, OnInit {

  @Input() placeholder: string;

  @Input() autocompletePosition: 'auto' | 'above' | 'below' = 'auto';

  @Input() multiple = true;

  inputFormCtrl = new FormControl();

  isLoading = true;

  options: T[] = [];

  @HostBinding('attr.tabindex') '-1';

  @ViewChild('input', {static: true}) input: ElementRef<HTMLInputElement>;

  @ViewChild(MatAutocompleteTrigger, {static: true}) autocompleteTrigger: MatAutocompleteTrigger;

  @ViewChild(MatAutocomplete, {static: true}) autocomplete: MatAutocomplete;

  protected allOptions: BehaviorSubject<T[]> = new BehaviorSubject([] as T[]);

  /**
   * These two variables track the state of the `input` and `mat-autocomplete` elements. When the
   * input loses focus *and* the `mat-autocomplete` panel is closed, then this component fires a
   * blur event which bubbles up (and is interpretted by `rx-editable`).
   */
  protected inputFocused = false;
  protected autocompleteOpen = false;

  constructor(
    protected configService: ConfigService,
    protected element: ElementRef,
  ) {
    super();
  }

  ngOnInit(): void {
    this.setupFocusWatchers();

    // Any time either `options` or `inputFormCtrl` changes, recompute the list of matching options.
    // This wont fire until they have both fired at least once.
    const inputValue = this.inputFormCtrl.valueChanges
      .pipe(startWith(''))
      .pipe(filter((value) => typeof value === 'string')); // important! to ignore object selection.
    combineLatest([this.allOptions, inputValue])
      .pipe(takeUntil(this.destroyed$))
      .pipe(map(([options, value]: [ T[], string ]) => this.filterOptions(options, value) ))
      .subscribe(filteredOptions => {
         this.options = filteredOptions;
       });
  }

  ngAfterViewInit(): void {
    this.autocompleteTrigger.position = this.autocompletePosition;
  }

  /**
   * Subcribes to input focus state and panel open state.
   *
   * If panel is closed and input does not have focus, it triggers a blur event on this component
   * which bubbles up and can be observed by other components (notably `EditableComponent`). This
   * could be done with template binding instead (e.g. <input (blur)="onInputBlur"), but this way,
   * we don't have to repeat this code in multiple templates.
   */
  protected setupFocusWatchers() {
    fromEvent(this.input.nativeElement, 'focus').pipe(takeUntil(this.destroyed$)).subscribe(() => {
      this.inputFocused = true;
    });
    fromEvent(this.input.nativeElement, 'blur').pipe(takeUntil(this.destroyed$)).subscribe(() => {
      this.inputFocused = false;
      this.blurIfClosed();
    });
    this.autocomplete.opened.pipe(takeUntil(this.destroyed$)).subscribe(() => {
      this.autocompleteOpen = true;
    });
    this.autocomplete.closed.pipe(takeUntil(this.destroyed$)).subscribe(() => {
      this.autocompleteOpen = false;
      this.blurIfClosed();
    });
  }

  /**
   * Warning: The logic here seems messy, but it is correct and must be changed very carefully.
   *
   * The goal is to emit a blur when both `input` loses focus and `mat-autocomplete` is not visible.
   * This must happen regardless of which event happens first, so after either event, we have to
   * check the state of the other to determine if a blur should be emitted.
   *
   * Naively you could just check `(!this.inputFocused && !this.autocomplete.isOpen)`, however this
   * will not work. the `autocomplete.closed` event is fired *before* the vlaue of
   * `autocomplete.isOpen` is changed for some reason.
   *
   * Likewise you cannot check `(this.inputFocused && !this.autocompleteOpen)`, because in some
   * cases, the autocomplete panel closes without firing `closed` (e.g. try selecting an option with
   * ",")
   *
   * We have to check `inputFocused` and *both* `autocomplete.isOpen` and `this.autocompleteOpen`.
   */
  protected blurIfClosed() {
    if (!this.inputFocused && (!this.autocompleteOpen || !this.autocomplete.isOpen)) {
      this.element.nativeElement.dispatchEvent(new Event('blur', {bubbles: true}));
    }
  }

  onSelect(value: T): void {
    if (value === undefined || value === null) {
      return;
    }
    this.input.nativeElement.value = '';
    this.inputFormCtrl.setValue('');
    this.doSelection(value);
    if (this.multiple) {
      setTimeout(() => this.autocompleteTrigger.openPanel(), 0);
    } else {
      // When not in multiple mode: we now want to close autocomplete panel and blur input element.
      // This has been made difficult due to a recent change in mat-autocomplete implementation.
      // Now, whenever an autocomplete value is selected, the autocomplete sends focus back to the
      // input element. Since this happens asynchronously, we have to reverse it by listening for
      // the subsequent closed event to fire (or just undoing it on a timeout).

      // Triggers input focus: https://github.com/angular/components/blob/816099e76cf352a04cfaeab83ddbde1e4ad2e4f1/src/material/autocomplete/autocomplete-trigger.ts#L566
      this.autocompleteTrigger.closePanel();

      // Asynchronously undo the input focus set by autocomplete after selection.
      setTimeout(() => this.input.nativeElement.blur());
    }
  }

  onInputKeydown(event: KeyboardEvent): void {
    if (event.key === 'Escape') {
      this.autocompleteTrigger.closePanel();
      this.input.nativeElement.blur();
      event.preventDefault();
    } else if (event.key === 'Tab') {
      event.preventDefault();
      const value = this.input.nativeElement.value;
      const largestPrefix = this.getLargestCommonPrefix(this.filterOptions(this.allOptions.value, value));
      if (largestPrefix.length > value.length) {
        this.input.nativeElement.value = largestPrefix;
        this.inputFormCtrl.setValue(largestPrefix);
      }
      this.autocompleteTrigger.openPanel();
    }
  }

  public open(): void {
    this.input.nativeElement.value = '';
    this.inputFormCtrl.setValue('');
    // Timeout with 0 to allow detection cycle to run before trying to focus. This is because the
    // element might not be visible (and thus not focusable) until detection runs
    setTimeout(() => {
      this.input.nativeElement.focus();
    }, 0);
  }

  /**
   * Returns longest element which is a prefix of all given autocompletion results.
   *
   * Comparable to the result of entering `tab` in unix terminals.
   */
  private getLargestCommonPrefix(values: T[]): string {
    if (values.length < 2) {
      return values.length === 0 ? '' : this.matchKey(values[0]);
    }
    let i = 1;
    let prefix = this.matchKey(values[0]);
    while (i < values.length) {
      let j = 0;
      const value = this.matchKey(values[i]);
      while (j < prefix.length && j < value.length) {
        if (prefix.charAt(j) === value.charAt(j)) {
          j++;
        } else {
          break;
        }
      }
      if (j < prefix.length) {
        prefix = prefix.substr(0, j);
      }
      i++;
    }
    return prefix;
  }

  protected abstract matchKey(value: T): string;

  protected abstract doSelection(selection: T);

  protected filterOptions(options: T[], value: string): T[] {
    value = value.trim().toLowerCase();
    if (this.configService.searchPrefixOnly) {
      return options.filter(option => {
        return this.matchKey(option).toLowerCase().indexOf(value) === 0;
      });
    } else {
      return options.filter(option => {
        return this.matchKey(option).toLowerCase().indexOf(value) !== -1;
      });
    }
  }
}
