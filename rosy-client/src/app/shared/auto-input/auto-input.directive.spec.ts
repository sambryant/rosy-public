import {fakeAsync, flush, tick, ComponentFixture, TestBed, } from '@angular/core/testing';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject, } from 'rxjs';
import {ReactiveFormsModule} from '@angular/forms';
import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, } from '@angular/core';
import {MatAutocompleteModule, MatAutocompleteTrigger, } from '@angular/material/autocomplete';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ConfigService} from '@app/services';

import {AutoInputDirective} from '.';


interface TestItem {
  value: string;
}

@Component({
  selector: 'test-input',
  template: `
<mat-autocomplete
  #inputAutocomplete="matAutocomplete"
  (optionSelected)="onSelect($event.option.value)">
  <mat-option *ngFor="let item of options" [value]="item" (click)="$event.stopPropagation()">
    {{item.value}}
  </mat-option>
</mat-autocomplete>

<input
  #input
  [formControl]="inputFormCtrl"
  [matAutocomplete]="inputAutocomplete"
  (keydown)="onInputKeydown($event)">
`,
})
class TestInputComponent extends AutoInputDirective<TestItem> implements OnInit {

  @Input() autoCompleteItems: Observable<TestItem[]>;

  @Output() selected: EventEmitter<TestItem> = new EventEmitter();

  constructor(
    configService: ConfigService,
    element: ElementRef,
  ) {
    super(configService, element);
  }

  override ngOnInit() {
    super.ngOnInit();
    this.isLoading = true;
    this.autoCompleteItems
      .pipe(takeUntil(this.destroyed$))
      .subscribe((options: TestItem[]) => {
        this.isLoading = false;
        this.allOptions.next(options);
      });
  }

  matchKey(item: TestItem): string { return item.value; }

  protected doSelection(selection: TestItem) { this.selected.emit(selection); }

}


@Component({
  template: `
<test-input
  (blur)="testInputBlurred()"
  (selected)="testInputSelected($event)"
  [autoCompleteItems]="autoCompleteItems">
</test-input>
<input #otherInput>
<input #thirdInput>
`,
})
class TestHostComponent {

  @ViewChild('otherInput') otherInput: ElementRef<HTMLInputElement>;

  @ViewChild('thirdInput') thirdInput: ElementRef<HTMLInputElement>;

  @ViewChild(TestInputComponent) component: TestInputComponent;

  autoCompleteItems: Observable<TestItem[]>;

  constructor(){}

  testInputBlurred() {}

  testInputSelected(item: TestItem) {}

}

describe('AutoInputDirective', () => {
  let host: TestHostComponent;
  let component: TestInputComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  let input: HTMLInputElement;
  let selectSpy: jasmine.Spy;
  let blurSpy: jasmine.Spy;
  let autocompleteTrigger: MatAutocompleteTrigger;

  let mockItemsResp: Subject<TestItem[]>;

  beforeEach(async () => {
    mockItemsResp = new Subject();

    await TestBed.configureTestingModule({
      declarations: [
        TestHostComponent,
        TestInputComponent,
      ],
      imports: [
        MatAutocompleteModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(TestHostComponent);
    host = fixture.componentInstance;
    host.autoCompleteItems = mockItemsResp;
    blurSpy = spyOn(host, 'testInputBlurred');
    selectSpy = spyOn(host, 'testInputSelected');
    fixture.detectChanges();

    component = host.component;
    input = component.input.nativeElement;
    autocompleteTrigger = component.autocompleteTrigger;
  });

  // Opens input/autocomplete and checks they are both focused/opened.
  function open() {
    component.open();
    fixture.detectChanges();
    flush();
    expect(document.activeElement).toBe(input);
    expect(autocompleteTrigger.panelOpen).toBe(true, 'This will fail unless browser has focus');
  };

  // Opens input/autocomplete and checks they are both focused/opened.
  function close() {
    input.blur();
    autocompleteTrigger.closePanel();
    fixture.detectChanges();
    flush();
    expect(document.activeElement).not.toBe(input);
    expect(autocompleteTrigger.panelOpen).toBe(false);
  };

  it('should mark as loading before autocomplete finishes', () => {
    expect(component.isLoading).toBeTrue();
    mockItemsResp.next([{value: 'thing 1'}, {value: 'thing 2'}]);
    expect(component.isLoading).toBeFalse();
    expect(component.options).toEqual([{value: 'thing 1'}, {value: 'thing 2'}]);
  });

  describe('filtering', () => {
    let configService: ConfigService;

    beforeEach(() => {
      configService = TestBed.inject(ConfigService);
    })

    it('should filter the displayed options based on input by prefix ', () => {
      configService.searchPrefixOnly = true;
      const mockItems = [
        'hog wash car',
        'hog cow lawn',
        'cow wash hog',
      ].map(value => ({value}));
      const testCases: [string, TestItem[]][] = [
        [''        , [0, 1, 2].map(i => mockItems[i])],
        ['hog '    , [0, 1   ].map(i => mockItems[i])],
        ['hog wash', [0      ].map(i => mockItems[i])],
      ];
      mockItemsResp.next(mockItems);

      testCases.forEach(([value, expItems]) => {
        input.value = value;
        input.dispatchEvent(new Event('input'));
        expect(component.options).toEqual(expItems);
      });
    });

    it('should filter the displayed options based on input by any contained string', () => {
      configService.searchPrefixOnly = false;
      const mockItems = [
        'hog wash car',
        'hog car lawn',
        'cow wash hog',
      ].map(value => ({value}));
      const testCases: [string, TestItem[]][] = [
        ['hog' , [0, 1, 2].map(i => mockItems[i])],
        ['wash', [0, 2   ].map(i => mockItems[i])],
        ['car' , [0, 1   ].map(i => mockItems[i])],
        ['lawn', [1      ].map(i => mockItems[i])],
      ];
      mockItemsResp.next(mockItems);

      testCases.forEach(([value, expItems]) => {
        input.value = value;
        input.dispatchEvent(new Event('input'));
        expect(component.options).toEqual(expItems);
      });
    });

  });

  it('should only emit blur even when autocomplete is closed and input is blurred', fakeAsync(() => {
    mockItemsResp.next([{value: 'thing 1'}]);

    // In these tests, we want to test when `component.blur` is fired. We expect this to happen
    // when both the autocomplete is closed and the input is blurred, regardless of the order
    // these events occur in.
    const reset = () => {
      input.blur();
      autocompleteTrigger.closePanel();
      blurSpy.calls.reset();
      open();
    };

    // Blur input, dont close autocomplete = expect no blur event
    reset();
    host.otherInput.nativeElement.focus();
    expect(document.activeElement).not.toBe(input,   'Input is not focused');
    expect(autocompleteTrigger.panelOpen).toBe(true, 'Autocomplete is opened');
    expect(blurSpy).not.toHaveBeenCalled();

    // Close autocomplete, dont blur input = expect no blur event
    reset();
    document.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    expect(document.activeElement).toBe(input,        'Input is focused');
    expect(autocompleteTrigger.panelOpen).toBe(false, 'Autocomplete is closed');
    expect(blurSpy).not.toHaveBeenCalled();

    // Blur input, then close autocomplete = expect blur event
    reset();
    host.otherInput.nativeElement.focus();
    document.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    expect(document.activeElement).not.toBe(input,    'Input is not focused');
    expect(autocompleteTrigger.panelOpen).toBe(false, 'Autocomplete is closed');
    expect(blurSpy).toHaveBeenCalledTimes(1);

    // Close autocomplete, then blur input = expect blur event
    reset();
    document.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    host.otherInput.nativeElement.focus();
    expect(document.activeElement).not.toBe(input,    'Input is not focused');
    expect(autocompleteTrigger.panelOpen).toBe(false, 'Autocomplete is closed');
    expect(blurSpy).toHaveBeenCalledTimes(1);
  }));

  describe('open', () => {

    beforeEach(() => {
      mockItemsResp.next([{value: 'thing 1'}]);
    });

    it('should focus the input after being opened', fakeAsync(() => {
      expect(document.activeElement).not.toBe(input, 'Input is not focused');
      component.open();
      fixture.detectChanges();
      flush();
      expect(document.activeElement).toBe(input, 'Input is focused');
    }));

    it('should open the autocomplete after being opened', fakeAsync(() => {
      expect(autocompleteTrigger.panelOpen).toBeFalse();
      component.open();
      fixture.detectChanges();
      flush();
      expect(autocompleteTrigger.panelOpen).toBe(true, 'This will fail unless browser has focus');
    }));

  });

  describe('onSelect', () => {
    let mockItems: TestItem[];

    beforeEach(() => {
      mockItems = [
        {value: 'item 1'},
        {value: 'item 2'},
        {value: 'third item'}
      ];
      mockItemsResp.next(mockItems);
    });

    it('should select an item', fakeAsync(() => {
      open();

      expect(selectSpy).not.toHaveBeenCalled();
      component.onSelect(mockItems[0]);
      flush();
      expect(selectSpy).toHaveBeenCalledOnceWith(mockItems[0]);
    }));

    it('should ignore an empty selection', fakeAsync(() => {
      component.multiple = false;
      fixture.detectChanges();

      open();

      expect(selectSpy).not.toHaveBeenCalled();

      component.onSelect(null as any);
      fixture.detectChanges();
      flush();

      expect(selectSpy).not.toHaveBeenCalled();
      // Input still focused, autocomplete still open
      expect(document.activeElement).toBe(input, 'Input is focused');
      expect(autocompleteTrigger.panelOpen).toBe(true, 'Autocomplete is open');
    }));

    it('should reset value of input', fakeAsync(() => {
      open();

      input.value = 'item ';
      input.dispatchEvent(new Event('input'));
      expect(component.options).toEqual([{value: 'item 1'}, {value: 'item 2'}]);

      component.onSelect(mockItems[0]);
      flush();

      expect(input.value).toEqual('');
      expect(component.options).toEqual([{value: 'item 1'}, {value: 'item 2'}, {value: 'third item'}]);
    }));

    it('should close everything and emit blur when multiple is false', fakeAsync(() => {
      component.multiple = false;
      fixture.detectChanges();

      open();
      const options = document.querySelectorAll('mat-option');

      // The input should be the focused element
      expect(document.activeElement).toBe(input, 'Input is focused');

      options[0].dispatchEvent(new MouseEvent('click', { bubbles: true }));
      flush();

      // Since multiple is false, the input should reliquish focus.
      expect(document.activeElement).not.toBe(input, 'Input is not focused');

      // Autocomplete panel should be closed.
      expect(autocompleteTrigger.panelOpen).toBe(false, 'Autocomplete is closed');

      expect(selectSpy).toHaveBeenCalledOnceWith(mockItems[0]);
      expect(blurSpy).toHaveBeenCalledTimes(1);
    }));

    it('should should keep everything open and not emit blur when multiple is true', fakeAsync(() => {
      component.multiple = true;
      fixture.detectChanges();

      open();

      const options = document.querySelectorAll('mat-option');
      options[0].dispatchEvent(new MouseEvent('click', { bubbles: true }));
      flush();

      expect(selectSpy).toHaveBeenCalledOnceWith(mockItems[0]);
      expect(document.activeElement).toBe(input, 'Input is focused');
      expect(autocompleteTrigger.panelOpen).toBe(true, 'Autocomplete is open');
      expect(blurSpy).not.toHaveBeenCalled();
    }));

  });

  describe('onInputKeydown', () => {

    beforeEach(() => {
      mockItemsResp.next([{value: 'thing 1'}, {value: 'thing 2'}]);
    });

    it('should not prevent default unless tab or escape', fakeAsync(() => {
      const mockEvents = [
        new KeyboardEvent('keydown', { key: 'Escape' }),
        new KeyboardEvent('keydown', { key: 'Tab' }),
        new KeyboardEvent('keydown', { key: 'a' }),
      ];
      const expPrevented = [ 1, 1, 0, ];

      open();

      mockEvents.forEach((ev, i) => {
        spyOn(ev, 'preventDefault');
        input.dispatchEvent(ev);
        expect(ev.preventDefault).toHaveBeenCalledTimes(expPrevented[i]);
      });
    }));

    it('should close everything on escape', fakeAsync(() => {
      const mockEvent = new KeyboardEvent('keydown', { key: 'Escape' });

      open();

      expect(document.activeElement).toBe(input, 'Input is focused');
      expect(autocompleteTrigger.panelOpen).toBe(true, 'Autocomplete is open');
      expect(blurSpy).not.toHaveBeenCalled();

      input.dispatchEvent(mockEvent);

      expect(document.activeElement).not.toBe(input, 'Input is blurred');
      expect(autocompleteTrigger.panelOpen).toBe(false, 'Autocomplete is closed');
      expect(blurSpy).toHaveBeenCalled();
      expect(selectSpy).not.toHaveBeenCalled();
      flush();
    }));

    it('should interpret tab to match largest common prefix', fakeAsync(() => {
      const mockItems: TestItem[] = [
        'this is an item',
        'different prefix',
        'this is another',
        'this is another with a longer title',
      ].map(value => ({value}));
      const mockEvent = new KeyboardEvent('keydown', { key: 'Tab' });
      const tests = [{
        oldInput: 'th',
        expInput: 'this is an',
        expOptions: [mockItems[0], mockItems[2], mockItems[3]],
        description: 'should fill up to the largest prefix when input matches multiple results',
      }, {
        oldInput: 'this is an i',
        expInput: 'this is an item',
        expOptions: [mockItems[0]],
        description: 'should fill the entire value when input matches only one result',
      }, {
        oldInput: 'zz',
        expInput: 'zz',
        expOptions: [],
        description: 'should do nothing when current value doesnt match any result',
      }, {
        oldInput: 'this is another',
        expInput: 'this is another',
        expOptions: [mockItems[2], mockItems[3]],
        description: 'should do nothing when current value already matches entirety of single result',
      }];

      mockItemsResp.next(mockItems);
      fixture.detectChanges();
      open();

      tests.forEach(({oldInput, expInput, expOptions, description}) => {
        input.value = oldInput;
        input.dispatchEvent(new Event('input'));
        input.dispatchEvent(mockEvent);
        expect(input.value).toEqual(expInput, description);
        expect(component.options).toEqual(expOptions, description);
      });
    }));

  });


});








