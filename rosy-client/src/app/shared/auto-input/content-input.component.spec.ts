import {
  fakeAsync,
  flush,
  tick,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';
import {BehaviorSubject, ReplaySubject, Subject, } from 'rxjs';
import {By} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatAutocomplete, MatAutocompleteModule, MatAutocompleteTrigger, } from '@angular/material/autocomplete';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {
  Artist,
  ContentMeta,
  ContentType,
  ContentTypeMap,
  ContentValue,
  GroupSimple,
  Name,
  Tag,
} from '@app/models';
import {ApiService, LibraryService, NotificationService, } from '@app/services';
import {MockArtist, MockGroup, MockName, MockTag, } from '@app/models/testing';

import {ContentInputComponent} from '.';


describe('ContentInputComponent', () => {
  let component: ContentInputComponent;
  let fixture: ComponentFixture<ContentInputComponent>;
  let libraryService: LibraryService;

  let fakeContentLists: ContentTypeMap<ContentValue[]>;
  let fakeContentResponse: ContentTypeMap<ReplaySubject<ContentValue[]>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ContentInputComponent,
      ],
      imports: [
        MatAutocompleteModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(NotificationService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();

    fakeContentLists = {
      artist: [1, 2, 3].map(i => MockArtist.new(i)),
      group: [1, 2, 3].map(i => MockGroup.new(i)),
      name: [1, 2, 3].map(i => MockName.new(i)),
      tag: [1, 2, 3].map(i => MockTag.new(i)),
    };
    fakeContentResponse = {
      artist: new ReplaySubject(1),
      group: new ReplaySubject(1),
      name: new ReplaySubject(1),
      tag: new ReplaySubject(1)
    };

    libraryService = TestBed.inject(LibraryService);
    spyOn(libraryService, 'getContentList').and.callFake((type: ContentType) => {
      return fakeContentResponse[type];
    });

    fixture = TestBed.createComponent(ContentInputComponent);
    component = fixture.componentInstance;
    component.multiple = true;
    component.allowCreate = false;
    fixture.detectChanges();
  });

  it('should set autocomplete options when server returns content list', () => {
    component.type = 'group';
    fixture.detectChanges();

    expect(component.options).toEqual([]);
    expect(component.isLoading).toBeTrue();

    fakeContentResponse.artist.next(fakeContentLists.artist);

    expect(component.options).toEqual([]);
    expect(component.isLoading).toBeTrue();

    fakeContentResponse.group.next(fakeContentLists.group);

    expect(component.isLoading).toBeFalse();
    expect(component.options).toEqual(fakeContentLists.group);
  });

  it('should filter group list when input changes', () => {
    component.type = 'group';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('input'));
    const fakeGroups: GroupSimple[] = [
      {id: 1, value: 'hog maw hog', description: 'hi', hidden: false},
      {id: 2, value: 'hog', description: 'bye', hidden: false},
      {id: 3, value: 'hog maw', description: 'okay', hidden: false},
    ];
    const fakeValues = ['hog', 'hog maw'];
    const expectedGroups = [fakeGroups, [fakeGroups[0], fakeGroups[2]]];

    fakeContentResponse.group.next(fakeGroups);
    expect(component.options).toEqual(fakeGroups);

    fakeValues.forEach((value, i) => {
      input.nativeElement.value = value;
      input.nativeElement.dispatchEvent(new Event('input'));
      expect(component.options).toEqual(expectedGroups[i]);
    });
  });

  describe('setType', () => {

    it('should switch autocomplete options when type changes', () => {
      component.type = 'group';
      fixture.detectChanges();

      fakeContentResponse.group.next(fakeContentLists.group);
      expect(component.options).toEqual(fakeContentLists.group);

      component.type = 'artist';
      fixture.detectChanges();

      expect(component.options).toEqual(fakeContentLists.group);
      fakeContentResponse.artist.next(fakeContentLists.artist);
      expect(component.options).toEqual(fakeContentLists.artist);
    });

    it('should handle when old autocompute request completes after type chnages', () => {
      component.type = 'artist';
      fixture.detectChanges();

      component.type = 'group';
      fixture.detectChanges();

      // Resolve metadata requests out of order
      fakeContentResponse.group.next(fakeContentLists.group);
      fakeContentResponse.artist.next(fakeContentLists.artist);

      expect(component.options).toEqual(fakeContentLists.group);
    });

    it('should handle switching type to null', fakeAsync(() => {
      component.allowCreate = false;
      component.type = 'artist';
      fixture.detectChanges();
      tick();

      expect(() => {
        component.type = null;
        fixture.detectChanges();
        tick();
      }).not.toThrow();
    }));

  });

  describe('open', () => {

    it('should not trigger error when actions are unregistered', fakeAsync(() => {
      component.allowCreate = false;
      component.type = 'artist';
      fixture.detectChanges();

      expect(() => component.open()).not.toThrow();
      tick();

      component.allowCreate = true;
      component.type = 'artist';
      fixture.detectChanges();

      expect(() => component.open()).not.toThrow();
      tick();
    }));

    it('should focus the input when opened', fakeAsync(() => {
      const focusSpy = spyOn(component.input.nativeElement, 'focus');
      fixture.detectChanges();

      expect(focusSpy).not.toHaveBeenCalled();
      component.open();
      expect(focusSpy).not.toHaveBeenCalled(); // dont want this to happen immediately
      tick();
      expect(focusSpy).toHaveBeenCalled();
    }));

  });

  describe('onSelect', () => {
    let createSpy: jasmine.Spy;
    let selectSpy: jasmine.Spy;

    beforeEach(() => {
      createSpy = spyOn(component.create, 'emit').and.callThrough();
      selectSpy = spyOn(component.selected, 'emit').and.callThrough();
    });

    it('should ignore undefined selections', fakeAsync(() => {
      component.type = 'group';
      fixture.detectChanges();
      const fakeGroup = {id: 1, title: 'First group', description: 'hi', hidden: false};

      expect(selectSpy).not.toHaveBeenCalled();
      component.onSelect(undefined as any);
      expect(selectSpy).not.toHaveBeenCalled();
      component.onSelect(null as any);
      expect(selectSpy).not.toHaveBeenCalled();
      component.onSelect({value: 'has value but no id'} as any);
      expect(selectSpy).not.toHaveBeenCalled();
      component.onSelect(MockGroup.new(42));
      expect(selectSpy).toHaveBeenCalledOnceWith({type: 'group', item: MockGroup.new(42)});
      tick(); // need this to flush the autocomplete reopen timer (See #122)
    }));

    it('should clear input after selecting content', fakeAsync(() => {
      component.type = 'group';
      fixture.detectChanges();
      const fakeValue = 'some value';
      const fakeGroup = MockGroup.new(1);;

      let input = fixture.debugElement.query(By.css('input'));
      input.nativeElement.value = fakeValue;
      input.nativeElement.dispatchEvent(new Event('input'));
      expect(input.nativeElement.value).toEqual(fakeValue);
      component.onSelect(fakeGroup);
      expect(input.nativeElement.value).toEqual('');
      tick(); // need this to flush the autocomplete reopen timer (See #122)
    }));

    it('should fire create when selection has no id', fakeAsync(() => {
      component.type = 'artist';
      component.allowCreate = true;
      fixture.detectChanges();

      expect(createSpy).not.toHaveBeenCalled();
      expect(selectSpy).not.toHaveBeenCalled();
      component.onSelect({value: 'new name'} as any as ContentValue);
      expect(createSpy).toHaveBeenCalledOnceWith({type: 'artist', value: 'new name'});
      expect(selectSpy).not.toHaveBeenCalled();
      tick(); // need this to flush the autocomplete reopen timer (See #122)
    }));

  })

  describe('ui features', () => {
    let createSpy: jasmine.Spy;
    let selectSpy: jasmine.Spy;
    let input: HTMLInputElement;

    beforeEach(() => {
      createSpy = spyOn(component.create, 'emit').and.callThrough();
      selectSpy = spyOn(component.selected, 'emit').and.callThrough();
      input = component.input.nativeElement;
    });

    function open() {
      spyOn(input, 'focus');
      component.open();
      fixture.detectChanges();
      tick();
      expect(input.focus).toHaveBeenCalledTimes(1);
      // Note: theoretically, I shouldn't need this line since the open function already calls
      // 'focus', but for some reason it doesn't work in tests.
      input.dispatchEvent(new Event('focusin'));
      fixture.detectChanges();
    }

    it('should show an option for each matching content item', fakeAsync(() => {
      const contentList = [new MockArtist(1, 'hog wash'), new MockArtist(2, 'hog feet'), new MockArtist(1, 'van')];
      const inputValues = ['', 'hog ', 'hog w'];
      const expectedOptionSizes = [3, 2, 1];

      component.allowCreate = false;
      component.type = 'artist';
      fixture.detectChanges();
      fakeContentResponse.artist.next(contentList);

      open();

      inputValues.forEach((value, i) => {
        input.value = value;
        input.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        expect(document.querySelectorAll('mat-option').length).toBe(expectedOptionSizes[i]);
      });
      tick();
    }));

    it('should update list of options when type changes', fakeAsync(() => {
      const artistList = [new MockArtist(1, 'hog 1'), new MockArtist(2, 'hog 2')];
      const groupList = [new MockGroup(1, 'wash 1')];

      component.allowCreate = false;
      component.type = 'artist';
      fixture.detectChanges();
      fakeContentResponse.artist.next(artistList);
      fakeContentResponse.group.next(groupList);

      open();

      expect(document.querySelectorAll('mat-option').length).toBe(2);

      component.type = 'group';
      fixture.detectChanges();

      expect(document.querySelectorAll('mat-option').length).toBe(1);

      tick();
    }));

    it('should fire select when option is selected', fakeAsync(() => {
      const selectedIndex = 0;
      const expectedSelect = {type: 'artist', item: fakeContentLists.artist[0]};

      component.allowCreate = false;
      component.type = 'artist';
      fixture.detectChanges();
      fakeContentResponse.artist.next(fakeContentLists.artist);

      open();

      document.querySelectorAll('mat-option')[selectedIndex].dispatchEvent(new Event('click'));
      fixture.detectChanges();

      expect(selectSpy).toHaveBeenCalledOnceWith(expectedSelect);
      tick();
    }));

    it('should fire create event when create new is clicked with input', fakeAsync(() => {
      component.allowCreate = true;
      component.type = 'artist';
      fixture.detectChanges();
      fakeContentResponse.artist.next(fakeContentLists.artist);

      open();

      input.value = 'a new artist';
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      document.querySelectorAll('mat-option')[0].dispatchEvent(new Event('click'));
      tick();
      fixture.detectChanges();
      expect(createSpy).toHaveBeenCalledOnceWith({type: 'artist', value: 'a new artist'});
    }));

    it('should not fire create event when create new is clicked with empty input', fakeAsync(() => {
      component.allowCreate = true;
      component.type = 'artist';
      fixture.detectChanges();
      fakeContentResponse.artist.next(fakeContentLists.artist);

      open();

      input.value = '';
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      document.querySelectorAll('mat-option')[0].dispatchEvent(new Event('click'));
      tick();
      fixture.detectChanges();
      expect(createSpy).not.toHaveBeenCalled();
    }));

  });

  it('should have more unit tests that test autocomplete/focus behavior', () => {
    // I have had trouble doing this. When I try basic things like `component.open()` or
    // `input.click()`, the autocomplete does not seem to open.
  });

});
