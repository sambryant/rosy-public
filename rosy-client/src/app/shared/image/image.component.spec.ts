import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {SafeUrl} from '@angular/platform-browser';
import {Subject} from 'rxjs';

import {Image} from '@app/models';
import {ApiService, ImageFetcherService, LibraryService, NotificationService, } from '@app/services';
import {MockImage} from '@app/models/testing';

import {ImageComponent, ImageTooltipPipe, ThumbnailComponent, } from '.';


describe('ImageComponent', () => {
  let component: ImageComponent;
  let fixture: ComponentFixture<ImageComponent>;
  let target: Image;

  let imageFetcher: ImageFetcherService;
  let getImageUrlSpy: jasmine.Spy;
  let mockImageResp: Subject<SafeUrl>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ImageComponent,
        ImageTooltipPipe,
      ],
      imports: [
        MatTooltipModule,
        NoopAnimationsModule,
      ],
      providers: [
        MockProvider(NotificationService),
        MockProvider(ApiService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
    imageFetcher = TestBed.inject(ImageFetcherService);
    mockImageResp = new Subject();
    getImageUrlSpy = spyOn(imageFetcher, 'getImage').and.returnValue(mockImageResp);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    target = MockImage.new(1);
    component.target = target;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should request a link to the target content', () => {
    target = MockImage.new(1);
    component.target = target;
    fixture.detectChanges();

    // Check that URLs appear as expected
    const expUrls = [null, 'fake url' as SafeUrl];
    let counter = 0;
    const sub = component.contentUrl.subscribe((url) => {
      expect(url).toEqual(expUrls[counter]);
      counter += 1;
    });

    expect(component.loading).toBeTrue();
    expect(imageFetcher.getImage).toHaveBeenCalledOnceWith(target);

    mockImageResp.next('fake url' as SafeUrl);

    expect(component.loading).toBeFalse();
    expect(counter).toBe(2);
  });

  it('should set showVideo after the target loads', () => {
    target = MockImage.new(1);
    target.isVideo = true;
    component.target = target;
    fixture.detectChanges();

    expect(component.showVideo).toBeFalsy();
    mockImageResp.next('fake url' as SafeUrl);
    expect(component.showVideo).toBeTrue();
  });

  it('should force load of video content when target changes', () => {
    component.target = MockImage.new(42);
    fixture.detectChanges();
    spyOn(component.video.nativeElement, 'load');

    const fakeTargets = [ MockImage.new(1), MockImage.new(2), ];
    fakeTargets[0].isVideo = false;
    fakeTargets[1].isVideo = true;

    component.target = fakeTargets[0];
    fixture.detectChanges();
    mockImageResp.next('fake url' as SafeUrl);
    expect(component.video.nativeElement.load).not.toHaveBeenCalled();

    component.target = fakeTargets[1];
    fixture.detectChanges();
    mockImageResp.next('fake url' as SafeUrl);
    expect(component.video.nativeElement.load).toHaveBeenCalledTimes(1);
  });

 it('should pause a video when target changes', () => {
    component.target = MockImage.new(42);
    fixture.detectChanges();
    spyOn(component.video.nativeElement, 'pause');

    const fakeTargets = [ MockImage.new(1), MockImage.new(2), ];
    fakeTargets[0].isVideo = true;
    fakeTargets[1].isVideo = false;

    component.target = fakeTargets[0];
    fixture.detectChanges();
    mockImageResp.next('fake url' as SafeUrl);

    component.target = fakeTargets[1];
    fixture.detectChanges();
    mockImageResp.next('fake url' as SafeUrl);
    expect(component.video.nativeElement.pause).toHaveBeenCalled();
  });

  it('should render in fullscreen', () => {
    spyOn(fixture.nativeElement, 'requestFullscreen');
    component.target = MockImage.new(1);

    // A button should appear when allowFullscreen is true
    component.allowFullscreen = true;
    fixture.detectChanges();
    fixture.nativeElement
      .querySelector('button')
      .dispatchEvent(new MouseEvent('click', { bubbles: true }));
    expect(fixture.nativeElement.requestFullscreen).toHaveBeenCalledTimes(1);

    // No button should appear when allowFullscreen is false
    component.allowFullscreen = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('button')).toBeFalsy();
  });

});


describe('ThumbnailComponent', () => {
  let component: ThumbnailComponent;
  let fixture: ComponentFixture<ThumbnailComponent>;
  let target: Image;

  let libraryService: LibraryService;
  let getThumbUrlSpy: jasmine.Spy;
  let mockThumbResp: Subject<SafeUrl>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ThumbnailComponent,
        ImageTooltipPipe,
      ],
      imports: [
        MatTooltipModule,
        NoopAnimationsModule,
      ],
      providers: [
        MockProvider(NotificationService),
        MockProvider(ApiService),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
    libraryService = TestBed.inject(LibraryService);
    mockThumbResp = new Subject();
    getThumbUrlSpy = spyOn(libraryService, 'getThumbnailBlobUrl').and.returnValue(mockThumbResp);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThumbnailComponent);
    component = fixture.componentInstance;
  });

  it('should request a link to the target content', () => {
    target = MockImage.new(1);
    component.target = target;
    fixture.detectChanges();

    // Check that URLs appear as expected
    const expUrls = [null, 'fake url' as SafeUrl];
    let counter = 0;
    const sub = component.contentUrl.subscribe((url) => {
      expect(url).toEqual(expUrls[counter]);
      counter += 1;
    });

    expect(component.loading).toBeTrue();
    expect(libraryService.getThumbnailBlobUrl).toHaveBeenCalledOnceWith(target);

    mockThumbResp.next('fake url' as SafeUrl);

    expect(component.loading).toBeFalse();
    expect(counter).toBe(2);
  });

});
