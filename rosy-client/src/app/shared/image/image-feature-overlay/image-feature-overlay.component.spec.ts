import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageFeatureOverlayComponent } from './image-feature-overlay.component';

describe('ImageFeatureOverlayComponent', () => {
  let component: ImageFeatureOverlayComponent;
  let fixture: ComponentFixture<ImageFeatureOverlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageFeatureOverlayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageFeatureOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
