import {
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {fromEvent, Subject, } from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {BACKSPACE, ESCAPE} from '@angular/cdk/keycodes';

import {
  Action,
  ContentMeta,
  Feature,
  Image,
  ImageFeature,
  Point,
} from '@app/models';
import {ConfigService, LibraryService, } from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-image-feature-overlay',
  templateUrl: './image-feature-overlay.component.html',
  styleUrls: ['./image-feature-overlay.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '[class.rx-hidden]': '!visible',
    '[class.crosshair]': '!!newFeature',
  },
})
export class ImageFeatureOverlayComponent extends UnsubscribeOnDestroyedDirective implements OnInit {

  /**
   * The target image for which the features should be shown.
   */
  @Input() get target(): Image { return this._target; }
  set target(target: Image) { this.setTarget(target); }
  protected _target: Image;

  /**
   * Null except when user is currently adding a new feature, in that case, it's the feature they
   * are about to add.
   */
  newFeature: Feature | null = null;

  /**
   * The first point in the rectangle for a new feature. This is only not-null while the user is
   * adding a new feature and they have clicked on the first point already.
   */
  newFeatureP1: Point | null = null;

  /**
   * Fires whenever user starts/stops adding a new feature to an image.
   *
   * While user is adding a feature, this component captures key presses to cancel. This observable
   * is used to stop the key press capture.
   */
  newFeature$: Subject<null> = new Subject();

  /**
   * Whether this component should explicitly be visible.
   * This tracks value from configService via a subscription.
   */
  showImageFeatures: boolean;

  /**
   * Whether this component is visible. True if either showImageFeatures or user is adding a new
   * feature.
   */
  visible: boolean = false;

  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;

  protected flContext: ImageFeatureGraphics | null = null;

  protected imgEl: HTMLImageElement | null = null;

  constructor(
    protected libraryService: LibraryService,
    protected configService: ConfigService,
  ) {
    super();
  }

  ngAfterViewInit() {
    this.flContext = new ImageFeatureGraphics(this.canvas.nativeElement);
  }

  ngOnInit(): void {
    this.configService.showImageFeatures$.pipe(takeUntil(this.destroyed$))
      .subscribe((value) => {
        this.showImageFeatures = value;
        this.updateVisibility();
      });
    this.libraryService.imageUpdated$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.updateGraphics();
      });
  }

  /**
   * Updates the image feature graphics by drawing the image features as boxes.
   *
   * This only actually draws the features:
   *  - The target image is valid
   *  - The canvas element is visible (see `visible`) and has real dimensions.
   *  - The graphics context `flContext` has been initialized.
   *  - The HTMLImageElement `imgEl` exists and has been loaded (has real dimensions).
   *
   * The following events should all trigger a redrawing of the image features:
   *   1. When `visible` changes.
   *   2. When the image element loads (an (onload) event).
   *   3. When the user starts adding a new feature.
   *   5. When the target image's data is updated.
   *   6. When the target image changes.
   * In the future, may also include:
   *   - When the user clicks while adding a new feature
   */
  protected updateGraphics(mousePos: [number, number] | null=null) {
    if (this.visible && this.flContext && this.imgEl && this._target) {
      this.flContext.updateGraphics(this._target, this.imgEl, this.newFeatureP1, mousePos);
    }
  }

  protected updateVisibility() {
    const visible = this.showImageFeatures || !!this.newFeature;
    if (visible !== this.visible) {
      this.visible = this.showImageFeatures || !!this.newFeature;
      // We need to allow angular to display the canvas before updating the graphics so its
      // dimensions are correct.
      setTimeout(() => this.updateGraphics(), 0);
    }
  }

  public imageLoaded(img: HTMLImageElement | null) {
    this.imgEl = img;
    this.updateGraphics();
  }

  public startNewFeature(feature: Feature) {
    this.newFeature = feature;
    this.newFeatureP1 = null;
    this.newFeature$.next(null);
    this.updateVisibility();

    // Listen for ESCAPE and BACKSPACE to cancel adding a new feature
    fromEvent(document, 'keydown')
      .pipe(takeUntil(this.newFeature$))
      .pipe(takeUntil(this.destroyed$))
      .subscribe((ev: any) => {
        if (ev.keyCode === ESCAPE || ev.keyCode === BACKSPACE) {
          this.stopNewFeature();
        }
      });
  }

  protected stopNewFeature() {
    this.newFeature = null;
    this.newFeatureP1 = null;
    this.newFeature$.next(null);
    this.updateVisibility();
  }

  handleCanvasClick(event) {
    if (this.newFeature !== null && this.flContext && this.imgEl && this._target) {
      const pt = this.flContext.clickToPoint(this.imgEl, event);
      if (pt !== null) {
        if (this.newFeatureP1) {
          this.libraryService.createImageFeature(this._target, this.newFeature, this.newFeatureP1, pt);
          this.newFeature = null;
          this.newFeatureP1 = null;
          this.updateVisibility();
        } else {
          this.newFeatureP1 = pt;
          this.updateGraphics();
        }
      }
    }
  }

  /**
   * Draw line following cursor position.
   */
  mouseMoved(event) {
    if (this.newFeature) {
      this.updateGraphics([event.layerX, event.layerY]);
    }
  }

  mouseLeft() {
    this.updateGraphics(null);
  }

  protected setTarget(target: Image) {
    this._target = target;
    this.newFeatureP1 = null; // reset any new features
    this.updateGraphics();
  }

}

export const FEATURE_LABEL_BORDER_LW = 2;

export const FEATURE_LABEL_TEXT_COLOR = 'white';

export const FEATURE_LABEL_TEXT_SIZE = '16px Arial';

/**
 * Represents the true dimensions and offset of a physical image drawn within the canvas space.
 */
export class ImageGeometry {

  constructor(public w: number, public h: number, public x0: number, public y0: number) {}

  /**
   * Computes the true dimensions and offset of a physical image drawn within the canvas space.
   */
  static compute(can: HTMLCanvasElement, img: HTMLImageElement): ImageGeometry | null {
    // Check if canvas and image are actually rendered (have real dimensions)
    if (can.clientWidth === 0 || can.clientHeight === 0 || img.naturalWidth === 0 || img.naturalHeight === 0) {
      return null;
    }
    // The image size (width, height) is the full canvas size because of the way its drawn. However
    // the space occupied by the image itself is smaller. This computes the offset of the image and
    // its true size, assuming it's centered within the canvas.
    const canDim = new Dims(can.clientWidth, can.clientHeight);
    const imgNatDim = new Dims(img.naturalWidth, img.naturalHeight);
    const geo = new ImageGeometry(canDim.w, canDim.h, 0, 0);
    if (imgNatDim.aspect > canDim.aspect) {
      geo.h = canDim.w / imgNatDim.aspect;
      geo.y0 = (canDim.h - geo.h) / 2;
    } else {
      geo.w = imgNatDim.aspect * canDim.h;
      geo.x0 = (canDim.w - geo.w) / 2;
    }
    return geo;
  }

  /**
   * Transforms a click event on the canvas into a point representing the fractional position with
   * respect to the image.
   *
   * When the click is on the canvas, but outisde the bounds of the image, this returns null.
   *
   * This is used to add new image feature boxes.
   */
  transClickToFraction(cx: number, cy: number): Point | null {
    const x = (cx - this.x0) / this.w;
    const y = (cy - this.y0) / this.h;
    if (x < 0.0 || x > 1.0 || y < 0.0 || y > 1.0) {
      return null;
    } else {
      return {x, y};
    }
  }

  /**
   * Transforms the fractional bounds of a rectangle within the image into an `ImageGeometry`
   * representing the pixel width, height, and offsets of the rectangle.
   *
   * This is used to draw the image feature boxes.
   */
  transformFractionalRect(x1: number, y1: number, x2: number, y2: number): ImageGeometry {
    const x0 = this.x0 + (x1 * this.w);
    const y0 = this.y0 + (y1 * this.h);
    const w = (x2 - x1) * this.w;
    const h = (y2 - y1) * this.h;
    return new ImageGeometry(w, h, x0, y0);
  }

  /**
   * Transforms the given fractional point into a point representing the pixel position.
   *
   * This is used to draw the points for new image feature boxes.
   */
  transformFractionalPoint(p: Point): Point {
    return {
      x: this.x0 + p.x * this.w,
      y: this.y0 + p.y * this.h,
    };
  }

}

export class Dims {
  public aspect: number;

  constructor(public w: number, public h: number) {
    this.aspect = this.w / this.h;
  }
}

export class ImageFeatureGraphics {
  public ctx: CanvasRenderingContext2D;

  constructor(canvas: HTMLCanvasElement) {
    this.ctx = canvas.getContext('2d')!;
  }

  updateGraphics(target: Image, img: HTMLImageElement, point: Point | null = null, mousePos: [number, number] | null = null) {
    // Get transformations required to map image to canavs
    const geo = ImageGeometry.compute(this.ctx.canvas, img);
    if (geo) {
      // Set logical canvas coords to match physical. NOTE: This clears the canvas.
      this.ctx.canvas.width = this.ctx.canvas.clientWidth;
      this.ctx.canvas.height = this.ctx.canvas.clientHeight;
      this.resetContextParams();

      target.features.forEach((fl) => {
        const trans = geo.transformFractionalRect(fl.x1, fl.y1, fl.x2, fl.y2);
        this.ctx.strokeStyle = '#F90';
        this.ctx.strokeRect(trans.x0, trans.y0, trans.w, trans.h);
        this.ctx.fillText(`${fl.id}: ${fl.value}`, trans.x0 + trans.w/2, trans.y0)
      });
      if (point) {
        const pt = geo.transformFractionalPoint(point);
        this.ctx.strokeStyle = '#0F9';
        this.ctx.beginPath();
        this.ctx.arc(pt.x, pt.y, 2, 0, 6.3);
        this.ctx.stroke();
        this.ctx.beginPath();
        this.ctx.moveTo(0.5, 0.0);
        this.ctx.lineTo(0.5, 1.0);
        this.ctx.stroke();
      }
      if (mousePos) {
        const x = mousePos[0];
        const y = mousePos[1];
        const x2 = geo.x0 + geo.w;
        const y2 = geo.y0 + geo.h;
        if ((x >= geo.x0 && x <= x2) && (y >= geo.y0 && y <= y2)) {
          if (point) {
            this.ctx.strokeStyle = '#90F';
          } else {
            this.ctx.strokeStyle = '#0F9';
          }
          this.ctx.beginPath();
          this.ctx.moveTo(x, geo.y0);
          this.ctx.lineTo(x, y2);
          this.ctx.stroke();
          this.ctx.beginPath();
          this.ctx.moveTo(geo.x0, y);
          this.ctx.lineTo(x2, y);
          this.ctx.stroke();
        }
      }
    }
  }

  clickToPoint(img: HTMLImageElement, click: any): Point | null {
    const geo = ImageGeometry.compute(this.ctx.canvas, img);
    if (geo) {
      return geo.transClickToFraction(click.layerX, click.layerY);
    }
    return null;
  }

  protected resetContextParams() {
    this.ctx.lineWidth = FEATURE_LABEL_BORDER_LW;
    this.ctx.fillStyle = FEATURE_LABEL_TEXT_COLOR;
    this.ctx.font = FEATURE_LABEL_TEXT_SIZE;
    this.ctx.textBaseline = 'bottom';
    this.ctx.textAlign = 'center';
  }

}
