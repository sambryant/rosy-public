import {Pipe, PipeTransform, } from '@angular/core';

import {Image} from '@app/models';

const MB = 1024 * 1024;
const KB = 1024;

@Pipe({
  name: 'imageTooltip'
})
export class ImageTooltipPipe implements PipeTransform {

  filesizePipe(size: number): string {
    if (size < MB) {
      return (size / KB).toFixed(2) + 'kB';
    } else {
      return (size / MB).toFixed(2) + 'MB';
    }
  }

  transform(image: Image): string {
    const size = this.filesizePipe(image.filesize);
    if (image.names.length === 0) {
      return `${image.id}: ${size}, (no names)`;
    } else {
      const nameStr = image.names.map(t => t.value).join(', ');
      return `${image.id}: ${size}, ${nameStr}`;
    }
  }

}
