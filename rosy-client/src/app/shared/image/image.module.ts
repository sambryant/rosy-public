import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ServicesModule} from '@app/services';

import {ImageComponent, ThumbnailComponent, } from './image.component';
import {ImageFeatureOverlayComponent} from './image-feature-overlay/image-feature-overlay.component';
import {ImageTooltipPipe} from './image-tooltip.pipe';


@NgModule({
  declarations: [
    ImageComponent,
    ImageFeatureOverlayComponent,
    ImageTooltipPipe,
    ThumbnailComponent,
  ],
  exports: [
    ImageComponent,
    ImageFeatureOverlayComponent,
    ThumbnailComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    RouterModule,
    ServicesModule
  ]
})
export class ImageModule { }
