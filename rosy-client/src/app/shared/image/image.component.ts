import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, } from 'rxjs';
import {SafeUrl} from '@angular/platform-browser';

import {APP_PAGES} from '@app/app-page.model';
import {Image} from '@app/models';
import {ImageFetcherService, LibraryService} from '@app/services';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageComponent extends UnsubscribeOnDestroyedDirective {

  /**
   * The target whose content this component should display.
   */
  @Input()
  get target(): Image { return this._target; }
  set target(target: Image) {
    this.setImage(target);
  }

  /**
   * When true, wraps content in `<a>` tags linking to the standalone `image-details-page`.
   */
  @Input() linkImage = false;

  /**
   * When true, displays a fullscreen button that requests full screen accesss for this element.
   */
  @Input() allowFullscreen = false;

  /**
   * When true, overlays image with the image tooltip.
   */
  @Input() showTooltip = false;

  @Output() imageLoaded: EventEmitter<HTMLImageElement> = new EventEmitter();

  @ViewChild('video') video: ElementRef<HTMLVideoElement>;

  /**
   * A SafeUrl pointing to a locally stored blob containing target content (video or image).
   *
   * It's important that this is a BehaviorSubject because the video source must be defined or it
   * will trigger an error. It does not like `Subject | async` for some reason.
   */
  contentUrl: BehaviorSubject<SafeUrl | null> = new BehaviorSubject(null);

  /**
   * Relative URL pointing towards application page dedicated to the target.
   *
   * This is used when `linkImage` is true to set the `<a href` target around the content.
   */
  targetPageUrl: string;

  /**
   * Whether content is displayed with <video> or <image> - recomputed when 'target' changes.
   */
  showVideo: boolean = false;

  /**
   * True when content is being loaded into locally stored blob obtained from server.
   */
  loading = false;

  /**
   * Indicates that target should be displayed in thumbnail mode.
   *
   * This is always false in this class and always true for ThumbnailComponent. Ideally this would
   * be bound through `@Input()` but it won't work because it's value is needed when `setImage` is
   * called which may be before it's property is bound here.
   */
  protected thumbnail = false;

  /**
   * Fires whenver the target changes to cancel the previous link subscription.
   */
  protected contentChanged = new EventEmitter<void>();

  protected _target: Image;

  constructor(
    protected element: ElementRef,
    protected imageFetcherService: ImageFetcherService) {
    super();
  }

  protected setImage(target: Image) {
    this._target = target;
    this.contentChanged.emit();

    // Update the href link to the new target.
    this.targetPageUrl = APP_PAGES.image.getBaseUrl() + '/' + target.id;

    // If old target was a video, we should clear it and pause it.
    if (this.video) {
      this.video.nativeElement.pause();
    }

    // Start loading the new content.
    this.loading = true;
    this.loadContent()
      .pipe(takeUntil(this.destroyed$))
      .pipe(takeUntil(this.contentChanged))
      .subscribe((url: SafeUrl) => {
        this.showVideo = !this.thumbnail && this.target.isVideo;
        this.loading = false;
        this.contentUrl.next(url);
        if (this.showVideo) {
          this.video.nativeElement.load();
        }
      });
  }

  protected loadContent(): Observable<SafeUrl> {
    return this.imageFetcherService.getImage(this._target);
  }

  onImageLoad(target: EventTarget | null) {
    if (target) {
      this.imageLoaded.emit(target as HTMLImageElement);
    }
  }

  requestFullscreen() {
    this.element.nativeElement.requestFullscreen();
  }

}

@Component({
  selector: 'rx-thumbnail',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
  host: {
    '[class.thumbnail]': `true`,
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThumbnailComponent extends ImageComponent {

  constructor(
    element: ElementRef,
    imageFetcherService: ImageFetcherService,
    protected libraryService: LibraryService) {
    super(element, imageFetcherService);
    this.thumbnail = true;
  }

  protected override loadContent(): Observable<SafeUrl> {
    return this.libraryService.getThumbnailBlobUrl(this._target);
  }

}
