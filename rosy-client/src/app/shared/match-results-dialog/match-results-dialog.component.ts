import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import {tap} from 'rxjs/operators';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

import {LibraryService, NotificationService, } from '@app/services';
import {DupeType, Image, ImageId, ImageSelection, } from '@app/models';
import {SelectionManager} from '@app/pages/browsers/tables/selection-manager';


export interface MatchResultsDialogData {
  type: 'dupes' | 'faces';
  target: Image;
}


@Component({
  selector: 'rx-match-results-dialog',
  templateUrl: './match-results-dialog.component.html',
  styleUrls: ['./match-results-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatchResultsDialogComponent implements OnInit {

  loading = true;

  matches: Image[] = [];

  message: string | null = null;

  type: 'dupes' | 'faces';

  /**
   * Source image whose matches are shown.
   */
  private target: Image;

  /**
   * Array indicating which items are selected.
   */
  rowSelected: boolean[] = [];

  /**
   * Number of selected images.
   */
  selectCount = 0;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: MatchResultsDialogData,
    protected libraryService: LibraryService,
    protected changeRef: ChangeDetectorRef) {
    this.type = data.type;
    this.target = data.target;
    this.loading = true;
    this.rowSelected = [];
  }

  ngOnInit(): void {
    let obs;
    if (this.type == 'faces') {
      obs = this.libraryService.queryFaceMatches(this.target)
        .pipe(tap((response) => { this.matches = response.items.map(m => m.item); }));
    } else {
      obs = this.libraryService.queryMarkedDupes(this.target)
        .pipe(tap((response) => { this.matches = response.items.map(([img, t]) => img); }));
    }
    obs.subscribe(response => {
      this.rowSelected = this.matches.map(_ => false);
      this.selectCount = 0;
      this.loading = false;
      this.setMessage();
      this.changeRef.markForCheck();
    }, error => {
      this.libraryService.handleErrorResponse(error);
      const msg = error && error.error && error.error.message || 'Unknown error';
      this.message = `Error: ${msg}`;
      this.matches = [];
      this.rowSelected = [];
      this.selectCount = 0;
      this.loading = false;
      this.changeRef.markForCheck();
    });
  }

  protected setMessage() {
    if (this.matches.length == 0) {
      this.message = 'No matching results!'
    } else if (this.selectCount > 0) {
      this.message = `Showing ${this.matches.length} matches, ${this.selectCount} selected`;
    } else {
      this.message = `Showing ${this.matches.length} matches`;
    }
  }

  markSelectedAsDuplicates(dupeType: DupeType) {
    if (this.selectCount > 0) {
      const images = this.matches.filter((image, index) => this.rowSelected[index]);
      images.push(this.target);
      this.libraryService.markDupes(images, dupeType);
    }
  }

  unmarkSelectedAsDuplicates() {
    if (this.selectCount > 0) {
      const images = this.matches.filter((image, index) => this.rowSelected[index]);
      images.push(this.target);
      this.libraryService.unmarkDupes(images);
    }
  }

  toggleSelection(index: number) {
    this.rowSelected[index] = !this.rowSelected[index];
    if (this.rowSelected[index]) {
      this.selectCount += 1;
    } else {
      this.selectCount -= 1;
    }
    this.setMessage();
    this.changeRef.markForCheck();
  }

}
