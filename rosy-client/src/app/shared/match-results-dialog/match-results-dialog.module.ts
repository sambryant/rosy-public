import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {NgModule} from '@angular/core';

import {ContextMenuModule} from '@app/shared/context-menu';
import {ImageModule} from '@app/shared/image';
import {ServicesModule} from '@app/services';

import {MatchResultsDialogComponent} from './match-results-dialog.component';


@NgModule({
  declarations: [
    MatchResultsDialogComponent,
  ],
  exports: [
    MatchResultsDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ContextMenuModule,
    ImageModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    ServicesModule,
  ]
})
export class MatchResultsDialogModule { }
