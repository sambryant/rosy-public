import {fakeAsync, tick, ComponentFixture, TestBed, } from '@angular/core/testing';
import {map} from 'rxjs/operators';
import {of, throwError, timer} from 'rxjs';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatDialogModule, MAT_DIALOG_DATA, } from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MockProvider} from 'ng-mocks';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {ApiService, NotificationService, } from '@app/services';
import {FaceMatch} from '@app/models';
import {MockImage} from '@app/models/testing';

import {MatchResultsDialogComponent, MatchResultsDialogData, MatchResultsDialogModule, } from '.';


describe('MatchResultsDialogComponent', () => {
  let component: MatchResultsDialogComponent;
  let fixture: ComponentFixture<MatchResultsDialogComponent>;
  let apiService: ApiService;
  let queryFaceMatchesSpy: jasmine.Spy;
  let queryMarkedDupesSpy: jasmine.Spy;
  let data: MatchResultsDialogData;

  beforeEach(async () => {
    data = {
      target: MockImage.new(11),
      type: 'faces'
    };

    TestBed.configureTestingModule({
      declarations: [
        MatchResultsDialogComponent
      ],
      imports: [
        MatDialogModule,
        MatMenuModule,
        NoopAnimationsModule,
      ],
      providers: [
        MockProvider(ApiService),
        MockProvider(NotificationService),
        { provide: MAT_DIALOG_DATA, useValue: data},
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    apiService = TestBed.inject(ApiService);
    queryFaceMatchesSpy = spyOn(apiService, 'queryFaceMatches');
    queryMarkedDupesSpy = spyOn(apiService, 'queryMarkedDupes');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchResultsDialogComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    queryFaceMatchesSpy.and.returnValue(of({items: [], count: 0}));
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should set matches from api call', () => {
    const fakeData: FaceMatch[] = [
      {item: MockImage.new(1), score: 0.01},
      {item: MockImage.new(2), score: 0.02},
    ];
    queryFaceMatchesSpy.and.returnValue(of({items: fakeData, count: 2}));
    fixture.detectChanges();

    expect(component.matches).toEqual(fakeData.map(m => m.item));
    expect(queryFaceMatchesSpy).toHaveBeenCalledWith(data.target);
  });

  it('should set loading until call is resolved', fakeAsync(() => {
    const fakeResponse = timer(1).pipe(map(() => ({
      count: 2,
      items: [
        {item: MockImage.new(1), score: 0.01},
        {item: MockImage.new(2), score: 0.02},
      ]
    })));
    queryFaceMatchesSpy.and.returnValue(fakeResponse);
    fixture.detectChanges();

    expect(component.loading).toBeTrue();
    expect(component.matches).toEqual([]);

    tick(10);

    expect(component.loading).toBeFalse();
    expect(component.matches.length).toEqual(2);
  }));

  it('should set message to number of results on non-empty match result', () => {
    const fakeResponse = of({
      count: 2,
      items: [
        {item: MockImage.new(1), score: 0.01},
        {item: MockImage.new(2), score: 0.02},
      ]
    });
    const expectedMessage = 'Showing 2 matches';

    queryFaceMatchesSpy.and.returnValue(fakeResponse);
    fixture.detectChanges();

    expect(component.message).toEqual(expectedMessage);
    expect(component.loading).toBeFalse();
  });

  it('should set message properly when match result is empty', () => {
    const fakeResponse = of({
      count: 0,
      items: []
    });
    const expectedMessage = 'No matching results!';

    queryFaceMatchesSpy.and.returnValue(fakeResponse);
    fixture.detectChanges();

    expect(component.message).toEqual(expectedMessage);
    expect(component.loading).toBeFalse();
  });

  it('should set message to error message when match call causes error', () => {
    const fakeResponse = throwError({
      error: {
        message: 'test something went wrong'
      }
    });
    const expectedMessage = 'Error: test something went wrong';

    queryFaceMatchesSpy.and.returnValue(fakeResponse);
    fixture.detectChanges();

    expect(component.message).toEqual(expectedMessage);
    expect(component.loading).toBeFalse();
  });

  it('should set message to generic on unstructured error', () => {
    const fakeResponse = throwError('hogmeat');
    const expectedMessage = 'Error: Unknown error';

    queryFaceMatchesSpy.and.returnValue(fakeResponse);
    fixture.detectChanges();

    expect(component.message).toEqual(expectedMessage);
    expect(component.loading).toBeFalse();
  });

  it('should handle querying dupe matches', () => {
    component.type = 'dupes';
    queryMarkedDupesSpy.and.returnValue(of({
      items: [
        [MockImage.new(1), 'Exact'],
        [MockImage.new(2), 'Partial'],
        [MockImage.new(3), 'None']
      ],
      count: 3
    }));
    fixture.detectChanges();

    expect(component.matches.map(item => item.id)).toEqual([1, 2, 3]);
  });

  describe('actions', () => {
    let fakeMatches: FaceMatch[];
    let markSpy: jasmine.Spy;
    let unmarkSpy: jasmine.Spy;

    beforeEach(() => {
      markSpy = spyOn(apiService, 'markDupes').and.returnValue(of({}));
      unmarkSpy = spyOn(apiService, 'unmarkDupes').and.returnValue(of({}));
      fakeMatches = [
        {item: MockImage.new(2), score: 0.001},
        {item: MockImage.new(3), score: 0.002},
        {item: MockImage.new(5), score: 0.003},
      ];
      queryFaceMatchesSpy.and.returnValue(of({count: fakeMatches.length, items: fakeMatches}));
      fixture.detectChanges();
    });

    it('should toggle a row', () => {
      expect(component.rowSelected).toEqual([false, false, false]);
      expect(component.selectCount).toBe(0);

      component.toggleSelection(1);
      expect(component.rowSelected).toEqual([false, true, false]);
      expect(component.selectCount).toBe(1);

      component.toggleSelection(1);
      expect(component.rowSelected).toEqual([false, false, false]);
      expect(component.selectCount).toBe(0);
    });

    it('should change messege when items are selected', () => {
      expect(component.message).toEqual('Showing 3 matches');
      component.toggleSelection(1);
      expect(component.message).toEqual('Showing 3 matches, 1 selected');
      component.toggleSelection(1);
      expect(component.message).toEqual('Showing 3 matches');
    });

    it('should not mark selection as duplicates when no selection', () => {
      expect(component.rowSelected).toEqual([false, false, false]);
      component.markSelectedAsDuplicates('Exact');
      expect(markSpy).not.toHaveBeenCalled();
    });

    it('should mark selection as duplicates', () => {
      const selectedIndex = 1;
      const expIds = [fakeMatches[1].item.id, data.target.id];

      component.toggleSelection(selectedIndex);
      component.markSelectedAsDuplicates('Exact');
      expect(markSpy).toHaveBeenCalledWith(expIds, 'Exact');
    });

    it('should not unmark selection as duplicates when no selection', () => {
      expect(component.rowSelected).toEqual([false, false, false]);
      component.unmarkSelectedAsDuplicates();
      expect(unmarkSpy).not.toHaveBeenCalled();
    });

    it('should unmark selection as duplicates', () => {
      const selectedIndex = 1;
      const expIds = [fakeMatches[1].item.id, data.target.id];

      component.toggleSelection(selectedIndex);
      component.unmarkSelectedAsDuplicates();
      expect(unmarkSpy).toHaveBeenCalledWith(expIds);
    });
  });


});
