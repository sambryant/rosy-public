import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {
  EditableComponent,
  EditFocusDirective,
  EditModeDirective,
  ViewModeDirective
} from './editable.component';


@NgModule({
  declarations: [
    EditableComponent,
    ViewModeDirective,
    EditModeDirective,
    EditFocusDirective,
  ],
  exports: [
    EditableComponent,
    ViewModeDirective,
    EditModeDirective,
    EditFocusDirective,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
})
export class EditableModule { }
