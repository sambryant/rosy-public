/**
 * A component to show different views for edit and view mode.
 *
 * The main component is the `rx-editable` component which uses three sub-directives:
 *     - rxViewMode: indicates template to be displayed in view mode
 *     - rxEditMode: indicates template to be displayed in edit mode (i.e. after clicking view)
 *     - rxEditFocus (optional): indicates element to be focused when switching to edit mode.
 *
 * By default it will display the rxViewMode template. When that element is clicked, it will switch
 * into edit mode and show the rxEditMode template. Then when the user clicks *outside* of the edit
 * template, the mode switches back to view and the component fires an `update` event.
 *
 * You can also tag an element within the rxEditMode directive with rxEditFocus. This will cause
 * that element's `focus` method to be called when switching to edit mode. This is useful for
 * `input` elements in particular.
 *
 * The basic usage is to provide an editable template and a view template, e.g.
 *
 *    <rx-editable (update)="onEdited()">
 *        <div rxViewMode class="display-value">
 *            {{value}}
 *        </div>
 *        <input rxEditMode rxEditFocus [(ngModel)]="value">
 *    </rx-editable>
 *
 * ** More complicated focus scenario **
 *
 * Sometimes you want focus behavior more complicated than raw HTML focus. In this case you can
 * handle the focus event with a listener function.
 *
 *        <div rxEditMode>
 *            <rx-content-input #groupInput (rxEditFocus)="groupInput.open()">
 *            </rx-content-input>
 *         </div>
 */
import {
  fromEvent,
  merge,
  Observable,
  NEVER,
  Subject,
  Subscription,
} from 'rxjs';
import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import {filter, take, takeUntil, tap, } from 'rxjs/operators';

import {safeUnsubscribeAll} from '@app/shared/utility';
import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Directive({ selector: '[rxEditFocus]' })
export class EditFocusDirective {

  @Output() rxEditFocus = new EventEmitter<void>();

  constructor(public host: ElementRef) {}

  public focus(): void {
    this.host.nativeElement.focus();
    this.rxEditFocus.emit();
  }
}


@Directive({})
class SubModalDirective {
  constructor(public host: ElementRef) {}
  hide() { this.host.nativeElement.classList.add('rx-hidden'); }
  show() { this.host.nativeElement.classList.remove('rx-hidden'); }
}


@Directive({selector: '[rxEditMode]'})
export class EditModeDirective extends SubModalDirective {}


@Directive({selector: '[rxViewMode]'})
export class ViewModeDirective extends SubModalDirective {}


@Component({
  selector: 'rx-editable',
  template: '<ng-content></ng-content>',
})
export class EditableComponent
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  @Input() editable = true;

  /**
   * Event fired whenever the editable component switches from edit mode to view mode.
   */
  @Output() update = new EventEmitter<void>();

  @ContentChild(EditFocusDirective, {static: true}) editFocusable: EditFocusDirective;

  @ContentChild(ViewModeDirective, {static: true}) viewElement: ViewModeDirective;

  @ContentChild(EditModeDirective, {static: true}) editElement: EditModeDirective;

  private stopViewMode$: Observable<any>;
  private viewModeSubscription: Subscription | null = null;
  private stopEditMode$: Observable<any>;
  private editModeSubscription: Subscription | null = null;

  constructor(private host: ElementRef, private cdr: ChangeDetectorRef)
  {
    super();
  }

  ngOnInit(): void {
    this.stopViewMode$ = fromEvent(this.host.nativeElement, 'click')
      .pipe(takeUntil(this.destroyed$))
      .pipe(take(1));

    // If there is a `editFocusable` directive, we want to exit edit mode when it loses focus.
    // In any case, we want to exit edit mode when user clicks outside of editable element.
    const editBlurred = (this.editFocusable ? fromEvent(this.editFocusable.host.nativeElement, 'blur') : NEVER)
      .pipe(takeUntil(this.destroyed$));
    const editClickOff = fromEvent(document, 'click')
      .pipe(takeUntil(this.destroyed$))
      .pipe(filter(({target}) => !this.host.nativeElement.contains(target)))
      .pipe(tap(e => e.stopPropagation()));
    this.stopEditMode$ = merge(editClickOff, editBlurred).pipe(take(1));
    this.view(false);
  }

  public view(emitUpdate = true): void {
    safeUnsubscribeAll([this.viewModeSubscription, this.editModeSubscription]);
    if (emitUpdate) {
      this.update.emit();
    }
    this.viewElement.show();
    this.editElement.hide();
    this.cdr.detectChanges();
    this.viewModeSubscription = this.stopViewMode$.subscribe((e: Event) => {
      this.edit();
    });
  }

  public edit(): void {
    if (!this.editable) {
      return;
    }
    safeUnsubscribeAll([this.viewModeSubscription, this.editModeSubscription]);
    this.viewElement.hide();
    this.editElement.show();
    this.cdr.detectChanges();
    this.focusEditableElement();

    this.editModeSubscription = this.stopEditMode$.subscribe(() => {
      this.view();
    });
  }

  private focusEditableElement() {
    // Attempt to focus target if it's present, not sure why timeout needed but it is.
    setTimeout(() => {
      if (this.editFocusable) {
        this.editFocusable.focus();
      }
    }, 0);
  }
}
