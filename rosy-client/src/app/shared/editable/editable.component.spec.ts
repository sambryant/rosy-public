import {
  Component,
  DebugElement,
  Directive,
  ElementRef,
  Type,
  ViewChild
} from '@angular/core';
import {fakeAsync, tick, ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {
  EditableComponent,
  EditableModule,
  EditModeDirective,
  EditFocusDirective,
  ViewModeDirective
} from '.';


describe('EditableComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        EditableModule,
        NoopAnimationsModule,
      ],
      declarations: [
        EditableWithoutFocusComponent,
        EditableWithFocusComponent,
      ]
    })
    .compileComponents();
  });

  it('should show view template by default', () => {
    const t = new BaseFixture(EditableWithoutFocusComponent);

    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(false);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(true);
  });

  it('should show edit template after click', () => {
    const t = new BaseFixture(EditableWithoutFocusComponent);

    // view visible, edit not
    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(false);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(true);

    // click on view element (allow to bubble)
    t.viewMode.nativeElement.dispatchEvent(new MouseEvent('click', { bubbles: true }));

    // edit visible, edit not
    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(true);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(false);
  });

  it('should respect editable=false', () => {
    const t = new BaseFixture(EditableWithoutFocusComponent, {editable: false});

    // view visible, edit not
    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(false);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(true);

    // click on view element (allow to bubble)
    t.viewMode.nativeElement.dispatchEvent(new MouseEvent('click', { bubbles: true }));

    // view visible, edit not
    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(false);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(true);
  });

  it('should show view template after click outside element', () => {
    const t = new BaseFixture(EditableWithoutFocusComponent);
    t.component.edit();

    // edit visible, edit not
    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(true);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(false);

    // click within editable
    t.editMode.nativeElement.dispatchEvent(new MouseEvent('click', { bubbles: true }));

    // expect no change b/c click within element
    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(true);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(false);

    // click outside of editable
    t.outsideElement.nativeElement.dispatchEvent(new MouseEvent('click', { bubbles: true }));

    // expect view visible, edit not
    expect(t.viewMode.nativeElement.classList.contains('rx-hidden')).toBe(false);
    expect(t.editMode.nativeElement.classList.contains('rx-hidden')).toBe(true);
  });

  it('should fire update only when switching from edit to view', () => {
    const t = new BaseFixture(EditableWithoutFocusComponent);
    const updateSpy = spyOn(t.component.update, 'emit').and.callThrough();
    t.component.edit();

    // switch to view mode, expect update to fire
    expect(updateSpy).toHaveBeenCalledTimes(0);
    t.outsideElement.nativeElement.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    expect(updateSpy).toHaveBeenCalledTimes(1);

    // switch to edit mode, expect update to not fire
    expect(updateSpy).toHaveBeenCalledTimes(1);
    t.viewMode.nativeElement.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    expect(updateSpy).toHaveBeenCalledTimes(1);
  });

  it('should only fire one update even if switching many times', () => {
    const t = new BaseFixture(EditableWithoutFocusComponent);
    const updateSpy = spyOn(t.component.update, 'emit').and.callThrough();
    t.component.edit();
    t.component.edit();
    t.component.edit();
    t.component.edit();
    t.component.edit();
    expect(updateSpy).not.toHaveBeenCalled();
    t.component.view();
    expect(updateSpy).toHaveBeenCalledTimes(1);
  });

  it('should focus into focusable element after click into edit mode', fakeAsync(() => {
    console.warn('Figure out why this test dont work');
    // const t = new BaseFixture(EditableWithFocusComponent);

    // t.viewMode.nativeElement.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    // t.fixture.detectChanges();
    // tick();

    // const inputEl = t.focusElement().nativeElement;
    // const focusEl = t.fixture.debugElement.query(By.css(':focus')).nativeElement;
    // expect(inputEl).toBe(focusEl);
  }));

});

class BaseFixture<T extends BaseTestHostDirective> {
  fixture: ComponentFixture<T>;
  hostComponent: T;
  component: EditableComponent;
  viewMode: ElementRef;
  editMode: ElementRef;
  outsideElement: ElementRef;

  constructor(testClass: Type<T>, hostProperties: object = {}) {
    this.fixture = TestBed.createComponent(testClass);
    this.hostComponent = this.fixture.componentInstance;
    Object.keys(hostProperties).forEach(key => {
      this.hostComponent[key] = hostProperties[key];
    });
    this.fixture.detectChanges();
    this.component = this.hostComponent.component;
    this.editMode = this.hostComponent.editMode;
    this.viewMode = this.hostComponent.viewMode;
    this.outsideElement = this.hostComponent.outsideElement;
  }
}


@Directive()
class BaseTestHostDirective {
  editable = true;
  @ViewChild(EditableComponent) component: EditableComponent;
  @ViewChild(ViewModeDirective, {read: ElementRef}) viewMode: ElementRef;
  @ViewChild(EditModeDirective, {read: ElementRef}) editMode: ElementRef;
  @ViewChild('outsideElement') outsideElement: ElementRef;
}

@Component({template: `
  <rx-editable [editable]="editable">
    <div rxViewMode>
      george footman foot
    </div>
    <div rxEditMode>
      moon faced kid
    </div>
  </rx-editable>
  <div #outsideElement>
    I dont work in this van
  </div>
`})
class EditableWithoutFocusComponent extends BaseTestHostDirective {}

@Component({template: `
  <rx-editable [editable]="editable">
    <div rxViewMode>
      george footman foot
    </div>
    <div rxEditMode>
      <input rxEditFocus />
    </div>
  </rx-editable>
  <div #outsideElement>
    I dont work in this van
  </div>
`})
class EditableWithFocusComponent extends BaseTestHostDirective {}
