import {Component, CUSTOM_ELEMENTS_SCHEMA, ViewChild, } from '@angular/core';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {GraphComponent, GraphLabelComponent, } from './graph.component';

describe('GraphComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GraphComponent,
        GraphLabelComponent,
        TestHostComponent,
        TestHostComponentWithLabels,
      ],
      imports: [
        MatTooltipModule,
        NoopAnimationsModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  });

  describe('without labels', () => {
    let hostComponent: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(TestHostComponent);
      hostComponent = fixture.componentInstance;
    });

    it('should calculate the height fractions and value signs', () => {
      const max = 20.0;
      const values = [10.0, -5.0, 15.0, 0.2];
      const expHeights = [50, 25, 75, 1];
      const expSigns = [true, false, true, true];

      hostComponent.height = 100;
      hostComponent.max = max;
      hostComponent.values = values;
      hostComponent.allowNegative = true;
      fixture.detectChanges();
      const component = fixture.componentInstance.component;

      expect(component.heights).toEqual(expHeights);
      expect(component.signs).toEqual(expSigns);
    });

    it('should display divs with the expected height', () => {
      const height = 128; // px
      const max = 20.0;
      const values = [10.0, -5.0, 15.0, 0.3125];
      const expDivHeights = [32, 16, 48, 1];
      const expPosHiddens = [false, true, false, false];
      const expNegHiddens = [true, false, true, true];

      hostComponent.allowNegative = true;
      hostComponent.height = height;
      hostComponent.max = max;
      hostComponent.values = values;
      fixture.detectChanges();
      const component = fixture.componentInstance.component;

      const pos: HTMLElement[] = Array.from(fixture.nativeElement.querySelector('.positives').querySelectorAll('div'));
      const neg: HTMLElement[] = Array.from(fixture.nativeElement.querySelector('.negatives').querySelectorAll('div'));
      expect(neg.map(t => t.getBoundingClientRect().height)).toEqual(expDivHeights);
      expect(pos.map(t => t.getBoundingClientRect().height)).toEqual(expDivHeights);
      expect(neg.map(t => t.classList.contains('hidden'))).toEqual(expNegHiddens);
      expect(pos.map(t => t.classList.contains('hidden'))).toEqual(expPosHiddens);
    });
  });

  describe('with labels', () => {
    let hostComponent: TestHostComponentWithLabels;
    let fixture: ComponentFixture<TestHostComponentWithLabels>;

    beforeEach(() => {
      fixture = TestBed.createComponent(TestHostComponentWithLabels);
      hostComponent = fixture.componentInstance;
    });

    it('should show labels with no style', () => {
      hostComponent.allowNegative = true;
      hostComponent.height = 100;
      hostComponent.max = 1.0;
      hostComponent.values = [1.0, 0.5, 0.0];
      hostComponent.labels = ['first', 'second', 'third'];
      hostComponent.labelStyle = 'none';
      fixture.detectChanges();
      const component = fixture.componentInstance.component;

      const expChips = [false, false, false];
      const expLabels = ['first', 'second', 'third'];

      const labels: HTMLElement[] = Array.from(fixture.nativeElement.querySelectorAll('rx-graph-label'));
      expect(labels.map(t => t.classList.contains('graph-label-chip'))).toEqual(expChips);
      expect(labels.map(t => t.innerText)).toEqual(expLabels);
    });

    it('should show labels with the chip style', () => {
      hostComponent.allowNegative = true;
      hostComponent.height = 100;
      hostComponent.max = 1.0;
      hostComponent.values = [1.0, 0.5, 0.0];
      hostComponent.labels = ['first', 'second', 'third'];
      hostComponent.labelStyle = 'chip';
      fixture.detectChanges();
      const component = fixture.componentInstance.component;

      const expChips = [true, true, true];
      const expLabels = ['first', 'second', 'third'];

      const labels: HTMLElement[] = Array.from(fixture.nativeElement.querySelectorAll('rx-graph-label'));
      expect(labels.map(t => t.classList.contains('graph-label-chip'))).toEqual(expChips);
      expect(labels.map(t => t.innerText)).toEqual(expLabels);
    });

  });
});


@Component({
  template: `
    <rx-graph [style.height.px]="height" [max]="max" [values]="values" [allowNegative]="allowNegative"><rx-graph>
  `
})
class TestHostComponent {
  allowNegative: boolean;
  height: number;
  max: number;
  values: number[];
  @ViewChild(GraphComponent) component: GraphComponent;
}

@Component({
  template: `
    <rx-graph [style.height.px]="height" [max]="max" [values]="values" [allowNegative]="allowNegative">
      <rx-graph-label *ngFor="let label of labels" [style]="labelStyle">
        {{label}}
      </rx-graph-label>
    <rx-graph>
  `
})
class TestHostComponentWithLabels {
  allowNegative: boolean;
  height: number;
  labels: string[];
  labelStyle: 'chip' | 'none';
  max: number;
  values: number[];
  @ViewChild(GraphComponent) component: GraphComponent;
}
