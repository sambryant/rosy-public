import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation, } from '@angular/core';

@Component({
  selector: 'rx-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphComponent {

  @Input() allowNegative: boolean;

  @Input() max: number;

  @Input()
  get values() { return this._values; }
  set values(values: number[]) {
    this._values = values;
    this.signs = this.values.map((v) => {
      return v > 0.0;
    });
    this.heights = this.values.map((v, i) => {
      return 100 * ((Math.abs(v) / this.max));
    });
  }

  heights: number[];

  signs: boolean[];

  protected _values: number[];

  constructor() { }

}


@Component({
  selector: 'rx-graph-label',
  template: '<ng-content></ng-content>',
  styleUrls: ['./graph.component.scss'],
  host: {
    '[class.graph-label-chip]': `style === 'chip'`,
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphLabelComponent {

  @Input() style: 'none' | 'chip' = 'none';

  constructor() { }

}
