import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatTooltipModule} from '@angular/material/tooltip';

import {GraphComponent, GraphLabelComponent, } from './graph.component';


@NgModule({
  declarations: [
    GraphComponent,
    GraphLabelComponent,
  ],
  exports: [
    GraphComponent,
    GraphLabelComponent,
  ],
  imports: [
    CommonModule,
    MatTooltipModule,
  ]
})
export class GraphModule { }
