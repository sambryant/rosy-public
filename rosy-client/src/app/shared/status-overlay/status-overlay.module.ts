import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import {StatusOverlayComponent} from './status-overlay.component';


@NgModule({
  declarations: [
    StatusOverlayComponent,
  ],
  exports: [
    StatusOverlayComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatProgressSpinnerModule,
  ]
})
export class StatusOverlayModule { }
