import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

import {Status} from './status.model';


@Component({
  host: {
    '[class.transparent]': "status === 'loading'",
    '[class.rx-hidden]': "status === 'normal'",
  },
  selector: 'rx-status-overlay',
  templateUrl: './status-overlay.component.html',
  styleUrls: ['./status-overlay.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusOverlayComponent {

  @Input() status!: Status;

  @Input() message: string | null = null;

}
