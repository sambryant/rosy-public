import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatMenuModule} from '@angular/material/menu';
import {NgModule} from '@angular/core';

import {ActionButtonModule} from '@app/shared/action-button';
import {DialogModule} from '@app/shared/dialog';
import {EditableModule} from '@app/shared/editable';
import {ServicesModule} from '@app/services';

import {ContextMenuComponent} from './context-menu.component';


@NgModule({
  declarations: [
    ContextMenuComponent,
  ],
  exports: [
    ContextMenuComponent,
  ],
  imports: [
    ActionButtonModule,
    BrowserAnimationsModule,
    CommonModule,
    DialogModule,
    EditableModule,
    MatButtonModule,
    MatDialogModule,
    MatDividerModule,
    MatMenuModule,
  ]
})
export class ContextMenuModule { }
