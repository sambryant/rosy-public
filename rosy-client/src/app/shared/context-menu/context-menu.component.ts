import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {filter, take, takeUntil, } from 'rxjs/operators';
import {fromEvent, Subscription, Subject, } from 'rxjs';
import {MatMenu, MatMenuTrigger} from '@angular/material/menu';

import {UnsubscribeOnDestroyedDirective} from '@app/shared/utility';


@Component({
  selector: 'rx-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss']
})
export class ContextMenuComponent
extends UnsubscribeOnDestroyedDirective
implements OnInit {

  @Output() closed = new EventEmitter();

  @Output() opened = new EventEmitter();

  contextMenuPosition = { x: '0px', y: '0px' };

  @ContentChild(MatMenu, {static: true}) contextMenu: MatMenu;

  @ViewChild(MatMenuTrigger, {static: true}) contextMenuTrigger: MatMenuTrigger;

  constructor()
  {
    super();
  }

  ngOnInit() {
    this.contextMenuTrigger.menuOpened
      .pipe(takeUntil(this.destroyed$))
      .subscribe(this.opened);
    this.contextMenuTrigger.menuClosed
      .pipe(takeUntil(this.destroyed$))
      .subscribe(this.closed);
  }

  open(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenuTrigger.menu.focusFirstItem('mouse');
    this.contextMenuTrigger.openMenu();
  }

  close() {
    this.contextMenuTrigger.closeMenu();
  }
}
