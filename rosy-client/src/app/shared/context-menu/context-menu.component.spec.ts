import {take} from 'rxjs/operators';
import {Component, ViewChild, } from '@angular/core';
import {ComponentFixture, TestBed, } from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatMenuModule} from '@angular/material/menu';

import {ContextMenuComponent} from '.';


describe('ContextMenuComponent', () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let component: ContextMenuComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ContextMenuComponent,
        TestHostComponent,
      ],
      imports: [
        MatMenuModule,
        NoopAnimationsModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    fixture.detectChanges();
    component = fixture.componentInstance.component;
  });

  it('should open', () => {
    const sub1 = component.contextMenuTrigger.menuOpened.pipe(take(1)).subscribe();
    const sub2 = component.opened.pipe(take(1)).subscribe();
    component.open(new MouseEvent('click'));
    expect(sub1.closed).toBeTrue();
    expect(sub2.closed).toBeTrue();
  });

  it('should close', () => {
    const sub1 = component.contextMenuTrigger.menuClosed.pipe(take(1)).subscribe();
    const sub2 = component.closed.pipe(take(1)).subscribe();
    component.open(new MouseEvent('click'));
    expect(sub1.closed).toBeFalse();
    expect(sub2.closed).toBeFalse();
    component.close();
    expect(sub1.closed).toBeTrue();
    expect(sub2.closed).toBeTrue();
  });
});

@Component({
  template: `
    <rx-context-menu>
      <mat-menu>
        <div mat-menu-item>Hi</div>
        <div mat-menu-item>Bye</div>
      </mat-menu>
    </rx-context-menu>
  `
})
class TestHostComponent {
  @ViewChild(ContextMenuComponent) component: ContextMenuComponent;
}
