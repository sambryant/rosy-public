import {
  A, B, D, E, F, G, H, I, S, Y, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, SLASH, BACKSPACE, TILDE
} from '@angular/cdk/keycodes';
import {BehaviorSubject} from 'rxjs';

import {ContentTypeMap} from './content.model';
import {DupeTypeMap} from './dupe.model';
import {KeyBinding, KeyBindingShortForm, } from './key-binding.model';

export type Category = 'General' | 'View' | 'Edit' | 'Quick actions' | 'Selected' | 'Admin';

/**
 * Order here determines order in the action dialog. If you change this, check that action dialog
 * still looks okay.
 */
export const CATEGORIES: Category[] = ['General', 'View', 'Edit', 'Selected', 'Quick actions'];

export const enum Action {
  AddFeature,
  AddName,
  AddTag,
  AddArtist,
  AddGroup,
  AddArtistToSelected,
  AddGroupToSelected,
  AddNameToSelected,
  AddTagToSelected,
  AdminComputeMetadata,
  AdminFixSequences,
  AdminGenerateDupeReport,
  AdminGenerateDataset,
  AdminGenerateFeatureDataset,
  AdminGenerateTagDataset,
  AdminMergeDuplicates,
  AdminRecomputeFilesizes,
  AdminReindexImages,
  AdminRegenerateThumbnails,
  AdminResetFiles,
  CreateNewArtist,
  CreateNewGroup,
  CreateNewName,
  CreateNewTag,
  DeleteSelected,
  RemoveFeature,
  RemoveArtistFromSelected,
  RemoveGroupFromSelected,
  RemoveNameFromSelected,
  RemoveTagFromSelected,
  LogIn,
  LogOut,
  MarkDupesExactFromSelected,
  MarkDupesMostFromSelected,
  MarkDupesPartialFromSelected,
  MarkDupesNoneFromSelected,
  UnmarkDupesFromSelected,
  NavBackward,
  NavForward,
  PermadeleteSelected,
  QuickAction1,
  QuickAction2,
  QuickAction3,
  QuickAction4,
  QuickAction5,
  QuickAction6,
  QuickAction7,
  Rate1,
  Rate2,
  Rate3,
  Rate4,
  Rate5,
  RestoreSelected,
  SetDatabaseSchema,
  ShowQuickActions,
  ShowKeyBindings,
  ShowMarkedDupes,
  ShowFaceMatches,
  ToggleFavorite,
  ToggleHidden,
  ToggleProcessed,
  ToggleMarked,
}

export const MARK_DUPES_FROM_SELECTION_ACTIONS: DupeTypeMap<Action> = {
  'Exact': Action.MarkDupesExactFromSelected,
  'Most': Action.MarkDupesMostFromSelected,
  'Partial': Action.MarkDupesPartialFromSelected,
  'None': Action.MarkDupesNoneFromSelected,
};

export const QUICK_ACTION_ACTIONS: Action[] = [
  Action.QuickAction1, Action.QuickAction2, Action.QuickAction3, Action.QuickAction4, Action.QuickAction5, Action.QuickAction6, Action.QuickAction7
];

export const RATE_ACTIONS: Action[] = [
  Action.Rate1, Action.Rate2, Action.Rate3, Action.Rate4, Action.Rate5
];

const FORBID_BIND: KeyBindingShortForm = [];

export class ActionData {
  category: Category;
  description: string;
  allowBinding: boolean;
  defaultBinding: KeyBinding | null;

  constructor(category: Category, description: string, defaultBinding: KeyBindingShortForm | null = null) {
    this.category = category;
    this.description = description;
    this.allowBinding = true;
    this.defaultBinding = null;
    if (defaultBinding !== null) {
      if (defaultBinding === FORBID_BIND) {
        this.allowBinding = false;
      } else {
        this.defaultBinding = KeyBinding.fromShortForm(defaultBinding);
      }
    }
  }

  withDefaultBinding(binding: KeyBinding): ActionData {
    this.defaultBinding = binding;
    return this;
  }
}

function ad(category: Category, description: string, defaultBinding: KeyBindingShortForm | null = null): ActionData {
  return new ActionData(category, description, defaultBinding);
}

export const ACTION_DATA: Map<Action, ActionData> = new Map([
  [Action.ShowKeyBindings, ad('General', 'Show key bindings', [SLASH, 'shift', 'ctrl'])],
  [Action.SetDatabaseSchema, ad('General', 'Set database schema')],
  [Action.LogIn, ad('General', 'Log in to application', FORBID_BIND)],
  [Action.LogOut, ad('General', 'Log out of application', FORBID_BIND)],
  [Action.NavBackward, ad('View', 'Navigate backward', [D, 'ctrl'])],
  [Action.NavForward, ad('View', 'Navigate forward', [F, 'ctrl'])],
  [Action.ShowFaceMatches, ad('View', 'Show possible face matches', [E, 'ctrl'])],
  [Action.ShowMarkedDupes, ad('View', 'Show marked duplicates')],
  [Action.AddFeature, ad('Edit', 'Add feature')],
  [Action.AddName, ad('Edit', 'Add name', [S, 'ctrl'])],
  [Action.AddTag, ad('Edit', 'Add tag', [A, 'ctrl'])],
  [Action.AddArtist, ad('Edit', 'Set artist', [Y, 'ctrl'])],
  [Action.AddGroup, ad('Edit', 'Set group')],
  [Action.RemoveFeature, ad('Edit', 'Remove feature...', [TILDE, 'ctrl'])],
  [Action.ToggleFavorite, ad('Edit', 'Toggle favorite', [B, 'ctrl'])],
  [Action.ToggleHidden, ad('Edit', 'Toggle hidden', [H, 'ctrl'])],
  [Action.ToggleProcessed, ad('Edit', 'Toggle processed', [G, 'ctrl'])],
  [Action.ToggleMarked, ad('Edit', 'Toggle marked')],
  [Action.Rate1, ad('Edit', 'Rate 1/5')
      .withDefaultBinding(new KeyBinding(ONE, {enableDuringInput: false}))],
  [Action.Rate2, ad('Edit', 'Rate 2/5')
      .withDefaultBinding(new KeyBinding(TWO, {enableDuringInput: false}))],
  [Action.Rate3, ad('Edit', 'Rate 3/5')
      .withDefaultBinding(new KeyBinding(THREE, {enableDuringInput: false}))],
  [Action.Rate4, ad('Edit', 'Rate 4/5')
      .withDefaultBinding(new KeyBinding(FOUR, {enableDuringInput: false}))],
  [Action.Rate5, ad('Edit', 'Rate 5/5')
      .withDefaultBinding(new KeyBinding(FIVE, {enableDuringInput: false}))],
  [Action.ShowQuickActions, ad('Quick actions', 'Show quick actions', [I, 'ctrl'])],
  [Action.QuickAction1, ad('Quick actions', 'Quick action 1', [ONE, 'ctrl'])],
  [Action.QuickAction2, ad('Quick actions', 'Quick action 2', [TWO, 'ctrl'])],
  [Action.QuickAction3, ad('Quick actions', 'Quick action 3', [THREE, 'ctrl'])],
  [Action.QuickAction4, ad('Quick actions', 'Quick action 4', [FOUR, 'ctrl'])],
  [Action.QuickAction5, ad('Quick actions', 'Quick action 5', [FIVE, 'ctrl'])],
  [Action.QuickAction6, ad('Quick actions', 'Quick action 6', [SIX, 'ctrl'])],
  [Action.QuickAction7, ad('Quick actions', 'Quick action 7', [SEVEN, 'ctrl'])],
  [Action.CreateNewArtist, ad('Selected', 'Create artist')],
  [Action.CreateNewGroup, ad('Selected', 'Create group')],
  [Action.CreateNewName, ad('Selected', 'Create name')],
  [Action.CreateNewTag, ad('Selected', 'Create tag')],
  [Action.DeleteSelected, ad('Selected', 'Delete selected')],
  [Action.PermadeleteSelected, ad('Selected', 'Permadelete selected')],
  [Action.RestoreSelected, ad('Selected', 'Restore selected')],
  [Action.AddArtistToSelected, ad('Selected', 'Set artist for selected', [Y, 'ctrl', 'shift'])],
  [Action.AddGroupToSelected, ad('Selected', 'Set group for selected')],
  [Action.AddNameToSelected, ad('Selected', 'Add name to selected', [S, 'ctrl', 'shift'])],
  [Action.AddTagToSelected, ad('Selected', 'Add tag to selected', [A, 'ctrl', 'shift'])],
  [Action.RemoveArtistFromSelected, ad('Selected', 'Remove artist from selected')],
  [Action.RemoveGroupFromSelected, ad('Selected', 'Remove group from selected')],
  [Action.RemoveNameFromSelected, ad('Selected', 'Remove name from selected', [S, 'ctrl', 'shift', 'alt'])],
  [Action.RemoveTagFromSelected, ad('Selected', 'Remove tag from selected', [A, 'ctrl', 'shift', 'alt'])],
  [Action.MarkDupesExactFromSelected, ad('Selected', 'Mark selected as exact duplicates')],
  [Action.MarkDupesMostFromSelected, ad('Selected', 'Mark selected as mostly duplicates')],
  [Action.MarkDupesPartialFromSelected, ad('Selected', 'Mark selected as partial duplicates')],
  [Action.MarkDupesNoneFromSelected, ad('Selected', 'Mark selected as not duplicates')],
  [Action.UnmarkDupesFromSelected, ad('Selected', 'Unmark selected as duplicates')],
  [Action.AdminComputeMetadata, ad('Admin', 'Compute metadata', FORBID_BIND)],
  [Action.AdminFixSequences, ad('Admin', 'Fix sequences', FORBID_BIND)],
  [Action.AdminGenerateDupeReport, ad('Admin', 'Generate dupe report', FORBID_BIND)],
  [Action.AdminGenerateDataset, ad('Admin', 'Generate full dataset', FORBID_BIND)],
  [Action.AdminGenerateFeatureDataset, ad('Admin', 'Generate feature dataset', FORBID_BIND)],
  [Action.AdminGenerateTagDataset, ad('Admin', 'Generate tag dataset', FORBID_BIND)],
  [Action.AdminMergeDuplicates, ad('Admin', 'Merge duplicates', FORBID_BIND)],
  [Action.AdminRecomputeFilesizes, ad('Admin', 'Recompute file sizes', FORBID_BIND)],
  [Action.AdminRegenerateThumbnails, ad('Admin', 'Regenerate thumbnails', FORBID_BIND)],
  [Action.AdminReindexImages, ad('Admin', 'Reindex images', FORBID_BIND)],
  [Action.AdminResetFiles, ad('Admin', 'Reset files', FORBID_BIND)],
]);
