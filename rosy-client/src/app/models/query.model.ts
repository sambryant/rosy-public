import {Params} from '@angular/router';
import {Sort, SortDirection, } from '@angular/material/sort';
import {
  encodeRange,
  decodeDateRange,
  decodeFloatRange,
  decodeIntRange,
  Range,
} from './range.model';
import {
  ArtistId,
  ContentMeta,
  GroupId,
  Id,
  NameId,
  TagId,
} from './content.model';

import {Constructor, } from './constructor';

export const DEFAULT_PAGE_SIZE = 25;
export const DEFAULT_DUPE_CUTOFF_FACE = 0.2;

export const enum FlagFilter {
  ForceFalse,
  ForceTrue,
  All,
};

export class Query {
  constructor() {}
  refresh(): void {}
  fromParams(params: Params): void {}
  toParams(): Params {
    return {} as Params;
  }
}

export interface ContentQuery extends Query {
  createdTime: Range<Date>;
  fromParams(params: Params): void;
  toParams(): Params;
}

export interface PagedQuery extends Query {
  size: number;
  page: number;
  fromParams(params: Params): void;
  toParams(): Params;
}

export interface SortedQuery extends Query {
  sort: Sort;
  fromParams(params: Params): void;
  toParams(): Params;
  toggleRandom(): boolean;
}

export interface TaggedQuery extends Query {
  hidden: FlagFilter;
  names: NameId[];
  tags: TagId[];
  notNames: NameId[];
  notTags: TagId[];
  fromParams(params: Params): void;
  toParams(): Params;
}

interface ContentQueryParams {
  createdTime?: string;
}

interface PagedQueryParams {
  size: string;
  page: string;
}

interface SortedQueryParams {
  sortBy?: string;
  direction?: string;
}

interface TaggedQueryParams {
  hidden?: string;
  names?: string;
  tags?: string;
  notNames?: string;
  notTags?: string;
}

type ContentQueryCtor = Constructor<ContentQuery>;

type PagedQueryCtor = Constructor<PagedQuery>;

type SortedQueryCtor = Constructor<SortedQuery>;

type TaggedQueryCtor = Constructor<TaggedQuery>;

function mixinContentQuery<T extends Constructor<Query>>(base: T): ContentQueryCtor & T {

  return class extends base {

    createdTime: Range<Date>;

    constructor(...args: any[]) {
      super(...args);
      this.createdTime = { lower: null, upper: null };
    }

    override fromParams(params: Params) {
      super.fromParams(params);
      this.createdTime = decodeDateRange(params, 'createdTime');
    }

    override toParams(): Params {
      const params = super.toParams() as ContentQueryParams;
      encodeRange(params, 'createdTime', this.createdTime);
      return params;
    }
  };
}

function mixinSortedQuery<T extends Constructor<Query>>(base: T, defaultSortBy: string, defaultSortDir: SortDirection = 'asc'): SortedQueryCtor & T {

  return class extends base {

    sort: Sort;

    constructor(...args: any[]) {
      super(...args);
      this.sort = {
        active: defaultSortBy,
        direction: defaultSortDir,
      };
    }

    override fromParams(params: Params) {
      super.fromParams(params);
      this.sort.direction = 'asc';
      if (params.hasOwnProperty('sortBy')) {
        this.sort.active = params['sortBy'];
        if (params['direction'] === 'desc') {
          this.sort.direction = 'desc';
        }
      } else {
        this.sort.active = defaultSortBy;
      }
    }

    override toParams(): Params {
      const params = super.toParams() as SortedQueryParams;
      if (this.sort.active) {
        params.sortBy = this.sort.active;
        if (this.sort.direction) {
          params.direction = this.sort.direction.toString();
        } else {
          params.direction = 'asc';
        }
      }
      return params;
    }

    toggleRandom(): boolean {
      if (this.sort.active === 'random') {
        this.sort.active = '';
      } else {
        this.sort.active = 'random';
      }
      return true;
    }

  };
}

function mixinPagedQuery<T extends Constructor<Query>>(base: T, defaultPageSize: number): PagedQueryCtor & T {

  return class extends base {

    size: number;

    page: number;

    constructor(...args: any[]) {
      super(...args);
      this.size = defaultPageSize;
      this.page = 0;
    }

    override refresh() {
      super.refresh();
      this.page = 0;
    }

    override fromParams(params: Params) {
      super.fromParams(params);
      this.page = 0;
      const size = decodeInt(params, 'size', 1);
      if (size !== null) {
        this.size = size;
        const page = decodeInt(params, 'page', 0);
        if (page !== null) {
          this.page = page;
        }
      } else {
        this.size = defaultPageSize;
      }
    }

    override toParams(): Params {
      const params = super.toParams() as PagedQueryParams;
      params.page = this.page.toString();
      params.size = this.size.toString();
      return params;
    }

  };
}

function mixinTaggedQuery<T extends Constructor<Query>>(base: T): TaggedQueryCtor & T {

  return class extends base {

    hidden: FlagFilter;

    names: NameId[];

    tags: TagId[];

    notNames: NameId[];

    notTags: TagId[];

    constructor(...args: any[]) {
      super(...args);
      this.hidden = FlagFilter.All;
      this.names = [];
      this.tags = [];
      this.notNames = [];
      this.notTags = [];
    }

    override fromParams(params: Params) {
      super.fromParams(params);
      this.hidden = decodeFlagFilter(params, 'hidden');
      this.names = decodeInts(params, 'names');
      this.tags = decodeInts(params, 'tags');
      this.notNames = decodeInts(params, 'notNames');
      this.notTags = decodeInts(params, 'notTags');
    }

    override toParams(): Params {
      const params = super.toParams() as TaggedQueryParams;
      encodeFlagFilter(params, 'hidden', this.hidden);
      if (this.tags.length > 0) {
        params.tags = this.tags.join(',');
      }
      if (this.names.length > 0) {
        params.names = this.names.join(',');
      }
      if (this.notTags.length > 0) {
        params.notTags = this.notTags.join(',');
      }
      if (this.notNames.length > 0) {
        params.notNames = this.notNames.join(',');
      }
      return params;
    }
  };
}


const _ArtistQueryBase = mixinContentQuery(mixinSortedQuery(Query, 'value'));
interface ArtistQueryParams extends ContentQueryParams, SortedQueryParams {}
export class ArtistQuery extends _ArtistQueryBase {}

const _DeletedQueryBase = mixinSortedQuery(mixinPagedQuery(Query, DEFAULT_PAGE_SIZE), 'id');
interface DeletedQueryParams extends PagedQueryParams, SortedQueryParams {}
export class DeletedQuery extends _DeletedQueryBase {}

const _DupeCandidatesQueryBase = mixinPagedQuery(Query, DEFAULT_PAGE_SIZE);
interface DupeCandidatesQueryParams extends PagedQueryParams {
  marked?: string;
  method?: string;
  cutoff?: string;
}
export class DupeCandidatesQuery extends _DupeCandidatesQueryBase {
  marked: FlagFilter;
  method: string | null;
  cutoff: number | null;

  constructor(size: number = DEFAULT_PAGE_SIZE, page: number = 0) {
    super();
    this.page = page;
    this.size = size;
    this.method = null;
    this.cutoff = DEFAULT_DUPE_CUTOFF_FACE;
    this.marked = FlagFilter.All;
  }

  override fromParams(params: Params) {
    super.fromParams(params);
    this.cutoff = decodeFloat(params, 'cutoff', DEFAULT_DUPE_CUTOFF_FACE);
    if (params.hasOwnProperty('method')) {
      this.method = params['method'];
    }
    this.marked = decodeFlagFilter(params, 'marked');
  }

  override toParams(): Params {
    const params = super.toParams() as DupeCandidatesQueryParams;
    if (this.method !== null) {
      params.method = this.method;
    }
    if (this.cutoff !== null) {
      params.cutoff = `${this.cutoff}`;
    }
    encodeFlagFilter(params, 'marked', this.marked);
    return params;
  }

}


const _GroupQueryBase = mixinContentQuery(mixinTaggedQuery(mixinSortedQuery(mixinPagedQuery(Query, DEFAULT_PAGE_SIZE), 'random')));
interface GroupQueryParams extends ContentQueryParams, PagedQueryParams, SortedQueryParams, TaggedQueryParams {}
export class GroupQuery extends _GroupQueryBase {

  constructor(size: number = DEFAULT_PAGE_SIZE, page: number = 0) {
    super();
    this.size = size;
    this.page = page;
  }

}

const _ImageQueryBase = mixinContentQuery(mixinTaggedQuery(mixinSortedQuery(mixinPagedQuery(Query, DEFAULT_PAGE_SIZE), 'random')));

interface ImageQueryParams extends ContentQueryParams, PagedQueryParams, SortedQueryParams, TaggedQueryParams {
  artists?: string;
  features?: string;
  groups?: string;
  notFeatures?: string;
  processed?: string;
  hasArtist?: string;
  grouped?: string;
  favorite?: string;
  isVideo?: string;
  marked?: string;
  rating?: string;
  ratingCount?: string;
  processedTime?: string;
}

export class ImageQuery extends _ImageQueryBase {
  artists: ArtistId[];
  features: Id[];
  groups: GroupId[];
  notFeatures: Id[];
  processed: FlagFilter;
  hasArtist: FlagFilter;
  grouped: FlagFilter;
  isVideo: FlagFilter;
  marked: FlagFilter;
  favorite: FlagFilter;
  rating: Range<number>;
  ratingCount: Range<number>;
  processedTime: Range<Date>;

  constructor(size: number = DEFAULT_PAGE_SIZE, page: number = 0) {
    super();
    this.size = size;
    this.page = page;
    this.artists = [];
    this.features = [];
    this.groups = [];
    this.notFeatures = [];
    this.processed = FlagFilter.All;
    this.hasArtist = FlagFilter.All;
    this.grouped = FlagFilter.All;
    this.isVideo = FlagFilter.All;
    this.marked = FlagFilter.All;
    this.favorite = FlagFilter.All;
    this.processedTime = { lower: null, upper: null };
    this.rating = { lower: null, upper: null };
    this.ratingCount = { lower: null, upper: null };
  }

  override fromParams(params: Params) {
    super.fromParams(params);
    this.favorite = decodeFlagFilter(params, 'favorite');
    this.hasArtist = decodeFlagFilter(params, 'hasArtist');
    this.grouped = decodeFlagFilter(params, 'grouped');
    this.isVideo = decodeFlagFilter(params, 'isVideo');
    this.marked = decodeFlagFilter(params, 'marked');
    this.processed = decodeFlagFilter(params, 'processed');

    this.artists = decodeInts(params, 'artists');
    this.features = decodeInts(params, 'features');
    this.groups = decodeInts(params, 'groups');
    this.notFeatures = decodeInts(params, 'notFeatures');

    this.rating = decodeFloatRange(params, 'rating');
    this.ratingCount = decodeIntRange(params, 'ratingCount');
    this.processedTime = decodeDateRange(params, 'processedTime');
  }

  override toParams(): Params {
    const params = super.toParams() as ImageQueryParams;
    encodeFlagFilter(params, 'favorite', this.favorite);
    encodeFlagFilter(params, 'hasArtist', this.hasArtist);
    encodeFlagFilter(params, 'grouped', this.grouped);
    encodeFlagFilter(params, 'isVideo', this.isVideo);
    encodeFlagFilter(params, 'marked', this.marked);
    encodeFlagFilter(params, 'processed', this.processed);
    if (this.artists.length > 0) {
      params.artists = this.artists.join(',');
    }
    if (this.features.length > 0) {
      params.features = this.features.join(',');
    }
    if (this.groups.length > 0) {
      params.groups = this.groups.join(',');
    }
    if (this.notFeatures.length > 0) {
      params.notFeatures = this.notFeatures.join(',');
    }
    encodeRange(params, 'processedTime', this.processedTime);
    encodeRange(params, 'rating', this.rating);
    encodeRange(params, 'ratingCount', this.ratingCount);
    return params;
  }

  static getParamsFor(filter: ContentMeta): Partial<ImageQueryParams> {
    if (filter.type === 'artist') {
      return { artists: filter.item.id.toString() };
    } else if (filter.type === 'feature') {
      return { features: filter.item.id.toString() };
    } else if (filter.type === 'group') {
      return { groups: filter.item.id.toString() };
    } else if (filter.type === 'name') {
      return { names: filter.item.id.toString() };
    } else if (filter.type === 'tag') {
      return { tags: filter.item.id.toString() };
    } else {
      throw new Error('Unreachable');
    }
  }
}

const _NameQueryBase = mixinContentQuery(mixinSortedQuery(Query, 'imageCount', 'desc'));
interface NameQueryParams extends ContentQueryParams, SortedQueryParams {}
export class NameQuery extends _NameQueryBase {}

const _TagQueryBase = mixinContentQuery(mixinSortedQuery(Query, 'imageCount', 'desc'));
interface TagQueryParams extends ContentQueryParams, SortedQueryParams {}
export class TagQuery extends _TagQueryBase {}

function encodeFlagFilter(params: Params, key: string, value: FlagFilter) {
  if (value !== FlagFilter.All) {
    params[key] = value === FlagFilter.ForceFalse ? 'false' : 'true';
  }
}

function decodeFlagFilter(params: Params, name: string): FlagFilter {
  if (params.hasOwnProperty(name)) {
    const value = params[name];
    if (value === 'true') {
      return FlagFilter.ForceTrue;
    } else if (value === 'false') {
      return FlagFilter.ForceFalse;
    }
  }
  return FlagFilter.All;
}

function decodeFloat(params: Params, name: string, defaultValue: number | null = null): number | null {
  if (params.hasOwnProperty(name)) {
    const value = parseFloat(params[name]);
    if (value === value) {
      return value;
    }
  }
  return defaultValue;
}

function decodeInt(params: Params, name: string, minValue: number | null): number | null {
  if (params.hasOwnProperty(name)) {
    const value = parseInt(params[name], 10);
    if (value === value) {
      if (minValue === null || value >= minValue) {
        return value;
      }
    }
  }
  return null;
}

function decodeInts(params: Params, name: string): number[] {
  if (params.hasOwnProperty(name) && params[name]) {
    return params[name].split(',')
      .map(intStr => parseInt(intStr, 10))
      .filter(intVal => intVal === intVal);
  } else {
    return [];
  }
}

interface EigendataQueryParams {
  numVecs?: string;
  numWeights?: string;
}

export class EigendataQuery extends Query {
  numVecs: number;
  numWeights: number;

  constructor() {
    super();
    this.clear();
  }

  protected clear() {
    this.numVecs = 15;
    this.numWeights = 15;
  }

  override fromParams(params: Params) {
    this.clear();

    const numVecs = decodeInt(params, 'numVecs', 1);
    if (numVecs !== null) {
      this.numVecs = numVecs;
    }
    const numWeights = decodeInt(params, 'numWeights', 1);
    if (numWeights !== null) {
      this.numWeights = numWeights;
    }
  }

  override toParams(): Params {
    const params: EigendataQueryParams = {};
    params.numVecs = this.numVecs.toString();
    params.numWeights = this.numWeights.toString();
    return params;
  }

}

export function QueryFromParams<T extends Query>(QueryClass: new () => T, params: Params): T {
  const query = new QueryClass();
  query.fromParams(params);
  return query;
}

export const DUPE_CANDIDATES_DEFAULT_PARAMS: Params = (function() {
  const q = new DupeCandidatesQuery();
  q.marked = FlagFilter.ForceFalse;
  return q.toParams();
})();

export const GROUP_DEFAULT_PARAMS: Params = (function() {
  const q = new GroupQuery();
  q.hidden = FlagFilter.ForceFalse;
  return q.toParams();
})();

export const IMAGE_DEFAULT_PARAMS: Params = (function() {
  const q = new ImageQuery();
  q.hidden = FlagFilter.ForceFalse;
  q.sort.active = 'random';
  return q.toParams();
})();

export const IMAGE_INBOX_PARAMS: Params = (function() {
  const q = new ImageQuery();
  q.sort.active = 'id';
  q.hidden = FlagFilter.ForceFalse;
  q.processed = FlagFilter.ForceFalse;
  q.isVideo = FlagFilter.ForceFalse;
  return q.toParams();
})();
