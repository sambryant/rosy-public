import {Image} from './content.model';

export interface FaceMatch {
  item: Image;
  score: number;
};
