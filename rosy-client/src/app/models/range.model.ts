import {Params} from '@angular/router';

export interface Range<T> {
  lower: T | null;
  upper: T | null;
}

function numberRangeToDateRange(range: Range<number>): Range<Date> {
  return {
    lower: range.lower !== null ? new Date(range.lower) : null,
    upper: range.upper !== null ? new Date(range.upper) : null,
  };
}

export function encodeRange<T extends Object>(params: Params, key: string, range: Range<T>) {
  if (range.lower !== null && range.upper !== null) {
    params[key] = `>${range.lower.valueOf()},<${range.upper.valueOf()}`;
  } else if (range.lower !== null) {
    params[key] = `>${range.lower.valueOf()}`;
  } else if (range.upper !== null) {
    params[key] = `<${range.upper.valueOf()}`;
  }
}

export function decodeFloatRange(params: Params, key: string): Range<number> {
  const range: Range<number> = {lower: null, upper: null};
  if (params.hasOwnProperty(key) && params[key]) {
    const encoded = params[key];
    encoded.split(',')
      .filter(part => part.length > 1)
      .forEach(part => {
        const val = parseFloat(part.substring(1));
        if (val === val && val !== undefined) {
          // TODO: Consider choosing the narrowest range when multiple conflicting filters given
          if (part[0] === '<') {
            range.upper = val;
          } else if (part[0] === '>') {
            range.lower = val;
          }
        }
      });
  }
  return range;
}

export function decodeIntRange(params: Params, key: string): Range<number> {
  const range: Range<number> = {lower: null, upper: null};
  if (params.hasOwnProperty(key) && params[key]) {
    const encoded = params[key];
    encoded.split(',')
      .filter(part => part.length > 1)
      .forEach(part => {
        const val = parseInt(part.substring(1), 10);
        if (val === val && val !== undefined) {
          // TODO: Consider choosing the narrowest range when multiple conflicting filters given
          if (part[0] === '<') {
            range.upper = val;
          } else if (part[0] === '>') {
            range.lower = val;
          }
        }
      });
  }
  return range;
}

export function decodeDateRange(params: Params, key: string): Range<Date> {
  return numberRangeToDateRange(decodeFloatRange(params, key));
}
