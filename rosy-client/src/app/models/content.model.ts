import {DupeType} from './dupe.model';
import {Point} from './point.model';

export type Id = number;
export type ArtistId = Id;
export type GroupId = Id;
export type ImageId = Id;
export type NameId = Id;
export type TagId = Id;

export interface Content {
  id: Id;
}

export type ContentType = 'artist' | 'feature' | 'group' | 'name'| 'tag';

export type ContentTypeFull = ContentType | 'image';

export type ContentValue = Artist | Feature | GroupSimple | Name | Tag;

export interface ContentTypeMap<T> {
  artist: T,
  feature: T,
  group: T,
  name: T,
  tag: T
};

export interface ContentEigendata {
  eigenvalue: number,
  weights: number[],
  items: {type: ContentType, id: Id, value: string}[],
};

export type ContentMeta = {type: 'artist'} & {item: Artist} |  {type: 'feature'} & {item: Feature} |
  {type: 'group'} & {item: GroupSimple} | {type: 'name'} & {item: Name} | {type: 'tag'} & {item: Tag};

export const CONTENT_TYPES: ContentType[] = ['artist', 'feature', 'group', 'name', 'tag'];

export interface Artist extends Content {
  // From `Content`:
  // id: ArtistId;
  createdTime: number;
  value: string;
}

export interface ArtistDetails extends Artist {
  // From `Artist`:
  // id: ArtistId;
  // createdTime: number;
  // value: string;
  imageCount: number;
  score: number;
  metadata?: ContentMetadata; // Note: this is always going to be undefined.
}

interface ContentMetadata {
  evecCoefs: number[];
  names: ContentRelationships;
  tags: ContentRelationships;
}

interface ContentRelationships {
  related: [Id, number][];
  unrelated: [Id, number][];
}

export interface Feature extends Content {
  createdTime: number;
  value: string;
}

export interface GroupSimple extends Content {
  // From `Content`:
  // id: GroupId;
  value: string;
  description: string;
  hidden: boolean;
}

export interface Group extends GroupSimple, TaggedContent {
  // From `TaggedContent`:
  // id: GroupId;
  // names: Name[];
  // tags: Tag[];

  // From `GroupSimple`:
  // title: string;
  // description: string;
  // hidden: boolean;

  images: Image[];
}

export interface Image extends TaggedContent {
  // From `TaggedContent`:
  // id: ImageId;
  // names: Name[];
  // tags: Tag[];
  dupes: {[key: number]: DupeType};
  filename: string;
  thumbnail: string;
  createdTime: number;
  filesize: number;
  hidden: boolean;
  favorite: boolean;
  isVideo: boolean;
  length: number | null;
  marked: boolean;
  processed: boolean;
  processedTime: number | null;
  artistId: ArtistId | null;
  groupId: GroupId  | null;
  ratingAverage: number | null;
  ratingCount: number;
  features: ImageFeature[];
}

export interface ImageFeature extends Content {
  imageId: Id;
  featureId: Id;
  value: string;
  x1: number;
  x2: number;
  y1: number;
  y2: number;
}

export interface Name extends Content {
  // From `Content`:
  // id: GroupId;
  createdTime: number;
  index: number | null;
  value: string;
}

export interface NameDetails extends Name {
  // From `Name`:
  // id: GroupId;
  // createdTime: number;
  // index: number | null;
  // value: string;
  groupCount: number;
  imageCount: number;
  score: number;
  metadata: ContentMetadata | null;
}

export interface Tag extends Content {
  // From `Content`:
  // id: TagId;
  createdTime: number;
  index: number | null;
  value: string;
}

export interface TagDetails extends Tag {
  // From `Tag`:
  // id: TagId;
  // createdTime: number;
  // index: number | null;
  // value: string;
  groupCount: number;
  imageCount: number;
  score: number;
  metadata: ContentMetadata | null;
}

export interface TaggedContent extends Content {
  names: Name[];
  tags: Tag[];
}
