export type Direction = 'next' | 'prev';

export interface NavigationRequest {
  direction: Direction;
  extend?: boolean; // by default is interpreted to be false
}
