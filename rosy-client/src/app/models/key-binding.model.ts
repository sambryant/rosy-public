import {
  BACKSPACE, TAB, ENTER, SPACE, PAGE_UP, PAGE_DOWN,
  PLUS_SIGN, DELETE, ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE,
  FF_SEMICOLON, FF_EQUALS, QUESTION_MARK, AT_SIGN,
  A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
  FF_MINUS, SEMICOLON, EQUALS, COMMA, DASH, SLASH, APOSTROPHE, TILDE, OPEN_SQUARE_BRACKET,
  BACKSLASH, CLOSE_SQUARE_BRACKET, SINGLE_QUOTE,
} from '@angular/cdk/keycodes';

export type Key = number;

export type KeyBindingHash = number;

export interface KeyEventLike {
  keyCode: Key;
  altKey: boolean;
  ctrlKey: boolean;
  shiftKey: boolean;
  enableDuringInput?: boolean;
}

const DEFAULT_KEY_BINDING_OPTIONS = {
  altKey: false,
  ctrlKey: false,
  shiftKey: false,
  enableDuringInput: true,
};

const CTRL_MASK = 256;
const SHIFT_MASK = 512;
const ALT_MASK = 1024;

export type KeyBindingShortForm = (Key | 'ctrl' | 'alt' | 'shift')[];

export class KeyBinding implements KeyEventLike {
  altKey: boolean;
  ctrlKey: boolean;
  keyCode: Key;
  shiftKey: boolean;
  enableDuringInput: boolean;

  constructor(keyCode: Key, options: Partial<KeyEventLike>) {
    const merged = {...DEFAULT_KEY_BINDING_OPTIONS, ...options};
    this.keyCode = keyCode;
    this.ctrlKey = merged.ctrlKey;
    this.shiftKey = merged.shiftKey;
    this.altKey = merged.altKey;
    this.enableDuringInput = merged.enableDuringInput;
    if (!KeyBinding.isValid(this)) {
      throw new Error(`Invalid key binding: ${this.keyCode}`);
    }
  }

  static fromShortForm(b: KeyBindingShortForm): KeyBinding {
    if (b.length < 1) {
      throw new Error(`Invalid key binding short form: ${b}`);
    } else if (typeof b[0] !== 'number') {
      throw new Error(`Invalid key binding short form: ${b}`);
    } else {
      const keyCode: number = b[0];
      const opts: Partial<KeyEventLike> = {altKey: false, ctrlKey: false, shiftKey: false};
      for (let i=1; i<b.length; i++) {
        if (b[i] === 'alt') {
          opts.altKey = true;
        } else if (b[i] === 'ctrl') {
          opts.ctrlKey = true;
        } else if (b[i] === 'shift') {
          opts.shiftKey = true;
        } else {
          throw new Error(`Invalid key binding short form: ${b}`);
        }
      }
      return new KeyBinding(keyCode, opts);
    }
  }

  static isValid(ev: KeyEventLike): boolean {
    if (KEY_CODE_TO_LABEL[ev.keyCode] === null) {
      return false;
    } else {
      return !FORBIDDEN_BINDING_HASHES.has(KeyBinding.keyToHash(ev));
    }
  }

  static fromEvent(event: KeyEventLike) {
    return new KeyBinding(event.keyCode, {
      ctrlKey: event.ctrlKey,
      altKey: event.altKey,
      shiftKey: event.shiftKey,
    });
  }

  /**
   * Rapidly converts key event into unique numeric hash that can be used to lookup bindings.
   *
   * This method should have two design constraints:
   *   1. Injective: if ev1 !== ev2, then KeyBinding.keyToHash(ev1) !== KeyBinding.keyToHash(ev2)
   *   2. Speed: This is called every single time a key is pressed so it should be as lightweight as possible
   */
  static keyToHash(ev: KeyEventLike): KeyBindingHash {
    return (ev.ctrlKey ? CTRL_MASK : 0) + (ev.shiftKey ? SHIFT_MASK : 0) + (ev.altKey ? ALT_MASK : 0) + ev.keyCode;
  }

  toHash(): KeyBindingHash {
    return KeyBinding.keyToHash(this);
  }

  toLabel(): string {
    const label = KEY_CODE_TO_LABEL[this.keyCode]!;
    return (this.altKey ? `${ALT_LABEL} + ` : '') +
           (this.ctrlKey ? `${CTRL_LABEL} + ` : '') +
           (this.shiftKey ? label[1] : label[0]);
  }
}

const FORBIDDEN_BINDING_HASHES: Set<KeyBindingHash> = new Set([
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: N }, // ctrl+n = new window
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: Q }, // ctrl+q = quit
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: T }, // ctrl+t = new tab
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: W }, // ctrl+w = close tab
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: X }, // ctrl+x = cut
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: C }, // ctrl+c = copy
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: V }, // ctrl+v = paste
  { altKey: false, shiftKey: false, ctrlKey: true, keyCode: Z }, // ctrl+z = undo
].map((ev: KeyEventLike) => KeyBinding.keyToHash(ev)));

const CTRL_LABEL = 'Ctrl';
const ALT_LABEL = 'Alt';

const KEY_CODE_TO_LABEL: ([string, string] | null)[] = Array.from([...Array(256).keys()]).map(_ => null);
KEY_CODE_TO_LABEL[BACKSPACE] = ['⌫', '⇧ + ⌫'];
KEY_CODE_TO_LABEL[TAB] = ['⇥', '⇧ + ⇥'];
KEY_CODE_TO_LABEL[ENTER] = ['⏎', '⇧ + ⏎'];
KEY_CODE_TO_LABEL[SPACE] = ['␣', '⇧ + ␣'];
KEY_CODE_TO_LABEL[PAGE_UP] = ['⇞', '⇧ + ⇞'];
KEY_CODE_TO_LABEL[PAGE_DOWN] = ['⇟', '⇧ + ⇟'];
KEY_CODE_TO_LABEL[DELETE] = ['⌦', '⇧ + ⌦'];
KEY_CODE_TO_LABEL[SLASH] = ['/', '?'];
KEY_CODE_TO_LABEL[COMMA] = [',', '<'];
KEY_CODE_TO_LABEL[APOSTROPHE] = ['`', '~'];
KEY_CODE_TO_LABEL[SINGLE_QUOTE] = ['\'', '"'];
KEY_CODE_TO_LABEL[OPEN_SQUARE_BRACKET] = ['[', '{'];
KEY_CODE_TO_LABEL[CLOSE_SQUARE_BRACKET] = [']', '}'];
KEY_CODE_TO_LABEL[BACKSLASH] = ['\\', '|'];

KEY_CODE_TO_LABEL[FF_EQUALS] = ['=', '+'];
KEY_CODE_TO_LABEL[EQUALS] = ['=', '+'];
KEY_CODE_TO_LABEL[FF_SEMICOLON] = [';', ':'];
KEY_CODE_TO_LABEL[SEMICOLON] = [';', ':'];
KEY_CODE_TO_LABEL[FF_MINUS] = ['-', '_'];
KEY_CODE_TO_LABEL[DASH] = ['-', '_'];

KEY_CODE_TO_LABEL[ONE] = ['1', '!'];
KEY_CODE_TO_LABEL[TWO] = ['2', '@'];
KEY_CODE_TO_LABEL[THREE] = ['3', '#'];
KEY_CODE_TO_LABEL[FOUR] = ['4', '$'];
KEY_CODE_TO_LABEL[FIVE] = ['5', '%'];
KEY_CODE_TO_LABEL[SIX] = ['6', '^'];
KEY_CODE_TO_LABEL[SEVEN] = ['7', '&'];
KEY_CODE_TO_LABEL[EIGHT] = ['8', '*'];
KEY_CODE_TO_LABEL[NINE] = ['9', '('];
KEY_CODE_TO_LABEL[ZERO] = ['0', ')'];

KEY_CODE_TO_LABEL[A] = ['a', 'A'];
KEY_CODE_TO_LABEL[B] = ['b', 'B'];
KEY_CODE_TO_LABEL[C] = ['c', 'C'];
KEY_CODE_TO_LABEL[D] = ['d', 'D'];
KEY_CODE_TO_LABEL[E] = ['e', 'E'];
KEY_CODE_TO_LABEL[F] = ['f', 'F'];
KEY_CODE_TO_LABEL[G] = ['g', 'G'];
KEY_CODE_TO_LABEL[H] = ['h', 'H'];
KEY_CODE_TO_LABEL[I] = ['i', 'I'];
KEY_CODE_TO_LABEL[J] = ['j', 'J'];
KEY_CODE_TO_LABEL[K] = ['k', 'K'];
KEY_CODE_TO_LABEL[L] = ['l', 'L'];
KEY_CODE_TO_LABEL[M] = ['m', 'M'];
KEY_CODE_TO_LABEL[N] = ['n', 'N'];
KEY_CODE_TO_LABEL[O] = ['o', 'O'];
KEY_CODE_TO_LABEL[P] = ['p', 'P'];
KEY_CODE_TO_LABEL[Q] = ['q', 'Q'];
KEY_CODE_TO_LABEL[R] = ['r', 'R'];
KEY_CODE_TO_LABEL[S] = ['s', 'S'];
KEY_CODE_TO_LABEL[T] = ['t', 'T'];
KEY_CODE_TO_LABEL[U] = ['u', 'U'];
KEY_CODE_TO_LABEL[V] = ['v', 'V'];
KEY_CODE_TO_LABEL[W] = ['w', 'W'];
KEY_CODE_TO_LABEL[X] = ['x', 'X'];
KEY_CODE_TO_LABEL[Y] = ['y', 'Y'];
KEY_CODE_TO_LABEL[Z] = ['z', 'Z'];

// // Mystery key codes (not sure how to trigger)
// KEY_CODE_TO_LABEL[PLUS_SIGN] = ['???', '???'];
// KEY_CODE_TO_LABEL[AT_SIGN] = ['???', '???'];
// KEY_CODE_TO_LABEL[QUESTION_MARK] = ['???', '???'];
// KEY_CODE_TO_LABEL[TILDE] = ['???', '???'];

export const VALID_KEY_CODES: Key[] = KEY_CODE_TO_LABEL
  .filter((value, code) => value !== null)
  .map((_, code) => code);
