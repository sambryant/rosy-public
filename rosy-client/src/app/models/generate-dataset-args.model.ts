import {Id} from './content.model';

export interface GenerateDatasetArgs {
  datasetName: string | null,
  useExistingImages: boolean,
  includeHidden: boolean,
  includeUnprocessed: boolean,
  trainSize: number,
};

export type GenerateTagDatasetArgs = GenerateDatasetArgs;

export interface GenerateFeatureDatasetArgs {
  datasetName: string | null,
  useExistingImages: boolean,
  includeHidden: boolean,
  includeUnprocessed: boolean,
  features: Id[],
  excludeTags: Id[],
  includeTags: Id[],
  generateNullData: boolean | null,
  nullFeatures: Id[],
};
