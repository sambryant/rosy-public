import {encodeRange, decodeDateRange, decodeFloatRange, Range, } from '.';


describe('Range<Float>', () => {

  it('should encode correctly', () => {
    const tests: [Range<number>, string, string][] = [
      [{ lower: 1.3, upper: null }, '>1.3', 'should encode with a lower bound'],
      [{ lower: 1, upper: null }, '>1', 'should encode with an integer lower bound'],
      [{ lower: null, upper: 4.5 }, '<4.5', 'should encode with an upper bound'],
      [{ lower: null, upper: 4 }, '<4', 'should encode with an integer upper bound'],
      [{ lower: 1.3, upper: 4.5 }, '>1.3,<4.5', 'should encode with an upper and lower bound'],
      [{ lower: 1, upper: 4 }, '>1,<4', 'should encode with an integer upper and lower bound'],
    ];
    tests.forEach(([range, expected, context]) => {
      const actParams = {existing: 'value'};
      const expParams = {existing: 'value', 'testKey': expected};
      encodeRange(actParams, 'testKey', range);
      expect(actParams).withContext(context).toEqual(expParams);
    });
  });

  it('should not store value when empty', () => {
    const range: Range<number> = { lower: null, upper: null };
    const actParams = {existing: 'value'};
    const expParams = {existing: 'value'};
    encodeRange(actParams, 'testKey', range);
    expect(actParams).toEqual(expParams);
  });

  it('should decode correctly when key is found in params', () => {
    const key = 'testKey';
    const params = { 'testKey': '' };
    const tests: [string, Range<number>, string][] = [
      ['>1.3', { lower: 1.3, upper: null }, 'should decode with a lower bound'],
      ['>1', { lower: 1, upper: null }, 'should decode with an integer lower bound'],
      ['<4.5', { lower: null, upper: 4.5 }, 'should decode with an upper bound'],
      ['<4', { lower: null, upper: 4 }, 'should decode with an integer upper bound'],
      ['>1.3,<4.5', { lower: 1.3, upper: 4.5 }, 'should decode with an upper and lower bound'],
      ['<4.5,>1.3', { lower: 1.3, upper: 4.5 }, 'should decode with an upper and lower bound'],
      ['>1,<4', { lower: 1, upper: 4 }, 'should decode with an integer upper and lower bound'],
    ];
    tests.forEach(([encoded, expected, context]) => {
      params[key] = encoded;
      expect(decodeFloatRange(params, key)).withContext(context).toEqual(expected);
    });
  });

  it('should decode correctly when key is not found in params', () => {
    const params = { otherKey: 'hi' };
    const expected = { lower: null, upper: null };
    expect(decodeFloatRange(params, 'testKey')).toEqual(expected);
  });

  it('should decode misformatted strings correctly', () => {
    const lower = null;
    const upper = 3;
    const tests: [string, Range<number>, string][] = [
      [' >1', { lower, upper: null }, 'should ignore when first char isnt comparison'],
      [' >1,<3', { lower, upper }, 'should still parse other valid filter'],
      ['>cow,<3', { lower, upper }, 'should ignore when value is not numeric'],
      ['>1,<cow', { lower: 1, upper: null }, 'should still parse other valid filter'],
      ['', { lower: null, upper: null }, 'should decode empty string as null'],
      [null as any, { lower: null, upper: null }, 'should decode null as null'],
      [undefined as any, { lower: null, upper: null }, 'should decode undefined as null'],
    ];
    tests.forEach(([encoded, expected, context]) => {
      const params = {test: encoded};
      expect(decodeFloatRange(params, 'test')).withContext(context).toEqual(expected);
    });
  });
});

describe('Range<Date>', () => {

  it('should encode correctly', () => {
    const tests: [Range<Date>, string, string][] = [
      [{ lower: new Date(12), upper: null }, '>12', 'should encode with a lower bound'],
      [{ lower: null, upper: new Date(34) }, '<34', 'should encode with an upper bound'],
      [{ lower: new Date(12), upper: new Date(34) }, '>12,<34', 'should encode with an upper and lower bound'],
    ];
    tests.forEach(([range, expected, context]) => {
      const actParams = {existing: 'value'};
      const expParams = {existing: 'value', 'testKey': expected};
      encodeRange(actParams, 'testKey', range);
      expect(actParams).withContext(context).toEqual(expParams);
    });
  });

  it('should decode correctly when key is found in params', () => {
    const key = 'testKey';
    const params = { 'testKey': '' };
    const tests: [string, Range<Date>, string][] = [
      ['>12', { lower: new Date(12), upper: null }, 'should decode with a lower bound'],
      ['<34', { lower: null, upper: new Date(34) }, 'should decode with an upper bound'],
      ['>12,<34', { lower: new Date(12), upper: new Date(34) }, 'should decode with a lower and upper bound'],
      ['<34,>12', { lower: new Date(12), upper: new Date(34) }, 'should decode with a lower and upper bound'],
    ];
    tests.forEach(([encoded, expected, context]) => {
      params[key] = encoded;
      expect(decodeDateRange(params, key)).withContext(context).toEqual(expected);
    });
  });

  it('should decode correctly when key is not found in params', () => {
    const params = { otherKey: 'hi' };
    const expected = { lower: null, upper: null };
    expect(decodeDateRange(params, 'testKey')).toEqual(expected);
  });

  it('should decode misformatted strings correctly', () => {
    const lower = null;
    const upper = new Date(34);
    const tests: [string, Range<Date>, string][] = [
      [' >12', { lower, upper: null }, 'should ignore when first char isnt comparison'],
      [' >12,<34', { lower, upper }, 'should still parse other valid filter'],
      ['>cow,<34', { lower, upper }, 'should ignore when value is not numeric'],
      ['>12,<cow', { lower: new Date(12), upper: null }, 'should still parse other valid filter'],
      ['', { lower: null, upper: null }, 'should decode empty string as null'],
      [null as any, { lower: null, upper: null }, 'should decode null as null'],
      [undefined as any, { lower: null, upper: null }, 'should decode undefined as null'],
    ];
    tests.forEach(([encoded, expected, context]) => {
      const params = {test: encoded};
      expect(decodeDateRange(params, 'test')).withContext(context).toEqual(expected);
    });
  });

});
