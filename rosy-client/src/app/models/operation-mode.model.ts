export type OperationMode = 'add' | 'remove' | 'toggle';
export const OPERATION_MODES: OperationMode[] = ['add', 'remove', 'toggle'];
