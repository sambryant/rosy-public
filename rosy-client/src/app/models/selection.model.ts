import {ArtistDetails, ContentEigendata, Group, Image, NameDetails, TagDetails, } from './content.model';

export class Selection<T> {
  selected: T[];
  item: T | null;

  constructor(selected: T[], item: T | null) {
    this.selected = selected;
    this.item = item;
  }
}

export class ArtistSelection extends Selection<ArtistDetails> {}

export class EigendataSelection extends Selection<ContentEigendata> {}

export class GroupSelection extends Selection<Group> {}

export class ImageSelection extends Selection<Image> {}

export class NameSelection extends Selection<NameDetails> {}

export class TagSelection extends Selection<TagDetails> {}

