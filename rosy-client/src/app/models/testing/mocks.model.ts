import {
  Artist,
  ArtistId,
  DupeType,
  Group,
  GroupId,
  Image,
  ImageId,
  Name,
  NameId,
  Tag,
  TagId,
} from '..';


export class MockArtist implements Artist {
  id: ArtistId;
  value: string;
  createdTime: number;

  constructor(id: ArtistId, value: string) {
    this.id = id;
    this.value = value;
    this.createdTime = 1234;
  }

  static new(id: ArtistId) {
    return new MockArtist(id, `Artist ${id}`);
  }

}

export class MockArtistD extends MockArtist {
  imageCount: number;
  score: number;
  metadata: undefined;

  constructor(id: ArtistId, value: string) {
    super(id, value);
    this.imageCount = 0;
    this.score = 0;
  }

  static override new(id: ArtistId) {
    return new MockArtistD(id, `ArtistD ${id}`);
  }

}

export class MockGroup implements Group {
  id: GroupId;
  value: string;
  description: string;
  hidden = false;
  names: Name[] = [];
  tags: Tag[] = [];
  images: Image[] = [];

  constructor(id: GroupId, value: string) {
    this.id = id;
    this.value = value;
  }

  static new(id: GroupId) {
    return new MockGroup(id, `Group ${id}`);
  }

  static setData(groups: Group[], data: {tags?: Tag[][], names?: Name[][], images?: Image[][]}) {
    groups.forEach((group, i) => {
      if (data.tags) {
        group.tags = data.tags[i];
      }
      if (data.names) {
        group.names = data.names[i];
      }
      if (data.images) {
        group.images = data.images[i];
      }
    });
  }

  withImages(imageIds: number[]): MockGroup {
    this.images = imageIds.map(id => MockImage.new(id));
    return this;
  }

}

interface MockImagesData {
  artistId?: (ArtistId | null)[];
  groupId?: (GroupId | null)[];
  dupes?: {[key: number]: DupeType}[];
  names?: Name[][];
  tags?: Tag[][];
}

export class MockImage implements Image {
  id: ImageId;
  filename: string;
  thumbnail: string;
  createdTime = 1234;
  filesize = 0;
  hidden = false;
  favorite = false;
  isVideo = false;
  length: number | null = null
  marked = false;
  processed = true;
  processedTime = 5678;
  ratingAverage: number | null = null;
  ratingCount = 0;
  artistId: ArtistId | null  = null;
  groupId: GroupId | null = null;
  dupes: {[key: number]: DupeType} = {};
  tags: Tag[] = [];
  names: Name[] = [];

  constructor(id: ImageId, filename: string) {
    this.id = id;
    this.filename = filename;
    this.thumbnail = 'test thumbnail';
  }

  static new(id: ImageId) {
    return new MockImage(id, `${id}.jpg`);
  }

  static setData(images: Image[], data: MockImagesData) {
    images.forEach((image, i) => {
      if (data.artistId) {
        image.artistId = data.artistId[i];
      }
      if (data.groupId) {
        image.groupId = data.groupId[i];
      }
      if (data.dupes) {
        image.dupes = data.dupes[i];
      }
      if (data.tags) {
        image.tags = data.tags[i];
      }
      if (data.names) {
        image.names = data.names[i];
      }
    });
  }
}

export class MockName {
  id: NameId;
  createdTime: number;
  value: string;
  index: number | null;

  constructor(id: NameId, value: string) {
    this.id = id;
    this.value = value;
  }

  static new(id: TagId) {
    return new MockName(id, `Name ${id}`);
  }
}

export class MockNameD extends MockName {
  groupCount: number;
  imageCount: number;
  score: number;
  metadata: null;

  constructor(id: NameId, value: string) {
    super(id, value);
    this.groupCount = 0;
    this.imageCount = 0;
    this.score = 0.0;
    this.metadata = null;
  }

  static override new(id: NameId) {
    return new MockNameD(id, `NameD ${id}`);
  }
}

export class MockTag {
  id: TagId;
  createdTime: number;
  value: string;
  index: number | null;

  constructor(id: TagId, value: string) {
    this.id = id;
    this.value = value;
    this.index = null;
    this.createdTime = 1234;
  }

  static new(id: TagId) {
    return new MockTag(id, `Tag ${id}`);
  }
}


export class MockTagD extends MockTag {
  groupCount: number;
  imageCount: number;
  score: number;
  metadata: null;

  constructor(id: TagId, value: string) {
    super(id, value);
    this.groupCount = 0;
    this.imageCount = 0;
    this.score = 0.0;
    this.metadata = null;
  }

  static override new(id: TagId) {
    return new MockTagD(id, `TagD ${id}`);
  }
}
