import {DupeType} from './dupe.model';
import {Image} from './content.model';

export interface DupeCandidate {
  item1: Image;
  item2: Image;
  score: number;
  mark: DupeType | null;
};
