import {
  A,
  DELETE,
  N,
} from '@angular/cdk/keycodes';

import {
  Key,
  KeyBindingHash,
  KeyBinding,
  KeyEventLike,
  VALID_KEY_CODES,
} from './key-binding.model';

describe('KeyBinding', () => {

  it('should allow setting alt, ctl, shift keys and enableDuring input', () => {
    let kb: KeyBinding;

    expect(new KeyBinding(A, {altKey: true}).altKey).toBeTrue();
    expect(new KeyBinding(A, {altKey: false}).altKey).toBeFalse();
    expect(new KeyBinding(A, {ctrlKey: true}).ctrlKey).toBeTrue();
    expect(new KeyBinding(A, {ctrlKey: false}).ctrlKey).toBeFalse();
    expect(new KeyBinding(A, {shiftKey: true}).shiftKey).toBeTrue();
    expect(new KeyBinding(A, {shiftKey: false}).shiftKey).toBeFalse();
    expect(new KeyBinding(A, {enableDuringInput: true}).enableDuringInput).toBeTrue();
    expect(new KeyBinding(A, {enableDuringInput: false}).enableDuringInput).toBeFalse();
  });


  it('should have an injective hash function', () => {
    let keyBindingCount = 0;
    const hashes = new Set<KeyBindingHash>();
    const keyValues = [false, true];

    keyValues.forEach(altKey => {
      keyValues.forEach(ctrlKey => {
        keyValues.forEach(shiftKey => {
          VALID_KEY_CODES.forEach(keyCode => {
            keyBindingCount += 1;
            hashes.add(KeyBinding.keyToHash({ altKey, ctrlKey, shiftKey, keyCode }));
          });
        });
      });
    });
    expect(hashes.size).toBe(keyBindingCount);
  });

  it('should set key binding label appropriately', () => {
    const tests: [KeyBinding, string][] = [
      [new KeyBinding(A, {ctrlKey: true}), 'Ctrl + a'],
      [new KeyBinding(A, {shiftKey: true}), 'A'],
      [new KeyBinding(A, {altKey: true}), 'Alt + a'],
      [new KeyBinding(A, {ctrlKey: true, shiftKey: true}), 'Ctrl + A'],
      [new KeyBinding(DELETE, {ctrlKey: true}), 'Ctrl + ⌦'],
      [new KeyBinding(DELETE, {shiftKey: true}), '⇧ + ⌦'],
      [new KeyBinding(DELETE, {altKey: true}), 'Alt + ⌦'],
      [new KeyBinding(DELETE, {ctrlKey: true, shiftKey: true}), 'Ctrl + ⇧ + ⌦'],
    ];
    tests.forEach(([keyBinding, expectedLabel]) => {
      expect(keyBinding.toLabel()).toEqual(expectedLabel);
    });
  });

  it('should throw an error when the key binding is invalid', () => {
    let keyData = {keyCode: N, ctrlKey: true, altKey: false, shiftKey: false};
    expect(KeyBinding.isValid(keyData)).toBeFalse();
    expect(() => KeyBinding.fromEvent(keyData)).toThrow(new Error(`Invalid key binding: ${N}`));

    keyData = {keyCode: 255, ctrlKey: true, altKey: false, shiftKey: false};
    expect(KeyBinding.isValid(keyData)).toBeFalse();
    expect(() => KeyBinding.fromEvent(keyData)).toThrow(new Error(`Invalid key binding: 255`));

    keyData = {keyCode: A, ctrlKey: true, altKey: false, shiftKey: false};
    expect(KeyBinding.isValid(keyData)).toBeTrue();
    expect(() => KeyBinding.fromEvent(keyData)).not.toThrowError();
  });

});
