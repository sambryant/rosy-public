import {
  DEFAULT_DUPE_CUTOFF_FACE,
  DEFAULT_PAGE_SIZE,
  DupeCandidatesQuery,
  EigendataQuery,
  FlagFilter,
  GroupQuery,
  ImageQuery,
  IMAGE_INBOX_PARAMS,
  NameQuery,
  TagQuery,
  QueryFromParams,
  Range,
} from '.';



describe('SortedQuery', () => {

  it('should use default direction when direction is not given', () => {
    const query = new ImageQuery();
    query.sort = {
      active: 'some field',
      direction: 'desc',
    };
    const params = {
      sortBy: 'id'
    };

    query.fromParams(params);
    expect(query.sort.active).toEqual('id');
    expect(query.sort.direction).toEqual('asc');
  });

  it('should use default direction when direction is invalid', () => {
    const query = new ImageQuery();
    query.sort = {
      active: 'some field',
      direction: 'desc',
    };
    const params = {
      sortBy: 'id',
      direction: 'garblar'
    };

    query.fromParams(params);
    expect(query.sort.active).toEqual('id');
    expect(query.sort.direction).toEqual('asc');
  });

});


describe('GroupQuery', () => {

  it('should create with default values', () => {
    const query = new GroupQuery();
    expect(query.size).toEqual(DEFAULT_PAGE_SIZE);
    expect(query.page).toEqual(0);
  });

  it('should create with non-default values', () => {
    const query = new GroupQuery(13, 143);
    expect(query.size).toEqual(13);
    expect(query.page).toEqual(143);
  });

});


describe('ImageQuery', () => {

  it('should have correct default values', () => {
    const query = new ImageQuery();

    expect(query.size).toBe(DEFAULT_PAGE_SIZE);
    expect(query.page).toBe(0);
    expect(query.tags).toEqual([]);
    expect(query.names).toEqual([]);
    expect(query.sort).toEqual({active: 'random', direction: 'asc'});
    expect(query.hidden).toBe(FlagFilter.All);
    expect(query.isVideo).toBe(FlagFilter.All);
    expect(query.favorite).toBe(FlagFilter.All);
    expect(query.rating).toEqual({ lower: null, upper: null });
    expect(query.ratingCount).toEqual({ lower: null, upper: null });
    expect(query.createdTime).toEqual({ lower: null, upper: null });
    expect(query.processedTime).toEqual({ lower: null, upper: null });
  });

  it('should accept page and size arguments', () => {
    const query = new ImageQuery(13, 143);

    expect(query.size).toBe(13);
    expect(query.page).toBe(143);
  });

  it('should toggle random', () => {
    const query = new ImageQuery(13, 143);
    query.sort.active = 'random';
    expect(query.toggleRandom()).toBeTrue();
    expect(query.sort.active).toEqual('');

    query.sort.active = 'id';
    expect(query.toggleRandom()).toBeTrue();
    expect(query.sort.active).toEqual('random');
  });

  it('should reset page when refreshing', () => {
    const query = new ImageQuery(13, 143);
    query.refresh();
    expect(query.size).toEqual(13);
    expect(query.page).toEqual(0);
  });

  it('should have a correct inbox query params constant', () => {
    const expParams = {
      size: DEFAULT_PAGE_SIZE.toString(),
      page: '0',
      sortBy: 'id',
      hidden: 'false',
      direction: 'asc',
      isVideo: 'false',
      processed: 'false'
    };
    expect(IMAGE_INBOX_PARAMS).toEqual(expParams);
  });

  describe('toParams', () => {

    it('should encode the page and size when default', () => {
      const query = new ImageQuery();
      const expectedSize = DEFAULT_PAGE_SIZE.toString();
      const expectedPage = '0';

      const params = query.toParams();
      expect(params['page']).toEqual(expectedPage);
      expect(params['size']).toEqual(expectedSize);
    });

    it('should encode the page and size when not default', () => {
      const query = new ImageQuery(13, 143);
      const expectedSize = '13';
      const expectedPage = '143';

      const params = query.toParams();
      expect(params['page']).toEqual(expectedPage);
      expect(params['size']).toEqual(expectedSize);
    });

    it('should not encode sorting features when sort.active is empty', () => {
      const query = new ImageQuery();
      let params;

      query.sort.direction = 'desc';
      query.sort.active = '';
      params = query.toParams();
      expect(params.hasOwnProperty('sortBy')).toBeFalse();
      expect(params.hasOwnProperty('direction')).toBeFalse();

      query.sort = {active: 'createdTime', direction: 'asc'};
      params = query.toParams();
      expect(params.hasOwnProperty('sortBy')).toBeTrue();
      expect(params.hasOwnProperty('direction')).toBeTrue();
    });

    it('should encode sorting features when sort.active is not null', () => {
      const query = new ImageQuery();
      let params;

      query.sort.active = 'processed';
      params = query.toParams();
      expect(params['sortBy']).toEqual('processed');
      expect(params['direction']).toEqual('asc');

      query.sort.direction = 'desc';
      params = query.toParams();
      expect(params['direction']).toEqual('desc');
    });

    it('should encode default direction when direction is empty', () => {
      const query = new ImageQuery();
      let params;

      query.sort.direction = '';
      query.sort.active = 'id';
      params = query.toParams();
      expect(params['sortBy']).toEqual('id');
      expect(params['direction']).toEqual('asc');
    });

    it('should encode artists', () => {
      const query = new ImageQuery();
      let params;

      query.artists = [1, 2, 3];
      params = query.toParams();
      expect(params['artists']).toEqual('1,2,3');

      query.artists = [];
      params = query.toParams();
      expect(params.hasOwnProperty('artists')).toBeFalse();
    });

    it('should encode groups', () => {
      const query = new ImageQuery();
      let params;

      query.groups = [1, 2, 3];
      params = query.toParams();
      expect(params['groups']).toEqual('1,2,3');

      query.groups = [];
      params = query.toParams();
      expect(params.hasOwnProperty('groups')).toBeFalse();
    });

    it('should not encode tags if list is empty', () => {
      const query = new ImageQuery();
      let params;

      query.tags = [];
      params = query.toParams();
      expect(params.hasOwnProperty('tags')).toBeFalse();

      query.tags = [1];
      params = query.toParams();
      expect(params.hasOwnProperty('tags')).toBeTrue();
    });

    it('should not encode names if list is empty', () => {
      const query = new ImageQuery();
      let params;

      query.names = [];
      params = query.toParams();
      expect(params.hasOwnProperty('names')).toBeFalse();

      query.names = [1];
      params = query.toParams();
      expect(params.hasOwnProperty('names')).toBeTrue();
    });

    it('should encode a single tag filter', () => {
      const query = new ImageQuery();
      query.tags = [1];
      const params = query.toParams();

      expect(params['tags']).toEqual('1');
    });

    it('should encode multiple tag filters', () => {
      const query = new ImageQuery();
      query.tags = [1, 2];
      const params = query.toParams();

      expect(params['tags']).toEqual('1,2');
    });

    it('should encode a single name filter', () => {
      const query = new ImageQuery();
      query.names = [1];
      const params = query.toParams();

      expect(params['names']).toEqual('1');
    });

    it('should encode multiple name filters', () => {
      const query = new ImageQuery();
      query.names = [1, 2];
      const params = query.toParams();

      expect(params['names']).toEqual('1,2');
    });

    it('should not encode hidden when its set to all', () => {
      const query = new ImageQuery();
      query.hidden = FlagFilter.All;
      const params = query.toParams();

      expect(params.hasOwnProperty('hidden')).toBeFalse();
    });

    it('should encode hidden when its true', () => {
      const query = new ImageQuery();
      query.hidden = FlagFilter.ForceTrue;
      const params = query.toParams();

      expect(params['hidden']).toEqual('true');
    });

    it('should encode hidden when its false', () => {
      const query = new ImageQuery();
      query.hidden = FlagFilter.ForceFalse;
      const params = query.toParams();

      expect(params['hidden']).toEqual('false');
    });

    it('should not encode isVideo when its null', () => {
      const query = new ImageQuery();
      query.isVideo = FlagFilter.All;
      const params = query.toParams();

      expect(params.hasOwnProperty('isVideo')).toBeFalse();
    });

    it('should encode isVideo when its true', () => {
      const query = new ImageQuery();
      query.isVideo = FlagFilter.ForceTrue;
      const params = query.toParams();

      expect(params['isVideo']).toEqual('true');
    });

    it('should encode isVideo when its false', () => {
      const query = new ImageQuery();
      query.isVideo = FlagFilter.ForceFalse;
      const params = query.toParams();

      expect(params['isVideo']).toEqual('false');
    });

    it('should encode favorite correctly', () => {
      const tests: [FlagFilter, string][] = [
        [FlagFilter.ForceTrue, 'true'],
        [FlagFilter.ForceFalse, 'false']
      ];
      tests.forEach(([value, expected]) => {
        const query = new ImageQuery();
        query.favorite = value;
        expect (query.toParams()['favorite']).toEqual(expected);
      });
    });

    it('should encode boolean fields correctly', () => {
      const fields = ['hasArtist', 'grouped', 'isVideo', 'processed'];
      const tests: [FlagFilter, string][] = [
        [FlagFilter.ForceTrue, 'true'],
        [FlagFilter.ForceFalse, 'false']
      ];
      fields.forEach((field) => {
        tests.forEach(([value, expected]) => {
          const query = new ImageQuery();
          query[field] = value;
          expect (query.toParams()[field]).toEqual(expected);
        });
      });
    });

    it('should not encode favorite when its null', () => {
      const query = new ImageQuery();
      query.favorite = FlagFilter.All;
      const params = query.toParams();

      expect(params.hasOwnProperty('favorite')).toBeFalse();
    });

    it('should encode rating correctly', () => {
      const tests: [number | null, number | null, string][] = [
        [1.0, null, '>1'],
        [null as any, 3.5, '<3.5'],
        [1.1, 3.1, '>1.1,<3.1'],
      ];
      const query = new ImageQuery();
      tests.forEach(([lower, upper, expected]) => {
        query.rating.lower = lower;
        query.rating.upper = upper;
        expect(query.toParams()['rating']).toEqual(expected);
      });
    });

    it('should not encode rating when its range is null', () => {
      const query = new ImageQuery();
      query.rating.lower = null;
      query.rating.upper = null;
      const params = query.toParams();

      expect(params.hasOwnProperty('rating')).toBeFalse();
    });

    it('should encode rating count correctly', () => {
      const tests: [number | null, number | null, string][] = [
        [1, null, '>1'],
        [null as any, 3, '<3'],
        [1, 3, '>1,<3'],
      ];
      const query = new ImageQuery();
      tests.forEach(([lower, upper, expected]) => {
        query.ratingCount.lower = lower;
        query.ratingCount.upper = upper;
        expect(query.toParams()['ratingCount']).toEqual(expected);
      });
    });

    it('should not encode rating count when its range is null', () => {
      const query = new ImageQuery();
      query.ratingCount.lower = null;
      query.ratingCount.upper = null;
      const params = query.toParams();

      expect(params.hasOwnProperty('ratingCount')).toBeFalse();
    });

    it('should encode createdTime correctly', () => {
      const tests: [Date | null, Date | null, string][] = [
        [new Date(12341234), null, '>12341234'],
        [null, new Date(56785678), '<56785678'],
        [new Date(12341234), new Date(56785678), '>12341234,<56785678'],
      ];
      const query = new ImageQuery();
      tests.forEach(([lower, upper, expected]) => {
        query.createdTime.lower = lower;
        query.createdTime.upper = upper;
        expect(query.toParams()['createdTime']).toEqual(expected);
      });
    });

    it('should not encode createdTime when its range is null', () => {
      const query = new ImageQuery();
      query.createdTime.lower = null;
      query.createdTime.upper = null;
      const params = query.toParams();

      expect(params.hasOwnProperty('createdTime')).toBeFalse();
    });

    it('should encode processedTime correctly', () => {
      const tests: [Date | null, Date | null, string][] = [
        [new Date(12341234), null, '>12341234'],
        [null, new Date(56785678), '<56785678'],
        [new Date(12341234), new Date(56785678), '>12341234,<56785678'],
      ];
      const query = new ImageQuery();
      tests.forEach(([lower, upper, expected]) => {
        query.processedTime.lower = lower;
        query.processedTime.upper = upper;
        expect(query.toParams()['processedTime']).toEqual(expected);
      });
    });

    it('should not encode processedTime when its range is null', () => {
      const query = new ImageQuery();
      query.processedTime.lower = null;
      query.processedTime.upper = null;
      const params = query.toParams();

      expect(params.hasOwnProperty('processedTime')).toBeFalse();
    });
  });


  describe('fromParams', () => {

    it('should use default values when params empty', () => {
      const params = {};
      const expectedQuery = new ImageQuery();

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
    });

    it('should decode the query size', () => {
      const params = {
        size: '103'
      };
      const expectedQuery = new ImageQuery();
      expectedQuery.size = 103;

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
    });

    it('should decode the query size and page', () => {
      const params = {
        size: '103',
        page: '44'
      };
      const expectedQuery = new ImageQuery();
      expectedQuery.size = 103;
      expectedQuery.page = 44;

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
    });

    it('should not decode the page if no size is given', () => {
      const params = {
        page: '44'
      };
      const expectedQuery = new ImageQuery();

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
    });

    it('should use default size when size param is invalid', () => {
      const sizeValues = [
        undefined, null, '', 'cow', 'batman103'
      ];
      const expectedQuery = new ImageQuery();

      sizeValues.forEach(value => {
        const params = {size: value};
        expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
      });
    });

    it('should use default page when page param is invalid', () => {
      const size = '103';
      const pageValues = [
        undefined, null, '', 'cow', 'batman103'
      ];
      const expectedQuery = new ImageQuery();
      expectedQuery.size = 103;

      pageValues.forEach(page => {
        expect(QueryFromParams(ImageQuery, {size, page})).toEqual(expectedQuery);
      });
    });

    it('should use default page when size param is invalid', () => {
      const sizeValues = [
        undefined, null, '', 'cow', 'batman103'
      ];
      const page = '3';
      const expectedQuery = new ImageQuery();

      sizeValues.forEach(size => {
        expect(QueryFromParams(ImageQuery, {size, page})).toEqual(expectedQuery);
      });
    });

    it('should decode sortBy and direction if present', () => {
      const params = {sortBy: 'processed', direction: 'asc'};
      const expectedQuery = new ImageQuery();
      expectedQuery.sort = {active: 'processed', direction: 'asc'};

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);

      params.direction = 'desc';
      expectedQuery.sort.direction = 'desc';

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
    });

    it('should use default direction if sortBy given but not direction', () => {
      const params = {sortBy: 'processed'};
      const expectedQuery = new ImageQuery();
      expectedQuery.sort = {active: 'processed', direction: 'asc'};

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
    });

    it('should use default direction if sortBy given but direction invalid', () => {
      const params = {sortBy: 'processed', direction: 'down'};
      const expectedQuery = new ImageQuery();
      expectedQuery.sort = {active: 'processed', direction: 'asc'};

      expect(QueryFromParams(ImageQuery, params)).toEqual(expectedQuery);
    });

    it('should decode "favorite" parameter correctly', () => {
      const tests: [string, FlagFilter, string][] = [
        ['false', FlagFilter.ForceFalse, 'should decode false'],
        ['true', FlagFilter.ForceTrue, 'should decode true'],
        ['blah', FlagFilter.All, 'decode nonsense string as null'],
        ['', FlagFilter.All, 'decode empty string as null'],
        ['0', FlagFilter.All, 'decode 0 string as null'],
        ['1', FlagFilter.All, 'decode 1 string as null'],
        [null as any, FlagFilter.All, 'decode null as null'],
        [undefined as any, FlagFilter.All, 'decode undefined as null'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { favorite: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.favorite = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "hasArtist" parameter correctly', () => {
      const tests: [string, FlagFilter, string][] = [
        ['false', FlagFilter.ForceFalse, 'should decode false'],
        ['true', FlagFilter.ForceTrue, 'should decode true'],
        ['blah', FlagFilter.All, 'decode nonsense string as null'],
        ['', FlagFilter.All, 'decode empty string as null'],
        ['0', FlagFilter.All, 'decode 0 string as null'],
        ['1', FlagFilter.All, 'decode 1 string as null'],
        [null as any, FlagFilter.All, 'decode null as null'],
        [undefined as any, FlagFilter.All, 'decode undefined as null'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { hasArtist: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.hasArtist = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "grouped" parameter correctly', () => {
      const tests: [string, FlagFilter, string][] = [
        ['false', FlagFilter.ForceFalse, 'should decode false'],
        ['true', FlagFilter.ForceTrue, 'should decode true'],
        ['blah', FlagFilter.All, 'decode nonsense string as null'],
        ['', FlagFilter.All, 'decode empty string as null'],
        ['0', FlagFilter.All, 'decode 0 string as null'],
        ['1', FlagFilter.All, 'decode 1 string as null'],
        [null as any, FlagFilter.All, 'decode null as null'],
        [undefined as any, FlagFilter.All, 'decode undefined as null'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { grouped: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.grouped = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "hidden" parameter correctly', () => {
      const tests: [string, FlagFilter, string][] = [
        ['false', FlagFilter.ForceFalse, 'should decode false'],
        ['true', FlagFilter.ForceTrue, 'should decode true'],
        ['blah', FlagFilter.All, 'decode nonsense string as null'],
        ['', FlagFilter.All, 'decode empty string as null'],
        ['0', FlagFilter.All, 'decode 0 string as null'],
        ['1', FlagFilter.All, 'decode 1 string as null'],
        [null as any, FlagFilter.All, 'decode null as null'],
        [undefined as any, FlagFilter.All, 'decode undefined as null'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { hidden: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.hidden = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "isVideo" parameter correctly', () => {
      const tests: [string, FlagFilter, string][] = [
        ['false', FlagFilter.ForceFalse, 'should decode false'],
        ['true', FlagFilter.ForceTrue, 'should decode true'],
        ['blah', FlagFilter.All, 'decode nonsense string as null'],
        ['', FlagFilter.All, 'decode empty string as null'],
        ['0', FlagFilter.All, 'decode 0 string as null'],
        ['1', FlagFilter.All, 'decode 1 string as null'],
        [null as any, FlagFilter.All, 'decode null as null'],
        [undefined as any, FlagFilter.All, 'decode undefined as null'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { isVideo: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.isVideo = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "processed" parameter correctly', () => {
      const tests: [string, FlagFilter, string][] = [
        ['false', FlagFilter.ForceFalse, 'should decode false'],
        ['true', FlagFilter.ForceTrue, 'should decode true'],
        ['blah', FlagFilter.All, 'decode nonsense string as null'],
        ['', FlagFilter.All, 'decode empty string as null'],
        ['0', FlagFilter.All, 'decode 0 string as null'],
        ['1', FlagFilter.All, 'decode 1 string as null'],
        [null as any, FlagFilter.All, 'decode null as null'],
        [undefined as any, FlagFilter.All, 'decode undefined as null'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { processed: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.processed = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "names" parameter correctly', () => {
      const tests: [string, number[], string][] = [
        ['1', [1], 'should decode a single value', ],
        ['1,2,3', [1, 2, 3], 'should decode multiple values'],
        ['', [], 'should decode empty string as []'],
        [undefined as any, [], 'should decode undefined as []'],
        [null as any, [], 'should decode null as []'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { names: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.names = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "tags" parameter correctly', () => {
      const tests: [string, number[], string][] = [
        ['1', [1], 'should decode a single value', ],
        ['1,2,3', [1, 2, 3], 'should decode multiple values'],
        ['', [], 'should decode empty string as []'],
        [undefined as any, [], 'should decode undefined as []'],
        [null as any, [], 'should decode null as []'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { tags: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.tags = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "artists" parameter correctly', () => {
      const tests: [string, number[], string][] = [
        ['1', [1], 'should decode a single integer string'],
        ['1,2,3', [1, 2, 3], 'should decode a list of integer strings '],
        ['1,2,cow', [1, 2], 'should decode a mixed list of valid and invalid integer strings'],
        ['blah', [], 'should decode an invalid integer string'],
        ['', [], 'should decode an empty string as []'],
        [null as any, [], 'should null an empty string as []'],
        [undefined as any, [], 'should undefined an empty string as []'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { artists: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.artists = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "groups" parameter correctly', () => {
      const tests: [string, number[], string][] = [
        ['1', [1], 'should decode a single integer string'],
        ['1,2,3', [1, 2, 3], 'should decode a list of integer strings '],
        ['1,2,cow', [1, 2], 'should decode a mixed list of valid and invalid integer strings'],
        ['blah', [], 'should decode an invalid integer string'],
        ['', [], 'should decode an empty string as []'],
        [null as any, [], 'should null an empty string as []'],
        [undefined as any, [], 'should undefined an empty string as []'],
      ];

      tests.forEach(([paramString, expected, context]) => {
        const params = { groups: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.groups = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "rating" parameter correctly', () => {
      const tests: [string, Range<number>, string][] = [
        ['>1.1', { lower: 1.1, upper: null }, 'should decode with an upper bound'],
        ['<3.5', { lower: null, upper: 3.5 }, 'should decode with a lower bound'],
        ['>1.1,<3.5', { lower: 1.1, upper: 3.5 }, 'should decode with an upper and lower bound'],
        ['blah,<3.5', { lower: null, upper: 3.5 }, 'should decode a mix of invalid and valid'],
        ['blah', { lower: null, upper: null }, 'should decode an invalid string as null'],
        ['', { lower: null, upper: null }, 'should decode an empty string as null'],
        [null as any, { lower: null, upper: null }, 'should decode null as null'],
        [undefined as any, { lower: null, upper: null }, 'should decode undefined as null'],
      ];
      tests.forEach(([paramString, expected, context]) => {
        const params = { rating: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.rating = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "ratingCount" parameter correctly', () => {
      const tests: [string, Range<number>, string][] = [
        ['>1', { lower: 1, upper: null }, 'should decode with an upper bound'],
        ['<3', { lower: null, upper: 3 }, 'should decode with a lower bound'],
        ['>1,<3', { lower: 1, upper: 3 }, 'should decode with an upper and lower bound'],
        ['blah,<3', { lower: null, upper: 3 }, 'should decode a mix of invalid and valid'],
        ['blah', { lower: null, upper: null }, 'should decode an invalid string as null'],
        ['', { lower: null, upper: null }, 'should decode an empty string as null'],
        [null as any, { lower: null, upper: null }, 'should decode null as null'],
        [undefined as any, { lower: null, upper: null }, 'should decode undefined as null'],
      ];
      tests.forEach(([paramString, expected, context]) => {
        const params = { ratingCount: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.ratingCount = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    function dateRange(lower: number | null, upper: number | null): Range<Date> {
      return {
        lower: lower !== null ? new Date(lower) : null,
        upper: upper !== null ? new Date(upper) : null,
      };
    }

    it('should decode "createdTime" parameter correctly', () => {
      const tests: [string, Range<Date>, string][] = [
        ['>12341234', dateRange(12341234, null), 'should decode with a lower bound'],
        ['<56785678', dateRange(null, 56785678), 'should decode with an upper bound'],
        ['>12341234,<56785678', dateRange(12341234, 56785678), 'should decode with an upper and lower bound'],
        ['blah,<56785678', dateRange(null, 56785678), 'should decode a mix of valid and invalid'],
        ['blah', dateRange(null, null), 'should decode an invalid string as null'],
        ['', dateRange(null, null), 'should decode an empty string as null'],
        [null as any, dateRange(null, null), 'should decode null as null'],
        [undefined as any, dateRange(null, null), 'should decode undefined as null'],
      ];
      tests.forEach(([paramString, expected, context]) => {
        const params = { createdTime: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.createdTime = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });

    it('should decode "processedTime" parameter correctly', () => {
      const tests: [string, Range<Date>, string][] = [
        ['>12341234', dateRange(12341234, null), 'should decode with a lower bound'],
        ['<56785678', dateRange(null, 56785678), 'should decode with an upper bound'],
        ['>12341234,<56785678', dateRange(12341234, 56785678), 'should decode with an upper and lower bound'],
        ['blah,<56785678', dateRange(null, 56785678), 'should decode a mix of valid and invalid'],
        ['blah', dateRange(null, null), 'should decode an invalid string as null'],
        ['', dateRange(null, null), 'should decode an empty string as null'],
        [null as any, dateRange(null, null), 'should decode null as null'],
        [undefined as any, dateRange(null, null), 'should decode undefined as null'],
      ];
      tests.forEach(([paramString, expected, context]) => {
        const params = { processedTime: paramString };
        const expectedQuery = new ImageQuery();
        expectedQuery.processedTime = expected;
        expect(QueryFromParams(ImageQuery, params)).withContext(context).toEqual(expectedQuery);
      });
    });
  });

  describe('roundTrip', () => {

    it('should have equality on round trip with defaults', () => {
      const query = new ImageQuery();
      const roundTripImageQuery = QueryFromParams(ImageQuery, query.toParams());
      expect(roundTripImageQuery).toEqual(query);
    });

    it('should have equality on round trip without defaults', () => {
      const query = new ImageQuery();
      query.size = 103;
      query.page = 2;
      query.tags = [1, 2];
      query.names = [1];
      query.groups = [1, 2, 3, 4];
      query.createdTime.lower = new Date(12341234);
      query.processedTime.upper = new Date(56785678);
      query.rating.upper = 3.5;
      query.sort = {
        active: 'processed',
        direction: 'desc'
      };
      query.hidden = FlagFilter.ForceFalse;

      const roundTripImageQuery = QueryFromParams(ImageQuery, query.toParams());
      expect(roundTripImageQuery).toEqual(query);
    });

  });

});

describe('NameQuery / TagQuery', () => {

  it('should have correct default default sort values', () => {
    const query1 = new NameQuery();
    expect(query1.sort.active).toEqual('imageCount');
    expect(query1.sort.direction).toEqual('desc');
    const query2 = new TagQuery();
    expect(query2.sort.active).toEqual('imageCount');
    expect(query2.sort.direction).toEqual('desc');
  });

});

describe('EigendataQuery', () => {

  it('should have correct default values', () => {
    const query = new EigendataQuery();

    expect(query.numVecs).toBe(15);
    expect(query.numWeights).toBe(15);

    query.numVecs = 1234123;
    query.numWeights = 134143;
    query.fromParams({});

    expect(query.numVecs).toBe(15);
    expect(query.numWeights).toBe(15);
  });

  it('should encode fields correctly', () => {
    const query = new EigendataQuery();
    query.numVecs = 18;
    query.numWeights = 12034;
    const expParams = {numVecs: '18', numWeights: '12034'};

    expect(query.toParams()).toEqual(expParams)
  });

  it('should decode fields correctly', () => {
    const params = {numVecs: '1234', numWeights: '5678'};
    const expQuery = new EigendataQuery();
    expQuery.numVecs = 1234;
    expQuery.numWeights = 5678;

    expect(QueryFromParams(EigendataQuery, params)).toEqual(expQuery);
  });

  it('should decode invalid fields as defaults', () => {
    const paramsList = [
      {numVecs: 'abc18', numWeights: '-1'},
      {numVecs: '0', numWeights: '0'},
    ];
    const expQuery = new EigendataQuery();
    expQuery.numVecs = 15;
    expQuery.numWeights = 15;

    paramsList.forEach(params => {
      expect(QueryFromParams(EigendataQuery, params)).toEqual(expQuery);
    });
  });

});

describe('DupeCandidatesQuery', () => {

  it('should have correct default values', () => {
    const query = new DupeCandidatesQuery();

    expect(query.size).toBe(DEFAULT_PAGE_SIZE);
    expect(query.page).toBe(0);
    expect(query.marked).toBe(FlagFilter.All);
    expect(query.cutoff).toBe(DEFAULT_DUPE_CUTOFF_FACE);
    expect(query.method).toBeNull();
  });

  it('should convert empty arguments correctly', () => {
    const query = new DupeCandidatesQuery();
    const expParams = {
      size: `${DEFAULT_PAGE_SIZE}`,
      page: '0',
      cutoff: `${DEFAULT_DUPE_CUTOFF_FACE}`,
    };

    expect(query.toParams()).toEqual(expParams);
  });

  it('should decode method correctly', () => {
    const params = {method: 'test-method'};
    const expQuery = new DupeCandidatesQuery();
    expQuery.method = 'test-method';

    const actQuery = QueryFromParams(DupeCandidatesQuery, params);
    expect(actQuery).toEqual(expQuery);
  });

  it('should decode cutoff correctly', () => {
    const tests: [any, number | null][] = [
      [{cutoff: 'cow'}, DEFAULT_DUPE_CUTOFF_FACE],
      [{cutoff: '1.4'}, 1.4]
    ];
    tests.forEach(([params, expected]) => {
      expect(QueryFromParams(DupeCandidatesQuery, params).cutoff).toEqual(expected);
    });
  });

  it('should decode marked correctly', () => {
    const tests: [any, FlagFilter][] = [
      [{}, FlagFilter.All],
      [{marked: '1'}, FlagFilter.All],
      [{marked: 'false'}, FlagFilter.ForceFalse],
      [{marked: 'true'}, FlagFilter.ForceTrue],
    ];
    tests.forEach(([params, expected]) => {
      expect(QueryFromParams(DupeCandidatesQuery, params).marked).toEqual(expected);
    });
  });

  it('should encode cutoff correctly', () => {
    const tests: [number | null, any][] = [
      [null, {page: '0', size: `${DEFAULT_PAGE_SIZE}`}],
      [1, {cutoff: '1', page: '0', size: `${DEFAULT_PAGE_SIZE}`}],
      [1.4, {cutoff: '1.4', page: '0', size: `${DEFAULT_PAGE_SIZE}`}],
    ];
    tests.forEach(([value, expected]) => {
      const q = new DupeCandidatesQuery();
      q.cutoff = value;
      expect(q.toParams()).toEqual(expected)
    });
  });

  it('should encode marked correctly', () => {
    const defaultParams = {
      size: `${DEFAULT_PAGE_SIZE}`,
      page: '0',
      cutoff: `${DEFAULT_DUPE_CUTOFF_FACE}`,
    };

    const tests: [FlagFilter, any][] = [
      [FlagFilter.All, {}],
      [FlagFilter.ForceTrue, {marked: 'true'}],
      [FlagFilter.ForceFalse, {marked: 'false'}],
    ];
    tests.forEach(([value, expected]) => {
      const expParams = {};
      Object.assign(expParams, defaultParams);
      Object.assign(expParams, expected);
      const q = new DupeCandidatesQuery();
      q.marked = value;
      expect(q.toParams()).toEqual(expParams);
    });
  });

  it('should encode method correctly', () => {
    const defaultParams = {
      size: `${DEFAULT_PAGE_SIZE}`,
      page: '0',
      cutoff: `${DEFAULT_DUPE_CUTOFF_FACE}`,
    };
    const tests: [string | null, any][] = [
      [null, {}],
      ['test-method', {method: 'test-method'}],
    ];
    tests.forEach(([value, expected]) => {
      const expParams = {};
      Object.assign(expParams, defaultParams);
      Object.assign(expParams, expected);
      const q = new DupeCandidatesQuery();
      q.method = value;
      expect(q.toParams()).toEqual(expParams)
    });
  });

});