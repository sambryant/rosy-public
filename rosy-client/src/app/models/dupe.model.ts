export type DupeType = 'Exact' | 'Most' | 'Partial' | 'None';

export const DUPE_TYPES: DupeType[] = ['Exact', 'Most', 'Partial', 'None'];

export interface DupeTypeMap<T> {
  Exact: T,
  Most: T,
  Partial: T,
  None: T
};

export type Dupe = [number, DupeType];
