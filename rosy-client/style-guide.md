# Rosy Angular Style Guide


## Imports

### Rules

  1. All modules should export members with an `index.ts` file.
  2. Prefer importing from module folder rather than individual files

```
    // Bad
    import {Image} from '@app/models/image.model';

    // Good
    import {Image} from '@app/models';
```

  3. If importing five or more members from a folder, each must get its own line:

```
    // Bad
    import {Model1, Model2, Model3, Model4, Model5, Model6} from '@app/models';

    // Good
    import {
      Model1,
      ...
      Model6,
    } from '@app/models';
```

  4. Import lists should always use a trailing comma

```
    // Bad
    import {
      Model1,
      Model2
    } from '@app/models';

    // Good
    import {
      Model1,
      Model2,
    } from '@app/models';
```

  5. Imports should be alphabetized with multiple-line import statements appearing first, e.g.

```
    import {
      AArdvark,
      Koala,
    } from '@app/animals';
    import {
      Marimba,
      Xyzlaphone,
    } from '@app/instruments';
    import {ApiService} from '@services';
    import {Banana} from '@app/fruits';
    import {Cat, Dog,} from '@app/dometicated';
```

  6. When alphabetizing, lowercase always comes first:
```
  apple,
  zebra,
  Aardvark,
  BANANA,
  Camel,
```