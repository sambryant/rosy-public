import './settings.css';
import './settings.html';

import {BackgroundRequest, ImportRequest, LocalData, REQUIRE_CTRL_KEY, mapErr, readLocalStorage, setLocalStorage, generateUrl} from './shared';

const visitButton = document.getElementById("visitButton") as HTMLButtonElement;
const targetSelectButton = document.getElementById("targetSelectionButton") as HTMLButtonElement;
const targetSelectInput  = document.getElementById("targetSelectionInput") as HTMLInputElement;
const schemaSelectButton = document.getElementById("schemaSelectionButton") as HTMLButtonElement;
const schemaSelectInput  = document.getElementById("schemaSelectionInput") as HTMLInputElement;
const httpsSelectInput = document.getElementById("httpsSelectionInput") as HTMLInputElement;
const requireCtrlKeyInput = document.getElementById("requireCtrlKeyInput") as HTMLInputElement;

readLocalStorage([]).then(
  (obj: LocalData) => {
    visitButton.disabled = !obj || !obj.target;
    targetSelectInput.value = obj.target || '';
    schemaSelectInput.value = obj.schema || '';
    httpsSelectInput.checked = obj.useHttps || false;
    requireCtrlKeyInput.checked = obj.requireCtrlKey || REQUIRE_CTRL_KEY;
  },
  console.error);

schemaSelectButton.addEventListener('click', () => {
  setLocalStorage({ schema: schemaSelectInput.value });
});

targetSelectButton.addEventListener('click', () => {
  const target = targetSelectInput.value;
  setLocalStorage({ target });
  visitButton.disabled = !target;
});

requireCtrlKeyInput.addEventListener('change', () => {
  setLocalStorage({ requireCtrlKey: requireCtrlKeyInput.checked });
});

httpsSelectInput.addEventListener('change', () => {
  setLocalStorage({ useHttps: httpsSelectInput.checked });
});

visitButton.addEventListener('click', () => {
  generateUrl().then((target) => {
     browser.tabs.create({ url: target.url });
  });
});
