export type StorageField = 'schema' | 'target' | 'useHttps' | 'requireCtrlKey';

export const REQUIRE_CTRL_KEY = false;

export interface BackgroundRequest {
  type: 'import' | 'message';
  message?: string;
  importData?: ImportRequest;
}

export interface ImportRequest {
  url: string;
  page: string;
}

export interface LocalData {
  schema?: string;
  target?: string;
  useHttps?: boolean;
  requireCtrlKey?: boolean;
}

export function mapErr<T, U>(promise: Promise<T>, errorValue: U): Promise<T> {
  return promise.catch(error => {
    throw errorValue;
  });
}

export function logErr<T>(promise: Promise<T>, userMessage: string, debugMessage?: any): Promise<T> {
  return promise.catch(error => {
    console.error('Request failed: ', { error, userMessage, debugMessage });
    throw userMessage;
  });
}

export function setLocalStorage(values: LocalData) {
  browser.storage.local.set(values);
}

export function generateUrl(): Promise<{url: string, schema: string}> {
  return readLocalStorage(['target', 'schema'])
    .then((localData: LocalData) => {
      const protocol = (localData.useHttps && 'https://' || 'http://');
      return {
        url: `${protocol}${localData.target}`,
        schema: localData.schema
      };
    });
}

export function isCtrlKeyRequired(): Promise<boolean> {
  return readLocalStorage([]).then(
    (obj: LocalData) => {
      if (obj && obj.requireCtrlKey !== undefined) {
        return obj.requireCtrlKey;
      } else {
        return REQUIRE_CTRL_KEY;
      }
    });
}

export function readLocalStorage(required: StorageField[]): Promise<LocalData> {
  const keys: StorageField[] = ['schema', 'target', 'useHttps', 'requireCtrlKey'];
  return mapErr(browser.storage.local.get(keys).then(obj => {
    if (!obj) {
      throw 'Nothing stored in local storage';
    } else {
      for (var i in required) {
        if (obj[required[i]] === undefined) {
          throw `Local storage missing required fields: ${required[i]}`;
        }
      }
      return obj as LocalData;
    }
  }), 'Unable to access local storage');
}
