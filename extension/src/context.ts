import {BackgroundRequest, REQUIRE_CTRL_KEY, isCtrlKeyRequired, } from './shared';

const CSS = `
  body.main-in-progress {
    border: 5px solid yellow !important;
  }
  body.main-finished {
    border: 5px solid green !important;
  }
  body.main-failed {
    border: 5px solid red !important;
  }
`;

function processImage(src: string) {
  var body = document.getElementsByTagName('body')[0];
  body.classList.remove('main-failed');
  body.classList.remove('main-finished');
  body.classList.add('main-in-progress');
  const request: BackgroundRequest = {
    type: 'import',
    importData: {
      url: src,
      page: window && window.location && window.location.href
    }
  };

  browser.runtime.sendMessage(request)
    .then(_ => {
      body.classList.remove('main-in-progress');
      body.classList.add('main-finished');
    }, _ => {
      body.classList.remove('main-in-progress');
      body.classList.add('main-failed');
    });
}

// initialize CSS
(function() {
  const style = document.createElement('style');
  const head = document.head || document.getElementsByTagName('head')[0];
  head.appendChild(style);
  style.type = 'text/css';
  style.appendChild(document.createTextNode(CSS));
})();

let ctrlKeyRequired = REQUIRE_CTRL_KEY;
isCtrlKeyRequired().then((requireCtrlKey) => {
  ctrlKeyRequired = requireCtrlKey;
});

document.addEventListener('contextmenu', (event: MouseEvent) => {
  // Read settings to determine if the ctrl key is required to be pressed
  const ctrlKey = !ctrlKeyRequired || event.ctrlKey;
  const imageTarget = (event.target as any).closest('img');
  if (imageTarget && ctrlKey) {
    const imageSource = imageTarget.currentSrc || imageTarget.src;
    processImage(imageSource);
    event.preventDefault();
    browser.menus.overrideContext({});
    return true;
  }

}, { capture: true });