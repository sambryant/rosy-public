import {BackgroundRequest, ImportRequest, LocalData, logErr, generateUrl} from './shared';

const ROSY_SCHEMA_HEADER = 'x-rosy-schema';
const API_IMPORT = '/api/external/import_image';
const NETWORK_ERR_MSG = 'Couldn\'t reach rosy. Is it running?';
const MISSING_TARGET_ERR_MSG = 'Must set target and schema in extension settings before importing';


function messageHandler(request: BackgroundRequest, _, __): Promise<string> {
  if (request.type === 'import') {
    return processImage(request.importData);
  } else if (request.type === 'message') {
    return showNotification(request.message, true);
  } else {
    throw 'Bad extension request';
  }
}

function showNotification(message: string, okay: boolean): Promise<string> {
  return browser.notifications.create({
    'type': 'basic',
    'title': 'Rosy',
    'message': message
  }).then(_ => {
    if (okay) {
      return message;
    } else {
      throw message;
    }
  });
}

function generateRequest(url: string, schema: string | undefined, method: string, data: Object): Request {
  let headers = {
    'Content-Type': 'application/json'
  };
  if (schema) {
    headers[ROSY_SCHEMA_HEADER] = schema;
  }
  return new Request(url, { body: JSON.stringify(data), method, headers });
}

function wrappedFetch(request: Request): Promise<Response> {
  return logErr(fetch(request), NETWORK_ERR_MSG, 'Fetch request was rejected');
}

function processImage(importData: ImportRequest): Promise<string> {

  function processResponse(response: Response): Promise<string> {
    const ok = response.ok;
    return logErr(response.json(), NETWORK_ERR_MSG, 'Couldnt parse response JSON')
      .then(responseData => {
        if (response && response.ok && responseData && responseData.id) {
          return `Added new image with id ${responseData.id}`;
        } else if (responseData && responseData.message) {
          throw `Error on import: ${responseData.message}`;
        } else {
          throw NETWORK_ERR_MSG;
        }
      });
  }

  return logErr(generateUrl(), MISSING_TARGET_ERR_MSG)
    .then(target => generateRequest(`${target.url}${API_IMPORT}`, target.schema, 'POST', importData))
    .then(wrappedFetch)
    .then(processResponse)
    .then((msg: string) => showNotification(msg, true))
    .catch((msg: string) => showNotification(msg, false));
}

browser.runtime.onMessage.addListener(messageHandler);
