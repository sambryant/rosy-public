# Extension Development Guide

The basic idea is that we use `webpack` to package a firefox extension written in TypeScript.
The source files live in `./src`. To build the web application, `cd` into this directory and do (in separate terminals):

    $ npm run watch
    $ npm run start

**What this does**

This will launch a firefox instance with the extension automatically added. The extension will automatically reload whenever you change any of the source files in `src/`.

There are three important meta files.

  1. `package.json` defines the `npm run build` scripts which call `webpack`.
  2. `webpack.config.js` describes how to package and build the application. It uses `tsc` to compile the `.ts` files to `.js` files.
  3. `tsconfig.js` tells the TypeScript compiler how to behave.

I don't have a great understanding of `node`, `TypeScript` or `webpack` so all of these are very fragile.
It took a lot of tinkering to get everything to work so don't mess with it too much.

I roughly followed [this guide](https://medium.com/better-programming/create-a-chrome-extension-using-react-and-typescript-50e94e14320c) to get my setup going.
Unfortunately, the CSS stuff did not work straight out of the box.
Instead I had to use a loader that copies over the `css` files and leaves them as raw files.
