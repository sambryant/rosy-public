const { optimize } = require('webpack');
const { join } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
let prodPlugins = [];

module.exports = {
  mode: process.env.NODE_ENV,
  entry: {
    background: join(__dirname, 'src/background.ts'),
    context: join(__dirname, 'src/context.ts'),
    settings: join(__dirname, 'src/settings.ts')
  },
  output: {
    path: join(__dirname, 'dist'),
    filename: '[name].js',
  },
  devtool: 'inline-source-map',
  optimization: {
    minimize: false
  },
  plugins: [
    new MiniCssExtractPlugin(), // keeps CSS files as separate files
    new CopyPlugin({
      patterns: [
        {from: 'manifest.json', to: 'manifest.json'},
        {from: 'icons/*', to: ''}
      ]
    })
  ],
  module: {
    rules: [
      {
        test: /\.ts?$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      },
      {
        test: /\.html$/i,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }
    ],
  },
  resolve: {
    extensions: ['.ts', '.js', '.css', '.html'],
  },
};