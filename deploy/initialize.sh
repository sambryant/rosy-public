#!/bin/bash
sudo true # noop: get sudo access
set -e

PARENT=$(dirname $(realpath "$0"))
cd "$PARENT"

echo "[1/2] Installing directories on host machine at /web/rosy"
sudo mkdir -p /web/rosy/
sudo mkdir -p /web/rosy/database
sudo mkdir -p /web/rosy/storage
sudo mkdir -p /web/rosy/media

echo "[2/2] Setting up encryption locking/unlocking services and scripts"
./deploy-locking.sh > /dev/null

touch ".env"

echo "Finished! Next steps:"
echo "  1. Define env variables required by docker-compose in $PARENT/.env"
echo "  2. Launch! 'docker-compose up -f FILE' (choose dev or prod)"
