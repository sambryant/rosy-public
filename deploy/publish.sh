#!/bin/bash

USAGE="$0 <version> <message>"

if [[ $# -ne 2 ]]; then
  echo "Bad usage. Usage: $USAGE" >> /dev/stderr
  exit 1
fi
VERSION="$1"
MESSAGE="$2"

echo "Publishing version $VERSION. This will not make any changes but it will print the commands for you to review"
echo "git tag -a v$VERSION -m $MESSAGE"
echo "git push --tags"
echo "docker tag registry.gitlab.com/sambryant/rosy/server-base:latest registry.gitlab.com/sambryant/rosy/server-base:$VERSION"
echo "docker tag registry.gitlab.com/sambryant/rosy/server:latest registry.gitlab.com/sambryant/rosy/server:$VERSION"
echo "docker tag registry.gitlab.com/sambryant/rosy/frontend:latest registry.gitlab.com/sambryant/rosy/frontend:$VERSION"
echo "docker push registry.gitlab.com/sambryant/rosy/server-base:$VERSION"
echo "docker push registry.gitlab.com/sambryant/rosy/server:$VERSION"
echo "docker push registry.gitlab.com/sambryant/rosy/frontend:$VERSION"
echo "docker push registry.gitlab.com/sambryant/rosy/server-base:latest"
echo "docker push registry.gitlab.com/sambryant/rosy/server:latest"
echo "docker push registry.gitlab.com/sambryant/rosy/frontend:latest"
