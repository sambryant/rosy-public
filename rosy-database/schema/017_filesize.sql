BEGIN;

ALTER TABLE public.image
    ADD COLUMN filesize integer NOT NULL DEFAULT 0;

COMMIT;