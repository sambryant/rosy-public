BEGIN;

DROP TABLE IF EXISTS public.tag_coefficients;
DROP TABLE IF EXISTS public.tag_eigenvector;
DROP TABLE IF EXISTS public.metadata_eigenvector;
DROP TABLE IF EXISTS public.metadata_eigendata;
DROP TABLE IF EXISTS public.name_metadata;
DROP TABLE IF EXISTS public.tag_metadata;

CREATE TABLE public.metadata_eigendata
(
    id integer NOT NULL,
    eigenvalue real NOT NULL,
    eigenvector real[] NOT NULL,
    CONSTRAINT metadata_eigendata_pkey PRIMARY KEY (id)
)
TABLESPACE pg_default;
ALTER TABLE public.metadata_eigendata OWNER to postgres;

CREATE TABLE public.name_metadata
(
    id integer NOT NULL,
    evec_coefs real[] NOT NULL,
    low_names integer[] NOT NULL,
    low_name_scores real[] NOT NULL,
    low_tags integer[] NOT NULL,
    low_tag_scores real[] NOT NULL,
    high_names integer[] NOT NULL,
    high_name_scores real[] NOT NULL,
    high_tags integer[] NOT NULL,
    high_tag_scores real[] NOT NULL,
    CONSTRAINT name_metadata_id_pkey PRIMARY KEY (id),
    CONSTRAINT name_metadata_id_fkey FOREIGN KEY (id)
        REFERENCES public.name (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.name_metadata OWNER to postgres;

CREATE TABLE public.tag_metadata
(
    id integer NOT NULL,
    evec_coefs real[] NOT NULL,
    low_names integer[] NOT NULL,
    low_name_scores real[] NOT NULL,
    low_tags integer[] NOT NULL,
    low_tag_scores real[] NOT NULL,
    high_names integer[] NOT NULL,
    high_name_scores real[] NOT NULL,
    high_tags integer[] NOT NULL,
    high_tag_scores real[] NOT NULL,
    CONSTRAINT tag_metadata_id_pkey PRIMARY KEY (id),
    CONSTRAINT tag_metadata_id_fkey FOREIGN KEY (id)
        REFERENCES public.tag (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.tag_metadata OWNER to postgres;

COMMIT;