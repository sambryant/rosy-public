ALTER TABLE public.image
    ADD COLUMN group_id integer;
ALTER TABLE public.image
    ADD CONSTRAINT image_group_id_fkey FOREIGN KEY (group_id)
    REFERENCES public."group" (id) MATCH SIMPLE
    ON UPDATE RESTRICT
    ON DELETE SET NULL
    NOT VALID;

DROP TABLE group_image;
