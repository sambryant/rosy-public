BEGIN;

-- Create tables
-- Table: public.feature
CREATE SEQUENCE public.feature_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;
ALTER SEQUENCE public.feature_id_seq OWNER TO postgres;
CREATE TABLE public."feature"
(
    id integer NOT NULL DEFAULT nextval('feature_id_seq'::regclass),
    created_time bigint NOT NULL,
    value text NOT NULL,
    CONSTRAINT feature_pkey PRIMARY KEY (id)
)
TABLESPACE pg_default;
ALTER TABLE public."feature" OWNER to postgres;

-- Table: public.image_feature
CREATE SEQUENCE public.image_feature_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;
ALTER SEQUENCE public.image_feature_id_seq OWNER TO postgres;
CREATE TABLE public."image_feature"
(
    id integer NOT NULL DEFAULT nextval('image_feature_id_seq'::regclass),
    image_id integer NOT NULL,
    feature_id integer NOT NULL,
    x1 real NOT NULL,
    x2 real NOT NULL,
    y1 real NOT NULL,
    y2 real NOT NULL,
    CONSTRAINT image_feature_pkey PRIMARY KEY (id),
    CONSTRAINT image_feature_image_id_fkey FOREIGN KEY (image_id)
        REFERENCES public."image" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE,
    CONSTRAINT image_feature_feature_id_fkey FOREIGN KEY (feature_id)
        REFERENCES public."feature" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public."image_feature" OWNER to postgres;

COMMIT;
