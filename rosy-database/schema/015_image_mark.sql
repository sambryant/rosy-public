BEGIN;

ALTER TABLE public.image
  ADD COLUMN marked boolean NOT NULL DEFAULT false;

COMMIT;
