BEGIN;

-- Create new tag table
CREATE SEQUENCE public.name_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;
CREATE TABLE public."new_name"
(
    id integer NOT NULL DEFAULT nextval('name_id_seq'::regclass),
    created_time bigint NOT NULL DEFAULT 1000 * extract(epoch from now()),
    value character varying(64) COLLATE pg_catalog."default" NOT NULL,
    index integer,
    CONSTRAINT new_name_pkey PRIMARY KEY (id),
    CONSTRAINT new_name_value_key UNIQUE (value),
    CONSTRAINT new_name_index_key UNIQUE (index)
)
TABLESPACE pg_default;

-- Create new image-name mapping table
CREATE TABLE public."image_name"
(
    img_id integer NOT NULL,
    nme_id integer NOT NULL,
    CONSTRAINT image_name_pkey PRIMARY KEY (img_id, nme_id),
    CONSTRAINT image_name_img_id_fkey FOREIGN KEY (img_id)
        REFERENCES public."image" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;

-- Create new group-name mapping table
CREATE TABLE public."new_group_name"
(
    grp_id integer NOT NULL,
    nme_id integer NOT NULL,
    CONSTRAINT group_name_pkeyy PRIMARY KEY (grp_id, nme_id),
    CONSTRAINT group_name_grp_id_fkey FOREIGN KEY (grp_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;

-- Create a new tag row for each unique tag encountered (both images and groups)
INSERT INTO public."new_name" (value)
    SELECT DISTINCT "value" FROM public."name";
INSERT INTO public."new_name" (value)
    SELECT DISTINCT "value" FROM public."group_name"
    ON CONFLICT DO NOTHING;

-- Import image names into new table
INSERT INTO public."image_name" (img_id, nme_id)
    SELECT name."image_id", new_name."id" FROM public."new_name"
    INNER JOIN public."name" on new_name."value" = name."value";

-- Import group names into new table
INSERT INTO public."new_group_name" (grp_id, nme_id)
    SELECT group_name."group_id", new_name."id" FROM public."new_name"
    INNER JOIN public."group_name" on new_name."value" = group_name."value";

DROP TABLE public."name";
DROP TABLE public."group_name";

ALTER TABLE public."new_name" RENAME TO "name";
ALTER TABLE public."new_group_name" RENAME TO "group_name";

ALTER TABLE public."image_name"
    ADD CONSTRAINT image_name_id_fkey FOREIGN KEY (nme_id)
        REFERENCES public."name" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE;
ALTER TABLE public."group_name"
    ADD CONSTRAINT group_name_id_fkey FOREIGN KEY (nme_id)
        REFERENCES public."name" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE;

ALTER SEQUENCE public.name_id_seq OWNER TO postgres;
ALTER TABLE public."name" OWNER to postgres;
ALTER TABLE public."image_name" OWNER to postgres;
ALTER TABLE public."group_name" OWNER to postgres;

COMMIT;
