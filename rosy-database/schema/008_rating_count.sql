
-- Create new column 'rating_count'
ALTER TABLE public.image
  ADD COLUMN rating_count integer;

UPDATE public.image
  SET rating_count=(SELECT COUNT(*)
    FROM public.rating
    WHERE public.rating.image_id=public.image.id);

ALTER TABLE public.image
  ALTER COLUMN rating_count SET NOT NULL;

-- Rename average_rating to rating_average
ALTER TABLE public.image
  ADD COLUMN rating_average real;

UPDATE public.image
  SET rating_average=average_rating;

ALTER TABLE public.image
  DROP COLUMN average_rating;
