ALTER TABLE public.image_index DROP COLUMN imghashes;

ALTER TABLE public.image_index
    ADD COLUMN filehash bytea;
