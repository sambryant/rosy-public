-- Switch artist id from using user_id_seq to using artist_id_seq
ALTER TABLE public.artist
  ALTER COLUMN "id"
    SET DEFAULT nextval('artist_id_seq'::regclass);

-- Reset values of user_id_seq and artist_id_seq to reflect the databases.
SELECT setval('public.user_id_seq', (SELECT MAX(id) FROM public.user)+1);
SELECT setval('public.artist_id_seq', (SELECT MAX(id) FROM public.artist)+1);
