BEGIN;

-- Create new image-image dupe mapping table
CREATE TABLE public."dupe"
(
  img1_id integer NOT NULL,
  img2_id integer NOT NULL,
  dupe_type character varying(16) COLLATE pg_catalog."default" NOT NULL,
  CONSTRAINT dupe_pkey PRIMARY KEY (img1_id, img2_id),
  CONSTRAINT dupe_img1_id_fkey FOREIGN KEY (img1_id)
    REFERENCES public."image" (id) MATCH SIMPLE
    ON UPDATE RESTRICT
    ON DELETE CASCADE,
  CONSTRAINT dupe_img2_id_fkey FOREIGN KEY (img2_id)
    REFERENCES public."image" (id) MATCH SIMPLE
    ON UPDATE RESTRICT
    ON DELETE CASCADE
)
TABLESPACE pg_default;

COMMIT;

