CREATE TABLE public.group_name
(
    group_id integer NOT NULL,
    value character varying(64) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT group_name_pkey PRIMARY KEY (group_id, value),
    CONSTRAINT group_name_group_id_fkey FOREIGN KEY (group_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.group_name OWNER to postgres;

CREATE TABLE public.group_tag
(
    group_id integer NOT NULL,
    value character varying(64) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT group_tag_pkey PRIMARY KEY (group_id, value),
    CONSTRAINT group_tag_group_id_fkey FOREIGN KEY (group_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.group_tag OWNER to postgres;

ALTER TABLE public."group"
    ADD COLUMN deleted boolean NOT NULL DEFAULT false;
ALTER TABLE public."group"
    ADD COLUMN hidden boolean NOT NULL DEFAULT false;
ALTER TABLE public."group"
    ALTER COLUMN title TYPE text COLLATE pg_catalog."default";
ALTER TABLE public."group"
    ADD COLUMN description text COLLATE pg_catalog."default";
