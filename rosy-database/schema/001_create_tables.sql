-- Create tables
-- Table: public.user
CREATE SEQUENCE public.user_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;
ALTER SEQUENCE public.user_id_seq OWNER TO postgres;
CREATE TABLE public."user"
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    username character varying(24) COLLATE pg_catalog."default" NOT NULL,
    password character varying(80) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT user_username_key UNIQUE (username)
)
TABLESPACE pg_default;
ALTER TABLE public."user" OWNER to postgres;

-- Table: public.image
CREATE SEQUENCE public.image_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;
ALTER SEQUENCE public.image_id_seq OWNER TO postgres;
CREATE TABLE public.image
(
    id integer NOT NULL DEFAULT nextval('image_id_seq'::regclass),
    filename character varying(120) COLLATE pg_catalog."default" NOT NULL,
    created_time bigint NOT NULL,
    deleted boolean NOT NULL DEFAULT false,
    hidden boolean NOT NULL DEFAULT false,
    favorite boolean NOT NULL DEFAULT false,
    is_video boolean NOT NULL DEFAULT false,
    processed boolean NOT NULL DEFAULT false,
    processed_time bigint,
    average_rating real,
    source character varying(120) COLLATE pg_catalog."default",
    source_page character varying(120) COLLATE pg_catalog."default",
    CONSTRAINT image_pkey PRIMARY KEY (id),
    CONSTRAINT image_filename_key UNIQUE (filename)
)
TABLESPACE pg_default;
ALTER TABLE public.image OWNER to postgres;

-- Table: public.group
CREATE SEQUENCE public.group_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;
ALTER SEQUENCE public.group_id_seq OWNER TO postgres;
CREATE TABLE public."group"
(
    id integer NOT NULL DEFAULT nextval('group_id_seq'::regclass),
    created_time bigint NOT NULL,
    title character varying(120) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT group_pkey PRIMARY KEY (id),
    CONSTRAINT group_title_key UNIQUE (title)
)
TABLESPACE pg_default;
ALTER TABLE public."group" OWNER to postgres;

-- Table: public.group_image
CREATE TABLE public.group_image
(
    image_id integer NOT NULL,
    group_id integer NOT NULL,
    CONSTRAINT group_image_pkey PRIMARY KEY (image_id, group_id),
    CONSTRAINT group_image_group_id_fkey FOREIGN KEY (group_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE,
    CONSTRAINT group_image_image_id_fkey FOREIGN KEY (image_id)
        REFERENCES public.image (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.group_image OWNER to postgres;

-- Table: public.image_index
CREATE TABLE public.image_index
(
    id integer NOT NULL,
    encodings bytea,
    locations bytea,
    imghashes bytea,
    CONSTRAINT image_index_pkey PRIMARY KEY (id),
    CONSTRAINT image_index_id_fkey FOREIGN KEY (id)
        REFERENCES public.image (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.image_index OWNER to postgres;

-- Table: public.name
CREATE TABLE public.name
(
    image_id integer NOT NULL,
    value character varying(64) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT name_pkey PRIMARY KEY (image_id, value),
    CONSTRAINT name_image_id_fkey FOREIGN KEY (image_id)
        REFERENCES public.image (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.name OWNER to postgres;

-- Table: public.tag
CREATE TABLE public.tag
(
    image_id integer NOT NULL,
    value character varying(64) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tag_pkey PRIMARY KEY (image_id, value),
    CONSTRAINT tag_image_id_fkey FOREIGN KEY (image_id)
        REFERENCES public.image (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.tag OWNER to postgres;

-- Table: public.rating
CREATE SEQUENCE public.rating_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;
ALTER SEQUENCE public.rating_id_seq OWNER TO postgres;
CREATE TABLE public.rating
(
    id integer NOT NULL DEFAULT nextval('rating_id_seq'::regclass),
    value integer NOT NULL,
    "time" bigint NOT NULL,
    image_id integer NOT NULL,
    CONSTRAINT rating_pkey PRIMARY KEY (id),
    CONSTRAINT rating_image_id_fkey FOREIGN KEY (image_id)
        REFERENCES public.image (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;
ALTER TABLE public.rating OWNER to postgres;
