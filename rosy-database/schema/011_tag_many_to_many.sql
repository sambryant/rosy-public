BEGIN;

-- Create new tag table
CREATE SEQUENCE public.tag_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;
CREATE TABLE public."new_tag"
(
    id integer NOT NULL DEFAULT nextval('tag_id_seq'::regclass),
    created_time bigint NOT NULL DEFAULT 1000 * extract(epoch from now()),
    value character varying(64) COLLATE pg_catalog."default" NOT NULL,
    index integer,
    CONSTRAINT new_tag_pkey PRIMARY KEY (id),
    CONSTRAINT new_tag_value_key UNIQUE (value),
    CONSTRAINT new_tag_index_key UNIQUE (index)
)
TABLESPACE pg_default;

-- Create new image-tag mapping table
CREATE TABLE public."image_tag"
(
    img_id integer NOT NULL,
    tag_id integer NOT NULL,
    CONSTRAINT image_tag_pkey PRIMARY KEY (img_id, tag_id),
    CONSTRAINT image_tag_img_id_fkey FOREIGN KEY (img_id)
        REFERENCES public."image" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;

-- Create new group-tag mapping table
CREATE TABLE public."new_group_tag"
(
    grp_id integer NOT NULL,
    tag_id integer NOT NULL,
    CONSTRAINT group_tag_pkeyy PRIMARY KEY (grp_id, tag_id),
    CONSTRAINT group_tag_grp_id_fkey FOREIGN KEY (grp_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)
TABLESPACE pg_default;

-- Create a new tag row for each unique tag encountered (both images and groups)
INSERT INTO public."new_tag" (value)
    SELECT DISTINCT "value" FROM public."tag";
INSERT INTO public."new_tag" (value)
    SELECT DISTINCT "value" FROM public."group_tag"
    ON CONFLICT DO NOTHING;

-- Import image tags into new table
INSERT INTO public."image_tag" (img_id, tag_id)
    SELECT tag."image_id", new_tag."id" FROM public."new_tag"
    INNER JOIN public."tag" on new_tag."value" = tag."value";

-- Import group tags into new table
INSERT INTO public."new_group_tag" (grp_id, tag_id)
    SELECT group_tag."group_id", new_tag."id" FROM public."new_tag"
    INNER JOIN public."group_tag" on new_tag."value" = group_tag."value";

DROP TABLE public."tag";
DROP TABLE public."group_tag";

ALTER TABLE public."new_tag" RENAME TO "tag";
ALTER TABLE public."new_group_tag" RENAME TO "group_tag";

ALTER TABLE public."image_tag"
    ADD CONSTRAINT image_tag_id_fkey FOREIGN KEY (tag_id)
        REFERENCES public."tag" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE;
ALTER TABLE public."group_tag"
    ADD CONSTRAINT group_tag_id_fkey FOREIGN KEY (tag_id)
        REFERENCES public."tag" (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE;

ALTER SEQUENCE public.tag_id_seq OWNER TO postgres;
ALTER TABLE public."tag" OWNER to postgres;
ALTER TABLE public."image_tag" OWNER to postgres;
ALTER TABLE public."group_tag" OWNER to postgres;

COMMIT;
