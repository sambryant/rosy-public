ALTER TABLE public.image
    ALTER COLUMN filename TYPE text COLLATE pg_catalog."default";

ALTER TABLE public.image
    ADD COLUMN thumbnail text;
