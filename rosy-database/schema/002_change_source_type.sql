ALTER TABLE public.image
  ALTER COLUMN source TYPE text COLLATE pg_catalog."default";
ALTER TABLE public.image
  ALTER COLUMN source_page TYPE text COLLATE pg_catalog."default";
