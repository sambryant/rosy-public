-- Create new table artist
CREATE SEQUENCE public.artist_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;
ALTER SEQUENCE public.artist_id_seq OWNER TO postgres;
CREATE TABLE public.artist
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    created_time bigint NOT NULL,
    deleted boolean NOT NULL DEFAULT false,
    name text NOT NULL,
    CONSTRAINT artist_name_key UNIQUE (name),
    PRIMARY KEY (id)
);

ALTER TABLE public.artist
    OWNER to postgres;

-- Create field on image linking it to artist
ALTER TABLE public.image
    ADD COLUMN artist_id integer;
ALTER TABLE public.image
    ADD CONSTRAINT image_artist_id_fkey FOREIGN KEY (artist_id)
    REFERENCES public."artist" (id) MATCH SIMPLE
    ON UPDATE RESTRICT
    ON DELETE SET NULL
    NOT VALID;
