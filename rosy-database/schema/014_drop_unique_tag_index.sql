BEGIN;

ALTER TABLE public.tag DROP CONSTRAINT new_tag_index_key;

ALTER TABLE public.name DROP CONSTRAINT new_name_index_key;

COMMIT;