BEGIN;

ALTER TABLE public.image
    ADD COLUMN length real;

COMMIT;