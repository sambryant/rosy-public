//! Defines coupling between environmental variables and configuration objects
use dotenv::dotenv;

use super::*;

impl EnvConfig for Config {
  fn from_env() -> Result<Self, EnvError> {
    dotenv().ok(); // this is a slow method.
    let dev_mode: bool = Self::read_env("DEV_MODE")?;
    let auth = AuthConfig::from_env()?;
    let db = DBConfig::from_env()?;
    let file = FileConfig::from_env()?;
    let host = HostConfig::from_env()?;
    Ok(Config { dev_mode, auth, db, host, file })
  }
}

impl EnvConfig for AuthConfig {
  fn from_env() -> Result<Self, EnvError> {
    Ok(AuthConfig::new(Self::read_env("SECRET_KEY")?))
  }
}

impl EnvConfig for DBConfig {
  fn from_env() -> Result<Self, EnvError> {
    let host: String = Self::read_env("DATABASE_HOST")?;
    let pass: String = Self::read_env("DATABASE_PASS")?;
    let user: String = Self::read_env("DATABASE_USER")?;
    let schema: String = Self::read_env("DATABASE_SCHEMA")?;
    Ok(DBConfig::new(user, pass, host, schema))
  }
}

impl EnvConfig for FileConfig {
  fn from_env() -> Result<Self, EnvError> {
    let storage_dir = Self::read_env("STORAGE_DIR")?;
    let base_media_dir = Self::read_env("BASE_MEDIA_DIR")?;
    FileConfig::new(storage_dir, base_media_dir).map_err(|e| EnvError::FileInit(e))
  }
}

impl EnvConfig for HostConfig {
  fn from_env() -> Result<Self, EnvError> {
    let server_port: u16 = Self::read_env("SERVER_PORT")?;
    HostConfig::new(server_port)
  }
}
