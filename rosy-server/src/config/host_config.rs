use std::{
  net::SocketAddr,
  str::FromStr,
};

use crate::constants::SERVER_HOST;
use super::EnvError;

/// Contains configuration info relating to the app's web addresses.
pub struct HostConfig {
  /// Local host+post to listen on e.g. 0.0.0.0:3000
  pub listen_addr: SocketAddr,
}

impl HostConfig {

  pub fn new(server_port: u16) -> Result<HostConfig, EnvError> {
    let addr_str = format!("{}:{}", SERVER_HOST, server_port);
    match SocketAddr::from_str(&addr_str) {
      Ok(listen_addr) => Ok(HostConfig { listen_addr }),
      Err(_) => Err(EnvError::InvalidVar(
        format!("Invalid server socket address: '{}' (is your port value valid?)", addr_str)))
    }
  }

  pub fn basic_info(&self) -> Vec<String> {
    vec![
      format!("{:<15}: {:?}", "listen addr", self.listen_addr),
    ]
  }

}
