use rand::distributions::{Distribution, Uniform};
use std::{
  collections::HashMap,
  ffi::{OsStr, OsString},
  fmt::Debug,
  fs,
  io::Error as IOError,
  path::{Path, PathBuf},
};

use crate::prelude::*;
use crate::{
  constants::THUMBNAIL_FMT,
  dupe_report::DupeReport,
};

// TODO: All file operations desparately need a refactor. Every file that interacts with the
// filesystem seems to reinvent the wheel and there is no rhyme or reason to where they are stored.


/// Container indicating some file has been created during an operation which may have failed.
pub type CreateRecord<T> = Option<T>;

/// Container indicating some file has been renamed during an operation which may have failed.
pub type RenameRecord<T> = Option<(T, T)>;

#[derive(Clone, Copy, Debug)]
pub struct FileId(pub u64);

#[derive(Clone, Debug)]
pub struct ContentFile(OsString);

#[derive(Clone, Debug)]
pub struct ThumbnailFile(OsString);

pub type ThreadId = u64;

/// Contains configuration info relating to the app's web addresses.
pub struct FileConfig {
  /// Root directory where application stores all content and files.
  ///
  /// This directory should always exist and be writable.
  storage_dir: PathBuf,

  /// Mount point (or symlink) of volume where application stores large data (media).
  base_media_dir: PathBuf,

  /// Directory housing raw media files.
  media_dir: PathBuf,

  /// Directory housing thumbnails of media files.
  thumb_dir: PathBuf,

  /// Directory housing temporary files.
  temp_dir: PathBuf,

  /// Directory housing small media files / records for ML training
  train_dir: PathBuf,

  /// Directory housing trashed media/thumbnail files.
  trash_dir: PathBuf,

  /// Directory housing thread files.
  thread_dir: PathBuf,

  file_id_generator: Uniform<u64>,

  thread_id_generator: Uniform<u64>,
}

#[derive(Debug)]
pub enum RenameError {
  TargetExists,
  SourceMissing,
  IO(IOError),
}

#[derive(Debug)]
/// Error occurring when initializing directory structure on file system.
pub enum FileInitError {
  /// Indicates a necessary directory was not found and could not be created.
  CreateDir(String),
  /// Indicates a necessary file exists but conflicts with requirements.
  FileConflicts(String),
  /// Indicates a necessary directory was not found.
  KeyFileMissing(String),
}

impl FileConfig {

  pub fn new(storage_dir_name: String, base_media_dir_name: String) -> Result<Self, FileInitError> {
    let storage_dir = PathBuf::from(storage_dir_name);
    let temp_dir = storage_dir.join("tmp");
    let thread_dir = storage_dir.join("threads");
    let base_media_dir = PathBuf::from(base_media_dir_name);
    let media_dir = base_media_dir.join("images");
    let thumb_dir = base_media_dir.join("thumbnails");
    let train_dir = base_media_dir.join("training");
    let trash_dir = base_media_dir.join("trash");

    let file_id_generator = Uniform::from(0..9223372036854775807); // (0, 2^63 - 1)
    let thread_id_generator = Uniform::from(0..9007199254740991); // (0, 2^53 - 1) (maximum integer in javascript)

    let fc = Self {
      storage_dir, thread_dir, base_media_dir, media_dir, temp_dir, thumb_dir, train_dir, trash_dir, file_id_generator, thread_id_generator
    };
    fc.initialize()
  }


  fn initialize(self) -> Result<Self, FileInitError> {
    if !self.storage_dir.is_dir() {
      return Err(FileInitError::KeyFileMissing(format!("Main storage directory was not found: '{:?}'", self.storage_dir)))
    }

    // TODO:
    // Need to check that underlying device is available within docker container

    self.create_tmp_dir().map_err(|e| FileInitError::CreateDir(
      format!("Failed to create temporary file directory: {:?}, cause: {:?}", self.temp_dir, e)
    ))?;
    self.create_thread_dir().map_err(|e| FileInitError::CreateDir(
      format!("Failed to create thread file directory: {:?}, cause: {:?}", self.thread_dir, e)
    ))?;

    Ok(self)
  }

  pub fn basic_info(&self) -> Vec<String> {
    vec![
      format!("{:<15}: {:?}", "storage dir", self.storage_dir),
      format!("{:<15}: {:?}", "media dir", self.media_dir),
      format!("{:<15}: {:?}", "base media dir", self.base_media_dir),
    ]
  }

  pub fn generate_new_thread_id(&self) -> ThreadId {
    let mut rng = rand::thread_rng();
    self.thread_id_generator.sample(&mut rng)
  }

  fn create_tmp_dir(&self) -> Result<(), std::io::Error> {
    fs::create_dir_all(&self.temp_dir)
  }

  fn create_thread_dir(&self) -> Result<(), std::io::Error> {
    fs::create_dir_all(&self.thread_dir)
  }

  pub fn get_media_dir(&self) -> PathBuf {
    return self.media_dir.clone();
  }

  pub fn get_trash_dir(&self, schema: &str) -> Result<PathBuf, std::io::Error> {
    let trash_dir = self.trash_dir.join(schema);
    fs::create_dir_all(&trash_dir).map(|_| trash_dir)
  }

  pub fn get_train_dir(&self) -> Result<PathBuf, std::io::Error> {
    fs::create_dir_all(&self.train_dir).map(|_| self.train_dir.clone())
  }

  pub fn get_temporary_filename(&self, local_filename: &str) -> PathBuf {
    self.create_tmp_dir().ok();
    self.temp_dir.join(local_filename)
  }

  pub fn fresh_dupe_report(&self) -> Result<(fs::File, DupeReport), std::io::Error> {
    self.create_tmp_dir()?;

    // Generate unique ID for the report
    let mut rng = rand::thread_rng();
    let id = self.file_id_generator.sample(&mut rng);

    // Create empty dupe report file and dupe image directory.
    let file = fs::File::create(&self.get_temporary_filename("dupe-report.json"))?;
    let output_dir = self.temp_dir.join("dupe-report-{}").join(id.to_string());
    fs::create_dir_all(&output_dir)?;

    let report = DupeReport { id, output_dir, dupes: Vec::new(), filenames: HashMap::new() };
    Ok((file, report))
  }

  pub fn create_thread_file_buffer(&self, thread_id: u64) -> Result<fs::File, std::io::Error> {
    self.create_thread_dir()?;
    fs::File::create(&self.get_thread_filename_buffer(thread_id))
  }

  pub fn swap_thread_file_buffer(&self, thread_id: u64) -> Result<(), std::io::Error> {
    fs::rename(
      self.get_thread_filename_buffer(thread_id),
      self.get_thread_filename(thread_id)
    )
  }

  pub fn open_thread_file(&self, thread_id: u64) -> Result<fs::File, std::io::Error> {
    fs::File::open(&self.get_thread_filename(thread_id))
  }

  pub fn get_thread_filename(&self, thread_id: u64) -> PathBuf {
    self.thread_dir.join(format!("{}.thread", thread_id))
  }

  fn get_thread_filename_buffer(&self, thread_id: u64) -> PathBuf {
    self.thread_dir.join(format!("{}.thread.tmp", thread_id))
  }

  pub fn generate_new_file_id(&self) -> FileId {
    let mut rng = rand::thread_rng();
    FileId(self.file_id_generator.sample(&mut rng))
  }

  /// Gets filename to use to store an image in the DB
  pub fn get_filename_column(&self, schema: &str, file_id: FileId, extension: &str) -> String {
    format!("{}/{}.{}", schema, file_id.0, extension)
  }

  pub fn get_filename_system(&self, filename_column: &str) -> PathBuf {
    self.media_dir.join(filename_column)
  }

  /// Gets filename to use to store image thumbnail in the DB
  pub fn get_thumbnail_column(&self, schema: &str, file_id: FileId) -> String {
    format!("{}/{}.{}", schema, file_id.0, THUMBNAIL_FMT)
  }

  /// Gets filename to use to store image thumbnail in filesystem
  pub fn get_thumbnail_system(&self, thumbnail_column: &str) -> PathBuf {
    self.thumb_dir.join(thumbnail_column)
  }

  pub fn get_temporary_frame_filename(&self, file_id: FileId) -> PathBuf {
    self.get_temporary_filename(&format!("{}.{}", file_id.0, THUMBNAIL_FMT))
  }

  pub fn extract_file_id(&self, filename_column: &str) -> Result<FileId, ()> {
    let p1 = filename_column.rfind('/').ok_or(())?;
    let p2 = filename_column.rfind('.').ok_or(())?;
    if p1 + 1 < p2 {
      filename_column[(p1+1)..p2].parse::<u64>()
        .map(|id| FileId(id))
        .map_err(|_| ())
    } else {
      Err(())
    }
  }

  pub fn rename_safe<T>(&self, src: T, dst: T, change: &mut RenameRecord<T>) -> Result<(), RenameError>
  where T: AsRef<OsStr> + AsRef<Path> + Debug {
    if Path::new(&dst).exists() {
      Err(RenameError::TargetExists)
    } else if !Path::new(&src).exists() {
      Err(RenameError::SourceMissing)
    } else {
      match fs::rename(&src, &dst) {
        Ok(()) => {
          *change = Some((src, dst));
          Ok(())
        },
        Err(e) => {
          if !Path::new(&src).exists() {
            log_err(format!("Critical: may have lost file during file rename: (original filename={:?}, updated filename={:?}", src, dst));
          }
          Err(RenameError::IO(e))
        }
      }
    }
  }

  pub fn rename_cleanup<T>(&self, change: &mut RenameRecord<T>)
  where T: AsRef<OsStr> + AsRef<Path> + Debug {
    if let Some((src_filename, dst_filename)) = change {
      fs::rename(&dst_filename, &src_filename).map_err(|_| {
        log_err(format!("Critical: may have lost file during file rename: (original filename={:?}, updated filename={:?})", src_filename, dst_filename));
      }).ok();
    }
  }

  pub fn cleanup_file<T>(&self, file: &T) where T: AsRef<Path> + AsRef<OsStr> + Debug {
    if Path::new(file).exists() {
      match std::fs::remove_file(file) {
        Ok(_) => {}
        Err(e) => {
          log_wrns(&vec![
            format!("Failed to cleanup file: {:?}", file),
            format!("Cause: {:?}", e)
          ]);
        }
      }
    }
  }
}

