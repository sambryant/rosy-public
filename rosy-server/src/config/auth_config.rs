use chrono::Utc;
use http::{header::AUTHORIZATION, HeaderMap, HeaderValue};
use jsonwebtoken::{DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

use crate::{
  constants::JWT_EXPIRE_TIME,
  models::{Id, User},
};

// JSON data encoded as a JWT stored in auth header used for authenticating requests.
#[derive(Serialize, Deserialize)]
struct AuthClaims {
  /// Expiration time of JWT in seconds.
  exp: i64,
  /// Logged in user id in string form.
  id: i32,
  /// Logged in user username.
  username: String,
}

/// Error occuring while checking JWT login token.
#[derive(Debug)]
pub enum AuthError {
  NotLoggedIn,
  InvalidToken,
  /// Indicates that JWT login token's date has expired.
  Expired,
  /// Indicates that the JWT token in header could not be decoded (corrupted).
  Decode(jsonwebtoken::errors::Error),
}

/// Contains configuration info relating to JSON Web Tokens used for authentication.
pub struct AuthConfig {
  encoding_key: EncodingKey,
  decoding_key: DecodingKey<'static>,
  header: Header,
  validation: Validation,
  /// Length of time login tokens are valid before user must re-log in.
  expire_time: chrono::Duration,
}

/// Wrapper around encoded token (JWT) for logged-in user that client must put in auth header.
#[derive(Debug)]
pub struct AuthToken(pub String);

impl AuthConfig {

  pub fn new(secret_key: String) -> Self {
    AuthConfig {
      encoding_key: EncodingKey::from_secret(&secret_key.as_ref()),
      decoding_key: DecodingKey::from_secret(&secret_key.as_ref()).into_static(),
      header: Header::default(),
      validation: Validation::default(),
      expire_time: chrono::Duration::seconds(JWT_EXPIRE_TIME),
    }
  }

  /// Decodes authorization header and returns logged in user id.
  fn authenticate_token(&self, token: AuthToken) -> Result<Id, AuthError> {
    let token = token.0;
    let claims = jsonwebtoken::decode::<AuthClaims>(&token, &self.decoding_key, &self.validation)
      .map_err(|e| AuthError::Decode(e))?
      .claims;
    if claims.exp < Utc::now().timestamp() {
      Err(AuthError::Expired)
    } else {
      Ok(claims.id)
    }
  }

  /// Returns logged in user id on success.
  pub fn authenticate_request(&self, headers: &HeaderMap) -> Result<i32, AuthError> {
    self.authenticate_token(self.extract_token_from_header(headers)?)
  }

  fn extract_token_from_header(&self, headers: &HeaderMap<HeaderValue>) -> Result<AuthToken, AuthError> {
    match headers.get(AUTHORIZATION) {
      None => Err(AuthError::NotLoggedIn),
      Some(value) => match value.to_str() {
        Err(_) => Err(AuthError::InvalidToken),
        Ok(s) => {
          let parts: Vec<&str> = s.split(" ").collect();
          if parts.len() != 2 {
            Err(AuthError::InvalidToken)
          } else if parts[0] != "JWT" {
            Err(AuthError::InvalidToken)
          } else {
            Ok(AuthToken(parts[1].to_string()))
          }
        }
      }
    }
  }

  /// Generates an authorization token for given user that client should put in auth headers.
  pub fn generate_token(&self, user: &User) -> Result<AuthToken, jsonwebtoken::errors::Error> {
    let claims = AuthClaims {
      exp: self.generate_expiration_time(),
      id: user.id,
      username: user.username.clone(),
    };
    jsonwebtoken::encode(&self.header, &claims, &self.encoding_key).map(|token| AuthToken(token))
  }

  /// Generates expiration timestamp for a new JWT in number of seconds since unix epoch.
  fn generate_expiration_time(&self) -> i64 {
    return (Utc::now() + self.expire_time).timestamp()
  }
}
