use diesel::prelude::*;
use crate::error::Error;

pub type DBConn = diesel::pg::PgConnection;

pub type DBType = diesel::pg::Pg;

#[derive(Debug)]
/// Container for error occuring when connection to database cannot be established.
pub struct ConnError(pub diesel::ConnectionError);

/// Contains configuration info relating to the postgres database.
pub struct DBConfig {
  /// Database URL fed into diesel.
  pub database: String,
  /// Database URL w/o the password for server logging.
  database_redacted: String,
  /// Default schema to use when forming new connections.
  pub default_schema: String,
}

impl DBConfig {

  pub fn new(user: String, pass: String, host: String, schema: String) -> DBConfig {
    let database = format!("postgres://{}:{}@{}", user, pass, host);
    let database_redacted = format!("postgres://{}:***@{} ({})", user, host, schema);
    DBConfig {
      database,
      database_redacted,
      default_schema: schema
    }
  }

  pub fn basic_info(&self) -> Vec<String> {
    vec![
      format!("{:<15}: {:?}", "database", self.database_redacted),
    ]
  }

  pub fn make_conn(&self, schema: &str) -> Result<DBConn, ConnError> {
    DBConn::establish(&format!("{}/{}", self.database, schema)).map_err(|e| ConnError(e))
  }

}

impl From<ConnError> for Error {
  fn from(e: ConnError) -> Error {
    Error::db_conn(e)
  }
}