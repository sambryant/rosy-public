use std::str::FromStr;

use crate::prelude::*;

#[doc(hidden)]
pub mod auth_config;
#[doc(hidden)]
pub mod db_config;
#[doc(hidden)]
pub mod file_config;
#[doc(hidden)]
pub mod host_config;

mod env_impl;

#[doc(inline)]
pub use auth_config::*;
#[doc(inline)]
pub use db_config::*;
#[doc(inline)]
pub use file_config::*;
#[doc(inline)]
pub use host_config::*;

pub struct Config {
  /// Indicates that the server is running in development mode.
  pub dev_mode: bool,
  /// Contains configuration data relating to the JWT (JSON web token) secrets.
  pub auth: AuthConfig,
  /// Contains configuration data relating to the database.
  pub db: DBConfig,
  /// Contains configuration data relating to the address/host/port of server.
  pub file: FileConfig,
  /// Contains configuration data relating to the address/host/port of server.
  pub host: HostConfig,
}

/// Trait used to generate configuration objects by reading environmental variables.
pub trait EnvConfig: Sized {

  fn from_env() -> Result<Self, EnvError>;

  fn read_env<T>(var_name: &'static str) -> Result<T, EnvError>
  where T: Sized + FromStr,
        <T as FromStr>::Err: std::fmt::Display {
    std::env::var(var_name)
      .map_err(|_| EnvError::MissingVar(var_name))?
      .parse::<T>()
      .map_err(|e| EnvError::InvalidVar(format!("Env var: '{}' has incorrect type: {}", var_name, e)))
  }

}

/// Error occurring when constructing configuration objects from environment variables.
#[derive(Debug)]
pub enum EnvError {
  /// Indicates an error occurred initalizing filesystem environment.
  FileInit(FileInitError),
  /// Indicates a necessary environmental variable is not set.
  MissingVar(&'static str),
  /// Indicates an environmental variable has an improper value.
  InvalidVar(String),
}

impl Config {
  pub fn show_basic_info(&self) {
    let mut info = vec![
      format!("Starting up new server with config:"),
      format!("{:<15}: {:?}", "mode", if self.dev_mode { "DEV" } else { "PROD" }),
    ];
    info.extend(self.file.basic_info());
    info.extend(self.db.basic_info());
    info.extend(self.host.basic_info());
    log_msgs(&info);
  }
}