use serde::{Deserialize, Serialize};
use std::io::BufReader;
use std::sync::Arc;

use crate::config::{Config, ThreadId};
use crate::error::Error;

#[derive(Serialize, Deserialize, Debug)]
pub struct ThreadStatus {
  pub current: usize,
  pub total: usize,
  pub state: ThreadState,
  pub message: Option<String>
}

#[allow(non_camel_case_types)]
#[derive(Serialize, Deserialize, Debug)]
pub enum ThreadState {
  active, failure, success
}

pub enum GetThreadStatusError {
  NotFound(std::io::Error),
  ReadError(serde_json::Error)
}

#[derive(Debug)]
pub enum UpdateThreadError {
  ThreadCreateError(std::io::Error),
  ThreadWriteError(serde_json::Error),
  ThreadSwapError(std::io::Error)
}

impl ThreadStatus {
  pub fn success() -> ThreadStatus {
    ThreadStatus {
      current: 1, total: 1, state: ThreadState::success, message: None
    }
  }
  pub fn progress(current: usize, total: usize) -> ThreadStatus {
    ThreadStatus {
      current, total, state: ThreadState::active, message: None
    }
  }

  pub fn error(error: Error) -> ThreadStatus {
    error.log();
    ThreadStatus {
      current: 0, total: 1, state: ThreadState::failure, message: Some(error.client_msg)
    }
  }
}

pub fn get_thread_status(cfg: &Arc<Config>, thread_id: ThreadId) -> Result<ThreadStatus, GetThreadStatusError> {
  match cfg.file.open_thread_file(thread_id) {
    Err(e) => Err(GetThreadStatusError::NotFound(e)),
    Ok(file) => serde_json::from_reader(BufReader::new(file))
      .map_err(|e| GetThreadStatusError::ReadError(e)),
  }
}

pub fn create_thread(cfg: &Arc<Config>) -> Result<(ThreadId, ThreadStatus), UpdateThreadError> {
  let thread_id = cfg.file.generate_new_thread_id();
  let status = update_thread(cfg, thread_id, ThreadStatus::progress(0, 1))?;
  Ok((thread_id, status))
}

pub fn update_thread(cfg: &Arc<Config>, thread_id: ThreadId, thread_status: ThreadStatus) -> Result<ThreadStatus, UpdateThreadError> {
  use UpdateThreadError::*;
  let file = cfg.file.create_thread_file_buffer(thread_id).map_err(|e| ThreadCreateError(e))?;

  match serde_json::to_writer(&file, &thread_status) {
    Err(e) => Err(ThreadWriteError(e)),
    Ok(_) => match cfg.file.swap_thread_file_buffer(thread_id) {
      Err(e) => Err(ThreadSwapError(e)),
      Ok(_) => Ok(thread_status)
    }
  }
}

