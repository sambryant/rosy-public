#[macro_use]
extern crate diesel;
use colored::Colorize;
use hyper::{
  service::{make_service_fn, service_fn},
  Body, Method, Request, Response, Server,
};
use std::{convert::Infallible, sync::Arc, time::Instant};
use tokio::signal::unix;

mod api;
mod client_models;
mod command;
mod common;
mod config;
mod constants;
mod content_types;
mod context;
mod dupe_report;
mod error;
mod logger;
mod models;
mod query;
mod schema;
mod threads;
mod trashed_file;
mod train;
mod util;
mod workers;
mod workflow;

use api::responses::{ApiResponse, ErrorResponse};
use config::{Config, EnvConfig};
use context::Context;
use error::Error;

pub type Id = i32;

pub mod prelude {
  use super::*;
  pub use super::Id;
  pub use std::sync::Arc;
  pub use config::{Config, ConnError, DBConn};
  pub use context::Context;
  pub use logger::*;
}
use prelude::*;


fn status_color(status: u16) -> (&'static str, bool) {
  if status < 300 {
    ("green", true)
  } else if status < 400 {
    ("yellow", false)
  } else {
    ("red", false)
  }
}

async fn main_wrapper(cfg: Arc<Config>, req: Request<Body>) -> Result<Response<Body>, Infallible> {
  let method = req.method().to_string();
  let path = req.uri().to_string();

  let t0 = Instant::now();
  let response = main_router(cfg, req).await;
  let elapsed = t0.elapsed().as_millis();

  let code = response.status().as_u16();
  let (color, _is_ok) = status_color(code);
  log_msg(format!("{}: {} {} ({} ms)",
    code.to_string().color(color),
    method,
    path,
    elapsed));
  Ok(response)
}

async fn parse_request(cfg: Arc<Config>, mut req: Request<Body>) -> Result<(Context, hyper::Method, hyper::Uri, hyper::HeaderMap, hyper::body::Bytes), Error> {
  let context = Context::from_headers(cfg, req.headers_mut())?;
  let (parts, raw_body) = req.into_parts();
  let body = hyper::body::to_bytes(raw_body).await
    .map_err(|_| Error::network_error())?;
  Ok((context, parts.method, parts.uri, parts.headers, body))
}

async fn main_router(cfg: Arc<Config>, req: Request<Body>) -> Response<Body> {

  // Extract headers and load body byte stream
  let (context, method, uri, headers, body) = match parse_request(cfg, req).await {
    Err(e) => return ErrorResponse::response(e),
    Ok(r) => r
  };

  // Extract URL path parts for route matching
  let paths: Vec<&str> = uri.path().split('/').collect();

  const GET: &Method = &Method::GET;
  const PUT: &Method = &Method::PUT;
  const DEL: &Method = &Method::DELETE;
  const PST: &Method = &Method::POST;

  match paths.get(1).cloned() {
    Some("auth") => match paths.get(2).cloned() {
      Some("check") => api::auth::check(&context, headers),
      Some("login") => api::auth::login(&context, body),
      _ => ErrorResponse::response(Error::not_found()),
    },
    Some("api") => match paths.get(2).cloned() {
      Some("admin") => match (&method, paths.get(3).cloned(), paths.get(4).cloned()) {
        (GET, Some("thread_status"), Some(id)) => api::admin::thread_status(context.config, id),
        (PUT, Some("compute_metadata"), _) => api::admin::calculate_metadata(&context),
        (PUT, Some("fix_sequences"), _) => api::admin::fix_sequences(&context),
        (PST, Some("generate_dupe_report"), _) => api::admin::generate_dupe_report(&context),
        (PST, Some("generate_dataset"), _) => api::admin::generate_dataset(&context, body),
        (PST, Some("generate_tag_dataset"), _) => api::admin::generate_tag_dataset(&context, body),
        (PST, Some("generate_feature_dataset"), _) => api::admin::generate_feature_dataset(&context, body),
        (PUT, Some("merge_duplicates"), _) => api::admin::merge_duplicates(&context),
        (PUT, Some("recompute_filesizes"), _) => api::admin::recompute_filesizes(&context),
        (PUT, Some("regenerate_thumbnails"), _) => api::admin::regenerate_thumbnails(&context),
        (PUT, Some("reindex_images"), _) => api::admin::reindex_images(&context),
        (PUT, Some("reset_files"), _) => api::admin::reset_files(&context),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("artist") => match (&method, paths.get(3).cloned()) {
        (DEL, Some(id)) => api::artist::delete(&context, id),
        (PST, None) => api::artist::create(&context, body),
        (PUT, None) => api::artist::update(&context, body),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("external") => match (&method, paths.get(3).cloned()) {
        (_, Some("import_image")) => api::external::import_image(&context, body).await,
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("feature") => match (&method, paths.get(3).cloned()) {
        (DEL, Some(id)) => api::feature::delete(&context, id),
        (PST, None) => api::feature::create(&context, body),
        (PUT, None) => api::feature::update(&context, body),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("group") => match (&method, paths.get(3).cloned(), paths.get(4).cloned()) {
        (DEL, Some("permadelete"), Some(id)) => api::group::permadelete(&context, id),
        (PUT, Some("restore"), Some(id)) => api::group::restore(&context, id),
        (DEL, Some(id), None) => api::group::delete(&context, id),
        (PST, None, None) => api::group::create(&context, body),
        (PUT, None, None) => api::group::update(&context, body),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("image") => match (&method, paths.get(3).cloned(), paths.get(4).cloned()) {
        (DEL, Some("permadelete"), Some(id)) => api::image::permadelete(&context, id),
        (PUT, Some("restore"), Some(id)) => api::image::restore(&context, id),
        (PST, Some("rate"), None) => api::image::rate(&context, body),
        (PUT, Some("mark_dupes"), None) => api::image::mark_dupes(&context, body),
        (PUT, Some("unmark_dupes"), None) => api::image::unmark_dupes(&context, body),
        (GET, Some("face_suggest"), Some(id)) => api::image::face_suggest(&context, id),
        (DEL, Some(id), None) => api::image::delete(&context, id),
        (GET, Some(id), None) => api::image::get(&context, id),
        (PUT, None, None) => api::image::update(&context, body),
        (PST, None, None) => api::image::upload(&context, headers, body).await,
        _ => ErrorResponse::response(Error::not_found()),
      }
      Some("image_feature") => match (&method, paths.get(3).cloned()) {
        (DEL, Some(id)) => api::image_feature::delete(&context, id),
        (PST, None) => api::image_feature::create(&context, body),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("metadata") => match (&method, paths.get(3).cloned()) {
        (GET, None) => api::metadata::get(&context),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("name") => match (&method, paths.get(3).cloned()) {
        (DEL, Some(id)) => api::name::delete(&context, id),
        (PST, None) => api::name::create(&context, body),
        (PUT, None) => api::name::update(&context, body),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("query") => match (&method, paths.get(3).cloned(), paths.get(4).cloned()) {
        (GET, Some("marked_dupes"), Some(id)) => api::query::marked_dupes(&context, id),
        (GET, Some("artists"), None) => api::query::artists(&context, uri),
        (GET, Some("deleted"), None) => api::query::deleted(&context, uri),
        (GET, Some("dupe_candidates"), None) => api::query::dupe_candidates(&context, uri),
        (GET, Some("eigendata"), None) => api::query::eigendata(&context, uri),
        (GET, Some("face_matches"), Some(id)) => api::query::face_matches(&context, id.to_string(), uri),
        (GET, Some("groups"), None) => api::query::groups(&context, uri),
        (GET, Some("images"), None) => api::query::images(&context, uri),
        (GET, Some("names"), None) => api::query::names(&context, uri),
        (GET, Some("tags"), None) => api::query::tags(&context, uri),
        _ => ErrorResponse::response(Error::not_found()),
      },
      Some("tag") => match (&method, paths.get(3).cloned()) {
        (DEL, Some(id)) => api::tag::delete(&context, id),
        (PST, None) => api::tag::create(&context, body),
        (PUT, None) => api::tag::update(&context, body),
        _ => ErrorResponse::response(Error::not_found()),
      },
      _ => ErrorResponse::response(Error::not_found()),
    },
    _ => ErrorResponse::response(Error::not_found()),
  }
}

async fn install_shutdown_signals() {
  unix::signal(unix::SignalKind::terminate())
    .expect("Failed to install SIGTERM listener")
    .recv()
    .await
    .expect("Failed to install SIGTERM listener")
}

#[tokio::main]
async fn main() {
  match Config::from_env() {
    Err(s) => {
      log_err(format!("Failed to start server: {:?}", s));
    },
    Ok(c) => {
      c.show_basic_info();
      let cfg = Arc::new(c);
      let addr = cfg.host.listen_addr.clone();
      let make_svc = make_service_fn(move |_| {
        let cfg = cfg.clone();
        async move { Ok::<_, Infallible>(service_fn(move |req| main_wrapper(cfg.clone(), req))) }
      });

      let server = Server::bind(&addr).serve(make_svc)
        .with_graceful_shutdown(install_shutdown_signals());

      if let Err(e) = server.await {
        eprintln!("Fatal Server Error: {}", e);
      }
    }
  }
}
