use diesel;
use diesel_derive_enum::DbEnum;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, DbEnum, Deserialize, Eq, Hash, Serialize, PartialEq)]
pub enum DupeType {
  Exact, Most, Partial, None
}

table! {
  use super::DupeTypeMapping;
  use diesel::sql_types::Integer;
  dupe (img1_id, img2_id) {
    img1_id -> Integer,
    img2_id -> Integer,
    dupe_type -> DupeTypeMapping,
  }
}

table! {
  user (id) {
    id -> Integer,
    username -> VarChar,
    password -> VarChar,
  }
}

table! {
  image_index (id) {
    id -> Integer,
    encodings -> Nullable<Binary>,
    locations -> Nullable<Binary>,
    filehash -> Nullable<Binary>,
  }
}

table! {
  image (id) {
    id -> Integer,
    filename -> VarChar,
    thumbnail -> VarChar,
    created_time -> BigInt,
    filesize -> Integer,
    deleted -> Bool,
    hidden -> Bool,
    favorite -> Bool,
    is_video -> Bool,
    marked -> Bool,
    processed -> Bool,
    processed_time -> Nullable<BigInt>,
    source -> Nullable<VarChar>,
    source_page -> Nullable<VarChar>,
    artist_id -> Nullable<Integer>,
    group_id -> Nullable<Integer>,
    rating_count -> Integer,
    rating_average -> Nullable<Float>,
    length -> Nullable<Float>,
  }
}

table! {
  image_name (img_id, nme_id) {
    img_id -> Integer,
    nme_id -> Integer,
  }
}

table! {
  image_tag (img_id, tag_id) {
    img_id -> Integer,
    tag_id -> Integer,
  }
}

table! {
  tag (id) {
    id -> Integer,
    value -> Varchar,
    index -> Nullable<Integer>,
    created_time -> BigInt,
  }
}

table! {
  name (id) {
    id -> Integer,
    value -> Varchar,
    index -> Nullable<Integer>,
    created_time -> BigInt,
  }
}

table! {
  rating (id) {
    id -> Integer,
    value -> Integer,
    time -> BigInt,
    image_id -> Integer,
  }
}

table! {
  feature (id) {
    id -> Integer,
    created_time -> BigInt,
    value -> VarChar,
  }
}

table! {
  image_feature (id) {
    id -> Integer,
    image_id -> Integer,
    feature_id -> Integer,
    x1 -> Float,
    x2 -> Float,
    y1 -> Float,
    y2 -> Float,
  }
}


table! {
  artist (id) {
    id -> Integer,
    created_time -> BigInt,
    deleted -> Bool,
    name -> VarChar,
  }
}

table! {
  group (id) {
    id -> Integer,
    created_time -> BigInt,
    title -> VarChar,
    description -> Nullable<VarChar>,
    deleted -> Bool,
    hidden -> Bool,
  }
}

table! {
  group_name (grp_id, nme_id) {
    grp_id -> Integer,
    nme_id -> Integer,
  }
}

table! {
  group_tag (grp_id, tag_id) {
    grp_id -> Integer,
    tag_id -> Integer,
  }
}

table! {
  metadata_eigendata(id) {
    id -> Integer,
    eigenvalue -> Float,
    eigenvector -> Array<Float>,
  }
}

table! {
  name_metadata(id) {
    id -> Integer,
    evec_coefs -> Array<Float>,
    high_names -> Array<Integer>,
    high_name_scores -> Array<Float>,
    high_tags -> Array<Integer>,
    high_tag_scores -> Array<Float>,
    low_names -> Array<Integer>,
    low_name_scores -> Array<Float>,
    low_tags -> Array<Integer>,
    low_tag_scores -> Array<Float>,
  }
}

table! {
  tag_metadata(id) {
    id -> Integer,
    evec_coefs -> Array<Float>,
    high_names -> Array<Integer>,
    high_name_scores -> Array<Float>,
    high_tags -> Array<Integer>,
    high_tag_scores -> Array<Float>,
    low_names -> Array<Integer>,
    low_name_scores -> Array<Float>,
    low_tags -> Array<Integer>,
    low_tag_scores -> Array<Float>,
  }
}

joinable!(rating -> image (image_id));
joinable!(image_index -> image (id));
joinable!(image -> group (group_id));
joinable!(image -> artist (artist_id));
joinable!(group_name -> group (grp_id));
joinable!(group_name -> name (nme_id));
joinable!(group_tag -> group (grp_id));
joinable!(group_tag -> tag (tag_id));
joinable!(image_feature -> image (image_id));
joinable!(image_feature -> feature (feature_id));
joinable!(image_name -> image (img_id));
joinable!(image_name -> name (nme_id));
joinable!(image_tag -> image (img_id));
joinable!(image_tag -> tag (tag_id));
joinable!(dupe -> image (img1_id));
allow_tables_to_appear_in_same_query!(
  artist,
  dupe,
  feature,
  group,
  group_name,
  group_tag,
  image,
  image_feature,
  image_index,
  image_name,
  image_tag,
  name,
  rating,
  tag,
  user,
);