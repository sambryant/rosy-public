use http::Uri;
use std::path::Path;
use std::ffi::OsStr;

use crate::api::search_request::QueryParser;

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum ContentType {
  Image,
  Video,
}

impl ContentType {

  pub fn header_to_ext(content_type_header: &str) -> Option<&'static str> {
    Some(match content_type_header {
      "image/bmp" => "bmp",
      "image/gif" => "gif",
      "image/jpeg" => "jpg",
      "image/jpg" => "jpg",
      "image/png" => "png",
      "image/webp" => "webp",
      "video/mp4" => "mp4",
      "video/webm" => "webm",
      _ => return None
    })
  }

  pub fn get_ext_from_uri(uri: &Uri) -> Option<String> {
    match ContentType::get_ext_from_filename(uri.path()) {
      Some(ext) => Some(ext),
      None => match uri.query() {
        Some(query) => {
          // Look for the file extension type in query parameters (twitter does this)
          let mut qp = QueryParser::from_query(query);
          match qp.parse::<String>("format").get() {
            Ok(Some(ext)) => Some(ext),
            _ => None
          }
        },
        None => None
      }
    }
  }

  pub fn get_ext_from_filename<T>(filename: T) -> Option<String>
  where T: AsRef<Path> + AsRef<OsStr> {
    Path::new(&filename)
      .extension()
      .and_then(OsStr::to_str)
      .map(|s| s.to_lowercase())
  }

  pub fn get(ext: &str) -> Option<ContentType> {
    Some(match ext {
      "bmp" => ContentType::Image,
      "gif" => ContentType::Image,
      "jpg" => ContentType::Image,
      "jpeg" => ContentType::Image,
      "png" => ContentType::Image,
      "webp" => ContentType::Image,
      "mp4" => ContentType::Video,
      "webm" => ContentType::Video,
      "gifv" => ContentType::Video,
      _ => return None
    })
  }
}
