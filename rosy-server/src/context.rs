use hyper::{
  HeaderMap,
  header::HeaderValue,
};
use std::{
  path::{PathBuf},
  sync::Arc,
};

use crate::prelude::*;
use crate::{
  constants::{SCHEMA_HEADER_KEY, USER_HEADER_KEY},
  error::Error,
};

#[derive(Clone)]
pub struct Context {
  pub config: Arc<Config>,
  pub user_id: Option<i32>,
  pub schema: String
}

pub use crate::config::{FileId, ContentFile, ThumbnailFile};

impl Context {

  /// Opens a new database connection with schema implied by header contexts.
  pub fn make_conn(&self) -> Result<DBConn, ConnError> {
    self.config.db.make_conn(&self.schema)
  }

  /// Gets filename to use to store an image in the DB.
  pub fn get_filename_column(&self, file_id: FileId, extension: &str) -> String {
    self.config.file.get_filename_column(&self.schema, file_id, extension)
  }

  /// Gets filename to use to store image thumbnail in the DB.
  pub fn get_thumbnail_column(&self, file_id: FileId) -> String {
    self.config.file.get_thumbnail_column(&self.schema, file_id)
  }

  /// Gets name of directory where files are trashed to.
  pub fn get_trash_dir(&self) -> Result<PathBuf, std::io::Error> {
    self.config.file.get_trash_dir(&self.schema)
  }

}

impl Context {
  pub fn from_headers(config: Arc<Config>, headers: &mut HeaderMap<HeaderValue>) -> Result<Context, Error> {
    let user_id = match headers.remove(USER_HEADER_KEY) {
      None => None,
      Some(v) => match v.to_str() {
        Err(e) => return Err(Error::request_error("Bad user id header").attach(e)),
        Ok(s) => match s.parse::<i32>() {
          Err(e) => return Err(Error::request_error("Bad user id header").attach(e)),
          Ok(user_id) => Some(user_id)
        }
      }
    };
    let schema = match headers.remove(SCHEMA_HEADER_KEY) {
      None => String::from(&config.db.default_schema),
      Some(v) => match v.to_str() {
        Err(e) => return Err(Error::request_error("Bad schema header").attach(e)),
        Ok(schema) => schema.to_string()
      }
    };
    Ok(Context { config, user_id, schema })
  }
}
