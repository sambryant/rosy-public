use diesel::{deserialize::Queryable, Identifiable};
use serde::{Deserialize, Serialize};
use std::hash::{Hash, Hasher};

use crate::{
  config::DBType,
  schema::*,
};
use crate::prelude::*;

pub type FileHash = Vec<u8>;

pub use crate::Id;

pub type ArtId = Id;

pub type GrpId = Id;

pub type ImgId = Id;

pub type NmeId = Id;

pub type TagId = Id;

pub type Index = usize;

pub type FaceEncoding = [f64; 128];

pub use crate::schema::DupeType;

#[derive(Associations, Deserialize, Hash, Identifiable, Insertable, Queryable, Serialize)]
#[primary_key("img1_id", "img2_id")]
#[belongs_to(Image, foreign_key="img1_id")]
#[table_name = "dupe"]
pub struct Dupe {
  pub img1_id: ImgId,
  pub img2_id: ImgId,
  pub dupe_type: DupeType,
}

#[derive(Queryable)]
pub struct User {
  pub id: i32,
  pub username: String,
  pub password: String,
}

pub enum ContentMeta {
  Name(Name),
  Tag(Tag),
}

#[derive(Associations, Identifiable, Queryable, Clone)]
#[table_name = "image"]
#[belongs_to(Artist)]
#[belongs_to(Group)]
pub struct Image {
  pub id: ImgId,
  pub filename: String,
  pub thumbnail: String,
  pub created_time: i64,
  pub filesize: i32,
  pub deleted: bool,
  pub hidden: bool,
  pub favorite: bool,
  pub is_video: bool,
  pub marked: bool,
  pub processed: bool,
  pub processed_time: Option<i64>,
  pub source: Option<String>,
  pub source_page: Option<String>,
  pub artist_id: Option<ArtId>,
  pub group_id: Option<GrpId>,
  pub rating_count: i32,
  pub rating_average: Option<f32>,
  pub length: Option<f32>,
}

#[derive(Deserialize, Insertable)]
#[table_name = "image"]
pub struct NewImage {
  pub filename: String,
  pub thumbnail: String,
  pub created_time: i64,
  pub filesize: i32,
  pub hidden: bool,
  pub favorite: bool,
  pub is_video: bool,
  pub processed: bool,
  pub source: Option<String>,
  pub source_page: Option<String>,
  pub rating_count: i32,
  pub length: Option<f32>,
}

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize, Insertable)]
#[primary_key("img_id", "tag_id")]
#[table_name = "image_tag"]
#[belongs_to(Tag, foreign_key="tag_id")]
#[belongs_to(Image, foreign_key="img_id")]
pub struct ImageTag {
  pub img_id: ImgId,
  pub tag_id: TagId,
}

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize, Insertable)]
#[primary_key("img_id", "nme_id")]
#[table_name = "image_name"]
#[belongs_to(Name, foreign_key="nme_id")]
#[belongs_to(Image, foreign_key="img_id")]
pub struct ImageName {
  pub img_id: ImgId,
  pub nme_id: NmeId,
}

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize)]
#[table_name = "rating"]
#[belongs_to(Image)]
pub struct Rating {
  pub id: i32,
  pub value: i32,
  pub time: i64,
  pub image_id: ImgId,
}

#[derive(Insertable)]
#[table_name = "rating"]
pub struct NewRating {
  pub value: i32,
  pub time: i64,
  pub image_id: ImgId,
}

#[derive(Associations, Debug, Identifiable, Queryable, Serialize, Deserialize, Eq, Clone)]
#[table_name = "feature"]
pub struct Feature {
  pub id: i32,
  pub created_time: i64,
  pub value: String,
}

#[derive(Insertable)]
#[table_name = "feature"]
pub struct NewFeature {
  pub created_time: i64,
  pub value: String,
}

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize, Clone)]
#[table_name = "image_feature"]
#[belongs_to(Feature, foreign_key="feature_id")]
#[belongs_to(Image, foreign_key="image_id")]
pub struct ImageFeature {
  pub id: i32,
  pub image_id: i32,
  pub feature_id: i32,
  pub x1: f32,
  pub x2: f32,
  pub y1: f32,
  pub y2: f32,
}

#[derive(Insertable)]
#[table_name = "image_feature"]
pub struct NewImageFeature {
  pub image_id: i32,
  pub feature_id: i32,
  pub x1: f32,
  pub x2: f32,
  pub y1: f32,
  pub y2: f32,
}

#[derive(Identifiable, Queryable, Serialize, Deserialize, Insertable)]
#[table_name = "metadata_eigendata"]
pub struct MetadataEigendata{
  pub id: i32,
  pub eigenvalue: f32,
  pub eigenvector: Vec<f32>
}

#[derive(Associations, Debug, Identifiable, Queryable, Serialize, Deserialize, Eq, Clone)]
#[table_name = "name"]
pub struct Name {
  pub id: NmeId,
  pub value: String,
  pub index: Option<i32>,
  pub created_time: i64,
}

/// Contains raw data required to construct `ClientNameDetails`
/// Order is: (Name, group count, (image count, score))
pub type NameDetailsData = (Name, usize, (usize, f32), Option<NameMetadata>);

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize, Insertable)]
#[table_name = "name_metadata"]
#[belongs_to(Name, foreign_key="id")]
pub struct NameMetadata {
  pub id: NmeId,
  pub evec_coefs: Vec<f32>,
  pub high_names: Vec<NmeId>,
  pub high_name_scores: Vec<f32>,
  pub high_tags: Vec<TagId>,
  pub high_tag_scores: Vec<f32>,
  pub low_names: Vec<NmeId>,
  pub low_name_scores: Vec<f32>,
  pub low_tags: Vec<TagId>,
  pub low_tag_scores: Vec<f32>,
}

#[derive(Associations, Debug, Identifiable, Queryable, Serialize, Deserialize, Eq, Clone)]
#[table_name = "tag"]
pub struct Tag {
  pub id: TagId,
  pub value: String,
  pub index: Option<i32>,
  pub created_time: i64,
}

/// Contains raw data required to construct `ClientTagDetails`
/// Order is: (Tag, group count, (image count, score))
pub type TagDetailsData = (Tag, usize, (usize, f32), Option<TagMetadata>);

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize, Insertable)]
#[table_name = "tag_metadata"]
#[belongs_to(Tag, foreign_key="id")]
pub struct TagMetadata {
  pub id: TagId,
  pub evec_coefs: Vec<f32>,
  pub high_names: Vec<NmeId>,
  pub high_name_scores: Vec<f32>,
  pub high_tags: Vec<TagId>,
  pub high_tag_scores: Vec<f32>,
  pub low_names: Vec<NmeId>,
  pub low_name_scores: Vec<f32>,
  pub low_tags: Vec<TagId>,
  pub low_tag_scores: Vec<f32>,
}

#[derive(Insertable)]
#[table_name = "name"]
pub struct NewName {
  pub created_time: i64,
  pub value: String
}

#[derive(Insertable)]
#[table_name = "tag"]
pub struct NewTag {
  pub created_time: i64,
  pub value: String
}

/// This model uses a custom serialization/deserialization procedure
#[derive(Associations, Identifiable)]
#[table_name = "image_index"]
#[belongs_to(Image, foreign_key = "id")]
pub struct ImageIndex {
  pub id: ImgId,
  pub encodings: Option<Vec<FaceEncoding>>,
  pub locations: Option<Vec<[u32; 4]>>,
  pub filehash: Option<FileHash>,
}

#[derive(Identifiable, Queryable)]
#[table_name = "artist"]
pub struct Artist {
  pub id: ArtId,
  pub created_time: i64,
  pub deleted: bool,
  pub name: String,
}

/// Contains raw data required to construct `ClientArtistDetails`
/// Order is: (Artist, (image count, score))
pub type ArtistDetailsData = (Artist, (usize, f32));

#[derive(Insertable)]
#[table_name = "artist"]
pub struct NewArtist {
  pub created_time: i64,
  pub name: String,
}

#[derive(Identifiable, Queryable, Serialize, Deserialize)]
#[table_name = "group"]
pub struct Group {
  pub id: GrpId,
  pub created_time: i64,
  pub title: String,
  pub description: Option<String>,
  pub deleted: bool,
  pub hidden: bool,
}

#[derive(Insertable)]
#[table_name = "group"]
pub struct NewGroup {
  pub created_time: i64,
  pub title: String,
}

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize, Insertable)]
#[primary_key("grp_id", "nme_id")]
#[table_name = "group_name"]
#[belongs_to(Name, foreign_key="nme_id")]
#[belongs_to(Group, foreign_key="grp_id")]
pub struct GroupName {
  pub grp_id: GrpId,
  pub nme_id: NmeId,
}

#[derive(Associations, Identifiable, Queryable, Serialize, Deserialize, Insertable)]
#[primary_key("grp_id", "tag_id")]
#[table_name = "group_tag"]
#[belongs_to(Tag, foreign_key="tag_id")]
#[belongs_to(Group, foreign_key="grp_id")]
pub struct GroupTag {
  pub grp_id: GrpId,
  pub tag_id: TagId,
}

pub type ImageData = (Image, Vec<Name>, Vec<Tag>, Vec<(ImageFeature, Feature)>, Vec<(ImgId, DupeType)>);

pub type GroupData = (Group, Vec<Name>, Vec<Tag>, Vec<ImageData>);

pub trait Association: From<(Id, Id)> {}

impl From<Name> for String {
  fn from(name: Name) -> String {
    name.value
  }
}

impl From<Tag> for String {
  fn from(tag: Tag) -> String {
    tag.value
  }
}

impl PartialEq for Dupe {
  fn eq(&self, other: &Self) -> bool {
    self.img1_id == other.img1_id && self.img2_id == self.img2_id && self.dupe_type == other.dupe_type
  }
}

impl Eq for Dupe {}

impl From<(GrpId, NmeId)> for GroupName {
  fn from((grp_id, nme_id): (GrpId, NmeId)) -> GroupName {
    GroupName { grp_id, nme_id }
  }
}

impl From<(GrpId, TagId)> for GroupTag {
  fn from((grp_id, tag_id): (GrpId, TagId)) -> GroupTag {
    GroupTag { grp_id, tag_id }
  }
}

impl From<(ImgId, NmeId)> for ImageName {
  fn from((img_id, nme_id): (ImgId, NmeId)) -> ImageName {
    ImageName { img_id, nme_id }
  }
}

impl From<(ImgId, TagId)> for ImageTag {
  fn from((img_id, tag_id): (ImgId, TagId)) -> ImageTag {
    ImageTag { img_id, tag_id }
  }
}

impl Association for GroupName {}
impl Association for GroupTag {}
impl Association for ImageName {}
impl Association for ImageTag {}

impl PartialEq for Feature {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl Hash for Feature {
  fn hash<H: Hasher>(&self, state: &mut H) {
    self.id.hash(state);
  }
}

impl Eq for ImageFeature {}

impl PartialEq for ImageFeature {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl Hash for ImageFeature {
  fn hash<H: Hasher>(&self, state: &mut H) {
    self.id.hash(state);
  }
}

impl PartialEq for Name {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl Hash for Name {
  fn hash<H: Hasher>(&self, state: &mut H) {
    self.id.hash(state);
  }
}

impl PartialEq for Tag {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl Hash for Tag {
  fn hash<H: Hasher>(&self, state: &mut H) {
    self.id.hash(state);
  }
}

impl Queryable<image_index::SqlType, DBType> for ImageIndex {
  type Row = (ImgId, Option<Vec<u8>>, Option<Vec<u8>>, Option<Vec<u8>>);

  fn build(row: Self::Row) -> Self {
    let id = row.0;
    // This method is not allowed to fail so we still need to return something on failure
    // TODO: Consider setting some field like "corrupt" when failure occurs and locking fields
    // behind accessor methods.
    match ImageIndex::deserialize(&row) {
      Ok(result) => result,
      Err(msg) => {
        log_err(format!("Image index ({}) deserialization failed: {}", id, msg));
        ImageIndex {
          id,
          encodings: None,
          locations: None,
          filehash: None,
        }
      }
    }
  }
}

impl ImageIndex {
  pub fn distance(a: &FaceEncoding, b: &FaceEncoding) -> f64 {
    let mut sum = 0.0;
    for i in 0..128 {
      sum += (a[i] - b[i]) * (a[i] - b[i]);
    }
    sum.sqrt()
  }

  pub fn is_okay(&self) -> bool {
    self.encodings.is_some()
  }

  pub fn has_some_encoding(&self) -> bool {
    self.is_okay() && self.encodings.as_ref().unwrap().len() > 0
  }

  pub fn has_one_face_encoding(&self) -> Option<FaceEncoding> {
    if let Some(encodings) = &self.encodings {
      if encodings.len() == 1 {
        Some(encodings[0].clone())
      } else {
        None
      }
    } else {
      None
    }
  }

  /**
   * This uses `NaN` as the None type to avoid having what is essentially an Option within an Option
   */
  pub fn get_distance(&self, other: &ImageIndex) -> f64 {
    match (self.encodings.as_ref(), other.encodings.as_ref()) {
      (Some(encs1), Some(encs2)) => {
        let mut min = f64::NAN;
        for enc1 in encs1.iter() {
          for enc2 in encs2.iter() {
            min = f64::min(min, ImageIndex::distance(&enc1, &enc2));
          }
        }
        min
      }
      _ => f64::NAN,
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // DESERIALIZATION STUFF
  //////////////////////////////////////////////////////////////////////////////

  fn bytes_to_u32_be(array: &[u8]) -> u32 {
    u32::from_be_bytes([array[0], array[1], array[2], array[3]])
  }

  fn bytes_to_f64_be(array: &[u8]) -> f64 {
    f64::from_be_bytes([
      array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7],
    ])
  }

  fn bytes_to_encodings_be(array: &[u8]) -> Result<Vec<FaceEncoding>, String> {
    if array.len() % (8 * 128) == 0 {
      let num_values = array.len() / 8;
      let num_faces = num_values / 128;

      // preallocate for data locality
      let mut result: Vec<FaceEncoding> = (0..num_faces)
        .into_iter()
        .map(|_| [0.0 as f64; 128])
        .collect();

      for i in 0..num_faces {
        for j in 0..128 {
          result[i][j] = ImageIndex::bytes_to_f64_be(&array[8 * (i * 128 + j)..]);
        }
      }
      Ok(result)
    } else if array.len() % 8 != 0 {
      Err("Byte array was not a multiple of the primitive size".to_string())
    } else {
      Err("Encoding value array was not a multiple of 128".to_string())
    }
  }

  fn bytes_to_locations_be(array: &[u8]) -> Result<Vec<[u32; 4]>, String> {
    if array.len() % (4 * 4) == 0 {
      let num_values = array.len() / 4;
      let num_faces = num_values / 4;

      // preallocate for data locality
      let mut result: Vec<[u32; 4]> = (0..num_faces).into_iter().map(|_| [0; 4]).collect();

      for i in 0..num_faces {
        for j in 0..4 {
          result[i][j] = ImageIndex::bytes_to_u32_be(&array[4 * (i * 4 + j)..]);
        }
      }
      Ok(result)
    } else if array.len() % 4 != 0 {
      Err("Byte array was not a multiple of the primitive size".to_string())
    } else {
      Err("Location value array was not a multiple of 4".to_string())
    }
  }

  fn deserialize(
    row: &(ImgId, Option<Vec<u8>>, Option<Vec<u8>>, Option<Vec<u8>>),
  ) -> Result<Self, String> {
    let encodings = match &row.1 {
      Some(encodings_data) => Some(ImageIndex::bytes_to_encodings_be(&encodings_data)?),
      None => None,
    };
    let locations = match &row.2 {
      Some(locations_data) => Some(ImageIndex::bytes_to_locations_be(&locations_data)?),
      None => None,
    };
    Ok(ImageIndex {
      id: row.0,
      encodings,
      locations,
      filehash: row.3.clone(),
    })
  }
}

impl Rating {
  pub fn average_value(ratings: &Vec<i32>) -> Option<f32> {
    if ratings.len() == 0 {
      None
    } else {
      let mut sum = 0;
      for r in ratings.iter() {
        sum += r;
      }
      Some((sum as f32) / (ratings.len() as f32))
    }
  }
  pub fn average(ratings: &Vec<Rating>) -> Option<f32> {
    Rating::average_value(&ratings.iter().map(|s| s.value).collect())
  }
}
