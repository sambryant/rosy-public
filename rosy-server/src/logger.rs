use chrono::Local;
use std::fmt::Display;

enum Level {
  Log, Err, Wrn
}

impl Level {
  fn formatted(&self) -> &'static str {
    match self {
      Level::Log => "LOG",
      Level::Err => "ERR",
      Level::Wrn => "WRN"
    }
  }
}

pub fn log_err<T>(message: T)
where T: Display {
  log_message(Level::Err, message)
}

pub fn log_errs<T>(messages: &Vec<T>)
where T: Display {
  log_messages(Level::Err, messages)
}

pub fn log_msg<T>(message: T)
where T: Display {
  log_message(Level::Log, message)
}

pub fn log_msgs<T>(messages: &Vec<T>)
where T: Display {
  log_messages(Level::Log, messages)
}

pub fn log_wrn<T>(message: T)
where T: Display {
  log_message(Level::Wrn, message)
}

pub fn log_wrns<T>(messages: &Vec<T>)
where T: Display {
  log_messages(Level::Wrn, messages)
}

fn log_message<T>(level: Level, message: T)
where T: Display {
  println!("{} [{}] {}", Local::now().format("%Y %T"), level.formatted(), message);
}

fn log_messages<T>(level: Level, messages: &Vec<T>)
where T: Display {
  if messages.len() == 1 {
    log_message(level, &messages[0])
  } else {
    let prefix = format!("{} [{}] ", Local::now().format("%Y %T"), level.formatted());
    let spaces = prefix.len();
    println!("{}{}", &prefix, messages[0]);
    for msg in messages[1..].iter() {
      println!("{:width$}{}", " ", msg, width=spaces)
    }
  }
}
