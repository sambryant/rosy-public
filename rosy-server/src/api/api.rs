use hyper::{Body, Response, HeaderMap, body::Bytes, Uri};
use std::sync::Arc;

use crate::prelude::*;
use crate::{
  common::cov_into,
  error::Error,
  workflow
};

use super::search_request::QueryRequest;
use super::requests::*;
use super::responses::*;

/**
 * Endpoints associated with admin tasks, although currently usable by all users.
 *
 * Currently this provides a bunch of "admin" tasks which can take a long time. Each of these tasks
 * executes asynchronously so they respond with a thread ID. The client can get the status of the
 * task by calling `thread_status` with the returned thread ID.
 */
pub mod admin {
  use super::*;
  use crate::threads;

  pub fn fix_sequences(cxt: &Context) -> Response<Body> {
    match workflow::fix_sequences(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(_) => EmptyResponse::response(()),
    }
  }

  pub fn generate_dataset(cxt: &Context, body: Bytes) -> Response<Body> {
    match GenerateDatasetRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(req) => match workflow::start_generate_dataset(cxt, req) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => ThreadStatusResponse::response(data)
      }
    }
  }

  pub fn generate_feature_dataset(cxt: &Context, body: Bytes) -> Response<Body> {
    match GenerateFeatureDatasetRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(req) => match workflow::start_generate_feature_dataset(cxt, req) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => ThreadStatusResponse::response(data)
      }
    }
  }

  pub fn generate_tag_dataset(cxt: &Context, body: Bytes) -> Response<Body> {
    match GenerateTagDatasetRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(req) => match workflow::start_generate_tag_dataset(cxt, req) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => ThreadStatusResponse::response(data)
      }
    }
  }

  pub fn calculate_metadata(cxt: &Context) -> Response<Body> {
    match workflow::calculate_metadata(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(_) => EmptyResponse::response(()),
    }
  }

  pub fn generate_dupe_report(cxt: &Context) -> Response<Body> {
    match workflow::generate_dupe_report(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(_) => EmptyResponse::response(()),
    }
  }

  pub fn thread_status(cfg: Arc<Config>, thread_id_str: &str) -> Response<Body> {
    use threads::GetThreadStatusError::*;
    match parse_id64(thread_id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(thread_id) => match threads::get_thread_status(&cfg, thread_id) {
        Ok(status) => ThreadStatusResponse::response((thread_id, status)),
        Err(error) => ErrorResponse::response(match error {
          NotFound(e) => Error::not_found().attach(e),
          ReadError(e) => Error::unknown().attach(e),
        }),
      }
    }
  }

  pub fn merge_duplicates(cxt: &Context) -> Response<Body> {
    match workflow::start_merge_duplicates(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(data) => ThreadStatusResponse::response(data)
    }
  }

  pub fn recompute_filesizes(cxt: &Context) -> Response<Body> {
    match workflow::start_recompute_filesizes(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(data) => ThreadStatusResponse::response(data),
    }
  }

  pub fn regenerate_thumbnails(cxt: &Context) -> Response<Body> {
    match workflow::start_regenerate_thumbnails(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(data) => ThreadStatusResponse::response(data),
    }
  }

  pub fn reindex_images(cxt: &Context) -> Response<Body> {
    match workflow::start_reindex_images(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(data) => ThreadStatusResponse::response(data),
    }
  }

  pub fn reset_files(cxt: &Context) -> Response<Body> {
    match workflow::start_reset_files(cxt) {
      Err(e) => ErrorResponse::response(e),
      Ok(data) => ThreadStatusResponse::response(data),
    }
  }

}

/**
 * Endpoints associated with CRUD tasks on artist objects.
 */
pub mod artist {
  use super::*;

  pub fn create(cxt: &Context, body: Bytes) -> Response<Body> {
    match CreateArtistRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::create_artist(cxt, r.value, r.imageIds) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => CreateArtistResponse::response(data.into()),
      }
    }
  }
  pub fn delete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::delete_artist(cxt, id) {
        Ok(_) => DeleteArtistResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }
  pub fn update(cxt: &Context, body: Bytes) -> Response<Body> {
    // TODO: Return refreshed artist on error so data stays in sync.
    match UpdateArtistRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(req) => match workflow::update_artist(cxt, req.id, req.value) {
        Err(e) => ErrorResponse::response(e),
        Ok(artist) => UpdateArtistResponse::response(artist.into()),
      }
    }
  }
}

/**
 * Endpoints which do not require authentication, used by the browser extension.
 *
 * Currently there is only one method and its only used by the browser extension. In the future I
 * want to still require authentication for this route so this may become obselete.
 */
pub mod external {
  use super::*;

  pub async fn import_image(cxt: &Context, body: Bytes) -> Response<Body> {
    match ImportImageRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(request) => match workflow::import_from_url(cxt, request.url, request.page).await {
        Ok(id) => ImportImageResponse::response(id),
        Err(e) => ErrorResponse::response(e),
      },
    }
  }
}

/**
 * Endpoints relating to authentication logic including checking login credentials and logging in.
 */
pub mod auth {
  use super::*;

  pub fn check(cxt: &Context, headers: HeaderMap) -> Response<Body> {
    use crate::config::AuthError::*;
    match cxt.config.auth.authenticate_request(&headers) {
      Ok(user_id) => CheckAuthResponse::response(user_id),
      Err(e) => ErrorResponse::response(match e {
        NotLoggedIn => Error::auth_error().attach("Not logged in"),
        Expired => Error::auth_error().attach("Expired token"),
        InvalidToken => Error::request_error("Invalid login").attach("Invalid auth token"),
        Decode(_) => Error::request_error("Invalid login").attach("Couldnt decode auth token"),
      })
    }
  }

  pub fn login(cxt: &Context, body: Bytes) -> Response<Body> {
    use workflow::LoginError::*;
    match LoginRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(login_request) => match workflow::handle_login(cxt, login_request) {
        Ok(access_token) => LoginResponse::response(access_token),
        Err(e) => ErrorResponse::response(match e {
          InvalidUsername => Error::request_error("Invalid username"),
          InvalidPassword => Error::request_error("Invalid password"),
          DatabaseConn(e) => Error::db_conn(e),
          Database(e) => Error::db_error().attach(e),
          InternalError => Error::unknown()
        })
      },
    }
  }
}

/**
 * Endpoints associated with CRUD tasks on feature objects.
 */
pub mod feature {
  use super::*;

  pub fn create(cxt: &Context, body: Bytes) -> Response<Body> {
    match CreateFeatureRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::create_feature(cxt, r.value) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => CreateFeatureResponse::response(data.into()),
      }
    }
  }

  pub fn delete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::delete_feature(cxt, id) {
        Ok(_) => DeleteFeatureResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn update(cxt: &Context, body: Bytes) -> Response<Body> {
    match UpdateFeatureRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::update_feature(cxt, r.id, r.value) {
        Err(e) => ErrorResponse::response(e),
        Ok(name) => UpdateFeatureResponse::response(name.into()),
      }
    }
  }

}

/**
 * Endpoints associated with CRUD tasks on group objects.
 */
pub mod group {
  use super::*;

  pub fn create(cxt: &Context, body: Bytes) -> Response<Body> {
    match CreateGroupRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::create_group(cxt, r.value, r.imageIds) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => CreateGroupResponse::response(data.into()),
      }
    }
  }

  pub fn delete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::delete_group(cxt, id) {
        Ok(_) => DeleteGroupResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn permadelete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::permadelete_group(cxt, id) {
        Ok(_) => EmptyResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn restore(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::restore_group(cxt, id) {
        Ok(_) => EmptyResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn update(cxt: &Context, body: Bytes) -> Response<Body> {
    // TODO: Return refreshed group on error so data stays in sync.
    match UpdateGroupRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(group) => match workflow::update_group(cxt, group) {
        Err(e) => ErrorResponse::response(e),
        Ok(group) => UpdateGroupResponse::response(group),
      }
    }
  }
}

/**
 * Endpoints associated with CRUD tasks on image feature objects.
 */
pub mod image_feature {
  use super::*;

  pub fn create(cxt: &Context, body: Bytes) -> Response<Body> {
    match CreateImageFeatureRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::create_image_feature(cxt, r.imageId, r.featureId, (r.x1, r.y1), (r.x2, r.y2)) {
        Err(e) => ErrorResponse::response(e),
        Ok(o) => CreateImageFeatureResponse::response(o.into()),
      }
    }
  }

  pub fn delete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::delete_image_feature(cxt, id) {
        Err(e) => ErrorResponse::response(e),
        Ok(_) => DeleteImageFeatureResponse::response(())
      }
    }
  }

}

/**
 * Endpoints associated with CRUD tasks on image objects.
 */
pub mod image {
  use super::*;

  pub fn delete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::delete_image(cxt, id) {
        Ok(_) => DeleteImageResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn face_suggest(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::get_name_suggestion::get_name_suggestions(cxt, id) {
        Err(e) => ErrorResponse::response(e),
        Ok(names) => FaceSuggestResponse::response((names.len() as i64, cov_into(names)))
      }
    }
  }

  pub fn get(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::get_image(cxt, id) {
        Err(e) => ErrorResponse::response(e),
        Ok(image) => GetImageResponse::response(image),
      }
    }
  }

  pub fn mark_dupes(cxt: &Context, body: Bytes) -> Response<Body> {
    match MarkDupesRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::update_dupe_marks(cxt, r.imageIds, r.dupeType) {
        Err(e) => ErrorResponse::response(e),
        Ok(()) => MarkDupesResponse::response(()),
      }
    }
  }

  pub fn permadelete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::permadelete_image(cxt, id) {
        Ok(_) => EmptyResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn restore(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::restore_image(cxt, id) {
        Ok(_) => EmptyResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn rate(cxt: &Context, body: Bytes) -> Response<Body> {
    match RateImageRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::rate_image(cxt, r.imageId, r.rating, r.clearExisting) {
        Err(e) => ErrorResponse::response(e),
        Ok((average, count)) => RateImageResponse::response((average, count)),
      }
    }
  }

  pub fn unmark_dupes(cxt: &Context, body: Bytes) -> Response<Body> {
    match UnmarkDupesRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::delete_dupe_marks(cxt, r.imageIds) {
        Err(e) => ErrorResponse::response(e),
        Ok(()) => UnmarkDupesResponse::response(()),
      }
    }
  }

  pub fn update(cxt: &Context, body: Bytes) -> Response<Body> {
    // TODO: Return refreshed image on error so data stays in sync.
    match UpdateImageRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(image) => match workflow::update_image(cxt, image) {
        Err(e) => ErrorResponse::response(e),
        Ok(image) => UpdateImageResponse::response(image),
      }
    }
  }

  pub async fn upload(cxt: &Context, headers: HeaderMap, body: Bytes) -> Response<Body> {
    match UploadFileRequestBody::from_request(headers, body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::import_from_upload(cxt, r.extension, r.content).await {
        Ok(id) => ImportImageResponse::response(id),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }
}

/**
 * Endpoints associated with retrieving application metadata.
 */
pub mod metadata {
  use super::*;

  pub fn get(cxt: &Context) -> Response<Body> {
    match workflow::get_metadata(cxt) {
      Ok(data) => GetMetadataResponse::response(data),
      Err(e) => ErrorResponse::response(e),
    }
  }

}

/**
 * Endpoints associated with CRUD tasks on name objects.
 */
pub mod name {
  use super::*;

  pub fn create(cxt: &Context, body: Bytes) -> Response<Body> {
    match CreateNameRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::create_name(cxt, r.value, r.imageIds) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => CreateNameResponse::response(data.into()),
      }
    }
  }

  pub fn delete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::delete_name(cxt, id) {
        Ok(_) => DeleteNameResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn update(cxt: &Context, body: Bytes) -> Response<Body> {
    // TODO: Return refreshed group on error so data stays in sync.
    match UpdateNameRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::update_name(cxt, r.id, r.value) {
        Err(e) => ErrorResponse::response(e),
        Ok(name) => UpdateNameResponse::response(name.into()),
      }
    }
  }

}

/**
 * Endpoints associated with querying lists of objects.
 */
pub mod query {
  use super::*;

  pub fn artists(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryArtistsRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(request) => match workflow::query_artists(cxt, request) {
        Ok((count, artists)) => QueryArtistsResponse::response((count, cov_into(artists))),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn deleted(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryDeletedRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(r) => match workflow::query_deleted(cxt, r) {
        Ok(data) => QueryDeletedResponse::response(data),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn dupe_candidates(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryDupeCandidatesRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(request) => match workflow::query_dupe_candidates(cxt, request) {
        Ok((count, items)) => QueryDupeCandidatesResponse::response((count, cov_into(items))),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn eigendata(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryEigendataRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(r) => match workflow::query_eigendata(cxt, r.num_vecs, r.num_weights) {
        Ok(data) => QueryEigendataResponse::response((data.len() as i64, cov_into(data))),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn face_matches(cxt: &Context, id_str: String, uri: Uri) -> Response<Body> {
    match parse_id(&id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match QueryFaceMatchesRequest::from_uri(uri) {
        Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
        Ok(request) => match workflow::query_face_matches(cxt, id, request) {
          Err(e) => ErrorResponse::response(e),
          Ok((count, items)) => QueryFaceMatchesResponse::response((count, cov_into(items))),
        }
      }
    }
  }

  pub fn groups(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryGroupsRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(request) => match workflow::query_groups(cxt, request) {
        Ok((count, groups)) => QueryGroupsResponse::response((count, cov_into(groups))),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn images(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryImagesRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(request) => match workflow::query_images(cxt, request) {
        Ok(data) => QueryImagesResponse::response(data),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn marked_dupes(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::get_marked_dupes(cxt, id) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => QueryMarkedDupesResponse::response((data.len() as i64, data)),
      }
    }
  }

  pub fn names(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryNamesRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(request) => match workflow::query_names(cxt, request) {
        Ok((count, names)) => QueryNamesResponse::response((count, cov_into(names))),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn tags(cxt: &Context, uri: Uri) -> Response<Body> {
    match QueryTagsRequest::from_uri(uri) {
      Err(msg) => ErrorResponse::response(Error::request_error(&msg)),
      Ok(request) => match workflow::query_tags(cxt, request) {
        Ok((count, tags)) => QueryTagsResponse::response((count, cov_into(tags))),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

}

/**
 * Endpoints associated with CRUD tasks on tag objects.
 */
pub mod tag {
  use super::*;

  pub fn create(cxt: &Context, body: Bytes) -> Response<Body> {
    match CreateTagRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::create_tag(cxt, r.value, r.imageIds) {
        Err(e) => ErrorResponse::response(e),
        Ok(data) => CreateTagResponse::response(data.into()),
      }
    }
  }

  pub fn delete(cxt: &Context, id_str: &str) -> Response<Body> {
    match parse_id(id_str) {
      Err(e) => ErrorResponse::response(e),
      Ok(id) => match workflow::delete_tag(cxt, id) {
        Ok(_) => DeleteTagResponse::response(()),
        Err(e) => ErrorResponse::response(e),
      }
    }
  }

  pub fn update(cxt: &Context, body: Bytes) -> Response<Body> {
    // TODO: Return refreshed group on error so data stays in sync.
    match UpdateTagRequest::from_body(body) {
      Err(e) => ErrorResponse::response(e),
      Ok(r) => match workflow::update_tag(cxt, r.id, r.value) {
        Err(e) => ErrorResponse::response(e),
        Ok(tag) => UpdateTagResponse::response(tag.into()),
      }
    }
  }

}
