#![allow(non_snake_case)]
use serde::de::DeserializeOwned;
use hyper::{header::CONTENT_TYPE, body::Bytes, HeaderMap};
use serde::{Deserialize};

use crate::{
  client_models::{ClientDupeType, ClientGroupDetails, ClientImageDetails},
  constants::{
    FACE_SIMILARITY_DUPE_THRESHOLD,
    MAX_IMAGE_FACE_MATCHES,
    FACE_MATCH_CUTOFF,
  },
  content_types::ContentType,
  error::Error,
  query::*,
  workflow::{
    DupeCandidateMethod,
    QueryArtistSortMethod,
    QueryGroupSortMethod,
    QueryImageSortMethod,
    QueryNameSortMethod,
    QueryTagSortMethod,
  },
};
use super::search_request::{
  QueryParser,
  QueryRequest,
};


#[derive(Deserialize)]
pub struct CreateFeatureRequest {
  pub value: String
}

#[derive(Deserialize)]
pub struct CreateImageFeatureRequest {
  pub imageId: i32,
  pub featureId: i32,
  pub x1: f32,
  pub x2: f32,
  pub y1: f32,
  pub y2: f32,
}

#[derive(Deserialize)]
pub struct CreateRelatedRequest {
  pub value: String,
  pub imageIds: Vec<i32>
}

pub type CreateArtistRequest = CreateRelatedRequest;
pub type CreateGroupRequest = CreateRelatedRequest;
pub type CreateNameRequest = CreateRelatedRequest;
pub type CreateTagRequest = CreateRelatedRequest;

#[derive(Deserialize)]
pub struct LoginRequest {
  pub username: String,
  pub password: String,
}

#[derive(Deserialize)]
pub struct MarkDupesRequest {
  pub imageIds: Vec<i32>,
  pub dupeType: ClientDupeType,
}

#[derive(Deserialize)]
pub struct ImportImageRequest {
  pub url: String,
  pub page: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct GenerateDatasetRequest {
  pub datasetName: Option<String>,
  pub useExistingImages: Option<bool>,
  pub includeHidden: Option<bool>,
  pub includeUnprocessed: Option<bool>,
  pub trainSize: Option<i32>,
}

#[derive(Deserialize, Debug)]
pub struct GenerateFeatureDatasetRequest {
  pub datasetName: Option<String>,
  pub useExistingImages: Option<bool>,
  pub includeHidden: Option<bool>,
  pub includeUnprocessed: Option<bool>,
  pub features: Option<Vec<i32>>,
  pub excludeTags: Option<Vec<i32>>,
  pub includeTags: Option<Vec<i32>>,
  pub nullFeatures: Option<Vec<i32>>,
  pub generateNullData: Option<bool>,
}

pub type GenerateTagDatasetRequest = GenerateDatasetRequest;

pub struct QueryArtistsRequest {
  pub sort_by: QueryArtistSortMethod,
  pub direction: SortDirection,
}

pub struct QueryDeletedRequest {
  pub page: i64,
  pub size: i64,
}

pub struct QueryDupeCandidatesRequest {
  pub page: usize,
  pub size: usize,
  pub method: DupeCandidateMethod,
  pub marked: Option<bool>,
  pub cutoff: f32,
}

pub struct QueryEigendataRequest {
  pub num_vecs: usize,
  pub num_weights: usize,
}

pub struct QueryFaceMatchesRequest {
  pub page: usize,
  pub size: usize,
  pub marked: Option<bool>,
  pub cutoff: f32,
}

pub struct QueryGroupsRequest {
  pub page: i64,
  pub size: i64,
  pub sort_by: QueryGroupSortMethod,
  pub direction: SortDirection,
  pub hidden: Option<bool>,
  pub names: Option<Vec<i32>>,
  pub tags: Option<Vec<i32>>,
  pub created_time: Option<Vec<DateFilter>>,
}

pub struct QueryImagesRequest {
  pub page: i64,
  pub size: i64,
  pub sort_by: QueryImageSortMethod,
  pub direction: SortDirection,
  pub favorite: Option<bool>,
  pub has_artist: Option<bool>,
  pub grouped: Option<bool>,
  pub hidden: Option<bool>,
  pub is_video: Option<bool>,
  pub marked: Option<bool>,
  pub processed: Option<bool>,
  pub artists: Option<Vec<i32>>,
  pub features: Option<Vec<i32>>,
  pub groups: Option<Vec<i32>>,
  pub names: Option<Vec<i32>>,
  pub tags: Option<Vec<i32>>,
  pub not_features: Option<Vec<i32>>,
  pub not_names: Option<Vec<i32>>,
  pub not_tags: Option<Vec<i32>>,
  pub rating_count: Option<Vec<CountFilter>>,
  pub rating: Option<Vec<RatingFilter>>,
  pub processed_time: Option<Vec<DateFilter>>,
  pub created_time: Option<Vec<DateFilter>>,
}

pub struct QueryNamesRequest {
  pub sort_by: QueryNameSortMethod,
  pub direction: SortDirection,
}

pub struct QueryTagsRequest {
  pub sort_by: QueryTagSortMethod,
  pub direction: SortDirection,
}

#[derive(Deserialize)]
pub struct RateImageRequest {
  pub imageId: i32,
  pub rating: i32,
  pub clearExisting: bool
}

#[derive(Deserialize)]
pub struct ReindexImagesRequest {
  pub excludeExisting: bool
}

#[derive(Deserialize)]
pub struct UnmarkDupesRequest {
  pub imageIds: Vec<i32>,
}

#[derive(Deserialize)]
pub struct UpdateArtistRequest {
  pub id: i32,
  pub value: String,
}

#[derive(Deserialize)]
pub struct UpdateFeatureRequest {
  pub id: i32,
  pub value: String,
}

pub type UpdateImageRequest = ClientImageDetails;

pub type UpdateGroupRequest = ClientGroupDetails;

#[derive(Deserialize)]
pub struct UpdateNameRequest {
  pub id: i32,
  pub value: String,
}

#[derive(Deserialize)]
pub struct UpdateTagRequest {
  pub id: i32,
  pub value: String,
}

pub struct UploadFileRequestBody {
  pub extension: String,
  pub content: Vec<u8>
}

pub trait BodyRequest: DeserializeOwned {
  fn from_body(body: Bytes) -> Result<Self, Error> {
    serde_json::from_slice(&body)
      .map_err(|e| Error::request_body_error().attach(e))
  }
}

impl BodyRequest for CreateFeatureRequest {}
impl BodyRequest for CreateImageFeatureRequest {}
impl BodyRequest for CreateRelatedRequest {}
impl BodyRequest for GenerateDatasetRequest {}
impl BodyRequest for GenerateFeatureDatasetRequest {}
impl BodyRequest for LoginRequest {}
impl BodyRequest for MarkDupesRequest {}
impl BodyRequest for ImportImageRequest {}
impl BodyRequest for ReindexImagesRequest {}
impl BodyRequest for RateImageRequest {}
impl BodyRequest for UnmarkDupesRequest {}
impl BodyRequest for UpdateArtistRequest {}
impl BodyRequest for UpdateFeatureRequest {}
impl BodyRequest for UpdateGroupRequest {}
impl BodyRequest for UpdateImageRequest {}
impl BodyRequest for UpdateNameRequest {}
impl BodyRequest for UpdateTagRequest {}

impl QueryRequest for QueryArtistsRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      sort_by: qp.parse::<QueryArtistSortMethod>("sortBy").get_or(QueryArtistSortMethod::Value)?,
      direction: qp.parse::<SortDirection>("direction").get_or(SortDirection::Asc)?,
    })
  }
}

impl QueryRequest for QueryDeletedRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      page: qp.parse::<i64>("page").get_or_catch(0),
      size: qp.parse::<i64>("size").get_or_catch(25),
    })
  }
}

impl QueryRequest for QueryDupeCandidatesRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      page: qp.parse::<usize>("page").get_or_catch(0 as usize),
      size: qp.parse::<usize>("size").get_or_catch(25 as usize),
      marked: qp.parse::<bool>("hidden").get()?,
      method: qp.parse::<DupeCandidateMethod>("method").get_or(DupeCandidateMethod::FaceComparison)?,
      cutoff: qp.parse::<f32>("cutoff").get_or_catch(FACE_SIMILARITY_DUPE_THRESHOLD),
    })
  }
}

impl QueryRequest for QueryEigendataRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      num_vecs: qp.parse::<usize>("numVecs").get_or_catch(15 as usize),
      num_weights: qp.parse::<usize>("numWeights").get_or_catch(15 as usize),
    })
  }
}

impl QueryRequest for QueryFaceMatchesRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      page: qp.parse::<usize>("page").get_or_catch(0 as usize),
      size: qp.parse::<usize>("size").get_or_catch(MAX_IMAGE_FACE_MATCHES),
      marked: qp.parse::<bool>("marked").get()?,
      cutoff: qp.parse::<f32>("cutoff").get_or_catch(FACE_MATCH_CUTOFF),
    })
  }
}

impl QueryRequest for QueryGroupsRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      page: qp.parse::<i64>("page").get_or_catch(0),
      size: qp.parse::<i64>("size").get_or_catch(25),
      sort_by: qp.parse::<QueryGroupSortMethod>("sortBy").get_or(QueryGroupSortMethod::Id)?,
      direction: qp.parse::<SortDirection>("direction").get_or(SortDirection::Asc)?,
      names: qp.parse::<Vec<i32>>("names").get()?,
      tags: qp.parse::<Vec<i32>>("tags").get()?,
      hidden: qp.parse::<bool>("hidden").get()?,
      created_time: qp.parse::<Vec<DateFilter>>("createdTime").get()?,
    })
  }
}

impl QueryRequest for QueryImagesRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      page: qp.parse::<i64>("page").validate(|page| *page >= 0).get_or_catch(0),
      size: qp.parse::<i64>("size").validate(|size| *size > 0).get_or_catch(25),
      sort_by: qp.parse::<QueryImageSortMethod>("sortBy").get_or(QueryImageSortMethod::Id)?,
      direction: qp.parse::<SortDirection>("direction").get_or(SortDirection::Asc)?,
      favorite: qp.parse::<bool>("favorite").get()?,
      has_artist: qp.parse::<bool>("hasArtist").get()?,
      grouped: qp.parse::<bool>("grouped").get()?,
      hidden: qp.parse::<bool>("hidden").get()?,
      is_video: qp.parse::<bool>("isVideo").get()?,
      marked: qp.parse::<bool>("marked").get()?,
      processed: qp.parse::<bool>("processed").get()?,
      artists: qp.parse::<Vec<i32>>("artists").get()?,
      features: qp.parse::<Vec<i32>>("features").get()?,
      groups: qp.parse::<Vec<i32>>("groups").get()?,
      names: qp.parse::<Vec<i32>>("names").get()?,
      tags: qp.parse::<Vec<i32>>("tags").get()?,
      not_features: qp.parse::<Vec<i32>>("notFeatures").get()?,
      not_names: qp.parse::<Vec<i32>>("notNames").get()?,
      not_tags: qp.parse::<Vec<i32>>("notTags").get()?,
      created_time: qp.parse::<Vec<DateFilter>>("createdTime").get()?,
      processed_time: qp.parse::<Vec<DateFilter>>("processedTime").get()?,
      rating: qp.parse::<Vec<RatingFilter>>("rating").get()?,
      rating_count: qp.parse::<Vec<CountFilter>>("ratingCount").get()?,
    })
  }
}

impl QueryRequest for QueryNamesRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      sort_by: qp.parse::<QueryNameSortMethod>("sortBy").get_or(QueryNameSortMethod::Value)?,
      direction: qp.parse::<SortDirection>("direction").get_or(SortDirection::Asc)?,
    })
  }
}

impl QueryRequest for QueryTagsRequest {

  fn from_query(query: &str) -> Result<Self, String> {
    let mut qp = QueryParser::from_query(query);
    Ok(Self {
      sort_by: qp.parse::<QueryTagSortMethod>("sortBy").get_or(QueryTagSortMethod::Value)?,
      direction: qp.parse::<SortDirection>("direction").get_or(SortDirection::Asc)?,
    })
  }
}

impl UploadFileRequestBody {
  pub fn from_request(headers: HeaderMap, body: Bytes) -> Result<UploadFileRequestBody, Error> {
    let ct_header = headers.get(CONTENT_TYPE)
      .ok_or(Error::request_error("unknown content type"))?
      .to_str()
      .map_err(|e| Error::request_error("unknown content type").attach(e))?;
    let extension = ContentType::header_to_ext(ct_header)
      .ok_or(Error::request_error("unknown content type"))?;
    Ok(UploadFileRequestBody {
      content: body.to_vec(),
      extension: String::from(extension)
    })
  }
}

pub fn parse_id(id_str: &str) -> Result<i32, Error> {
  id_str.parse::<i32>()
    .map_err(|e| Error::request_error("Invalid id string").attach(e))
}

pub fn parse_id64(id_str: &str) -> Result<u64, Error> {
  id_str.parse::<u64>()
    .map_err(|e| Error::request_error("Invalid id string").attach(e))
}

#[cfg(test)]
mod test_QueryImagesRequest {
  use super::*;
  use percent_encoding::{
    utf8_percent_encode,
    AsciiSet,
    CONTROLS,
  };

  // https://url.spec.whatwg.org/#query-percent-encode-set
  const QUERY_ASCII_SET: AsciiSet = CONTROLS.add(b' ').add(b'"').add(b'#').add(b'<').add(b'>');

  #[test]
  fn it_handles_empty_uri() {
    let uri_path = "http://localhost";
    let expected_tags = None;
    let expected_page = 0;
    let expected_size = 25;
    let req = QueryImagesRequest::from_query(&uri_path).unwrap();
    assert_eq!(req.tags, expected_tags);
    assert_eq!(req.page, expected_page);
    assert_eq!(req.size, expected_size);
  }

  #[test]
  fn it_decodes_page_parameter() {
    for (parameter, expected) in [
      (Some("0"), 0),
      (Some("53"), 53),
      (None, 0) // Default page value
    ].iter() {
      let request = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("page={}&names=1", param_value),
        None => format!("names=1"),
      }).unwrap();
      assert_eq!(request.page, *expected);
    }
  }

  #[test]
  fn it_handles_invalid_page_parameter_values() {
    let expected_page = 0;
    for query in vec!["page=-1", "page=batman", "page=&names=1"].iter() {
      let request = QueryImagesRequest::from_query(query).unwrap();
      assert_eq!(request.page, expected_page);
    }
  }

  #[test]
  fn it_decodes_size_parameter() {
    for (parameter, expected) in [
      (Some("1"), 1),
      (Some("53"), 53),
      (None, 25) // Default size value
    ].iter() {
      let request = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size={}&names=1", param_value),
        None => format!("names=1"),
      }).unwrap();
      assert_eq!(request.size, *expected);
    }
  }

  #[test]
  fn it_handles_invalid_size_parameter_values() {
    let expected_size = 25;
    for query in vec!["size=0, size=-1", "size=batman", "size=&names=1"].iter() {
      let request = QueryImagesRequest::from_query(query).unwrap();
      assert_eq!(request.size, expected_size);
    }
  }

  #[test]
  fn it_decodes_sort_by_parameter() {
    use QueryImageSortMethod::*;
    for (parameter, expected) in [
      (Some("id"), Id),
      (Some("createdTime"), CreatedTime),
      (Some(""), Id),
      (None, Id),
    ].iter() {
      let request = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&sortBy={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap();
      assert_eq!(request.sort_by, *expected);
    }
  }

  #[test]
  fn it_decodes_sort_direction_parameter() {
    for (parameter, expected) in [
      (Some("asc"), SortDirection::Asc),
      (Some("desc"), SortDirection::Desc),
      (Some(""), SortDirection::Asc),
      (None, SortDirection::Asc),
    ].iter() {
      let request = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&direction={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap();
      assert_eq!(request.direction, *expected);
    }
  }

  #[test]
  fn it_decodes_hidden_parameter() {
    for (parameter, expected) in [
      (Some("true"), Some(true)),
      (Some("false"), Some(false)),
      (Some(""), None),
      (None, None),
    ].iter() {
      let actual = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&hidden={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap().hidden;
      assert_eq!(actual, *expected);
    }
  }

  #[test]
  fn it_decodes_processed_parameter() {
    for (parameter, expected) in [
      (Some("true"), Some(true)),
      (Some("false"), Some(false)),
      (Some(""), None),
      (None, None),
    ].iter() {
      let actual = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&processed={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap().processed;
      assert_eq!(actual, *expected);
    }
  }

  #[test]
  fn it_decodes_favorite_parameter() {
    for (parameter, expected) in [
      (Some("true"), Some(true)),
      (Some("false"), Some(false)),
      (Some(""), None),
      (None, None),
    ].iter() {
      let actual = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&favorite={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap().favorite;
      assert_eq!(actual, *expected);
    }
  }

  #[test]
  fn it_decodes_is_video_parameter() {
    for (parameter, expected) in [
      (Some("true"), Some(true)),
      (Some("false"), Some(false)),
      (Some(""), None),
      (None, None),
    ].iter() {
      let actual = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&isVideo={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap().is_video;
      assert_eq!(actual, *expected);
    }
  }

  #[test]
  fn it_decodes_grouped_parameter() {
    for (parameter, expected) in [
      (Some("true"), Some(true)),
      (Some("false"), Some(false)),
      (Some(""), None),
      (None, None),
    ].iter() {
      let actual = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&grouped={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap().grouped;
      assert_eq!(actual, *expected);
    }
  }

  #[test]
  fn it_decodes_tags_and_names_from_uri() {
    let uri_path = "http://localhost?page=0&size=100&tags=1,2&names=1,49,8";
    let req = QueryImagesRequest::from_query(&uri_path).unwrap();
    let expected_tags = Some(vec![1, 2]);
    let expected_names = Some(vec![1, 49, 8]);
    assert_eq!(req.tags, expected_tags);
    assert_eq!(req.names, expected_names);
  }

  #[test]
  fn it_decodes_empty_tags() {
    let query = "size=10&tags=";
    let expected_tags = None;
    let actual = QueryImagesRequest::from_query(&query).unwrap().tags;
    assert_eq!(actual, expected_tags);
  }

  #[test]
  fn it_decodes_groups_parameter() {
    for (parameter, expected) in [
      (Some("1,2,3"), Some(vec![1, 2, 3])),
      (Some(""), None),
      (None, None)
    ].iter() {
      let request = QueryImagesRequest::from_query(&match parameter {
        Some(param_value) => format!("size=10&groups={}&names=1", param_value),
        None => format!("size=10&names=1"),
      }).unwrap();
      assert_eq!(request.groups, *expected);
    }
  }

  #[test]
  fn it_decodes_rating() {
    let rating = vec!["<3.5", ">1.0"];
    let expected_rating: Vec<RatingFilter> = vec![
      RatingFilter { value: 3.5, relation: OrderRelation::Lower },
      RatingFilter { value: 1.0, relation: OrderRelation::Higher },
    ];

    let query = format!("page=0&rating={}&size=10", rating.into_iter()
      .map(|condition| utf8_percent_encode(condition, &QUERY_ASCII_SET).to_string())
      .collect::<Vec<String>>()
      .join(","));
    let request = QueryImagesRequest::from_query(&query).unwrap();
    assert_eq!(request.rating.unwrap(), expected_rating);
  }

  #[test]
  fn it_decodes_rating_filters_raw() {
    let query = "page=0&size=18&rating=%3C4.5,%3E2";
    let expected_rating: Vec<RatingFilter> = vec![
      RatingFilter { value: 4.5, relation: OrderRelation::Lower },
      RatingFilter { value: 2.0, relation: OrderRelation::Higher },
    ];

    let request = QueryImagesRequest::from_query(&query).unwrap();
    assert_eq!(request.rating.unwrap(), expected_rating);
  }

  #[test]
  fn it_decodes_processed_time() {
    let parameter = vec![">12341234", "<56785678"];
    let expected: Vec<DateFilter> = vec![
      DateFilter { value: 12341234, relation: OrderRelation::Higher },
      DateFilter { value: 56785678, relation: OrderRelation::Lower },
    ];

    let query = format!("page=0&processedTime={}&size=10", parameter.into_iter()
      .map(|condition| utf8_percent_encode(condition, &QUERY_ASCII_SET).to_string())
      .collect::<Vec<String>>()
      .join(","));
    let actual = QueryImagesRequest::from_query(&query).unwrap().processed_time;
    assert_eq!(actual.unwrap(), expected);
  }

  #[test]
  fn it_decodes_created_time() {
    let parameter = vec![">12341234", "<56785678"];
    let expected: Vec<DateFilter> = vec![
      DateFilter { value: 12341234, relation: OrderRelation::Higher },
      DateFilter { value: 56785678, relation: OrderRelation::Lower },
    ];

    let query = format!("page=0&createdTime={}&size=10", parameter.into_iter()
      .map(|condition| utf8_percent_encode(condition, &QUERY_ASCII_SET).to_string())
      .collect::<Vec<String>>()
      .join(","));
    let actual = QueryImagesRequest::from_query(&query).unwrap().created_time;
    assert_eq!(actual.unwrap(), expected);
  }
}
