#![allow(non_snake_case)]
use hyper::{Body, Response};
use serde::{Deserialize, Serialize};

use crate::{
  config::AuthToken,
  client_models::*,
  constants::USER_HEADER_KEY,
  error::Error,
  models::ImgId,
  threads::ThreadStatus
};

pub trait ApiResponse<T>: Serialize + Sized {
  fn pack(self, code: u16) -> Response<Body> {
    Response::builder()
      .status(code)
      .body(Body::from(serde_json::to_vec(&self).unwrap()))
      .unwrap()
  }
  fn response(data: T) -> Response<Body>;
}

#[derive(Serialize)]
pub struct CreateItemResponse<T: Serialize + Sized> {
  item: T
}

#[derive(Serialize)]
pub struct ItemResponse<T: Serialize + Sized> {
  item: T
}

#[derive(Serialize)]
pub struct ItemsResponse<T: Serialize + Sized> {
  items: Vec<T>,
  count: i64
}

pub type CreateArtistResponse = CreateItemResponse<ClientArtist>;

pub type CreateFeatureResponse = CreateItemResponse<ClientFeature>;

pub type CreateGroupResponse = CreateItemResponse<ClientGroupDetails>;

pub type CreateImageFeatureResponse = ItemResponse<ClientImageDetails>;

pub type CreateNameResponse = CreateItemResponse<ClientName>;

pub type CreateTagResponse = CreateItemResponse<ClientTag>;

#[derive(Serialize)]
pub struct CheckAuthResponse {
  pub user_id: i32 // goes in header
}

pub type DeleteArtistResponse = EmptyResponse;

pub type DeleteFeatureResponse = EmptyResponse;

pub type DeleteGroupResponse = EmptyResponse;

pub type DeleteImageResponse = EmptyResponse;

pub type DeleteImageFeatureResponse = EmptyResponse;

pub type DeleteNameResponse = EmptyResponse;

pub type DeleteTagResponse = EmptyResponse;

#[derive(Serialize, Deserialize)]
pub struct EmptyResponse {}

#[derive(Serialize, Deserialize)]
pub struct ErrorResponse {
  pub message: String
}

pub type FaceSuggestResponse = ItemsResponse<ClientName>;

pub type GetImageResponse = ItemResponse<ClientImageDetails>;

#[derive(Serialize, Deserialize)]
pub struct GetMetadataResponse {
  pub artist: ContentTable<ClientArtist>,
  pub feature: ContentTable<ClientFeature>,
  pub group: ContentTable<ClientGroup>,
  pub name: ContentTable<ClientName>,
  pub tag: ContentTable<ClientTag>,
}

#[derive(Serialize, Deserialize)]
pub struct ImportImageResponse {
  pub id: ImgId,
}

pub type MarkDupesResponse = EmptyResponse;

#[derive(Serialize, Deserialize)]
pub struct LoginResponse {
  pub access_token: String,
}

#[derive(Serialize, Deserialize)]
pub struct RateImageResponse {
  pub average: f32,
  pub count: i32,
}

pub type QueryArtistsResponse = ItemsResponse<ClientArtistDetails>;

pub type QueryDeletedResponse = ItemsResponse<ClientImageDetails>;

pub type QueryDupeCandidatesResponse = ItemsResponse<ClientDupeCandidate>;

pub type QueryEigendataResponse = ItemsResponse<ClientContentEigendata>;

pub type QueryFaceMatchesResponse = ItemsResponse<ClientFaceMatch>;

pub type QueryGroupsResponse = ItemsResponse<ClientGroupDetails>;

pub type QueryImagesResponse = ItemsResponse<ClientImageDetails>;

pub type QueryMarkedDupesResponse = ItemsResponse<(ClientImageDetails, ClientDupeType)>;

pub type QueryNamesResponse = ItemsResponse<ClientNameDetails>;

pub type QueryTagsResponse = ItemsResponse<ClientTagDetails>;

#[derive(Serialize, Deserialize, Debug)]
pub struct ThreadStatusResponse {
  pub thread_id: u64,
  pub status: ThreadStatus
}

pub type UnmarkDupesResponse = EmptyResponse;

pub type UpdateArtistResponse = ItemResponse<ClientArtist>;

pub type UpdateFeatureResponse = ItemResponse<ClientFeature>;

pub type UpdateGroupResponse = ItemResponse<ClientGroupDetails>;

pub type UpdateImageResponse = ItemResponse<ClientImageDetails>;

pub type UpdateNameResponse = ItemResponse<ClientName>;

pub type UpdateTagResponse = ItemResponse<ClientTag>;

impl<T: Sized + Serialize> ApiResponse<T> for CreateItemResponse<T> {
  fn response(item: T) -> Response<Body> {
    ItemResponse { item }.pack(201)
  }
}

impl<T: Serialize + Sized> ApiResponse<T> for ItemResponse<T> {
  fn response(item: T) -> Response<Body> {
    ItemResponse { item }.pack(200)
  }
}

impl<T: Serialize + Sized> ApiResponse<(i64, Vec<T>)> for ItemsResponse<T> {
  fn response((count, items): (i64, Vec<T>)) -> Response<Body> {
    ItemsResponse { count, items }.pack(200)
  }
}

impl ApiResponse<i32> for CheckAuthResponse {
  fn pack(self, code: u16) -> Response<Body> {
    Response::builder()
      .status(code)
      .header(USER_HEADER_KEY, self.user_id.to_string())
      .body(Body::from(""))
      .unwrap()
  }
  fn response(user_id: i32) -> Response<Body> {
    CheckAuthResponse { user_id }.pack(200)
  }
}

impl ApiResponse<()> for EmptyResponse {
  fn response(_: ()) -> Response<Body> {
    EmptyResponse { }.pack(200)
  }
}

impl ApiResponse<Error> for ErrorResponse {
  fn response(e: Error) -> Response<Body> {
    e.log();
    ErrorResponse { message: e.client_msg }.pack(e.code)
  }
}

pub type GetMetadataResponseArg = (ContentTable<ClientArtist>, ContentTable<ClientFeature>, ContentTable<ClientGroup>, ContentTable<ClientName>, ContentTable<ClientTag>);

impl ApiResponse<GetMetadataResponseArg> for GetMetadataResponse {
  fn response((artist, feature, group, name, tag): GetMetadataResponseArg) -> Response<Body> {
    GetMetadataResponse { artist, feature, group, name, tag }.pack(200)
  }
}
impl ApiResponse<ImgId> for ImportImageResponse {
  fn response(id: ImgId) -> Response<Body> {
    ImportImageResponse { id }.pack(201)
  }
}
impl ApiResponse<AuthToken> for LoginResponse {
  fn response(token: AuthToken) -> Response<Body> {
    LoginResponse { access_token: token.0 }.pack(200)
  }
}
impl ApiResponse<(f32, i32)> for RateImageResponse {
  fn response((average, count): (f32, i32)) -> Response<Body> {
    RateImageResponse { average, count }.pack(200)
  }
}
impl ApiResponse<(u64, ThreadStatus)> for ThreadStatusResponse {
  fn response((thread_id, status): (u64, ThreadStatus)) -> Response<Body> {
    ThreadStatusResponse { thread_id, status }.pack(200)
  }
}
