use percent_encoding;
use hyper::Uri;
use std::collections::HashMap;
use std::fmt::Debug;
use std::str::FromStr;
use std::str::Utf8Error;

use crate::{
  query::*,
  workflow::{
    DupeCandidateMethod,
    QueryArtistSortMethod,
    QueryGroupSortMethod,
    QueryImageSortMethod,
    QueryNameSortMethod,
    QueryTagSortMethod,
  }
};

pub struct QueryParser<'a> {
  query: &'a str,
  key_map: HashMap<&'a str, (usize, usize)>
}

pub trait QueryRequest: Sized {

  fn from_uri(uri: Uri) -> Result<Self, String> {
    Self::from_query(uri.query().unwrap_or(""))
  }

  fn from_query(query: &str) -> Result<Self, String>;

}

pub trait ParsableField: Sized {
  type Error: Debug;

  fn parse(from: &str) -> Result<Self, Self::Error>;
}

impl ParsableField for i32 {
  type Error = std::num::ParseIntError;

  fn parse(from: &str) -> Result<i32, Self::Error> {
    from.parse::<i32>()
  }
}

impl ParsableField for usize {
  type Error = std::num::ParseIntError;

  fn parse(from: &str) -> Result<usize, Self::Error> {
    from.parse::<usize>()
  }
}

impl ParsableField for i64 {
  type Error = std::num::ParseIntError;

  fn parse(from: &str) -> Result<i64, Self::Error> {
    from.parse::<i64>()
  }
}

impl ParsableField for f32 {
  type Error = std::num::ParseFloatError;

  fn parse(from: &str) -> Result<f32, Self::Error> {
    from.parse::<f32>()
  }
}

impl ParsableField for bool {
  type Error = &'static str;

  fn parse(from: &str) -> Result<bool, Self::Error> {
    match from {
      "true" => Ok(true),
      "false" => Ok(false),
      _ => Err("Invalid boolean argument"),
    }
  }
}

impl ParsableField for String {
  type Error = Utf8Error;

  fn parse(from: &str) -> Result<String, Self::Error> {
    percent_encoding::percent_decode_str(from)
      .decode_utf8()
      .map(|s| s.to_string())
  }
}

impl ParsableField for SortDirection {
  type Error = &'static str;

  fn parse(from: &str) -> Result<SortDirection, Self::Error> {
    match from {
      "asc" => Ok(SortDirection::Asc),
      "desc" => Ok(SortDirection::Desc),
      _ => Err("Invalid sort direction"),
    }
  }
}

impl ParsableField for OrderRelation {
  type Error = &'static str;

  fn parse(from: &str) -> Result<OrderRelation, Self::Error> {
    if let Some(c) = from.chars().nth(0) {
      match c {
        '>' => Ok(OrderRelation::Higher),
        '<' => Ok(OrderRelation::Lower),
        _ => Err("Invalid relation operator")
      }
    } else {
      Err("Missing relation operator")
    }
  }
}

impl ParsableField for DupeCandidateMethod {
  type Error = &'static str;

  fn parse(from: &str) -> Result<DupeCandidateMethod, Self::Error> {
    use DupeCandidateMethod::*;
    match from {
      "face" => Ok(FaceComparison),
      _ => Err("Invalid duplicate method")
    }
  }
}

impl ParsableField for QueryArtistSortMethod {
  type Error = &'static str;

  fn parse(from: &str) -> Result<QueryArtistSortMethod, Self::Error> {
    use QueryArtistSortMethod::*;
    match from {
      "id" => Ok(Id),
      "createdTime" => Ok(CreatedTime),
      "imageCount" => Ok(ImageCount),
      "score" => Ok(Score),
      "value" => Ok(Value),
      _ => Err("Invalid sort value")
    }
  }
}

impl ParsableField for QueryGroupSortMethod {
  type Error = &'static str;

  fn parse(from: &str) -> Result<QueryGroupSortMethod, Self::Error> {
    use QueryGroupSortMethod::*;
    match from {
      "id" => Ok(Id),
      "random" => Ok(Random),
      "createdTime" => Ok(CreatedTime),
      _ => Err("Invalid sort value")
    }
  }
}

impl ParsableField for QueryImageSortMethod {
  type Error = &'static str;

  fn parse(from: &str) -> Result<QueryImageSortMethod, Self::Error> {
    use QueryImageSortMethod::*;
    match from {
      "id" => Ok(Id),
      "favorite" => Ok(Favorite),
      "filesize" => Ok(Filesize),
      "length" => Ok(Length),
      "processed" => Ok(Processed),
      "artistId" => Ok(ArtistId),
      "groupId" => Ok(GroupId),
      "random" => Ok(Random),
      "createdTime" => Ok(CreatedTime),
      "processedTime" => Ok(ProcessedTime),
      "rating" => Ok(Rating),
      "ratingCount" => Ok(RatingCount),
      _ => Err("Invalid sort value")
    }
  }
}

impl ParsableField for QueryNameSortMethod {
  type Error = &'static str;

  fn parse(from: &str) -> Result<QueryNameSortMethod, Self::Error> {
    use QueryNameSortMethod::*;
    match from {
      "id" => Ok(Id),
      "createdTime" => Ok(CreatedTime),
      "groupCount" => Ok(GroupCount),
      "imageCount" => Ok(ImageCount),
      "score" => Ok(Score),
      "value" => Ok(Value),
      _ => Err("Invalid sort value")
    }
  }
}

impl ParsableField for QueryTagSortMethod {
  type Error = &'static str;

  fn parse(from: &str) -> Result<QueryTagSortMethod, Self::Error> {
    use QueryTagSortMethod::*;
    match from {
      "id" => Ok(Id),
      "createdTime" => Ok(CreatedTime),
      "groupCount" => Ok(GroupCount),
      "imageCount" => Ok(ImageCount),
      "score" => Ok(Score),
      "value" => Ok(Value),
      _ => Err("Invalid sort value")
    }
  }
}

#[derive(Debug)]
pub enum ParseRelationFilterError<T> {
  DecodeStringError(Utf8Error),
  LengthError,
  ParseOperatorError,
  ParseValueError(T),
}

impl<T> ParsableField for RelationFilter<T>
where T: FromStr, <T as FromStr>::Err: Debug
{
  type Error = ParseRelationFilterError<<T as FromStr>::Err>;

  fn parse(from: &str) -> Result<RelationFilter<T>, Self::Error> {
    use ParseRelationFilterError::*;
    let decoded = String::parse(from).map_err(|e| DecodeStringError(e))?;
    if decoded.len() > 1 {
      let (relation_str, value_str) = decoded.split_at(1);
      Ok(RelationFilter{
        relation: OrderRelation::parse(relation_str).map_err(|_| ParseOperatorError)?,
        value: T::from_str(value_str).map_err(|e| ParseValueError(e))?
      })
    } else {
      Err(LengthError)
    }
  }
}

impl<T> ParsableField for Vec<T> where T: ParsableField {
  type Error = T::Error;

  fn parse(from: &str) -> Result<Vec<T>, Self::Error> {
    let mut values = Vec::new();
    if from.len() > 0 {
      for val in from.split(',') {
        values.push(T::parse(val)?);
      }
    }
    Ok(values)
  }
}

enum ParserState {
  SearchKeyEnd,
  SearchValueEnd
}

impl<'a> QueryParser<'a> {

  pub fn from_query(query: &'a str) -> QueryParser<'a> {
    use ParserState::*;
    let mut key_map: HashMap<&str, (usize, usize)> = HashMap::new();
    let mut key_start = 0;
    let mut val_start = 0;
    let mut state = SearchKeyEnd;

    for (i, c) in query.chars().enumerate() {
      state = match state {
        SearchKeyEnd => if c == '=' {
          val_start = i + 1;
          SearchValueEnd
        } else {
          SearchKeyEnd
        },
        SearchValueEnd => if c == '&' {
          if val_start != i {
            key_map.insert(&query[key_start..(val_start - 1)], (val_start, i));
          }
          key_start = i + 1;
          SearchKeyEnd
        } else {
          SearchValueEnd
        }
      }
    }
    if let SearchValueEnd = state {
      if val_start != query.len() {
        key_map.insert(&query[key_start..(val_start - 1)], (val_start, query.len()));
      }
    }
    QueryParser { query, key_map }
  }

  pub fn parse<T>(&mut self, field: &'static str) -> ParsedParameter<T>
  where T: ParsableField {
    ParsedParameter {
      field,
      result: match self.key_map.remove(field) {
        Some(pts) => T::parse(&self.query[pts.0..pts.1])
          .map_err(|_| format!("Invalid value for parameter {}", field))
          .map(|r| Some(r)),
        None => Ok(None)
      }
    }
  }

}

pub struct ParsedParameter<T> {
  field: &'static str,
  result: Result<Option<T>, String>
}

impl<T> ParsedParameter<T> {

  pub fn get(self) -> Result<Option<T>, String> {
    self.result
  }

  pub fn get_or<E>(self, default: E) -> Result<T, String>
  where E: Into<T> {
    self.result.map(|value| value.unwrap_or(default.into()))
  }

  pub fn get_or_catch<E>(self, default: E) -> T
  where E: Into<T> {
    match self.result {
      Ok(Some(value)) => value,
      _ => default.into()
    }
  }

  pub fn validate<F>(mut self, f: F) -> ParsedParameter<T>
  where F: FnOnce(&T) -> bool
  {
    if let Ok(Some(ref value)) = self.result {
      if !f(&value) {
        self.result = Err(format!("Invalid value for parameter {}", self.field));
      }
    }
    self
  }

}
