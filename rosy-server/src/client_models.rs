use itertools::izip;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};

use crate::common::cov_into;
use crate::models::*;
use crate::workflow::{
  DupeCandidate,
  FaceMatch,
};

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Eq)]
pub struct ClientArtist {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientArtistDetails {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
  pub imageCount: usize,
  pub score: f32,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientContentEigendata {
  pub eigenvalue: f32,
  pub weights: Vec<f32>,
  pub items: Vec<ClientContentMeta>
}

#[derive(Serialize, Deserialize)]
pub struct ClientContentMeta {
  pub r#type: ClientContentType,
  pub id: Id,
  pub value: String,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientContentMetadata {
  pub evecCoefs: Vec<f32>,
  pub names: ClientContentRelationships,
  pub tags: ClientContentRelationships,
}

#[derive(Serialize, Deserialize)]
pub struct ClientContentRelationships {
  pub related: Vec<(Id, f32)>,
  pub unrelated: Vec<(Id, f32)>,
}

#[allow(non_camel_case_types)]
#[derive(Serialize, Deserialize)]
pub enum ClientContentType {
  name,
  tag
}

#[derive(Serialize, Deserialize)]
pub struct ClientDupeCandidate {
  pub item1: ClientImageDetails,
  pub item2: ClientImageDetails,
  pub score: f64,
  pub mark: Option<DupeType>,
}

pub type ClientDupeType = DupeType;

#[derive(Serialize, Deserialize)]
pub struct ClientFaceMatch {
  pub item: ClientImageDetails,
  pub score: f64,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Eq)]
pub struct ClientFeature {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Eq)]
pub struct ClientGroup {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
  pub hidden: bool,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientGroupDetails {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
  pub description: Option<String>,
  pub hidden: bool,
  pub names: Vec<ClientName>,
  pub tags: Vec<ClientTag>,
  pub images: Vec<ClientImageDetails>
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientImageDetails {
  pub id: i32,
  pub filename: String,
  pub thumbnail: String,
  pub createdTime: i64,
  pub filesize: i32,
  pub hidden: bool,
  pub favorite: bool,
  pub isVideo: bool,
  pub length: Option<f32>,
  pub marked: bool,
  pub processed: bool,
  pub processedTime: Option<i64>,
  pub artistId: Option<i32>,
  pub groupId: Option<i32>,
  pub ratingAverage: Option<f32>,
  pub ratingCount: i32,
  pub names: Vec<ClientName>,
  pub tags: Vec<ClientTag>,
  pub dupes: HashMap<ImgId, ClientDupeType>,
  pub features: Vec<ClientImageFeature>,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientImageFeature {
  pub id: i32,
  pub imageId: i32,
  pub featureId: i32,
  pub value: String,
  pub x1: f32,
  pub x2: f32,
  pub y1: f32,
  pub y2: f32,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Eq)]
pub struct ClientName {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
  pub index: Option<i32>
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientNameDetails {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
  pub index: Option<i32>,
  pub groupCount: usize,
  pub imageCount: usize,
  pub score: f32,
  pub metadata: Option<ClientNameMetadata>,
}

pub type ClientNameMetadata = ClientContentMetadata;

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Eq)]
pub struct ClientTag {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
  pub index: Option<i32>
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct ClientTagDetails {
  pub id: i32,
  pub createdTime: i64,
  pub value: String,
  pub index: Option<i32>,
  pub groupCount: usize,
  pub imageCount: usize,
  pub score: f32,
  pub metadata: Option<ClientTagMetadata>,
}

pub type ClientTagMetadata = ClientContentMetadata;

pub type ContentTable<T> = (Vec<T>, HashMap<Id, u32>);

impl From<Artist> for ClientArtist {
  fn from(artist: Artist) -> Self {
    Self {
      id: artist.id,
      value: artist.name,
      createdTime: artist.created_time,
    }
  }
}

impl From<ArtistDetailsData> for ClientArtistDetails {
  fn from((item, (image_count, score)): ArtistDetailsData) -> Self {
    Self {
      id: item.id,
      value: item.name,
      createdTime: item.created_time,
      imageCount: image_count,
      score,
    }
  }
}

impl From<(f32, Vec<(ContentMeta, f32)>)> for ClientContentEigendata {
  fn from((score, vec): (f32, Vec<(ContentMeta, f32)>)) -> Self {
    Self {
      eigenvalue: score,
      weights: vec.iter().map(|(_, w)| *w).collect(),
      items: vec.into_iter().map(|(cm, _)| cm.into()).collect()
    }
  }
}

impl From<ContentMeta> for ClientContentMeta {
  fn from(cm: ContentMeta) -> Self {
    match cm {
      ContentMeta::Name(t) => Self {
        r#type: ClientContentType::name,
        id: t.id,
        value: t.value
      },
      ContentMeta::Tag(t) => Self {
        r#type: ClientContentType::tag,
        id: t.id,
        value: t.value
      }
    }
  }
}

impl From<DupeCandidate> for ClientDupeCandidate {
  fn from(v: DupeCandidate) -> Self {
    ClientDupeCandidate {
      item1: v.item1.into(),
      item2: v.item2.into(),
      score: v.score,
      mark: v.mark
    }
  }
}

impl From<FaceMatch> for ClientFaceMatch {
  fn from(v: FaceMatch) -> Self {
    ClientFaceMatch {
      item: v.item.into(),
      score: v.score,
    }
  }
}

impl From<Feature> for ClientFeature {
  fn from(v: Feature) -> Self {
    Self {
      id: v.id,
      value: v.value,
      createdTime: v.created_time,
    }
  }
}

impl From<(ImageFeature, Feature)> for ClientImageFeature {
  fn from((v, f): (ImageFeature, Feature)) -> Self {
    Self {
      id: v.id,
      imageId: v.image_id,
      featureId: v.feature_id,
      value: f.value,
      x1: v.x1,
      x2: v.x2,
      y1: v.y1,
      y2: v.y2,
    }
  }
}

impl From<GroupData> for ClientGroupDetails {
  fn from((group, names, tags, images): GroupData) -> Self {
    let name_values: Vec<ClientName> = cov_into(names);
    let tag_values: Vec<ClientTag> = cov_into(tags);
    (group, name_values, tag_values, images).into()
  }
}

impl From<(Group, Vec<ClientName>, Vec<ClientTag>, Vec<ImageData>)> for ClientGroupDetails {
  fn from((group, names, tags, mut images): (Group, Vec<ClientName>, Vec<ClientTag>, Vec<ImageData>)) -> Self {
    images.sort_by(|a, b| a.0.id.partial_cmp(&b.0.id).unwrap());
    let mut g = ClientGroupDetails {
      id: group.id,
      value: group.title,
      description: group.description,
      createdTime: group.created_time,
      hidden: group.hidden,
      names,
      tags,
      images: cov_into(images)
    };
    g.names.sort();
    g.tags.sort();
    g
  }
}

impl From<(Group, HashSet<Name>, HashSet<Tag>, Vec<ImageData>)> for ClientGroupDetails {
  fn from((group, names, tags, images): (Group, HashSet<Name>, HashSet<Tag>, Vec<ImageData>)) -> Self {
    (
      group,
      names.into_iter().map(|name| name.into()).collect::<Vec<ClientName>>(),
      tags.into_iter().map(|tag| tag.into()).collect::<Vec<ClientTag>>(),
      images
    ).into()
  }
}

impl From<Group> for ClientGroup {
  fn from(group: Group) -> Self {
    Self {
      id: group.id,
      value: group.title,
      createdTime: group.created_time,
      hidden: group.hidden,
    }
  }
}

impl<NtV, NT, TtV, TT, FLtV, FLT, DtV, DT> From<(Image, NtV, TtV, FLtV, DtV)> for ClientImageDetails
where
  NtV: IntoIterator<Item=NT>,
  TtV: IntoIterator<Item=TT>,
  FLtV: IntoIterator<Item=FLT>,
  DtV: IntoIterator<Item=DT>,
  NT: Into<ClientName>,
  TT: Into<ClientTag>,
  FLT: Into<ClientImageFeature>,
  DT: Into<(ImgId, ClientDupeType)>
{
  fn from((image, names, tags, features, dupes): (Image, NtV, TtV, FLtV, DtV)) -> Self {
    let mut ci = Self {
      id: image.id,
      filename: image.filename,
      thumbnail: image.thumbnail,
      createdTime: image.created_time,
      filesize: image.filesize,
      hidden: image.hidden,
      favorite: image.favorite,
      isVideo: image.is_video,
      length: image.length,
      marked: image.marked,
      processed: image.processed,
      processedTime: image.processed_time,
      ratingAverage: image.rating_average,
      ratingCount: image.rating_count,
      artistId: image.artist_id,
      groupId: image.group_id,
      features: features.into_iter().map(|n| n.into()).collect(),
      names: names.into_iter().map(|n| n.into()).collect(),
      tags: tags.into_iter().map(|n| n.into()).collect(),
      dupes: dupes.into_iter().map(|n| n.into()).collect(),
    };
    ci.features.sort();
    ci.names.sort();
    ci.tags.sort();
    ci
  }
}

impl From<Name> for ClientName {
  fn from(name: Name) -> Self {
    Self {
      id: name.id,
      value: name.value,
      index: name.index,
      createdTime: name.created_time,
    }
  }
}

impl From<NameDetailsData> for ClientNameDetails {
  fn from((item, group_count, (image_count, score), metadata): NameDetailsData) -> Self {
    Self {
      id: item.id,
      value: item.value,
      index: item.index,
      createdTime: item.created_time,
      groupCount: group_count,
      imageCount: image_count,
      score,
      metadata: metadata.map(|m| m.into())
    }
  }
}

impl From<NameMetadata> for ClientNameMetadata {
  fn from(data: NameMetadata) -> Self {
    Self {
      evecCoefs: data.evec_coefs,
      names: ClientContentRelationships {
        related: izip!(data.high_names, data.high_name_scores).collect(),
        unrelated: izip!(data.low_names, data.low_name_scores).collect(),
      },
      tags: ClientContentRelationships {
        related: izip!(data.high_tags, data.high_tag_scores).collect(),
        unrelated: izip!(data.low_tags, data.low_tag_scores).collect(),
      }
    }
  }
}

impl From<Tag> for ClientTag {
  fn from(tag: Tag) -> Self {
    Self {
      id: tag.id,
      value: tag.value,
      index: tag.index,
      createdTime: tag.created_time,
    }
  }
}

impl From<TagMetadata> for ClientTagMetadata {
  fn from(data: TagMetadata) -> Self {
    Self {
      evecCoefs: data.evec_coefs,
      names: ClientContentRelationships {
        related: izip!(data.high_names, data.high_name_scores).collect(),
        unrelated: izip!(data.low_names, data.low_name_scores).collect(),
      },
      tags: ClientContentRelationships {
        related: izip!(data.high_tags, data.high_tag_scores).collect(),
        unrelated: izip!(data.low_tags, data.low_tag_scores).collect(),
      }
    }
  }
}

impl From<TagDetailsData> for ClientTagDetails {
  fn from((item, group_count, (image_count, score), metadata): TagDetailsData) -> Self {
    Self {
      id: item.id,
      value: item.value,
      index: item.index,
      createdTime: item.created_time,
      groupCount: group_count,
      imageCount: image_count,
      score,
      metadata: metadata.map(|m| m.into())
    }
  }
}

impl Eq for ClientImageFeature {}

impl PartialEq for ClientArtist {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl PartialEq for ClientFeature {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl PartialEq for ClientGroup {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl PartialEq for ClientImageFeature {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl PartialEq for ClientName {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl PartialEq for ClientTag {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
  }
}

impl PartialOrd for ClientArtist {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.value.cmp(&other.value))
  }
}

impl PartialOrd for ClientFeature {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.value.cmp(&other.value))
  }
}

impl PartialOrd for ClientGroup {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.value.cmp(&other.value))
  }
}

impl PartialOrd for ClientImageFeature {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.id.cmp(&other.id))
  }
}

impl PartialOrd for ClientName {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.value.cmp(&other.value))
  }
}

impl PartialOrd for ClientTag {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.value.cmp(&other.value))
  }
}

impl Ord for ClientArtist {
  fn cmp(&self, other: &Self) -> Ordering {
    self.value.cmp(&other.value)
  }
}

impl Ord for ClientFeature {
  fn cmp(&self, other: &Self) -> Ordering {
    self.value.cmp(&other.value)
  }
}

impl Ord for ClientImageFeature {
  fn cmp(&self, other: &Self) -> Ordering {
    self.id.cmp(&other.id)
  }
}

impl Ord for ClientGroup {
  fn cmp(&self, other: &Self) -> Ordering {
    self.value.cmp(&other.value)
  }
}

impl Ord for ClientName {
  fn cmp(&self, other: &Self) -> Ordering {
    self.value.cmp(&other.value)
  }
}

impl Ord for ClientTag {
  fn cmp(&self, other: &Self) -> Ordering {
    self.value.cmp(&other.value)
  }
}
