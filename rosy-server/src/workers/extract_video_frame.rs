use std::{
  path::{Path, PathBuf},
  process::Command,
};

use crate::{
  command::{run_command, Error as CommandError},
  config::file_config::CreateRecord,
};


#[derive(Debug)]
pub enum Error {
  CommandError(CommandError),
  ResultMissingError,
}

pub fn extract_video_frame(src: &Path, dst: &Path, time_seconds: f32, created: &mut CreateRecord<PathBuf>) -> Result<(), Error> {
  run_command(Command::new("ffmpeg")
    .arg("-i")
    .arg(&src)
    .arg("-ss")
    .arg(&time_to_timestamp(time_seconds))
    .arg("-vframes")
    .arg("1")
    .arg(&dst), true)
    .map_err(|e| Error::CommandError(e))
    .and_then(|_| {
      if Path::new(&dst).exists() {
        *created = Some(dst.to_path_buf());
        Ok(())
      } else {
        Err(Error::ResultMissingError)
      }
    })
}

impl std::fmt::Display for Error {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    write!(f, "{}", <&str>::from(self))
  }
}

impl From<&Error> for &'static str {
  fn from(error: &Error) -> &'static str {
    match error {
      Error::CommandError(e) => <&str>::from(e),
      Error::ResultMissingError => "Command successful but result is missing:",
    }
  }
}

fn time_to_timestamp(time_seconds: f32) -> String {
  let mut remainder = time_seconds as i64;
  let hours = remainder / (60 * 60);
  remainder = remainder - hours * (60 * 60);
  let mins = remainder / 60;
  remainder = remainder - mins * 60;
  let secs = remainder;
  let ms = ((time_seconds - ((time_seconds as i64) as f32)) * 1000.0) as i64;
  format!("{:02}:{:02}:{:02}.{:03}", hours, mins, secs, ms)
}



#[cfg(test)]
mod test_time_to_timestamp {
  use super::*;

  #[test]
  fn it_should_handle_sub_second_time() {
    let tests = vec![
      (0.0019, "00:00:00.001"),
      (0.1234, "00:00:00.123"),
      (0.5000, "00:00:00.500"),
      (0.9998, "00:00:00.999"),
    ];
    for (time, expected_stamp) in tests.into_iter() {
      assert_eq!(time_to_timestamp(time), String::from(expected_stamp))
    }
  }

  #[test]
  fn it_should_handle_sub_minute_time() {
    let tests = vec![
      (59.0019, "00:00:59.001"),
      (1.1234, "00:00:01.123"),
      (35.5000, "00:00:35.500"),
      (59.9998, "00:00:59.999"),
    ];
    for (time, expected_stamp) in tests.into_iter() {
      assert_eq!(time_to_timestamp(time), String::from(expected_stamp))
    }
  }

  #[test]
  fn it_should_handle_sub_hour_time() {
    let tests = vec![
      (121.0019, "00:02:01.001"),
      (60.1234, "00:01:00.123"),
      (61.1234, "00:01:01.123"),
      (2135.5000, "00:35:35.500"),
      (3599.9998, "00:59:59.999"),
    ];
    for (time, expected_stamp) in tests.into_iter() {
      assert_eq!(time_to_timestamp(time), String::from(expected_stamp))
    }
  }
}