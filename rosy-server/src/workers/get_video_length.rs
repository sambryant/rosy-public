use std::fmt::Display;
use std::ffi::OsStr;
use std::str::Utf8Error;
use std::num::ParseFloatError;
use std::process::Command;

use crate::command::run_command;
use crate::command::Error as CommandError;


#[derive(Debug)]
pub enum Error {
  ParseOutputStringError(Utf8Error),
  ParseOutputFloatError(ParseFloatError),
  CommandError(CommandError),
}

pub fn get_video_length<T>(source: T) -> Result<f32, Error>
where T: AsRef<OsStr> {
  run_command(Command::new("ffprobe")
    .arg("-i")
    .arg(&source)
    .arg("-show_entries")
    .arg("format=duration")
    .arg("-v")
    .arg("quiet")
    .arg("-of")
    .arg("csv=p=0"), true)
    .map_err(|e| Error::CommandError(e))
    .and_then(|output| {
      std::str::from_utf8(&output.stdout)
        .map_err(|e| Error::ParseOutputStringError(e))
        .map(|s| String::from(s.trim()))
    })
    .and_then(|output_string| {
      output_string.parse::<f32>()
        .map_err(|e| Error::ParseOutputFloatError(e))
    })
}

impl Display for Error {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    write!(f, "{}", String::from(self))
  }
}

impl From<&Error> for String {
  fn from(error: &Error) -> String {
    match error {
      Error::ParseOutputStringError(e) => format!("Failed to parse output as UTF8 string: {:?}", e),
      Error::ParseOutputFloatError(e) => format!("Failed to parse output as float: {:?}", e),
      Error::CommandError(e) => String::from(<&str>::from(e))
    }
  }
}
