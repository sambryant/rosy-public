use std::{
  ffi::OsStr,
  process::Command,
};

use crate::prelude::*;
use crate::{command, models::ImgId};

pub fn generate_image_index<T>(cxt: &Context, id: ImgId, sys_filename: T) -> Result<(), String>
where T: AsRef<OsStr> {
  let id_str = id.to_string();
  let args = [
    "-m".as_ref(),
    "rosy.workers.generate_image_index".as_ref(),
    cxt.config.db.database.as_ref(),
    cxt.schema.as_ref(),
    id_str.as_ref(),
    sys_filename.as_ref(),
  ];
  match command::run_command(Command::new("python3").args(&args), true) {
    Ok(_) => Ok(()),
    Err(command::Error::BadCode(2)) => Err("Target image does not exist".to_string()),
    Err(command::Error::BadCode(_)) => Err("Something went wrong".to_string()),
    Err(e) => Err(<&str>::from(&e).to_string())
  }
}
