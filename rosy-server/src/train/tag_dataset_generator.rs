use diesel::prelude::*;
use itertools::izip;
use std::path::PathBuf;

use crate::prelude::*;
use crate::{
  api::requests::GenerateTagDatasetRequest,
  error::Error as ClientError,
  config::{
    DBType,
    ThreadId,
  },
  constants::DEFAULT_TRAIN_SIZE,
  models::{Image, ImageTag},
  schema::image,
  workflow::{
    importer::thumbnail::generate_thumbnail,
    thread_worker::ThreadWorker,
  },
};
use super::{
  DatasetGenerator,
  DatasetGeneratorError,
  dataset_record::v1::{DatasetRecordV1, EntryRecordData},
};

pub struct TagDatasetGenerator {
  pub dataset_name: Option<String>,
  pub use_existing_images: bool,
  pub include_hidden: bool,
  pub include_unprocessed: bool,
  pub train_size: u32,
}

impl From<GenerateTagDatasetRequest> for TagDatasetGenerator {
  fn from(req: GenerateTagDatasetRequest) -> Self {
    Self {
      dataset_name: req.datasetName,
      use_existing_images: req.useExistingImages.unwrap_or(false),
      include_hidden: req.includeHidden.unwrap_or(false),
      include_unprocessed: req.includeUnprocessed.unwrap_or(false),
      train_size: req.trainSize.map(|i| i as u32).unwrap_or(DEFAULT_TRAIN_SIZE)
    }
  }
}

impl ThreadWorker for TagDatasetGenerator {
  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    self._do_work(cxt, thread_id)
  }
}

impl DatasetGenerator for TagDatasetGenerator {
  type Associated = Vec<ImageTag>;
  type Record = DatasetRecordV1;

  fn get_dataset_name(&self) -> Option<String> { self.dataset_name.clone() }

  fn create_dataset_record(&self) -> Self::Record {
    DatasetRecordV1::new()
  }

  fn get_use_existing_images(&self) -> bool { self.use_existing_images }

  fn get_query<'a>(&'a self) -> image::BoxedQuery<'a, DBType> {
    let mut query = image::table.into_boxed()
      .filter(image::deleted.eq(false))
      .filter(image::is_video.eq(false));
    if !self.include_hidden {
      query = query.filter(image::hidden.eq(false));
    }
    if !self.include_unprocessed {
      query = query.filter(image::processed.eq(true));
    }
    query
  }

  fn get_associated(&self, conn: &DBConn, images: Vec<Image>) -> Result<Vec<(Image, Self::Associated)>, ClientError> {
    let tags = ImageTag::belonging_to(&images)
      .load::<ImageTag>(conn)?
      .grouped_by(&images);
    Ok(izip!(images, tags).collect())
  }

  fn generate_example_image(&self, src_file: &PathBuf, dst_file: &PathBuf, created_file: &mut Option<PathBuf>, _: &Self::Associated) -> Result<(), DatasetGeneratorError> {
    Ok(generate_thumbnail(src_file, dst_file, created_file, (self.train_size, self.train_size))?)
  }

}

impl From<Vec<ImageTag>> for EntryRecordData {
  fn from(tags: Vec<ImageTag>) -> Self {
    EntryRecordData::tag {
      tags: tags.into_iter().map(|i| i.tag_id).collect()
    }
  }
}
