use std::{
  fs,
  path::PathBuf,
};

use crate::prelude::*;
use crate::{
  constants::TRAIN_RECORD_FILENAME,
  models::Image,
  workflow::fileops::{
    remove_file_ignored,
    PendingDelete,
  },
};
use super::dataset_record::{
  AddEntry,
  DatasetRecord,
};

/// Represents a set of images and associated data used in building an ML training set.
///
/// This object is basically a builder - it is used to construct the database. Once the construction
/// is complete, the dataset is written to the database with `write_record_file`.
pub struct DatasetBuilder<T>
where T: DatasetRecord {
  /// Directory that dataset example images are written to.
  pub output_dir: PathBuf,
  /// File operations which may be rolled back on failure or finalized on success.
  fileops: DatasetBuilderFileops,
  /// Dataset record data stored in form suitable for JSON serialization.
  record: T,
}

/// Error incurred while writing the JSON training dataset file.
#[derive(Debug)]
pub enum WriteDatasetRecordError {
  OpenRecordFile(std::io::Error),
  JSON(serde_json::Error),
}

/// Holds file operations performed while building a training dataset.
///
/// At the end of building the dataset, these may be either rolled back (on failure) or finalized
/// (on success).
pub struct DatasetBuilderFileops {
  created_files: Vec<PathBuf>,
  deleted_files: Vec<PendingDelete>,
}

impl<S: DatasetRecord> DatasetBuilder<S> {

  pub fn new(cfg: &Config, name: &Option<String>, record: S) -> Result<Self, std::io::Error> {
    let name = match name {
      Some(n) => n.clone(),
      None => cfg.file.generate_new_file_id().0.to_string(),
    };
    let output_dir = cfg.file.get_train_dir()?.join(name.clone()).join("raw");
    fs::create_dir_all(&output_dir)?;
    Ok(Self {
      output_dir,
      record,
      fileops: DatasetBuilderFileops {
        created_files: Vec::new(),
        deleted_files: Vec::new(),
      },
    })
  }

  /// Writes the dataset record file to a JSON file.
  ///
  /// On both sucess and failure it returns the associated file operations for this dataset
  /// (`TrainDatasetRollback`). On success, these should be finalized (i.e. finalize `PendingDelete`
  /// operations). On failure, these should be rolled back - undoing `PendingDelete`, removing
  /// created files.
  pub fn write_record_file(self) -> Result<DatasetBuilderFileops, (WriteDatasetRecordError, DatasetBuilderFileops)> {
    let filename = self.output_dir.join(TRAIN_RECORD_FILENAME);
    match fs::File::create(&filename) {
      Err(e) => Err((WriteDatasetRecordError::OpenRecordFile(e), self.fileops)),
      Ok(file) => {
        let (record, fileops) = (self.record, self.fileops);
        match serde_json::to_writer(&file, &record) {
          Err(e) => Err((WriteDatasetRecordError::JSON(e), fileops)),
          Ok(_) => {
            log_msg(format!("Created train record file: {:?}", filename));
            Ok(fileops)
          },
        }
      }
    }
  }

  pub fn add_entry<T>(&mut self, local_filename: String, target_image: &Image, data: T, deleted_file: Option<PendingDelete>, created_file: Option<PathBuf>)
  where S: AddEntry<T> {
    self.record.add_entry(local_filename, target_image, data);
    if let Some(image_delete) = deleted_file {
      self.fileops.deleted_files.push(image_delete);
    }
    if let Some(image_create) = created_file {
      self.fileops.created_files.push(image_create.clone());
    }
  }

}

impl DatasetBuilderFileops {

  pub fn finalize(self) {
    for pd in self.deleted_files.into_iter() {
      pd.purge_ignored();
    }
  }

  pub fn rollback(self) {
    let (created, deleted) = (self.created_files, self.deleted_files);
    for f in created.into_iter() {
      remove_file_ignored(f);
    }
    for pd in deleted.into_iter() {
      pd.undo_ignored();
    }
  }

}
