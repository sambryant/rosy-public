use diesel::prelude::*;
use std::path::{
  Path,
  PathBuf,
};

use crate::prelude::*;
use crate::{
  error::Error as ClientError,
  config::{
    DBType,
    ThreadId,
  },
  constants::THUMBNAIL_FMT,
  models::Image,
  schema::image,
  threads::{update_thread, ThreadStatus},
  workflow::{
    fileops::{
      MarkForDeletionError,
      PendingDelete,
    },
    importer::thumbnail::GenerateThumbnailError,
  },
};
use super::{
  dataset_builder::DatasetBuilder,
  dataset_record::{
    AddEntry,
    DatasetRecord,
  },
};

#[derive(Debug)]
pub enum Error {
  RemoveOldImage(MarkForDeletionError),
  BadFilename,
  GenerateImage(GenerateThumbnailError),
}

pub trait DatasetGenerator
where Self: Sized {
  /// Data loaded along with Image in use for building dataset. This could be e.g. Vec<ImageTag> or
  /// ImageFeature.
  type Associated;
  /// Record type used to store dataset in JSON. This is a a type implementing DatasetRecord which
  /// has a defined `add_entry` method for the `Associated` type.
  type Record: DatasetRecord + AddEntry<Self::Associated>;

  /// Simple accessor for self.dataset_name.
  fn get_dataset_name(&self) -> Option<String>;

  /// Simple accessor for self.use_existing_images.
  fn get_use_existing_images(&self) -> bool;

  /// Simple accessor for self.dataset_name.
  fn get_query<'a>(&'a self) -> image::BoxedQuery<'a, DBType>;

  /// Gets an Image query matching the filters in this request.
  fn get_associated(&self, conn: &DBConn, images: Vec<Image>) -> Result<Vec<(Image, Self::Associated)>, ClientError>;

  /// Generates and saves an image file for the given dataset example.
  fn generate_example_image(&self, src_file: &PathBuf, dst_file: &PathBuf, created_file: &mut Option<PathBuf>, associated: &Self::Associated) -> Result<(), Error>;

  /// Gets the local filename corresponding to the given dataset example.
  fn get_example_filename(&self, cxt: &Context, image: &Image, _: &Self::Associated) -> Result<String, Error> {
    let file_id = cxt.config.file.extract_file_id(&image.filename).map_err(|_| Error::BadFilename)?;
    let local_filename = format!("{}.{}", file_id.0, THUMBNAIL_FMT);
    Ok(local_filename)
  }

  /// Processes the given dataset example by generating a corresponding image file and saving the
  /// information to the dataset record.
  fn process_dataset_example(&self, cxt: &Context, dataset: &mut DatasetBuilder<Self::Record>, image: &Image, associated: Self::Associated) -> Result<(), Error> {
    let dst_filename_local = self.get_example_filename(cxt, &image, &associated)?;
    let dst_filename = dataset.output_dir.join(&dst_filename_local);

    // Keep track of file operations so they can be rolled back or finalized.
    let mut created_file: Option<PathBuf> = None;
    let mut deleted_file: Option<PendingDelete> = None;

    // If the target example image already exists, either remove it, or re-use it.
    if Path::new(&dst_filename).exists() {
      if self.get_use_existing_images() {
        return Ok(dataset.add_entry(dst_filename_local, image, associated, deleted_file, created_file));
      } else {
        deleted_file = Some(PendingDelete::new(&dst_filename).map_err(|e| Error::RemoveOldImage(e))?);
      }
    }

    // Generate the dataset example image file.
    let src_filename = cxt.config.file.get_filename_system(&image.filename);
    match self.generate_example_image(&src_filename, &dst_filename, &mut created_file, &associated) {
      Ok(_) => {
        // On success, add the dataset example information to the record.
        dataset.add_entry(dst_filename_local, image, associated, deleted_file, created_file);
        Ok(())
      },
      Err(e) => {
        // On failure, undo any file operations and return an error.
        if let Some(file) = created_file {
          cxt.config.file.cleanup_file(&file);
        }
        if let Some(image_delete) = deleted_file {
          image_delete.undo_ignored();
        }
        Err(e)
      }
    }
  }

  fn create_dataset_record(&self) -> Self::Record;

  /// Performs the logic used in `ThreadWorker`.
  ///
  /// Roughly this works in the following steps
  ///   1. Query all images which match the filters given in the struct implementing this trait.
  ///   2. Query all associated data (e.g. ImageTag) needed to build the dataset.
  ///   3. Organize the images/data into dataset examples
  ///   4. For each example, generate an image file and save information to dataset record.
  ///   5. Save the dataset record as a JSON file.
  fn _do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    let mut dataset = DatasetBuilder::new(
      &cxt.config, &self.get_dataset_name(), self.create_dataset_record()
    ).map_err(|e| ClientError::internal("Couldn't create new train dataset").attach(e))?;

    let conn = cxt.make_conn()?;
    let images = self.get_query().load::<Image>(&conn)?;
    let total = images.len();
    let mut data = self.get_associated(&conn, images)?;
    let mut count = 0;
    while let Some((image, associated)) = data.pop() {
      update_thread(&cxt.config, thread_id, ThreadStatus::progress(count, total)).ok();
      if let Err(e) = self.process_dataset_example(cxt, &mut dataset, &image, associated) {
        log_err(format!("Generate train image failed for image {}: {:?}", image.id, e));
      }
      count = count + 1;
    }
    match dataset.write_record_file() {
      Ok(fileops) => {
        fileops.finalize();
        Ok(())
      },
      Err((e, fileops)) => {
        log_errs(&vec![
          format!("Failed to write train record file!"),
          format!("Rolling back file changes")]);
        fileops.rollback();
        Err(ClientError::internal("Failed to generate train image record file, undoing operations").attach(e))
      }
    }
  }

}

impl From<GenerateThumbnailError> for Error {
  fn from(e: GenerateThumbnailError) -> Self {
    Error::GenerateImage(e)
  }
}
