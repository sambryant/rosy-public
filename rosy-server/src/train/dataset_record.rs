use serde::{Deserialize, Serialize};

use crate::{
  common::get_utc_timestamp,
  models::{Id, Image, ImageFeature, ImageName, ImageTag},
};

pub trait AddEntry<T> {
  fn add_entry(&mut self, local_filename: String, img: &Image, data: T);
}

pub trait DatasetRecord: Sized + Serialize {}

pub mod v1 {
  use super::*;

  /// Struct used to store training dataset in JSON.
  #[derive(Deserialize, Serialize)]
  pub struct DatasetRecordV1 {
    records: Vec<EntryRecord>,
    created_time: i64,
  }

  /// Struct used to store single dataset example in JSON.
  #[derive(Deserialize, Serialize)]
  struct EntryRecord {
    filename: String,
    image_id: Id,
    data: EntryRecordData,
  }

  /// Struct used to store type-specific data for single dataset example in JSON.
  #[derive(Deserialize, Serialize)]
  #[serde(tag = "type")]
  #[allow(non_camel_case_types)]
  pub enum EntryRecordData {
    feature {
      is_null: bool,
      feature_id: Option<Id>,
      image_feature_id: Option<Id>,
      x1: f32,
      x2: f32,
      y1: f32,
      y2: f32,
    },
    tag {
      tags: Vec<Id>,
    },
  }

  impl<T> AddEntry<T> for DatasetRecordV1 where T: Into<EntryRecordData> {
    fn add_entry(&mut self, local_filename: String, img: &Image, data: T) {
      self.records.push(EntryRecord {
        filename: local_filename,
        image_id: img.id,
        data: data.into()
      })
    }
  }

  impl DatasetRecordV1 {
    pub fn new() -> Self {
      Self {
        created_time: get_utc_timestamp() as i64,
        records: Vec::new(),
      }
    }
  }

  impl DatasetRecord for DatasetRecordV1 {}

}

pub mod v2 {
  use super::*;

  /// Struct used to store training dataset in JSON.
  #[derive(Deserialize, Serialize)]
  pub struct DatasetRecordV2 {
    imgsize: u32,
    records: Vec<EntryRecord>,
    created_time: i64,
  }

  #[derive(Deserialize, Serialize)]
  struct EntryRecord {
    filename: String,
    image_id: Id,
    rating_average: Option<f32>,
    hidden: bool,
    processed: bool,
    artist: Option<Id>,
    features: Vec<FeatureRecord>,
    names: Vec<Id>,
    tags: Vec<Id>,
  }

  #[derive(Deserialize, Serialize)]
  pub struct FeatureRecord {
    feature_id: Id,
    image_feature_id: Id,
    x1: f32,
    x2: f32,
    y1: f32,
    y2: f32,
  }

  impl AddEntry<(Vec<ImageFeature>, Vec<ImageName>, Vec<ImageTag>)> for DatasetRecordV2 {
    fn add_entry(&mut self, local_filename: std::string::String, img: &Image, data: (Vec<ImageFeature>, Vec<ImageName>, Vec<ImageTag>)) {
      let (features, names, tags) = data;
      self.records.push(EntryRecord {
        filename: local_filename,
        image_id: img.id,
        rating_average: img.rating_average,
        hidden: img.hidden,
        processed: img.processed,
        artist: img.artist_id,
        features: features.into_iter().map(|v| FeatureRecord {
          feature_id: v.feature_id,
          image_feature_id: v.id,
          x1: v.x1,
          x2: v.x2,
          y1: v.y1,
          y2: v.y2,
        }).collect(),
        names: names.into_iter().map(|i| i.nme_id).collect(),
        tags: tags.into_iter().map(|i| i.tag_id).collect(),
      })
    }
  }

  impl DatasetRecordV2 {
    pub fn new(imgsize: u32) -> Self {
      Self {
        imgsize,
        created_time: get_utc_timestamp() as i64,
        records: Vec::new(),
      }
    }
  }

  impl DatasetRecord for DatasetRecordV2 {}
}
