use diesel::prelude::*;
use itertools::izip;
use std::path::PathBuf;

use crate::prelude::*;
use crate::{
  api::requests::GenerateDatasetRequest,
  error::Error as ClientError,
  config::{
    DBType,
    ThreadId,
  },
  constants::DEFAULT_TRAIN_SIZE,
  models::{Image, ImageFeature, ImageName, ImageTag},
  schema::image,
  workflow::{
    importer::thumbnail::generate_thumbnail,
    thread_worker::ThreadWorker,
  },
};
use super::{
  DatasetGenerator,
  DatasetGeneratorError,
  dataset_record::v2::DatasetRecordV2,
};

pub struct FullDatasetGenerator {
  pub dataset_name: Option<String>,
  pub use_existing_images: bool,
  pub include_hidden: bool,
  pub include_unprocessed: bool,
  pub train_size: u32,
}

impl From<GenerateDatasetRequest> for FullDatasetGenerator {
  fn from(req: GenerateDatasetRequest) -> Self {
    Self {
      dataset_name: req.datasetName,
      use_existing_images: req.useExistingImages.unwrap_or(false),
      include_hidden: req.includeHidden.unwrap_or(false),
      include_unprocessed: req.includeUnprocessed.unwrap_or(false),
      train_size: req.trainSize.map(|i| i as u32).unwrap_or(DEFAULT_TRAIN_SIZE)
    }
  }
}

impl ThreadWorker for FullDatasetGenerator {
  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    self._do_work(cxt, thread_id)
  }
}

impl DatasetGenerator for FullDatasetGenerator {
  type Associated = (Vec<ImageFeature>, Vec<ImageName>, Vec<ImageTag>);
  type Record = DatasetRecordV2;

  fn get_dataset_name(&self) -> Option<String> { self.dataset_name.clone() }

  fn create_dataset_record(&self) -> Self::Record {
    DatasetRecordV2::new(self.train_size)
  }

  fn get_use_existing_images(&self) -> bool { self.use_existing_images }

  /// Gets an Image query matching the filters in this request.
  fn get_query<'a>(&'a self) -> image::BoxedQuery<'a, DBType> {
    let mut query = image::table.into_boxed()
      .filter(image::deleted.eq(false))
      .filter(image::is_video.eq(false));
    if !self.include_hidden {
      query = query.filter(image::hidden.eq(false));
    }
    if !self.include_unprocessed {
      query = query.filter(image::processed.eq(true));
    }
    query
  }

  /// Returns a list of pairs (Image, ImageFeature) corresponding to the training dataset examples.
  fn get_associated(&self, conn: &DBConn, images: Vec<Image>) -> Result<Vec<(Image, Self::Associated)>, ClientError> {
    let features: Vec<Vec<ImageFeature>> = ImageFeature::belonging_to(&images)
      .load::<ImageFeature>(conn)?
      .grouped_by(&images);
    let names: Vec<Vec<ImageName>> = ImageName::belonging_to(&images)
      .load::<ImageName>(conn)?
      .grouped_by(&images);
    let tags: Vec<Vec<ImageTag>> = ImageTag::belonging_to(&images)
      .load::<ImageTag>(conn)?
      .grouped_by(&images);
    Ok(izip!(images, izip!(features, names, tags)).collect())
  }

  fn generate_example_image(&self, src_file: &PathBuf, dst_file: &PathBuf, created_file: &mut Option<PathBuf>, _: &Self::Associated) -> Result<(), DatasetGeneratorError> {
    Ok(generate_thumbnail(src_file, dst_file, created_file, (self.train_size, self.train_size))?)
  }
}
