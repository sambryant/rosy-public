use diesel::{dsl::exists, dsl::not, prelude::*};
use itertools::izip;
use std::collections::HashSet;
use std::path::PathBuf;

use crate::prelude::*;
use crate::{
  api::requests::GenerateFeatureDatasetRequest,
  error::Error as ClientError,
  config::{
    DBType,
    ThreadId,
  },
  constants::THUMBNAIL_FMT,
  models::{Image, ImageFeature},
  schema::{
    image as image_schema,
    image_feature,
    image_tag,
  },
  workflow::thread_worker::ThreadWorker,
};
use super::{
  DatasetGenerator,
  DatasetGeneratorError,
  dataset_record::v1::{DatasetRecordV1, EntryRecordData},
};

pub struct FeatureDatasetGenerator {
  pub dataset_name: Option<String>,
  pub use_existing_images: bool,
  pub include_hidden: bool,
  pub include_unprocessed: bool,
  pub features: HashSet<i32>,
  pub exclude_tags: Vec<i32>,
  pub include_tags: Vec<i32>,
  /// When true, will create null features consisting of randomly cropped portion of images.
  /// This guarantees that there is no overlap with actual features.
  pub generate_null_data: bool,
  /// List of features that will be considered null
  pub null_features: HashSet<i32>,
  queried_features: HashSet<i32>,
}

pub enum ImageFeatureExample {
  Real(ImageFeature),
  Null(NullImageFeature)
}

pub struct NullImageFeature {
  pub image_id: i32,
  pub count: i32,
  pub x1: f32,
  pub x2: f32,
  pub y1: f32,
  pub y2: f32,
}

impl From<GenerateFeatureDatasetRequest> for FeatureDatasetGenerator {
  fn from(req: GenerateFeatureDatasetRequest) -> Self {
    let mut r = Self {
      dataset_name: req.datasetName,
      use_existing_images: req.useExistingImages.unwrap_or(false),
      include_hidden: req.includeHidden.unwrap_or(false),
      include_unprocessed: req.includeUnprocessed.unwrap_or(false),
      features: HashSet::from_iter(req.features.unwrap_or(Vec::new()).into_iter()),
      exclude_tags: req.excludeTags.unwrap_or(Vec::new()),
      include_tags: req.includeTags.unwrap_or(Vec::new()),
      generate_null_data: req.generateNullData.unwrap_or(false),
      null_features: HashSet::from_iter(req.nullFeatures.unwrap_or(Vec::new()).into_iter()),
      queried_features: HashSet::new(),
    };
    if r.features.len() > 0 {
      for fid in r.features.iter() {
        r.queried_features.insert(*fid);
      }
      for fid in r.null_features.iter() {
        r.queried_features.insert(*fid);
      }
    }
    r
  }
}

impl ThreadWorker for FeatureDatasetGenerator {
  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    self._do_work(cxt, thread_id)
  }
}

impl DatasetGenerator for FeatureDatasetGenerator {
  type Associated = ImageFeatureExample;
  type Record = DatasetRecordV1;

  fn get_dataset_name(&self) -> Option<String> { self.dataset_name.clone() }

  fn create_dataset_record(&self) -> Self::Record {
    DatasetRecordV1::new()
  }

  fn get_use_existing_images(&self) -> bool { self.use_existing_images }

  /// Gets an Image query matching the filters in this request.
  fn get_query<'a>(&'a self) -> image_schema::BoxedQuery<'a, DBType> {
    let mut query = image_schema::table.into_boxed()
      .filter(image_schema::deleted.eq(false))
      .filter(image_schema::is_video.eq(false));
    if !self.include_hidden {
      query = query.filter(image_schema::hidden.eq(false));
    }
    if !self.include_unprocessed {
      query = query.filter(image_schema::processed.eq(true));
    }
    if self.queried_features.len() > 0 {
      // Only return images which have an image_feature explicitly marked as selected or null.
      query = query.filter(exists(
        image_feature::table
          .filter(image_feature::image_id.eq(image_schema::id))
          .filter(image_feature::feature_id.eq_any(&self.queried_features))
          .limit(1),
      ));
    } else {
      // Only return images which have any image_feature.
      query = query.filter(exists(
        image_feature::table
          .filter(image_feature::image_id.eq(image_schema::id))
          .limit(1),
      ));
    }
    if self.include_tags.len() > 0 {
      for id in &self.include_tags {
        query = query.filter(exists(
          image_tag::table
            .filter(image_tag::img_id.eq(image_schema::id))
            .filter(image_tag::tag_id.eq(id))
            .limit(1),
        ));
      }
    }
    if self.exclude_tags.len() > 0 {
      for id in &self.exclude_tags {
        query = query.filter(not(exists(
          image_tag::table
            .filter(image_tag::img_id.eq(image_schema::id))
            .filter(image_tag::tag_id.eq(id))
            .limit(1),
        )));
      }
    }
    query
  }

  /// Gets the local filename for a given training example image.
  fn get_example_filename(&self, cxt: &Context, image: &Image, feature: &ImageFeatureExample) -> Result<String, DatasetGeneratorError> {
    let file_id = cxt.config.file.extract_file_id(&image.filename)
      .map_err(|_| DatasetGeneratorError::BadFilename)?;
    match feature {
      ImageFeatureExample::Real(f) => Ok(format!("{}-{}.{}", file_id.0, f.id, THUMBNAIL_FMT)),
      ImageFeatureExample::Null(f) => Ok(format!("{}-null-{}.{}", file_id.0, f.count, THUMBNAIL_FMT)),
    }
  }

  /// Returns a list of pairs (Image, ImageFeature) corresponding to the training dataset examples.
  fn get_associated(&self, conn: &DBConn, images: Vec<Image>) -> Result<Vec<(Image, Self::Associated)>, ClientError> {
    let img_ids: Vec<i32> = images.iter().map(|i| i.id).collect();
    let mut feature_query = image_feature::table.into_boxed()
      .filter(image_feature::image_id.eq_any(&img_ids));
    if self.queried_features.len() > 0 {
      feature_query = feature_query
        .filter(image_feature::feature_id.eq_any(&self.queried_features));
    }
    let features = feature_query
      .load::<ImageFeature>(conn)?
      .grouped_by(&images);
    let features = self.split_real_and_null(features);
    let data = izip!(images, features).collect();

    if self.generate_null_data {
      Ok(self.generate_null_examples(data))
    } else {
      // For each ImageFeature, return a pair (Image, ImageFeature) since this corresponds to a single
      // training example.
      let mut output = Vec::new();
      for (image, (real_features, null_features)) in data.into_iter() {
        for feature in real_features.into_iter() {
          output.push((image.clone(), ImageFeatureExample::Real(feature)));
        }
        for feature in null_features.into_iter() {
          output.push((image.clone(), ImageFeatureExample::Null(feature)));
        }
      }
      Ok(output)
    }
  }

  /// Generates an image for the given ImageFeature. This is a cropped version of the image cut off
  /// at the bounds specified in ImageFeature.
  fn generate_example_image(&self, src_file: &PathBuf, dst_file: &PathBuf, created_file: &mut Option<PathBuf>, feature: &ImageFeatureExample) -> Result<(), DatasetGeneratorError> {
    use crate::workflow::importer::thumbnail::GenerateThumbnailError::*;
    use image::io::Reader;
    let img = Reader::open(&src_file)
      .map_err(|e| ImageOpen(e))?
      .with_guessed_format()
      .map_err(|e| ImageFormat(e))?
      .decode()
      .map_err(|e| ImageDecode(e))?;
    let w = img.width() as f32;
    let h = img.height() as f32;
    let (x1, x2) = feature.get_xs();
    let (y1, y2) = feature.get_ys();
    let x = (x1 * w).floor() as u32;
    let y = (y1 * h).floor() as u32;
    let tw = ((x2 - x1)*w).round() as u32;
    let th = ((y2 - y1)*h).round() as u32;
    let img = img.crop_imm(x, y, tw, th);

    // Even on failure, mark the file as having been created just in case its partially created.
    *created_file = Some(dst_file.clone());
    img.save(dst_file).map_err(|e| ThumbSave(e))?;
    Ok(())
  }
}

impl FeatureDatasetGenerator {

  fn split_real_and_null(&self, features: Vec<Vec<ImageFeature>>) -> Vec<(Vec<ImageFeature>, Vec<NullImageFeature>)> {
    features.into_iter()
      .map(|fs| {
        let mut img_real_features = Vec::new();
        let mut img_null_features = Vec::new();
        for feature in fs.into_iter() {
          if self.null_features.contains(&feature.feature_id) {
            img_null_features.push((img_null_features.len(), feature).into());
          } else if self.features.len() > 0 {
            if self.features.contains(&feature.feature_id) {
              img_real_features.push(feature);
            }
          } else {
            img_real_features.push(feature);
          }
        }
        (img_real_features, img_null_features)
      })
      .collect()
  }

  fn find_null_example(image_id: i32, size_basis: &null_math::Rect, real: &Vec<ImageFeature>, null: &Vec<NullImageFeature>) -> Option<NullImageFeature> {
    let count = null.len();
    let mut rects: Vec<null_math::Rect> = real.iter().map(|r| r.into()).collect();
    for n in null.iter() {
      rects.push(n.into());
    }
    if let Some(rect) = null_math::find_new_rect(&size_basis, &rects, 20) {
      Some(NullImageFeature {
        image_id,
        count: count as i32,
        x1: rect.a.x,
        x2: rect.b.x,
        y1: rect.a.y,
        y2: rect.b.y,
      })
    } else {
      None
    }
  }

  fn generate_null_examples(&self, data: Vec<(Image, (Vec<ImageFeature>, Vec<NullImageFeature>))>) -> Vec<(Image, ImageFeatureExample)> {
    let mut real_count = 0;
    let mut null_count = 0;
    let mut output = Vec::new();
    for (img, (real_features, mut null_features)) in data.into_iter() {
      null_count += null_features.len();
      real_count += real_features.len();
      if null_count < real_count {
        let image_id = img.id;
        let size_basis = if real_features.len() > 0 { (&real_features[0]).into() } else { (&null_features[0]).into() };
        while null_count < real_count {
          if let Some(null) = FeatureDatasetGenerator::find_null_example(image_id, &size_basis, &real_features, &null_features) {
            null_features.push(null);
            null_count += 1;
          } else {
            break;
          }
        }
      }
      for f in real_features.into_iter() {
        output.push((img.clone(), ImageFeatureExample::Real(f)));
      }
      for f in null_features.into_iter() {
        output.push((img.clone(), ImageFeatureExample::Null(f)));
      }
    }
    output
  }

}

mod null_math {
  use crate::models::ImageFeature;
  use rand::Rng;
  use super::NullImageFeature;

  pub struct Point {
    pub x: f32,
    pub y: f32,
  }
  pub struct Rect {
    pub a: Point,
    pub b: Point,
  }

  impl From<&ImageFeature> for Rect {
    fn from(f: &ImageFeature) -> Rect {
      Rect {
        a: Point { x: f.x1, y: f.y1 },
        b: Point { x: f.x2, y: f.y2 },
      }
    }
  }

  impl From<&NullImageFeature> for Rect {
    fn from(f: &NullImageFeature) -> Rect {
      Rect {
        a: Point { x: f.x1, y: f.y1 },
        b: Point { x: f.x2, y: f.y2 },
      }
    }
  }

  impl Rect {
    fn overlaps(&self, other: &Rect) -> bool {
      if other.a.x > self.b.x || other.b.x < self.a.x {
        false
      } else if other.a.y > self.b.y || other.b.y < self.a.y {
        false
      } else {
        true
      }
    }
    fn overlaps_any(&self, other: &Vec<Rect>) -> bool {
      for o in other.iter() {
        if self.overlaps(o) {
          return true;
        }
      }
      false
    }
  }

  fn propose(width: f32, height: f32) -> Rect {
    let mut rng = rand::thread_rng();
    let x = (1.0 - width) * rng.gen::<f32>();
    let y = (1.0 - height) * rng.gen::<f32>();
    Rect {
      a: Point { x, y },
      b: Point { x: x + width, y: y + height}
    }
  }

  pub fn find_new_rect(base: &Rect, rects: &Vec<Rect>, tries: usize) -> Option<Rect> {
    let nw = base.b.x - base.a.x;
    let nh = base.b.y - base.a.y;
    let mut count = 0;
    while count < tries {
      let proposed = propose(nw, nh);
      if !proposed.overlaps_any(rects) {
        return Some(proposed);
      }
      count = count + 1;
    }
    None
  }

}

impl ImageFeatureExample {
  pub fn get_xs(&self) -> (f32, f32) {
    match self {
      ImageFeatureExample::Real(f) => {(f32::min(f.x1, f.x2), f32::max(f.x1, f.x2))},
      ImageFeatureExample::Null(f) => {(f32::min(f.x1, f.x2), f32::max(f.x1, f.x2))},
    }
  }
  pub fn get_ys(&self) -> (f32, f32) {
    match self {
      ImageFeatureExample::Real(f) => {(f32::min(f.y1, f.y2), f32::max(f.y1, f.y2))},
      ImageFeatureExample::Null(f) => {(f32::min(f.y1, f.y2), f32::max(f.y1, f.y2))},
    }
  }
}

impl From<(usize, ImageFeature)> for NullImageFeature {
  fn from((i, f): (usize, ImageFeature)) -> Self {
    Self {
      image_id: f.image_id,
      count: i as i32,
      x1: f.x1, x2: f.x2, y1: f.y1, y2: f.y2
    }
  }
}

impl From<ImageFeature> for EntryRecordData {
  fn from(feature: ImageFeature) -> Self {
    EntryRecordData::feature {
      is_null: false,
      feature_id: Some(feature.feature_id),
      image_feature_id: Some(feature.id),
      x1: feature.x1,
      x2: feature.x2,
      y1: feature.y1,
      y2: feature.y2,
    }
  }
}

impl From<ImageFeatureExample> for EntryRecordData {
  fn from(feature: ImageFeatureExample) -> Self {
    match feature {
      ImageFeatureExample::Real(f) => f.into(),
      ImageFeatureExample::Null(f) => {
        EntryRecordData::feature {
          is_null: true,
          feature_id: None,
          image_feature_id: None,
          x1: f.x1,
          x2: f.x2,
          y1: f.y1,
          y2: f.y2,
        }
      }
    }
  }
}
