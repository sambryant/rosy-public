use std::collections::HashMap;
use std::iter::Iterator;


pub trait GroupedWith<C>: IntoIterator<Item = C> + Sized {
  ///
  /// Returns a list of child objects grouped by the parent objects, similar to diesel's `group_by`.
  ///
  /// # Arguments
  ///
  /// * `parent_id_fn` - a function which returns the parent's id from a parent object.
  ///
  /// * `child_parent_id_fn` - a function which returns the parent's id from a child object.
  ///
  /// # Examples
  ///
  /// ```
  /// let parents = vec![(42, "another"), (1, "one")];
  /// let children = vec![(1, 1), (2, 1), (3, 1), (4, 42), (8, 42)];
  /// let grouped = children.grouped_with(&parents, |p| p.0, |c| c.1);
  /// assert_eq!(grouped, vec![[(4, 42), (8, 42)], [(1, 1), (2, 1), (3, 1)]]);
  /// ```
  fn grouped_with<P, FP, FC>(self, parents: &[P], parent_id_fn: FP, child_parent_id_fn: FC) -> Vec<Vec<C>>
    where FP: Fn(&P) -> i32,
          FC: Fn(&C) -> i32;
}

impl<C, Iter> GroupedWith<C> for Iter
where Iter: IntoIterator<Item = C> {
  fn grouped_with<P, FP, FC>(self, parents: &[P], parent_id_fn: FP, child_parent_id_fn: FC) -> Vec<Vec<C>>
    where FP: Fn(&P) -> i32,
          FC: Fn(&C) -> i32
  {
    let parent_id_map: HashMap<i32, usize> = parents.iter()
      .enumerate()
      .map(|(index, parent)| (parent_id_fn(parent), index))
      .collect();
    let mut result: Vec<Vec<C>> = (0..parent_id_map.len()).map(|_| Vec::new()).collect();
    for child in self {
      let id = child_parent_id_fn(&child);
      if let Some(index) = parent_id_map.get(&id) {
        result[*index].push(child);
      }
    }
    result
  }
}

#[cfg(test)]
mod test_grouped_with {
  use super::*;

  #[allow(dead_code)]
  struct Parent {
    id: i32,
    field: &'static str,
  }

  #[allow(dead_code)]
  struct Child {
    id: i32,
    parent_id: i32,
    name: &'static str,
  }

  impl Parent {
    pub fn new(id: i32, field: &'static str) -> Parent {
      Parent { id, field }
    }
  }
  impl Child {
    pub fn new(id: i32, parent_id: i32, name: &'static str) -> Child {
      Child { id, parent_id, name }
    }
  }

  #[test]
  fn it_groups_children_by_parents() {
    let parents = vec![(1, "first"), (2, "second"), (3, "third")];
    let children = vec![(1, 1), (2, 1), (3, 1), (4, 2), (5, 2), (6, 3)];
    let exp_grouped = vec![
      vec![(1, 1), (2, 1), (3, 1)],
      vec![(4, 2), (5, 2)],
      vec![(6, 3)]
    ];
    let act_grouped = children.into_iter()
      .grouped_with(&parents, |p| p.0, |c| c.1);
    assert_eq!(act_grouped, exp_grouped);
  }

  #[test]
  fn it_maintains_order_of_parent_list() {
    let parents = vec![
      Parent::new(42, "hi"),
      Parent::new(13, "bye"),
      Parent::new(23, "hola"),
    ];
    let children = vec![
      Child::new(1, 13, "child 1"),
      Child::new(2, 23, "child 2"),
      Child::new(3, 13, "child 3"),
      Child::new(4, 42, "child 4"),
      Child::new(5, 42, "child 5"),
      Child::new(6, 13, "child 6"),
    ];
    let exp_grouped_ids: Vec<Vec<i32>> = vec![
      vec![4, 5],
      vec![1, 3, 6],
      vec![2]
    ];
    let act_grouped_ids: Vec<Vec<i32>> = children.into_iter()
      .grouped_with(&parents, |p| p.id, |c| c.parent_id)
      .into_iter()
      .map(|list| list.into_iter().map(|child| child.id).collect())
      .collect();
    assert_eq!(act_grouped_ids, exp_grouped_ids);
  }

  #[test]
  fn it_discards_children_when_parent_id_not_found() {
    let parents = vec![
      Parent::new(42, "hi"),
      Parent::new(13, "bye")
    ];
    let children = vec![
      Child::new(1, 42, "okay 1"),
      Child::new(2, 13, "okay 2"),
      Child::new(3, 1234, "invalid parent id"),
    ];
    let exp_grouped_ids: Vec<Vec<i32>> = vec![
      vec![1],
      vec![2],
    ];
    let act_grouped_ids: Vec<Vec<i32>> = children.into_iter()
      .grouped_with(&parents, |p| p.id, |c| c.parent_id)
      .into_iter()
      .map(|list| list.into_iter().map(|child| child.id).collect())
      .collect();
    assert_eq!(act_grouped_ids, exp_grouped_ids);
  }

}