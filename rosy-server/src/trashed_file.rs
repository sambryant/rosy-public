use std::{
  ffi::{OsStr, OsString},
  fmt::Debug,
  fs,
  path::{Path, PathBuf},
};

use crate::context::Context;

#[derive(Debug)]
pub enum Error {
  BadPathError,
  SourceMissing,
  PathConflict,
  IOError(std::io::Error)
}

pub struct TrashedFile<T>
where T: AsRef<Path> + AsRef<OsStr> + Debug {
  original: T,
  pub trashed: PathBuf,
}

impl<T> TrashedFile<T> where T: AsRef<Path> + AsRef<OsStr> + Debug {

  pub fn trash_as<U>(cxt: &Context, file: T, trashed_filename: &U) -> Result<TrashedFile<T>, Error>
  where U: AsRef<Path> + AsRef<OsStr> + Debug {
    let mut trashed = cxt.get_trash_dir().map_err(|e| Error::IOError(e))?;
    trashed.push(trashed_filename);

    if Path::new(&file).exists() {
      if !Path::new(&trashed).exists() {
        fs::rename(&file, &trashed)
          .map_err(|e| Error::IOError(e))
          .map(|_| Self {
            original: file,
            trashed,
          })
      } else {
        Err(Error::PathConflict)
      }
    } else {
      Err(Error::SourceMissing)
    }
  }

  pub fn trash_with_prefix(cxt: &Context, file: T, prefix: &str) -> Result<TrashedFile<T>, Error> {
    let path: &Path = file.as_ref();
    let old_local_filename = path.file_name().ok_or(Error::BadPathError)?;
    let mut new_local_filename = OsString::from(prefix);
    new_local_filename.push(old_local_filename);
    Self::trash_as(cxt, file, &new_local_filename)
  }

  pub fn delete(self) -> Result<(), Error> {
    if self.trashed.exists() {
      std::fs::remove_file(&self.trashed)
        .map_err(|e| Error::IOError(e))
        .map(|_| ())
    } else {
      Err(Error::SourceMissing)
    }
  }

  pub fn restore(self) -> Result<(), Error> {
    if self.trashed.exists() {
      if !Path::new(&self.original).exists() {
        fs::rename(&self.trashed, &self.original)
          .map_err(|e| Error::IOError(e))
      } else {
        Err(Error::SourceMissing)
      }
    } else {
      Err(Error::PathConflict)
    }
  }

}

