#[derive(Debug, PartialEq)]
pub enum SortDirection { Asc, Desc }

#[derive(Debug, PartialEq)]
pub enum OrderRelation {
  Lower,
  Higher,
}

#[derive(Debug, PartialEq)]
pub struct RelationFilter<T> {
  pub value: T,
  pub relation: OrderRelation,
}

pub type RatingFilter = RelationFilter<f32>;

pub type DateFilter = RelationFilter<i64>;

pub type CountFilter = RelationFilter<i32>;
