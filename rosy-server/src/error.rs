use std::fmt::Debug;
use serde::{Deserialize, Serialize};

use crate::prelude::*;
use crate::threads;

#[derive(Serialize, Deserialize, Debug)]
pub struct Error {
  pub code: u16,
  pub client_msg: String,
  pub server_msg: Option<String>
}

impl Error {

  fn new(code: u16, client_msg: &str) -> Error {
    Error {
      code,
      client_msg: client_msg.to_string(),
      server_msg: None
    }
  }

  pub fn log(&self) {
    log_err(format!("Error ({})\n  client message: {}", self.code, self.client_msg));
    if let Some(msg) = &self.server_msg {
      log_err(format!("  server message: {}", msg));
    }
  }

  pub fn msg(mut self, msg: &'static str) -> Self {
    self.client_msg = msg.to_string();
    self
  }

  pub fn attach<T>(mut self, cause: T) -> Self
  where T: Debug {
    self.server_msg = Some(format!("Error cause: {:?}", cause));
    self
  }

  pub fn attach_msg(mut self, cause: &str) -> Self {
    self.server_msg = Some(String::from(cause));
    self
  }

  pub fn request_body_error() -> Error {
    Error::new(400, "Invalid request body")
  }

  pub fn request_error(msg: &str) -> Error {
    Error::new(400, msg)
  }

  pub fn create_thread_error(e: threads::UpdateThreadError) -> Error {
    Error::new(500, "Failed to start new worker thread").attach(e)
  }

  pub fn auth_error() -> Error {
    Error::new(401, "Not logged in")
  }

  pub fn not_authorized() -> Error {
    Error::new(403, "Not authorized to access resource")
  }

  pub fn not_found() -> Error {
    Error::new(404, "Resource not found")
  }

  pub fn deleted() -> Error {
    Error::new(410, "Resource was deleted")
  }

  pub fn network_error() -> Error {
    Error::new(500, "Network error")
  }

  pub fn internal(client_msg: &str) -> Error {
    Error::new(500, client_msg)
  }

  pub fn unknown() -> Error {
    Error::new(500, "Unknown error")
  }

  pub fn db_conn(err: ConnError) -> Error {
    Error::new(500, "Error connecting to database").attach(err)
  }

  pub fn db_error() -> Error {
    Error::new(500, "Internal database error")
  }
}

impl From<diesel::result::Error> for Error {
  fn from(error: diesel::result::Error) -> Error {
    Error::db_error().attach(error)
  }
}
