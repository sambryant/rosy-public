use std::{
  io,
  io::Write,
  process::Command,
};

use crate::prelude::*;

#[derive(Debug)]
pub enum Error {
  NeverRan(std::io::Error),
  Terminated(std::process::Output),
  BadCode(i32)
}

pub fn run_command(command: &mut Command, log_errors: bool) -> Result<std::process::Output, Error> {
  match command.output() {
    Ok(output) => match output.status.code() {
      Some(code) => match code {
        0 => Ok(output),
        _ => {
          if log_errors {
            log_err(&format!("External command failed with code {}", code));
            log_err(&format!("External command: |{:?}|", command));
            log_err(&format!("External command std output:"));
            io::stdout().write_all(&output.stdout).unwrap();
            log_err(&format!("External command std error:"));
            io::stdout().write_all(&output.stderr).unwrap();
          }
          Err(Error::BadCode(code))
        }
      },
      None => Err(Error::Terminated(output))
    },
    Err(e) => Err(Error::NeverRan(e))
  }
}

impl std::fmt::Display for Error {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    write!(f, "{}", <&str>::from(self))
  }
}

impl From<&Error> for &'static str {
  fn from(e: &Error) -> &'static str {
    match e {
      Error::NeverRan(_) => "Process failed to run",
      Error::Terminated(_) => "Process was terminated",
      Error::BadCode(_) => "Process exited with non-zero code",
    }
  }
}
