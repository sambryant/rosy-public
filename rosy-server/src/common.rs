use std::{
  collections::HashSet,
  ffi::OsStr,
  iter::FromIterator,
  path::Path,
  time::SystemTime,
};

pub fn remove_after_index<T>(vec: &mut Vec<T>, index: usize) {
  if vec.len() > index {
    vec.drain(index..);
  }
}

pub fn get_utc_timestamp() -> u64 {
  SystemTime::now()
    .duration_since(SystemTime::UNIX_EPOCH)
    .unwrap()
    .as_millis() as u64
}

pub fn symmetric_difference<T>(a: HashSet<T>, mut b: HashSet<T>) -> (Vec<T>, Vec<T>)
  where T: std::cmp::Eq + std::hash::Hash + Sized
{
  let a_minus_b = a.into_iter()
    .filter(|s| {
      if b.contains(s) {
        b.remove(s);
        false
      } else {
        true
      }
    })
    .collect();
  let b_minus_a = b.into_iter().collect();
  (a_minus_b, b_minus_a)
}

pub fn cov_into<T, U, W>(v: Vec<T>) -> U
where U: FromIterator<W>,
      W: From<T> {
  v.into_iter().map(|v| v.into()).collect()
}

pub fn remove_file<T>(file: &T) -> Result<(), std::io::Error>
where T: AsRef<Path> + AsRef<OsStr> {
  if Path::new(file).exists() {
    std::fs::remove_file(&file).map(|_| ())
  } else {
    Ok(())
  }
}

#[cfg(test)]
mod test_symmetric_difference {
  use super::*;

  #[test]
  fn it_works() {
    let set_a = vec!['a', 'b', 'c', 'd'].into_iter().collect();
    let set_b = vec!['e', 'f', 'c', 'd'].into_iter().collect();
    let (mut amb, mut bma) = symmetric_difference(set_a, set_b);
    assert_eq!(amb.sort(), vec!['b', 'a'].sort());
    assert_eq!(bma.sort(), vec!['e', 'f'].sort());
  }

}
