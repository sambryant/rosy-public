use std::{
  collections::HashMap,
  path::PathBuf,
};
use serde::{Deserialize, Serialize};
use crate::models::{Dupe, ImgId};

#[derive(Deserialize, Serialize)]
/// Struct containing information about a generated dupe report (list of all marked duplicates).
/// This report is associated with a set of created small thumbnails used as inputs for ML.
pub struct DupeReport {
  /// Unique ID for this dupe report (defines output directory).
  pub id: u64,
  /// Directory that resized image files are written to.
  pub output_dir: PathBuf,
  /// List of all duplicate relations.
  pub dupes: Vec<Dupe>,
  /// Map from image IDs to the full system filename of the reduced images.
  pub filenames: HashMap<ImgId, String>
}
