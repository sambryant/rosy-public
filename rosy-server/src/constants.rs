pub static SCHEMA_HEADER_KEY: &'static str = "x-rosy-schema";
pub static USER_HEADER_KEY: &'static str = "x-rosy-user-id";
pub static THUMBNAIL_FMT: &'static str = "png";
pub static THUMBNAIL_SIZE: (u32, u32) = (128, 128);
pub static DEFAULT_TRAIN_SIZE: u32 = 32;
pub static ML_SIZE: (u32, u32) = (32, 32);
pub static MAX_IMAGE_FACE_MATCHES: usize = 30;
pub static FACE_SIMILARITY_DUPE_THRESHOLD: f32 = 0.1;
pub static FACE_MATCH_CUTOFF: f32 = 0.55;

/// This value is very important because it ensures that only connections from within the network
/// will be accepted.
pub const SERVER_HOST: &'static str = "0.0.0.0";

pub const JWT_EXPIRE_TIME: i64 = 60 * 60 * 24; // 1 day

pub const METADATA_NUM_RELATED: usize = 8;

pub const METADATA_NUM_EVEC_COEFS: usize = 15;

pub static TRAIN_RECORD_FILENAME: &'static str = "raw-dataset.json";
