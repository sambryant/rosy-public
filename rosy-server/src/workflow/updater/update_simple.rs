use diesel::prelude::*;

use crate::{
  context::Context,
  error::Error,
  models::{Artist, ArtId, Dupe, DupeType, Feature, Id, Name, NmeId, Tag, TagId},
  schema::{artist, dupe, feature, name, tag}
};

pub fn update_artist(cxt: &Context, id: ArtId, name: String) -> Result<Artist, Error> {
  let conn = cxt.make_conn()?;

  conn.transaction::<_, Error, _>(|| {
    diesel::update(artist::table)
      .filter(artist::id.eq(id))
      .set(artist::name.eq(name))
      .execute(&conn)?;
    Ok(())
  })?;
  artist::table
    .filter(artist::id.eq(id))
    .load::<Artist>(&conn)?
    .pop()
    .ok_or(Error::not_found())
}

pub fn update_feature(cxt: &Context, id: Id, value: String) -> Result<Feature, Error> {
  let conn = cxt.make_conn()?;

  conn.transaction::<_, Error, _>(|| {
    diesel::update(feature::table)
      .filter(feature::id.eq(id))
      .set(feature::value.eq(value))
      .execute(&conn)?;
    Ok(())
  })?;
  feature::table
    .filter(feature::id.eq(id))
    .load::<Feature>(&conn)?
    .pop()
    .ok_or(Error::not_found())
}

pub fn update_dupe_marks(cxt: &Context, image_ids: Vec<i32>, dupe_type: DupeType) -> Result<(), Error> {
  let conn = cxt.make_conn()?;

  // Always add pairs symmetrically
  let new_dupes: Vec<Dupe> = image_ids.iter()
    .map(|img1_id| image_ids.iter()
      .map(|img2_id| (*img1_id, *img2_id))
      .collect::<Vec<(i32, i32)>>())
    .flatten()
    .filter(|(id1, id2)| id1 != id2)
    .map(|(img1_id, img2_id)| Dupe { img1_id, img2_id, dupe_type: dupe_type.clone() })
    .collect();
  conn.transaction::<_, Error, _>(|| {
    // Delete existing dupe marks
    diesel::delete(dupe::table)
      .filter(dupe::img1_id.eq_any(&image_ids))
      .filter(dupe::img2_id.eq_any(&image_ids))
      .execute(&conn)?;
    diesel::insert_into(dupe::table)
      .values(&new_dupes)
      .execute(&conn)?;
    Ok(())
  })
}

pub fn update_name(cxt: &Context, id: NmeId, value: String) -> Result<Name, Error> {
  let conn = cxt.make_conn()?;

  conn.transaction::<_, Error, _>(|| {
    diesel::update(name::table)
      .filter(name::id.eq(id))
      .set(name::value.eq(value))
      .execute(&conn)?;
    Ok(())
  })?;
  name::table
    .filter(name::id.eq(id))
    .load::<Name>(&conn)?
    .pop()
    .ok_or(Error::not_found())
}

pub fn update_tag(cxt: &Context, id: TagId, value: String) -> Result<Tag, Error> {
  let conn = cxt.make_conn()?;

  conn.transaction::<_, Error, _>(|| {
    diesel::update(tag::table)
      .filter(tag::id.eq(id))
      .set(tag::value.eq(value))
      .execute(&conn)?;
    Ok(())
  })?;
  tag::table
    .filter(tag::id.eq(id))
    .load::<Tag>(&conn)?
    .pop()
    .ok_or(Error::not_found())
}
