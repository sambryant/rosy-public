use diesel::prelude::*;
use std::collections::HashSet;
use std::iter::FromIterator;

use crate::{client_models, common, config, context::Context, error, models, schema};
use client_models::ClientImageDetails;
use common::{get_utc_timestamp, symmetric_difference};
use config::{DBConn};
use error::Error;
use models::{ImgId, ImageName, ImageTag, NmeId, TagId};
use schema::{image, image_name, image_tag};
use crate::workflow::get_image_details;
use super::Update;

pub fn update_image(
  cxt: &Context, incoming_data: ClientImageDetails) -> Result<ClientImageDetails, Error> {
  let conn = cxt.make_conn()?;
  let target_id = incoming_data.id;

  let (old_image, old_names, old_tags, _, _) = get_image_details(&conn, target_id)?;
  let old_tag_ids = old_tags.iter().map(|t| t.id).collect();
  let old_name_ids = old_names.iter().map(|t| t.id).collect();
  let new_tag_ids = HashSet::from_iter(incoming_data.tags.into_iter().map(|t| t.id));
  let new_name_ids = HashSet::from_iter(incoming_data.names.into_iter().map(|t| t.id));

  let column_update = ColumnUpdate {
    hidden: Update::from(old_image.hidden, incoming_data.hidden),
    favorite: Update::from(old_image.favorite, incoming_data.favorite),
    processed: Update::from(old_image.processed, incoming_data.processed),
    marked: Update::from(old_image.marked, incoming_data.marked),
    old_processed_time: old_image.processed_time,
    artist_id: Update::from(old_image.artist_id, incoming_data.artistId),
    group_id: Update::from(old_image.group_id, incoming_data.groupId),
  };

  // Perform transaction atomically: TODO: test if this is actually atomic!
  conn.transaction::<_, Error, _>(|| {
    update_tags(&conn, target_id, Update::from(old_tag_ids, new_tag_ids))?;
    update_names(&conn, target_id, Update::from(old_name_ids, new_name_ids))?;
    update_columns(&conn, target_id, column_update)?;
    Ok(())
  })?;

  // TODO: See if we can use DB results without additional calls.
  Ok(get_image_details(&conn, target_id)?.into())
}

struct ColumnUpdate {
  hidden: Update<bool>,
  favorite: Update<bool>,
  processed: Update<bool>,
  marked: Update<bool>,
  artist_id: Update<Option<i32>>,
  group_id: Update<Option<i32>>,
  old_processed_time: Option<i64>,
}

impl ColumnUpdate {
  fn has_changes(&self) -> bool {
    self.hidden.has_changes() || self.favorite.has_changes() || self.processed.has_changes() || self.group_id.has_changes() || self.artist_id.has_changes() || self.marked.has_changes()
  }
}

fn update_columns(conn: &DBConn, target_id: ImgId, update: ColumnUpdate) -> Result<(), Error> {
  if update.has_changes() {
    // if processed field changes, update processed time as well
    let new_processed_time = match update.processed.has_changes() {
      false => update.old_processed_time,
      true => match update.processed.new {
        true => Some(get_utc_timestamp() as i64),
        false => None,
      },
    };

    // // Below should not be necessary due to FK constraint
    // // If the group is changed, we have to check that the new group exists
    // if update.group_id.has_changes() {
    //   if let Some(group_id) = update.group_id.new {
    //     if !group_exists(conn, group_id)? {
    //       return Err(Error::request_error("New group did not exist"))
    //     }
    //   }
    // }

    diesel::update(image::table)
      .filter(image::id.eq(target_id))
      .set((
        image::hidden.eq(update.hidden.new),
        image::favorite.eq(update.favorite.new),
        image::processed.eq(update.processed.new),
        image::marked.eq(update.marked.new),
        image::processed_time.eq(new_processed_time),
        image::artist_id.eq(update.artist_id.new),
        image::group_id.eq(update.group_id.new),
      ))
      .execute(conn)?;
    Ok(())
  } else {
    Ok(())
  }
}

fn update_tags(
  conn: &DBConn, target_id: ImgId, update: Update<HashSet<TagId>>,
) -> Result<(), Error> {
  let (to_del, to_add) = symmetric_difference(update.old, update.new);
  if to_del.len() > 0 {
    diesel::delete(image_tag::table)
      .filter(image_tag::img_id.eq(target_id))
      .filter(image_tag::tag_id.eq_any(&to_del))
      .execute(conn)?;
  }
  if to_add.len() > 0 {
    let to_add_objs: Vec<ImageTag> = to_add.into_iter().map(|tag_id| ImageTag {
      img_id: target_id,
      tag_id,
    })
    .collect();
    diesel::insert_into(image_tag::table)
      .values(&to_add_objs)
      .execute(conn)?;
  }
  Ok(())
}

fn update_names(
  conn: &DBConn, target_id: ImgId, update: Update<HashSet<NmeId>>,
) -> Result<(), Error> {
  let (to_del, to_add) = symmetric_difference(update.old, update.new);
  if to_del.len() > 0 {
    diesel::delete(image_name::table)
      .filter(image_name::img_id.eq(target_id))
      .filter(image_name::nme_id.eq_any(&to_del))
      .execute(conn)?;
  }
  if to_add.len() > 0 {
    let to_add_objs: Vec<ImageName> = to_add.into_iter().map(|nme_id| ImageName {
      img_id: target_id,
      nme_id
    })
    .collect();
    diesel::insert_into(image_name::table)
      .values(&to_add_objs)
      .execute(conn)?;
  }
  Ok(())
}
