use diesel::prelude::*;
use std::collections::HashSet;
use std::iter::FromIterator;

use crate::{client_models, common, config, context::Context, error, models, schema};
use client_models::ClientGroupDetails;
use common::symmetric_difference;
use config::{DBConn};
use error::Error;
use models::{GrpId, GroupName, GroupTag, ImgId, NmeId, TagId};
use schema::{group, group_name, group_tag};
use crate::workflow::get_groups::{get_group_details, get_group_details_partial};
use super::Update;

pub fn update_group(cxt: &Context, incoming_data: ClientGroupDetails) -> Result<ClientGroupDetails, Error> {
  let target_id = incoming_data.id;
  let conn = cxt.make_conn()?;

  let (old_group, old_names, old_tags) = get_group_details_partial(&conn, target_id)?;
  let old_tag_ids = old_tags.iter().map(|t| t.id).collect();
  let old_name_ids = old_names.iter().map(|t| t.id).collect();
  let new_tag_ids = HashSet::from_iter(incoming_data.tags.into_iter().map(|t| t.id));
  let new_name_ids = HashSet::from_iter(incoming_data.names.into_iter().map(|t| t.id));

  let column_update = ColumnUpdate {
    description: Update::from(old_group.description, incoming_data.description),
    hidden: Update::from(old_group.hidden, incoming_data.hidden),
    title: Update::from(old_group.title, incoming_data.value),
  };

  // Perform transaction atomically: TODO: test if this is actually atomic!
  conn.transaction::<_, Error, _>(|| {
    update_tags(&conn, target_id, Update::from(old_tag_ids, new_tag_ids))?;
    update_names(&conn, target_id, Update::from(old_name_ids, new_name_ids))?;
    update_columns(&conn, target_id, column_update)?;
    Ok(())
  })?;

  // TODO: See if we can use DB results without additional calls.
  Ok(get_group_details(&conn, target_id)?.into())
}

struct ColumnUpdate {
  description: Update<Option<String>>,
  hidden: Update<bool>,
  title: Update<String>,
}

impl ColumnUpdate {
  fn has_changes(&self) -> bool {
    self.hidden.has_changes() || self.title.has_changes() || self.description.has_changes()
  }
}

fn update_columns(conn: &DBConn, target_id: ImgId, update: ColumnUpdate) -> Result<(), Error> {
  if update.has_changes() {
    diesel::update(group::table)
      .filter(group::id.eq(target_id))
      .set((
        group::description.eq(update.description.new),
        group::hidden.eq(update.hidden.new),
        group::title.eq(update.title.new),
      ))
      .execute(conn)?;
    Ok(())
  } else {
    Ok(())
  }
}

fn update_tags(
  conn: &DBConn, target_id: GrpId, update: Update<HashSet<TagId>>,
) -> Result<(), Error> {
  let (to_del, to_add) = symmetric_difference(update.old, update.new);
  if to_del.len() > 0 {
    diesel::delete(group_tag::table)
      .filter(group_tag::grp_id.eq(target_id))
      .filter(group_tag::tag_id.eq_any(&to_del))
      .execute(conn)?;
  }
  if to_add.len() > 0 {
    let to_add_objs: Vec<GroupTag> = to_add.into_iter().map(|tag_id| GroupTag {
      grp_id: target_id,
      tag_id,
    })
    .collect();
    diesel::insert_into(group_tag::table)
      .values(&to_add_objs)
      .execute(conn)?;
  }
  Ok(())
}

fn update_names(
  conn: &DBConn, target_id: i32, update: Update<HashSet<NmeId>>,
) -> Result<(), Error> {
  let (to_del, to_add) = symmetric_difference(update.old, update.new);
  if to_del.len() > 0 {
    diesel::delete(group_name::table)
      .filter(group_name::grp_id.eq(target_id))
      .filter(group_name::nme_id.eq_any(&to_del))
      .execute(conn)?;
  }
  if to_add.len() > 0 {
    let to_add_objs: Vec<GroupName> = to_add.into_iter().map(|nme_id| GroupName {
      grp_id: target_id,
      nme_id
    })
    .collect();
    diesel::insert_into(group_name::table)
      .values(&to_add_objs)
      .execute(conn)?;
  }
  Ok(())
}
