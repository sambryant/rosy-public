pub struct Update<T>
where
  T: std::cmp::PartialEq,
{
  pub old: T,
  pub new: T,
}

impl<T> Update<T>
where
  T: std::cmp::PartialEq,
{
  pub fn from(old: T, new: T) -> Update<T> {
    Update { old, new }
  }
  pub fn has_changes(&self) -> bool {
    self.old != self.new
  }
}
