mod update;
mod update_simple;
mod update_group;
mod update_image;

pub use update_group::update_group;
pub use update_image::update_image;
pub use update_simple::{
  update_artist,
  update_feature,
  update_dupe_marks,
  update_name,
  update_tag
};
use update::Update;
