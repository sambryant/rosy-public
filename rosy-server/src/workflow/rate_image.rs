use diesel::prelude::*;

use crate::{
  common::get_utc_timestamp,
  config::DBConn,
  context::Context,
  error::Error,
  models::{ImgId, Rating, NewRating},
  schema::{image, rating},
};

pub fn rate_image(cxt: &Context, image_id: ImgId, rating_value: i32, clear_existing: bool) -> Result<(f32, i32), Error> {
  let conn = cxt.make_conn()?;
  if clear_existing {
    set_image_rating(&conn, image_id, rating_value)
  } else {
    add_image_rating(&conn, image_id, rating_value)
  }
}

fn set_image_rating(conn: &DBConn, image_id: ImgId, rating_value: i32) -> Result<(f32, i32), Error> {
  let new_rating = NewRating { value: rating_value, time: get_utc_timestamp() as i64, image_id };

  conn.transaction::<_, Error, _>(|| {
    diesel::delete(rating::table)
      .filter(rating::image_id.eq(image_id))
      .execute(conn)?;
    diesel::insert_into(rating::table)
      .values(new_rating)
      .execute(conn)?;
    diesel::update(image::table)
      .filter(image::id.eq(image_id))
      .set((
        image::rating_average.eq(rating_value as f32),
        image::rating_count.eq(1),
      ))
      .execute(conn)?;
    Ok((rating_value as f32, 1))
  })
}

fn add_image_rating(conn: &DBConn, image_id: ImgId, rating_value: i32) -> Result<(f32, i32), Error> {
  let mut ratings: Vec<i32> = rating::table
    .select(rating::value)
    .filter(rating::image_id.eq(image_id))
    .load::<i32>(conn)?;
  ratings.push(rating_value);

  let new_rating_count = ratings.len() as i32;
  let new_average = Rating::average_value(&ratings).unwrap(); // safe because has at least one element.
  let new_rating = (
    rating::value.eq(rating_value),
    rating::time.eq(get_utc_timestamp() as i64),
    rating::image_id.eq(image_id),
  );

  conn.transaction::<_, Error, _>(|| {
    diesel::insert_into(rating::table)
      .values(&vec![new_rating])
      .execute(conn)?;
    diesel::update(image::table)
      .filter(image::id.eq(image_id))
      .set((
        image::rating_average.eq(new_average),
        image::rating_count.eq(new_rating_count),
      ))
      .execute(conn)?;
    Ok(())
  })?;
  Ok((new_average, new_rating_count))
}
