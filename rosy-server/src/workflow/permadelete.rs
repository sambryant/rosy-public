use diesel::prelude::*;
use std::{
  ffi::OsStr,
  fmt::Debug,
  path::{Path, PathBuf},
};

use crate::prelude::*;
use crate::{
  error::Error,
  models::{GrpId, Image, ImgId},
  schema::{group, image},
  trashed_file::{
    Error as TrashedFileError,
    TrashedFile,
  }
};

pub fn permadelete_group(cxt: &Context, group_id: GrpId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;

  // Ensure that record exists
  group::table
    .select(group::id)
    .filter(group::id.eq(group_id))
    .load::<GrpId>(&conn)?
    .pop()
    .ok_or(Error::not_found())?;
  diesel::delete(group::table.filter(group::id.eq(group_id)))
    .execute(&conn)?;
  Ok(())
}

type FileChanges = (Option<TrashedFile<PathBuf>>, Option<TrashedFile<PathBuf>>);

pub fn permadelete_image(cxt: &Context, image_id: ImgId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;

  // Ensure image exists and get hard content file names.
  let image: Image = image::table
    .filter(image::id.eq(image_id))
    .load(&conn)?
    .pop()
    .ok_or(Error::not_found())?;

  let mut trashed = (None, None);

  // The logic flow goes as follows
  // 1. Move image file / thumbnail file to the trash
  //    - if the target image file / thumbnail doesn't exist, this is okay
  // 2. Update database
  // 3. Remove trashed files
  match trash_files(cxt, &image, &mut trashed)
    .and_then(|_| diesel::delete(image::table.filter(image::id.eq(image_id)))
      .execute(&conn)
      .map_err(|e| e.into()))
  {
    Err(err) => rollback(trashed, err),
    Ok(_) => cleanup(trashed),
  }
}

fn trash_files(cxt: &Context, image: &Image, trashed: &mut FileChanges) -> Result<(), Error> {
  trashed.0 = trash_file(cxt, cxt.config.file.get_filename_system(&image.filename), "image")?;
  trashed.1 = trash_file(cxt, cxt.config.file.get_thumbnail_system(&image.thumbnail), "thumb")?;
  Ok(())
}

fn trash_file<T>(cxt: &Context, file: T, prefix: &str) -> Result<Option<TrashedFile<T>>, Error>
where T: AsRef<Path> + AsRef<OsStr> + Debug {
  match TrashedFile::trash_with_prefix(cxt, file, prefix) {
    Ok(trashed) => Ok(Some(trashed)),
    Err(TrashedFileError::SourceMissing) => {
      eprintln!("Warning: Could not find source file while permadeleting image");
      Ok(None) // give warning but continue with permadelete
    },
    Err(TrashedFileError::IOError(e)) => {
      Err(Error::internal("Couldnt delete content file")
        .attach(e))
    }
    Err(TrashedFileError::PathConflict) => {
      Err(Error::internal("Couldnt delete content file")
        .attach_msg("Error: File already exists in trash with same name as target"))
    }
    Err(TrashedFileError::BadPathError) => {
      eprintln!("Error: Unable to content filename for file while permadeleting");
      eprintln!("     : this should never happen");
      panic!() // should not happen
    }
  }
}

fn cleanup(trashed: FileChanges) -> Result<(), Error> {
  // If files are found in the trash, delete them now permanently.
  let mut failure = None;
  let (t1, t2) = trashed;
  if let Some(f) = t1 {
    if let Err(e) = f.delete() {
      failure = Some(e);
      eprintln!("Error: failed to empty trashed file after permadeleting");
    }
  }
  if let Some(f) = t2 {
    if let Err(e) = f.delete() {
      failure = Some(e);
      eprintln!("Error: failed to empty trashed file after permadeleting");
    }
  }
  if let Some(e) = failure {
    Err(Error::internal("Permadelete succeeded but failed to clean up results").attach(e))
  } else {
    Ok(())
  }
}

fn rollback<T>(trashed: FileChanges, error: T) -> Result<(), T> {
  eprintln!("Permadelete failed, rolling back file deletion");
  let mut failure = false;
  let (t1, t2) = trashed;
  if let Some(f) = t1 {
    if let Err(e) = f.restore() {
      failure = true;
      eprintln!("Error: failed to rollback deletion of file");
      eprintln!("Cause: {:?}", e)
    }
  }
  if let Some(f) = t2 {
    if let Err(e) = f.restore() {
      failure = true;
      eprintln!("Error: failed to rollback deletion of file");
      eprintln!("Cause: {:?}", e)
    }
  }
  if !failure {
    eprintln!("Permadelete rollback succeeded");
  }
  Err(error)
}
