use diesel::prelude::*;
use std::collections::{HashSet, HashMap};

use crate::prelude::*;
use crate::{
  constants::{THUMBNAIL_FMT, ML_SIZE},
  error::Error,
  models::{Dupe, ImgId},
  schema::{dupe, image},
  workflow::importer::thumbnail::generate_thumbnail,
};

pub fn generate_dupe_report(cxt: &Context) -> Result<(), Error> {
  let conn = cxt.make_conn()?;

  let mut dupes = dupe::table.load::<Dupe>(&conn)?;
  let mut ids = HashSet::new();
  for dupe in dupes.iter() {
    ids.insert(dupe.img1_id);
    ids.insert(dupe.img2_id);
  }
  let ids: Vec<ImgId> = ids.into_iter().collect();
  let raw_filenames: HashMap<ImgId, String> = image::table
    .select((image::id, image::filename))
    .filter(image::id.eq_any(ids))
    .filter(image::deleted.eq(false))
    .filter(image::is_video.eq(false))
    .load::<(ImgId, String)>(&conn)?
    .into_iter()
    .map(|(id, fname)| (id, fname))
    .collect();

  // Remove any dupes which were not matched to images by query (i.e. videos)
  dupes = dupes.into_iter()
    .filter(|d| raw_filenames.contains_key(&d.img1_id) && raw_filenames.contains_key(&d.img2_id))
    .collect();

  let (file, mut report) = cxt.config.file.fresh_dupe_report()
    .map_err(|e| Error::unknown().attach(e))?;

  // We have to generate a reduced size version for each image
  for (id, filename) in raw_filenames.into_iter() {
    let sysname = cxt.config.file.get_filename_system(&filename);
    let newname = format!("{:?}{}.{}", report.output_dir, id, THUMBNAIL_FMT);
    let mut created_file = None;
    generate_thumbnail(&sysname, &newname, &mut created_file, ML_SIZE)
      .map_err(|e| Error::unknown().attach(e))?;
    report.filenames.insert(id, newname);
  }
  report.dupes = dupes;

  serde_json::to_writer(&file, &report).map_err(|e| Error::unknown().attach(e))?;
  log_msg(format!("Created new dupe report at {:?}", file));
  Ok(())
}
