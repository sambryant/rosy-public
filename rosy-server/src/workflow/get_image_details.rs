use diesel::prelude::*;
use itertools::izip;
use std::collections::HashSet;

use crate::{config, models, schema};
use crate::error::Error;
use config::DBConn;
use models::*;
use schema::{dupe, feature, image, image_feature, image_name, image_tag, name, tag};

pub enum GetImageDetailsError {
  DieselError(diesel::result::Error),
  NotFound,
  Deleted
}

impl From<GetImageDetailsError> for Error {
  fn from(error: GetImageDetailsError) -> Error {
    use GetImageDetailsError::*;
    match error {
      DieselError(e) => Error::db_error().attach(e),
      NotFound => Error::not_found(),
      Deleted => Error::deleted(),
    }
  }
}

/// Gets relationships grouped by image for the given image models.
///
/// This makes two separate DB calls, but repeats no rows, so is best suited for lists of images.
pub fn get_images_details(conn: &DBConn, images: Vec<Image>) -> Result<Vec<ImageData>, diesel::result::Error> {
  let names: Vec<Vec<Name>> = ImageName::belonging_to(&images)
    .inner_join(name::table)
    .load::<(ImageName, Name)>(conn)? // need to load ImageName so we can group results
    .grouped_by(&images)
    .into_iter()
    .map(|name_group| name_group.into_iter().map(|(_, name)| name).collect()) // discard ImageName
    .collect();
  let tags: Vec<Vec<Tag>> = ImageTag::belonging_to(&images)
    .inner_join(tag::table)
    .load::<(ImageTag, Tag)>(conn)? // need to load ImageTag so we can group results
    .grouped_by(&images)
    .into_iter()
    .map(|tag_group| tag_group.into_iter().map(|(_, tag)| tag).collect()) // discard ImageTag
    .collect();
  let features: Vec<Vec<(ImageFeature, Feature)>> = ImageFeature::belonging_to(&images)
      .inner_join(feature::table)
      .load::<(ImageFeature, Feature)>(conn)?
      .grouped_by(&images)
      .into_iter()
      .map(|v| v)
      .collect();
  let dupes: Vec<Vec<(ImgId, DupeType)>> = Dupe::belonging_to(&images)
    .load::<Dupe>(conn)? // need to load ImageName so we can group results
    .grouped_by(&images)
    .into_iter()
    .map(|list| list.into_iter()
      .map(|d| (d.img2_id, d.dupe_type))
      .collect())
    .collect();
  Ok(izip!(images, names, tags, features, dupes).collect())
}

/// Gets image model and relationships for a given image id.
///
/// This makes a single DB call, but repeats rows, so is best suited for a single image.
pub fn get_image_details(conn: &DBConn, target_id: ImgId) -> Result<(Image, HashSet<Name>, HashSet<Tag>, HashSet<(ImageFeature, Feature)>, HashSet<(ImgId, DupeType)>), GetImageDetailsError> {
  let results = image::table
    .left_outer_join(image_name::table.inner_join(name::table))
    .left_outer_join(image_tag::table.inner_join(tag::table))
    .left_outer_join(image_feature::table.inner_join(feature::table))
    .left_outer_join(dupe::table)
    .select((image::all_columns, name::all_columns.nullable(), tag::all_columns.nullable(), (image_feature::all_columns, feature::all_columns).nullable(), dupe::all_columns.nullable()))
    .filter(image::id.eq(target_id))
    .load::<(Image, Option<Name>, Option<Tag>, Option<(ImageFeature, Feature)>, Option<Dupe>)>(conn)
    .map_err(|e| GetImageDetailsError::DieselError(e))?;

  let mut image_opt: Option<Image> = None;
  let mut names = HashSet::with_capacity(results.len());
  let mut tags = HashSet::with_capacity(results.len());
  let mut features = HashSet::with_capacity(results.len());
  let mut dupes = HashSet::with_capacity(results.len());
  for (image, name_opt, tag_opt, feature_opt, dupe_opt) in results {
    if image_opt.is_none() {
      image_opt = Some(image);
    }
    if let Some(name) = name_opt {
      names.insert(name);
    }
    if let Some(tag) = tag_opt {
      tags.insert(tag);
    }
    if let Some(v) = feature_opt {
      features.insert(v);
    }
    if let Some(dupe) = dupe_opt {
      dupes.insert((dupe.img2_id, dupe.dupe_type));
    }
  }
  match image_opt {
    None => Err(GetImageDetailsError::NotFound),
    Some(image) => match image.deleted {
      true => Err(GetImageDetailsError::Deleted),
      false => Ok((image, names, tags, features, dupes))
    }
  }
}
