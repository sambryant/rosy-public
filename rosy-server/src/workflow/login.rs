use diesel::prelude::*;

use crate::{
  api::requests::LoginRequest,
  config::{
    AuthToken,
  },
  context::Context,
  models::User,
  prelude::*,
  schema::user,
};

#[derive(Debug)]
pub enum LoginError {
  DatabaseConn(ConnError),
  Database(diesel::result::Error),
  InternalError,
  InvalidUsername,
  InvalidPassword,
}

pub fn handle_login(cxt: &Context, req: LoginRequest) -> Result<AuthToken, LoginError> {
  let usr = get_user(cxt, &req.username)?;
  let matches = bcrypt::verify(&req.password, &usr.password)
    .map_err(|_| {
      log_err(format!("bcrypt error!"));
      LoginError::InternalError
    })?;
  if matches {
    cxt.config.auth.generate_token(&usr).map_err(|e| {
      log_err(&format!("Token creation failed: {:?}", e));
      LoginError::InternalError
    })
  } else {
    Err(LoginError::InvalidPassword)
  }
}

fn get_user(cxt: &Context, username: &str) -> Result<User, LoginError> {
  let conn = cxt.make_conn().map_err(|e| LoginError::DatabaseConn(e))?;
  user::table
    .filter(user::username.eq(username))
    .load::<User>(&conn).map_err(|e| LoginError::Database(e))?
    .pop()
    .ok_or(LoginError::InvalidUsername)
}
