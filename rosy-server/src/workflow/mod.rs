mod calculate_metadata;
mod create;
mod delete;
pub mod fileops;
mod generate_dupe_report;
mod get;
mod get_groups;
pub mod get_image_details;
pub mod get_name_suggestion;
pub mod importer;
mod login;
mod merge_images;
mod query;
mod permadelete;
mod rate_image;
mod simple;
pub mod thread_worker;
mod updater;


pub use calculate_metadata::calculate_metadata;
pub use create::{
  create_artist,
  create_feature,
  create_group,
  create_image_feature,
  create_name,
  create_tag,
};
pub use delete::{
  delete_artist,
  delete_dupe_marks,
  delete_feature,
  delete_group,
  delete_image,
  delete_image_feature,
  delete_name,
  delete_tag,
  restore_group,
  restore_image,
};
pub use generate_dupe_report::generate_dupe_report;
pub use get_groups::get_group_details;
pub use get::get_marked_dupes;
pub use importer::import_from_url;
pub use importer::import_from_upload;
pub use login::{handle_login, LoginError};
pub use permadelete::{
  permadelete_group,
  permadelete_image,
};
pub use query::{
  query_artists,
  query_dupe_candidates,
  query_eigendata,
  query_face_matches,
  query_groups,
  query_images,
  query_deleted,
  query_names,
  query_tags,
  DupeCandidate,
  DupeCandidateMethod,
  FaceMatch,
  QueryArtistSortMethod,
  QueryGroupSortMethod,
  QueryImageSortMethod,
  QueryNameSortMethod,
  QueryTagSortMethod,
};
pub use rate_image::rate_image;
pub use simple::{fix_sequences, get_image, get_metadata};
pub use thread_worker::{
  start_generate_dataset,
  start_generate_feature_dataset,
  start_generate_tag_dataset,
  start_merge_duplicates,
  start_recompute_filesizes,
  start_regenerate_thumbnails,
  start_reindex_images,
  start_reset_files
};
pub use updater::{
  update_artist,
  update_dupe_marks,
  update_feature,
  update_group,
  update_image,
  update_name,
  update_tag,
};

// Methods used internally by other mod.rs methods
use get_image_details::{
  get_images_details,
  get_image_details
};
