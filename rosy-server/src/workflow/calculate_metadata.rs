use diesel::prelude::*;
use itertools::izip;
use nalgebra::{Dynamic, OMatrix, linalg};
use std::collections::HashMap;

use crate::{
  config::DBConn,
  context::Context,
  constants::{METADATA_NUM_RELATED, METADATA_NUM_EVEC_COEFS},
  error::Error,
  models::{
    ImageTag, ImgId, Index, Name, NameMetadata, ImageName, NmeId, Tag, TagId, MetadataEigendata, TagMetadata
  },
  schema::{image_tag, name, name_metadata, tag, tag_metadata, image_name, metadata_eigendata},
};

pub fn calculate_metadata(cxt: &Context) -> Result<(), Error> {
  let conn = cxt.make_conn()?;

  // Obtain a canonical ordering of names/tags to use for matrix assignment.
  let indexer = Indexer::new(name::table.load::<Name>(&conn)?, tag::table.load::<Tag>(&conn)?);

  // Group the names/tags by image so they correlations can be counted.
  let grouped = group_by_image(&indexer, image_name::table.load(&conn)?, image_tag::table.load(&conn)?);

  // Generate the correlation matrix.
  let correlations = Correlations::new(&grouped);

  // Compute related name/tags from the correlation matrix.
  let (name_rels, tag_rels) = EntityRelations::get_full(
    &indexer, &correlations, METADATA_NUM_RELATED);

  // Compute the leading eigenvectors and eigenvalues
  let eigenpairs = compute_eigenvectors(correlations);
  let evnum = METADATA_NUM_EVEC_COEFS;

  let new_name_metadata = NameMetadata::get_all(&indexer, &eigenpairs[0..evnum], name_rels);
  let new_tag_metadata = TagMetadata::get_all(&indexer, &eigenpairs[0..evnum], tag_rels);
  let new_metadata_eigendata = construct_metadata_eigendata(eigenpairs);

  do_update(&conn, indexer, new_metadata_eigendata, new_name_metadata, new_tag_metadata)
}

/**
 * Keeps track of a two-way map between name/tag ID and their index in matrices.
 */
struct Indexer {
  name: HashMap<NmeId, usize>,
  name_ids: Vec<NmeId>,
  tag: HashMap<TagId, usize>,
  tag_ids: Vec<TagId>,
}

type Matrix = OMatrix<f32, Dynamic, Dynamic>;

struct Correlations {
  counts: Vec<u32>,
  matrix: OMatrix<f32, Dynamic, Dynamic>,
}

/**
 * Contains all of the related entities to a particular entity.
 */
struct EntityRelations {
  high_names: Vec<NmeId>,
  high_name_scores: Vec<f32>,
  high_tags: Vec<TagId>,
  high_tag_scores: Vec<f32>,
  low_names: Vec<NmeId>,
  low_name_scores: Vec<f32>,
  low_tags: Vec<TagId>,
  low_tag_scores: Vec<f32>,
}

type NameRelations = EntityRelations;

type TagRelations = EntityRelations;

struct Eigenpair {
  eval: f32,
  evec: Vec<f32>,
}

/**
 * Represents a link from some source to a target with a given score.
 */
type Relation<T> = (T, f32);


impl Indexer {
  pub fn new(mut names: Vec<Name>, mut tags: Vec<Tag>) -> Indexer {
    names.sort_by(|a, b| a.value.partial_cmp(&b.value).unwrap());
    tags.sort_by(|a, b| a.value.partial_cmp(&b.value).unwrap());
    let name_ids: Vec<NmeId> = names.iter().map(|t| t.id).collect();
    let tag_ids: Vec<TagId> = tags.iter().map(|t| t.id).collect();
    Indexer {
      name: name_ids.iter().enumerate().map(|(index, id)| (*id, index)).collect(),
      name_ids,
      tag :  tag_ids.iter().enumerate().map(|(index, id)| (*id, names.len() + index)).collect(),
      tag_ids,
    }
  }
}

fn group_by_image(index: &Indexer, mut image_names: Vec<ImageName>, mut image_tags: Vec<ImageTag>) -> Vec<Vec<Index>> {
  let mut image_map: HashMap<ImgId, Vec<Index>> = HashMap::new();

  while let Some(name) = image_names.pop() {
    let i = *index.name.get(&name.nme_id).unwrap();
    match image_map.get_mut(&name.img_id) {
      Some(list) => { list.push(i); }
      None => { image_map.insert(name.img_id, vec!(i)); }
    }
  }
  while let Some(tag) = image_tags.pop() {
    let i = *index.tag.get(&tag.tag_id).unwrap();
    match image_map.get_mut(&tag.img_id) {
      Some(list) => { list.push(i); }
      None => { image_map.insert(tag.img_id, vec!(i)); }
    }
  }
  image_map.values().cloned().collect()
}

impl Correlations {
  pub fn new(item_groups: &Vec<Vec<Index>>) -> Correlations {
    // Find the maximum index value, assume there are this many elements.
    let num = (item_groups.iter()
      .map(|group| group.iter().max().unwrap_or(&0))
      .max().unwrap_or(&0) + 1) as usize;

    let mut counts: Vec<u32> = (0..num).map(|_| 0).collect();
    let mut matrix: Matrix = Matrix::zeros(num, num);
    for item_group in item_groups.iter() {
      for i in item_group {
        counts[*i] += 1;
        for j in item_group {
          matrix[(*i, *j)] += 1.0;
        }
      }
    }

    // Shift the correlation so it gives connected correlation
    let n = item_groups.len() as f32;
    for i in 0..num {
      for j in 0..num {
        matrix[(i, j)] = matrix[(i, j)] / n - (counts[i] * counts[j]) as f32 / (n * n);
      }
    }
    Correlations { counts, matrix }
  }
}

impl EntityRelations {

  pub fn new(indexer: &Indexer, correlations: &Correlations, num_relations: usize, i: usize) -> EntityRelations {
    let m = &correlations.matrix;
    let n = correlations.counts.len();
    let name_scores: Vec<(NmeId, f32)> = (0..indexer.name.len()).map(|j| {
      let score = if m[(i, i)] == 0.0 { 0.0 } else { m[(i, j)] / m[(i, i)] * 100.0 };
      (indexer.name_ids[j], score)
    }).collect();
    let tag_scores : Vec<(NmeId, f32)>  = (indexer.name.len()..n).map(|j| {
      let score = if m[(i, i)] == 0.0 { 0.0 } else { m[(i, j)] / m[(i, i)] * 100.0 };
      (indexer.tag_ids[j - indexer.name.len()] , score)
    }).collect();
    // Extract the relations with the highest few and lowest few scores.
    let ((low_names, low_name_scores), (high_names, high_name_scores)) = get_extremum(name_scores, num_relations);
    let ((low_tags , low_tag_scores ), (high_tags , high_tag_scores )) = get_extremum(tag_scores, num_relations);
    EntityRelations {
      high_names, high_tags, low_names, low_tags,
      high_name_scores, high_tag_scores, low_name_scores, low_tag_scores
    }
  }

  pub fn get_full(indexer: &Indexer, correlations: &Correlations, num_relations: usize) -> (Vec<NameRelations>, Vec<TagRelations>) {
    let n = correlations.counts.len();
    let names = (0..indexer.name.len()).map(|i| {
      EntityRelations::new(indexer, correlations, num_relations, i)
    }).collect();
    let tags = (indexer.name.len()..n).map(|i| {
      EntityRelations::new(indexer, correlations, num_relations, i)
    }).collect();
    ( names, tags )
  }

}

/**
 * Returns the lowest few and highest few items from the list (and separates them).
 */
fn get_extremum<T: Copy>(mut relations: Vec<Relation<T>>, num: usize) -> ((Vec<T>, Vec<f32>), (Vec<T>, Vec<f32>)) {
  relations.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
  let len = if num > relations.len() { relations.len() } else { num };
  let (mut low_values, mut low_scores) = (Vec::with_capacity(len), Vec::with_capacity(len));
  let (mut high_values, mut high_scores) = (Vec::with_capacity(len), Vec::with_capacity(len));
  for i in 0..len {
    low_values.push(relations[i].0.clone());
    low_scores.push(relations[i].1.clone());
    high_values.push(relations[relations.len() - i - 1].0.clone());
    high_scores.push(relations[relations.len() - i - 1].1.clone());
  }
  ((low_values, low_scores), (high_values, high_scores))
}

fn compute_eigenvectors(correlations: Correlations) -> Vec<Eigenpair> {
  let n = correlations.counts.len();
  let s = linalg::SymmetricEigen::new(correlations.matrix);
  let mut eigenpairs: Vec<Eigenpair> = Vec::new();
  for i in 0..n {
    eigenpairs.push(Eigenpair {
      eval: s.eigenvalues[i],
      evec: (0..n).map(|j| s.eigenvectors[(j, i)]).collect()
    });
  }

  // Sort eigenvectors according to eigenvalue and only keep leading few entries
  eigenpairs.sort_by(|a, b| b.eval.partial_cmp(&a.eval).unwrap());
  eigenpairs
}

impl NameMetadata {
  fn new(id: NmeId, index: Index, eigenpairs: &[Eigenpair], r: NameRelations) -> NameMetadata {
    NameMetadata {
      id: id,
      evec_coefs: eigenpairs.iter().map(|ep| ep.evec[index]).collect(),
      high_tags   : r.high_tags   , high_tag_scores   : r.high_tag_scores,
      high_names  : r.high_names  , high_name_scores  : r.high_name_scores,
      low_tags : r.low_tags , low_tag_scores : r.low_tag_scores,
      low_names: r.low_names, low_name_scores: r.low_name_scores,
    }
  }
  fn get_all(indexer: &Indexer, eigenpairs: &[Eigenpair], relations_list: Vec<NameRelations>) -> Vec<NameMetadata> {
    izip!(indexer.name_ids.iter(), relations_list.into_iter())
      .map(|(id, r)| {
        NameMetadata::new(*id, indexer.name[&id], eigenpairs, r)
      }).collect()
  }
}

impl TagMetadata {
  fn new(id: TagId, index: Index, eigenpairs: &[Eigenpair], r: TagRelations) -> TagMetadata {
    TagMetadata {
      id: id,
      evec_coefs: eigenpairs.iter().map(|ep| ep.evec[index]).collect(),
      high_tags   : r.high_tags   , high_tag_scores   : r.high_tag_scores,
      high_names  : r.high_names  , high_name_scores  : r.high_name_scores,
      low_tags : r.low_tags , low_tag_scores : r.low_tag_scores,
      low_names: r.low_names, low_name_scores: r.low_name_scores,
    }
  }
  fn get_all(indexer: &Indexer, eigenpairs: &[Eigenpair], relations_list: Vec<TagRelations>) -> Vec<TagMetadata> {
    izip!(indexer.tag_ids.iter(), relations_list.into_iter())
      .map(|(id, r)| {
        TagMetadata::new(*id, indexer.tag[&id], eigenpairs, r)
      }).collect()
  }
}

fn construct_metadata_eigendata(eigenpairs: Vec<Eigenpair>) -> Vec<MetadataEigendata> {
  eigenpairs.into_iter()
    .enumerate()
    .map(|(id, eigenpair)| MetadataEigendata {
      id: (id + 1) as i32,
      eigenvalue: eigenpair.eval,
      eigenvector: eigenpair.evec,
    })
    .collect()
}

fn do_update(conn: &DBConn, indexer: Indexer, new_md_ev: Vec<MetadataEigendata>, new_name_md: Vec<NameMetadata>, new_tag_md: Vec<TagMetadata>) -> Result<(), Error> {
  conn.transaction::<_, Error, _>(|| {
    // We have to update the index of every single tag and name in the database
    for (index, id) in indexer.tag_ids.iter().enumerate() {
      diesel::update(tag::table)
        .filter(tag::id.eq(id))
        .set(tag::index.eq(Some((indexer.name.len() + index) as i32)))
        .execute(conn)?;
    }
    for (index, id) in indexer.name_ids.iter().enumerate() {
      diesel::update(name::table)
        .filter(name::id.eq(id))
        .set(name::index.eq(Some(index as i32)))
        .execute(conn)?;
    }
    diesel::delete(metadata_eigendata::table).execute(conn)?;
    diesel::insert_into(metadata_eigendata::table)
      .values(&new_md_ev)
      .execute(conn)?;
    diesel::delete(name_metadata::table).execute(conn)?;
    diesel::insert_into(name_metadata::table)
      .values(&new_name_md)
      .execute(conn)?;
    diesel::delete(tag_metadata::table).execute(conn)?;
    diesel::insert_into(tag_metadata::table)
      .values(&new_tag_md)
      .execute(conn)?;
    Ok(())
  })
}
