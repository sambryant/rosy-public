use diesel::prelude::*;
use diesel::sql_query;
use std::collections::HashMap;

use crate::{
  api::responses::GetMetadataResponseArg,
  client_models::{ClientArtist, ClientFeature, ClientGroup, ClientImageDetails, ClientName, ClientTag},
  context::Context,
  error::Error,
  models::{Artist, Feature, Group, ImgId, Name, Tag},
  schema::{artist, feature, group, name, tag}
};
use super::get_image_details;

pub fn fix_sequences(cxt: &Context) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  sql_query("SELECT setval('public.image_id_seq', (SELECT MAX(id) FROM public.image)+1)")
    .execute(&conn)?;
  sql_query("SELECT setval('public.user_id_seq', (SELECT MAX(id) FROM public.user)+1)")
    .execute(&conn)?;
  sql_query("SELECT setval('public.artist_id_seq', (SELECT MAX(id) FROM public.artist)+1)")
    .execute(&conn)?;
  sql_query("SELECT setval('public.group_id_seq', (SELECT MAX(id) FROM public.group)+1)")
    .execute(&conn)?;
  sql_query("SELECT setval('public.rating_id_seq', (SELECT MAX(id) FROM public.rating)+1)")
    .execute(&conn)?;
  Ok(())
}

pub fn get_image(cxt: &Context, image_id: ImgId) -> Result<ClientImageDetails, Error> {
  let conn = cxt.make_conn()?;
  Ok(get_image_details(&conn, image_id)?.into())
}

pub fn get_metadata(cxt: &Context) -> Result<GetMetadataResponseArg, Error> {
  let conn = cxt.make_conn()?;

  let mut artists: Vec<ClientArtist> = artist::table
    .load::<Artist>(&conn)?
    .into_iter()
    .map(|t| t.into())
    .collect();
  let mut features: Vec<ClientFeature> = feature::table
    .load::<Feature>(&conn)?
    .into_iter()
    .map(|t| t.into())
    .collect();
  let mut groups: Vec<ClientGroup> = group::table
    .load::<Group>(&conn)?
    .into_iter()
    .map(|t| t.into())
    .collect();
  let mut names: Vec<ClientName> = name::table
    .load::<Name>(&conn)?
    .into_iter()
    .map(|t| t.into())
    .collect();
  let mut tags: Vec<ClientTag> = tag::table
    .load::<Tag>(&conn)?
    .into_iter()
    .map(|t| t.into())
    .collect();
  artists.sort();
  features.sort();
  groups.sort();
  names.sort();
  tags.sort();
  let artist_map: HashMap<i32, u32> = artists.iter().enumerate().map(|(i, t)| (t.id, i as u32)).collect();
  let feature_map: HashMap<i32, u32> = features.iter().enumerate().map(|(i, t)| (t.id, i as u32)).collect();
  let group_map: HashMap<i32, u32> = groups.iter().enumerate().map(|(i, t)| (t.id, i as u32)).collect();
  let name_map: HashMap<i32, u32> = names.iter().enumerate().map(|(i, t)| (t.id, i as u32)).collect();
  let tag_map: HashMap<i32, u32> = tags.iter().enumerate().map(|(i, t)| (t.id, i as u32)).collect();
  Ok(((artists, artist_map), (features, feature_map), (groups, group_map), (names, name_map), (tags, tag_map)))
}
