use diesel::prelude::*;
use itertools::izip;
use std::collections::{HashMap, HashSet};

use crate::{
  config::DBConn,
  error::Error,
  models::{Group, GroupData, GroupName, GroupTag, Name, Image, ImageData, ImgId, Tag},
  schema::{group, group_name, group_tag, image, name, tag}
};
use super::get_image_details::get_images_details;


pub enum GetGroupDetailsError {
  DieselError(diesel::result::Error),
  NotFound,
  Deleted
}


impl From<GetGroupDetailsError> for Error {
  fn from(error: GetGroupDetailsError) -> Error {
    use GetGroupDetailsError::*;
    match error {
      DieselError(e) => Error::db_error().attach(e),
      NotFound => Error::not_found(),
      Deleted => Error::deleted(),
    }
  }
}


/// Gets image model and relationships for a given image id.
///
/// This makes a single DB call, but repeats rows, so is best suited for a single group.
pub fn get_group_details_partial(conn: &DBConn, target_id: i32) -> Result<(Group, HashSet<Name>, HashSet<Tag>), GetGroupDetailsError> {
  let results = group::table
    .left_outer_join(group_name::table.inner_join(name::table))
    .left_outer_join(group_tag::table.inner_join(tag::table))
    .select((group::all_columns, name::all_columns.nullable(), tag::all_columns.nullable()))
    .filter(group::id.eq(target_id))
    .load::<(Group, Option<Name>, Option<Tag>)>(conn)
    .map_err(|e| GetGroupDetailsError::DieselError(e))?;

  // Get simpled owned relationships.
  let mut group_opt: Option<Group> = None;
  let mut names = HashSet::with_capacity(results.len());
  let mut tags = HashSet::with_capacity(results.len());
  for (group, name_opt, tag_opt) in results {
    if group_opt.is_none() {
      group_opt = Some(group);
    }
    if let Some(name) = name_opt {
      names.insert(name);
    }
    if let Some(tag) = tag_opt {
      tags.insert(tag);
    }
  }
  match group_opt {
    None => Err(GetGroupDetailsError::NotFound),
    Some(group) => match group.deleted {
      true => Err(GetGroupDetailsError::Deleted),
      false => Ok((group, names, tags))
    }
  }
}


/// Gets image model and relationships for a given image id.
///
/// This makes fewer DB calls, but repeats rows, so is best suited for a single group.
pub fn get_group_details(conn: &DBConn, target_id: i32) -> Result<(Group, HashSet<Name>, HashSet<Tag>, Vec<ImageData>), GetGroupDetailsError> {
  // Note: don't include image::deleted at this stage or it will filter out groups that contain only
  // deleted images.
  let results = group::table
    .left_outer_join(group_name::table.inner_join(name::table))
    .left_outer_join(group_tag::table.inner_join(tag::table))
    .left_outer_join(image::table)
    .select((group::all_columns, name::all_columns.nullable(), tag::all_columns.nullable(), image::all_columns.nullable()))
    .filter(group::deleted.eq(false))
    .filter(group::id.eq(target_id))
    .load::<(Group, Option<Name>, Option<Tag>, Option<Image>)>(conn)
    .map_err(|e| GetGroupDetailsError::DieselError(e))?;

  // Get simpled owned relationships.
  let mut group_opt: Option<Group> = None;
  let mut names = HashSet::with_capacity(results.len());
  let mut tags = HashSet::with_capacity(results.len());
  let mut imgs = HashMap::with_capacity(results.len());
  for (group, name_opt, tag_opt, img_opt) in results {
    if group_opt.is_none() {
      group_opt = Some(group);
    }
    if let Some(name) = name_opt {
      names.insert(name);
    }
    if let Some(tag) = tag_opt {
      tags.insert(tag);
    }
    if let Some(img) = img_opt {
      if !img.deleted {
        imgs.insert(img.id, img);
      }
    }
  }
  match group_opt {
    None => Err(GetGroupDetailsError::NotFound),
    Some(group) => match group.deleted {
      true => Err(GetGroupDetailsError::Deleted),
      false => {
        let images: Vec<Image> = imgs.into_iter()
          .map(|(_, v)| v)
          .collect();
        let img_data = get_images_details(conn, images)
          .map_err(|e| GetGroupDetailsError::DieselError(e))?;
        Ok((group, names, tags, img_data))
      }
    }
  }
}

pub fn get_groups_details(conn: &DBConn, groups: Vec<Group>) -> Result<Vec<GroupData>, Error> {
  let names: Vec<Vec<Name>> = GroupName::belonging_to(&groups)
    .inner_join(name::table)
    .load::<(GroupName, Name)>(conn)? // need to load GroupName so we can group results
    .grouped_by(&groups)
    .into_iter()
    .map(|name_group| name_group.into_iter().map(|(_, name)| name).collect()) // discard GroupName
    .collect();
  let tags: Vec<Vec<Tag>> = GroupTag::belonging_to(&groups)
    .inner_join(tag::table)
    .load::<(GroupTag, Tag)>(conn)? // need to load GroupTag so we can group results
    .grouped_by(&groups)
    .into_iter()
    .map(|tag_group| tag_group.into_iter().map(|(_, tag)| tag).collect()) // discard GroupTag
    .collect();
  let images = Image::belonging_to(&groups)
    .filter(image::deleted.eq(false))
    .load::<Image>(conn)?
    .grouped_by(&groups);

  // Keep track of which images are in which group so we can restructure data after flattening (to
  // obtain details)
  let mut image_to_group_index: HashMap<ImgId, usize> = HashMap::new();
  for i in 0..images.len() {
    for img in images[i].iter() {
      image_to_group_index.insert(img.id, i);
    }
  }

  // Flatten images and obtain image details
  let images: Vec<Image> = images.into_iter().flatten().collect();
  let images_details = get_images_details(conn, images)?;

  // Restructure image data
  let mut group_images: Vec<Vec<ImageData>> = (0..groups.len())
    .map(|_| Vec::new()).collect();
  for image_details in images_details.into_iter() {
    let index = *image_to_group_index.get(&image_details.0.id)
      .ok_or(Error::unknown())?; // should never happen I think
    group_images[index].push(image_details);
  }
  Ok(izip!(groups, names, tags, group_images).collect())
}
