use diesel::prelude::*;

use crate::{
  client_models::ClientImageDetails,
  common::get_utc_timestamp,
  config::DBConn,
  context::Context,
  error::Error,
  models::{
    Artist, Feature, Group, GroupData, Id, Image, ImageFeature, ImageName, ImageTag, ImgId, Name, NewArtist, NewFeature, NewGroup, NewImageFeature, NewName, NewTag, Tag
  },
  schema::{artist, feature, group, image, image_feature, image_name, image_tag, name, tag},
  workflow::{
    get_image_details,
    get_images_details,
  },
};

pub fn create_artist(cxt: &Context, value: String, image_ids: Vec<i32>) -> Result<Artist, Error> {
  use diesel::pg::expression::dsl::any;

  let conn = cxt.make_conn()?;

  // Check that images all exist - it's okay if they already have an artist
  check_images_exist(&conn, &image_ids)?;

  let artist = NewArtist {
    name: value,
    created_time: get_utc_timestamp() as i64
  };

  conn.transaction::<_, Error, _>(|| {
    let artist = diesel::insert_into(artist::table)
      .values(artist)
      .get_results::<Artist>(&conn)?
      .pop()
      .ok_or(Error::db_error().attach("Newly created artist was not found"))?;
    diesel::update(image::table)
      .filter(image::id.eq(any(&image_ids)))
      .set(image::artist_id.eq(artist.id))
      .execute(&conn)?;
    Ok(artist)
  })
}

pub fn create_image_feature(cxt: &Context, image_id: Id, feature_id: Id, p1: (f32, f32), p2: (f32, f32)) -> Result<ClientImageDetails, Error> {
  let conn = cxt.make_conn()?;

  let target = NewImageFeature {
    image_id,
    feature_id,
    x1: f32::min(p1.0, p2.0),
    x2: f32::max(p1.0, p2.0),
    y1: f32::min(p1.1, p2.1),
    y2: f32::max(p1.1, p2.1),
  };

  conn.transaction::<_, Error, _>(|| {
    // Ensure that image and feature both exist and are not deleted.
    let image = image::table
      .filter(image::id.eq(image_id))
      .load::<Image>(&conn)?
      .pop()
      .ok_or(Error::not_found())?;
    if image.deleted {
      return Err(Error::deleted());
    }

    diesel::insert_into(image_feature::table)
      .values(target)
      .get_results::<ImageFeature>(&conn)?
      .pop()
      .ok_or(Error::db_error().attach("Newly created image feature was not found"))?;
    Ok(get_image_details(&conn, image_id)?.into())
  })
}

pub fn create_feature(cxt: &Context, value: String) -> Result<Feature, Error> {
  let conn = cxt.make_conn()?;

  let target = NewFeature {
    value,
    created_time: get_utc_timestamp() as i64
  };

  conn.transaction::<_, Error, _>(|| {
    let target = diesel::insert_into(feature::table)
      .values(target)
      .get_results::<Feature>(&conn)?
      .pop()
      .ok_or(Error::db_error().attach("Newly created feature was not found"))?;
    Ok(target)
  })
}

pub fn create_group(cxt: &Context, value: String, image_ids: Vec<ImgId>) -> Result<GroupData, Error> {
  use diesel::pg::expression::dsl::any;

  let conn = cxt.make_conn()?;

  // Check that images all exist and are not already in a group;
  let mut images = check_images_exist(&conn, &image_ids)?;
  for image in images.iter() {
    if image.group_id.is_some() {
      return Err(Error::request_error("Image is already in a different group"))
    }
  }

  let group = NewGroup {
    title: value,
    created_time: get_utc_timestamp() as i64
  };
  conn.transaction::<_, Error, _>(|| {
    let group = diesel::insert_into(group::table)
      .values(group)
      .get_results::<Group>(&conn)?
      .pop()
      .ok_or(Error::db_error().attach("Newly created group was not found"))?;
    diesel::update(image::table)
      .filter(image::id.eq(any(&image_ids)))
      .set(image::group_id.eq(group.id))
      .execute(&conn)?;
    // Update groupId in images since we are returning that data to client.
    for image in images.iter_mut() {
      image.group_id = Some(group.id);
    }
    let image_data = get_images_details(&conn, images)?;
    Ok((group, Vec::new(), Vec::new(), image_data))
  })
}

pub fn create_name(cxt: &Context, value: String, image_ids: Vec<ImgId>) -> Result<Name, Error> {
  let conn = cxt.make_conn()?;

  // Check that images all exist
  check_images_exist(&conn, &image_ids)?;

  let name = NewName {
    value,
    created_time: get_utc_timestamp() as i64
  };
  conn.transaction::<_, Error, _>(|| {
    let name = diesel::insert_into(name::table)
      .values(name)
      .get_results::<Name>(&conn)?
      .pop()
      .ok_or(Error::db_error().attach("Newly created name was not found"))?;
    let new_image_names: Vec<ImageName> = image_ids.iter().map(|img_id| (*img_id, name.id).into()).collect();
    diesel::insert_into(image_name::table)
      .values(&new_image_names)
      .execute(&conn)?;
    Ok(name)
  })
}

pub fn create_tag(cxt: &Context, value: String, image_ids: Vec<ImgId>) -> Result<Tag, Error> {
  let conn = cxt.make_conn()?;

  // Check that images all exist
  check_images_exist(&conn, &image_ids)?;

  let tag = NewTag {
    value,
    created_time: get_utc_timestamp() as i64
  };
  conn.transaction::<_, Error, _>(|| {
    let tag = diesel::insert_into(tag::table)
      .values(tag)
      .get_results::<Tag>(&conn)?
      .pop()
      .ok_or(Error::db_error().attach("Newly created tag was not found"))?;
    let new_image_tags: Vec<ImageTag> = image_ids.iter().map(|img_id| (*img_id, tag.id).into()).collect();
    diesel::insert_into(image_tag::table)
      .values(&new_image_tags)
      .execute(&conn)?;
    Ok(tag)
  })
}

fn check_images_exist(conn: &DBConn, image_ids: &Vec<ImgId>) -> Result<Vec<Image>, Error> {
  use diesel::pg::expression::dsl::any;
  let images = image::table
    .filter(image::id.eq(any(&image_ids)))
    .load::<Image>(conn)?;
  if images.len() != image_ids.len() {
    Err(Error::not_found())
  } else {
    Ok(images)
  }
}
