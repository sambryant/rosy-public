use diesel::prelude::*;

use crate::{
  context::Context,
  error::Error,
  models::{ArtId, GrpId, Id, ImgId, NmeId, TagId},
  schema::{artist, dupe, feature, group, image, image_feature, name, tag},
};

pub fn delete_artist(cxt: &Context, artist_id: ArtId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  conn.transaction::<_, Error, _>(|| {
    diesel::update(image::table)
      .filter(image::artist_id.eq(artist_id))
      .set(image::artist_id.eq::<Option<i32>>(None))
      .execute(&conn)?;
    diesel::update(artist::table)
      .filter(artist::id.eq(artist_id))
      .set(artist::deleted.eq(true))
      .execute(&conn)?;
    Ok(())
  })
}

pub fn delete_feature(cxt: &Context, id: Id) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  conn.transaction::<_, Error, _>(|| {
    diesel::delete(feature::table)
      .filter(feature::id.eq(id))
      .execute(&conn)?;
    Ok(())
  })
}

pub fn delete_image_feature(cxt: &Context, id: Id) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  conn.transaction::<_, Error, _>(|| {
    diesel::delete(image_feature::table)
      .filter(image_feature::id.eq(id))
      .execute(&conn)?;
    Ok(())
  })
}

pub fn delete_dupe_marks(cxt: &Context, image_ids: Vec<i32>) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  conn.transaction::<_, Error, _>(|| {
    diesel::delete(dupe::table)
      .filter(dupe::img1_id.eq_any(&image_ids))
      .filter(dupe::img2_id.eq_any(&image_ids))
      .execute(&conn)?;
    Ok(())
  })
}

pub fn delete_group(cxt: &Context, group_id: GrpId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  conn.transaction::<_, Error, _>(|| {
    diesel::update(image::table)
      .filter(image::group_id.eq(group_id))
      .set(image::group_id.eq::<Option<i32>>(None))
      .execute(&conn)?;
    diesel::update(group::table)
      .filter(group::id.eq(group_id))
      .set(group::deleted.eq(true))
      .execute(&conn)?;
    Ok(())
  })
}

pub fn delete_image(cxt: &Context, image_id: ImgId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  diesel::update(image::table)
    .filter(image::id.eq(image_id))
    .set(image::deleted.eq(true))
    .execute(&conn)?;
  Ok(())
}

pub fn delete_name(cxt: &Context, name_id: NmeId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  conn.transaction::<_, Error, _>(|| {
    diesel::delete(name::table)
      .filter(name::id.eq(name_id))
      .execute(&conn)?;
    Ok(())
  })
}

pub fn delete_tag(cxt: &Context, tag_id: TagId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  conn.transaction::<_, Error, _>(|| {
    diesel::delete(tag::table)
      .filter(tag::id.eq(tag_id))
      .execute(&conn)?;
    Ok(())
  })
}

pub fn restore_group(cxt: &Context, group_id: GrpId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  diesel::update(group::table)
    .filter(group::id.eq(group_id))
    .set(group::deleted.eq(false))
    .execute(&conn)?;
  // Check to make sure record exists
  group::table
    .select(group::id)
    .filter(group::id.eq(group_id))
    .load::<GrpId>(&conn)?
    .pop()
    .ok_or(Error::not_found())?;
  Ok(())
}

pub fn restore_image(cxt: &Context, image_id: ImgId) -> Result<(), Error> {
  let conn = cxt.make_conn()?;
  diesel::update(image::table)
    .filter(image::id.eq(image_id))
    .set(image::deleted.eq(false))
    .execute(&conn)?;
  // Check to make sure record exists
  image::table
    .select(image::id)
    .filter(image::id.eq(image_id))
    .load::<ImgId>(&conn)?
    .pop()
    .ok_or(Error::not_found())?;
  Ok(())
}
