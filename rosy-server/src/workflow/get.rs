use diesel::prelude::*;
use itertools::izip;
use std::collections::HashMap;

use crate::{
  client_models::ClientImageDetails,
  config::DBConn,
  context::Context,
  error::Error,
  models::*,
  schema::{dupe, image, image_name, image_tag},
  util::GroupedWith,
};
use super::get_images_details;


pub fn get_artists_details(conn: &DBConn, artists: Vec<Artist>) -> Result<Vec<ArtistDetailsData>, diesel::result::Error> {
  let image_data: Vec<(usize, f32)> = Image::belonging_to(&artists)
    .select((image::artist_id, image::rating_average))
    .load::<(Option<ArtId>, Option<f32>)>(conn)?
    .grouped_with(&artists, |artist| artist.id, |(artist_id, _)| artist_id.unwrap())
    .into_iter()
    .map(parse_related_image_list)
    .collect();
  Ok(izip!(artists, image_data).collect())
}

pub fn get_names_details(conn: &DBConn, items: Vec<Name>) -> Result<Vec<NameDetailsData>, diesel::result::Error> {
  let group_counts: Vec<usize> = GroupName::belonging_to(&items)
    .load::<GroupName>(conn)?
    .grouped_by(&items)
    .into_iter()
    .map(|images| images.len())
    .collect();
  let image_data: Vec<(usize, f32)> = ImageName::belonging_to(&items)
    .inner_join(image::table)
    .select((image_name::all_columns, image::rating_average))
    .load::<(ImageName, Option<f32>)>(conn)?
    .grouped_by(&items)
    .into_iter()
    .map(parse_related_image_list)
    .collect();
  let metadata: Vec<Option<NameMetadata>> = NameMetadata::belonging_to(&items)
    .load::<NameMetadata>(conn)?
    .grouped_by(&items)
    .into_iter()
    .map(|mut metadatas| metadatas.pop())
    .collect();

  Ok(izip!(items, group_counts, image_data, metadata).collect())
}

pub fn get_marked_dupes(cxt: &Context, target_id: ImgId) -> Result<Vec<(ClientImageDetails, DupeType)>, Error> {
  let conn = cxt.make_conn()?;

  // Collect all the IDs of images marked as duplicates
  let dupes: Vec<Dupe> = dupe::table
    .filter(dupe::img1_id.eq(target_id))
    .load::<Dupe>(&conn)?;
  let id_map: HashMap<ImgId, DupeType> = dupes.into_iter()
    .map(|d| (d.img2_id, d.dupe_type))
    .collect();

  // Query these images and return
  let images = image::table
    .filter(image::deleted.eq(false))
    .filter(image::id.eq_any(id_map.keys()))
    .load::<Image>(&conn)?;
  Ok(get_images_details(&conn, images)?
    .into_iter()
    .map(|image_data| {
      let dupe_type = id_map[&image_data.0.id].clone();
      (image_data.into(), dupe_type)
    })
    .collect())
}

pub fn get_tags_details(conn: &DBConn, items: Vec<Tag>) -> Result<Vec<TagDetailsData>, diesel::result::Error> {
  let group_counts: Vec<usize> = GroupTag::belonging_to(&items)
    .load::<GroupTag>(conn)?
    .grouped_by(&items)
    .into_iter()
    .map(|images| images.len())
    .collect();
  let image_data: Vec<(usize, f32)> = ImageTag::belonging_to(&items)
    .inner_join(image::table)
    .select((image_tag::all_columns, image::rating_average))
    .load::<(ImageTag, Option<f32>)>(conn)?
    .grouped_by(&items)
    .into_iter()
    .map(parse_related_image_list)
    .collect();
  let metadata: Vec<Option<TagMetadata>> = TagMetadata::belonging_to(&items)
    .load::<TagMetadata>(conn)?
    .grouped_by(&items)
    .into_iter()
    .map(|mut metadatas| metadatas.pop())
    .collect();

  Ok(izip!(items, group_counts, image_data, metadata).collect())
}

/// Takes a list of image ratings and converts it to an image count and overall rating score.
fn parse_related_image_list<T>(image_list: Vec<(T, Option<f32>)>) -> (usize, f32) {
  let image_count = image_list.len();
  let mut rating_total = 0.0;
  let mut rating_count = 0.0;
  for (_, rating_opt) in image_list.into_iter() {
    if let Some(rating) = rating_opt {
      rating_total += rating;
      rating_count += 1.0;
    }
  }
  let score = get_score(rating_total, rating_count);
  (image_count, score)
}

/// Computes a rating score using Laplace smoothing.
fn get_score(rating_total: f32, rating_count: f32) -> f32 {
  // We rescale the 1 - 5 rating to be 0 - 1.
  let rescaled_total = (rating_total - rating_count) / 4.0;
  // Then apply Laplace smoothing with alpha = 1.0, beta = 2.0.
  (rescaled_total + 1.0) / (rating_count + 2.0)
}
