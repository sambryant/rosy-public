use async_trait::async_trait;
use http::header::USER_AGENT;
use http::Uri;
use std::convert::TryInto;
use std::fs::File;
use std::io::copy;

use super::importer::Importer;
use crate::error::Error;
use crate::content_types::ContentType;

pub struct Downloader {
  url: String,
  source_page: Option<String>
}

impl Downloader {
  pub fn new(url: String, page: Option<String>) -> Downloader {
    Downloader { url, source_page: page }
  }
}

static SPOOF_USER_AGENT: &'static str = "Mozilla/5.0 (X11; Linux x86_64; rv:76.0) Gecko/20100101 Firefox/76.0";

#[async_trait]
impl Importer for Downloader {

  fn get_ext(&self) -> Result<String, Error> {
    let uri: Uri = self.url.clone()
      .try_into()
      .map_err(|e| Error::request_error("Invalid URL").attach(e))?;
    ContentType::get_ext_from_uri(&uri)
      .ok_or(Error::request_error("Could not determine file extension"))
  }

  async fn save_file(&self, dest: &mut File) -> Result<(), Error> {
    // Download the file from the url
    let client = reqwest::Client::new();
    let response = client
      .get(&self.url)
      .header(USER_AGENT, SPOOF_USER_AGENT)
      .send()
      .await
      .map_err(|e| Error::internal("Error downloading file").attach(e))?;
    if !response.status().is_success() {
      return Err(match response.status().as_u16() {
        403 => Error::not_authorized(),
        404 => Error::not_found(),
        code => Error::internal(&format!("Unknown download error, code: {}", code))
      });
    }
    let response = response
      .bytes()
      .await
      .map_err(|e| Error::internal("Error downloading file").attach(e))?;

    copy(&mut response.as_ref(), dest)
      .map_err(|e| Error::internal("Error downloading file").attach(e))?;
    Ok(())
  }

  fn get_source(&self) -> String {
    self.url.clone()
  }

  fn get_page(&self) -> Option<String> {
    self.source_page.clone()
  }
}
