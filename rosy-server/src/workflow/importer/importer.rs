use async_trait::async_trait;
use core::fmt::Debug;
use diesel::prelude::*;
use std::{
  ffi::OsStr,
  fs,
  fs::File,
  path::{Path, PathBuf},
};

use crate::prelude::*;
use crate::{
  common::get_utc_timestamp,
  config::file_config::CreateRecord,
  content_types::ContentType,
  error::Error,
  models::{Image, ImgId, NewImage},
  schema::image,
  workers::generate_image_index,
};

use super::thumbnail::make_thumbnail;

/// Determines method of importing file (i.e. uploaded from computer or downloaded from website)
#[async_trait]
pub trait Importer {
  fn get_ext(&self) -> Result<String, Error>;
  async fn save_file(&self, dest: &mut File) -> Result<(), Error>;
  fn get_source(&self) -> String;
  fn get_page(&self) -> Option<String>;
}

/// Struct tracking changes made during import in case we need to rollback.
#[derive(Clone, Debug)]
struct Changes {
  created_filename: CreateRecord<PathBuf>,
  created_thumbnail: CreateRecord<PathBuf>,
}

pub async fn import_file<T: Importer>(cxt: &Context, importer: T) -> Result<ImgId, Error> {
  let conn = cxt.make_conn()?;

  // Keep track of persistent changes we make so we can roll them back on error
  let mut changes = Changes {
    created_filename: None,
    created_thumbnail: None,
  };

  // Determine content type and extension, throw error if unhandled extension.
  let ext = importer.get_ext()?;
  let content_type = ContentType::get(ext.as_str())
    .ok_or(Error::request_error("Unknown content type"))?;
  let is_video = content_type == ContentType::Video;

  // Generate random filename to store content under
  let file_id = cxt.config.file.generate_new_file_id();
  let filename_column = cxt.get_filename_column(file_id, &ext);
  let filename_system = cxt.config.file.get_filename_system(&filename_column);

  // Create empty placeholder file for content
  let mut file_dest = create_new_file(&filename_system, &mut changes.created_filename)
    .map_err(|msg| Error::internal("Could not create file").attach(msg))?;

  // Save the file, insert the row, trigger index calculation
  importer.save_file(&mut file_dest)
    .await
    .and_then(|_| {
      match fs::metadata(&filename_system) {
        Ok(md) => Ok(md.len() as i32),
        Err(e) => {
          log_err(format!("Importer: couldnt determine file size for file {:?}: {:?}", filename_system, e));
          Ok(0)
        }
      }
    })
    .and_then(|size| {
      make_thumbnail(cxt, file_id, is_video, &filename_system, &mut changes.created_thumbnail)
        .map_err(|e| e.into())
        .map(|(thumb, length)| (size, thumb, length))
    })
    .and_then(|(filesize, thumbnail_column, length_column)| {
      let id = conn.transaction::<_, Error, _>(|| {
        insert_new_row(
          &conn,
          filename_column,
          thumbnail_column,
          filesize,
          is_video,
          length_column,
          importer.get_source(),
          importer.get_page())
      })?;
      if content_type == ContentType::Image {
        generate_image_index(cxt, id, &filename_system).unwrap_or_else(|msg| {
          log_wrn(format!("Failed to create image index for {}: {}", id, msg));
        });
      }
      Ok(id)
    })
    .map_err(|error| {
      cleanup(&cxt, changes);
      error
    })
}

fn create_new_file<T>(filename: &T, result: &mut Option<T>) -> Result<File, &'static str>
where T: AsRef<Path> + AsRef<OsStr> + Clone {
  if Path::new(filename).exists() {
    Err("Conflicting file exists with target name")
  } else {
    // Create empty file and record change
    let dest = File::create(filename)
      .map_err(|_| "Could not create file")?;
    *result = Some(filename.clone());
    Ok(dest)
  }
}

fn insert_new_row(
  conn: &DBConn, filename: String, thumbnail: String, filesize: i32, is_video: bool,
  length: Option<f32>, source: String, source_page: Option<String>
) -> Result<ImgId, Error> {
  let image = NewImage {
    filename,
    thumbnail,
    created_time: get_utc_timestamp() as i64,
    filesize,
    hidden: false,
    favorite: false,
    is_video,
    length,
    processed: false,
    source: Some(source),
    source_page: source_page,
    rating_count: 0,
  };
  let id = diesel::insert_into(image::table)
    .values(image)
    .get_results::<Image>(conn)?
    .pop()
    .ok_or(Error::db_error().attach("Newly created image was not found"))?
    .id;
  Ok(id)
}

fn cleanup(cxt: &Context, changes: Changes) {
  if let Some(fname) = &changes.created_filename {
    cxt.config.file.cleanup_file(fname);
  }
  if let Some(fname) = &changes.created_thumbnail {
    cxt.config.file.cleanup_file(fname);
  }
}
