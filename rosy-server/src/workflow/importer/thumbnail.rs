use image::{
  io::Reader as ImageReader,
  imageops::FilterType,
};
use std::{
  path::{Path, PathBuf},
  ffi::OsStr,
};

use crate::common::remove_file;
use crate::constants::THUMBNAIL_SIZE;
use crate::context::{Context, FileId};
use crate::error::Error as ClientError;
use crate::workers::{
  get_video_length,
  GetVideoLengthError,
  extract_video_frame,
  ExtractVideoFrameError,
};

#[derive(Debug)]
pub enum Error {
  RemoveExistingError(std::io::Error),
  GenerateThumbnailError(GenerateThumbnailError),
  GetVideoLengthError(GetVideoLengthError), // only applies to video content
  ExtractVideoFrameError(ExtractVideoFrameError), // only applies to video content
}

#[derive(Debug)]
pub enum GenerateThumbnailError {
  ImageOpen(std::io::Error),
  ImageFormat(std::io::Error),
  ImageDecode(image::ImageError),
  ThumbSave(image::ImageError)
}

pub fn make_thumbnail(cxt: &Context, file_id: FileId, is_video: bool, src_file: &PathBuf, created: &mut Option<PathBuf>) -> Result<(String, Option<f32>), Error> {

  let thumbnail_column = cxt.get_thumbnail_column(file_id);
  let thumbnail_system = cxt.config.file.get_thumbnail_system(&thumbnail_column);
  let mut created_temp_file = None;
  let mut video_length = None;

  remove_file(&thumbnail_system).map_err(|e| Error::RemoveExistingError(e))?;

  let result = if is_video {
    let temporary_frame_filename = cxt.config.file.get_temporary_frame_filename(file_id);
    remove_file(&temporary_frame_filename).map_err(|e| Error::RemoveExistingError(e))?;
    get_video_length(src_file)
      .map_err(|e| Error::GetVideoLengthError(e))
      .and_then(|length| {
        video_length = Some(length);
        let frame_time = length / 2.0; // take frame from 1/2 of video by default.
        extract_video_frame(src_file, &temporary_frame_filename, frame_time, &mut created_temp_file)
          .map_err(|e| Error::ExtractVideoFrameError(e))
      })
      .and_then(|_| {
        generate_thumbnail(&temporary_frame_filename, &thumbnail_system, created, THUMBNAIL_SIZE)
          .map_err(|e| Error::GenerateThumbnailError(e))
      })
  } else {
    generate_thumbnail(src_file, &thumbnail_system, created, THUMBNAIL_SIZE)
      .map_err(|e| Error::GenerateThumbnailError(e))
  };

  if let Some(fname) = created_temp_file {
    // Remove temporary file regardless of what happens
    cxt.config.file.cleanup_file(&fname);
  }

  result.map(|_| (thumbnail_column, video_length))
}

pub fn generate_thumbnail<S, T>(src_file: &S, dst_file: &T, created: &mut Option<T>, size: (u32, u32)) -> Result<(), GenerateThumbnailError>
where S: AsRef<OsStr> + AsRef<Path>,
      T: AsRef<OsStr> + AsRef<Path> + Clone
{
  use GenerateThumbnailError::*;
  let r = ImageReader::open(&src_file)
    .map_err(|e| ImageOpen(e))?
    .with_guessed_format()
    .map_err(|e| ImageFormat(e))?
    .decode()
    .map_err(|e| ImageDecode(e))?
    .resize(size.0, size.1, FilterType::Triangle)
    .save(&dst_file);
  // Even on failure, mark the file as having been created just in case its partially created.
  *created = Some(dst_file.clone());
  r.map_err(|e| ThumbSave(e))?;
  Ok(())
}

impl From<Error> for ClientError {
  fn from(err: Error) -> Self {
    use Error::*;
    match err {
      GenerateThumbnailError(e) => e.into(),
      GetVideoLengthError(e) => ClientError::internal("Could process video").attach_msg(&String::from(&e)),
      ExtractVideoFrameError(e) => ClientError::internal("Could process video").attach_msg((&e).into()),
      RemoveExistingError(e) => ClientError::internal("Could not remove existing file").attach(e),
    }
  }
}

impl From<GenerateThumbnailError> for ClientError {
  fn from(err: GenerateThumbnailError) -> Self {
    use GenerateThumbnailError::*;
    match err {
      ImageOpen(e) => ClientError::internal("Could not open image file").attach(e),
      ImageFormat(e) => ClientError::internal("Could not determine image format").attach(e),
      ImageDecode(e) => ClientError::internal("Could not decode image").attach(e),
      ThumbSave(e) => ClientError::internal("Could not save thumbnail").attach(e),
    }
  }
}
