use async_trait::async_trait;
use std::io::prelude::*;
use std::fs::File;

use super::importer::Importer;
use crate::error::Error;

pub struct Uploader {
  extension: String,
  content: Vec<u8>
}

impl Uploader {
  pub fn new(content: Vec<u8>, extension: String) -> Uploader {
    Uploader { extension, content }
  }
}

#[async_trait]
impl Importer for Uploader {
  fn get_ext(&self) -> Result<String, Error> {
    Ok(self.extension.clone())
  }

  async fn save_file(&self, dest: &mut File) -> Result<(), Error> {
    let mut pos = 0;
    while pos < self.content.len() {
      pos += dest.write(&self.content[pos..])
        .map_err(|e| Error::internal("Error writing file").attach(e))?;
    }
    Ok(())
  }

  fn get_source(&self) -> String {
    String::from("FILE")
  }

  fn get_page(&self) -> Option<String> {
    None
  }
}
