use crate::context::Context;
use crate::error::Error;
use crate::models::ImgId;

mod importer;
mod download_importer;
mod upload_importer;
pub mod thumbnail;

pub async fn import_from_url(cxt: &Context, url: String, page: Option<String>,
) -> Result<ImgId, Error> {
  let downloader = download_importer::Downloader::new(url, page);
  importer::import_file(cxt, downloader).await
}

pub async fn import_from_upload(cxt: &Context, extension: String, content: Vec<u8>) -> Result<ImgId, Error>{
  let uploader = upload_importer::Uploader::new(content, extension);
  importer::import_file(cxt, uploader).await
}
