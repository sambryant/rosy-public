use std::{
  ffi::{OsStr, OsString},
  fs::{remove_file, rename},
  path::Path,
};

use crate::prelude::*;

#[derive(Debug)]
pub struct PendingDelete {
  original_filename: OsString,
  temporary_filename: OsString,
}

#[derive(Debug)]
pub enum UndoError {
  RenameFailure(std::io::Error),
  OriginalExists,
  AlreadyRemoved
}

#[derive(Debug)]
pub enum MarkForDeletionError {
  RenameFailure(std::io::Error, OsString),
  SourceMissing,
  TargetExists,
}

pub fn remove_file_ignored<T>(file: T)
where T: AsRef<Path> + AsRef<OsStr> {
  if Path::new(&file).exists() {
    if let Result::Err(e) = remove_file(file) {
      log_wrn(format!("Failed to purge removed file: {:?}", e));
    }
  }
}

impl PendingDelete {

  pub fn new<T>(filename: T) -> Result<PendingDelete, MarkForDeletionError>
  where T: AsRef<OsStr>
  {
    let mut temporary_filename = OsString::from(filename.as_ref());
    temporary_filename.push(".tmp");
    let pd = PendingDelete {
      original_filename: OsString::from(filename.as_ref()),
      temporary_filename,
    };

    if !Path::new(&pd.original_filename).exists() {
      Err(MarkForDeletionError::SourceMissing)
    } else if Path::new(&pd.temporary_filename).exists() {
      Err(MarkForDeletionError::TargetExists)
    } else {
      rename(&pd.original_filename, &pd.temporary_filename)
        .map_err(|e| MarkForDeletionError::RenameFailure(e, pd.temporary_filename.clone()))
        .map(|_| pd)
    }
  }

  pub fn purge(self) -> Result<(), std::io::Error> {
    if Path::new(&self.temporary_filename).exists() {
      remove_file(&self.temporary_filename)
    } else {
      Ok(())
    }
  }

  pub fn purge_ignored(self) {
    if let Result::Err(e) = self.purge() {
      log_wrn(format!("Failed to purge removed file: {:?}", e));
    }
  }

  pub fn undo(self) -> Result<(), UndoError> {
    if Path::new(&self.temporary_filename).exists() {
      if !Path::new(&self.original_filename).exists() {
        rename(&self.temporary_filename, &self.original_filename)
          .map_err(|e| UndoError::RenameFailure(e))
      } else {
        Err(UndoError::OriginalExists)
      }
    } else {
      Err(UndoError::AlreadyRemoved)
    }
  }

  pub fn undo_ignored(self) {
    if let Result::Err(e) = self.undo() {
      log_err(format!("Unable to undo delete operation: {:?}", e));
    }
  }
}
