use std::path::Path;
use diesel::prelude::*;

use crate::prelude::*;
use crate::{
  config::ThreadId,
  error::Error as ClientError,
  models::Image,
  schema::image,
  threads::{update_thread, ThreadStatus},
  workflow::fileops::*,
};
use super::{
  super::importer::thumbnail::{
    make_thumbnail,
    Error as MakeThumbnailError
  },
  ThreadWorker
};

pub struct RegenerateThumbnails {}

impl ThreadWorker for RegenerateThumbnails {

  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    let conn = cxt.make_conn()?;
    let images = image::table
      .filter(image::deleted.eq(false))
      .load::<Image>(&conn)?;
    let total = images.len();

    for (i, image) in images.iter().enumerate() {
      update_thread(&cxt.config, thread_id, ThreadStatus::progress(i, total)).ok();

      if let Err(e) = regenerate_thumbnail(&conn, cxt, image) {
        log_err(format!("Regenerate thumbnail failed for image {}: {:?}", image.id, e));
      }
    }
    Ok(())
  }
}

#[derive(Debug)]
enum Error {
  UpdateDB(diesel::result::Error),
  RemoveOldThumbnail(MarkForDeletionError),
  BadFilename,
  MakeThumbnailError(MakeThumbnailError),
}

fn regenerate_thumbnail(conn: &DBConn, cxt: &Context, image: &Image) -> Result<(), Error> {
  let thumbnail_system = cxt.config.file.get_thumbnail_system(&image.thumbnail);

  // If the image already has a thumbnail, remove it (temporarily rename it)
  let delete_original_thumb = if Path::new(&thumbnail_system).exists() {
    Some(PendingDelete::new(&thumbnail_system).map_err(|e| Error::RemoveOldThumbnail(e))?)
  } else {
    None
  };

  // Determine file id from the file name
  let file_id = cxt.config.file.extract_file_id(&image.filename).map_err(|_| Error::BadFilename)?;
  let filename_system = cxt.config.file.get_filename_system(&image.filename);
  let mut created_file = None;

  match make_thumbnail(cxt, file_id, image.is_video, &filename_system, &mut created_file)
    .map_err(|e| Error::MakeThumbnailError(e))
    .and_then(|(thumbnail_column, length_column)| {
      diesel::update(image::table)
        .filter(image::id.eq(image.id))
        .set((image::thumbnail.eq(thumbnail_column), image::length.eq(length_column)))
        .execute(conn)
        .map_err(|e| Error::UpdateDB(e))
    }) {
    Ok(_) => {
      if let Some(thumbnail_delete) = delete_original_thumb {
        thumbnail_delete.purge_ignored();
      }
      Ok(())
    },
    Err(e) => {
      if let Some(file) = created_file {
        cxt.config.file.cleanup_file(&file);
      }
      if let Some(thumbnail_delete) = delete_original_thumb {
        thumbnail_delete.undo_ignored();
      }
      Err(e)
    }
  }
}


