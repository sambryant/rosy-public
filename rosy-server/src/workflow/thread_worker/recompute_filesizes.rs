use diesel::prelude::*;
use std::fs;

use crate::prelude::*;
use crate::{
  config::ThreadId,
  error::Error as ClientError,
  models::Image,
  schema::image,
  threads::{update_thread, ThreadStatus},
};
use super::ThreadWorker;

pub struct RecomputeFilesizes {}

impl ThreadWorker for RecomputeFilesizes {

  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    let conn = cxt.make_conn()?;
    let images = image::table
      .filter(image::deleted.eq(false))
      .load::<Image>(&conn)?;
    let total = images.len();

    for (i, image) in images.iter().enumerate() {
      update_thread(&cxt.config, thread_id, ThreadStatus::progress(i, total)).ok();

      if let Err(e) = recompute_filesize(&conn, cxt, image) {
        log_err(format!("File size computation failed for image {}: {:?}", image.id, e));
      }
    }
    Ok(())
  }
}

#[derive(Debug)]
enum Error {
  UpdateDB(diesel::result::Error),
  IOError(std::io::Error)
}

fn recompute_filesize(conn: &DBConn, cxt: &Context, image: &Image) -> Result<(), Error> {
  let filename_system = cxt.config.file.get_filename_system(&image.filename);
  let size = fs::metadata(filename_system)
    .map_err(|e| Error::IOError(e))?
    .len() as i32;
  diesel::update(image::table)
    .filter(image::id.eq(image.id))
    .set(image::filesize.eq(size))
    .execute(conn)
    .map_err(|e| Error::UpdateDB(e))?;
  Ok(())
}


