use std::{
  ffi::OsStr,
  process::Command,
  thread,
};

use crate::api::requests::{
  GenerateDatasetRequest,
  GenerateFeatureDatasetRequest,
  GenerateTagDatasetRequest,
};
use crate::command;
use crate::config::ThreadId;
use crate::context::Context;
use crate::error::Error as ClientError;
use crate::threads::*;
use crate::train::{FullDatasetGenerator, FeatureDatasetGenerator, TagDatasetGenerator};

/// Reprents a job that runs on a separate thread.
pub trait ThreadWorker : Sized + Send + 'static {

  fn run(self, cxt: &Context) -> Result<(ThreadId, ThreadStatus), ClientError> {
    Self::precheck(cxt)?;

    let (thread_id, thread_status) = create_thread(&cxt.config)
      .map_err(|e| ClientError::create_thread_error(e))?;
    let context = cxt.clone();
    thread::spawn(move || {
      match self.do_work(&context, thread_id) {
        Ok(_) => update_thread(&context.config, thread_id, ThreadStatus::success()).ok(),
        Err(e) => update_thread(&context.config, thread_id, ThreadStatus::error(e)).ok(),
      }
    });
    Ok((thread_id, thread_status))
  }

  /// Hook method called before attempting to spawn a thread.
  fn precheck(_cxt: &Context) -> Result<(), ClientError> {
    Ok(())
  }

  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError>;

}

/// Reprents an external shell command that runs on a separate thread.
trait ExternalWorker: Sized + Send + 'static {
  type ArgType: AsRef<OsStr>;

  fn args(cxt: &Context, thread_id: ThreadId) -> Vec<Self::ArgType>;

  fn command() -> &'static str;

  fn failure_code_to_string(code: i32) -> &'static str;

  fn spawn_command(cxt: &Context, thread_id: ThreadId) -> Result<(), &'static str> {
    command::run_command(Command::new(Self::command()).args(Self::args(cxt, thread_id)), true)
      .map(|_| ())
      .map_err(|e| (&e).into())
  }

}

impl<T> ThreadWorker for T where T: ExternalWorker {
  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    Self::spawn_command(cxt, thread_id).map_err(|msg| ClientError::unknown().attach(msg))
  }
}

mod merge_duplicates;
mod recompute_filesizes;
mod regenerate_thumbnails;
mod reindex_images;
mod reset_files;

pub fn start_generate_dataset(cxt: &Context, req: GenerateDatasetRequest) -> Result<(ThreadId, ThreadStatus), ClientError> {
  FullDatasetGenerator::from(req).run(cxt)
}

pub fn start_generate_feature_dataset(cxt: &Context, req: GenerateFeatureDatasetRequest) -> Result<(ThreadId, ThreadStatus), ClientError> {
  FeatureDatasetGenerator::from(req).run(cxt)
}

pub fn start_generate_tag_dataset(cxt: &Context, req: GenerateTagDatasetRequest) -> Result<(ThreadId, ThreadStatus), ClientError> {
  TagDatasetGenerator::from(req).run(cxt)
}

pub fn start_merge_duplicates(cxt: &Context) -> Result<(ThreadId, ThreadStatus), ClientError> {
  merge_duplicates::MergeDuplicates{}.run(cxt)
}

pub fn start_recompute_filesizes(cxt: &Context) -> Result<(ThreadId, ThreadStatus), ClientError> {
  recompute_filesizes::RecomputeFilesizes{}.run(cxt)
}

pub fn start_regenerate_thumbnails(cxt: &Context) -> Result<(ThreadId, ThreadStatus), ClientError> {
  regenerate_thumbnails::RegenerateThumbnails{}.run(cxt)
}

pub fn start_reindex_images(cxt: &Context) -> Result<(ThreadId, ThreadStatus), ClientError> {
  reindex_images::ReindexImages{}.run(cxt)
}

pub fn start_reset_files(cxt: &Context) -> Result<(ThreadId, ThreadStatus), ClientError> {
  reset_files::ResetFiles{}.run(cxt)
}
