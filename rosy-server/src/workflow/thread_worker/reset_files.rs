use core::fmt::Debug;
use diesel::prelude::*;
use std::path::PathBuf;

use crate::prelude::*;
use crate::{
  config::{
    RenameError,
    ThreadId,
    RenameRecord,
  },
  content_types::ContentType,
  error::Error as ClientError,
  models::Image,
  schema::image,
  threads::{update_thread, ThreadStatus},
};
use super::ThreadWorker;

pub struct ResetFiles {}

impl ThreadWorker for ResetFiles {

  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    let conn = cxt.make_conn()?;
    let images = image::table
      .filter(image::deleted.eq(false))
      .load::<Image>(&conn)?;
    let total = images.len();

    for (i, image) in images.iter().enumerate() {
      update_thread(&cxt.config, thread_id, ThreadStatus::progress(i, total)).ok();

      if let Err(e) = fix_files(&conn, cxt, image) {
        log_err(format!("Fixing files failed for image {}: {:?}", image.id, e));
      }
    }
    Ok(())
  }
}

#[derive(Debug)]
enum Error {
  ExtensionError(),
  Rename(RenameError),
  UpdateDB(diesel::result::Error),
}

struct Changes {
  moved_file: RenameRecord<PathBuf>,
  moved_thumb: RenameRecord<PathBuf>,
}

fn fix_files(conn: &DBConn, cxt: &Context, target: &Image) -> Result<(), Error> {
  let mut changes = Changes { moved_file: None, moved_thumb: None };

  let file_id = cxt.config.file.generate_new_file_id();

  ContentType::get_ext_from_filename(&target.filename)
    .ok_or(Error::ExtensionError())
    .and_then(|extension| {
      let new_filename_column = cxt.get_filename_column(file_id, &extension);
      cxt.config.file.rename_safe(
        cxt.config.file.get_filename_system(&target.filename),
        cxt.config.file.get_filename_system(&new_filename_column),
        &mut changes.moved_file
      )
        .map_err(|e| Error::Rename(e))
        .map(|_| new_filename_column)
    })
    .and_then(|new_filename_column| {
      let new_thumbnail_column = cxt.get_thumbnail_column(file_id);
      cxt.config.file.rename_safe(
        cxt.config.file.get_thumbnail_system(&target.thumbnail),
        cxt.config.file.get_thumbnail_system(&new_thumbnail_column),
        &mut changes.moved_thumb
      )
        .map_err(|e| Error::Rename(e))
        .map(|_| (new_filename_column, new_thumbnail_column))
    })
    .and_then(|(new_filename_column, new_thumbnail_column)| {
      diesel::update(image::table)
        .filter(image::id.eq(target.id))
        .set((image::filename.eq(new_filename_column), image::thumbnail.eq(new_thumbnail_column)))
        .execute(conn)
        .map_err(|e| Error::UpdateDB(e))?;
        Ok(())
    })
    .map_err(|e| {
      cxt.config.file.rename_cleanup(&mut changes.moved_file);
      cxt.config.file.rename_cleanup(&mut changes.moved_thumb);
      e
    })
}
