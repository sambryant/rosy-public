use diesel::prelude::*;
use std::collections::{HashMap, HashSet};


use crate::config::{DBConn, ThreadId};
use crate::context::Context;
use crate::error::Error as ClientError;
use crate::models::{Image, ImageIndex, ImgId, FileHash};
use crate::schema::{image, image_index};
use crate::threads::*;
use crate::workflow;
use super::ThreadWorker;

pub struct MergeDuplicates {}

impl ThreadWorker for MergeDuplicates {
  fn do_work(self, cxt: &Context, thread_id: ThreadId) -> Result<(), ClientError> {
    let conn = cxt.make_conn()?;
    let dupes = detect_duplicates(&conn)?;
    merge_results(cxt, dupes, thread_id)
      .map_err(|e| ClientError::unknown().attach(e))?;
    Ok(())
  }
}

fn detect_duplicates(conn: &DBConn) -> Result<Vec<Vec<ImgId>>, ClientError> {
  let indexes: Vec<ImageIndex> = image_index::table
    .inner_join(image::table)
    .filter(image::deleted.eq(false))
    .load::<(ImageIndex, Image)>(conn)?
    .into_iter()
    .map(|(img, _)| img)
    .collect();

  let mut hash_dupes: HashSet<FileHash> = HashSet::new();
  let mut hash_to_ids: HashMap<FileHash, Vec<ImgId>> = HashMap::new();

  for index in indexes {
    if let Some(hash) = index.filehash {
      match hash_to_ids.get_mut(&hash) {
        Some(ids) => {
          ids.push(index.id);
          hash_dupes.insert(hash);
        },
        None => {
          hash_to_ids.insert(hash, vec![index.id]);
        }
      }
    }
  }
  Ok(hash_dupes.into_iter()
    .map(|hash| hash_to_ids.remove(&hash).unwrap())
    .collect())
}

fn merge_results(cxt: &Context, duplicates: Vec<Vec<ImgId>>, thread_id: ThreadId) -> Result<(), workflow::merge_images::Error> {
  let num = duplicates.len();
  for i in 0..num {
    update_thread(&cxt.config, thread_id, ThreadStatus::progress(num + i, 2 * num)).ok(); // 50% - 100%
    workflow::merge_images::merge_images(cxt, &duplicates[i])?;
  }
  Ok(())
}
