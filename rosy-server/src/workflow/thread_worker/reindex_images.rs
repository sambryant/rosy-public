use std::ffi::OsString;

use crate::config::ThreadId;
use crate::context::Context;

use super::ExternalWorker;

pub struct ReindexImages {}

impl ExternalWorker for ReindexImages {
  type ArgType = OsString;

  fn args(cxt: &Context, thread_id: ThreadId) -> Vec<OsString> {
    let thread_filename = cxt.config.file.get_thread_filename(thread_id).into_os_string();
    vec![
      OsString::from("-m"),
      OsString::from("rosy.workers.reindex_images"),
      OsString::from(&cxt.config.file.get_media_dir()),
      OsString::from(&cxt.config.db.database),
      OsString::from(&cxt.schema),
      OsString::from("--exclude-existing"),
      OsString::from(thread_filename),
    ]
  }

  fn command() -> &'static str { "python3" }

  fn failure_code_to_string(code: i32) -> &'static str {
    match code {
      1 => "worker thread failed with code 1 (unhandled exception)",
      2 => "worker thread failed with code 2 (improper arguments)",
      3 => "worker thread failed with code 3 (improper environment)",
      4 => "worker thread failed with code 4 (database error)",
      5 => "worker thread failed with code 5 (unknown reason)",
      _ => "worker thread failed with unknown code (bug)"
    }
  }
}
