use std::cmp::Ordering;
use crate::query::SortDirection;

mod artists;
mod deleted;
mod duplicates;
mod eigendata;
mod groups;
mod images;
mod names;
mod tags;

fn sort_by_float<T, F>(items: &mut Vec<T>, f: F, dir: SortDirection)
where F: Fn(&T) -> f32
{
  match dir {
    SortDirection::Asc => {
      items.sort_by(|a, b| f(a).partial_cmp(&f(b)).unwrap())
    }
    SortDirection::Desc => {
      items.sort_by(|b, a| f(a).partial_cmp(&f(b)).unwrap())
    }
  }
}

fn sort_by_key<T, K, F>(items: &mut Vec<T>, f: F, dir: SortDirection)
where F: Fn(&T) -> K,
      K: Ord
{
  match dir {
    SortDirection::Asc => {
      items.sort_by(|a, b| f(a).cmp(&f(b)))
    }
    SortDirection::Desc => {
      items.sort_by(|b, a| f(a).cmp(&f(b)))
    }
  }
}

fn sort_by_cmp<T, F>(items: &mut Vec<T>, f: F, dir: SortDirection)
where F: Fn(&T, &T) -> Ordering
{
  match dir {
    SortDirection::Asc => {
      items.sort_by(|a, b| f(a, b))
    }
    SortDirection::Desc => {
      items.sort_by(|b, a| f(a, b))
    }
  }
}

pub use artists::{
  query_artists,
  QueryArtistSortMethod,
};
pub use deleted::{
  query_deleted
};
pub use duplicates::{
  query_dupe_candidates,
  query_face_matches,
  FaceMatch,
  DupeCandidate,
  DupeCandidateMethod,
};
pub use eigendata::{
  query_eigendata,
};
pub use groups::{
  query_groups,
  QueryGroupSortMethod,
};
pub use images::{
  query_images,
  QueryImageSortMethod,
};
pub use names::{
  query_names,
  QueryNameSortMethod,
};
pub use tags::{
  query_tags,
  QueryTagSortMethod,
};
