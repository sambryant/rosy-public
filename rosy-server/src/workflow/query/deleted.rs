use diesel::prelude::*;

use crate::{
  api::requests::QueryDeletedRequest,
  client_models::ClientImageDetails,
  common::cov_into,
  context::Context,
  error::Error,
  models::Image,
  schema::image,
};
use super::super::get_images_details;


pub fn query_deleted(cxt: &Context, req: QueryDeletedRequest) -> Result<(i64, Vec<ClientImageDetails>), Error> {
  let conn = cxt.make_conn()?;

  let count = image::table
    .filter(image::deleted.eq(true))
    .count()
    .get_result(&conn)?;
  let images: Vec<Image> = image::table
    .filter(image::deleted.eq(true))
    .limit(req.size)
    .offset(req.page * req.size)
    .load(&conn)?;

  let client_models: Vec<ClientImageDetails> = cov_into(get_images_details(&conn, images)?);
  Ok((count, client_models))
}
