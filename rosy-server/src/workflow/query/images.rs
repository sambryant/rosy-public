use diesel::{dsl::exists, dsl::not, prelude::*, sql_types};

use crate::{
  api::requests::QueryImagesRequest,
  client_models::ClientImageDetails,
  common::cov_into,
  config::DBType,
  context::Context,
  error::Error,
  models::Image,
  query::*,
  schema::{image, image_feature, image_name, image_tag},
};
use super::super::get_images_details;

no_arg_sql_function!(RANDOM, sql_types::Integer, "Represents the sql RANDOM() function");

#[derive(PartialEq, Debug)]
pub enum QueryImageSortMethod {
  Id,
  Favorite,
  Filesize,
  Processed,
  ArtistId,
  GroupId,
  Random,
  CreatedTime,
  ProcessedTime,
  Rating,
  RatingCount,
  Length,
}


fn apply_filters<'a>(
  req: &'a QueryImagesRequest, mut query: image::BoxedQuery<'a, DBType>,
) -> image::BoxedQuery<'a, DBType> {
  if let Some(features) = &req.features {
    for feature_id in features {
      query = query.filter(exists(
        image_feature::table
          .filter(image_feature::image_id.eq(image::id))
          .filter(image_feature::feature_id.eq(feature_id))
          .limit(1),
      ));
    }
  }
  if let Some(names) = &req.names {
    for name_id in names {
      query = query.filter(exists(
        image_name::table
          .filter(image_name::img_id.eq(image::id))
          .filter(image_name::nme_id.eq(name_id))
          .limit(1),
      ));
    }
  }
  if let Some(tags) = &req.tags {
    for tag_id in tags {
      query = query.filter(exists(
        image_tag::table
          .filter(image_tag::img_id.eq(image::id))
          .filter(image_tag::tag_id.eq(tag_id))
          .limit(1),
      ));
    }
  }

  // Blacklist names, features, tags
  if let Some(not_features) = &req.not_features {
    for feature_id in not_features {
      query = query.filter(not(exists(
        image_feature::table
          .filter(image_feature::image_id.eq(image::id))
          .filter(image_feature::feature_id.eq(feature_id))
          .limit(1),
      )));
    }
  }
  if let Some(not_names) = &req.not_names {
    for name_id in not_names {
      query = query.filter(not(exists(
        image_name::table
          .filter(image_name::img_id.eq(image::id))
          .filter(image_name::nme_id.eq(name_id))
          .limit(1),
      )));
    }
  }
  if let Some(not_tags) = &req.not_tags {
    for tag_id in not_tags {
      query = query.filter(not(exists(
        image_tag::table
          .filter(image_tag::img_id.eq(image::id))
          .filter(image_tag::tag_id.eq(tag_id))
          .limit(1),
      )));
    }
  }

  // Other filters
  if let Some(created_time_filters) = &req.created_time {
    for filter in created_time_filters.iter() {
      query = match filter.relation {
        OrderRelation::Higher => query.filter(image::created_time.ge(filter.value)),
        OrderRelation::Lower => query.filter(image::created_time.le(filter.value)),
      };
    }
  }

  if let Some(rating_filters) = &req.rating {
    for filter in rating_filters.iter() {
      query = match filter.relation {
        OrderRelation::Higher => query.filter(image::rating_average.ge(filter.value)),
        OrderRelation::Lower => query.filter(image::rating_average.le(filter.value)),
      };
    }
  }

  if let Some(rating_count_filters) = &req.rating_count {
    for filter in rating_count_filters.iter() {
      query = match filter.relation {
        OrderRelation::Higher => query.filter(image::rating_count.ge(filter.value)),
        OrderRelation::Lower => query.filter(image::rating_count.le(filter.value)),
      };
    }
  }

  if let Some(processed_time_filters) = &req.processed_time {
    for filter in processed_time_filters.iter() {
      query = match filter.relation {
        OrderRelation::Higher => query.filter(image::processed_time.ge(filter.value)),
        OrderRelation::Lower => query.filter(image::processed_time.le(filter.value)),
      };
    }
  }

  if let Some(artist_ids) = &req.artists {
    query = query.filter(image::artist_id.eq_any(artist_ids));
  }

  if let Some(group_ids) = &req.groups {
    query = query.filter(image::group_id.eq_any(group_ids));
  }

  if let Some(state) = req.favorite {
    query = query.filter(image::favorite.eq(state));
  }

  if let Some(state) = req.is_video {
    query = query.filter(image::is_video.eq(state));
  }

  if let Some(state) = req.marked {
    query = query.filter(image::marked.eq(state));
  }

  if let Some(state) = req.processed {
    query = query.filter(image::processed.eq(state));
  }

  if let Some(state) = req.hidden {
    query = query.filter(image::hidden.eq(state));
  }

  if let Some(has_artist) = req.has_artist {
    query = if has_artist {
      query.filter(image::artist_id.is_not_null())
    } else {
      query.filter(image::artist_id.is_null())
    };
  }

  if let Some(grouped) = req.grouped {
    query = if grouped {
      query.filter(image::group_id.is_not_null())
    } else {
      query.filter(image::group_id.is_null())
    };
  }

  query = query.filter(image::deleted.eq(false));

  query
}

fn apply_sorting<'a>(
  req: &'a QueryImagesRequest, mut query: image::BoxedQuery<'a, DBType>,
) -> Result<image::BoxedQuery<'a, DBType>, Error> {
  // Sort results by requested column
  use QueryImageSortMethod::*;
  match req.sort_by {
    Id => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::id.desc())
      } else {
        query = query.order(image::id.asc())
      }
      Ok(query)
    }
    CreatedTime => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::created_time.desc());
        Ok(query.then_order_by(image::id.desc()))
      } else {
        query = query.order(image::created_time.asc());
        Ok(query.then_order_by(image::id.asc()))
      }
    }
    ProcessedTime => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::processed_time.desc());
        Ok(query.then_order_by(image::id.desc()))
      } else {
        query = query.order(image::processed_time.asc());
        Ok(query.then_order_by(image::id.asc()))
      }
    }
    Rating => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::rating_average.desc())
      } else {
        query = query.order(image::rating_average.asc())
      }
      Ok(query.then_order_by(RANDOM))
    }
    RatingCount => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::rating_count.desc())
      } else {
        query = query.order(image::rating_count.asc())
      }
      Ok(query.then_order_by(RANDOM))
    }
    Filesize => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::filesize.desc())
      } else {
        query = query.order(image::filesize.asc())
      }
      Ok(query.then_order_by(image::created_time.asc()))
    }
    Favorite => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::favorite.desc())
      } else {
        query = query.order(image::favorite.asc())
      }
      Ok(query.then_order_by(image::created_time.asc()))
    }
    Length => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::length.desc())
      } else {
        query = query.order(image::length.asc())
      }
      Ok(query.then_order_by(image::created_time.asc()))
    }
    Processed => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::processed.desc())
      } else {
        query = query.order(image::processed.asc())
      }
      Ok(query.then_order_by(image::created_time.asc()))
    }
    ArtistId => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::artist_id.desc())
      } else {
        query = query.order(image::artist_id.asc())
      }
      Ok(query.then_order_by(image::created_time.asc()))
    }
    GroupId => {
      if let SortDirection::Desc = req.direction {
        query = query.order(image::group_id.desc())
      } else {
        query = query.order(image::group_id.asc())
      }
      Ok(query.then_order_by(image::created_time.asc()))
    }
    Random => {
      Ok(query.order(RANDOM))
    }
  }
}

pub fn query_images(
  cxt: &Context, req: QueryImagesRequest,
) -> Result<(i64, Vec<ClientImageDetails>), Error> {
  let conn = cxt.make_conn()?;

  // We have to issue query twice, once to get full count and once to get results
  let count = apply_filters(&req, image::table.into_boxed())
    .count()
    .get_result(&conn)?;

  let mut query = apply_filters(&req, image::table.into_boxed());
  query = apply_sorting(&req, query)?
    .limit(req.size)
    .offset(req.page * req.size);

  let images = query.load::<Image>(&conn)?;
  let client_models = cov_into(get_images_details(&conn, images)?);
  Ok((count, client_models))
}
