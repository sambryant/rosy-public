use diesel::prelude::*;

use crate::{
  api::requests::QueryTagsRequest,
  context::Context,
  error::Error,
  models::TagDetailsData,
  schema::tag,
};
use super::{
  sort_by_cmp,
  sort_by_float,
  sort_by_key,
  super::get::get_tags_details,
};

pub enum QueryTagSortMethod {
  Id,
  CreatedTime,
  GroupCount,
  ImageCount,
  Score,
  Value,
}

pub fn query_tags(
  cxt: &Context, req: QueryTagsRequest,
) -> Result<(i64, Vec<TagDetailsData>), Error> {
  let conn = cxt.make_conn()?;

  let items = tag::table.load(&conn)?;
  let mut data = get_tags_details(&conn, items)?;
  let t = &mut data;

  use QueryTagSortMethod::*;
  match req.sort_by {
    Id => sort_by_key(t, |item| item.0.id, req.direction),
    CreatedTime => sort_by_key(t, |item| item.0.created_time, req.direction),
    GroupCount => sort_by_key(t, |item| item.1, req.direction),
    ImageCount => sort_by_key(t, |item| item.2.0, req.direction),
    Score => sort_by_float(t, |item| item.2.1, req.direction),
    Value => sort_by_cmp(t, |a, b| a.0.value.cmp(&b.0.value), req.direction),
  };
  Ok((data.len() as i64, data))
}
