use diesel::prelude::*;
use std::collections::HashMap;

use crate::{
  common::remove_after_index,
  config::DBConn,
  context::Context,
  error::Error,
  models::{
    ContentMeta, Name, Tag, MetadataEigendata
  },
  schema::{name, tag, metadata_eigendata},
};

pub fn query_eigendata(cxt: &Context, num_vecs: usize, num_weights: usize) -> Result<Vec<(f32, Vec<(ContentMeta, f32)>)>, Error> {
  let conn = cxt.make_conn()?;

  let indexed = Indexed::new(&conn)?;

  // Only keep most relevant eigenvectors
  let mut data: Vec<MetadataEigendata> = metadata_eigendata::table.load(&conn)?;
  data.sort_by(|a, b| b.eigenvalue.partial_cmp(&a.eigenvalue).unwrap());
  remove_after_index(&mut data, num_vecs);

  // For each eigenvector, sort by most relevant weights and map to labels
  Ok(data.into_iter().map(|d| {
    let mut evec: Vec<(i32, f32)> = d.eigenvector.into_iter()
      .enumerate()
      .map(|(index, w)| (index as i32, w))
      .collect();

    // Sort by absolute value of the eigenvector weights and only keep most relevant weights.
    evec.sort_by(|a, b| b.1.abs().partial_cmp(&a.1.abs()).unwrap());
    remove_after_index(&mut evec, num_weights);

    // Multiply by +/- 1 so that most relevant weight is positive
    let factor = if evec[0].1 > 0.0 { 1.0 } else { -1.0 };

    (d.eigenvalue, evec.into_iter().map(|(index, w)| {
      let label = indexed.lookup(index).expect(&format!("No find name/tag with index: {}", index));
      (label, factor * w)
    }).collect())
  }).collect())
}

struct Indexed {
  names: HashMap<i32, Name>,
  tags: HashMap<i32, Tag>,
}

impl Indexed {
  pub fn new(conn: &DBConn) -> Result<Self, Error> {
    Ok(Self {
      // Get names/tags so we can lookup the label by indexes
      names: name::table.load(conn)?
        .into_iter()
        .filter_map(|n: Name| {
          n.index.map(|index| (index, n))
        })
        .collect(),
      tags: tag::table.load(conn)?
        .into_iter()
        .filter_map(|n: Tag| {
          n.index.map(|index| (index, n))
        })
        .collect(),
    })
  }

  pub fn lookup(&self, index: i32) -> Option<ContentMeta> {
    match self.names.get(&index) {
      Some(m) => Some(ContentMeta::Name(m.clone())),
      None => match self.tags.get(&index) {
        Some(m) => Some(ContentMeta::Tag(m.clone())),
        None => None
      }
    }
  }
}
