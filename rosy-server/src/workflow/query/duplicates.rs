use diesel::prelude::*;
use std::collections::{HashMap, HashSet};

use crate::{
  api::requests::{
    QueryFaceMatchesRequest,
    QueryDupeCandidatesRequest,
  },
  config::DBConn,
  context::Context,
  error::Error,
  models::{DupeType, Image, ImageData, ImageIndex, ImgId},
  schema::{dupe, image, image_index},
  workflow::get_images_details,
};

pub enum DupeCandidateMethod {
  FaceComparison,
}

pub struct DupeCandidate {
  pub item1: ImageData,
  pub item2: ImageData,
  pub score: f64,
  pub mark: Option<DupeType>,
}

pub struct FaceMatch {
  pub item: ImageData,
  pub score: f64,
}

pub use dupe_candidate::query_dupe_candidates;
pub use face_matches::query_face_matches;

mod dupe_candidate {
  use super::*;

  pub fn query_dupe_candidates(cxt: &Context, req: QueryDupeCandidatesRequest) -> Result<(i64, Vec<DupeCandidate>), Error> {
    let conn = cxt.make_conn()?;

    // Get list of all indexed images which are not deleted.
    let (images, indexes): (Vec<Image>, Vec<ImageIndex>) = get_image_indexes(&conn, None)?.into_iter().unzip();

    // This step is quite slow
    let cutoff = req.cutoff as f64;
    let dupe_pairs = compute_face_matches(&conn, &indexes, req.marked, cutoff)?;
    let total_count = dupe_pairs.len() as i64;

    let dupe_pairs = apply_pagination(dupe_pairs, req.page, req.size);

    // Get full image details and store in a map
    let img_map: HashMap<ImgId, ImageData> = get_images_details(&conn, images)?
      .into_iter()
      .map(|data| (data.0.id, data))
      .collect();

    let candidates = dupe_pairs.into_iter()
      .map(|((id1, id2), score)| {
        DupeCandidate::new(img_map[&id1].clone(), img_map[&id2].clone(), score)
      })
      .collect();

    Ok((total_count, candidates))
  }

  fn compute_face_matches(conn: &DBConn, indexes: &Vec<ImageIndex>, marked: Option<bool>, cutoff: f64) -> Result<Vec<((ImgId, ImgId), f64)>, Error> {
    let mut dupes = Vec::new();
    for i in 0..(indexes.len() - 1) {
      let in1 = &indexes[i];
      for j in (i+1)..indexes.len() {
        let in2 = &indexes[j];
        let d = in1.get_distance(&in2);
        if d <= cutoff {
          dupes.push(((in1.id, in2.id), d))
        }
      }
    }
    match marked {
      None => Ok(dupes),
      Some(value) => {
        let marked_pairs = get_marked_dupes(conn, None)?;
        Ok(dupes.into_iter()
          .filter(|(id_pair, _)| value == marked_pairs.contains(id_pair))
          .collect())
      }
    }
  }

}

mod face_matches {
  use super::*;

  pub fn query_face_matches(cxt: &Context, target_id: ImgId, req: QueryFaceMatchesRequest) -> Result<(i64, Vec<FaceMatch>), Error> {
    let conn = cxt.make_conn()?;

    let target_index = load_target_index(&conn, target_id)?;

    // Get all valid image indexes which can be checked against
    let checked = get_image_indexes(&conn, Some(target_id))?;

    // Compute the list of images which match the target
    let cutoff = req.cutoff as f64;
    let matches = compute_matches(&conn, target_id, &target_index, checked, req.marked, cutoff)?;
    let total_count = matches.len() as i64;
    let matches = apply_pagination(matches, 0, req.size);

    // Return detailed image data for each match
    let (images, scores): (Vec<Image>, Vec<f64>) = matches.into_iter().unzip();
    let images = get_images_details(&conn, images)?;
    Ok((total_count, images.into_iter().zip(scores).map(|(item, score)| FaceMatch { item, score }).collect()))
  }

  fn compute_matches(conn: &DBConn, target_id: ImgId, target_index: &ImageIndex, checked: Vec<(Image, ImageIndex)>, marked: Option<bool>, cutoff: f64) -> Result<Vec<(Image, f64)>, Error> {
    let matches: Vec<(Image, f64)> = checked
      .into_iter()
      .map(|(image, ind)| (image, target_index.get_distance(&ind)))
      .filter(|(image, s)| s == s && *s < cutoff && image.id != target_id)
      .collect();
    match marked {
      None => Ok(matches),
      Some(value) => {
        let marked_dupes: HashSet<ImgId> = get_marked_dupes(conn, Some(target_id))?
          .into_iter().map(|(_, id2)| id2).collect();
        Ok(matches.into_iter()
          .filter(|(img, _)| value == marked_dupes.contains(&img.id))
          .collect())
      }
    }
  }

  fn load_target_index(conn: &DBConn, target_id: ImgId) -> Result<ImageIndex, Error> {
    let target_index = image_index::table
      .filter(image_index::id.eq(target_id))
      .load::<ImageIndex>(conn)?
      .pop()
      .ok_or(Error::not_found().msg("Target image does not exist or could not be indexed"))?;

    if target_index.is_okay() {
      if target_index.has_some_encoding() {
        Ok(target_index)
      } else {
        Err(Error::request_error("Could not find a face in target image"))
      }
    } else {
      Err(Error::internal("Index data for target image appears to have been corrupted"))
    }
  }
}

fn get_image_indexes(conn: &DBConn, skip_id: Option<ImgId>) -> Result<Vec<(Image, ImageIndex)>, Error> {
  // Get list of all indexed images which are not marked as duplicates or deleted.
  let indexes = image_index::table
    .inner_join(image::table)
    .select((image::all_columns, image_index::all_columns))
    .filter(image::deleted.eq(false))
    .load::<(Image, ImageIndex)>(conn)?
    .into_iter();
  Ok(if let Some(id) = skip_id {
    indexes.filter(|(img, ind)| img.id != id && ind.has_some_encoding()).collect()
  } else {
    indexes.filter(|(_, ind)| ind.has_some_encoding()).collect()
  })
}

fn apply_pagination<T>(mut entries: Vec<(T, f64)>, page: usize, size: usize) -> Vec<(T, f64)> {
  // Sort by how closely related they are
  entries.sort_unstable_by(|(_, d1), (_, d2)| d1.partial_cmp(d2).unwrap());
  let start_index = (page * size) as usize;
  if start_index != 0 {
    entries.drain(0..start_index);
  }
  entries.truncate(size);
  entries
}

fn get_marked_dupes(conn: &DBConn, target_id: Option<ImgId>) -> Result<HashSet<(ImgId, ImgId)>, Error> {
  let mut query = dupe::table.select((dupe::img1_id, dupe::img2_id)).into_boxed();
  if let Some(id) = target_id {
    query = query.filter(dupe::img1_id.eq(id));
  }
  Ok(query.load::<(ImgId, ImgId)>(conn)?
    .into_iter()
    .collect())
}

impl DupeCandidate {
  pub fn new(item1: ImageData, item2: ImageData, score: f64) -> DupeCandidate {
    let id2 = item2.0.id;
    // Determine if these two items are duplicates of one another
    let mark = item1.4.iter().find(|(id, _)| *id == id2).map(|(_, dt)| dt.clone());
    DupeCandidate { item1, item2, score, mark }
  }
}
