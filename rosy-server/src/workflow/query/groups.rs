use diesel::{dsl::exists, prelude::*, sql_types};

use crate::{
  api::requests::QueryGroupsRequest,
  config::DBType,
  context::Context,
  error::Error,
  models::{Group, GroupData},
  query::*,
  schema::{group, group_name, group_tag}
};
use super::super::get_groups::get_groups_details;

pub enum QueryGroupSortMethod {
  Id,
  Random,
  CreatedTime,
}


pub fn query_groups(
  cxt: &Context, req: QueryGroupsRequest,
) -> Result<(i64, Vec<GroupData>), Error> {
  let conn = cxt.make_conn()?;

  // This call makes 3 + 3 + 6 = 12 DB calls total (oof)
  // First it makes three calls to Groups/Tags/Names in order to generate the query count
  // Then it makes three calls to Groups/Tags/Names to get actual groups
  // Then it requires 6 calls to actually load all the related objects.

  // We have to issue query twice, once to get full count and once to get results
  let count = apply_filters(&req, group::table.into_boxed())
    .count()
    .get_result(&conn)?;

  let mut query = apply_filters(&req, group::table.into_boxed());
  query = apply_sorting(&req, query)?
    .limit(req.size)
    .offset(req.page * req.size);

  let groups = query.load::<Group>(&conn)?;
  Ok((count, get_groups_details(&conn, groups)?))
}

no_arg_sql_function!(RANDOM, sql_types::Integer, "Represents the sql RANDOM() function");

fn apply_filters<'a>(
  req: &'a QueryGroupsRequest, mut query: group::BoxedQuery<'a, DBType>,
) -> group::BoxedQuery<'a, DBType> {
  if let Some(tags) = &req.tags {
    for tag_id in tags {
      query = query.filter(exists(
        group_tag::table
          .filter(group_tag::grp_id.eq(group::id))
          .filter(group_tag::tag_id.eq(tag_id))
          .limit(1),
      ));
    }
  }

  if let Some(names) = &req.names {
    for name_id in names {
      query = query.filter(exists(
        group_name::table
          .filter(group_name::grp_id.eq(group::id))
          .filter(group_name::nme_id.eq(name_id))
          .limit(1),
      ));
    }
  }

  if let Some(created_time_filters) = &req.created_time {
    for filter in created_time_filters.iter() {
      query = match filter.relation {
        OrderRelation::Higher => query.filter(group::created_time.ge(filter.value)),
        OrderRelation::Lower => query.filter(group::created_time.le(filter.value)),
      };
    }
  }

  if let Some(state) = req.hidden {
    query = query.filter(group::hidden.eq(state));
  }

  query = query.filter(group::deleted.eq(false));

  query
}

fn apply_sorting<'a>(
  req: &'a QueryGroupsRequest, mut query: group::BoxedQuery<'a, DBType>,
) -> Result<group::BoxedQuery<'a, DBType>, Error> {
  // Sort results by requested column
  use QueryGroupSortMethod::*;
  match req.sort_by {
    Id => {
      if let SortDirection::Desc = req.direction {
        query = query.order(group::id.desc())
      } else {
        query = query.order(group::id.asc())
      }
      Ok(query)
    }
    CreatedTime => {
      if let SortDirection::Desc = req.direction {
        query = query.order(group::created_time.desc());
        Ok(query.then_order_by(group::id.desc()))
      } else {
        query = query.order(group::created_time.asc());
        Ok(query.then_order_by(group::id.asc()))
      }
    }
    Random => {
      Ok(query.order(RANDOM))
    }
  }
}

