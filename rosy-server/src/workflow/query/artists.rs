use diesel::prelude::*;

use crate::{
  api::requests::QueryArtistsRequest,
  context::Context,
  error::Error,
  models::Artist,
  schema::artist,
};
use super::{
  sort_by_cmp,
  sort_by_float,
  sort_by_key,
  super::get::get_artists_details,
};

pub enum QueryArtistSortMethod {
  Id,
  CreatedTime,
  ImageCount,
  Score,
  Value,
}

pub fn query_artists(
  cxt: &Context, req: QueryArtistsRequest,
) -> Result<(i64, Vec<(Artist, (usize, f32))>), Error> {
  let conn = cxt.make_conn()?;

  let items = artist::table.load(&conn)?;
  let mut data = get_artists_details(&conn, items)?;
  let t = &mut data;

  use QueryArtistSortMethod::*;
  match req.sort_by {
    Id => sort_by_key(t, |item| item.0.id, req.direction),
    CreatedTime => sort_by_key(t, |item| item.0.created_time, req.direction),
    ImageCount => sort_by_key(t, |item| item.1.0, req.direction),
    Score => sort_by_float(t, |item| item.1.1, req.direction),
    Value => sort_by_cmp(t, |a, b| a.0.name.cmp(&b.0.name), req.direction),
  };
  Ok((data.len() as i64, data))
}
