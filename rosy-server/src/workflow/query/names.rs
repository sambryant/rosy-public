use diesel::prelude::*;

use crate::{
  api::requests::QueryNamesRequest,
  context::Context,
  error::Error,
  models::NameDetailsData,
  schema::name,
};
use super::{
  sort_by_cmp,
  sort_by_float,
  sort_by_key,
  super::get::get_names_details,
};

pub enum QueryNameSortMethod {
  Id,
  CreatedTime,
  GroupCount,
  ImageCount,
  Score,
  Value,
}

pub fn query_names(
  cxt: &Context, req: QueryNamesRequest,
) -> Result<(i64, Vec<NameDetailsData>), Error> {
  let conn = cxt.make_conn()?;

  let items = name::table.load(&conn)?;
  let mut data = get_names_details(&conn, items)?;
  let t = &mut data;

  use QueryNameSortMethod::*;
  match req.sort_by {
    Id => sort_by_key(t, |item| item.0.id, req.direction),
    CreatedTime => sort_by_key(t, |item| item.0.created_time, req.direction),
    GroupCount => sort_by_key(t, |item| item.1, req.direction),
    ImageCount => sort_by_key(t, |item| item.2.0, req.direction),
    Score => sort_by_float(t, |item| item.2.1, req.direction),
    Value => sort_by_cmp(t, |a, b| a.0.value.cmp(&b.0.value), req.direction),
  };
  Ok((data.len() as i64, data))
}
