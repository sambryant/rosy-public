use diesel::prelude::*;
use std::collections::HashSet;
use std::iter::FromIterator;

use crate::{
  config::DBConn,
  context::Context,
  models::{Id, ImgId, Image, ImageName, ImageTag, NewRating, NmeId, Rating, TagId},
  schema::{image, image_name, image_tag, rating}
};

#[derive(Debug)]
pub enum Error {
  NotFound,
  Deleted,
  MultipleArtistError,
  MultipleGroupError,
  DatabaseConnError,
  DatabaseReadError,
  DatabaseWriteError
}

pub fn merge_images(cxt: &Context, image_ids: &Vec<ImgId>) -> Result<(), Error> {
  if image_ids.len() == 0 {
    return Err(Error::NotFound)
  }

  let conn = cxt.make_conn().map_err(|_| Error::DatabaseConnError)?;

  let mut images = get_images(&conn, image_ids)?;
  let names: Vec<Vec<NmeId>> = ImageName::belonging_to(&images)
    .load::<ImageName>(&conn).map_err(|_| Error::DatabaseReadError)?
    .grouped_by(&images)
    .into_iter()
    .map(|name_group| name_group.into_iter().map(|name| name.nme_id).collect())
    .collect();
  let tags: Vec<Vec<TagId>> = ImageTag::belonging_to(&images)
    .load::<ImageTag>(&conn).map_err(|_| Error::DatabaseReadError)?
    .grouped_by(&images)
    .into_iter()
    .map(|tag_group| tag_group.into_iter().map(|tag| tag.tag_id).collect())
    .collect();
  let ratings = Rating::belonging_to(&images)
    .load::<Rating>(&conn).map_err(|_| Error::DatabaseReadError)?
    .grouped_by(&images);

  // Check that images do not have multiple artists / groups.
  let (artist_id, group_id) = get_unique_groups_artists(&images)?;

  // Resolve column values from image table
  let hidden = !images.iter().any(|img| !img.hidden); // if any image is not hidden, then target is not hidden
  let favorite = images.iter().any(|img| img.favorite);
  let processed_time = images.iter()
    .filter(|img| img.processed_time.is_some())
    .map(|img| img.processed_time.unwrap())
    .max();
  let processed = processed_time.is_some();
  let all_ratings = ratings.iter().flatten().map(|r| r.value).collect();
  let rating_average = Rating::average_value(&all_ratings);
  let rating_count = all_ratings.len() as i32;

  let keep_img = images.pop().unwrap();
  let keep_id = keep_img.id;
  let fold_ids  = images.iter().map(|img| img.id).collect::<Vec<ImgId>>();
  let fold_names = compute_folded_relations::<ImageName>(keep_id, names);
  let fold_tags = compute_folded_relations::<ImageTag>(keep_id, tags);
  let fold_ratings = compute_folded_ratings(keep_id, ratings);

  conn.transaction::<_, diesel::result::Error, _>(|| {
    diesel::update(image::table)
      .filter(image::id.eq(keep_id))
      .set((
        image::rating_average.eq(rating_average),
        image::rating_count.eq(rating_count),
        image::hidden.eq(hidden),
        image::favorite.eq(favorite),
        image::processed.eq(processed),
        image::processed_time.eq(processed_time),
        image::artist_id.eq(artist_id),
        image::group_id.eq(group_id),
      )).execute(&conn)?;
    diesel::insert_into(image_name::table).values(&fold_names).execute(&conn)?;
    diesel::insert_into(image_tag::table).values(&fold_tags).execute(&conn)?;
    diesel::insert_into(rating::table).values(&fold_ratings).execute(&conn)?;

    diesel::update(image::table)
      .filter(image::id.eq_any(fold_ids))
      .set(image::deleted.eq(true))
      .execute(&conn)?;
    Ok(())
  })
  .map_err(|_| Error::DatabaseWriteError)
}

fn get_images(conn: &DBConn, image_ids: &Vec<ImgId>) -> Result<Vec<Image>, Error> {
  let images = image::table
    .filter(image::id.eq_any(image_ids))
    .order(image::id.desc()) // so that pop removes the one we keep
    .load::<Image>(conn)
    .map_err(|_| Error::DatabaseReadError)?;

  // Check that all of the images were found and none of them are deleted
  if images.len() != image_ids.len() {
    Err(Error::NotFound)
  } else if images.iter().any(|img| img.deleted) {
    Err(Error::Deleted)
  } else {
    Ok(images)
  }
}

fn compute_folded_relations<U>(keep_id: ImgId, mut grouped_relations: Vec<Vec<Id>>) -> Vec<U>
where U: From<(ImgId, Id)> {
  let keep_objs = HashSet::<Id>::from_iter(grouped_relations.pop().unwrap());
  let mut fold_objs = HashSet::<Id>::from_iter(grouped_relations.into_iter().flatten());
  fold_objs.retain(|v| !keep_objs.contains(v));
  fold_objs
    .into_iter()
    .map(|id| (keep_id, id).into())
    .collect()
}

fn compute_folded_ratings(keep_id: ImgId, mut ratings: Vec<Vec<Rating>>) -> Vec<NewRating> {
  ratings.pop();
  ratings.into_iter().flatten()
    .map(|r| NewRating { value: r.value, time: r.time, image_id: keep_id })
    .collect()
}

fn get_unique_groups_artists(images: &Vec<Image>) -> Result<(Option<i32>, Option<i32>), Error> {
  // Check that the images do not belong to two or more different group or artists
  let mut artist_ids: HashSet<i32> = HashSet::new();
  let mut group_ids: HashSet<i32> = HashSet::new();
  for image in images.iter() {
    if let Some(artist_id) = image.artist_id {
      artist_ids.insert(artist_id);
    }
    if let Some(group_id) = image.group_id {
      group_ids.insert(group_id);
    }
  }

  if group_ids.len() > 1 {
    Err(Error::MultipleGroupError)
  } else if artist_ids.len() > 1 {
    Err(Error::MultipleArtistError)
  } else {
    let group_id = group_ids.into_iter().collect::<Vec<i32>>().pop();
    let artist_id = artist_ids.into_iter().collect::<Vec<i32>>().pop();
    Ok((artist_id, group_id))
  }
}
