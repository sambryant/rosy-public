use diesel::prelude::*;
use std::collections::{HashMap, HashSet};

use crate::prelude::*;
use crate::{
  error::Error,
  models::{FaceEncoding, Image, ImageIndex, ImageName, ImgId, Name, NmeId},
  schema::{image, image_index, image_name, name},
};


/// Returns a vector of Names which are the closest matches for the detected facial encodings.
///
/// This essentially runs KNN with k=1.
///
/// Roughly, this looks at all of the extracted facial encodings in the target image. For each
/// detected face, it matches it against all reference images (see below) to find the best match.
/// It then returns a vector of the best matched name for each detected encoding.
///
/// Reference images: these are images in the library with exactly one Name and one FaceEncoding.
/// All other images are ignored because the encoding to name mapping is ambiguous. In the future,
/// we may want to use a more sophisticated algorithm to relax this restriction.
pub fn get_name_suggestions(cxt: &Context, target_id: ImgId) -> Result<Vec<Name>, Error> {
  let conn = cxt.make_conn()?;

  match load_target_encodings(&conn, target_id)? {
    None => Ok(Vec::new()),
    Some(encodings) => {
      let checked: Vec<(Image, ImageName, FaceEncoding)> = get_single_face_images(&conn, target_id)?;
      let matches: HashSet<NmeId> = encodings.into_iter()
        .filter_map(|encoding| get_best_match(encoding, &checked))
        .collect();
      if matches.len() == 0 {
        Ok(Vec::new())
      } else {
        Ok(name::table
          .filter(name::id.eq_any(&matches))
          .load::<Name>(&conn)?)
      }
    }
  }
}

/// Loads a vector of face encodings for the target image.
///
/// If there are no encodings, this will return None instead of an empty vector.
fn load_target_encodings(conn: &DBConn, target_id: ImgId) -> Result<Option<Vec<FaceEncoding>>, Error> {
  match image_index::table
    .filter(image_index::id.eq(target_id))
    .load::<ImageIndex>(conn)?
    .pop()
  {
    None => {
      log_err(format!("Image missing index: {}", target_id));
      Ok(None)
    },
    Some(index) => {
      if index.is_okay() {
        if let Some(encodings) = index.encodings {
          if encodings.len() > 0 {
            return Ok(Some(encodings))
          }
        }
        Ok(None)
      } else {
        log_err(format!("Image index is corrupted: {}", target_id));
        Ok(None)
      }
    },
  }
}

/// Returns Id of Name corresponding to face encoding that is closest to the target encoding.
fn get_best_match(target: FaceEncoding, checked: &Vec<(Image, ImageName, FaceEncoding)>) -> Option<NmeId> {
  let mut min = None;
  for (i, (_, _, encoding)) in checked.into_iter().enumerate() {
    let score = ImageIndex::distance(&target, encoding);
    if let Some((_, min_score)) = min {
      if score < min_score {
        min = Some((i, score));
      }
    } else {
      min = Some((i, score));
    }
  }
  min.map(|(i, _)| checked[i].1.nme_id)
}

/// Returns all images in library which have a single tagged name and a single face encoding.
fn get_single_face_images(conn: &DBConn, skip_id: ImgId) -> Result<Vec<(Image, ImageName, FaceEncoding)>, Error> {

  // Get all ImageNames and make map from each image id to all of the matching ImageNames.
  let image_names = image_name::table
    .load::<ImageName>(conn)?;
  let mut img_id_to_name_ids: HashMap<ImgId, Vec<ImageName>> = HashMap::new();
  for imgn in image_names.into_iter() {
    if let Some(name_ids) = img_id_to_name_ids.get_mut(&imgn.img_id) {
      name_ids.push(imgn);
    } else {
      img_id_to_name_ids.insert(imgn.img_id, vec![imgn]);
    }
  }
  // Create map from image id to `ImageName` for all images with exactly one ImageName.
  let mut img_id_to_name: HashMap<ImgId, ImageName> = img_id_to_name_ids.into_iter()
    .filter_map(|(img_id, mut names)| {
      if names.len() == 1 && img_id != skip_id {
        Some((img_id, names.pop().unwrap()))
      } else {
        None
      }
    })
    .collect();
  // Get all ImageIndexes with exactly one corresponding ImageName and exactly one FaceEncoding
  Ok(image_index::table
    .inner_join(image::table)
    .select((image::all_columns, image_index::all_columns))
    .filter(image::deleted.eq(false))
    .filter(image::id.eq_any(img_id_to_name.keys()))
    .load::<(Image, ImageIndex)>(conn)?
    .into_iter()
    .filter_map(|(img, index)| {
      let id = img.id;
      match (img_id_to_name.remove(&id), index.has_one_face_encoding()) {
        (Some(image_name), Some(face_encoding)) => Some((img, image_name, face_encoding)),
        _ => None
      }
    })
    .collect())
}
