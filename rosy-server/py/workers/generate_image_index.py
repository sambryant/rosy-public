import hashlib
import numpy as np
import PIL
import struct
import sqlalchemy
import sys
from ..models import ImageIndex
from sqlalchemy import BLOB, Integer, Column, BIGINT
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from typing import Any, List, Tuple, Optional

CODE_SUCCESS = 0
CODE_FILE_READING_ERR = 2 # your fault
CODE_ENVIRONMENT_ERR =  3 # your fault
CODE_BAD_ARGUMENTS_ERR = 4 # your fault
CODE_INTERNAL_BUG = 5 # my fault
CODE_UNKNOWN_CAUSE = 6 # ?

def err(msg: str, code: int) -> None:
  print('Err: %s' % msg)
  sys.exit(code)

def get_session(database_url: str, schema: str):
  Session = sessionmaker(bind=create_engine('%s/%s' % (database_url, schema)))
  return Session()

def go(database_url: str, schema: str, image_id: str, filename: str) -> None:
  # Import here because its slow
  from . import image_indexer

  filehash, encodings, locations = None, None, None

  try:
    encodings, locations = image_indexer.compute_facial_data(filename)
    filehash = image_indexer.compute_filehash(filename)
  except image_indexer.MissingFileError:
    err('Target file does not exist', CODE_FILE_READING_ERR)
  except image_indexer.GPURuntimeError:
    err('Something went wrong, most likely you ran out of vram', CODE_INTERNAL_BUG)
  except image_indexer.ComputationError:
    err('Something went wrong in face recognition calculation', CODE_INTERNAL_BUG)
  try:
    image_index = ImageIndex(
      id=image_id,
      encodings=encodings,
      locations=locations,
      filehash=filehash)
    session = get_session(database_url, schema)
    session.add(image_index)
    session.commit()
    sys.exit(CODE_SUCCESS)
  except Exception as ex:
    print('encountered SQLAlchemy exception: %s' % str(ex))
    err('Database error', CODE_UNKNOWN_CAUSE)

if __name__ == '__main__':
  if not len(sys.argv) == 5:
    err('Expected four arguments', CODE_BAD_ARGUMENTS_ERR)

  database_url = sys.argv[1]
  schema = sys.argv[2]
  image_id = sys.argv[3]
  filename = sys.argv[4]

  # Check that dependencies API has not changed
  if not hasattr(PIL, 'UnidentifiedImageError'):
    err('Invalid Pillow version', CODE_ENVIRONMENT_ERR)

  go(database_url, schema, image_id, filename)

