import argparse
import os
import PIL
import json
import sys
import sqlalchemy
from typing import Optional
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ..models import Image, ImageIndex
from . import image_indexer

CODE_SUCCESS = 0
CODE_BAD_ARGUMENTS_ERR = 2
CODE_ENVIRONMENT_ERR =  3
CODE_DATABASE_ERR = 4
CODE_UNKNOWN_CAUSE = 5

def err(msg: str, code: int) -> None:
  print('[worker] reindex_images: %s' % msg, file=sys.stderr)
  sys.exit(code)

def nonfatal_err(msg: str) -> None:
  print('[worker] reindex_images: %s' % msg, file=sys.stderr)

def set_thread_progress(update_file: Optional[str], number: int, total: int) -> None:
  if update_file:
    try:
      # Write to a tmp file and the swap it with real file to prevent multithreading issues
      tmp_fname = update_file + '.tmp'
      with open(tmp_fname, 'w') as f:
        f.write(json.dumps({
          'current': number,
          'total': total,
          'state': 'active'
        }))
      os.rename(tmp_fname, update_file)

    except Exception as ex:
      nonfatal_err('failed to update thread file (%s)' % update_file)
      nonfatal_err('cause: %s' % str(ex))

def go(image_dir: str, database_url: str, schema: str, exclude_existing: bool, update_file: str=None) -> None:
  s = sessionmaker(bind=create_engine('%s/%s' % (database_url, schema)))()
  images = s.query(Image).all()

  for i, image in enumerate(images):
    set_thread_progress(update_file, i, len(images))

    if image.is_video or image.deleted:
      continue

    index = s.query(ImageIndex).get(image.id)

    if not exclude_existing or not index or not index.encodings:
      try:
        fname = os.path.join(image_dir, image.filename)
        encodings, locations = image_indexer.compute_facial_data(fname)
        filehash = image_indexer.compute_filehash(fname)
      except image_indexer.IIError as ex:
        nonfatal_err('failed to index image %d' % image.id)
        continue
      if not index:
        index = ImageIndex(id=image.id)
        s.add(index)
      index.encodings = encodings
      index.locations = locations
      index.filehash = filehash
      try:
        s.commit()
      except Exception as ex:
        err('failed to commit changes: %s' % str(ex), CODE_DATABASE_ERR)

  set_thread_progress(update_file, len(images), len(images))


if __name__ == '__main__':
  # Check that dependencies API has not changed
  if not hasattr(PIL, 'UnidentifiedImageError'):
    err('Invalid Pillow version', CODE_ENVIRONMENT_ERR)

  parser = argparse.ArgumentParser()
  parser.add_argument(
    'image_dir',
    type=str,
    help='Name of image directory for which image filenames are relative to')
  parser.add_argument(
    'database_url',
    type=str,
    help='URI of SQL database including username, password, and protocol')
  parser.add_argument(
    'schema',
    type=str,
    help='Schema to use in SQL database')
  parser.add_argument('--exclude-existing', dest='exclude_existing', action='store_true')
  parser.add_argument('--include-existing', dest='exclude_existing', action='store_false')
  parser.set_defaults(exclude_existing=True)
  parser.add_argument(
    'update_file',
    type=str,
    help='Name of temporary file where worker progress will be written in JSON')
  args = parser.parse_args(sys.argv[1:])

  go(args.image_dir, args.database_url, args.schema, args.exclude_existing, args.update_file)
