import face_recognition
import sys
import hashlib
import numpy as np
import PIL
import struct
import sqlalchemy
from typing import Any, List, Tuple, Optional

class IIError(Exception):
  """
  Base class for all errors in this file.
  """
  pass

class MissingFileError(IIError):
  """
  Caused when target file doesnt exist
  """
  pass

class GPURuntimeError(IIError):
  """
  Most likely triggered when GPU runs out of memory.
  """
  pass

class ComputationError(IIError):
  """
  Catch-all error for when face_recognition library returns unexpected things.
  """
  pass

def compute_filehash(fname: str) -> bytes:
  BUF_SIZE = 65536
  md5 = hashlib.md5()

  try:
    with open(fname, 'rb') as f:
      while True:
        data = f.read(BUF_SIZE)
        if not data:
          break
        md5.update(data)
    return md5.digest()
  except FileNotFoundError as ex:
    raise MissingFileError()

def compute_facial_data(fname: str) -> Tuple[Optional[bytes], Optional[bytes]]:
  try:
    loaded_image = face_recognition.load_image_file(fname)
    w, h, _ = loaded_image.shape
    pixels = w * h
    if pixels > 10000000:
      wh_ratio = w/h
      h = int(np.sqrt(10000000/wh_ratio))
      w = int(wh_ratio * h)
      loaded_image = np.array(PIL.Image.fromarray(loaded_image).resize((w, h)))

    locations = face_recognition.face_locations(loaded_image)
    encodings = np.array(face_recognition.face_encodings(
      loaded_image,
      known_face_locations=locations)).tolist()
    return _serialize_face_data(encodings, locations)
  except PIL.UnidentifiedImageError as ex:
    return (None, None)
  except FileNotFoundError as ex:
    raise MissingFileError()
  except ComputationError as ex:
    raise ex
  except RuntimeError as ex:
    print('encountered exception: %s' % str(ex))
    raise GPURuntimeError()

def _serialize_face_data(encodings: List[List[float]], locations: List[Tuple[int, int, int, int]]) -> Tuple[bytes, bytes]:
  if len(encodings) != len(locations):
    raise ComputationError()
  num = len(encodings)

  if num != 0:
    if len(encodings[0]) != 128:
      raise ComputationError()
    if len(locations[0]) != 4:
      raise ComputationError()

  ser_encs = bytearray(num * 128 * 8)
  ser_locs = bytearray(num * 4   * 4)
  for i in range(num):
    for j in range(128):
      struct.pack_into(">d", ser_encs, (i * 128 + j) * 8, encodings[i][j])
    for j in range(4):
      struct.pack_into(">i", ser_locs, (i * 4   + j) * 4, locations[i][j])
  return ser_encs, ser_locs
