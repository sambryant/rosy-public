from __future__ import annotations

import sqlalchemy
import sys
import sqlalchemy as sa
from sqlalchemy import BINARY, Integer, Column, BIGINT, String, Float, ForeignKey, SmallInteger, Boolean, Text
from sqlalchemy.ext.declarative import declarative_base
from typing import Any, ClassVar, Dict, List, Optional, Type, Tuple, Set, cast

ImageId = Integer
TAG_SIZE = 64
USERNAME_SIZE = 24
GROUP_TITLE_SIZE = 120
REL_OPTS = {
  'ondelete': 'CASCADE',
  'onupdate': 'RESTRICT'
}

Base = declarative_base()

class User(Base):
  __tablename__ = 'user'
  id = Column(Integer, primary_key=True)
  username = Column(String(USERNAME_SIZE), nullable=False, unique=True)
  password = Column(String(80), nullable=False)

class Image(Base):
  __tablename__ = 'image'
  id = Column(ImageId, primary_key=True)
  filename = Column(Text, unique=True, nullable=False)
  thumbnail = Column(Text, nullable=True)
  created_time = Column(BIGINT, nullable=False)
  deleted = Column(Boolean, default=False, nullable=False, server_default='F')
  hidden = Column(Boolean, default=False, nullable=False, server_default='F')
  favorite = Column(Boolean, default=False, nullable=False, server_default='F')
  is_video = Column(Boolean, default=False, nullable=False, server_default='F')
  processed = Column(Boolean, default=False, nullable=False, server_default='F')
  processed_time = Column(BIGINT, default=None, nullable=True)
  source = Column(Text, default=None, nullable=True)
  source_page = Column(Text, default=None, nullable=True)
  rating_average = Column(Float(precision=4), default=None, nullable=True)
  rating_count = Column(Integer, default=0, nullable=False)
  artist_id = Column(Integer, ForeignKey('artist.id', ondelete='SET NULL', onupdate='RESTRICT'))
  group_id = Column(Integer, ForeignKey('group.id', ondelete='SET NULL', onupdate='RESTRICT'))

class ImageIndex(Base):
  __tablename__ = 'image_index'
  id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), primary_key=True)
  encodings = Column(BINARY, nullable=True)
  locations = Column(BINARY, nullable=True)
  filehash = Column(BINARY, nullable=True)

class Name(Base):
  __tablename__ = 'name'
  image_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), primary_key=True)
  value = Column(String(TAG_SIZE), primary_key=True)

class Tag(Base):
  __tablename__ = 'tag'
  image_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), primary_key=True)
  value = Column(String(TAG_SIZE), primary_key=True)

class Rating(Base):
  __tablename__ = 'rating'
  id = Column(Integer, primary_key=True)
  value = Column(Integer, nullable=False)
  time = Column(BIGINT, nullable=False)
  image_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), nullable=False)

class ImageMerge(Base):
  __tablename__ = 'image_merge'
  keep_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), nullable=False, primary_key=True)
  merge_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), nullable=False, primary_key=True)

class Artist(Base):
  __tablename__ = 'artist'
  id = Column(Integer, primary_key=True)
  created_time = Column(BIGINT, nullable=False)
  deleted = Column(Boolean, default=False, nullable=False, server_default='F')
  name = Column(Text, unique=True, nullable=False)

class Group(Base):
  __tablename__ = 'group'
  id = Column(Integer, primary_key=True)
  created_time = Column(BIGINT, nullable=False)
  deleted = Column(Boolean, default=False, nullable=False, server_default='F')
  hidden = Column(Boolean, default=False, nullable=False, server_default='F')
  description = Column(Text, nullable=True)
  title = Column(Text, unique=True, nullable=False)

if __name__ == '__main__':
  c = Connector(schema='test', sql_echo=True)
  c.create_all()
