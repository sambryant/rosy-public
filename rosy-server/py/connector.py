from __future__ import annotations

import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
from typing import Any

from .models import Base
from .utils import prompt_yes_no

class Connector(object):
  db_schema: str
  db_url: str
  db_uri: str
  sql_echo: bool
  _engine: Any
  _session_maker: Any

  def __init__(self, schema: str=None, sql_echo: bool=False) -> None:
    load_dotenv()
    self.sql_echo = sql_echo
    self.db_url = os.environ.get('DATABASE_URL', None)
    if not self.db_url:
      raise Exception('DATABASE_URL must be set in env variables or .env')
    self.db_schema = schema or os.environ.get('DATABASE_SCHEMA', None)
    if not self.db_schema:
      raise Exception('DATABASE_SCHEMA must be set in env variables or .env')
    self.db_uri = '%s/%s' % (self.db_url, self.db_schema)
    self._engine = create_engine(self.db_uri, echo=self.sql_echo)
    self._session_maker = sessionmaker(bind=self._engine)

  def get_session(self):
    return self._session_maker()

  def drop_all(self):
    if prompt_yes_no('The schema is "%s". Are you sure you want to drop all tables?' % self.db_schema, default=False):
      print('Dropping all tables')
      Base.metadata.drop_all(bind=self._engine)

  def create_all(self):
    print('Creating all tables')
    Base.metadata.create_all(bind=self._engine)

if __name__ == '__main__':
  c = Connector(schema='test', sql_echo=True)
  c.drop_all()
  c.create_all()
