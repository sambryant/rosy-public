from __future__ import annotations

import os, sys, re
import progressbar
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from typing import Any

import models_mysql as mymod
import models_pg as pgmod
from utils import prompt_yes_no

MYSQL_DB = 'mysql://%s:%s@localhost/%s'
PG_DB = 'postgresql://postgres:%s@localhost/%s'

## IMPORTANT: Read this !!
## When you are done the postgres id sequence will be out of sync with the database. To fix this,
## run psql and type the following:
# BEGIN;
# -- protect against concurrent inserts while you update the counter
# LOCK TABLE image IN EXCLUSIVE MODE;
# -- Update the sequence
# SELECT setval('image_id_seq', COALESCE((SELECT MAX(id)+1 FROM image), 1), false);
# COMMIT;

class Connector(object):
  db_schema: str
  db_url: str
  db_uri: str
  sql_echo: bool
  _engine: Any
  _session_maker: Any

  def __init__(self, username, password, schema, mysql: bool, sql_echo: bool=False) -> None:
    self.sql_echo = sql_echo
    self.db_schema = schema

    if mysql:
      self.db_uri = MYSQL_DB % (username, password, schema)
    else:
      self.db_uri = PG_DB % (password, schema)
    self._engine = create_engine(self.db_uri, echo=self.sql_echo)
    self._session_maker = sessionmaker(bind=self._engine)

  def get_session(self):
    return self._session_maker()

  def drop_all(self):
    if prompt_yes_no('The schema is "%s". Are you sure you want to drop all tables?' % self.db_schema, default=False):
      print('Dropping all tables')
      pgmod.Base.metadata.drop_all(bind=self._engine)

  def create_all(self):
    print('Creating all tables')
    pgmod.Base.metadata.create_all(bind=self._engine)

if __name__ == '__main__':
  username = input('Username?: ')
  password = input('Password?: ')
  schema = input('Schema?: ')

  my = Connector(username, password, schema, True)
  pg = Connector(username, password, schema, False)
  mys = my.get_session()
  pgs = pg.get_session()

  pg.drop_all()
  pg.create_all()

  # Copy users
  for user in mys.query(mymod.User).all():
    nusr = pgmod.User(id=user.id, username=user.username, password=user.password)
    pgs.add(nusr)
  pgs.commit()

  # Everything that follows is complicated because we are doing two thigns
  # 1. We are converting from MySql to PG which is relatively straightforward
  # 2. We are converting from UUID back to sequential IDs, recovering original IDs when possible
  # This latter constraint is difficult.
  pat = re.compile(r'[^\/]+/([0-9]+)\..+')

  # We assign each image a new id based on its creation date
  flag_found_uuid = False
  last_id = None
  old_id_to_new_id = {}
  new_id_to_old_id = {}
  id_to_img = {}
  images = mys.query(mymod.Image).order_by(mymod.Image.created_time).all()
  print('adding images...')
  for i, img in enumerate(images):

    # Extract original id from filename
    if not flag_found_uuid:
      org_id = pat.match(img.filename).group(1)
      if len(org_id) > 6:
        flag_found_uuid = True
        new_id = last_id + 1
      else:
        new_id = int(org_id)
    else:
      new_id = last_id + 1
    last_id = new_id

    old_id_to_new_id[img.id] = new_id
    new_id_to_old_id[new_id] = img.id
    id_to_img[new_id] = img

    nimg = pgmod.Image(
      id=new_id,
      filename=img.filename,
      created_time=img.created_time,
      deleted=img.deleted,
      hidden=img.hidden,
      favorite=img.favorite,
      is_video=img.is_video,
      processed=img.processed,
      processed_time=img.processed_time,
      average_rating=img.average_rating,
      source=img.source,
      source_page=img.source_page)
    pgs.add(nimg)
  pgs.commit()

  # Add owned objects
  print('adding names...')
  for name in progressbar.progressbar(mys.query(mymod.Name).all()):
    pgs.add(pgmod.Name(image_id=old_id_to_new_id[name.image_id], value=name.value))
  pgs.commit()
  print('adding tags...')
  for tag in progressbar.progressbar(mys.query(mymod.Tag).all()):
    pgs.add(pgmod.Tag(image_id=old_id_to_new_id[tag.image_id], value=tag.value))
  pgs.commit()
  print('adding image indices...')
  for ind in progressbar.progressbar(mys.query(mymod.ImageIndex).all()):
    pgs.add(pgmod.ImageIndex(
      id=old_id_to_new_id[ind.id],
      encodings=ind.encodings,
      locations=ind.locations,
      imghashes=ind.imghashes))
  pgs.commit()
  print('adding ratings...')
  for rat in progressbar.progressbar(mys.query(mymod.Rating).all()):
    pgs.add(pgmod.Rating(
      value=rat.value,
      time=rat.time,
      image_id=old_id_to_new_id[rat.image_id]))
  pgs.commit()

  # print('merges: %d' % len(mys.query(mymod.ImageMerge).all()))
  print('groups: %d' % len(mys.query(mymod.Group).all()))
  print('groups: %d' % len(mys.query(mymod.GroupImage).all()))
