from __future__ import annotations

import sqlalchemy
import sys
import sqlalchemy as sa
from sqlalchemy import Binary, Integer, Column, BIGINT, String, Float, ForeignKey, SmallInteger, Boolean
from sqlalchemy.ext.declarative import declarative_base
from typing import Any, ClassVar, Dict, List, Optional, Type, Tuple, Set, cast

ImageId = Integer
TAG_SIZE = 64
USERNAME_SIZE = 24
FILENAME_SIZE = 120
SOURCE_SIZE = 120
GROUP_TITLE_SIZE = 120
REL_OPTS = {
  'ondelete': 'CASCADE',
  'onupdate': 'RESTRICT'
}

Base = declarative_base()

class User(Base):
  __tablename__ = 'user'
  id = Column(Integer, primary_key=True)
  username = Column(String(USERNAME_SIZE), nullable=False, unique=True)
  password = Column(String(80), nullable=False)

class Image(Base):
  __tablename__ = 'image'
  id = Column(ImageId, primary_key=True)
  filename = Column(String(FILENAME_SIZE), unique=True, nullable=False)
  created_time = Column(BIGINT, nullable=False)
  deleted = Column(Boolean, default=False, nullable=False, server_default='F')
  hidden = Column(Boolean, default=False, nullable=False, server_default='F')
  favorite = Column(Boolean, default=False, nullable=False, server_default='F')
  is_video = Column(Boolean, default=False, nullable=False, server_default='F')
  processed = Column(Boolean, default=False, nullable=False, server_default='F')
  processed_time = Column(BIGINT, default=None, nullable=True)
  average_rating = Column(Float(precision=4), default=None, nullable=True)
  source = Column(String(SOURCE_SIZE), default=None, nullable=True)
  source_page = Column(String(SOURCE_SIZE), default=None, nullable=True)

class ImageIndex(Base):
  __tablename__ = 'image_index'
  id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), primary_key=True)
  encodings = Column(Binary, nullable=True)
  locations = Column(Binary, nullable=True)
  imghashes = Column(Binary, nullable=True)

class Name(Base):
  __tablename__ = 'name'
  image_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), primary_key=True)
  value = Column(String(TAG_SIZE), primary_key=True)

class Tag(Base):
  __tablename__ = 'tag'
  image_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), primary_key=True)
  value = Column(String(TAG_SIZE), primary_key=True)

class Rating(Base):
  __tablename__ = 'rating'
  id = Column(Integer, primary_key=True)
  value = Column(Integer, nullable=False)
  time = Column(BIGINT, nullable=False)
  image_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), nullable=False)

class ImageMerge(Base):
  __tablename__ = 'image_merge'
  keep_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), nullable=False, primary_key=True)
  merge_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), nullable=False, primary_key=True)

class Group(Base):
  __tablename__ = 'group'
  id = Column(Integer, primary_key=True)
  created_time = Column(BIGINT, nullable=False)
  title = Column(String(GROUP_TITLE_SIZE), unique=True, nullable=False)

class GroupImage(Base):
  __tablename__ = 'group_image'
  image_id = Column(ImageId, ForeignKey('image.id', **REL_OPTS), primary_key=True)
  group_id = Column(Integer, ForeignKey('group.id', **REL_OPTS), primary_key=True)

if __name__ == '__main__':
  c = Connector(schema='test', sql_echo=True)
  c.create_all()
